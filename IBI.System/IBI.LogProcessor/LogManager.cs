﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.SqlServerCe;
using System.Timers;
using System.Data;
using System.Configuration;
using Microsoft.SqlServer.Types;
using IBI.Shared.Diagnostics;

namespace IBI.LogProcessor
{
    public class LogManager : IDisposable
    {
        #region Members

        private System.Timers.Timer TmrProcessLogs
        {
            get;
            set;
        }

        private System.Timers.Timer tmrBulkCopyLog
        {
            get;
            set;
        }

        #endregion

        public LogManager()
        {
            AppUtility.Log2File("LogManager", "Log Manager's new instance started");

            this.StartProcessTimer();


            this.tmrBulkCopyLog = new System.Timers.Timer();
            this.tmrBulkCopyLog.Interval = TimeSpan.FromSeconds(2).TotalMilliseconds;
            this.tmrBulkCopyLog.Elapsed += new System.Timers.ElapsedEventHandler(tmrBulkCopyLog_Elapsed);
            this.tmrBulkCopyLog.Enabled = true;

            AppUtility.Log2File("BulkCopy", "BulkCopy timer Initializes, timer is " + this.tmrBulkCopyLog.Enabled); 
        }

        private void StartProcessTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.TmrProcessLogs == null)
            {
                this.TmrProcessLogs = new System.Timers.Timer();
                this.TmrProcessLogs.Elapsed += new System.Timers.ElapsedEventHandler(TmrProcessLogs_Elapsed);
            }

            this.TmrProcessLogs.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(15); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            nextTick = nextTick.AddSeconds(-1 * nextTick.Second);

            this.TmrProcessLogs.Interval = Math.Max(5000, nextTick.Subtract(DateTime.Now).TotalMilliseconds);
            this.TmrProcessLogs.Enabled = true;

            AppUtility.Log2File("LogManager", "LogProcessing Timer initializes");
            AppUtility.Log2File("LogManager", "Next log processing at " + nextTick.ToString());

        }


        private void TmrProcessLogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.TmrProcessLogs.Enabled = false;
            AppUtility.Log2File("LogManager", "ProcessLog Timer Stops + ProcessLog Starts");


            try
            {
                this.ProcessLogs();
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("LogManager", "ProcessLog Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                this.StartProcessTimer();
                AppUtility.Log2File("LogManager", "ProcessLog Timer initializes again");
            }
        }
        public void ProcessLogs()
        {
            if (!AppSettings.LogProcessingEnabled())
                return;

            AppUtility.Log2File("LogManager", "Log Processing --> Loop on Log directories");
            foreach (String logDirectory in Directory.GetDirectories(Path.Combine(AppSettings.GetResourceDirectory(), "Logs")))
            {
                //SqlCeConnection con = sqlMgr.GetNewDatabase(logDirectory.Substring(logDirectory.LastIndexOf("\\")+1));
                //SqlCeConnection con = SqlCompact.SQLCompactManager.GetConnection();

                AppUtility.Log2File("LogManager", "Log Processing --> Processing directory: " + logDirectory);
                this.ProcessLogs(new DirectoryInfo(logDirectory));

            }

        }

        private void ProcessLogs(DirectoryInfo logDirectory)
        {

            FileInfo currentLogFile = null;

            try
            {
                AppUtility.Log2File("LogManager", "Log Processing --> Looping through all the files of : " + logDirectory);
                foreach (FileInfo logFile in logDirectory.GetFiles())
                {
                    System.Threading.Thread.Sleep(AppSettings.LogProcessingFileInterval());

                    currentLogFile = logFile;

                    //if (logFile.LastWriteTime < DateTime.Now.AddMinutes(-5)) //Make sure that the file is fully written
                    if (logFile.LastWriteTime < DateTime.Now.AddMinutes(5)) //Make sure that the file is fully written
                    {
                        AppUtility.WriteLine("Processing log: " + logFile.FullName);

                        String targetDirectory = Path.Combine(Path.GetTempPath(), "IBI_Logs\\" + Guid.NewGuid());

                        if (!Directory.Exists(targetDirectory))
                            Directory.CreateDirectory(targetDirectory);


                        AppUtility.Log2File("LogManager", "  Using temporary directory: " + targetDirectory);

                        try
                        {

                            String targetLogName = Path.Combine(targetDirectory, logFile.Name);

                            List<String> logFiles = null;

                            switch (logFile.Extension.ToLower())
                            {
                                case ".tmp":
                                    AppUtility.WriteLine("  Extracting files");

                                    logFile.CopyTo(targetLogName);
                                    logFiles = this.ExtractFiles(new FileInfo(targetLogName));

                                    AppUtility.Log2File("LogManager", "  Extracting files to: " + targetLogName);

                                    break;

                                case ".log":
                                    logFiles = new List<string>();

                                    logFile.CopyTo(targetLogName);
                                    logFiles.Add(targetLogName);

                                    AppUtility.Log2File("LogManager", "  .log file copied --> " + targetLogName);

                                    break;

                                default:
                                    continue;
                            }

                            foreach (String workingLogFile in logFiles)
                            {
                                System.Threading.Thread.Sleep(AppSettings.LogProcessingFileInterval());

                                switch (Path.GetFileName(workingLogFile.ToLower()))
                                {
                                    case "progressevent.log":
                                        {
                                            AppUtility.Log2File("LogManager", "  Start importing Progress Log");
                                            this.ImportProgressLog(workingLogFile);
                                            AppUtility.Log2File("LogManager", "  End importing Progress Log");
                                            break;
                                        }

                                    case "event.log":
                                        {
                                            AppUtility.Log2File("LogManager", "  Start importing Event Log");
                                            this.ImportEventLog(workingLogFile);
                                            AppUtility.Log2File("LogManager", "  End importing Event Log");
                                            break;
                                        }

                                    case "location.log":
                                        {
                                        AppUtility.Log2File("LogManager", "  Start importing Location Log");
                                        this.ImportLocationLog(workingLogFile);
                                        AppUtility.Log2File("LogManager", "  End importing Location Log");

                                        break;
                                        }
                                    default:
                                        break;
                                }

                                if(logFile.Exists)
                                    logFile.Delete();                                

                                AppUtility.Log2File("LogManager", "  Log File deleted -> " + logFile.FullName);
                            }
                        }
                        catch (Exception ex)
                        {
                            AppUtility.WriteError(ex);
                            AppUtility.Log2File("LogManager", "  ProcessLog() CRASHES 1 --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);

                            //if you are here, there is some problem with current tmp file. Simply move it to badLogFolder
                            ////////////if (logFile != null && logFile.Exists)
                            ////////////    MoveLogFileToBadLogFolder(logFile.FullName, true);

                            //if (File.Exists(logFile.FullName))
                            //    File.Delete(logFile.FullName);
                        }
                        finally
                        {
                            Directory.Delete(targetDirectory, true);
                            AppUtility.Log2File("LogManager", "  Target Directory Deleted --> " + targetDirectory);
                        }


                        GC.Collect();
                    }
                }

                if (logDirectory.Exists && logDirectory.GetFiles().Length == 0)
                {
                    logDirectory.Delete(true);
                    AppUtility.Log2File("LogManager", "  Log Directory Deleted --> " + logDirectory);
                }
                //else
                //{
                //  ProcessLogs(logDirectory);
                //}

            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("LogManager", " ProcessLog() CRASHES 2 --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
                 
            }
        }

        private void ImportProgressLog(String filePath)
        {
            AppUtility.WriteLine("  Importing ProgressLog: " + filePath);
            AppUtility.Log2File("LogManager", "  Importing ProgressLog: " + filePath);

            //using (connection)
            //{
            SqlCeConnection connection = SQLCompactManager.GetConnection();
            connection.Open();


            AppUtility.Log2File("LogManager", "   ImportProgressLog()  Looping through all the lines of files");
            foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
            {
                System.Threading.Thread.Sleep(AppSettings.LogProcessingLineInterval());

                try
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        DateTime timestamp = DateTime.MinValue;
                        int busNumber = 0;
                        String clientType = "";
                        Boolean isMaster = false;
                        Boolean isOnline = false;
                        String source = "";
                        Boolean isOverwritten = false;
                        long journeyNumber = 0;
                        String currentLine = "";
                        String from = "";
                        String destination = "";
                        long stopNumber = 0;
                        int zone = 0;
                        String latitude = "";
                        String longitude = "";
                        String logTrigger = "";
                        String data = "";
                        int result = 0;
                        String signData = "";

                        switch (values[0].ToLower())
                        {
                            case "ver1":
                                //Ignoring VER1
                                AppUtility.WriteLine("  Error reading line (Ignoring VER1): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Ignoring VER1): " + line);

                                continue;

                            case "ver2":
                                timestamp = DateTime.Parse(values[1], AppUtility.GetDateTimeFormatProvider());
                                int.TryParse(values[2], out busNumber);
                                clientType = values[3];
                                isMaster = (values[4] == "1");
                                isOnline = (values[5] == "1");
                                source = values[6];
                                isOverwritten = (values[7] == "1");
                                long.TryParse(values[8], out journeyNumber);
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                long.TryParse(values[12], out stopNumber);
                                int.TryParse(values[13], out zone);
                                latitude = values[14];
                                longitude = values[15];
                                logTrigger = values[16];
                                data = values[17];
                                int.TryParse(values[18], out result);

                                if (busNumber <= 0 || String.IsNullOrEmpty(clientType))
                                    continue;

                                break;

                            case "ver3":
                                timestamp = DateTime.Parse(values[1], AppUtility.GetDateTimeFormatProvider());
                                int.TryParse(values[2], out busNumber);
                                clientType = values[3];
                                isMaster = (values[4] == "1");
                                isOnline = (values[5] == "1");
                                source = values[6];
                                isOverwritten = (values[7] == "1");
                                long.TryParse(values[8], out journeyNumber);
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                long.TryParse(values[12], out stopNumber);
                                int.TryParse(values[13], out zone);
                                latitude = values[14];
                                longitude = values[15];
                                logTrigger = values[16];
                                data = values[17];
                                int.TryParse(values[18], out result);

                                if (values.Length > 19)
                                    signData = values[19];

                                if (busNumber <= 0 || String.IsNullOrEmpty(clientType)) //Error parsing line. No BusNumber or ClientType
                                    continue;

                                break;

                            default:
                                //Unknown version
                                AppUtility.WriteLine("  Error reading line (Unknown version): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Unknown version): " + line);

                                continue;
                        }

                        String script = "INSERT INTO [BusProgressLog](" +
                                            "[Timestamp], " +
                                            "[BusNumber], " +
                                            "[ClientType], " +
                                            "[IsMaster], " +
                                            "[IsOnline], " +
                                            "[Source], " +
                                            "[IsOverwritten], " +
                                            "[JourneyNumber], " +
                                            "[Line], " +
                                            "[From], " +
                                            "[Destination], " +
                                            "[StopNumber], " +
                                            "[Zone], " +
                                            "[Latitude], " +
                                            "[Longitude], " +
                                            "[LogTrigger], " +
                                            "[Data], " +
                                            "[Result], " +
                                            "[SignData]" +
                                        ")" + Environment.NewLine +
                                        "VALUES(" +
                                            "'" + timestamp.ToString("yyyy-MM-dd HH:mm:ss") + "', " +
                                            busNumber + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(clientType) + "', " +
                                            (isMaster ? 1 : 0) + ", " +
                                            (isOnline ? 1 : 0) + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(source) + "', " +
                                            (isOverwritten ? 1 : 0) + ", " +
                                            journeyNumber + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(currentLine) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(from) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(destination) + "', " +
                                            stopNumber + ", " +
                                            zone + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(latitude) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(longitude) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(logTrigger) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(data) + "', " +
                                            result + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(signData) + "'" +
                                         ")";

                        using (SqlCeCommand command = new SqlCeCommand(script, connection))
                        {
                            command.ExecuteNonQuery();
                            //AppUtility.Log2File("LogManager", "  Successfully saved Progress Log record in CE Database");  
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Error reading line
                    AppUtility.WriteError("  Error reading line: " + line);
                    AppUtility.Log2File("LogManager", " ImportProgressLog() -->  Error reading line: " + line + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + "File: " + filePath, true);

                    //move this log file to BadLogFiles location
                    //MoveLogFileToBadLogFolder(filePath, true);
                    //return;


                    MoveBadLinesToBadLogFolder(filePath,"ProgressLog",line);
                }
            }
            connection.Close();
            //}
        }

        private void ImportEventLog(String filePath)
        {
            AppUtility.WriteLine("  Importing EventLog: " + filePath);
            AppUtility.Log2File("LogManager", "  Importing EventLog: " + filePath);

            //using (connection)
            //{
            SqlCeConnection connection = SQLCompactManager.GetConnection();
            connection.Open();

            foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
            {
                System.Threading.Thread.Sleep(AppSettings.LogProcessingLineInterval());

                try
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        String busNumber = "";
                        String clientType = "";
                        DateTime timestamp = DateTime.MinValue;
                        String gps1 = "1.1,1.1";
                        String gps2 = "1.1,1.1";
                        String isMaster = "NULL";
                        String isOverwritten = "NULL";
                        String source = "";
                        String currentLine = "";
                        String from = "";
                        String destination = "";
                        String journeyNumber = "NULL";
                        long stopNumber = 0;
                        String zone = "NULL";
                        String eventSource = "";
                        String eventId = "";
                        String eventData = "";
                        int result = 0;
                        String extraData = "";

                        switch (values[0].ToLower())
                        {
                            case "ver1":
                                busNumber = values[1];
                                clientType = values[2];
                                timestamp = DateTime.Parse(values[3], AppUtility.GetDateTimeFormatProvider());
                                gps1 = AppUtility.ValidateGPS(values[4]);
                                gps2 = AppUtility.ValidateGPS(values[5]);
                                isMaster = String.IsNullOrEmpty(values[6]) ? "NULL" : values[6];
                                isOverwritten = String.IsNullOrEmpty(values[7]) ? "NULL" : values[7];
                                source = values[8];
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                journeyNumber = String.IsNullOrEmpty(values[12]) ? "NULL" : values[12];
                                long.TryParse(values[13], out stopNumber);
                                zone = String.IsNullOrEmpty(values[14]) ? "NULL" : values[14];
                                eventSource = values[16];
                                eventId = values[15];
                                eventData = values[17];
                                int.TryParse(values[18], out result);
                                extraData = values[19];

                                break;

                            default:
                                //Unknown version
                                AppUtility.WriteError("  Error reading line (Unknown version): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Unknown version): " + line);
                                continue;
                        }

                        if (String.IsNullOrEmpty(busNumber) || String.IsNullOrEmpty(clientType))
                            continue;

                        String script = "INSERT INTO [BusEventLog](" +
                                            "[Timestamp], " +
                                            "[BusNumber], " +
                                            "[ClientType], " +
                                            "[IsMaster], " +
                                            "[IsOverwritten], " +
                                            "[Source], " +
                                            "[Line], " +
                                            "[From], " +
                                            "[Destination], " +
                                            "[JourneyNumber], " +
                                            "[StopNumber], " +
                                            "[Zone], " +
                                            "[GPS1_Temp], " +
                                            "[GPS2_Temp], " +
                                            "[EventSource], " +
                                            "[EventId], " +
                                            "[EventData], " +
                                            "[Result], " +
                                            "[ExtraData]" +
                                        ")" + Environment.NewLine +
                                        "VALUES(" +
                                            "'" + timestamp.ToString("yyyy-MM-dd HH:mm:ss") + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(busNumber) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(clientType) + "', " +
                                            isMaster + ", " +
                                            isOverwritten + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(source) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(currentLine) + "', " +
                                            "N'" + AppUtility.SafeSqlLiteral(from) + "', " +
                                            "N'" + AppUtility.SafeSqlLiteral(destination) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(journeyNumber) + "', " +
                                            stopNumber + ", " +
                                            zone + ", " +
                            //AppUtility.ConvertToSqlPoint(gps1) + ", " +
                            //AppUtility.ConvertToSqlPoint(gps2) + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(gps1) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(gps2) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(eventSource) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(eventId) + "', " +
                                            "N'" + AppUtility.SafeSqlLiteral(eventData) + "', " +
                                            result + ", " +
                                            "N'" + AppUtility.SafeSqlLiteral(extraData) + "'" +
                                         ")";

                        using (SqlCeCommand command = new SqlCeCommand(script, connection))
                        {
                            command.ExecuteNonQuery();
                            //AppUtility.Log2File("LogManager", "Saved BusEventLog to CE Database");
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Error reading line
                    AppUtility.WriteError("  Error reading line: " + line);
                    AppUtility.Log2File("LogManager", " ImportEventLog() -->  Error reading line: " + line + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + "File: " + filePath, true);

                    //move this log file to BadLogFiles location
                    //MoveLogFileToBadLogFolder(filePath, true);
                    MoveBadLinesToBadLogFolder(filePath,"EventLog",line);

                }
                finally
                {

                }
            }
            connection.Close();
            //}
        }

        private void ImportLocationLog(String filePath)
        {
            AppUtility.WriteLine("  Importing LocationLog: " + filePath);
            AppUtility.Log2File("LogManager", "  Importing LocationLog" + filePath);
            //using (connection)
            //{
            SqlCeConnection connection = SQLCompactManager.GetConnection();
            connection.Open();

            foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
            {
                System.Threading.Thread.Sleep(AppSettings.LogProcessingLineInterval());

                try
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        //Fix: invalid versions deployed
                        if (values.Length != 9)
                            continue;

                        String busNumber = "";
                        String clientType = "";
                        DateTime timestamp = DateTime.MinValue;
                        String gps1 = "1.1,1.1";
                        String gps2 = "1.1,1.1";
                        String speed = "";
                        int playerPid = 0;
                        String systemStatus = "";

                        switch (values[0].ToLower())
                        {
                            case "ver1":
                                busNumber = values[1];
                                clientType = values[2];
                                timestamp = DateTime.Parse(values[3], AppUtility.GetDateTimeFormatProvider());
                                gps1 = AppUtility.ValidateGPS(values[4]);
                                gps2 = AppUtility.ValidateGPS(values[5]);
                                speed = values[6];
                                int.TryParse(values[7], out playerPid);
                                systemStatus = values[8];

                                //Console.WriteLine("BusNumber: " + busNumber + " - GPS 1:" + gps1 + " - GPS 2:" + gps2 + " - Timestamp: " + timestamp + " - Speed:" + speed + " - Status: " + systemStatus);

                                break;

                            default:
                                //Unknown version
                                AppUtility.WriteError("  Error reading line (Unknown version): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Unknown version): " + line);
                                continue;
                        }

                        if (String.IsNullOrEmpty(busNumber) || String.IsNullOrEmpty(clientType))
                            continue;


                        String script = "INSERT INTO [BusLocationLog](" +
                                            "[Timestamp], " +
                                            "[BusNumber], " +
                                            "[ClientType], " +
                                            "[GPS1_Temp], " +
                                            "[GPS2_Temp], " +
                                            "[Speed], " +
                                            "[Player_PID], " +
                                            "[SystemStatus]" +
                                        ")" + Environment.NewLine +
                                        "VALUES(" +
                                            "'" + timestamp.ToString("yyyy-MM-dd HH:mm:ss") + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(busNumber) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(clientType) + "', " +
                            //AppUtility.ConvertToSqlPoint(gps1) + ", " +
                            //AppUtility.ConvertToSqlPoint(gps2) + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(gps1) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(gps2) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(speed) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(playerPid.ToString()) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(systemStatus) + "'" +
                                         ")";

                        using (SqlCeCommand command = new SqlCeCommand(script, connection))
                        {
                            command.ExecuteNonQuery();
                            //AppUtility.Log2File("LogManager", "  ImportLocationLog() --> Saved in CE Database");
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Error reading line
                    AppUtility.WriteError("  Error reading line: " + line);
                    AppUtility.Log2File("LogManager", " ImportLocationLog() -->  Error reading line: " + line + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + "File: " + filePath, true);

                    //move this log file to BadLogFiles location
                    //MoveLogFileToBadLogFolder(filePath, true);
                    //return;

                    MoveBadLinesToBadLogFolder(filePath,"Location",line);
                }
                finally
                {

                }
            }
            connection.Close();
            //}
        }

       
        private void MoveLogFileToBadLogFolder(string filePath, bool delete = false)
        {

            try
            {

                if (File.Exists(filePath))
                {
                    DateTime fileTime = File.GetLastWriteTime(filePath);

                    string badFileName = fileTime.ToString("yyyyMMdd_HHmmssfff") + "_" + Path.GetFileName(filePath);


                    string badFolder = Path.Combine(ConfigurationManager.AppSettings["BadLogDirectory"], fileTime.ToString("yyyyMMdd"));

                    if (!Directory.Exists(badFolder))
                    {
                        DirectoryInfo dirInfo = Directory.CreateDirectory(badFolder);
                    }

                    File.Copy(filePath, Path.Combine(badFolder, badFileName));

                    if (delete)
                        File.Delete(filePath);

                    
                    AppUtility.Log2File("LogManager", "--> Problematic file moved to BadLogArchive folder: " + Path.Combine(badFolder, badFileName));
                    AppUtility.WriteError("LogManager --> Problematic file moved to BadLogArchive folder: " + Path.Combine(badFolder, badFileName));
                }
            }
            catch(Exception ex)
            {
                //do nothing
                AppUtility.Log2File("LogManager", " Error while moving a log file (" + filePath + ") to BadLogFolder. " + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }

        }



        private void MoveBadLinesToBadLogFolder(string filePath, string fileType,string badLine)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    DateTime fileTime = File.GetLastWriteTime(filePath);

                    AppUtility.Log2File("LogManager", "--> File.GetLastWriteTime: " + fileTime.ToString());

                    string badFolder = Path.Combine(ConfigurationManager.AppSettings["BadLogDirectory"], fileTime.ToString("yyyyMMdd"));
                    string badFileName = fileTime.ToString("yyyyMMdd") + "_" + fileType + "_BadLines.log";

                    if (!Directory.Exists(badFolder)) Directory.CreateDirectory(badFolder);

                    badFileName = Path.Combine(badFolder, badFileName);
                    badLine += Environment.NewLine + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + Environment.NewLine;
                    File.AppendAllText(badFileName, badLine);

                    AppUtility.Log2File("LogManager", "--> Problematic Line moved to BadLog File: " + Path.Combine(badFolder, badFileName));
                    AppUtility.WriteError("LogManager --> Problematic Line moved to BadLog File: " + Path.Combine(badFolder, badFileName));
                }
            }
            catch (Exception ex)
            {
                //do nothing
                AppUtility.Log2File("LogManager", " Error while moving a Bad Lines (" + badLine + ") to BadLogFile. " + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
        }

        private void tmrBulkCopyLog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Console.WriteLine("Check if Bulk Copy is enabled");
            AppUtility.Log2File("BulkCopy", "Check if Bulk Copy is enabled", true);
            if (!AppSettings.LogBulkCopyingEnabled())
            {
                //Console.WriteLine("BulkCopy is not enabled");
                AppUtility.Log2File("BulkCopy", "BulkCopy is not enabled");
                return; //stop bulk copying (skipped)
            }
            //Console.WriteLine("BulkCopy is enabled");
            AppUtility.Log2File("BulkCopy", "BulkCopy is enabled");

            this.tmrBulkCopyLog.Enabled = false;
            AppUtility.Log2File("BulkCopy", "BulkCopy Timer Stops + BulkCopying Starts");


            SqlCeConnection con;
            try
            {
                //BULK COPY OPERATION HERE

                AppUtility.Log2File("BulkCopy", "BulkCopy Looping All SQLCE Files in Data Directory");
                foreach (string fileName in Directory.GetFiles(AppSettings.GetSQLCEDirectory()))
                {
                    System.Threading.Thread.Sleep(AppSettings.BulkCopyFileInterval());

                    FileInfo fl = new FileInfo(fileName);
                    if (fl.Extension.ToLower() != ".sdf")
                        continue;

                    if (((fl.Length) / 1024 / 1024) < int.Parse(SQLCompactManager.MaxCompactDBSize))
                        continue;

                    AppUtility.Log2File("BulkCopy", "BulkCopy --> " + fl.FullName);

                    con = SQLCompactManager.GetConnection(fl.FullName);

                    con.Open();
                    AppUtility.Log2File("BulkCopy", "  DB Connection Opens");

                    //SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM BusEventLog; SELECT * FROM BusProgressLog; SELECT * FROM BusLocationLog;", con);
                    SqlCeCommand cmdEvnt = new SqlCeCommand(@"SELECT [Timestamp]
                                                                          ,[BusNumber]
                                                                          ,[ClientType]
                                                                          ,[IsMaster]
                                                                          ,[IsOverwritten]
                                                                          ,[Source]
                                                                          ,[Line]
                                                                          ,[From]
                                                                          ,[Destination]
                                                                          ,[JourneyNumber]
                                                                          ,[StopNumber]
                                                                          ,[Zone]
                                                                          ,[GPS1_Temp]
                                                                          ,[GPS2_Temp]
                                                                          ,[EventSource]
                                                                          ,[EventId]
                                                                          ,[EventData]
                                                                          ,[Result]
                                                                          ,[ExtraData]
                                                                      FROM BusEventLog ", con);
                    SqlCeCommand cmdProg = new SqlCeCommand(@"SELECT [Timestamp]
                                                                        ,[BusNumber]
                                                                        ,[ClientType]
                                                                        ,[IsMaster]
                                                                        ,[IsOnline]
                                                                        ,[Source]
                                                                        ,[IsOverwritten]
                                                                        ,[JourneyNumber]
                                                                        ,[Line]
                                                                        ,[From]
                                                                        ,[Destination]
                                                                        ,[StopNumber]
                                                                        ,[Zone]
                                                                        ,[Latitude]
                                                                        ,[Longitude]
                                                                        ,[LogTrigger]
                                                                        ,[Data]
                                                                        ,[Result]
                                                                        ,[SignData]
                                                                    FROM [BusProgressLog]", con);
                    SqlCeCommand cmdLoc = new SqlCeCommand(@"SELECT [Timestamp]
                                                                          ,[BusNumber]
                                                                          ,[ClientType]
                                                                          ,[GPS1_Temp]
                                                                          ,[GPS2_Temp]
                                                                          ,[Speed]
                                                                          ,[Player_PID]
                                                                          ,[SystemStatus]
                                                                      FROM  [BusLocationLog]", con);

                    DataSet ds = new DataSet();
                    SqlCeDataAdapter da = new SqlCeDataAdapter();

                    da.SelectCommand = cmdEvnt;
                    da.Fill(ds, "BusEventLog");
                     

                    da.SelectCommand = cmdProg;
                    da.Fill(ds, "BusProgressLog");
                     

                    da.SelectCommand = cmdLoc;
                    da.Fill(ds, "BusLocationLog");
                     

                    con.Close();
                    AppUtility.Log2File("BulkCopy", "  DB Connection Closes");

                    AppUtility.Log2File("BulkCopy", "BulkCopy Started");

                    bool bulkCopySuccess = BulkCopy(ds, ConfigurationManager.ConnectionStrings["IBILogsDatabaseConnection"].ToString());
                    
                    ds.Clear();
                    ds.Dispose();

                    bulkCopySuccess = BulkCopyFromImport();

                    AppUtility.Log2File("BulkCopy", "BulkCopy Completed -- > Status: " + bulkCopySuccess);

                    if (bulkCopySuccess)
                    {
                        //empty Import Tables
                        using (SqlConnection conLogs = new SqlConnection(ConfigurationManager.ConnectionStrings["IBILogsDatabaseConnection"].ToString()))
                        {
                            using (SqlCommand cmdClear = new SqlCommand())
                            {
                                conLogs.Open();

                                cmdClear.Connection = conLogs;
                                cmdClear.CommandText = "ClearImportTables";
                                cmdClear.CommandType = CommandType.StoredProcedure;
                                cmdClear.ExecuteNonQuery();

                                conLogs.Close();
                            }
                        }

                        SQLCompactManager.RemoveDatabase(fl.FullName);
                        AppUtility.Log2File("BulkCopy", "BulkCopy SQLCE File removed -- > " + fl.FullName);
                    }

                }

            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("BulkCopy", ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                this.tmrBulkCopyLog.Interval = (DateTime.Now.AddMinutes(2) - DateTime.Now).TotalMilliseconds; // AppUtility.DifferenceToMidnight().TotalMilliseconds;
                this.tmrBulkCopyLog.Enabled = true;

                AppUtility.Log2File("BulkCopy", "BulkCopy Timer started again. Next tick is:" + DateTime.Now.AddMinutes(2));

                con = null;
            }
        }

        public void Dispose()
        {
            this.tmrBulkCopyLog.Enabled = false;
            this.tmrBulkCopyLog.Dispose();

            this.TmrProcessLogs.Enabled = false;
            this.TmrProcessLogs.Dispose();
        }
        #region Private helpers

        private List<String> ExtractFiles(FileInfo file)
        {
            String targetPath = file.Directory.FullName;

            List<String> fileList = new List<String>();

            FastZip zipEngine = new FastZip();
            zipEngine.ExtractZip(file.FullName, targetPath, "");

            AppUtility.Log2File("LogManager", "  Extracting files");
            foreach (String logFilePath in Directory.GetFiles(targetPath))
            {
                if (String.Compare(Path.GetFileName(file.FullName), Path.GetFileName(logFilePath), true) != 0)
                {
                    fileList.Add(logFilePath);
                    AppUtility.Log2File("LogManager", "    Extracte file: " + logFilePath);
                }

            }

            return fileList;
        }

        // from import tables to --> Final Log Tables
        public static bool BulkCopyFromImport()
        {
            AppUtility.Log2File("BulkCopy", " BulkCopying( from Import Table to Final Tables ) Starts: ", true);
            try
            {
                using(IBI.DataAccess.DBModels.IBILogsModel dbContext = new DataAccess.DBModels.IBILogsModel())
                {
                    dbContext.Database.CommandTimeout = 500;
                    dbContext.BulkCopyFromImportTable();    
                }
                 
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("BulkCopy", " BulkCopying( from Import Table to Final Tables ) CRASHES: --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
                return false;
            }

            return true;

        }

        // form SQL CE to IBI_Logs --> Import Table
        public static bool BulkCopy(DataSet dataSet, string destConnectionString)
        {

            try
            {

                using (SqlConnection destConn = new SqlConnection(destConnectionString))
                {

                    destConn.Open();
                    AppUtility.Log2File("BulkCopy", "Bulk Copy SDF to Import Table --> Open IBI Database Coonection");

                    SqlCommand cmd = new SqlCommand("ClearImportTables", destConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConn, SqlBulkCopyOptions.FireTriggers, null))
                    {
                        bulkCopy.BulkCopyTimeout = AppSettings.SQLBulkCopyTimeout();

                        bulkCopy.ColumnMappings.Clear();
                        //BUS EVENT LOG COPYING
                        bulkCopy.DestinationTableName = "BusEventLog_Import";

                        bulkCopy.ColumnMappings.Add("Timestamp", "Timestamp");
                        bulkCopy.ColumnMappings.Add("BusNumber", "BusNumber");
                        bulkCopy.ColumnMappings.Add("ClientType", "ClientType");
                        bulkCopy.ColumnMappings.Add("IsMaster", "IsMaster");
                        bulkCopy.ColumnMappings.Add("IsOverwritten", "IsOverwritten");
                        bulkCopy.ColumnMappings.Add("Source", "Source");
                        bulkCopy.ColumnMappings.Add("Line", "Line");
                        bulkCopy.ColumnMappings.Add("From", "From");
                        bulkCopy.ColumnMappings.Add("Destination", "Destination");
                        bulkCopy.ColumnMappings.Add("JourneyNumber", "JourneyNumber");
                        bulkCopy.ColumnMappings.Add("StopNumber", "StopNumber");
                        bulkCopy.ColumnMappings.Add("Zone", "Zone");
                        bulkCopy.ColumnMappings.Add("GPS1_Temp", "GPS1_Temp");
                        bulkCopy.ColumnMappings.Add("GPS2_Temp", "GPS2_Temp");
                        bulkCopy.ColumnMappings.Add("EventSource", "EventSource");
                        bulkCopy.ColumnMappings.Add("EventId", "EventId");
                        bulkCopy.ColumnMappings.Add("EventData", "EventData");
                        bulkCopy.ColumnMappings.Add("Result", "Result");
                        //bulkCopy.ColumnMappings.Add("ExtraData ", "ExtraData");

                        AppUtility.Log2File("BulkCopy", " --> Start writing BusEventLog. " + dataSet.Tables["BusEventLog"].Rows.Count + " records");
                        bulkCopy.WriteToServer(dataSet.Tables["BusEventLog"], DataRowState.Unchanged);
                        AppUtility.Log2File("BulkCopy", " --> End writing BusEventLog");


                        bulkCopy.ColumnMappings.Clear();
                        //BusLocationLog COPYING
                        bulkCopy.DestinationTableName = "dbo.BusLocationLog_Import";

                        bulkCopy.ColumnMappings.Add("Timestamp", "Timestamp");
                        bulkCopy.ColumnMappings.Add("BusNumber", "BusNumber");
                        bulkCopy.ColumnMappings.Add("ClientType", "ClientType");
                        bulkCopy.ColumnMappings.Add("GPS1_Temp", "GPS1_Temp");
                        bulkCopy.ColumnMappings.Add("GPS2_Temp", "GPS2_Temp");
                        bulkCopy.ColumnMappings.Add("Speed", "Speed");
                        bulkCopy.ColumnMappings.Add("Player_PID", "Player_PID");
                        bulkCopy.ColumnMappings.Add("SystemStatus", "SystemStatus");

                        AppUtility.Log2File("BulkCopy", " --> Start writing BusLocationLog. " + dataSet.Tables["BusLocationLog"].Rows.Count + " records");
                        bulkCopy.WriteToServer(dataSet.Tables["BusLocationLog"], DataRowState.Unchanged);
                        AppUtility.Log2File("BulkCopy", " --> End writing BusLocationLog");

                        bulkCopy.ColumnMappings.Clear();
                        //BusProgressLog COPYING
                        bulkCopy.DestinationTableName = "dbo.BusProgressLog_Import";

                        bulkCopy.ColumnMappings.Add("Timestamp", "Timestamp");
                        bulkCopy.ColumnMappings.Add("BusNumber", "BusNumber");
                        bulkCopy.ColumnMappings.Add("ClientType", "ClientType");
                        bulkCopy.ColumnMappings.Add("IsMaster", "IsMaster");
                        bulkCopy.ColumnMappings.Add("IsOnline", "IsOnline");
                        bulkCopy.ColumnMappings.Add("Source", "Source");
                        bulkCopy.ColumnMappings.Add("IsOverwritten", "IsOverwritten");
                        bulkCopy.ColumnMappings.Add("JourneyNumber", "JourneyNumber");
                        bulkCopy.ColumnMappings.Add("Line", "Line");
                        bulkCopy.ColumnMappings.Add("From", "From");
                        bulkCopy.ColumnMappings.Add("Destination", "Destination");
                        bulkCopy.ColumnMappings.Add("StopNumber", "StopNumber");
                        bulkCopy.ColumnMappings.Add("Zone", "Zone");
                        bulkCopy.ColumnMappings.Add("Latitude", "Latitude");
                        bulkCopy.ColumnMappings.Add("Longitude", "Longitude");
                        bulkCopy.ColumnMappings.Add("LogTrigger", "LogTrigger");
                        bulkCopy.ColumnMappings.Add("Data", "Data");
                        bulkCopy.ColumnMappings.Add("Result", "Result");
                        bulkCopy.ColumnMappings.Add("SignData", "SignData");


                        AppUtility.Log2File("BulkCopy", " --> Start writing BusProgressLog. " + dataSet.Tables["BusProgressLog"].Rows.Count + " records");
                        bulkCopy.WriteToServer(dataSet.Tables["BusProgressLog"], DataRowState.Unchanged);
                        AppUtility.Log2File("BulkCopy", " --> End writing BusProgressLog");

                    }
                    //    if (append)
                    //        AppendBulkLoaded(destConn, TIMEOUT); // 45 minutes


                }
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("BulkCopy", " BulkCopying CRASHES: --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
                return false;
            }

            return true;
        }


        private void UpdateGPS()
        {
            try
            {
            
                using (SqlConnection conLogs = new SqlConnection(ConfigurationManager.ConnectionStrings["IBILogsDatabaseConnection"].ToString()))
                {
                    using (SqlCommand cmdLogs = new SqlCommand())
                    {

                        cmdLogs.Connection = conLogs;
                        cmdLogs.CommandText = "UpdateGPS";
                        cmdLogs.CommandType = CommandType.StoredProcedure;                    
                    
                            try
                            {
                                conLogs.Open();
                                cmdLogs.ExecuteNonQuery();
                                conLogs.Close();
                            }
                            catch(Exception err)
                            {
                                Console.WriteLine("Error Executing:  UpdateGPS() --> " + err.Message);
                                AppUtility.Log2File("BulkCopy", "Error Executing: UpdateGPS() --> " + err.Message + " --- " + err.StackTrace);                                
                            }
                            conLogs.Close();

                        }


                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error Executing:  UpdateGPS() --> " + ex.Message);
                AppUtility.WriteError("Error Executing: UpdateGPS() --> " + ex.Message + " --- " + ex.StackTrace);
                AppUtility.Log2File("BulkCopy", "Error Executing: UpdateGPS() --> " + ex.Message + " --- " + ex.StackTrace);
               
            }

            
        }


        static void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
           
            //throw new NotImplementedException();
            //e.RowsCopied
        }


        #endregion
    }
}
