﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IBI.LogProcessor
{
    public partial class LogProcessingService : ServiceBase
    {
        private LogManager serverInstance;

        public LogProcessingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.serverInstance = new LogProcessor.LogManager();
        }

        protected override void OnStop()
        {
        }
    }
}
