﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Collections;
using System.Globalization;

namespace IBI.JourneyImporter.UI
{
    public partial class Form1 : Form
    {
        #region Variables
        Hashtable hexaToBinary;

        List<Journey> Journeys { get; set; }
        List<KeyValuePair<string, string>> StopNamesCache { get; set; } //value: stopname
        List<KeyValuePair<string, string>> StopCoordCache { get; set; } //value: lat|lon

        List<String> Lines { get; set; }
        
         string[] FPlanData {get; set;}
         string[] StopNamesData  {get; set;}
         string[] StopCoordData  {get; set;}
         string[] JourneyDurationData {get; set;}
         string[] OperationDaysData { get; set; }


         int TotalCheckBoxes = 0;
         int TotalCheckedCheckBoxes = 0;
         CheckBox HeaderCheckBox = null;
         bool IsHeaderCheckBoxClicked = false;


        #endregion



        public Form1()
        {
            InitializeComponent();

            Journeys = new List<Journey>();
            StopNamesCache = new List<KeyValuePair<string, string>>();
            StopCoordCache = new List<KeyValuePair<string, string>>();

            Lines= new List<String>();

            
        }


        #region Grid Functionality


        private void gvJourney_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!IsHeaderCheckBoxClicked)
                RowCheckBoxClick((DataGridViewCheckBoxCell)gvJourney[e.ColumnIndex, e.RowIndex]);
        }

        private void gvJourney_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (gvJourney.CurrentCell is DataGridViewCheckBoxCell)
                gvJourney.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void HeaderCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            HeaderCheckBoxClick((CheckBox)sender);
        }

        private void HeaderCheckBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
                HeaderCheckBoxClick((CheckBox)sender);
        }

        private void gvJourney_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex == 0)
                ResetHeaderCheckBoxLocation(e.ColumnIndex, e.RowIndex);
        }

        private void AddHeaderCheckBox()
        {
            HeaderCheckBox = new CheckBox();

            HeaderCheckBox.Size = new Size(15, 15);

            //Add the CheckBox into the DataGridView
            this.gvJourney.Controls.Add(HeaderCheckBox);
        }

        private void ResetHeaderCheckBoxLocation(int ColumnIndex, int RowIndex)
        {
            //Get the column header cell bounds
            Rectangle oRectangle = this.gvJourney.GetCellDisplayRectangle(ColumnIndex, RowIndex, true);

            Point oPoint = new Point();

            oPoint.X = oRectangle.Location.X + (oRectangle.Width - HeaderCheckBox.Width) / 2 + 1;
            oPoint.Y = oRectangle.Location.Y + (oRectangle.Height - HeaderCheckBox.Height) / 2 + 1;

            //Change the location of the CheckBox to make it stay on the header
            HeaderCheckBox.Location = oPoint;
        }

        private void HeaderCheckBoxClick(CheckBox HCheckBox)
        {
            IsHeaderCheckBoxClicked = true;

            foreach (DataGridViewRow Row in gvJourney.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["chkBxSelect"]).Value = HCheckBox.Checked;

            gvJourney.RefreshEdit();

            TotalCheckedCheckBoxes = HCheckBox.Checked ? TotalCheckBoxes : 0;

            IsHeaderCheckBoxClicked = false;
        }

        private void RowCheckBoxClick(DataGridViewCheckBoxCell RCheckBox)
        {
            if (RCheckBox != null)
            {
                //Modifiy Counter;            
                if ((bool)RCheckBox.Value && TotalCheckedCheckBoxes < TotalCheckBoxes)
                    TotalCheckedCheckBoxes++;
                else if (TotalCheckedCheckBoxes > 0)
                    TotalCheckedCheckBoxes--;

                //Change state of the header CheckBox.
                if (TotalCheckedCheckBoxes < TotalCheckBoxes)
                    HeaderCheckBox.Checked = false;
                else if (TotalCheckedCheckBoxes == TotalCheckBoxes)
                    HeaderCheckBox.Checked = true;
            }
        }

        #endregion

        private void btnBrowseFPlan_Click(object sender, EventArgs e)
        {
            Journeys.Clear();
            gvJourney.DataSource = null;


            if(String.IsNullOrEmpty(CustomerID.Text))
            {
                MessageBox.Show("Please provide a Customer ID for this data import.");
                return;
            }


            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fplanfilepath.Text = openFileDialog1.FileName;

                string folderPath = fplanfilepath.Text.Substring(0, fplanfilepath.Text.LastIndexOf("\\"));

               if(Directory.Exists(folderPath))
               {
                   stopcoordfilepath.Text = System.IO.Path.Combine(folderPath, "bfkoord.hsb");
                   stopnamesfilepath.Text = System.IO.Path.Combine(folderPath, "bahnhof");
                   operationdaysfilepath.Text = System.IO.Path.Combine(folderPath, "bitfeld");
                   journeydurationfilepath.Text = System.IO.Path.Combine(folderPath, "eckdaten");
               }

               ProcessFPlanFile();
            }
        }

        #region Button Events


        private void btnBrowseStopNames_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                stopnamesfilepath.Text = openFileDialog1.FileName;
                ProcessFPlanFile();
            }
        }

        private void btnBrowseStopCoord_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                stopcoordfilepath.Text = openFileDialog1.FileName;
                ProcessFPlanFile();
            }
        }

        private void btnBrowseDaysFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
              operationdaysfilepath.Text = openFileDialog1.FileName;
              ProcessFPlanFile();
            }
        }

        private void btnBrowseDuration_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                journeydurationfilepath.Text = openFileDialog1.FileName;
                ProcessFPlanFile();
            }
        }
         

        private void btnImport_Click(object sender, EventArgs e)
        {
            ImportJourneys();            
        }

        #endregion

        #region Hexa2Binary Functions

        public string HexToBinary(string hexValue)
        {
            
            ulong number = UInt64.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            return Convert.ToString((int)number, 2);
        }

        private void initializeHexToBinary()
        {
            hexaToBinary = new Hashtable();
            hexaToBinary["0"] = HexToBinary("0");
            hexaToBinary["1"] = HexToBinary("1");
            hexaToBinary["2"] = HexToBinary("2");
            hexaToBinary["3"] = HexToBinary("3");
            hexaToBinary["4"] = HexToBinary("4");
            hexaToBinary["5"] = HexToBinary("5");
            hexaToBinary["6"] = HexToBinary("6");
            hexaToBinary["7"] = HexToBinary("7");
            hexaToBinary["8"] = HexToBinary("8");
            hexaToBinary["9"] = HexToBinary("9");
            hexaToBinary["A"] = HexToBinary("A");
            hexaToBinary["B"] = HexToBinary("B");
            hexaToBinary["C"] = HexToBinary("C");
            hexaToBinary["D"] = HexToBinary("D");
            hexaToBinary["E"] = HexToBinary("E");
            hexaToBinary["F"] = HexToBinary("F");
        }


        #endregion


        private void ProcessFPlanFile()
        {            
            if (String.IsNullOrEmpty(fplanfilepath.Text) || !File.Exists(fplanfilepath.Text))
            {
                MessageBox.Show("Please provide future plan path");
                return;
            }
            if (String.IsNullOrEmpty(stopnamesfilepath.Text) || !File.Exists(stopnamesfilepath.Text))
            {
                MessageBox.Show("Please provide stop name file path");
                return;
            }
            if (String.IsNullOrEmpty(stopcoordfilepath.Text) || !File.Exists(stopcoordfilepath.Text))
            {
                MessageBox.Show("Please provide stop coord file path");
                return;
            }
            if (String.IsNullOrEmpty(operationdaysfilepath.Text) || !File.Exists(operationdaysfilepath.Text))
            {
                MessageBox.Show("Please provide operationdays file path");
                return;
            }
            if (String.IsNullOrEmpty(journeydurationfilepath.Text) || !File.Exists(journeydurationfilepath.Text))
            {
                MessageBox.Show("Please provide journey duration file path");
                return;
            }

            btnImport.Enabled = false;

            //load all schedules of current customer into cache
            Util.FillSchedulesOfCurrentCustomer(CustomerID.Text);


            FPlanData = File.ReadAllLines(fplanfilepath.Text, Encoding.Default);

            List<String> lstplan = FPlanData.ToList();
            lstplan.Add("*Z");
            FPlanData = lstplan.ToArray();

            StopNamesData = File.ReadAllLines(stopnamesfilepath.Text, Encoding.Default);
            StopCoordData = File.ReadAllLines(stopcoordfilepath.Text, Encoding.Default);
            JourneyDurationData = File.ReadAllLines(journeydurationfilepath.Text, Encoding.Default);
            OperationDaysData = File.ReadAllLines(operationdaysfilepath.Text, Encoding.Default);
            
            
            CultureInfo provider = CultureInfo.InvariantCulture;
            
            initializeHexToBinary();


            bool journeyEnds = false;
            string operationDaysReference = string.Empty;
            DateTime journeyDurationStartTime = DateTime.ParseExact(JourneyDurationData[2].Split(' ')[0], "dd.MM.yyyy", provider);
            DateTime journeyDurationEndTime = DateTime.ParseExact(JourneyDurationData[3].Split(' ')[0], "dd.MM.yyyy", provider);

            string journeyStartTime = string.Empty;
            string journeyEndTime = string.Empty;

            ArrayList journeyOptions = new ArrayList();
            ArrayList journeyStops = new ArrayList();

            Lines.Clear();
            Line.Text = "";

            string jLine = "";
            foreach (string line in FPlanData)
            {
                string lineCode = line.Length>0 ? line.Substring(0, 2) : "";

                if (lineCode == "")
                    continue;

                switch (lineCode)
                {
                    case "*G":
                        //TODO
                    case "*R":
                        //TODO
                        break;
                    case "*F":
                        //TODO - check if it is valid line
                        break;
                    case "*I":
                        //TODO
                        break;
                    case "*C":
                        //TODO
                        break;
                    case "*A":
                        journeyOptions.Add(line);
                        break;
                    case "*L":
                        string[] lineJourneyElements = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        jLine = lineJourneyElements[1];
                        
                        if(String.IsNullOrEmpty(Lines.Find(l=>l==jLine)))
                            Lines.Add(jLine);
                         
                        break;
                    case "*Z":
                        if (journeyEnds)
                        {
                            // journey ends
                            foreach (string optionLine in journeyOptions)
                            {
                                string[] lineElements = optionLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                operationDaysReference = lineElements[4];
                                bool bAlldays = operationDaysReference.Equals("000000");
                                string bitValues = string.Empty; //365 char string to hold indexes of days
                                if (!bAlldays)
                                {
                                    var opDaysLine = Array.Find(OperationDaysData, s => s.StartsWith(operationDaysReference));
                                    if (opDaysLine != null)
                                    {
                                        // 3E7CF8000000000000300000000000000000000000000000000000000000000000000000000000000000000000000
                                        string[] opDataColumns = opDaysLine.ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                        string daysHexValue = opDataColumns[1].Substring(4);
                                        
                                        //DateTime journeyStartDateTime = DateTime.Parse(journeyStartTime, "dd.mm.yyyy");
                                        for (int i = 0; i < daysHexValue.Length; i++)
                                        {
                                            bitValues += hexaToBinary[daysHexValue.Substring(i, 1)];
                                            if (bitValues.Length >= (journeyDurationEndTime - journeyDurationStartTime).TotalDays)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }

                                journeyStartTime = lineElements[5].Substring(1);
                                journeyEndTime = lineElements[6].Substring(1);
                                int jDays2AddStart = 0;
                                int jDays2AddEnd = 0;

                                journeyStartTime = Adjust24HourTime(journeyStartTime, ref jDays2AddStart);
                                journeyEndTime = Adjust24HourTime(journeyEndTime, ref jDays2AddEnd);

                                // enter journey into DB/List/Grid
                                int dateIndex = 0;
                                DateTime journeyDate = journeyDurationStartTime;
                                for (DateTime dtStart = journeyDurationStartTime; dateIndex <  (journeyDurationEndTime - journeyDurationStartTime).TotalDays; dateIndex++)
                                {
                                    if (bAlldays || bitValues[dateIndex] == '1')
                                    {


                                        Journey journey = new Journey();

                                        journey.JourneyDate = journeyDurationStartTime.AddDays(dateIndex);

                                        string jStartPoint = GetStopName(lineElements[2]); //StopNamesData.First(s => s.Split(' ')[0] == lineElements[2]).Split(' ')[2]; 
                                        string jDestination = GetStopName(lineElements[3]); //StopNamesData.First(s => s.Split(' ')[0] == lineElements[3]).Split(' ')[2];



                                        DateTime jPlannedDeparture = String.IsNullOrEmpty(journeyStartTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + journeyStartTime, "MM/dd/yyyy HHmm", CultureInfo.InvariantCulture);
                                        DateTime jPlannedArrival = String.IsNullOrEmpty(journeyEndTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + journeyEndTime, "MM/dd/yyyy HHmm", CultureInfo.InvariantCulture);

                                        if (jPlannedDeparture != DateTime.MinValue)
                                        {
                                            jPlannedDeparture = jPlannedDeparture.AddDays(jDays2AddStart);
                                        }

                                        if (jPlannedArrival != DateTime.MinValue)
                                        {
                                            jPlannedArrival = jPlannedArrival.AddDays(jDays2AddEnd);
                                        }
                                         
                                        //DateTime jPlannedDeparture = DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " +  journeyStartTime, "MM/dd/yyyy HHmm", provider);
                                        //DateTime jPlannedArrival = DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + journeyEndTime, "MM/dd/yyyy HHmm", provider);
                                         


                                        journey.BusNumber = "0";
                                        journey.CustomerId = int.Parse(CustomerID.Text);
                                        journey.JourneyNumber = 0;
                                        journey.StartTime = null;
                                        journey.EndTime = null;

                                        journey.StartPoint = jStartPoint;
                                        journey.Line = jLine;
                                        journey.Destination = jDestination;
                                        journey.PlannedDepartureTime = jPlannedDeparture;
                                        journey.PlannedArrivalTime = jPlannedArrival;
                                        journey.ScheduleNumber = Util.GetScheduleNumber(journey, false);
                                       
                                        //Add Stops to journey
                                        journey.JourneyStops.AddRange(journeyStops);
                                        
                                        //create JourneyData XML
                                        journey.JourneyData = CreateJourneyData(journey);
                                        

                                        this.Journeys.Add(journey);
                                        // Add values/row for this journeyDate
                                    }
                                    dtStart = dtStart.AddDays(1);
                                    
                                }
                            } 
                            //TODO
                            // initialize journey variables
                            journeyStartTime = string.Empty;
                            journeyEndTime = string.Empty;
                            journeyOptions.Clear();
                            journeyStops.Clear();
                        }
                        else
                        {
                            journeyEnds = true;
                        }
                        break;
                    default:
                        {
                            if(line.StartsWith("*"))
                                continue;


                            // hop line
                            string stopNumber = line.Substring(0, 9); // stop length can be less than 9 - check it based on setting(PDF)
                            stopNumber = stopNumber;
                            string stopName = GetStopName(stopNumber); //StopNamesData.First(s => s.Split(' ')[0] == stopNumber).Split(' ')[2];
                            string arrivalTime = line.Substring(32, 5).Substring(1).Trim();
                            string departureTime = line.Substring(39, 5).Substring(1).Trim();
                            int day2addArrival = 0;
                            int day2addDeparture = 0;

                            arrivalTime = Adjust24HourTime(arrivalTime, ref day2addArrival);
                            departureTime = Adjust24HourTime(departureTime, ref day2addDeparture);

                            string coord = GetStopCoord(stopNumber);
                            string[] arrCoord = coord.Split('|');
                            string lon = arrCoord.Length > 0 ? arrCoord[0] : "";  //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
                            string lat = arrCoord.Length > 1 ? arrCoord[1] : "";//StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];
                            string zone = Util.GetZoneName(lat, lon, false);
                            string isCheckpoint = "false"; //must calculate
                            journeyStops.Add(new StopHop(stopNumber, stopName, arrivalTime, departureTime, lat, lon, zone, isCheckpoint, day2addArrival, day2addDeparture));
                                                                                   
                            break;
                        }
                }//end swtich
            }//end foreach

            AddHeaderCheckBox();

            //bind events
            HeaderCheckBox.KeyUp += new KeyEventHandler(HeaderCheckBox_KeyUp);
            HeaderCheckBox.MouseClick += new MouseEventHandler(HeaderCheckBox_MouseClick);
            gvJourney.CellValueChanged += new DataGridViewCellEventHandler(gvJourney_CellValueChanged);
            gvJourney.CurrentCellDirtyStateChanged += new EventHandler(gvJourney_CurrentCellDirtyStateChanged);
            gvJourney.CellPainting += new DataGridViewCellPaintingEventHandler(gvJourney_CellPainting);

            gvJourney.DataSource = Journeys;
            TotalCheckBoxes = gvJourney.RowCount;
            TotalCheckedCheckBoxes = 0;

            TotalJourneys.Text = Journeys.Count.ToString();

            btnImport.Enabled = Journeys.Count>0;

            foreach (string l in Lines)
               Line.Text += l + ", ";
            Line.Text = Line.Text.Trim().TrimEnd(',');
           

        }


        private void ImportJourneys()         
        {

            int index = 0;

            try
            {
                if (String.IsNullOrEmpty(Line.Text))
                    MessageBox.Show("");

                using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                {

                    //global list of All stops from database
                    var allDBStops = dbContext.Stops.ToList();
                    

                    foreach (Journey journey in Journeys)
                    {
                        //gvJourney.Rows[index++].Cells[0].

                        foreach (StopHop stop in journey.JourneyStops)
                        {
                            if (stop.Zone == "0" && String.IsNullOrEmpty(stop.StopName) == false)
                            {
                                stop.Zone = Util.GetZoneName(stop.Latitude, stop.Longitude, true);
                            }
                        }

                        if(journey.ScheduleNumber==0)
                        {
                            journey.ScheduleNumber = Util.GetScheduleNumber(journey, true);
                        }
                                            
                        

                         journey.JourneyData = CreateJourneyData(journey);

                        DataAccess.DBModels.Journey j = new DataAccess.DBModels.Journey
                        {
                            BusNumber = journey.BusNumber,
                            CustomerId = journey.CustomerId,
                            EndTime = journey.EndTime,
                            Destination = journey.Destination,
                            Active = journey.Active,
                            //JourneyAhead = journey.JourneyAhead,
                            //JourneyBehind = journey.JourneyBehind,
                            //JourneyData = journey.JourneyData, 
                            Line = journey.Schedule.Line,
                            PlannedArrivalTime = journey.PlannedArrivalTime,
                            PlannedDepartureTime = journey.PlannedDepartureTime,
                            ScheduleNumber = journey.ScheduleNumber,
                            StartPoint = journey.StartPoint,
                            StartTime = journey.StartTime,
                            
                        };

                        dbContext.AddToFutureJourneys(j);

                        //add missing stops in DB Stops Table
                        foreach (StopHop stop in journey.JourneyStops)
                        {

                            try
                            {
                                decimal ibiStopNumber = IBI.Shared.AppUtility.IsNullDec(Util.TransformStopNo(stop.StopNumber));
                                //check stop from global stop list.
                                //var st = allDBStops.Where(s => s.StopName == stop.StopName && s.GID == ibiStopNumber).FirstOrDefault();
                                var st = allDBStops.Where(s => s.GID == ibiStopNumber).FirstOrDefault();
                                if (st == null)
                                {
                                    DataAccess.DBModels.Stop newStop = new DataAccess.DBModels.Stop()
                                    {
                                        StopName = stop.StopName,
                                        GID = IBI.Shared.AppUtility.IsNullDec(Util.TransformStopNo(stop.StopNumber)),
                                        GPSCoordinateEW = stop.Longitude,
                                        GPSCoordinateNS = stop.Latitude,
                                        GPSProjection = "UTM32N"
                                    };

                                    dbContext.AddToStops(newStop);
                                    allDBStops.Add(newStop);//added to global list for better next comparison.
                                }
                                else
                                {
                                    // dont update 
                                    //st.StopName = TransformStopNo(stop.StopName),
                                    //st.GID = IBI.Shared.AppUtility.IsNullDec(TransformStopNo(stop.StopNumber));
                                    //st.GPSCoordinateEW = stop.Longitude;
                                    //st.GPSCoordinateNS = stop.Latitude;
                                    //st.GPSProjection = "UTM32N";
                                }

                            }
                            catch (Exception exStops)
                            {
                                throw new Exception(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
                            }
                        }
                    
                    }
                     
                    //Save new stops to DB-->Stops table
                    
                    dbContext.SaveChanges();
                    MessageBox.Show("All journeys successfully imported to the database");
                }
            }
            catch (Exception ex) {
                MessageBox.Show("Problem Saving Journeys to Database. [" + ex.Message + "]");
            }
                
        }

        private string GetStopName(string stopNumber)
        {
            stopNumber = Util.UnTransformStopNo(stopNumber);

            //search in cache first
            var stop = StopNamesCache.FirstOrDefault(s => s.Key == stopNumber);
            string stopName = "";

            if (!String.IsNullOrEmpty(stop.Value))
            {
                stopName = stop.Value;
            }
            else
            {
                var sstop = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();

                if (sstop!=null)
                {
                    //stopName = sstop.Split(' ')[2];
                    stopName = sstop.Substring(14);

                    int endIndex = stopName.IndexOf('(');
                    if(endIndex>0)
                        stopName = stopName.Substring(0, endIndex).Trim();

                    StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
                }
                else
                {
                    stopName = "";
                }

                
            }
            
            return stopName;
        }

        private string GetStopCoord(string stopNumber)
        {
            //search in cache first
            var stop = StopCoordCache.FirstOrDefault(s => s.Key == stopNumber);
            string stopCoord = "|";
            if (!String.IsNullOrEmpty(stop.Value))
            {
                stopCoord = stop.Value;
            }
            else
            {
                stopCoord = StopCoordData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();
                if (!String.IsNullOrEmpty(stopCoord))
                {
                    string[] arrCoord = stopCoord.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string lat = arrCoord.Length > 1 ? arrCoord[1] : "";
                    string lon = arrCoord.Length > 2 ? arrCoord[2] : "";

                    stopCoord = lat + "|" + lon;
                    StopCoordCache.Add(new KeyValuePair<string, string>(stopNumber, stopCoord));
                }
                else
                {
                    stopCoord = "|";
                }

            }

            
            return stopCoord;
             
        }

        private string Adjust24HourTime(string time, ref int day2Add) {

            if (!String.IsNullOrEmpty(time))
            {
                int t = 0;
                bool tResult = int.TryParse(time, out t);

                if (tResult)
                {
                    if (t >= 2400)
                    {
                        day2Add = 1;
                        time = (t - 2400).ToString();
                        time = time.Length < 4 ? "00000" + time : time;
                        time = time.Substring(time.Length - 4);

                    }
                }
            }

            return time;
        }


        private string CreateJourneyData(Journey journey)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
           
           string stops = "";
           foreach(StopHop jStop in journey.JourneyStops)
            {

               DateTime plannedDepartureTime = String.IsNullOrEmpty(jStop.DepartureTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + jStop.DepartureTime, "MM/dd/yyyy HHmm", CultureInfo.InvariantCulture);
               DateTime plannedArrivalTime = String.IsNullOrEmpty(jStop.ArrivalTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + jStop.ArrivalTime, "MM/dd/yyyy HHmm", CultureInfo.InvariantCulture);

               if (plannedDepartureTime != DateTime.MinValue)
               {
                   plannedDepartureTime = plannedDepartureTime.AddDays(jStop.Day2addDeparture);
               }

               if (plannedArrivalTime != DateTime.MinValue)
               {
                   plannedArrivalTime = plannedArrivalTime.AddDays(jStop.Day2addArrival );
               }

               stops += String.Format(@"<StopInfo StopNumber=""{0}"">
                              <StopName>{1}</StopName>
                              <GPSCoordinateNS>{2}</GPSCoordinateNS>
                              <GPSCoordinateEW>{3}</GPSCoordinateEW>
                              <Zone>{4}</Zone>
                              <IsCheckpoint>{5}</IsCheckpoint>
                              <ActualArrivalTime />
                              <ActualDepartureTime />
                              <PlannedDepartureTime>{6}</PlannedDepartureTime>
                              <PlannedArrivalTime>{7}</PlannedArrivalTime>
                              <EstimatedArrivalTime />
                              <EstimatedDepartureTime />
                            </StopInfo>", Util.TransformStopNo(jStop.StopNumber), jStop.StopName, jStop.Latitude, jStop.Longitude, jStop.Zone, jStop.IsCheckpoint,
                                        plannedDepartureTime == DateTime.MinValue ? "" : plannedDepartureTime.ToString("MM/dd/yyyyTHH:mm:ss"),
                                        plannedArrivalTime == DateTime.MinValue ? "" : plannedArrivalTime.ToString("MM/dd/yyyyTHH:mm:ss"));
               
            }

           string xmlData = String.Format(@"<Journey>
                                  <BusNumber>{0}</BusNumber>
                                  <JourneyNumber>{1}</JourneyNumber>
                                  <Line>{2}</Line>
                                  <ScheduleNumber>{3}</ScheduleNumber>
                                  <FromName>{4}</FromName>
                                  <DestinationName>{5}</DestinationName>
                                  <JourneyStops>
                                     {6}
                                  </JourneyStops>    
                                </Journey>", "0", 0, journey.Line, journey.ScheduleNumber, journey.StartPoint, journey.Destination, stops);
           return xmlData;           
        
        }


        #region Private Helper       

   

        public string GetUniqueSchedules()
        {
            return "";  
        }


        #endregion

    }

    public class Util
    {

        private static List<KeyValuePair<string, string>> _ScheduleCache = null;
        private static List<KeyValuePair<string, string>> _ZoneCache = null;

        static List<KeyValuePair<string, string>> ScheduleCache 
        { 
            get
            {
                if (_ScheduleCache == null)
                    _ScheduleCache = new List<KeyValuePair<string, string>>();

                return _ScheduleCache;
            } 
            set
            {
                _ScheduleCache = value;
            }  //value: scheduleId|stopSequenceMD5
        }

        static List<KeyValuePair<string, string>> ZoneCache
        { 
            get
            {
                if (_ZoneCache == null)
                    _ZoneCache = new List<KeyValuePair<string, string>>();

                return _ZoneCache;
            }
            set
            {
                _ZoneCache = value;
            }
        } 

        public static string GetZoneName(string lat, string lon, bool genearateZoneInDB)
        {
            //search in cache first
            string zoneName = "";
            string key = lat + "," + lon;

            var zone = ZoneCache.FirstOrDefault(s => s.Key == key);
            

            if (!String.IsNullOrEmpty(zone.Value))
            {
                zoneName = zone.Value;
            }
            else
            {
                if (genearateZoneInDB)
                {
                    try
                    {
                        using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                        {
                            var dbZone = IBI.REST.Schedule.ScheduleServiceController.GetZone(lat, lon);

                            if (dbZone != null)
                            {
                                zoneName = dbZone.Zone;
                                ZoneCache.Add(new KeyValuePair<string, string>(key, zoneName));
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        zoneName = string.Empty;
                    }
                }

                else
                {
                    zoneName = "0";
                }
            }

            return zoneName;
        }

        public static void FillSchedulesOfCurrentCustomer(string CustomerID)
        {
            ScheduleCache.Clear();

            using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
            {
                int custId = Convert.ToInt32(CustomerID);
                var schedules = dbContext.Schedules.Where(s => s.CustomerID == custId);
                foreach (var schedule in schedules)
                {
                    string key = custId.ToString() + "|" + schedule.Line + "|" + schedule.FromName + "|" + schedule.Destination + "|" + schedule.ViaName;
                    string value = schedule.ScheduleID + "|" + schedule.StopsSequenceMD5;
                    ScheduleCache.Add(new KeyValuePair<string, string>(key, value));
                }

            }
        }

        public static int GetScheduleNumber(Journey journey, bool generateScheduleInDB)
        {
            int scheduleId = 0;

            int customerId = journey.CustomerId;
            string line = journey.Line;
            string fromName = journey.StartPoint;
            string destination = journey.Destination;
            string viaName = journey.ViaName;
            string md5 = "";

            //generate StopSequenceMD5

            string stopSequence = "";
            foreach (StopHop stop in journey.JourneyStops)
            {
                stopSequence += stop.StopNumber + ",";
            }
            stopSequence = stopSequence.TrimEnd(',');

            string curStopSequenceMD5 = Util.ConvertStringtoMD5(stopSequence);


            try
            {
                string key = customerId.ToString() + "|" + line + "|" + fromName + "|" + destination + "|" + viaName;

                var scheduleItems = ScheduleCache.Where(s => s.Key == key);

                if (scheduleItems != null)
                {

                    foreach (KeyValuePair<string, string> scheduleItem in scheduleItems)
                    {
                        if (scheduleItem.Value != null && scheduleItem.Value.Split('|')[0] != "0")
                        {
                            scheduleId = Convert.ToInt32(scheduleItem.Value.ToString().Split('|')[0]);
                            string scheduleStopSeqMD5 = scheduleItem.Value.ToString().Split('|')[1];

                            //Compare StopSequenceMD5
                            if (curStopSequenceMD5 == scheduleStopSeqMD5)
                            {
                                return scheduleId;
                            }
                        }

                    }
                }

                scheduleId = 0; //reset
                using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                {
                    var schedules = dbContext.Schedules.Where(s => s.CustomerID == customerId &&
                                                    s.Line == line &&
                                                    s.FromName == fromName &&
                                                    s.Destination == destination &&
                                                    (s.ViaName == viaName || s.ViaName == null || s.ViaName == "")
                                                    );

                    foreach (var schedule in schedules)
                    {
                        if (schedule != null && curStopSequenceMD5 == schedule.StopsSequenceMD5)
                        {
                            scheduleId = schedule.ScheduleID;
                        }
                    }

                    if (scheduleId == 0 && generateScheduleInDB)
                    {
                        DataAccess.DBModels.Schedule sch = new DataAccess.DBModels.Schedule
                        {
                            CustomerID = customerId,
                            Destination = destination,
                            FromName = fromName,
                            Schedule1 = "",
                            ScheduleXML = "",
                            SelectionStructure = "",
                            StopCount = journey.JourneyStops.Count,
                            Line = line,
                            Updated = DateTime.Now,
                            ViaName = viaName,
                            MD5 = md5,
                            StopsSequenceMD5 = curStopSequenceMD5
                        };

                        dbContext.AddToSchedules(sch);

                        dbContext.SaveChanges();

                        scheduleId = sch.ScheduleID;

                        string scheduleXML = Util.GenerateScheduleXML(journey, scheduleId);

                        //update 4 columns of schedule. and update again
                        sch.Schedule1 = @"<?xml version=""1.0"" encoding=""utf-8""?>" + scheduleXML;
                        sch.ScheduleXML = scheduleXML;
                        sch.Updated = DateTime.Now;
                        sch.MD5 = Util.ConvertStringtoMD5(sch.Schedule1);

                        sch.StopsSequenceMD5 = curStopSequenceMD5;


                        dbContext.SaveChanges();

                        dbContext.UpdateSelectionStructure(true);


                        //add this schedule to ScheduleCache.
                        ScheduleCache.Add(new KeyValuePair<string, string>(key, scheduleId + "|" + sch.StopsSequenceMD5));

                    }
                    else
                    {
                        scheduleId = 0;
                    }





                }


            }

            catch (Exception ex)
            {
                MessageBox.Show("Failed to get schedule for current journey [" + ex.Message + "]");
            }

            return scheduleId;
        }

        public static string GenerateScheduleXML(Journey journey, int scheduleId)
        {

            string stopsXML = "";
            foreach (StopHop stop in journey.JourneyStops)
            {
                stopsXML += String.Format(@"<StopInfo StopNumber=""{0}"">
                                                              <StopName>{1}</StopName>
                                                              <GPSCoordinateNS>{2}</GPSCoordinateNS>
                                                              <GPSCoordinateEW>{3}</GPSCoordinateEW>
                                                              <Zone>{4}</Zone>
                                                              <IsCheckpoint>{5}</IsCheckpoint>
                                                            </StopInfo>", TransformStopNo(stop.StopNumber), stop.StopName, stop.Latitude, stop.Longitude, stop.Zone, stop.IsCheckpoint);

            }

            string scheduleXML = String.Format(@"<Schedule xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                                                      <ScheduleNumber>{0}</ScheduleNumber>
                                                                      <Line>{1}</Line>
                                                                      <FromName>{2}</FromName>
                                                                      <DestinationName>{3}</DestinationName>
                                                                      <ViaName>{4}</ViaName>
                                                                      <JourneyStops>{5}</JourneyStops>
                                                                 </Schedule>", scheduleId.ToString(), journey.Line, journey.StartPoint, journey.Destination, journey.ViaName, stopsXML);

            return scheduleXML;

        }

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static string ConvertStringtoMD5(string strword)
        {
            return mermaid.BaseObjects.IO.FileHash.FromStream(GenerateStreamFromString(strword)).ToString();
        }

        public static string TransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = "9025200" + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }


        public static string UnTransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.unTransformStopNo"))
            {

                //eg. 9025200000028662
                if (stopNo.Length == 16) // it's a transformed stop number
                {
                    stopNo = Convert.ToInt32(stopNo.Replace("9025200", "")).ToString();
                }

                return stopNo;
            }
        }
    }
}
