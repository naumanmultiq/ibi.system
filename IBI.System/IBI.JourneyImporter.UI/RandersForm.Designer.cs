﻿namespace IBI.JourneyImporter.UI
{
    partial class RandersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rutfilepath = new System.Windows.Forms.TextBox();
            this.btnBrowseTUR = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.outputFolder = new System.Windows.Forms.TextBox();
            this.btnBrowseOutputFolder = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.stafielpath = new System.Windows.Forms.TextBox();
            this.btnBrowseSTA = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.turfilepath = new System.Windows.Forms.TextBox();
            this.btnBrowseRUT = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnImportRanders = new System.Windows.Forms.Button();
            this.coordfilepath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.customerid = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rutfilepath
            // 
            this.rutfilepath.Location = new System.Drawing.Point(110, 13);
            this.rutfilepath.Name = "rutfilepath";
            this.rutfilepath.Size = new System.Drawing.Size(381, 20);
            this.rutfilepath.TabIndex = 5;
            this.rutfilepath.Text = "C:\\mermaid\\documents\\Nobina Data\\Randers\\RUT.dat";
            // 
            // btnBrowseTUR
            // 
            this.btnBrowseTUR.Location = new System.Drawing.Point(510, 52);
            this.btnBrowseTUR.Name = "btnBrowseTUR";
            this.btnBrowseTUR.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseTUR.TabIndex = 4;
            this.btnBrowseTUR.Text = "Browse";
            this.btnBrowseTUR.UseVisualStyleBackColor = true;
            this.btnBrowseTUR.Click += new System.EventHandler(this.btnBrowseTUR_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "RUT File";
            // 
            // outputFolder
            // 
            this.outputFolder.Location = new System.Drawing.Point(110, 164);
            this.outputFolder.Name = "outputFolder";
            this.outputFolder.Size = new System.Drawing.Size(381, 20);
            this.outputFolder.TabIndex = 8;
            this.outputFolder.Text = "C:\\mermaid\\documents\\Nobina Data\\Randers\\Output";
            // 
            // btnBrowseOutputFolder
            // 
            this.btnBrowseOutputFolder.Location = new System.Drawing.Point(510, 161);
            this.btnBrowseOutputFolder.Name = "btnBrowseOutputFolder";
            this.btnBrowseOutputFolder.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseOutputFolder.TabIndex = 7;
            this.btnBrowseOutputFolder.Text = "Browse";
            this.btnBrowseOutputFolder.UseVisualStyleBackColor = true;
            this.btnBrowseOutputFolder.Click += new System.EventHandler(this.btnBrowseOutputFolder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Output Folder";
            // 
            // stafielpath
            // 
            this.stafielpath.Location = new System.Drawing.Point(110, 89);
            this.stafielpath.Name = "stafielpath";
            this.stafielpath.Size = new System.Drawing.Size(381, 20);
            this.stafielpath.TabIndex = 11;
            this.stafielpath.Text = "C:\\mermaid\\documents\\Nobina Data\\Randers\\STA.dat";
            // 
            // btnBrowseSTA
            // 
            this.btnBrowseSTA.Location = new System.Drawing.Point(510, 86);
            this.btnBrowseSTA.Name = "btnBrowseSTA";
            this.btnBrowseSTA.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseSTA.TabIndex = 10;
            this.btnBrowseSTA.Text = "Browse";
            this.btnBrowseSTA.UseVisualStyleBackColor = true;
            this.btnBrowseSTA.Click += new System.EventHandler(this.btnBrowseSTA_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Stop(STA) File";
            // 
            // turfilepath
            // 
            this.turfilepath.Location = new System.Drawing.Point(110, 50);
            this.turfilepath.Name = "turfilepath";
            this.turfilepath.Size = new System.Drawing.Size(381, 20);
            this.turfilepath.TabIndex = 14;
            this.turfilepath.Text = "C:\\mermaid\\documents\\Nobina Data\\Randers\\TUR.dat";
            // 
            // btnBrowseRUT
            // 
            this.btnBrowseRUT.Location = new System.Drawing.Point(510, 15);
            this.btnBrowseRUT.Name = "btnBrowseRUT";
            this.btnBrowseRUT.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseRUT.TabIndex = 13;
            this.btnBrowseRUT.Text = "Browse";
            this.btnBrowseRUT.UseVisualStyleBackColor = true;
            this.btnBrowseRUT.Click += new System.EventHandler(this.btnBrowseRUT_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "TUR File";
            // 
            // btnImportRanders
            // 
            this.btnImportRanders.Location = new System.Drawing.Point(416, 197);
            this.btnImportRanders.Name = "btnImportRanders";
            this.btnImportRanders.Size = new System.Drawing.Size(75, 23);
            this.btnImportRanders.TabIndex = 15;
            this.btnImportRanders.Text = "Import";
            this.btnImportRanders.UseVisualStyleBackColor = true;
            this.btnImportRanders.Click += new System.EventHandler(this.btnImportRanders_Click);
            // 
            // coordfilepath
            // 
            this.coordfilepath.Location = new System.Drawing.Point(110, 126);
            this.coordfilepath.Name = "coordfilepath";
            this.coordfilepath.Size = new System.Drawing.Size(381, 20);
            this.coordfilepath.TabIndex = 18;
            this.coordfilepath.Text = "C:\\mermaid\\documents\\Nobina Data\\STOP_Randers.csv";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(510, 123);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "CORD file";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // customerid
            // 
            this.customerid.Location = new System.Drawing.Point(110, 197);
            this.customerid.Name = "customerid";
            this.customerid.Size = new System.Drawing.Size(96, 20);
            this.customerid.TabIndex = 19;
            this.customerid.Text = "112164";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "CustomerID";
            // 
            // RandersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 231);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.customerid);
            this.Controls.Add(this.coordfilepath);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnImportRanders);
            this.Controls.Add(this.turfilepath);
            this.Controls.Add(this.btnBrowseRUT);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.stafielpath);
            this.Controls.Add(this.btnBrowseSTA);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.outputFolder);
            this.Controls.Add(this.btnBrowseOutputFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rutfilepath);
            this.Controls.Add(this.btnBrowseTUR);
            this.Controls.Add(this.label1);
            this.Name = "RandersForm";
            this.Text = "RandersForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox rutfilepath;
        private System.Windows.Forms.Button btnBrowseTUR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox outputFolder;
        private System.Windows.Forms.Button btnBrowseOutputFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox stafielpath;
        private System.Windows.Forms.Button btnBrowseSTA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox turfilepath;
        private System.Windows.Forms.Button btnBrowseRUT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnImportRanders;
        private System.Windows.Forms.TextBox coordfilepath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox customerid;
        private System.Windows.Forms.Label label6;
    }
}