﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.JourneyImporter.UI
{
    class StopHop
    {
        public string StopNumber { get; set; }
        public string StopName { get; set; }
        public string ArrivalTime { get; set; }
        public string DepartureTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Zone { get; set; }
        public string IsCheckpoint { get; set; }
        public int Day2addArrival { get; set; }
        public int Day2addDeparture { get; set; }


        public StopHop(string stopNumber, string stopName, string arrivalTime, string departureTime, string lat, string lon, string zone, string isCheckpoint, int day2AddArrival, int day2AddDeparture )
        {
            this.StopNumber = stopNumber;
            this.StopName = stopName;
            this.ArrivalTime = arrivalTime;
            this.DepartureTime = departureTime;
            this.Latitude = lat;
            this.Longitude = lon;
            this.Zone = zone;
            this.IsCheckpoint = String.IsNullOrEmpty(IsCheckpoint) ? "false" : isCheckpoint;
            this.Day2addArrival = day2AddArrival;
            this.Day2addDeparture = day2AddDeparture;
        }
    }
}
