﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.JourneyImporter.UI
{ 
    public class Journey
    {
        public int JourneyNumber { get; set; }
        public int ScheduleNumber { get; set; }
        public string BusNumber { get; set; }
        public int CustomerId { get; set; }
        public DateTime JourneyDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Line { get; set; }
        public string StartPoint { get; set; }
        public string Destination { get; set; }
        public string ViaName { get; set; }
        public DateTime? PlannedArrivalTime { get; set; }
        public DateTime? PlannedDepartureTime { get; set; }
        public string JourneyData { get; set; }
        public int? JourneyAhead { get; set; }
        public int? JourneyBehind { get; set; }
        public bool? IsLastStop { get; set; }

       
        public ArrayList JourneyStops { get; set;}


        public Journey()
        {
            //Stops = new List<JourneyStop>();
            JourneyStops = new ArrayList();
        }
    }

    //public class JourneyStop 
    //{
    //    public string StopNumber { get; set; }
    //    public string StopName { get; set; }        
    //    public string Latitude { get; set; }
    //    public string Longitude { get; set; }
    //    public string Zone { get; set; }
    //    public bool IsCheckpoint { get; set; }
    //    public DateTime ActualArrivalTime { get; set; }
    //    public DateTime ActualDepartureTime { get; set; }
    //    public DateTime PlannedDepartureTime { get; set; }
    //    public DateTime PlannedArrivalTime { get; set; }
    //    public DateTime EstimatedArrivalTime { get; set; }
    //    public DateTime EstimatedDepartureTime { get; set; }

    //}
}
