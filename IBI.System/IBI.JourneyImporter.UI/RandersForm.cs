﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IBI.JourneyImporter.UI
{
    public partial class RandersForm : Form
    {
        public RandersForm()
        {
            InitializeComponent();
        }

        private void btnBrowseTUR_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                turfilepath.Text = openFileDialog1.FileName;
            }
        }

        private void btnBrowseRUT_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                rutfilepath.Text = openFileDialog1.FileName;
            }
        }

        private void btnBrowseSTA_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                stafielpath.Text = openFileDialog1.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                coordfilepath.Text = openFileDialog1.FileName;
            }
        }

        private void btnBrowseOutputFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                outputFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void CreateCSVContent(Journey journey)
        {
            string csvContent = "Scheduel number;" + journey.ScheduleNumber + ";;\n";
            csvContent += "Line;" + journey.Line + ";;\n";
            csvContent += "From;" + journey.StartPoint + ";;\n";
            csvContent += "To;" + journey.Destination + ";;\n";
            csvContent += "Via;" + journey.ViaName + ";;\n";
            csvContent += "Selection;;;\n";
            csvContent += ";;;\n";
            csvContent += ";;;\n";
            csvContent += "Id;Navn;X;Y\n";
            foreach (StopHop stop in journey.JourneyStops)
            {
                //731620000;Agerskellet/endestation;562188,069;6260015,491
                csvContent += stop.StopNumber + ";" + stop.StopName + ";" + stop.Latitude + ";" + stop.Longitude + "\n";
            }
            string filePath = Path.Combine(outputFolder.Text, journey.ScheduleNumber.ToString() + ".csv");
            FileInfo fInfo = new FileInfo(filePath);
            if (!fInfo.Directory.Exists)
            {
                fInfo.Directory.Create();
            }
            File.AppendAllText(filePath, csvContent, Encoding.UTF8);//.GetEncoding("IBM00858"));

        }

        private void btnImportRanders_Click(object sender, EventArgs e)
        {
            // Import Logic
            Util.FillSchedulesOfCurrentCustomer(customerid.Text);

            string[] rutData = File.ReadAllLines(rutfilepath.Text, Encoding.GetEncoding("IBM00858"));
            string[] turData = File.ReadAllLines(turfilepath.Text, Encoding.GetEncoding("IBM00858"));
            string[] staData = File.ReadAllLines(stafielpath.Text, Encoding.GetEncoding("IBM00858"));
            string[] coordData = File.ReadAllLines(coordfilepath.Text, Encoding.GetEncoding("IBM00858"));
            string outputPath = outputFolder.Text;

            string rutLine = string.Empty;
            string journeyId = string.Empty;
            string stopnumber = string.Empty;
            string stoparrival = string.Empty;
            string stopdeparture = string.Empty;

            Journey tmpJourney = null;
            ArrayList list = new ArrayList();
            string missingCoordinateFiles = string.Empty;
            string audioLinkFile = string.Empty;
            ArrayList audioStops = new ArrayList();

            for (int index = 0; index <= rutData.Length; index++)
            {
                string tmpJourneyId = string.Empty;
                if (index < rutData.Length)
                {
                    rutLine = rutData[index];
                    tmpJourneyId = rutLine.Substring(0, 5);
                }
                
                if ( string.Compare(tmpJourneyId, journeyId) != 0 )
                {
                    // new journey started
                    if ( journeyId != "" )
                    {
                        tmpJourney.StartPoint = ((StopHop)tmpJourney.JourneyStops[0]).StopName;
                        tmpJourney.Destination = ((StopHop)tmpJourney.JourneyStops[tmpJourney.JourneyStops.Count-1]).StopName;
                        tmpJourney.ViaName = string.Empty;
                        tmpJourney.ScheduleNumber = Util.GetScheduleNumber(tmpJourney, true);
                        tmpJourney.JourneyNumber = Int32.Parse(journeyId);
                        if (!list.Contains(tmpJourney.ScheduleNumber.ToString()))
                        {
                            CreateCSVContent(tmpJourney);
                            list.Add(tmpJourney.ScheduleNumber.ToString());
                        }
                        //tmpJourney.PlannedDepartureTime = ((StopHop)tmpJourney.JourneyStops[0]).DepartureTime;
                        //tmpJourney.PlannedArrivalTime = ((StopHop)tmpJourney.JourneyStops[tmpJourney.JourneyStops.Count - 1]).ArrivalTime;
                        // journey completed

                    }
                    tmpJourney = new Journey();
                    tmpJourney.CustomerId = Int32.Parse(customerid.Text);
                    journeyId = tmpJourneyId;
                }
                stopnumber = rutLine.Substring(16, 9);
                stoparrival = rutLine.Substring(25, 4);
                stopdeparture = rutLine.Substring(29, 4);

                var tmpTURData = turData.Where(s => s.StartsWith(journeyId)).FirstOrDefault();
                string line = tmpTURData.Substring(5, 5).Trim();
                string journeyDesc = tmpTURData.Substring(12, 56);
                tmpJourney.Line = line;

                var tmpSTAData = staData.Where(s => s.StartsWith(stopnumber)).FirstOrDefault();
                string stopname = tmpSTAData.Substring(9, 50);
                if ( stopname.IndexOf("(") != -1 )
                {
                    stopname = stopname.Substring(0, stopname.IndexOf("("));
                }
                stopname = stopname.Replace("/endestation", "").Trim();

                if (!audioStops.Contains(stopnumber))
                {
                    audioLinkFile += stopnumber + ";" + stopname + ";" + System.Configuration.ConfigurationManager.AppSettings["serverurl"] + "/REST/TTS/TTSFile.mp3?filename=" + mermaid.BaseObjects.IO.FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopname))).ToString() + ".mp3&CustomerId=" + customerid.Text + "\n";
                    audioStops.Add(stopnumber);
                }

                decimal lat = 0;
                decimal lng = 0;
                var tmpCOORDData = coordData.Where(s => s.StartsWith(stopnumber)).FirstOrDefault();
                if (tmpCOORDData != null)
                {
                    string[] coordLine = tmpCOORDData.Split(new char[] { ';' });

                    string[] tmpCoordinates = IBI.Shared.GPSConverter.UTMToDec(coordLine[1].Replace(',', '.'), coordLine[2].Replace(',', '.'), 32);

                    lat = decimal.Parse(tmpCoordinates[0]);
                    lng = decimal.Parse(tmpCoordinates[1]);
                }
                else
                {
                    if (!missingCoordinateFiles.Contains(stopnumber + ";" + stopname))
                    {
                        missingCoordinateFiles += stopnumber + ";" + stopname + "\n";
                    }
                }

                string zone = "";// Util.GetZoneName(lat.ToString(), lng.ToString(), true);
                tmpJourney.JourneyStops.Add(new StopHop(stopnumber, stopname, stoparrival, stopdeparture, lat.ToString(), lng.ToString(), zone, "false", 0, 0));
            }
            //File.Create(Path.Combine(outputFolder.Text, "coord.txt"));
            File.AppendAllText(Path.Combine(outputFolder.Text, "missingcoord.txt"), missingCoordinateFiles);
            File.AppendAllText(Path.Combine(outputFolder.Text, "audio.csv"), audioLinkFile);
            MessageBox.Show("Process Completed, Check files in: " + outputFolder.Text);
        }

    }
}
