﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusSimulator
{
    public partial class frmBusSimulator : Form
    {
        BusThread[] busThreads = null;
        System.Timers.Timer timer = new System.Timers.Timer();
        public frmBusSimulator()
        {
            timer.Elapsed += timer_Elapsed;

            InitializeComponent();
        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                RootPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void StartJourneyButton_Click(object sender, EventArgs e)
        {
            timer.Interval = 60 * 1000;
            timer.Enabled = true;
            timer.Start();

            if (String.IsNullOrEmpty(CustomerId.Text) ||
                String.IsNullOrEmpty(SiteUrl.Text) ||
                String.IsNullOrEmpty(Buses.Text) ||
                String.IsNullOrEmpty(Schedules.Text) ||
                String.IsNullOrEmpty(RootPath.Text) ||
                String.IsNullOrEmpty(Interval.Text) ||
                String.IsNullOrEmpty(StopInterval.Text)
                )
            {
                MessageBox.Show("Please provide values for all fields");
            }
            try
            {
                StartJourneyButton.Enabled = false;
                btnStopJourney.Enabled = true;
                chkStopped.Enabled = true;
                string[] buses = Buses.Text.Split(',');
                string[] schedules = Schedules.Text.Split(',');
                busThreads = new BusThread[buses.Length];

                for (int threadIndex = 0; threadIndex < busThreads.Length; threadIndex++)
                {
                    busThreads[threadIndex] = new BusThread();
                    busThreads[threadIndex].thread_id = threadIndex;
                    busThreads[threadIndex].busnumber = Int32.Parse(buses[threadIndex]);
                    busThreads[threadIndex].customerid = Int32.Parse(CustomerId.Text);
                    busThreads[threadIndex].rootpath = RootPath.Text;
                    busThreads[threadIndex].schedulenumber = Int32.Parse(schedules[threadIndex]);
                    busThreads[threadIndex].siteurl = SiteUrl.Text;
                    busThreads[threadIndex].startwaittime = threadIndex * Int32.Parse(Interval.Text);
                    busThreads[threadIndex].stopinterval = Int32.Parse(StopInterval.Text);
                    busThreads[threadIndex].stopstoskip = Int32.Parse(txtStopsToSkip.Text);
                    busThreads[threadIndex].stopped = false;

                    Thread oThread = new Thread(new ThreadStart(busThreads[threadIndex].StartBus));
                    // Start the thread
                    oThread.Start();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //StartJourneyButton.Enabled = true;
                //btnStopJourney.Enabled = false;
            }
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;
            if (!busThreads.Any(j => j.journeycomplete == false))
            {
                if (StartJourneyButton.InvokeRequired)
                {
                    StartJourneyButton.Invoke(new MethodInvoker(delegate
                    {
                        StartJourneyButton.Enabled = true;
                    }));
                }

                if (btnStopJourney.InvokeRequired)
                {
                    btnStopJourney.Invoke(new MethodInvoker(delegate
                    {
                        btnStopJourney.Enabled = false;
                    }));
                }
            }
            else
                timer.Enabled = true;
        }

        private void chkStopped_CheckedChanged(object sender, EventArgs e)
        {
            foreach(BusThread bThread in busThreads)
            {
                bThread.stopped =  chkStopped.Checked;
            }
            //busThreads[0].stopped = chkStopped.Checked;
        }



        private void btnStopJourney_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (BusThread bThread in busThreads)
                {
                    bThread.terminat = true;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                StartJourneyButton.Enabled = true;
                btnStopJourney.Enabled = false;
            }
        }
    }
}
