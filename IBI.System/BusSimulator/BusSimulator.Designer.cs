﻿namespace BusSimulator
{
    partial class frmBusSimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CustomerId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SiteUrl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Buses = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Schedules = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RootPath = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.Interval = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.StopInterval = new System.Windows.Forms.TextBox();
            this.StartJourneyButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.txtStopsToSkip = new System.Windows.Forms.TextBox();
            this.chkStopped = new System.Windows.Forms.CheckBox();
            this.btnStopJourney = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CustomerId
            // 
            this.CustomerId.Location = new System.Drawing.Point(142, 41);
            this.CustomerId.Name = "CustomerId";
            this.CustomerId.Size = new System.Drawing.Size(183, 20);
            this.CustomerId.TabIndex = 0;
            this.CustomerId.Text = "2140";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "CustomerID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "IBI Site URL";
            // 
            // SiteUrl
            // 
            this.SiteUrl.Location = new System.Drawing.Point(142, 79);
            this.SiteUrl.Name = "SiteUrl";
            this.SiteUrl.Size = new System.Drawing.Size(399, 20);
            this.SiteUrl.TabIndex = 2;
            this.SiteUrl.Text = "http://localhost/IBI";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Buses";
            // 
            // Buses
            // 
            this.Buses.Location = new System.Drawing.Point(142, 117);
            this.Buses.Multiline = true;
            this.Buses.Name = "Buses";
            this.Buses.Size = new System.Drawing.Size(399, 53);
            this.Buses.TabIndex = 4;
            this.Buses.Text = "1061,1062,1063";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Schedules";
            // 
            // Schedules
            // 
            this.Schedules.Location = new System.Drawing.Point(142, 189);
            this.Schedules.Multiline = true;
            this.Schedules.Name = "Schedules";
            this.Schedules.Size = new System.Drawing.Size(399, 55);
            this.Schedules.TabIndex = 6;
            this.Schedules.Text = "2597,2597,2597";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 268);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Root Folder";
            // 
            // RootPath
            // 
            this.RootPath.Location = new System.Drawing.Point(142, 265);
            this.RootPath.Name = "RootPath";
            this.RootPath.Size = new System.Drawing.Size(399, 20);
            this.RootPath.TabIndex = 8;
            this.RootPath.Text = "C:\\mermaid\\journeyclient";
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(557, 263);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(84, 23);
            this.BrowseButton.TabIndex = 10;
            this.BrowseButton.Text = "Browse";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Bus Interval";
            // 
            // Interval
            // 
            this.Interval.Location = new System.Drawing.Point(142, 304);
            this.Interval.Name = "Interval";
            this.Interval.Size = new System.Drawing.Size(183, 20);
            this.Interval.TabIndex = 11;
            this.Interval.Text = "30";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(55, 347);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Stops Interval";
            // 
            // StopInterval
            // 
            this.StopInterval.Location = new System.Drawing.Point(142, 344);
            this.StopInterval.Name = "StopInterval";
            this.StopInterval.Size = new System.Drawing.Size(183, 20);
            this.StopInterval.TabIndex = 13;
            this.StopInterval.Text = "5";
            // 
            // StartJourneyButton
            // 
            this.StartJourneyButton.Location = new System.Drawing.Point(142, 383);
            this.StartJourneyButton.Name = "StartJourneyButton";
            this.StartJourneyButton.Size = new System.Drawing.Size(84, 23);
            this.StartJourneyButton.TabIndex = 15;
            this.StartJourneyButton.Text = "Start Journeys";
            this.StartJourneyButton.UseVisualStyleBackColor = true;
            this.StartJourneyButton.Click += new System.EventHandler(this.StartJourneyButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(341, 307);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Stops to Skip";
            // 
            // txtStopsToSkip
            // 
            this.txtStopsToSkip.Location = new System.Drawing.Point(428, 304);
            this.txtStopsToSkip.Name = "txtStopsToSkip";
            this.txtStopsToSkip.Size = new System.Drawing.Size(113, 20);
            this.txtStopsToSkip.TabIndex = 16;
            this.txtStopsToSkip.Text = "0";
            // 
            // chkStopped
            // 
            this.chkStopped.AutoSize = true;
            this.chkStopped.Enabled = false;
            this.chkStopped.Location = new System.Drawing.Point(428, 346);
            this.chkStopped.Name = "chkStopped";
            this.chkStopped.Size = new System.Drawing.Size(110, 17);
            this.chkStopped.TabIndex = 18;
            this.chkStopped.Text = "Resume Journeys";
            this.chkStopped.UseVisualStyleBackColor = true;
            this.chkStopped.CheckedChanged += new System.EventHandler(this.chkStopped_CheckedChanged);
            // 
            // btnStopJourney
            // 
            this.btnStopJourney.Enabled = false;
            this.btnStopJourney.Location = new System.Drawing.Point(242, 383);
            this.btnStopJourney.Name = "btnStopJourney";
            this.btnStopJourney.Size = new System.Drawing.Size(87, 23);
            this.btnStopJourney.TabIndex = 19;
            this.btnStopJourney.Text = "Stop Journeys";
            this.btnStopJourney.UseVisualStyleBackColor = true;
            this.btnStopJourney.Click += new System.EventHandler(this.btnStopJourney_Click);
            // 
            // frmBusSimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 443);
            this.Controls.Add(this.btnStopJourney);
            this.Controls.Add(this.chkStopped);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtStopsToSkip);
            this.Controls.Add(this.StartJourneyButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.StopInterval);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Interval);
            this.Controls.Add(this.BrowseButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.RootPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Schedules);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Buses);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SiteUrl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CustomerId);
            this.Name = "frmBusSimulator";
            this.Text = "Bus Simulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CustomerId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SiteUrl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Buses;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Schedules;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox RootPath;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Interval;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox StopInterval;
        private System.Windows.Forms.Button StartJourneyButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtStopsToSkip;
        private System.Windows.Forms.CheckBox chkStopped;
        private System.Windows.Forms.Button btnStopJourney;
    }
}

