﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Net;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Globalization;


namespace IBI.HaConHRXPost
{
    public class HRXDataPost : IDisposable
    {

        #region Members

        private int CustomerId { get; set; }
        private string Sender { get; set; }
        private string Lines { get; set; }
        private int PostIntervalInSeconds { get; set; }
        private int PingToleranceInSeconds { get; set; }
        private string HRX_Url { get; set; }
        private string HRX_Username { get; set; }
        private string HRX_Password { get; set; }

        private System.Timers.Timer tmrHRXPost
        {
            get;
            set;
        }


        private StringBuilder log = new StringBuilder();



        XslCompiledTransform transform = new XslCompiledTransform(false);

        #endregion



        #region Constructors

        public HRXDataPost()
        {
            log.AppendLine(DateTime.Now.ToString() + ": HRXDataPost new instance started");

            //transform.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data\\HRX.xslt"));
            transform.Load(typeof(XSLT_HRX)); //picking XSLT from compiled DLL 

            this.PostIntervalInSeconds = AppSettings.PostIntervalInSeconds();

            int[] customerIds = AppSettings.HaConHRXPostCustomerIds();
            if (customerIds.Length > 0)
            {
                this.CustomerId = customerIds[0];
            }

            string[] senders = AppSettings.HaConHRXPostSenders();
            if (senders.Length > 0)
            {
                this.Sender = senders[0];
            }

            string[] lns = AppSettings.HaConHRXPostLines();
            if (lns.Length > 0)
            {
                this.Lines = String.Join(",", lns);//IBI.Shared.AppUtility.ListToCommaValues(lines.ToList());
            }


            this.PingToleranceInSeconds = AppSettings.PingToleranceInSeconds();

            this.HRX_Url = AppSettings.HaConHRXPost_EndPoint_Url();
            //this.HRX_Username = AppSettings.HaConHRXPost_EndPoint_Username();
            //this.HRX_Password = AppSettings.HaConHRXPost_EndPoint_Password();

            this.tmrHRXPost = new System.Timers.Timer();
            this.tmrHRXPost.Interval = TimeSpan.FromSeconds(PostIntervalInSeconds).TotalMilliseconds;
            this.tmrHRXPost.Elapsed += new System.Timers.ElapsedEventHandler(tmrHRXPost_Elapsed);
            this.tmrHRXPost.Enabled = true;

            log.AppendLine(DateTime.Now.ToString() + ": DataService timer Initializes, timer is " + this.tmrHRXPost.Enabled);

        }

        #endregion

        #region Events

        private void tmrHRXPost_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            if (log == null)
                log = new StringBuilder();


            this.tmrHRXPost.Enabled = false;
            log.AppendLine(DateTime.Now.ToString() + ": DataService Timer Stops + ProcessLog Starts");


            try
            {
                string data = this.GetHRXData();
                PostHRX(data);

            }
            catch (Exception ex)
            {
                log.AppendLine(DateTime.Now.ToString() + ": DataService Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace);
                //throw ex;
            }
            finally
            {
                log.AppendLine(DateTime.Now.ToString() + ": DataService Timer initializes again");

                AppUtility.Log2File("DataService", log.ToString());

                log.Clear();
                log = null;

                this.StartDataServiceTimer();
            }

        }


        public void Dispose()
        {
            this.tmrHRXPost.Enabled = false;
            this.tmrHRXPost.Dispose();

        }

        #endregion

        #region Methods

        private void StartDataServiceTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.tmrHRXPost == null)
            {
                this.tmrHRXPost = new System.Timers.Timer();
                this.tmrHRXPost.Elapsed += new System.Timers.ElapsedEventHandler(tmrHRXPost_Elapsed);
            }

            this.tmrHRXPost.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(this.PostIntervalInSeconds); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            this.tmrHRXPost.Interval = TimeSpan.FromSeconds(PostIntervalInSeconds).TotalMilliseconds; ;
            this.tmrHRXPost.Enabled = true;

            log.AppendLine(Environment.NewLine + "#########################################################" + Environment.NewLine + Environment.NewLine + "DataService Timer initializes");

        }


        private string GetHRXData()
        {
            log.Append(Environment.NewLine + "#########################################################" + Environment.NewLine + Environment.NewLine);
            String result = string.Empty;

            if (AppSettings.HaConHRXPostEnabled())
            {
                String xmlData = "";
                bool bHasData = false;

                try
                {
                    using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                    {


                        String script = "GetHaConHRXData";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {

                            connection.Open();

                            command.CommandType = System.Data.CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("CustomerId", this.CustomerId);
                            command.Parameters.AddWithValue("Lines", this.Lines);
                            command.Parameters.AddWithValue("PingToleranceInSeconds", this.PingToleranceInSeconds);


                            using(XmlReader reader = command.ExecuteXmlReader())
                            {

                                bHasData = reader.Read();
                                while (reader.ReadState != System.Xml.ReadState.EndOfFile)
                                {
                                    xmlData += reader.ReadOuterXml();
                                }
                                reader.Close();
                            }

                            connection.Close();
                        }
                    }

                    if (!bHasData)
                    {
                        log.Append("No records found from DB" + Environment.NewLine);
                    }
                    else
                    {
                        //[KHI]: Temporary line below for testing.. need to delete
                        //xmlData = "<Buses><Bus><BusNumber>5643</BusNumber><CustomerId>2233</CustomerId><Longitude>9.935075</Longitude><Latitude>56.146943</Latitude><CurrentJourneyNumber>0</CurrentJourneyNumber><LastPing>2014-10-23T12:22:29.120</LastPing></Bus><Bus><BusNumber>5671</BusNumber><CustomerId>2233</CustomerId><Longitude>11.874755</Longitude><Latitude>54.782742</Latitude><CurrentJourneyNumber>0</CurrentJourneyNumber><LastPing>2014-10-23T12:21:29.137</LastPing></Bus><Bus><BusNumber>5673</BusNumber><CustomerId>2233</CustomerId><Longitude>12.022330</Longitude><Latitude>54.889832</Latitude><CurrentJourneyNumber>326838</CurrentJourneyNumber><LastPing>2014-10-23T12:22:29.170</LastPing><ScheduleId>1496</ScheduleId><ExternalReference>HaCon-20141023-860253250000</ExternalReference><Stop><StopId>40803</StopId><PlannedDepartureTime>2014-10-23T12:11:00</PlannedDepartureTime><StopSequence>1</StopSequence></Stop><Stop><StopId>41659</StopId><PlannedDepartureTime>2014-10-23T13:04:00</PlannedDepartureTime><StopSequence>37</StopSequence></Stop></Bus><Bus><BusNumber>5674</BusNumber><CustomerId>2233</CustomerId><Longitude>11.922698</Longitude><Latitude>54.724565</Latitude><CurrentJourneyNumber>326780</CurrentJourneyNumber><LastPing>2014-10-23T12:22:29.243</LastPing><ScheduleId>1487</ScheduleId><Line>740</Line><ExternalReference>HaCon-20141023-860229640100</ExternalReference><Stop><StopId>40897</StopId><PlannedDepartureTime>2014-10-23T11:56:00</PlannedDepartureTime><StopSequence>1</StopSequence></Stop><Stop><StopId>41660</StopId><PlannedDepartureTime>2014-10-23T12:35:00</PlannedDepartureTime><StopSequence>22</StopSequence></Stop></Bus><Bus><BusNumber>9977</BusNumber><CustomerId>2233</CustomerId><Longitude>11.876853</Longitude><Latitude>54.766797</Latitude><CurrentJourneyNumber>316534</CurrentJourneyNumber><LastPing>2014-10-23T12:22:29.023</LastPing><ScheduleId>2475</ScheduleId><ExternalReference>HaCon-20141022-860098320500</ExternalReference><Stop><StopId>41770</StopId><PlannedDepartureTime>2014-10-22T14:49:00</PlannedDepartureTime><StopSequence>1</StopSequence></Stop><Stop><StopId>42140</StopId><PlannedDepartureTime>2014-10-22T15:19:00</PlannedDepartureTime><StopSequence>30</StopSequence></Stop></Bus></Buses>";

                        log.Append("Data: " + Environment.NewLine + xmlData + Environment.NewLine);

                         
                        Regex pattern = new Regex(@"(\w+)-(?<date>\d+)-(?<tripNumber>\d+)");
                         
                        StringReader rd = new StringReader(xmlData);
                        XDocument xDoc = XDocument.Load(rd);

                        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
                        //TODO: KHI - Use this instead of hardcoding
                        //xDoc.Descendants("PlannedDepartureTime").ToList().ForEach(b => b.Value = string.Concat(b.Value, "+01:00"));
                        xDoc.Descendants("PlannedDepartureTime").ToList().ForEach(b => b.Value = string.Concat(b.Value, DateTime.Now.ToString("zzz")));
                        //xDoc.Descendants("LastPing").ToList().ForEach(b => b.Value = Convert.ToDateTime(b.Value).ToString("yyyy-MM-ddTHH:mm:ss+01:00"));
                        xDoc.Descendants("LastPing").ToList().ForEach(b => b.Value = Convert.ToDateTime(b.Value).ToString("yyyy-MM-ddTHH:mm:ss") + DateTime.Now.ToString("zzz"));
                        xDoc.Descendants("ExternalReference").Where(e => e.Value != null).ToList().ForEach(b => b.Value = DateTime.ParseExact(pattern.Match(b.Value).Groups["date"].Value, "yyyyMMdd", CultureInfo.InstalledUICulture).ToString("yyyy-MM-dd"));
                        xDoc.Descendants("ExternalReference").Where(e => String.IsNullOrEmpty(e.Value)).ToList().ForEach(b => b.Value = DateTime.Now.ToString("yyyy-MM-dd"));
                         
                        log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> XSLT Transformation starts" + Environment.NewLine);
                         
                        XsltArgumentList argsList = new XsltArgumentList();
                        //argsList.AddParam("FetchTime", "", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss+01:00"));
                        argsList.AddParam("FetchTime", "", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));
                        argsList.AddParam("Sender", "", this.Sender);
                        //argsList.AddParam("Line", "", this.Line);
                        argsList.AddParam("Operator", "", "Movia");


                        //BELOW code moved to Class LEVEL var and Constructor
                        //XslCompiledTransform transform = new XslCompiledTransform(false);
                        //string xsltFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data\\HRX.xslt");
                        //transform.Load(xsltFilePath);

                        using (StringWriter writer = new StringWriter())
                        {
                            transform.Transform(xDoc.CreateReader(), argsList, writer);
                            result = writer.ToString();
                        }

                        // stream.Close();
                        // stream.Dispose();
                        // transform = null;
                        xDoc = null;
                        xmlData = string.Empty;

                        log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "Transformed Data: " + result + Environment.NewLine);
                        log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> XSLT Transformation ends" + Environment.NewLine);
                    }

                }
                catch (Exception ex)
                {
                    log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> ERROR !!!" + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine);
                    AppUtility.Log2File("DataService", log.ToString() + Environment.NewLine + "Service must restart Now" + Environment.NewLine + "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" + Environment.NewLine);
                    //throw ex;
                }
                finally
                { 
                }


            }
            else
            {
                //AppUtility.Log2File("DataService", "DataService Not enabled");
            }

            return result;
        }


        private string PostHRX(string data)
        {
            string result = string.Empty;
            try
            {

                DateTime serverStartTime = DateTime.Now;

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(data);


                result = SOAPRequest.CallWebService(this.HRX_Url, this.HRX_Url, xDoc.DocumentElement.OuterXml);

                log.AppendLine(DateTime.Now.ToString() + ": " + String.Format("Result From HaCon:\n{0}\n\nWebClient Call Completed\n\n", result));

                xDoc = null;

            }
            catch (Exception ex)
            {
                log.AppendLine(DateTime.Now.ToString() + ": WebClient Call fails::" + ex.Message + " . " + ex.StackTrace + Environment.NewLine);
            }

            return result;
        }


        #endregion

    }


    public class SOAPRequest
    {
        public static string CallWebService(string _url, string _action, string data)
        {
            //var _url = "http://xxxxxxxxx/Service1.asmx";
            //   var _action = "http://xxxxxxxx/Service1.asmx?op=HelloWorld";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(data);
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            string soapResult;
            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }

                return soapResult;
            }

            return string.Empty;
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static XmlDocument CreateSoapEnvelope(string data)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            //soapEnvelop.LoadXml(string.Format(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body>{0}</SOAP-ENV:Body></SOAP-ENV:Envelope>", data));
            soapEnvelop.LoadXml(string.Format(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body>{0}</SOAP-ENV:Body></SOAP-ENV:Envelope>", data));

            return soapEnvelop;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
    }

}
