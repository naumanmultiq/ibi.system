﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" />

  <xsl:param name="Operator"></xsl:param>
  <xsl:param name="FetchTime"></xsl:param>
  <xsl:param name="Sender"></xsl:param>
  <xsl:param name="Line"></xsl:param>

  <xsl:key name="GroupData" match="Buses/Bus" use="@BusNumber" /> 
  
  
  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  
  <xsl:template match="/">
    <hrx:RealtimeInfo version="2.3.3" xsi:schemaLocation="urn:hrx hafHRX-2.3.3_eng.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:hrx="urn:hrx">
      <xsl:attribute name="operator">
        <xsl:value-of select="$Operator"/>
      </xsl:attribute>
      <xsl:attribute name="timestamp">
        <xsl:value-of select="$FetchTime"/>
      </xsl:attribute>
      <xsl:attribute name="sender">
        <xsl:value-of select="$Sender"/>
      </xsl:attribute>      
      <!--<xsl:for-each select="Buses/Bus[count(. | key('GroupData', concat(@BusNumber, '|', @StopSequence))[1]) = 1]">-->
      <xsl:for-each select="Buses/Bus[count(. | key('GroupData', @BusNumber)[1]) = 1]">
        <xsl:variable name="line" select="Line" />
        <xsl:variable name="jDate" select="LastPing" />
        <xsl:variable name="busNumber" select="BusNumber" />
        
        <hrx:RealTrip>
          <hrx:TripRef>
            <hrx:TripID>
              <hrx:TripName>
                <xsl:value-of select="$line" />                
              </hrx:TripName>
              <!-- this will always be 740, if bus is activly on a line 740 trip -->
              <hrx:OperatingDay>
                <xsl:value-of select="$jDate" />
              </hrx:OperatingDay>
              <hrx:UniqueID>
                <xsl:value-of select="$busNumber" />
              </hrx:UniqueID>
              <!-- Busnumber -->
            </hrx:TripID>
            <hrx:TripStartEnd>
              <xsl:for-each select="Stop">
                <xsl:choose>
                  <xsl:when test="StopSequence=1">
                    <hrx:StartStopID>
                      <xsl:value-of select="StopId" />
                    </hrx:StartStopID> 
                    <hrx:StartTime>
                      <xsl:value-of select="PlannedDepartureTime" />
                    </hrx:StartTime>
                  </xsl:when>
                  <xsl:otherwise>
                    <hrx:EndStopID>
                      <xsl:value-of select="StopId" />
                    </hrx:EndStopID> 
                    <hrx:EndTime>
                      <xsl:value-of select="PlannedDepartureTime" />
                    </hrx:EndTime>
                  </xsl:otherwise>
                </xsl:choose>                           
              </xsl:for-each>
            </hrx:TripStartEnd>
          </hrx:TripRef>
          <hrx:RealPosition>
            <hrx:XCoordinate>
              <xsl:value-of select="Longitude"/>
            </hrx:XCoordinate>
            <hrx:YCoordinate>
              <xsl:value-of select="Latitude"/>
            </hrx:YCoordinate>
            <hrx:RealDeparturePrediction>
              <xsl:value-of select="$FetchTime"/>
            </hrx:RealDeparturePrediction>
            <!-- timestamp for when this data was collected -->
          </hrx:RealPosition>
        </hrx:RealTrip> 
        
      </xsl:for-each>
    </hrx:RealtimeInfo>
  </xsl:template>
</xsl:stylesheet>
 