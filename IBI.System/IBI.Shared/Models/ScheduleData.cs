﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace IBI.Shared.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class ScheduleData
    {
        [DataMember(Order = 0)]
        public Schedule[] Schedules
        {
            get;
            set;
        }
    }
}