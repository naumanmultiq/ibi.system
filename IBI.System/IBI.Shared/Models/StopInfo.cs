﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared.Models
{
    [DataContract(Namespace="http://schemas.mermaid.dk/IBI")]
    public class StopInfo
    {
        private String stopNumber;
        private String stopName;
        private DateTime arrivalTime;
        private DateTime departureTime;
        private String gpsCoordinateNS;
        private String gpsCoordinateEW;
        private String stopSequence;
        private String zone;

        [XmlAttribute]
        [DataMember]
        public String StopNumber
        {
            get { return stopNumber; }
            set { stopNumber = value; }
        }

        [DataMember]
        public String StopName
        {
            get { return stopName; }
            set { stopName = value; }
        }

        [DataMember]
        public DateTime ArrivalTime
        {
            get { return arrivalTime; }
            set { arrivalTime = value; }
        }

        [DataMember]
        public DateTime DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }
        

        [DataMember]
        public String GPSCoordinateNS
        {
            get { return gpsCoordinateNS; }
            set { gpsCoordinateNS = value; }
        }

        [DataMember]
        public String GPSCoordinateEW
        {
            get { return gpsCoordinateEW; }
            set { gpsCoordinateEW = value; }
        }

        [DataMember]
        public String StopSequence
        {
            get { return stopSequence; }
            set { stopSequence = value; }
        }

        [DataMember]
        public String Zone
        {
            get { return zone; }
            set { zone = value; }
        }
        
        [DataMember]
        public Boolean IsCheckpoint
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PlannedDepartureTime { get; set; }

       
        [DataMember]
        public DateTime AADT_ArrivalTime { get; set; }

       
        [DataMember]
        public DateTime ADDT_DepartureTime { get; set; }

        
        [DataMember]
        public DateTime LMDT_Time { get; set; }
                      

    }
}

namespace IBI.Shared.Models.Realtime
{
    [DataContract(Name = "StopInfo",Namespace = "http://schemas.mermaid.dk/IBI"), XmlRoot(ElementName="StopInfo")]
    public class StopInfo
    {
        private String stopNumber;
        private String stopName;
        private DateTime arrivalTime;
        private DateTime departureTime;
        private String gpsCoordinateNS;
        private String gpsCoordinateEW;
        private String stopSequence;
        private String zone;

        [XmlAttribute]
        [DataMember]
        public String StopNumber
        {
            get { return stopNumber; }
            set { stopNumber = value; }
        }

        [DataMember]
        public String StopName
        {
            get { return stopName; }
            set { stopName = value; }
        }

        [DataMember]
        public DateTime ArrivalTime
        {
            get { return arrivalTime; }
            set { arrivalTime = value; }
        }

        [DataMember]
        public DateTime DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }


        [DataMember]
        public String GPSCoordinateNS
        {
            get { return gpsCoordinateNS; }
            set { gpsCoordinateNS = value; }
        }

        [DataMember]
        public String GPSCoordinateEW
        {
            get { return gpsCoordinateEW; }
            set { gpsCoordinateEW = value; }
        }

        [DataMember]
        public String StopSequence
        {
            get { return stopSequence; }
            set { stopSequence = value; }
        }

        [DataMember]
        public String Zone
        {
            get { return zone; }
            set { zone = value; }
        }

        [DataMember]
        public Boolean IsCheckpoint
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PlannedDepartureTime { get; set; }

        [DataMember]
        public DateTime PlannedArrivalTime { get; set; }

        //[DataMember]
        //public DateTime ExpectedDepartureTime { get; set; }


        //[DataMember]
        //public DateTime ExpectedArrivalTime { get; set; }



    }
}