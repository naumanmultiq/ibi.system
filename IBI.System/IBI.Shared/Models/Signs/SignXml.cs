﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class SignXml
    {
        private string image;

        [DataMember(Order = 1)]
        public string Xml
        {
            get { return image; }
            set { image = value; }
        }
    }
}
