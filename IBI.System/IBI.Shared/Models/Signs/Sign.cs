﻿using IBI.Shared.Models.BusTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IBI.Shared.Models.Signs
{
    [DataContract]
    public class Sign : IEquatable<Sign>
    {
        private const string IMAGE_ROOT_PATH = "/images/destinations";

        //basic properties from interface --> restricted to keep naming convention bcoz of JQuery Tree internal naming structure.

        [DataMember(Order = 1)]
        public string title { get; set; }

        [DataMember(Order = 2)]
        public bool isFolder { get; set; }

        [DataMember(Order = 3)]
        public bool isLazy { get; set; }


        [DataMember(Order = 4)]
        public string key { get; set; }

        [DataMember(Order = 5)]
        public bool expand { get; set; }


        [DataMember(Order = 6)]
        public List<Sign> children { get; set; }

        //additional properties

        [DataMember]
        public int Pk { get; set; }

         [DataMember]
        public int SignId { get; set; }


        [XmlAttribute]
        [DataMember]
        public int? ScheduleId { get; set; }

        [XmlAttribute]
        [DataMember(Order = 7)]
        public string Line { get; set; }

        [XmlAttribute]
        [DataMember(Order = 8)]
        public int SqlId { get; set; }

        [XmlAttribute]
        [DataMember(Order = 9)]
        public string Text { get; set; }

        [XmlAttribute]
        [DataMember(Order = 10)]
        public string MainText { get; set; }

        [XmlAttribute]
        [DataMember(Order = 11)]
        public string SubText { get; set; }

        [XmlAttribute]
        [DataMember(Order = 12)]
        public string Name { get; set; }

        [XmlAttribute]
        [DataMember(Order = 13)]
        public string Route { get; set; }

        [XmlAttribute]
        [DataMember(Order = 14)]
        public int CustomerId { get; set; }

        [XmlAttribute]
        [DataMember(Order = 15)]
        public string CustomerName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsGroup { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public DateTime DateAdded { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        [DataMember]
        public int? GroupId { get; set; }

        [DataMember]
        public int? ParentGroupId { get; set; }

        [DataMember]
        public string ParentGroupName { get; set; }

        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public String AlternateRowCss { get; set; }

        [DataMember]
        public String ClientInformation { get; set; }

        private static int rowNum;


        public Sign()
        {
            Line = "";
            CustomerName = "";
            Description = "";
            IsGroup = false;
            IsActive = false;
            DateAdded = DateTime.MinValue;
            DateModified = DateTime.MinValue;
            ParentGroupId = 0;
            ParentGroupName = "";
            Priority = 0;
            Text = "";

            MainText = SubText = Name = "";

            children = new List<Sign>();


        }

        private static int _keyId = 1;
        public Sign(int groupId, int signId, int customerId, string customerName, int? scheduleId, string name, string mainText, string subText, string lineText, bool isGroup, int? parentGroupId, string parentGroupName, bool excluded, int sortOrder)
        {
            this.SignId = signId;
            this.Line = lineText;
            this.CustomerId = customerId;
            this.CustomerName = customerName;
            this.ScheduleId = scheduleId;
            this.SqlId = isGroup ? groupId : signId;
            this.Name = name;
            this.MainText = mainText;
            this.SubText = subText;
            this.IsGroup = isGroup;
            this.IsActive = !excluded;
            this.GroupId = groupId;
            this.ParentGroupId = parentGroupId;
            this.ParentGroupName = parentGroupName;
            this.isFolder = this.IsGroup;
            this.Priority = sortOrder;
            this.key = (_keyId++).ToString();
            children = new List<Sign>();

            SetNode();
        }

        //this function needs to be changed if any requirements changes for Tree GUI.
        public void SetNode()
        {
            this.Text = this.Text ?? "";
            this.MainText = this.MainText ?? "";
            this.SubText = this.SubText ?? "";
            this.Line = this.Line ?? ""; 
            this.Name = this.Name ?? "";

            int type = 1;

            //KHI: isFolder needed?
            if (this.isFolder)
                type = 1;
            else
                type = 2;

            this.expand = true;

            this.Name = this.Name.Replace("#", "");
            
            //set alternate rowCSS

            if (type == 2 && this.IsGroup == false)
            {
                this.AlternateRowCss = rowNum % 2 == 0 ? " grey" : " white";
                //increment rowNum
                rowNum++;
            }
            else { this.AlternateRowCss = " emptyRow"; }


            if (IsGroup == true)
            {
                if (this.Name == "Special Destinations")
                {
                    this.title = "Mittle section~~~~~~~~";
                    return;
                }

                if (this.Name == "Destinations")
                {
                    this.title = "Line selection buttons~~~~~~~~";
                    return;
                }

                if (this.GroupId < 10000001)
                    this.title = this.Name + "~~~~~~" + GetSortImages() + "~~";
                else
                    this.title = this.Name + "~~~~~~~~";
            }
            else
            {
                this.title = this.Name + "~" + this.Line + "~" + this.MainText + "~" + this.SubText +"~" + this.ScheduleId.ToString() + "~" + GetStatusImage(this.IsActive) + "~" + GetSortImages() + "~" + GetEditLink() + "~" + GetDestImageLink();
            }

        }

        private string GetEditLink()
        {
            return "[EDIT]";
        }


        private string GetDestImageLink()
        {
            return "[IMAGE]";
        }

        private string GetStatusImage(bool status)
        {
            return status ? "[ACTIVE]" : "[INACTIVE]";

            //string img = "";

            //img = status ? "/green.png" : "/red.png";
           
            //if (img == "")
            //    return "";

            //string tooltip = status ? "Active" : "Inactive";


            //string imgPath = BusUtility.AppPath + IMAGE_ROOT_PATH + img;

            //string retImg = @"<img title='" + tooltip +
            //                "' src='" + imgPath +
            //                "' style='vertical-align: middle; width: 12px; height: 12px;' />";
            //return retImg;
        }

        private string GetSortImages()
        {
            return "[SORT]";

            //string imgUpPath = BusUtility.AppPath + IMAGE_ROOT_PATH + "/scrollup_grey.png";
            //string imgDownPath = BusUtility.AppPath + ".." + IMAGE_ROOT_PATH + "/scrolldown.png";

            //string retImg1 = @"<img title='" +
            //                "' src='" + imgUpPath +
            //                "' style='display: inline; vertical-align: middle; width: 14px; height: 10px; cursor:hand; cursor: pointer' onclick=\"SortUp(this.node," +groupId+", "+signId+", "+parentGroupId+")\"  />";

            //string retImg2 = @"<img title='" +
            //                "' src='" + imgDownPath +
            //                "' style='display: inline; vertical-align: middle; width: 14px; height: 10px; cursor:hand; cursor: pointer' onclick=\"SortDown(this.node" + key + "'," + groupId + ", " + signId + ", " + parentGroupId + ")\" />";
            //return retImg1 + retImg2;
        }

        private string GetSpecialDestImage()
        {
            string imgPath = BusUtility.AppPath + ".." + IMAGE_ROOT_PATH + "/DCU1.png";

            string retImg1 = @"<img title='" +
                            "' src='" + imgPath +
                            "' style='display: inline; vertical-align: middle; width: 64px; height: 39px;' />";

            return retImg1;
        }


        private string GetDestImage()
        {
            string imgPath = BusUtility.AppPath + ".." + IMAGE_ROOT_PATH + "/DCU2.png";

            string retImg1 = @"<img title='" +
                            "' src='" + imgPath +
                            "' style='display: inline; vertical-align: middle; width: 64px; height: 39px;' />";

            return retImg1;
        }

        public bool Equals(Sign other)
        {
           return true;
           /* return this.DestinationName == other.DestinationName &&
             this.ParentGroupName == other.ParentGroupName &&
              this.SelectionStructure == other.SelectionStructure &&
             this.Line == other.Line &&
             (other.IsGroup == true || this.Text == other.Text)
             ;
          */
        }


    }
}
