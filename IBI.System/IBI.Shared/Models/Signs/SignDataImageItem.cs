﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class SignDataImageItem
    {

        [DataMember(Order = 1)]
        public int SignDataId
        {
            get;
            set;
        }

        [DataMember(Order = 2)]
        public string Base64String
        {
            get;
            set;
        }

        [DataMember(Order = 3)]
        public string SignImage
        {
            get;
            set;
        }

        [DataMember(Order = 4)]
        public string base64ViewString
        {
            get;
            set;
        }

        [DataMember(Order = 4)]
        public string base64ViewLargeString
        {
            get;
            set;
        }
    }
}
