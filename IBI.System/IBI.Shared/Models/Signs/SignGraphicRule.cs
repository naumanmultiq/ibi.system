﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    public partial class SignGraphicRule
    { 
        #region Simple Properties

        [DataMember]
        public int GraphicsRuleId
        { get; set; }
        
        
        [DataMember]
        public string MainFont
        { get; set; }
        

        [DataMember]
        public string SubFont
        { get; set; }
        

        [DataMember]
        public string MainStartX
        { get; set; }
        

        [DataMember]
        public string MainStartY
        { get; set; }
        
        

        [DataMember]
        public string SubStartX
        { get; set; }
        

        [DataMember]
        public string SubStartY
        { get; set; }
        

        [DataMember]
        public string AllCaps
        { get; set; }
        

        [DataMember]
        public string ShowGrid
        { get; set; }
        

        [DataMember]
        public string LineX
        { get; set; }
        

        [DataMember]
        public string LineY
        { get; set; }
        

        [DataMember]
        public string LineDivider
        { get; set; }
        

        [DataMember]
        public string LineArea
        { get; set; }
        

        [DataMember]
        public string LineFont
        { get; set; }
        

        [DataMember]
        public string template
        { get; set; }
        

        [DataMember]
        public string MainFontSize
        { get; set; }
        

        [DataMember]
        public string SubFontSize
        { get; set; }
        

        [DataMember]
        public string Center
        { get; set; }
        

        [DataMember]
        public string UpScale
        { get; set; }
        

        [DataMember]
        public string DownScale
        { get; set; }
        

        [DataMember]
        public string AniDelay
        { get; set; }
        

        [DataMember]
        public string ShowErrors
        { get; set; }
        

        [DataMember]
        public string LineFontSize
        { get; set; }

        [DataMember]
        public string Note
        {
            get;
            set;
        }
        
        #endregion
    }
}
