﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class SignAddonXmlLayout
    {
        private int layoutId;
        private int? graphicRuleId;
        private string type;
        private int? width;
        private int? height;
        private string manufacturer;
        private string technology;
        private int? customerid;
        private SignXmlData[] signs;

        [DataMember(Order = 1)]
        public int LayoutId
        {
            get{return layoutId;}
            set{layoutId = value;}
        }

        [DataMember(Order = 2)]
        public int? GraphicRuleId
        {
            get { return graphicRuleId; }
            set { graphicRuleId = value; }
        }

        [DataMember(Order = 3)]
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        [DataMember(Order = 4)]
        public int? Width
        {
            get { return width; }
            set { width = value; }
        }

        [DataMember(Order = 5)]
        public int? Height
        {
            get { return height; }
            set { height = value; }
        }

        [DataMember(Order = 6)]
        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }

        [DataMember(Order = 7)]
        public string Technology
        {
            get { return technology; }
            set { technology = value; }
        }

        [DataMember(Order = 8)]
        public int? CustomerId
        {
            get { return customerid; }
            set { customerid = value; }
        }

        [DataMember(Order = 9)]
        public SignXmlData[] Signs
        {
            get { return this.signs; }
            set { this.signs = value; }
        }

        public SignAddonXmlLayout(IBI.DataAccess.DBModels.SignLayout layout)
        {
            this.LayoutId = layout.LayoutId;
            this.GraphicRuleId = layout.GraphicsRuleId;
            this.Type = layout.Type;
            this.Width = layout.Width;
            this.Height = layout.Height;
            this.Manufacturer = layout.Manufacturer;
            this.Technology = layout.Technology;
            this.CustomerId = layout.CustomerId;
        }

        public SignAddonXmlLayout(IBI.DataAccess.DBModels.SignLayout layout, int? graphicRuleId)
        {
            this.LayoutId = layout.LayoutId;
            this.GraphicRuleId = graphicRuleId;
            this.Type = layout.Type;
            this.Width = layout.Width;
            this.Height = layout.Height;
            this.Manufacturer = layout.Manufacturer;
            this.Technology = layout.Technology;
            this.CustomerId = layout.CustomerId;
        }

         public SignAddonXmlLayout()
         {
             
         }
    }
}
