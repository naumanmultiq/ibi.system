﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.SignAddons
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class SignAddonXmlData
    {
        private string signAddonId;
        private string contentId;
        private string page;
        private string image;
        private string ledImage;
        private string md5;

        [DataMember(Order = 1)]
        public string SignAddonId
        {
            get { return signAddonId; }
            set { signAddonId = value; }
        }

        [DataMember(Order = 2)]
        public string ContentId
        {
            get { return contentId; }
            set { contentId = value; }
        }

        [DataMember(Order = 3)]
        public string Page
        {
            get { return page; }
            set { page = value; }
        }

        [DataMember(Order = 4)]
        public string Base64
        {
            get { return image; }
            set { image = value; }
        }

        [DataMember(Order = 5)]
        public string MD5
        {
            get { return md5; }
            set { md5 = value; }
        }
    }
}
