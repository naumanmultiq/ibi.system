﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.SignAddons
{
    public class SignAddonEnums
    {
        public enum ActiveStatusColor
        {
            NONE = 0,
            GREEN = 1,
            RED = 2
        }

        public enum SignAddonType
        {
            SPECIAL = 100001,
            NORMAL = 100001
        }
    }
}
