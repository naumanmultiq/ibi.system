﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Shared.Models
{
    [DataContract]
    public class Customer
    {
        
        [DataMember]
        public int? CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

    }
     
}
