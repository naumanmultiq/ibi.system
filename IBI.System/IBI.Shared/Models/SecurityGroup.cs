﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class SecurityGroup
    {
        #region Properties

        [DataMember(Order = 0)]
        public String Name
        {
            get;
            set;
        }

        [DataMember(Order = 1)]
        public List<int> AccessibleCustomers
        {
            get;
            set;
        }

        #endregion

        public SecurityGroup()
        {
            this.AccessibleCustomers = new List<int>();
        }
    }
}