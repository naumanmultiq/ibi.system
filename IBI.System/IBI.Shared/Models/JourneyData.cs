﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.Shared.Models
{
    public class JourneyData
    {
        #region Properties

        public int BusNo
        {
            get;
            set;
        }

        public String Line
        {
            get;
            set;
        }

        public int JourneyID
        {
            get;
            set;
        }

        public String From
        {
            get;
            set;
        }

        public String To
        {
            get;
            set;
        }

        //public StopInfo NextStop
        //{
        //    get;
        //    set;
        //}

        #endregion

        //public JourneyData(String line, int journeyID, String from, String to)
        //{
        //    this.Line = line;
        //    this.JourneyID = journeyID;
        //    this.From = from;
        //    this.To = to;
        //}
    }
}