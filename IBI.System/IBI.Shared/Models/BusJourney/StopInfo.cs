﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Mvc;
using System.ServiceModel;
using IBI.Shared.Annotations;




namespace IBI.Shared.Models.BusJourney
{
    [DataContract(Namespace = "")]
    public class StopInfo
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string JourneyNumber { get; set; }

        [DataMember]
        public string StopName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime PlannedDepartureTime { get; set; }

        //[DataMember(EmitDefaultValue = false)]
        //public DateTime PlannedArrivalTime { get; set; }        

        [DataMember(EmitDefaultValue = false)]        
        public DateTime ArrivalTime { get; set; }
         
        [DataMember(EmitDefaultValue = false)]        
        public DateTime DepartureTime { get; set; }
        
        [DataMember]
        public string Latitude{ get; set; }
        
        [DataMember]
        public string Longitude { get; set; }
        
        [DataMember]
        public string Zone { get; set; }
        
        [DataMember]
        public int PlanDifference { get; set; }
        
        [DataMember]
        public string Status { get; set; }


        
    }
}
