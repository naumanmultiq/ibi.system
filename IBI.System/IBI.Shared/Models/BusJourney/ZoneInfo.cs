﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Mvc;
using System.ServiceModel;
using IBI.Shared.Annotations;




namespace IBI.Shared.Models.BusJourney
{
    [DataContract(Namespace = "")]
    public class ZoneInfo
    {
        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public string Operator { get; set; } 
         
        [DataMember]
        public string KML { get; set; }

        [DataMember]
        public DateTime Timestamp {get; set;   }
         
    }
}
