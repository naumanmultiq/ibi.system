﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Mvc;
using System.ServiceModel;
using IBI.Shared.Annotations;




namespace IBI.Shared.Models.BusJourney
{
    [DataContract(Namespace = "")]
    public class Journey
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string BusNumber { get; set; }

        [DataMember]
        public int CustomerId { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public DateTime JourneyDate { get; set; }

        [DataMember]
        public int JourneyNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime PlannedArrivalTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime PlannedDepartureTime { get; set; }


        [DataMember(EmitDefaultValue=false)]        
        public DateTime? StartTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? EndTime { get; set; }

        [DataMember]
        [DefaultValue("")]
        public String Line { get; set; }

        [DataMember]
        [DefaultValue("")]
        public String StartPoint { get; set; }

        [DataMember]
        [DefaultValue("")]
        public String Destination { get; set; }

        [DataMember]
        [DefaultValue("")]
        public String MoviaSchedule { get; set; }

        [DataMember]
        [DefaultValue("")]
        public String RealTimeData { get; set; }
    }
}
