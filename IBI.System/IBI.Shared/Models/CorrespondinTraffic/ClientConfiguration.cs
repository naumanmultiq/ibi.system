﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBI.Shared.Models.CorrespondingTraffic
{
    public class ClientConfiguration
    {

        public int CustomerId { get; set; }

        public string Key { get; set; }

        public string  Value { get; set; }
    }
}
