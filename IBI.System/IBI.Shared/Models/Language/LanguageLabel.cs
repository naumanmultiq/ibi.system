﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IBI.Shared.Models.Language
{
    [DataContract(Namespace = "")]
    public class LanguageLabel
    {
        public LanguageLabel()
        {
        }

        public LanguageLabel(string label, string title)
        {
            LabelId = label??string.Empty;
            Title = title??string.Empty;
        }

        [XmlAttribute]
        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public string LabelId { get; set; }

        [DataMember]
        public string Title { get; set; }
    }
}
