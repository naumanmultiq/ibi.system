using System;

namespace IBI.Shared.Models.BusTree
{
    /// <summary>
    /// overall detailed status of a particular Unit inside the bus.
    /// </summary>
    public class BusUnitStatus
    {
        public BusUnitStatus() {
            Color = BusStatusColor.NONE;
        }

        public BusUnitStatus(string description) {
            Color = BusStatusColor.NONE;
            Description = description;
        }


        public DateTime LastPing { get; set; }
        public DateTime LastPingUpdated { get; set; }

        public BusStatusColor Color { get; set; }

        public String Description { get; set; }

        public String MacAddress { get; set; }

        public String Link { get; set; }

        public bool MarkedForService { get; set; }

        public bool Exists { get; set; }
    }
}