﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Shared.Models.BusTree
{
    [DataContract]
    [KnownType(typeof(BusNode))]
    [KnownType(typeof(BusStatusColor))]
    [KnownType(typeof(GroupNode))]
    [KnownType(typeof(BusDetail))]
    [KnownType(typeof(BusUnitStatus))]
    public class Tree
    {
        [DataMember]
        public List<BusNode> Groups { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<BusStatusColor> Filters { get; set; }

        [DataMember(EmitDefaultValue=false)]
        public new Dictionary<string, int> Counters { get; set; }

        [DataMember]
        public int UserID { get; set; }

        public Tree(List<BusNode> groups, int userId)
        {
            Groups = groups;
            UserID = userId;
        }

        public Tree(List<BusNode> groups, List<BusStatusColor> filters, Dictionary<string, int> counters, int userId )
        {
            Groups = groups;
            Filters = filters;
            Counters = counters;
            UserID = userId;
        }

        public Tree()
        {
            //do nothing.
            this.Counters = new Dictionary<string, int>();
            this.Counters.Add("GREEN", 0);
            this.Counters.Add("GREY", 0);
            this.Counters.Add("LIGHT_GREY", 0);
            this.Counters.Add("ORANGE", 0);
            this.Counters.Add("RED", 0);
            this.Counters.Add("YELLOW", 0);
            this.Counters.Add("IN_OPERATION", 0);
            this.Counters.Add("INACTIVE", 0);
        }
    }

}
