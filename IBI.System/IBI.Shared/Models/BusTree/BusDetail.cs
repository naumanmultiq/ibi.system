using System.Runtime.Serialization;
using System;
using System.Collections.Generic;

namespace IBI.Shared.Models.BusTree
{
    /// <summary>
    /// Represents a Bus with all its Units. [Doesn't hold the complete bus info, rather just keep the properties which we need at GUI end]
    /// </summary>
    [DataContract]
    [KnownType(typeof(BusUnitStatus))]
    [KnownType(typeof(List<BusUnitStatus>))]
    public class BusDetail
    {
        public BusDetail()
        {
            DCU = new BusUnitStatus("DCU");

            CCU = new BusUnitStatus("CCU");

            VTC = new BusUnitStatus("VTC");
            
            Infotainment = new BusUnitStatus("Infotainment");
            
            Hotspot = new BusUnitStatus("Hotspot");
            
            InService = new BusUnitStatus("In Service");

            Clients = new List<BusUnitStatus>();

            this.BusStatus = BusStatusColor.NONE; 
            LastTimeInService = System.DateTime.MinValue; //.ToString("yyyy-MM-dd hh:mm:ss");
           
        }

        public BusUnitStatus CCU { get; set; }

        public BusUnitStatus DCU { get; set; }

        public BusUnitStatus VTC { get; set; }

        public BusUnitStatus Hotspot { get; set; }

        public BusUnitStatus Infotainment { get; set; }

        public BusUnitStatus InService { get; set; }


        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string BusNumber { get; set; }

        [DataMember]
        public string BusAlias { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public DateTime LastTimeInService { get; set; }

        [DataMember]
        public DateTime LastUpdatedInService { get; set; }
         
        [DataMember]
        public bool ScheduleOk { get; set; }

        [DataMember]
        public bool InOperation { get; set; }
          
        [DataMember]
        public String Remarks { get; set; }

        [DataMember]
        public long CurrentJourney { get; set; }

        [DataMember]
        public long ScheduledJourney { get; set; }

        [DataMember]
        public String LastKnownLocation { get; set; }
         

        [DataMember]
        public String InfotainmentMacAddress { get; set; }
        
        [DataMember]
        public bool IsServiceCase { get; set; }

        [DataMember]
        public int ServiceLevel{ get; set; }

        [DataMember]
        public String ServiceDetail { get; set; }

        [DataMember]
        public bool HasDCU { get; set; }

        [DataMember]
        public bool HasCCU { get; set; }

         
        public BusStatusColor BusStatus {get; set;}

        [DataMember]
        public List<BusUnitStatus> Clients { get; set; }

    }

    
}