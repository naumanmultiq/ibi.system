﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;  


namespace IBI.Shared.Models.Accounts
{
    [DataContract(Namespace = "")]  
    [KnownType(typeof(IBI.Shared.Models.Accounts.User))]
    public class Auth
    {

       
        public Int64 Id { get; set; }

        [DataMember]
        public String AuthToken { get; set; }

        [DataMember]
        public int UserId { get; set; }

        //[DataMember]
        public DateTime CreatedOn { get; set; }

        //[DataMember]
        public DateTime LastAccessed { get; set; }
          
        [DataMember]
        public String Username { get; set; }
 
        [DataMember]
        public String FullName { get; set; }

        [DataMember]
        public String Type { get; set; }
         
        [DataMember]
        public string ErrorMessage { get; set;}

        [DataMember]
        public bool IsValid { get; set; }

        
        public int ApiId { get; set; }

        [DataMember]
        public string ApiKey { get; set; }

        [DataMember]
        public string Applicaiton { get; set; }



        [DataMember(EmitDefaultValue=false)]        
        public User User { get; set; }
    } 
}