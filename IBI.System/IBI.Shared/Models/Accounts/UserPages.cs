﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;  


namespace IBI.Shared.Models.Accounts
{ 

   [DataContract(Namespace = "")] 
    public class UserPage
    {        

        [DataMember]
        public int PageId
        {
            get;
            set;
        }

        [DataMember]
        public int ParentPageId
        {
            get;
            set;
        }

       [DataMember]
        public string ParentPageName
        {
            get;
            set;
        }        

        [DataMember]
        public string Path
        {
            get;
            set;
        }

        [DataMember]
        public string PageName
        {
            get;
            set;
        }


        [DataMember]
        public string MenuInitials
        {
            get;
            set;
        }

        [DataMember]
        public string Controller
        {
            get;
            set;
        }

       [DataMember]
        public string Action
        {
            get;
            set;
        }


       [DataMember]
       public bool ShowMenu
       {
           get;
           set;
       }


        [DataMember]
        public string Access
        {
            get;
            set;
        }



        [DataMember]
        public bool Read
        {
            get;
            set;
        }

        [DataMember]
        public bool Write
        {
            get;
            set;
        }

        [DataMember]
        public bool Delete
        {
            get;
            set;
        }

        [DataMember]
        public bool Update
        {
            get;
            set;
        }

        
    }
}