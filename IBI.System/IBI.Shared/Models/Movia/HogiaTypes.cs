﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Movia.HogiaTypes
{
   
    public class Line
    { 
        public int number { get; set; }
        public string designation { get; set; }
        public string typeCode { get; set; }
    }


    public class Journey
    {
        public string id { get; set; }
        public Line isOnLine { get; set; }
        public int number { get; set; }
        public string destination { get; set; }
        public double score { get; set; }
        public bool isAssigned { get; set; }
        public bool isCanceled { get; set; }
        public string plannedStartDateTime { get; set; }
        public string plannedEndDateTime { get; set; }
    }

    public class MoviaJourney
    {
        public DateTime time { get; set; }
        public string journeyId { get; set; }
        public string journeyKey { get; set; }
        public Line isOnLine { get; set; }  
        public string lineDirection { get; set; }
        public string origin { get; set; }
        //public object destination { get; set; }
        public string[] destination { get; set; }
        public MoviaJourneyStop[] stops { get; set; }
      
    }

    public class MoviaJourneyStop {
        public int stopPointNumber { get; set; }
        public string stopPointName { get; set; }
        public DateTime plannedArrivalDateTime { get; set; }
        public DateTime plannedDepartureDateTime { get; set; }
        public DateTime expectedArrivalDateTime { get; set; }
        public DateTime expectedDepartureDateTime { get; set; }
        public int departureState { get; set; }
        public int arrivalState { get; set; }
        public int zoneNumber { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public DateTime departureTime { get; set; }
        public DateTime arrivalTime { get; set; }
    }

    public class MoviaDestination {
        public string name { get; set; }
    }
}


/*
 "time": "2015-10-02T12:13:45.5592253+02:00",
"journeyId": 1310507832772642,
"journeyKey": "20151002L0201J0095",
"isOnLine": 
{

    "number": 201,
    "designation": "201A",
    "typeCode": "ABUS"

},
"lineDirection": "Trekroner St. - Svogerslev",
"origin": "Trekroner st.",
"destination": 
[

    "Svogerslev"

],
"stops": 
[

{

    "stopPointNumber": 4359,
    "stopPointName": "Trekroner St.",
    "plannedArrivalDateTime": "2015-10-02T11:42:00",
    "plannedDepartureDateTime": "2015-10-02T11:42:00",
    "expectedArrivalDateTime": "2015-10-02T11:42:00",
    "expectedDepartureDateTime": "2015-10-02T11:42:00",
    "departureState": 9,
    "arrivalState": 11,
    "zoneNumber": 8,
    "latitude": 55.64834780044444,
    "longitude": 12.131920414432315

},
 
 */

