﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Shared.Models.Schedules
{
    [DataContract]
    public class DestListModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? ScheduleId { get; set; }

        [DataMember]
        public string Special { get; set; }

        [DataMember]
        public string Line { get; set; }

        [DataMember]
        public string Destination { get; set; }

        [DataMember]
        public string From { get; set; }

        [DataMember]
        public string Via { get; set; }

        [DataMember]
        public int StopCount { get; set; }

        [DataMember]
        public List<string> Groups { get; set; }

        [DataMember]
        public string LineGroup { get; set; }

        [DataMember]
        public string DestinationGroup { get; set; }

        [DataMember]
        public string FromGroup { get; set; }

        [DataMember]
        public string ViaGroup { get; set; }
        
        [DataMember]
        public bool IsSpecial { get; set; }

        [DataMember]
        public string SelectionStructure { get; set; }

        [DataMember]
        public string NodeText { get; set; }
        
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public int Parts { get; set; }

        [DataMember]
        public bool MakeGroup { get; set; }


        public class Comparer : IEqualityComparer<DestListModel>
        {
            public bool Equals(DestListModel x, DestListModel y)
            {
                return x.Line == y.Line && x.Destination == y.Destination && x.Via == y.Via;
            }

            public int GetHashCode(DestListModel obj)
            {
                return (obj.StopCount).GetHashCode();
            }
        }
    }

}
