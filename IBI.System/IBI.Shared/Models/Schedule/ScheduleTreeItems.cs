﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.ScheduleTreeItems
{

    [DataContract]
    public class Schedule
    {

        //basic properties from interface --> restricted to keep naming convention bcoz of JQuery Tree internal naming structure.

        [DataMember(Order = 1)]
        public string title { get; set; }

        [DataMember(Order = 2)]
        public bool isFolder { get; set; }

        [DataMember(Order = 3)]
        public bool isLazy { get; set; }
        
        [DataMember(Order = 4)]
        public string key { get; set; }

        [DataMember(Order = 5)]
        public bool expand { get; set; }
                
        [DataMember(Order = 6)]
        public List<Schedule> children { get; set; }

        //additional properties

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int ScheduleId { get; set; }
        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public string Line { get; set; }
        [DataMember]
        public string FromName { get; set; }
        [DataMember]
        public string Destination { get; set; }
        [DataMember]
        public string ViaName { get; set; }
        [DataMember]
        public DateTime? LastUpdated { get; set; }
        [DataMember]
        public List<ScheduleStop> ScheduleStops { get; set; }

        [DataMember]
        public String AlternateRowCss { get; set; }
        [DataMember]
        public String ClientInformation { get; set; }

        private static int rowNum; 


        public Schedule()
        {
            ScheduleStops = new List<ScheduleStop>();
         
        }

        private static int _keyId = 1;
        public Schedule(int scheduleId, int customerId, string line, string fromName, string destination, string viaName, DateTime? lastUpdated, List<ScheduleStop> stops )
        {

            this.ScheduleId = scheduleId;
            this.Name = scheduleId.ToString();
            this.CustomerId = customerId; 
            this.Line = line;
            this.FromName = fromName;
            this.Destination = destination;
            this.ViaName = viaName;
            this.LastUpdated = lastUpdated;
            this.ScheduleStops = stops;

            this.key = (_keyId++).ToString(); 

            SetNode();
        }

        //this function needs to be changed if any requirements changes for Tree GUI.
        public void SetNode()
        {
          
            this.Line = this.Line ?? ""; 
            this.FromName = this.FromName ?? "";
            this.Destination = this.Destination ?? "";
            this.ViaName = this.ViaName ?? ""; 

            int type = 1;

            //KHI: isFolder needed?
            if (this.isFolder)
                type = 1;
            else
                type = 2;

            this.expand = true;

            this.Name = this.ScheduleId.ToString();
            
            //set alternate rowCSS

            if (type == 2)
            {
                this.AlternateRowCss = rowNum % 2 == 0 ? " grey" : " white";
                //increment rowNum
                rowNum++;
            }
            else { this.AlternateRowCss = " emptyRow"; }
             
                this.title = this.Name + "~"  + this.Line + "~" + this.FromName +"~" + this.Destination + "~" + this.ViaName + "~" + GetEditLink() + "~" + GetViewImageLink() + "~" + GetDownloadLink();
            
        }

        private string GetEditLink()
        {
            return "[EDIT]";
        }

        private string GetDownloadLink()
        {
            return "[DOWNLOAD]";
        }

        private string GetViewImageLink()
        {
            return "[VIEW]";
        }

        private string GetStatusImage(bool status)
        {
            return status ? "[ACTIVE]" : "[INACTIVE]"; 
        }

        private string GetSortImages()
        {
            return "[SORT]"; 
        } 
        public bool Equals(Schedule other)
        {
           return true;
           /* return this.DestinationName == other.DestinationName &&
             this.ParentGroupName == other.ParentGroupName &&
              this.SelectionStructure == other.SelectionStructure &&
             this.Line == other.Line &&
             (other.IsGroup == true || this.Text == other.Text)
             ;
          */
        }

    }

    [DataContract]
    public class ScheduleStop
    {
        [DataMember]
        public int StopSequence { get; set; }
        [DataMember]
        public string StopName { get; set; }
        [DataMember]
        public decimal StopId { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Zone { get; set; }

    }

    [DataContract]
    [KnownType(typeof(Schedule))]
    public class ScheduleTree
    { 
        [DataMember]
        public List<Schedule> Schedules { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public int CustomerID { get; set; }

        public ScheduleTree()
        {

        }

        public ScheduleTree(int userId)
        {
            this.UserID = userId;
        }
    }
}
