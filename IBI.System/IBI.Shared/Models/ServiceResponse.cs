﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models
{
    [DataContract]
    public class ServiceResponse<T>
    {
        //private T _data;

        [DataMember()]
        public T Data { get; set; }

        [DataMember()]
        public bool Status { get; set; }

        [DataMember()]
        public string Message { get; set; }


        public ServiceResponse() { }

        public ServiceResponse(ref T data)
        {
            this.Data = data;
        }
    }
}
