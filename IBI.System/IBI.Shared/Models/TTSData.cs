﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class TTSData
    {
        private AudioFile[] files;

        [DataMember(Order = 0)]
        public AudioFile[] Files
        {
            get { return files; }
            set { files = value; }
        }
    }
}