﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Hotspot
{
    [DataContract]
    public class Hotspot
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string HotspotID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SSID { get; set; }
        [DataMember]
        public string LandingPage { get; set; }
        [DataMember]
        public string Theme { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public bool RegistrationRequired { get; set; }
        [DataMember]
        public string StartPage { get; set; }
        [DataMember]
        public Nullable<int> DailyUserUsageLimitMB { get; set; }
        [DataMember]
        public Nullable<bool> UsageLoggingEnabled { get; set; }
        [DataMember]
        public List<AccessPointGroup> AccessPointGroups { get; set; }

    }

    [DataContract(Namespace = "")]
    public class AccessPointGroup
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SSID { get; set; }
        [DataMember]
        public string LandingPage { get; set; }
        [DataMember]
        public string Theme { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public int Hotspot_ID { get; set; }
        [DataMember]
        public string StartPage { get; set; }
        [DataMember]
        public List<AccessPoint> AccessPoints { get; set; }
        [DataMember]
        public Hotspot Hotspot { get; set; }
        [DataMember]
        public int TotalAccessPoints { get; set; }
    }

    [DataContract(Namespace = "")]
    public class AccessPoint
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string MAC { get; set; }
        [DataMember]
        public bool Enabled { get; set; }
        [DataMember]
        public string SSID { get; set; }
        [DataMember]
        public string LandingPage { get; set; }
        [DataMember]
        public string Theme { get; set; }
        [DataMember]
        public string GlobalIP { get; set; }
        [DataMember]
        public string PendingCommand { get; set; }
        [DataMember]
        public System.DateTime LastPing { get; set; }
        [DataMember]
        public System.DateTime LastActivity { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public int AccessPointGroup_ID { get; set; }
        [DataMember]
        public string StartPage { get; set; }
        

        [DataMember]
        public AccessPointGroup AccessPointGroup { get; set; }        
        [DataMember]
        public string AccessPointGroupName { get; set; }
        [DataMember]
        public int HotspotId { get; set; }
        [DataMember]
        public string HotspotName { get; set; }
        [DataMember]
        public AccessPointUsage TodayUsage { get; set; }
    }

    [DataContract(Namespace = "")]
    public class AccessPointDailyActivity
    {
        [DataMember]
        public int? AccessPoint_Id { get; set; }
        [DataMember]
        public DateTime? Date { get; set; }
        [DataMember]
        public long? BytesSent { get; set; }
        [DataMember]
        public long? BytesReceived { get; set; }
        [DataMember]
        public int? SuccessfulLogins { get; set; }
        [DataMember]
        public int? FailedLogins { get; set; }
        [DataMember]
        public int? ActivityTime { get; set; }
        
    }

    [DataContract(Namespace = "")]
    public class AccessPointUsage
    {
        [DataMember]
        public long? BytesSent { get; set; }
        [DataMember]        
        public long? BytesReceived { get; set; }
        
    }
}
