﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace IBI.Shared.Models
{
    [DataContract]
    public class Configuration
    {
        [DataMember]
        public int ConfgurationPropertyId { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        [DataMember]
        public string DefaultValue { get; set; }
    }
}
