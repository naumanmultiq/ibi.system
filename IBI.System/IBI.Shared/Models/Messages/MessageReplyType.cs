﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;

namespace IBI.Shared.Models.Messages
{
    [DataContract(Namespace = "")]  
    [KnownType(typeof(IBI.Shared.Models.Messages.Message))]
    public class MessageReplyType
    {
        [DataMember]
        public int MessageReplyTypeId { get; set; }

        [DataMember]
        public string MessageReplyTypeName { get; set; }

        [DataMember]
        public string ReplyValues { get; set; }

    }
}
