﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Mvc;
using System.ServiceModel;
using IBI.Shared.Annotations;




namespace IBI.Shared.Models.Messages
{
    [DataContract(Namespace="")]
    [MatchDateRange("ValidFrom", "ValidTo", ErrorMessage = "Valid-To must be greater than Valid-from")]
    public class Message
    {
        [DataMember]
        [ScaffoldColumn(true)]
        [HiddenInput(DisplayValue = false)]
        public int MessageId { get; set; }

        //[DataMember]
        //[HiddenInput(DisplayValue = false)]
        //public string BusNumber { get; set; }
        
        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public string DefaultBus { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int CustomerId { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public string CustomerName { get; set; }

        [DataMember]
        [DisplayName("Category")]
        [Required(ErrorMessage = "Message category required")]
        public int? MessageCategoryId { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public string CategoryName { get; set; }

        [DataMember]
        [DisplayName("Title")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Title required")]
        public string Subject { get; set; }

        [DataMember]
        [DisplayName("Message Text")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Message text required")]
        public string MessageText { get; set; }
          
        [DataMember]
        [DisplayName("Valid From")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Valid-From date required")]
        public DateTime ValidFrom { get; set; }

        [DataMember]
        [DisplayName("Valid To")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Valid-To date required")]
        public DateTime ValidTo { get; set; }

        [DataMember(EmitDefaultValue=false)]
        [HiddenInput(DisplayValue = false)]
        public int? SenderId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [HiddenInput(DisplayValue = false)]
        public string SenderName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [HiddenInput(DisplayValue = false)]
        public int? LastModifiedBy { get; set; }


        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public DateTime? DateAdded { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public DateTime? DateModified { get; set; }


        [DataMember(EmitDefaultValue=false)]
        [DisplayName("Group")]
        public string GroupName { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [DisplayName("Group")]        
        public int GroupId { get; set; }
                

        [DataMember]
        [DefaultValue(false)]
        public bool IsArchived { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [HiddenInput(DisplayValue = false)]
        public List<String> Buses
        {
            get;
            set;
        }

        [DataMember(EmitDefaultValue = false)]
        [HiddenInput(DisplayValue = false)]
        public List<String> BusesAlias
        {
            get;
            set;
        }

        public IEnumerable<IBI.DataAccess.DBModels.Bus> DbBuses
        {
            set 
            {
                this.Buses = value.Select(b => b.BusNumber).ToList();
            }
        }

        [DataMember(EmitDefaultValue = false)]
        [HiddenInput(DisplayValue = false)]
        public bool Editable { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? MessageReplyTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MessageReplyTypeName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReplyValues { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool ShowAsPopup { get; set; }
    }
}
