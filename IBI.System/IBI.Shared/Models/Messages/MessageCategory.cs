﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;

namespace IBI.Shared.Models.Messages
{
    [DataContract(Namespace = "")]  
    [KnownType(typeof(IBI.Shared.Models.Messages.Message))]
    public class MessageCategory
    {
        [DataMember]
        public int MessageCategoryId { get; set; }

        [DataMember]
        public string CategoryName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? Validity { get; set; }

        [DataMember]
        public string ValidityRule { get; set; }

        [DataMember]
        public string ValidityValue { get; set; }


        [DataMember]
        public DateTime? DateAdded { get; set; }

        [DataMember]
        public DateTime? DateModified { get; set; }

        [DataMember]
        public virtual List<Message> Messages {get; set;}
    }
}
