﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Converters;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using IBI.Shared.Diagnostics;


namespace IBI.Shared.Common
{
    public enum PostFormat
    {
        Json = 1,
        Xml = 2
    }


    public class RESTBase
    {

        //For REST -> GET call only
        public virtual T Get<T>(string url)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            // webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows; Windows NT 5.1; rv:1.9.2.4) Gecko/20100611 Firefox/3.6.4");
            //webClient.Headers.Add("Accept-Encoding", "");

            string result = string.Empty;

            try
            {
                //result = webClient.DownloadString(url);
                byte[] data = webClient.DownloadData(url);
                result = System.Text.Encoding.UTF8.GetString(data);
            }
            catch (Exception e)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.RestManager, string.Format("Communication errors with endpoint '{0}'. Error: {1}", url, Logger.GetDetailedError(e)));
                throw e;
            }
            //T retObj = JsonConvert.DeserializeObject<T>(result);
            T retObj = JsonConvert.DeserializeObject<T>(result);

            return retObj;
        }

        public virtual string Post(string url, byte[] data)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);


            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            string responseString = string.Empty;

            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                responseString = "SUCCESS|" + new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.RestManager, string.Format("Communication errors with endpoint '{0}'. Error: {1}", url, Logger.GetDetailedError(e)));
                    responseString = "FAILURE|Errors while communicating with the server";
                    return responseString;
                }

                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    //Console.WriteLine("Error code: {0}", httpResponse.StatusCode );
                    using (Stream sdata = response.GetResponseStream())
                    using (var reader = new StreamReader(sdata))
                    {
                        responseString = reader.ReadToEnd();
                    }
                }

                responseString = "FAILURE|" + responseString;
            }

            return responseString;
        }

        //For REST -> GET call only
        public virtual string GetString(string url)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            // webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows; Windows NT 5.1; rv:1.9.2.4) Gecko/20100611 Firefox/3.6.4");
            //webClient.Headers.Add("Accept-Encoding", "");


            string responseString = string.Empty;
            string result = string.Empty;

            try
            {
                //result = webClient.DownloadString(url);
                byte[] data = webClient.DownloadData(url);
                result = System.Text.Encoding.UTF8.GetString(data);
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.RestManager, string.Format("Communication errors with endpoint '{0}'. Error: {1}", url, Logger.GetDetailedError(e)));
                    responseString = "Errors while communicating with the server";
                    return responseString;
                }

                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    //Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream sdata = response.GetResponseStream())
                    using (var reader = new StreamReader(sdata))
                    {
                        responseString = reader.ReadToEnd();
                    }
                }

                result = responseString;
            }

            return result;
        }

        public virtual string PostString(string url, byte[] data)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);


            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            string responseString = string.Empty;

            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.RestManager, string.Format("Communication errors with endpoint '{0}'. Error: {1}", url, Logger.GetDetailedError(e)));
                    responseString = "FAILURE|Errors while communicating with the server";
                    return responseString;
                }

                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    //Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream sdata = response.GetResponseStream())
                    using (var reader = new StreamReader(sdata))
                    {
                        responseString = reader.ReadToEnd();
                    }
                }
            }

            return responseString;
        }
    }

    //Sample uriTemplate = bustree/" + uid
    public class RESTManager : RESTBase
    {

        public Uri BasePath
        {
            get;
            set;
            //get
            //{
            //    return new Uri(System.Configuration.ConfigurationSettings.AppSettings["HogiaBasePath"].ToString());
            //}
        }

        public string GetString(string uriTemplate)
        {
            string url = BasePath.AbsoluteUri + uriTemplate;
            return base.GetString(url);
        }

        public override string PostString(string uriTemplate, byte[] data)
        {
            string url = BasePath.AbsoluteUri + uriTemplate;
            return base.PostString(url, data);
        }

        public override T Get<T>(string uriTemplate)
        {
            string url = BasePath.AbsoluteUri + uriTemplate;
            return base.Get<T>(url);
        }


        //public bool Post(string uriTemplate, string content) {
        //    string url = BasePath.AbsoluteUri + uriTemplate;
        //    //string data = JsonConvert.SerializeObject(content);
        //    return base.Post<string>(url, data, PostFormat.Json);
        //}

        public override string Post(string uriTemplate, byte[] data)
        {
            string url = BasePath.AbsoluteUri + uriTemplate;

            //string data = "";
            //switch (postFormat)
            //{
            //    case PostFormat.Json:
            //        {
            //            data = JsonConvert.SerializeObject(content, new IsoDateTimeConverter());
            //            break;
            //        }
            //    case PostFormat.Xml:
            //        {

            //            //data = SerializeInXML<Message>(postData);
            //            data = XmlUtility.Serialize<TData>(content);
            //            break;
            //        }
            //}

            return base.Post(url, data);
        }

        public static RESTManager Instance
        {
            get
            {
                return new RESTManager();
            }
        }

        //public static Auth Validate(string authToken)
        //{
        //    return Instance.Get<Auth>(UserAuthService, "Json/Validate?authToken=" + authToken);
        //}

        ////not in use .. just for testing.
        //public static bool SaveMessage(Message msg){
        //    return Instance.Post<bool, Message>(MessageService, "XML/SaveMessage", msg, PostFormat.Xml);
        //} 

        #region Helper Functions

        #endregion
    }

}