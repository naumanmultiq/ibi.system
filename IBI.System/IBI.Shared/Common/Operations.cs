﻿using IBI.DataAccess.DBModels;
using IBI.Shared.Common.Types;
using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Common
{
    public class Operations
    {
        
         public static void UpdateDataStatus(APIDataKeys dataKey, int customerId, int busNumber = 0 , string value = "")
        {
             try
             {
                
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    string key = dataKey.ToString();
                    var status = dbContext.DataUpdateStatuses.Where(d=>d.CustomerId == customerId && d.BusNumber == busNumber && d.DataKey == key).FirstOrDefault();

                    bool newRec = false;
                    if(status==null)
                    {
                        newRec = true;
                        status = new DataAccess.DBModels.DataUpdateStatus();

                        status.CustomerId = customerId;
                        status.BusNumber = busNumber;
                        status.DataKey = dataKey.ToString();
                    }

                
                        status.Timestamp = DateTime.Now;

                        if(!String.IsNullOrEmpty(value))
                            status.Value = value;
                                        
              
                    if(newRec)
                    {
                        dbContext.DataUpdateStatuses.Add(status);
                        
                    }

                    dbContext.SaveChanges();
                }   
             }
             catch(Exception ex)
             {
                //do nothing
                 Logger.AppendToSystemLog( Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
             }
        }
    }
}
