﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Common.Types
{
    public enum APIDataKeys
    {
        SIGNDATA,
        LAND2BUS_MESSAGES,
        OFFLINE_SCHEDULES,
        AUDIO_FILES,
        DESTINATION_LIST,
        SIGN_ADDON_LIST
    }

    public enum JourneyStates
    {
        FUTURE = 0,
        ACTIVE = 1,
        FINISHED_NOT_CLOSED = 2,
        FINISHED_NOT_DRIVEN = 3,
        FINISHED_DRIVEN = 4
    }


    public class IBIKeyValuePair
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public IBIKeyValuePair() 
        { //do nothing
        }
        public IBIKeyValuePair(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

    }
}
