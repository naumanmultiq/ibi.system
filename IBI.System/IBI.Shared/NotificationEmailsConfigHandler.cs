﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace IBI.Shared
{
    public class NotificationEmailsConfigHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            return NotificationEmailsConfig.ParseXml(section);
        }
    }
    public class NotificationEmailsConfig
    {
        public XDocument Xdoc { get; set; }
        public static NotificationEmailsConfig ParseXml(XmlNode section)
        {
            NotificationEmailsConfig config = new NotificationEmailsConfig();
            config.Xdoc = XDocument.Parse(section.OuterXml.ToString());
            return config;
        }
    }
}