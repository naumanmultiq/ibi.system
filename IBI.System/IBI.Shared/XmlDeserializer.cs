﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Xml;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared
{
    public class XmlDeserializer
    {
        public static Object deserialize(Type type, XmlElement source)
        {
            // Create result type
            Object result = Activator.CreateInstance(type);

            // Investigate all properties of the given type
            PropertyDescriptorCollection src_pdc = TypeDescriptor.GetProperties(result);

            foreach (PropertyDescriptor pd in src_pdc)
            {
                // Check whether the property has a DataMemberAttribute; otherwise ignore it
                DataMemberAttribute dma = pd.Attributes[typeof(DataMemberAttribute)] as DataMemberAttribute;

                if (dma != null)
                {
                    Object value = null;
                    XmlElement propertyElement = null;

                    // Check whether to look for the value in a childnode or an attribute
                    XmlAttributeAttribute xaa = pd.Attributes[typeof(XmlAttributeAttribute)] as XmlAttributeAttribute;

                    if (xaa != null) // Value is stored in an attribute
                    {
                        XmlAttribute valueElement = source.Attributes.GetNamedItem(pd.Name) as XmlAttribute;

                        if (valueElement != null)
                            value = convertToType(pd.PropertyType, valueElement.Value);
                    }
                    else // Value is stored in a childnode
                    {
                        XmlNode valueElement = source.SelectSingleNode(pd.Name);
                        propertyElement = (XmlElement)valueElement;

                        if (valueElement != null)
                            value = convertToType(pd.PropertyType, valueElement.InnerText);
                    }

                    // Treat Arrays special
                    if (!pd.PropertyType.IsArray)
                    {
                        // Check whether the property is a simple type or it needs to be deserialized further
                        DataContractAttribute dca = TypeDescriptor.GetAttributes(pd.PropertyType)[typeof(DataContractAttribute)] as DataContractAttribute;

                        if (dca == null) // Simple property
                            pd.SetValue(result, value);
                        else if (propertyElement != null) // Serializable property
                            pd.SetValue(result, XmlDeserializer.deserialize(pd.PropertyType, propertyElement));
                    }
                    else
                    {
                        // Deserialize the entire array
                        if (propertyElement != null)
                            pd.SetValue(result, deserializeArray(pd.PropertyType.GetElementType(), propertyElement));
                    }
                }
            }

            return result;
        }

        private static Object deserializeArray(Type type, XmlElement source)
        {
            ArrayList result = new ArrayList();

            foreach (XmlNode elementNode in source.ChildNodes)
            {
                Object entry = deserialize(type, elementNode as XmlElement);

                if (entry != null)
                    result.Add(entry);
            }

            return result.ToArray(type);
        }

        private static Object convertToType(Type type, String value)
        {
            if (type.FullName == typeof(Int32).FullName)
                return Int32.Parse(value);
            else if (type.FullName == typeof(Int64).FullName)
                return Int64.Parse(value);
            else if (type.FullName == typeof(DateTime).FullName)
                return XmlConvert.ToDateTime(value);
            else if (type.FullName == typeof(Boolean).FullName)
                return Boolean.Parse(value);

            return value;
        }
    }
}