﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace IBI.Shared.ConfigSections
{

    public class ScheduleRewriterDefinition
    {
        public List<VdvSchedule> Schedules { get; set; }
        public ScheduleRewriterDefinition() {
             
        }
         
        public ScheduleRewriterDefinition(string rewriterDefinitionXML)
        { 
            this.Schedules = new List<VdvSchedule>();

            if (String.IsNullOrEmpty(rewriterDefinitionXML))
                return;
            
            XDocument xDoc = XDocument.Parse(rewriterDefinitionXML);
            
                var schedules = (from e in xDoc.Descendants("Schedule")
                                 select e);


                foreach (var schedule in schedules)
                {
                    string l, d, rd, rv;
                    string[] c = null;
                    string[] m = null;


                    l = schedule.Attribute("line").Value;
                    d = schedule.Attribute("destination").Value;
                    

                    var rdNode = schedule.Descendants("RewriteDestination").FirstOrDefault();
                    var rvNode = schedule.Descendants("RewriteVia").FirstOrDefault();

                    rd = rdNode != null ? rdNode.Value : "";
                    rv = rvNode != null ? rvNode.Value : "";

                    var cNode = schedule.Descendants("ContainsStops").FirstOrDefault();
                    if (cNode != null)
                    {
                        c = Array.ConvertAll(cNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
                    }

                    var mNode = schedule.Descendants("MissingStops").FirstOrDefault();
                    if (mNode != null)
                    {
                        m = Array.ConvertAll(mNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
                    }

                    this.Schedules.Add(new VdvSchedule(l, d, rd, rv, c, m));
                }

                xDoc = null;

        }

        public ScheduleRewriterDefinition(List<VdvSchedule> schedules)
        {
            this.Schedules = schedules;
        }

        public static List<VdvSchedule> ReadAllConfigurations()
        {
            ScheduleRewriterConfig config = (IBI.Shared.ConfigSections.ScheduleRewriterConfig)ConfigurationManager.GetSection("ScheduleRewriteDefinitions");

            List<VdvSchedule> data = new List<VdvSchedule>();

            //here should be a null check to avoid exception in case of missing config
            if (config.Xdoc == null)
                return data;

            //XDocument xDoc = XDocument.Parse(rewriterDefinitionXML);

            var schedules = (from e in config.Xdoc.Descendants("Schedule")
                             select e);


            foreach (var schedule in schedules)
            {
                string l, d, rd, rv;
                string[] c = null;
                string[] m = null;


                l = schedule.Attribute("line").Value;
                d = schedule.Attribute("destination").Value;


                var rdNode = schedule.Descendants("RewriteDestination").FirstOrDefault();
                var rvNode = schedule.Descendants("RewriteVia").FirstOrDefault();

                rd = rdNode != null ? rdNode.Value : "";
                rv = rvNode != null ? rvNode.Value : "";

                var cNode = schedule.Descendants("ContainsStops").FirstOrDefault();
                if (cNode != null)
                {
                    c = Array.ConvertAll(cNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
                }

                var mNode = schedule.Descendants("MissingStops").FirstOrDefault();
                if (mNode != null)
                {
                    m = Array.ConvertAll(mNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
                }

                data.Add(new VdvSchedule(l, d, rd, rv, c, m));
            }

            //config.Xdoc = null;
            return data;
            
        }
    }

    public class VdvSchedule
    {
        public string Line { get; set; }
        public string Destination { get; set; }
        public string RewriteDestination { get; set; }
        public string RewriteVia { get; set; }

        public string[] ContainsStops { get; set; }
        public string[] MissingStops { get; set; }

        public VdvSchedule() { }

        public VdvSchedule(string line, string destination, string rewriteDestination, string rewriteVia, string[] containsStops, string[] missingStops)
        {
            this.Line = line;
            this.Destination = destination;
            this.RewriteDestination = rewriteDestination;
            this.RewriteVia = rewriteVia;
            this.ContainsStops = containsStops;
            this.MissingStops = missingStops;
        }
    }

    public class VdvCustomerConfiguration{
       public int CustomerId {get; set;}
        public bool Active {get; set;}
        public bool SaveDataInDB { get; set; } 
        public string[] LineFilter { get; set; }
        public string[] DepartmentFilter { get; set; }        
        public ScheduleRewriterDefinition ScheduleRewriteDefinitions { get; set; }

    }

    public class VdvSubscriptionConfiguration
    {
        public bool Active { get; set; }
        public string Sender { get; set; }
        public string ProductKey { get; set; }
        public Uri ServerAddress { get; set; }
        public string ServerLogin { get; set; }
        public string SubscriptionUrl { get; set; }
        public string PollDataUrl { get; set; }
        public string ServerStatusUrl { get; set; }
        public string DataReadyStatusUrl { get; set; }
        public int JourneyTimerInterval { get; set; }
        public string NotificationEmail { get; set; }
        public int MaxTimeToWaitForDataReady { get; set; }
        public int FutureMinutes { get; set; }
        public int FutureDaysBuffer { get; set; }
        public string TimetablePackageDate { get; set; }
        public int SubscriptionValidityMinutes { get; set; }
        public bool SaveDataInFiles { get; set; }
        public string ExternalReferencePrefix { get; set; }

        public VdvCustomerConfiguration[] Customers {get; set;}

        public VdvSubscriptionConfiguration() { }

        public static VdvSubscriptionConfiguration[] ReadAllConfigurations()
        {
            VdvJourneyImporterConfig config = (IBI.Shared.ConfigSections.VdvJourneyImporterConfig)ConfigurationManager.GetSection("VdvJourneyImporterConfigurations");
            var subConfigs = (from e in config.Xdoc.Descendants("Subscription")
                                  where e.Attribute("active").Value == "true"
                                  select e);
           
            VdvSubscriptionConfiguration[] subConfigurations = subConfigs.Select(c => new VdvSubscriptionConfiguration
            { 
                Active = true,
                Sender = c.Descendants("Sender").FirstOrDefault().Value,
                ProductKey = c.Attribute("productKey") != null ? c.Attribute("productKey").Value : "",
                ServerAddress = new Uri(c.Descendants("ServerAddress").FirstOrDefault().Value),
                ServerLogin = c.Descendants("ServerLogin").Any()?c.Descendants("ServerLogin").FirstOrDefault().Value:"",
                SubscriptionUrl = c.Descendants("SubscriptionUrl").FirstOrDefault().Value,
                PollDataUrl = c.Descendants("PollDataUrl").FirstOrDefault().Value,
                ServerStatusUrl = c.Descendants("ServerStatusUrl").FirstOrDefault().Value,
                DataReadyStatusUrl = c.Descendants("DataReadyStatusUrl").FirstOrDefault().Value,                
                JourneyTimerInterval = int.Parse(c.Descendants("JourneyTimerInterval").FirstOrDefault().Value),
                NotificationEmail = c.Descendants("NotificationEmail").FirstOrDefault().Value,
                MaxTimeToWaitForDataReady = int.Parse(c.Descendants("MaxTimeToWaitForDataReady").FirstOrDefault().Value), 
                FutureMinutes = int.Parse(c.Descendants("FutureMinutes").FirstOrDefault().Value),
                SubscriptionValidityMinutes = int.Parse(c.Descendants("SubscriptionValidityMinutes").FirstOrDefault().Value),
                FutureDaysBuffer = int.Parse(c.Descendants("FutureDaysBuffer").FirstOrDefault().Value),
                ExternalReferencePrefix = c.Descendants("ExternalReferencePrefix").FirstOrDefault().Value,                
                SaveDataInFiles = bool.Parse(c.Descendants("SaveDataInFiles").FirstOrDefault().Value),
                TimetablePackageDate = c.Descendants("TimetablePackageDate").Any()?c.Descendants("TimetablePackageDate").FirstOrDefault().Value:"",                
                Customers = GetCustomerConfiguration(c.Descendants("Customers").FirstOrDefault())             
            }).ToArray();

            return subConfigurations;
        }

        private static VdvCustomerConfiguration[] GetCustomerConfiguration(XElement customers)
        {
            var custConfigs = (from e in customers.Descendants("Customer")
                                  where e.Attribute("active").Value == "true"
                                  select e);

            VdvCustomerConfiguration[] customerConfigurations = custConfigs.Select(c => new VdvCustomerConfiguration
            {
                Active = true,
                CustomerId = int.Parse(c.Attribute("customerId").Value),
                SaveDataInDB = bool.Parse(c.Descendants("SaveDataInDB").FirstOrDefault().Value),
                LineFilter = c.Descendants("LineFilter").FirstOrDefault().Value.ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray(),
                DepartmentFilter = c.Descendants("DepartmentFilter").Any()?c.Descendants("DepartmentFilter").FirstOrDefault().Value.ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim().ToUpper()).ToArray():null,
                ScheduleRewriteDefinitions = new ScheduleRewriterDefinition(c.Descendants("ScheduleRewriteDefinitions").FirstOrDefault().ToString()) 

            }).ToArray();

            return customerConfigurations;
        }
    }


    public class VdvJourneyImporterConfigHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            return VdvJourneyImporterConfig.ParseXml(section);
        }
    }
    public class VdvJourneyImporterConfig
    {
        public XDocument Xdoc { get; set; }
        public static VdvJourneyImporterConfig ParseXml(XmlNode section)
        {
            VdvJourneyImporterConfig config = new VdvJourneyImporterConfig();
            config.Xdoc = XDocument.Parse(section.OuterXml.ToString());
            return config;
        }
    }

    public class ScheduleRewriterConfigHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            return ScheduleRewriterConfig.ParseXml(section);
        }
    }
    public class ScheduleRewriterConfig
    {
        public XDocument Xdoc { get; set; }
        public static ScheduleRewriterConfig ParseXml(XmlNode section)
        {
            ScheduleRewriterConfig config = new ScheduleRewriterConfig();
            config.Xdoc = XDocument.Parse(section.OuterXml.ToString());
            return config;
        }
    }

}