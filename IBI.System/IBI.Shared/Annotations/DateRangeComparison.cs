﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace IBI.Shared.Annotations
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MatchDateRange : ValidationAttribute
    {
        public String SourceField { get; set; }
        public String MatchField { get; set; }

        public MatchDateRange(object source, object match)
        {
            SourceField = source.ToString();
            MatchField = match.ToString();
        }

        public override Boolean IsValid(Object value)
        {
            Type objectType = value.GetType();

            PropertyInfo[] properties = objectType.GetProperties();

            DateTime sourceValue = new DateTime();
            DateTime matchValue = new DateTime();

            Type sourceType = null;
            Type matchType = null;

            int counter = 0;

            foreach (PropertyInfo propertyInfo in properties)
            {
                if (propertyInfo.Name ==  SourceField || propertyInfo.Name == MatchField )
                {

                    if (counter == 0)
                    {
                        sourceValue = DateTime.Parse(propertyInfo.GetValue(value, null).ToString());
                        sourceType = propertyInfo.GetValue(value, null).GetType();
                    }
                    if (counter == 1)
                    {
                        matchValue = DateTime.Parse(propertyInfo.GetValue(value, null).ToString());
                        matchType = propertyInfo.GetValue(value, null).GetType();
                    }
                    counter++;
                    if (counter == 2)
                    {
                        break;
                    }
                }
            }

            if (sourceType != null && matchType != null)
            {
                //if (Convert.ChangeType(sourceValue, sourceType) <= Convert.ChangeType(matchValue, matchType))
                if(sourceValue < matchValue)
                    return true;
            }
            return false;
        }
    }
}