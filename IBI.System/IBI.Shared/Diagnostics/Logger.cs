﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using IBI.DataAccess.DBModels;

namespace IBI.Shared.Diagnostics
{
    public class Logger
    {
        #region Enums

        public enum EntryTypes
        {
            Error,
            Warning,
            Information
        }

        public enum EntryCategories
        {
            MoviaWS,
            IBI,
            IBIData,
            Upload,
            OIORest,
            MoviaWSData,
            Novo,
            IBI_SERVER,
            SYNC_SCHEDULE,
            LOG_PROCESSING,
            TTS,
            DEPARTURE_BOARD,
            SIGNMANAGEMENT,
            JOURNEY,
            CORRESPONDING_TRAFFIC,
            USER_MGMT,
            Bus2Land,
            Land2Bus,
            MoviaHogia,
            RestManager,
            SignQueueProcessor,
            InServiceStatus,
            SignAddonManagement,
            SCHEDULE_TREE,
            JOURNEY_PREDICTION,
            HOTSPOT,
            JOURNEY_PREDICTION_QUEUE,
            MoviaHogiaAssignment,
            BUS_OPERATIONS,
            MESSAGE_QUEUE,
            INSERVICE_SYNC
        }

        #endregion

        public static void LogError(Exception ex)
        {
            //LogMessage(GetDetailedError(ex));

        }

        public static void LogMessage(string debugInfo)
        {
            try
            {
                mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI", debugInfo);
            }
            catch (Exception ex)
            {
                //UNDONE - ignore
            }
        }

        public static string GetDetailedError(Exception ex)
        {
            string description = string.Empty;

            Exception tmpException = ex;

            while (tmpException != null)
            {
                description += tmpException.Message + Environment.NewLine;
                description += tmpException.StackTrace + Environment.NewLine;
                description += "--" + Environment.NewLine;

                tmpException = tmpException.InnerException;
            }

            return description;
        }

        public static void AppendToSystemLog(EntryTypes entryType, EntryCategories entryCategory, Exception ex)
        {
            AppendToSystemLog(entryType, entryCategory, GetDetailedError(ex));
        }

        //public static void AppendToSystemLog(EntryTypes entryType, EntryCategories entryCategory, String description)
        //{
        //    try
        //    {
        //        using (IBIDataModel dbContext = new IBIDataModel())
        //        {   
        //            List<String> logTypes = IBI.Shared.AppUtility.CommaValuesToList(AppSettings.SystemLogType());

        //            if (logTypes.Count==1 && logTypes.First().ToLower() == "none")
        //            {
        //                //no logs should be saved
        //                return;
        //            }

        //            bool allowed = false;
        //            foreach(string logType in logTypes)
        //            {
        //                if(entryType.ToString().ToLower() == logType)
        //                {
        //                    allowed = true;
        //                }
        //            }

        //            if(allowed)
        //            {
        //                dbContext.AppendLog(entryType.ToString(), entryCategory.ToString(), description);
        //                dbContext.SaveChanges();   
        //            }
        //        }
                
        //    }
        //    catch (Exception ex)
        //    {
        //        //UNDONE: Silent log
               
        //    }
        //}

        public static decimal AppendToSystemLog(EntryTypes entryType, EntryCategories entryCategory, String description)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    List<String> logTypes = IBI.Shared.AppUtility.CommaValuesToList(AppSettings.SystemLogType());

                    if (logTypes.Count == 1 && logTypes.First().ToLower() == "none")
                    {
                        //no logs should be saved
                        return 0;
                    }

                    bool allowed = false;
                    foreach (string logType in logTypes)
                    {
                        if (entryType.ToString().ToLower() == logType)
                        {
                            allowed = true;
                        }
                    }

                    if (allowed)
                    {
                        decimal? val = dbContext.AppendLog(entryType.ToString(), entryCategory.ToString(), description).FirstOrDefault();
                        dbContext.SaveChanges();

                        return val.HasValue ? val.Value : 0;
                    }
                }

            }
            catch (Exception ex)
            {
                //UNDONE: Silent log

            }

            return 0;
        }

        public static void increaseCounter(String counterName)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    dbContext.UpdateCounter(counterName);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                AppendToSystemLog(EntryTypes.Error, EntryCategories.IBI, ex);
            }
        }
    }
}