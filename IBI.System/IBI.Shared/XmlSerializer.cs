﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace IBI.Shared
{
    public class XmlSerializer
    {
        public static String Serialize(Object obj)
        {

            String XmlizedString = null;

            MemoryStream memoryStream = new MemoryStream();

            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            System.Xml.XmlTextWriter xmlTextWriter = new System.Xml.XmlTextWriter(memoryStream, Encoding.GetEncoding("UTF-8"));

            xs.Serialize(xmlTextWriter, obj);
            
            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;

            XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());

            return XmlizedString;
        }

        private static String UTF8ByteArrayToString(Byte[] characters)
        {
            Encoding encoding = Encoding.GetEncoding("UTF-8");

            String constructedString = encoding.GetString(characters);

            while (constructedString[0] != '<')
                constructedString = constructedString.Substring(1);

            return (constructedString);
        }

    }
}