﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO; 
using System.Web.UI.WebControls; 
using System.Web; 
using System.Configuration; 
using System.Web.Mail;
using System.Text.RegularExpressions;
using System.Net.Mail;


namespace Adex
{

	public class SiteMail
	{
		private static string AppPath = ConfigurationManager.AppSettings["AppPath"].ToString();
		private static string AppName = ConfigurationManager.AppSettings["AppName"].ToString(); 
		private static string AdminEmail = ConfigurationManager.AppSettings["AdminEmail"].ToString();
		private static string NoReplyEmail = ConfigurationManager.AppSettings["NoReplyEmail"].ToString();
		private static string AccountsEmail = ConfigurationManager.AppSettings["AccountsEmail"].ToString(); 
		private static string SalesEmail = ConfigurationManager.AppSettings["SalesEmail"].ToString();

	 
 
		public static bool SendActivationEmail(int uid, string pwd, string email, bool firstActivation )
		{

			string title = AppName + " Activation Email";

			string FromAccount = AdminEmail;
			string msg = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/emails/signup-activation-mail.htm"));
			 
			try {
				SendSimpleMail(FromAccount, email, title, msg, null);
				return true;
			} catch {
				return false;
			}

		}


		public static bool SendSimpleMail(string fromAccount, string toAccount, string title, string msg, System.Net.Mail.Attachment[] attachments )
		{

			msg += "<br /><br /><hr size=1 /><br />If you no longer want to recieve this email, please <a href='" + AppPath + "/unsubscribe.aspx'><u>click here to unsubscibe from this email</u></a>.";


			string MyHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
			string MySMTPPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();
			string MySMTPSSL = ConfigurationManager.AppSettings["SMTPSSL"].ToString();

			System.Net.Mail.MailAddress toAddr = new System.Net.Mail.MailAddress(toAccount);
			System.Net.Mail.MailAddress fromAddr = new System.Net.Mail.MailAddress(fromAccount, AppName);

			System.Net.Mail.MailMessage smtpMssg = new System.Net.Mail.MailMessage(fromAddr, toAddr);

			smtpMssg.Subject = title;
			smtpMssg.Body = msg;
			smtpMssg.IsBodyHtml = true;
			smtpMssg.Priority = System.Net.Mail.MailPriority.Normal;

			if ((attachments != null)) {
				foreach (System.Net.Mail.Attachment att in attachments) {
					smtpMssg.Attachments.Add(att);
				}
			}


			//SmtpClient is built in SMTP class. set SmtpServer= IIS SMTP default domain.
			System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(MyHost, Convert.ToInt32(MySMTPPort));
			smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;



			if (MySMTPSSL == "true") {
				smtpClient.EnableSsl = true;
			}



			try {
				smtpClient.Send(smtpMssg);
				return true;
			} catch (Exception ex) {
				//HttpContext.Current.Response.Write("<script>alert('Email not sent | Reason: [" & ex.Message & "]')</script>")
				throw new Exception("Email not sent | Reason: [" + ex.Message + "]");
				
			}

		}
         

	}
}
