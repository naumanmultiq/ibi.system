﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Diagnostics;
using mermaid.BaseObjects.Diagnostics;

namespace IBI.Shared
{
    public class CallCounter : IDisposable
    {
        private String name;

        PerformanceCounter totalCounter;
        PerformanceCounter activeCounter;

        public CallCounter(String name)
        {
            this.name = name;

            this.RegisterCounters();

            //totalCounter = new PerformanceCounter();
            //totalCounter.CategoryName = "ibi.mermaid.dk - " + name;
            //totalCounter.CounterName = name + " - Total";
            //totalCounter.ReadOnly = false;

            PerformanceCounterManager.CategoryName = "ibi.mermaid.dk";
            totalCounter.Increment();

            //activeCounter = new PerformanceCounter(); DONE
            //activeCounter.CategoryName = "ibi.mermaid.dk - " + name;
            //activeCounter.CounterName = name + " - Active";
            //activeCounter.ReadOnly = false;

            PerformanceCounterManager.CategoryName = "ibi.mermaid.dk";
            activeCounter.Increment();
        }

        private void RegisterCounters()
        {
            //// Bind the counters to a PerformanceCounterCategory
            //// Check if the category already exists or not.
            //String categoryName = "ibi.mermaid.dk - " + name;

            //if (!PerformanceCounterCategory.Exists(categoryName))
            //{
            //    CounterCreationDataCollection col = new CounterCreationDataCollection();

            //    // Create two custom counter objects.
            //    CounterCreationData activeCounterData = new CounterCreationData();
            //    activeCounterData.CounterName = name + " - Active";
            //    activeCounterData.CounterHelp = "Active calls";
            //    activeCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

            //    CounterCreationData totalCounterData = new CounterCreationData();
            //    totalCounterData.CounterName = name + " - Total";
            //    totalCounterData.CounterHelp = "Total calls";
            //    totalCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

            //    // Add custom counter objects to CounterCreationDataCollection.
            //    col.Add(activeCounterData);
            //    col.Add(totalCounterData);

            //    PerformanceCounterCategory category = PerformanceCounterCategory.Create(categoryName, "ibi.mermaid.dk", col);
            //    category.CategoryType = PerformanceCounterCategoryType.SingleInstance;

            //}

            PerformanceCounterManager.CategoryName = "ibi.mermaid.dk";
            this.totalCounter = PerformanceCounterManager.GetPerformanceCounter(name + " - Total");
            PerformanceCounterManager.CategoryName = "ibi.mermaid.dk";
            this.activeCounter = PerformanceCounterManager.GetPerformanceCounter(name + " - Active");

        }

        public void Dispose()
        {
            PerformanceCounterManager.CategoryName = "ibi.mermaid.dk";
            activeCounter.Decrement();

            //activeCounter.Close();
            activeCounter = null;

            //totalCounter.Close();
            totalCounter = null;
        }
    }
}