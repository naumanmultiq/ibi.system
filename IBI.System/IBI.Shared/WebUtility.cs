﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace IBI.Shared
{
    public class WebUtility
    {
        public static string AppPath
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    HttpRequest request = HttpContext.Current.Request;
                    string appPath = HttpContext.Current.Request.ApplicationPath;
                    appPath = appPath.Length == 1 ? appPath.TrimEnd('/') : appPath;
                    return appPath;
                }
                else
                {
                    return "";
                }

            }
        }


        public static string GetWebResponse(string requestURL)
        { 
            
            try
            {
                WebRequest request = HttpWebRequest.Create(requestURL);

                request.Timeout = 10000; //milliseconds

                HttpWebResponse webresponse;

                webresponse = (HttpWebResponse)request.GetResponse();

                System.Text.Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);

                string response = responseStream.ReadToEnd();

                responseStream.Close();
                webresponse.Close();
                return response;
            }
            catch (Exception ex) 
            {
                throw ex;
            }            
        }

        public static Stream GetWebResponseStream(string requestURL)
        {

            try
            {
                WebRequest request = HttpWebRequest.Create(requestURL);

                request.Timeout = 10000; //milliseconds

                HttpWebResponse webresponse;

                webresponse = (HttpWebResponse)request.GetResponse();

                //System.Text.Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
                //StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);

                //string response = responseStream.ReadToEnd();

                //responseStream.Close();
                //webresponse.Close();
                //return response;

                return webresponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
