﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.VehicleSchedules.Interfaces
{
    public interface IVehicleScheduleImporter
    {
        int CustomerId { get; set; }
        string Url { get; set; }

        void ImportVehicleSchedules();
    }
}
