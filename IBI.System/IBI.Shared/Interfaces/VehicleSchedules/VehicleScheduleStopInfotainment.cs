﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Interfaces.VehicleSchedules
{
    public class VehicleScheduleStopInfotainment
    {
        #region Attributes
        private bool _hasannouncement;
        private string _announcementtext;
        #endregion

        #region Properties
        public bool HasAnnouncement
        {
            get
            {
                return _hasannouncement;
            }
            set
            {
                _hasannouncement = value;
            }
        }

        public string AnnouncementText
        {
            get
            {
                return _announcementtext;
            }

            set
            {
                _announcementtext = value;
            }
        }
        #endregion

        public VehicleScheduleStopInfotainment(bool hasAnnouncement, string announcementText)
        {
            _hasannouncement = hasAnnouncement;
            _announcementtext = announcementText;
        }
    }
}
