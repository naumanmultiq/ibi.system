﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBI.Shared.Models;

namespace IBI.Shared.Interfaces
{
    public interface ISyncInService
    {
        int CustomerId { get; set; }

        string SyncInServiceStatus(string busNumber);
       
    }
}
