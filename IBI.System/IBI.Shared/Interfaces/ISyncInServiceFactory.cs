﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBI.Shared.Interfaces
{
    public interface ISyncInServiceFactory
    {
        ISyncInService GetSynInService(int customerId);
        
    }
 

}
