﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBI.Shared.Interfaces.ServiceProviders
{
    public interface IProviderFactory
    {
        IScheduleProvider GetSchedularProvider(int customerId);
        ICorrespondingTrafficProvider GetCorrespondingTrafficProvider(int customerId);
    }
}
