﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Interfaces.ServiceProviders
{
    public interface IServiceProvider
    {
        int CustomerId { get; set; }
    }
}
