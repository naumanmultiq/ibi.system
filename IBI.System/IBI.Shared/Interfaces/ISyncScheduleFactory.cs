﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBI.Shared.Interfaces
{
    public interface ISyncScheduleFactory
    {
        ISyncSchedular GetSyncSchedular(int customerId);
        ISyncDepartureBoard GetSyncDepartureBoard(int customerId);
        ISyncInService GetSyncInService(int customerId);
    }
 

}
