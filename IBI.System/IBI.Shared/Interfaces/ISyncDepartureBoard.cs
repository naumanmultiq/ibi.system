﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Interfaces
{
    public interface ISyncDepartureBoard
    {
        int CustomerId { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string Url { get; set; }
        string StopInfoUrl { get; set; }
        string GetDepartureBoard(string busNumber, string stopNumber, string line, int showJourneys, DateTime dateTime);
        string GetDepartureBoard(List<KeyValuePair<string, object>> parameters);
    }
}
