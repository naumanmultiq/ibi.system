﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;

namespace IBI.Shared
{
    public class AppSettings
    {
        public static SqlConnection GetIBIDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["IBIDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static SqlConnection GetHotspotDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["HotspotDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static SqlConnection GetvTouchDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["vTouchDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static SqlConnection GetNovoDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["NovoDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static String GetTTSDatabaseConnectionString()
        {
            String connectionString = ConfigurationManager.AppSettings["TTSDatabaseConnection"];

            return connectionString;
        }

        public static SqlConnection GetTTSDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["TTSDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static String GetResourceDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["ResourceDirectory"];

            return resourceDirectory;
        }

        public static TimeSpan GetOfflineTolerance()
        {
            String tolerance = ConfigurationManager.AppSettings["OfflineTolerance"];

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetDCUVSInService()
        {
            String tolerance = ConfigurationManager.AppSettings["DCU_VS_InService"];

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetCCUVSInService()
        {
            String tolerance = string.Empty;
            try
            {
                tolerance = ConfigurationManager.AppSettings["CCU_VS_InService"];
            }
            catch(Exception ex)
            {
                tolerance = "00:10:00";
            }
            

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetDCUVSOtherSubSystem()
        {
            String tolerance = ConfigurationManager.AppSettings["DCU_VS_OtherSubSystem"];

            return TimeSpan.Parse(tolerance);
        }
        public static TimeSpan GetCCUVSOtherSubSystem()
        {
            String tolerance = string.Empty;
            try
            {
                tolerance = ConfigurationManager.AppSettings["CCU_VS_OtherSubSystem"];
            }
            catch (Exception ex)
            {
                tolerance = "00:10:00";
            }
            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetVTCVSInService()
        {
            String tolerance = ConfigurationManager.AppSettings["VTC_VS_InService"];

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetVTCVSOtherSubSystem()
        {
            String tolerance = ConfigurationManager.AppSettings["VTC_VS_OtherSubSystem"];

            return TimeSpan.Parse(tolerance);
        }


        public static TimeSpan GetInfotainmentVSInService()
        {
            String tolerance = ConfigurationManager.AppSettings["Infotainment_VS_InService"];

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetInfotainmentVSOtherSubSystem()
        {
            String tolerance = ConfigurationManager.AppSettings["Infotainment_VS_OtherSubSystem"];

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetHotspotVSInService()
        {
            String tolerance = ConfigurationManager.AppSettings["Hotspot_VS_InService"];

            return TimeSpan.Parse(tolerance);
        }

        public static TimeSpan GetHotspotVSOtherSubSystem()
        {
            String tolerance = ConfigurationManager.AppSettings["Hotspot_VS_OtherSubSystem"];

            return TimeSpan.Parse(tolerance);
        }


        public static String GetIUSR()
        {
            //TODO: Hardcoded IUSR
            String user = ConfigurationManager.AppSettings["IUSR"];

            return user;
        }

        public static Boolean ShouldValidateSchedules()
        {
            String value = ConfigurationManager.AppSettings["ValidateSchedules"];

            if (!String.IsNullOrEmpty(value))
                return Boolean.Parse(value);

            return true;
        }

        public static int SignListCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["signlistcache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch(Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static int SignAddOnSelectionListCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["signaddonselectionlistcache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch (Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static int SignAddOnListCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["signaddonlistcache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch (Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static int SignAddOnCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["signaddoncache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch (Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static int TTSFileCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["ttsfilecache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch (Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static int ScheduleListCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["schedulelistcache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch (Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static int DestinationSelectionListCache
        {
            get
            {
                int nResult = 30;
                try
                {
                    object val = ConfigurationManager.AppSettings["destinationselectionlistcache"];
                    if (val != null)
                    {
                        nResult = Int32.Parse(val.ToString());
                    }
                }
                catch (Exception ex)
                {
                    nResult = 30;
                }
                return nResult;
            }
        }

        public static String RestPath
        {
            get
            {
                return ConfigurationManager.AppSettings["REST_PATH"].ToString();
            }
        }

        public static String DefaultCulture
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultCulture"].ToString();
            }
        }

        public static bool InfotainmentPing
        {
            get
            {
                return ConfigurationManager.AppSettings["InfotainmentPing"].ToString().ToLower() == "true" ? true : false;
            }
        }

        public static bool HotspotPing
        {
            get
            {
                return ConfigurationManager.AppSettings["HotspotPing"].ToString().ToLower() == "true" ? true : false;
            }
        }

        public static bool MessagePing
        {
            get
            {
                return ConfigurationManager.AppSettings["MessagePing"].ToString().ToLower() == "true" ? true : false;
            }
        }

        public static TimeSpan AuthTokenValidity
        {
            get
            {
                return TimeSpan.Parse(ConfigurationManager.AppSettings["AuthTokenValidity"].ToString());
            }
        }

        public static string DestinationNotificationEmail
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["DestinationNotificationEmail"]))
                {
                    return "mas@mermaid.dk";
                }
                return ConfigurationManager.AppSettings["DestinationNotificationEmail"].ToString();
            }
        }

        public static List<string> NotificationEmail(string section,string customerId)
        {
             NotificationEmailsConfig config = (NotificationEmailsConfig)ConfigurationManager.GetSection("NotificationEmails");
             var email = (from d in config.Xdoc.Descendants(section)
                                from e in d.Descendants("Notification")
                                where e.Attribute("customerId").Value == customerId
                                select e.Value).ToList();
             return email;
        }

        public static int ShowOfflineBusInterval
        {
            get
            {
                int retVal = 5;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowOfflineBusInterval"]))
                {
                    retVal = int.Parse(ConfigurationManager.AppSettings["ShowOfflineBusInterval"]);
                }
                return retVal;
            }
        }

        public static string ApplicationDirectory
        {
            get { return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); }
        }

        public static string TemporaryDirectory
        {
            get { return Path.Combine(ApplicationDirectory, "Temp"); }
        }

        public static String SystemLogType()
        {
            String val = ConfigurationManager.AppSettings["SystemLogType"];

            return val;
        }

        public static int FuturejourneyDefaultRadius()
        {
            int val = int.Parse(ConfigurationManager.AppSettings["FuturejourneyDefaultRadius"].ToString());
            return val;
        }

        public static String GetImageBuilderServerUrl()
        {
            String url = ConfigurationManager.AppSettings["imagegeneratorurl"];
            if (string.IsNullOrEmpty(url))
                url = "http://krusty.mermaid.dk/imagegen/buildimage.php?";
            return url;
        }

        public static int FileInterval()
        {
            int retVal = 5000;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["FileInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["FileInterval"]);
            }
            return retVal;
        }

        public static int SyncScheduleBusInterval()
        {
            int retVal = 5000;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SyncScheduleBusInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["SyncScheduleBusInterval"]);
            }
            return retVal;
        }

        public static bool MoviaJourneySyn()
        {
            return string.IsNullOrEmpty(ConfigurationManager.AppSettings["MoviaJourneySyn"]) ? true : bool.Parse(ConfigurationManager.AppSettings["MoviaJourneySyn"]);
        }

        public static bool MoviaJourneySynLogEnabled()
        {
            return string.IsNullOrEmpty(ConfigurationManager.AppSettings["MoviaJourneySynLogEnabled"]) ? false : bool.Parse(ConfigurationManager.AppSettings["MoviaJourneySynLogEnabled"]);
        }

        public static int SyncScheduleStopInterval()
        {
            int retVal = 50;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SyncScheduleStopInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["SyncScheduleStopInterval"]);
            }

            return retVal;
        }

        public static bool LogAllMessages()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
            }

            return retVal;
        }

        public static int GetNorgebussFutureDays()
        {
            int nResult = 3;
            String futureDays = ConfigurationManager.AppSettings["NorgebussFutureDays"];
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NorgebussFutureDays"]))
            {
                nResult = int.Parse(ConfigurationManager.AppSettings["NorgebussFutureDays"]);
            }
            return nResult;
        }

     
    }
}