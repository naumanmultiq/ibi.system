﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.MSMQ
{
    

    public class VDVQueueManager : IDisposable
    {
    #region Members

        /// <summary>
        /// Sender must always be provided with default constructor of this class
        /// </summary>
        public string Sender { get; set; }

        
        private string QueueName
        {
            get
            { 
                return string.Format(".\\Private$\\vdv_DataReadyPing_{0}", this.Sender);   
            }
        }  

        private StringBuilder log = new StringBuilder();

        private QueueStatus _status;
        public QueueStatus Status
        {
            get
            {
                if(_status != QueueStatus.EMPTY && _status != QueueStatus.NOTEMPTY)
                {
                    return QueueStatus.EMPTY;
                }

                return _status;
            }
            set
            {
                _status = value;
            }
                
        }

        #endregion



        #region Constructors

     

        public VDVQueueManager(string sender)
        {
            this.Sender = sender;
            log.AppendLine(DateTime.Now.ToString() + ": QueueManager new instance started for Sender: " + sender); 
        }

        #endregion

        #region Events

         
        public void Dispose()
        {
            //this.tmrEstimation.Enabled = false;
            //this.tmrEstimation.Dispose(); 
 
        }

        #endregion

        #region Methods

          public  void AppendToQueue(string data)
        {
            MessageQueue mq = null;

            if(!MessageQueue.Exists(QueueName))
            {
                mq = MessageQueue.Create(QueueName);
                mq.SetPermissions("Everyone", MessageQueueAccessRights.FullControl);
            }
            else
            {
                mq = new MessageQueue(QueueName);                              
            }

            mq.Send(data);

            //if (this.Status == QueueStatus.EMPTY)
            //{
            //    QueueManager qm = new QueueManager();
            //}
        }

      
        public string ReadNext()
        {

            log.Append(Environment.NewLine + "#########################################################" + Environment.NewLine + Environment.NewLine);
            String result = string.Empty;

            MessageQueue mq = null;
             
            if (MessageQueue.Exists(QueueName))
            {
                mq = new System.Messaging.MessageQueue(QueueName);
                mq.SetPermissions("Everyone", MessageQueueAccessRights.FullControl);
            }
                
            else
                return string.Empty;

            string j = string.Empty;

            try
            {
                Message msg = mq.Receive(new TimeSpan(0, 0, 3));
                msg.Formatter = new XmlMessageFormatter(
                  new String[] { "System.String,mscorlib" });
                j = msg.Body.ToString();
                
            }
            catch
            {
                j = string.Empty;
            }
             
            
            this.Status = j == string.Empty ? QueueStatus.EMPTY : QueueStatus.NOTEMPTY;

            if(j != string.Empty)
            {
                //Importer.ImportPredictions(j);   
                return j;
            }

            //return "SUCCESS";
            return string.Empty;
        }
         

        #endregion




      
    }

     


}
