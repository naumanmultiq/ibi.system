﻿using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.MSMQ
{
    
    public class MSMQManager : IDisposable
    { 

        const string QUEUE_PATH = @".\private$\";
		const string RETRY_PATH = @".\private$\_retry";		
		const string DEAD_LETTER_QUEUE_PATH = @".\private$\_dead_letter";

        public string QueueName { get; set; }

        private MessageQueue SelectedQueue
        {
            get { return CreateAndGetQueue(string.Format("{0}{1}", QUEUE_PATH, QueueName)); }
        }

        private MessageQueue Queue
        {
            get
            {
                return CreateAndGetQueue(string.Format("{0}{1}", QUEUE_PATH, QueueName));
            }
        }

        private MessageQueue CreateAndGetQueue(string path)
        {
            if (!MessageQueue.Exists(path))
            {
                MessageQueue.Create(path, true);
            }
            return new MessageQueue(path);
        }


        public MSMQManager(string queueName) {
            this.QueueName = queueName;
        }

        public void Send(string label, string body)
        {
            try
            {
                Queue.Send(body, label, MessageQueueTransactionType.Single);
                Trace.WriteLine("Sent message: " + label);
                
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Couldn't send message: " + ex);
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MESSAGE_QUEUE, string.Format("Queue '{0}', Item: {1}, Send Entry failed: Error Details: {2}", Queue.QueueName, body, Logger.GetDetailedError(ex)));
            }
        }

        public Message Receive() {
            IFailedMessageHandler failureHandler = null;
            failureHandler = new AlwaysRollBackHandler();

            //if (discardRadioButton.Checked)
            //{
            //    failureHandler = new DiscardMessageHandler();
            //}
            //if (alwaysRollbackRadioButton.Checked)
            //{
            //    failureHandler = new AlwaysRollBackHandler();
            //}
            //else if (sendToBackRadioButton.Checked)
            //{
            //    failureHandler = new SendToBackHandler(
            //            CreateAndGetQueue(DEAD_LETTER_QUEUE_PATH));
            //}
            //else if (sendToDeadLetterRadioButton.Checked)
            //{
            //    failureHandler = new RetrySendToDeadLetterQueueHandler(
            //            CreateAndGetQueue(DEAD_LETTER_QUEUE_PATH));
            //}
            //else if (separateRetryRadioButton.Checked)
            //{
            //    failureHandler = new SeparateRetryQueueHandler(
            //            CreateAndGetQueue(RETRY_PATH));
            //}

            return new MessageReceiver(Queue).ReceiveMessage(failureHandler);
             
        }

        public int Count() {
            
            return SelectedQueue.GetAllMessages().Count();
        }

        public List<Message> GetAllMessages()
        {
            List<Message> list = new List<Message>();
            try
            {
                foreach (Message message in SelectedQueue.GetAllMessages())
                {
                    list.Add(message);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Couldn't refresh: " + ex);
            }

            return list;
        }

        public void Dispose()
        {
            
        }

    }




}
