﻿using System.Web;
using System.Web.Mvc;

namespace IBI.Web.HRX
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}