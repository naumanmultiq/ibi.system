﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IBI.Web.HRX
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        { 
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{data}",
                defaults: new { controller = "Hrx", action = "Estimation", data = UrlParameter.Optional }
            );

            
            //routes.MapRoute(
            //    name: "default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Hrx", action = "SenderResponse", sender = UrlParameter.Optional }
            //);

         

            //routes.MapRoute(
            //    name: "HaCon",
            //    url: "",
            //    defaults: new { controller = "Hrx", action = "SenderResponse" }
            //);
        } 
    }
}