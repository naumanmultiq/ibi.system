﻿CREATE FUNCTION [dbo].[ConvertToGeography]
( 
@text varchar(50) 
) 
RETURNS geography 
AS 
BEGIN 
	DECLARE @geographyText varChar(50)
	DECLARE @geography geography
		
	SET @geographyText = REPLACE(@text, ',', ' ');

	IF @geographyText!=''
		SET @geography = geography::STGeomFromText('POINT('+@geographyText+')', 4326)
	
	RETURN @geography
END
