﻿CREATE PROCEDURE [dbo].[sp_FindScheduleID]
	@line nvarchar(max),
	@fromName nvarchar(max),
	@destination nvarchar(max) 
AS
BEGIN	
	--DECLARE @line nvarchar(max)
	--DECLARE @fromName nvarchar(max)
	--DECLARE @destination nvarchar(max)
	--SET @line = '2A'
	--SET @fromName = ''
	--SET @destination = 'Kastrup st.'
	
	DECLARE @resolvedDestination nvarchar(max)
	DECLARE @resolvedViaName nvarchar(max)
		
	DECLARE @splits TABLE([id] [int], [val] nvarchar(max))
	INSERT INTO @splits 
		SELECT * FROM [dbo].[Split](@destination, '/')
	
	IF (SELECT COUNT(*) FROM @splits) > 1
	BEGIN
		SELECT @resolvedDestination = [val] FROM @splits WHERE [id]=1
		SELECT @resolvedViaName = [val] FROM @splits WHERE [id]=2
	END
	ELSE
	BEGIN
		SELECT @resolvedDestination = [val] FROM @splits WHERE [id]=1
		SET @resolvedViaName = ''	
	END
	
	DECLARE @scheduleID int
	--SELECT @scheduleID = [ScheduleID] FROM [Schedules] WHERE [Line]=@line AND ([FromName]=@fromName OR @fromName = '') AND [Destination]=@resolvedDestination AND [ViaName]=@resolvedViaName
    SELECT @scheduleID = [ScheduleID] FROM [Schedules] WHERE [Line]=@line AND [Destination]=@resolvedDestination AND [ViaName]=@resolvedViaName
	IF (@scheduleID IS NULL)
	BEGIN
		PRINT '!!!!! --- SCHEDULE ID NOT RESOLVED --- !!!!!'
		PRINT ''
		PRINT ' Line:        ' + @line
		PRINT ' FromName:    ' + @fromName
		PRINT ' Destination: ' + @destination
		PRINT ''

		SET @scheduleID = 0
	END

	RETURN @scheduleID
END
