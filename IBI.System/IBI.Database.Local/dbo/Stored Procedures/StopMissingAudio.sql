﻿-- =============================================
-- Author:  NAK
-- Create date: 21 Feb 2013
-- Description: It returns list of all stop names that have no audio file
-- =============================================
CREATE PROCEDURE [dbo].[StopMissingAudio](@customerId int, @voice varchar(50)) 
 
AS
BEGIN
  
	SELECT distinct ss.StopName
		FROM Schedules s
		INNER JOIN ScheduleStops ss ON s.ScheduleId = ss.ScheduleId
		WHERE s.CustomerId = @customerId AND ISNULL(ss.StopName, '') <> ''
	EXCEPT
	 SELECT tts.[Text]
	 FROM [TTSServer_GRANDPA].[dbo].[AudioFiles] tts where tts.Voice = @voice 
	 
END