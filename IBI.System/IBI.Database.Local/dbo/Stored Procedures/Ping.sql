﻿CREATE PROC [dbo].[Ping]
 @BusNo varchar(20),
 @ClientType varchar(10),
 @CustomerId int,
 @MacAddress varchar(50),
 @JourneyNumber int,
 @SimID varchar(50),
 @Latitude varchar(50),
 @Longitude varchar(50),
 @UpdateLastTime bit
 
AS

/**SAMPLE DATA
set @BusNo = '1224'
set @ClientType = 'DCU'
set @CustomerId = 2140
set @MacAddress ='Mac123'
set @JourneyNumber ='23'
set @SimID = 'simid123'
set @Latitude = '40.45455'
set @Longitude = '41.4545'
*/
 
DECLARE @LastTimeInService datetime
DECLARE @DefaultGroup int
DECLARE @FoundBus int
DECLARE @LastKnownLocation Geography
 
SET @LastKnownLocation = geography::STPointFromText('POINT(' + @Latitude + ' ' + @Longitude + ')', 4326)
 
If EXISTS(SELECT BusNumber From Buses b WHERE b.BusNumber=@BusNo AND b.CustomerId=@CustomerId)
	
	BEGIN
	
		IF @UpdateLastTime=1
			UPDATE Buses SET 			
				LastTimeInService = GETDATE(),			
				LastKnownLocation = @LastKnownLocation
				WHERE BusNumber = @BusNo
		ELSE
			UPDATE Buses SET 						
				LastKnownLocation = @LastKnownLocation
				WHERE BusNumber = @BusNo
	END
	
ELSE
	
	BEGIN
		--add new bus
		IF @UpdateLastTime = 1
			INSERT INTO Buses(
				BusNumber, 
				CustomerId, 
				InOperation, 
				CurrentJourneyNumber,
				LastTimeInService,
				LastKnownLocation)
			VALUES (@BusNo, @CustomerId, 0, @JourneyNumber, GETDATE(), @LastKnownLocation)
		ELSE
			INSERT INTO Buses(
				BusNumber, 
				CustomerId, 
				InOperation, 
				CurrentJourneyNumber,
				LastKnownLocation)
			VALUES (@BusNo, @CustomerId, 0, @JourneyNumber, @LastKnownLocation)

		--add bus association with default root level group				 
		SELECT @DefaultGroup = GroupID FROM Groups WHERE CustomerId = @CustomerId AND ParentGroupID IS NULL
		
		INSERT into GroupBuses (GroupId, BusNumber, CustomerId)
			VALUES (@DefaultGroup, @BusNo, @CustomerId)
 
	END

IF EXISTS(SELECT c.ClientID from Clients c WHERE c.BusNumber=@BusNo AND CustomerId=@CustomerId AND ClientType=@ClientType)  
	BEGIN 
		--update client
		UPDATE Clients SET
			MacAddress = @MacAddress,
			SimID = @SimID,
			LastPing = GETDATE()
			WHERE BusNumber = @BusNo AND CustomerId=@CustomerId AND ClientType = @ClientType	
	END
ELSE	
	BEGIN
		--add new client
		INSERT INTO Clients (BusNumber, ClientType, CustomerId, MacAddress, SimID, LastPing)
		  VALUES(@BusNo, @ClientType, @CustomerId, @MacAddress, @SimID, GETDATE())
	END
