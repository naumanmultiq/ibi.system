﻿CREATE PROC [dbo].[xml_Schedule]
@ScheduleId int
AS

BEGIN

select ScheduleId  AS [ScheduleNumber],
 Schedules.Line AS [Line],  
 Schedules.FromName AS [FromName],  
 Schedules.Destination AS [DestinationName], 
 Schedules.ViaName AS [ViaName],
	(
	select StopId as [@StopNumber], 
	 StopName as [StopName], 
	 --CAST(StopGPS.STPointN(1).Lat AS VARCHAR(10))  AS [GPSCoordinateNS], 
	 CONVERT(varchar(100), CAST(StopGPS.STPointN(1).Lat AS decimal(38,13)))   AS [GPSCoordinateNS],
	 CONVERT(varchar(100), CAST(StopGPS.STPointN(1).Long AS decimal(38,13)))   AS [GPSCoordinateEW],
	 --CAST(StopGPS.STPointN(1).Long AS VARCHAR(10)) AS [GPSCoordinateEW],
	 Zone as [Zone] 
	 --CASE IsCheckpoint WHEN 1 THEN 'true' ELSE 'false' END as [IsCheckpoint]
		From ScheduleStops
		where ScheduleId=@ScheduleId
		for xml path('StopInfo'), TYPE, ROOT('JourneyStops')
		) as [*]
from Schedules Where ScheduleId = @ScheduleId
for xml path ('Schedule')

END