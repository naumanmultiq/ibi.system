﻿-- =============================================
-- Author:		Nauman Khan
-- Create date: 02 March 2014
-- Description:	Journey Investigation
-- =============================================
CREATE PROCEDURE [dbo].[A_JourneyInvestigation] 
	-- Add the parameters for the stored procedure here
	@ScheduleNumber int = 0,
	@BusNumber int = 0,
	@Hours int = -2,
	@LastHours int = -24 

	
AS
BEGIN
	DECLARE @journeyNumber int
	DECLARE @jScheduleNumber int
	DECLARE @jBusNumber int
	DECLARE @jXmlData xml
	DECLARE @jStartDate datetime
	DECLARE @jEndDate datetime
	DECLARE @jIsLastStop int
	DECLARE @jAhead int
	DECLARE @jBehind int

	DECLARE @multipleJourneyAhead bit
	DECLARE @multipleAheadJourneys varchar(MAX)
	DECLARE @multipleBehindJourneys varchar(MAX)
	DECLARE @missedStopsClient varchar(MAX)
	DECLARE @missedJourneyStops varchar(MAX)
	DECLARE @currentJourneyAhead int
	DECLARE @currentJourneyBehind int
	DECLARE @differentAheadCounter int
	DECLARE @differentBehindCounter int

	DECLARE @sActualArrivalTime varchar(50)
	DECLARE @sActualDepartureTime varchar(50)
	DECLARE @sStopName varchar(100)
	DECLARE @missingInStops varchar(100)

	SET @multipleJourneyAhead = 0
	SET @multipleAheadJourneys  = ''
	SET @multipleBehindJourneys  = ''
	SET @missedStopsClient = ''
	SET @missedJourneyStops = ''
	SET @currentJourneyAhead = 0
	SET @currentJourneyBehind = 0
	SET @differentAheadCounter = 0
	SET @differentBehindCounter = 0
	SET @missingInStops = ''

	DELETE FROM diagnosis_table
	
	DECLARE db_cursor CURSOR FOR
	
	select top 100 JourneyNumber, ScheduleNumber, BusNumber, JourneyData, StartTime, EndTime, IsLastStop, JourneyAhead, JourneyBehind from Journeys
	where 
	((@ScheduleNumber != 0 and ScheduleNumber=@ScheduleNumber) OR @ScheduleNumber = 0) AND
	((@BusNumber != 0 and BusNumber=@BusNumber) OR @BusNumber = 0) AND
	--JourneyDate < DATEADD(hour, @Hours, GetDate()) AND
	JourneyDate > DATEADD(hour, @LastHours, GetDate())
	ORDER BY JourneyDate DESC
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO 
	@journeyNumber,@jScheduleNumber,@jBusNumber,@jXmlData,@jStartDate,@jEndDate,@jIsLastStop,@jAhead,@jBehind
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		--if @jAhead is not null and @jAhead != 0 and @jAhead != @currentJourneyAhead
		--begin
		--	SET @differentAheadCounter = @differentAheadCounter + 1
		--	SET @currentJourneyAhead = @jAhead
		--	SET @multipleAheadJourneys = '<' + CAST( @jAhead as varchar(50) ) + '>' + @multipleAheadJourneys
		--end

		--if @jBehind is not null and @jBehind != 0 and @jBehind != @currentJourneyBehind
		--begin
		--	SET @differentBehindCounter = @differentBehindCounter + 1
		--	SET @currentJourneyBehind = @jBehind
		--	SET @multipleBehindJourneys = '<' + CAST( @jBehind as varchar(50) ) + '>' + @multipleBehindJourneys
		--end
		-- Check Missed stops
		DECLARE journey_cursor CURSOR FOR
		SELECT 
			JData.Stops.value('(ActualArrivalTime/text())[1]', 'varchar(50)') ActualArrivalTime,
			JData.Stops.value('(PlannedArrivalTime/text())[1]', 'varchar(50)') PlannedArrivalTime,
			JData.Stops.value('(StopName/text())[1]', 'varchar(50)') StopName
		FROM 
			@jXmlData.nodes('/Journey/JourneyStops/StopInfo') AS JData(Stops)

		OPEN journey_cursor
			FETCH NEXT FROM journey_cursor INTO 
				@sActualArrivalTime,@sActualDepartureTime,@sStopName
				WHILE @@FETCH_STATUS = 0   
				BEGIN
					-- Check stop data
					if @sActualArrivalTime IS NULL or @sActualArrivalTime = ''
					BEGIN
						SET @missedStopsClient = '<' + @sStopName + '>' + @missedStopsClient 
					END
					--BEGIN
					--	-- this is stop missing
					SET @missingInStops = (SELECT StopName FROM JourneyStops WHERE JourneyNumber=@journeyNumber AND StopName=@sStopName)
					IF @missingInStops IS NULL OR @missingInStops=''
					BEGIN
						SET @missedJourneyStops = '<' + @sStopName + '>' + @missedJourneyStops 
					END
					--END

					FETCH NEXT FROM journey_cursor INTO 
						@sActualArrivalTime,@sActualDepartureTime,@sStopName		
				END
		CLOSE journey_cursor   
		DEALLOCATE journey_cursor

		INSERT into diagnosis_table
		SELECT @journeyNumber as JourneyNumber, @jStartDate as StartDate,@jEndDate as EndDate, @jScheduleNumber as ScheduleNumber, @jBusNumber as BusNumber, @missedStopsClient as MissedStopsXml, @missedJourneyStops as MissedJourneyStops, @jIsLastStop as LastStop

		FETCH NEXT FROM db_cursor INTO 
		@journeyNumber,@jScheduleNumber,@jBusNumber,@jXmlData,@jStartDate,@jEndDate,@jIsLastStop,@jAhead,@jBehind
	END   

	CLOSE db_cursor   
	DEALLOCATE db_cursor

	SELECT * FROM diagnosis_table

END
