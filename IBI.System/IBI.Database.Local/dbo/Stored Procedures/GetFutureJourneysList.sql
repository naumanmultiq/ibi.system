﻿-- =============================================
-- Author:           nak
-- Create date: 16 may 2013
-- =============================================
CREATE PROCEDURE [dbo].[GetFutureJourneysList]
       @period int = 24,
       @customerid int
AS
BEGIN
	DECLARE @tmpFutureJourneys TABLE  
	( 
		CustomerId INT NOT NULL,
		Line nvarchar(MAX), 
		FromName nvarchar(MAX) NOT NULL,
		Destination  nvarchar(MAX) NOT NULL, 
		ScheduleId INT NOT NULL, 
		JourneyId INT NOT NULL,
		PlannedDepartureTime datetime,
		PlannedArrivalTime datetime,
		Latitude decimal(38,13),
		Longitude  decimal(38,13),
		LastUpdated datetime
	)

	INSERT INTO @tmpFutureJourneys
		SELECT
			   j.CustomerId, 
			   s.Line, 
			   s.FromName,
			   s.Destination, 
			   j.ScheduleId,
			   j.JourneyId, 
			   j.PlannedStartTime,
			   j.PlannedEndTime,
			   CONVERT(varchar(100), CAST((Select TOP 1 js.StopGPS.STPointN(1).Lat from JourneyStops js where js.JourneyId = j.JourneyId order by StopSequence) AS decimal(38,13)))   AS [Latitude],
			   CONVERT(varchar(100), CAST((Select TOP 1 js.StopGPS.STPointN(1).Long  from JourneyStops js where js.JourneyId = j.JourneyId order by StopSequence) AS decimal(38,13)))   AS [Longitude],
			   j.LastUpdated
			   FROM Journeys j inner join Schedules s on j.ScheduleId = s.ScheduleId
		WHERE
			   BusNumber=0 
			   AND Active = 0
			   AND j.PlannedStartTime >=  DATEADD(HOUR, -2, GETDATE()) AND  j.PlannedStartTime <= DATEADD(HOUR, @period, GETDATE())
			   AND j.CustomerId=@customerid
	 SELECT 
		@period As [@period],
		(
			SELECT
				CustomerId As [@CustomerId],
				ScheduleId As [@ScheduleId],
				JourneyId As [@JourneyId],
				Line As [@Line],
				FromName As [@FromName],
				Destination As [@Destination],
				PlannedDepartureTime As [@PlannedDepartureTime],
				PlannedArrivalTime as [@PlannedArrivalTime],
				Latitude As [@Latitude],
				Longitude As [@Longitude],
				LastUpdated As [@LastUpdated]
			FROM @tmpFutureJourneys
			FOR XML PATH ('Journey'), Type
		  )
      FOR XML PATH ('FutureJourneys'), Type
	
    --  IF ( @lat <> '' AND @lng <> '' AND @period <> -1 )
		  --BEGIN
			 --DECLARE @p GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + @lat + ' ' + @lng + ')', 4326);
			 --INSERT INTO @tmpFutureJourneys
				-- SELECT
				--	   j.CustomerId, 
				--	   s.Line, 
				--	   s.FromName,
   	--			       s.Destination, 
				--	   j.ScheduleId,
				--	   j.JourneyId, 
				--	   j.PlannedStartTime,
				--	   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Lat AS decimal(38,13)))   AS [Latitude],
				--	   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Long AS decimal(38,13)))   AS [Longitude]
				--	   FROM Journeys j inner join Schedules s on j.ScheduleId = s.ScheduleId inner join JourneyStops js on j.JourneyId = js.JourneyId
				-- WHERE
				--	   BusNumber=0 
				--	   AND Active = 0
				--	   AND js.StopGPS.STBuffer(@radius).STIntersects(@p) = 1
				--	   AND j.PlannedStartTime <= DATEADD(HOUR, @period, GETDATE()) 
				--	   AND j.CustomerId=@customerid
		  --END
    --  IF ( @lat <> '' AND @lng <> '' AND @starttime <> '' AND @endtime <> '' )
		  --BEGIN
			 --DECLARE @p1 GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + @lat + ' ' + @lng + ')', 4326);
			 --INSERT INTO @tmpFutureJourneys
				-- SELECT
				--	   j.CustomerId, 
				--	   s.Line, 
				--	   s.FromName,
   	--			       s.Destination, 
				--	   j.ScheduleId,
				--	   j.JourneyId, 
				--	   j.PlannedStartTime,
				--	   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Lat AS decimal(38,13)))   AS [Latitude],
				--	   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Long AS decimal(38,13)))   AS [Longitude]
				--	   FROM Journeys j inner join Schedules s on j.ScheduleId = s.ScheduleId inner join JourneyStops js on j.JourneyId = js.JourneyId
				-- WHERE
				--	   BusNumber=0 
				--	   AND Active = 0
				--	   AND js.StopGPS.STBuffer(@radius).STIntersects(@p1) = 1
				--	   AND j.PlannedStartTime >= CONVERT(Datetime, @starttime) AND j.PlannedStartTime <= CONVERT(Datetime, @endtime) 
				--	   AND j.CustomerId=@customerid
		  --END
  --    IF ( @line <> '' AND @period <> -1 )
		--BEGIN
		--	 INSERT INTO @tmpFutureJourneys
		--		 SELECT
		--			   j.CustomerId, 
		--			   s.Line, 
		--			   s.FromName,
  -- 				       s.Destination, 
		--			   j.ScheduleId,
		--			   j.JourneyId, 
		--			   j.PlannedStartTime,
		--			   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Lat AS decimal(38,13)))   AS [Latitude],
		--			   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Long AS decimal(38,13)))   AS [Longitude]
		--			   FROM Journeys j inner join Schedules s on j.ScheduleId = s.ScheduleId inner join JourneyStops js on j.JourneyId = js.JourneyId
		--		 WHERE
		--			   BusNumber=0 
		--			   AND Active = 0
		--			   AND s.Line=@line
		--			   AND j.PlannedStartTime <= DATEADD(HOUR, @period, GETDATE()) 
		--			   AND j.CustomerId=@customerid
		--  END

    --  IF ( @line <> '' AND @starttime <> '' AND @endtime <> '' )
		  --BEGIN
			 --INSERT INTO @tmpFutureJourneys
				-- SELECT
				--	   j.CustomerId, 
				--	   s.Line, 
				--	   s.FromName,
   	--			       s.Destination, 
				--	   j.ScheduleId,
				--	   j.JourneyId, 
				--	   j.PlannedStartTime,
				--	   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Lat AS decimal(38,13)))   AS [Latitude],
				--	   CONVERT(varchar(100), CAST(js.StopGPS.STPointN(1).Long AS decimal(38,13)))   AS [Longitude]
				--	   FROM Journeys j inner join Schedules s on j.ScheduleId = s.ScheduleId inner join JourneyStops js on j.JourneyId = js.JourneyId
				-- WHERE
				--	   BusNumber=0 
				--	   AND Active = 0
				--	   AND s.Line=@line
				--	   AND j.PlannedStartTime >= CONVERT(Datetime, @starttime) AND j.PlannedStartTime <= CONVERT(Datetime, @endtime) 
				--	   AND j.CustomerId=@customerid
		  --END
	--SELECT * FROM @tmpFutureJourneys
	
	--SELECT 
	--	@lat As [@lat],
	--	@lng As [@lng],
	--	@radius As [@radius],
	--	@line As [@line],
	--	@period As [@period],
	--	@starttime As [@starttime],
	--	@endtime As [@endtime],
	--	(
	--		SELECT
	--			CustomerId As [@CustomerId],
	--			Line As [@Line],
	--			FromName As [@FromName],
	--			Destination As [@Destination],
	--			ScheduleNumber As [@ScheduleNumber],
	--			JourneyNumber As [@JourneyNumber],
	--			PlannedDepartureTime As [@PlannedDepartureTime],
	--			Latitude As [@Latitude],
	--			Longitude As [@Longitude] 
	--		FROM @tmpFutureJourneys
	--		for xml path ('Journey'), Type
	--	  )
 --    for xml path ('FutureJourneys'), Type
	  --for xml path ('FutureJourneys')
END
