﻿CREATE PROC [dbo].[SyncSchedules]
@CustomerId int,
@Line varchar(255),
@FromName varchar(255),
@Destination varchar(255),
@ViaName varchar(255),
@Schedule text
 
 
 
 AS
	
	BEGIN
   
   DECLARE @ScheduleId int
   DECLARE @RouteDirectionId int

	 
   DECLARE @XmlSchedule xml
   SET @XmlSchedule = @Schedule

	 
    DECLARE @tmpStops TABLE
    (
		ScheduleId int, 
		StopSequence [int] IDENTITY(1,1) NOT NULL, 
		StopId decimal(19,0), 
		StopName nvarchar(max), 
		StopGPS geography, 
		Zone nvarchar(max)
    )
				 						 
	--DBCC CHECKIDENT (@tmpStops, RESEED, 0)
	  
	INSERT INTO @tmpStops (ScheduleId, StopId, StopName, StopGPS, Zone)
	  SELECT  
	    0,
	    JData.Stops.value('@StopNumber', 'varchar(max)') as StopId,
		IsNUll(JData.Stops.value('(StopName/text())[1]', 'varchar(100)'), '') StopName, 
		geography::Point(JData.Stops.value('(GPSCoordinateNS/text())[1]', 'varchar(100)'), JData.Stops.value('(GPSCoordinateEW/text())[1]', 'varchar(100)'), 4326),
	    JData.Stops.value('(Zone/text())[1]', 'varchar(100)') Zone	 
		FROM 
			@XmlSchedule.nodes('/Schedule/JourneyStops/StopInfo') AS JData(Stops)
 
	  
	SET @ScheduleId = (SELECT TOP 1 ScheduleId FROM [Schedules]
						 WHERE [CustomerID]=@CustomerId	AND 
							   [Line]=@Line AND 
							   [FromName]=@FromName AND 
							   [Destination]=@Destination AND 
							   ISNULL([ViaName], '')= ISNULL(@ViaName, '')
						 ORDER BY LastUpdated DESC)		   
					  
			
	IF @ScheduleId IS NULL
		BEGIN   
													 
						  
				 SET  @RouteDirectionId= (SELECT TOP 1 RouteDirectionId FROM Schedules WHERE CustomerId=@CustomerId AND Line=@Line  AND Destination=@Destination) 
				  
				  IF @RouteDirectionId IS NULL
					BEGIN
						INSERT INTO RouteDirections (Name)
						VALUES (CAST(@CustomerId AS VARCHAR(max)) + '-' + IsNull(@Line, '') + '-'  + IsNull(@Destination, '') )
						
						SET @RouteDirectionId = SCOPE_IDENTITY()
					END
				  
				 INSERT INTO Schedules (CustomerId, RouteDirectionId, Line, FromName, Destination, ViaName, LastUpdated)
				 	VALUES(@CustomerId, @RouteDirectionId, @Line, @FromName, @Destination, @ViaName, GETDATE())
			  				  
					SET @ScheduleId = SCOPE_IDENTITY()
				
		 
				 INSERT INTO ScheduleStops 
					SELECT @ScheduleId, StopSequence, StopId, StopName, StopGPS, Zone From @tmpStops
			
			
		END
		
	 ELSE  
	 
		BEGIN  
			 
			DECLARE @ExistingStops VARCHAR(MAX)  
			SELECT @ExistingStops = COALESCE(@ExistingStops, '') + CAST(StopId AS VARCHAR(50)) FROM ScheduleStops WHERE ScheduleId = @ScheduleId
			--SELECT @ExistingStops
			
			
			DECLARE @NewStops VARCHAR(MAX)  
			SELECT @NewStops = COALESCE(@NewStops, '') + CAST(StopId AS VARCHAR(50)) FROM @tmpStops
			--SELECT @NewStops
			
			If(ISNULL(@ExistingStops, '') <> IsNull(@NewStops, ''))
			 BEGIN
				 DELETE FROM ScheduleStops WHERE ScheduleId = @ScheduleId
				
				 INSERT INTO ScheduleStops 
					SELECT @ScheduleId, StopSequence, StopId, StopName, StopGPS, Zone From @tmpStops
					
				 UPDATE Schedules SET LastUpdated = GETDATE() WHERE ScheduleId = @ScheduleId
			END 
				
		END
		 
		SELECT * FROM Schedules s WHERE s.ScheduleId = @ScheduleId
			 
END

