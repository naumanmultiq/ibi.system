﻿CREATE TABLE [dbo].[SignContents] (
    [ContentId]  INT           IDENTITY (1, 1) NOT NULL,
    [Line]       VARCHAR (50)  NULL,
    [MainText]   VARCHAR (200) NULL,
    [SubText]    VARCHAR (200) NULL,
    [CustomerId] INT           NOT NULL,
    CONSTRAINT [PK_SignContents] PRIMARY KEY CLUSTERED ([ContentId] ASC),
    CONSTRAINT [FK_SignContents_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

