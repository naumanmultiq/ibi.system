﻿CREATE TABLE [dbo].[SignGroups] (
    [GroupId]       INT           IDENTITY (1, 1) NOT NULL,
    [GroupName]     VARCHAR (100) NOT NULL,
    [ParentGroupId] INT           NULL,
    [CustomerId]    INT           NOT NULL,
    [SortValue]     INT           CONSTRAINT [DF_SignGroups_SortOrder] DEFAULT ((0)) NOT NULL,
    [Excluded]      BIT           CONSTRAINT [DF_SignGroups_Enabled] DEFAULT ((0)) NOT NULL,
    [DateAdded]     DATETIME      CONSTRAINT [DF_SignGroups_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified]  DATETIME      CONSTRAINT [DF_SignGroups_DateModified] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SignGroups] PRIMARY KEY CLUSTERED ([GroupId] ASC),
    CONSTRAINT [FK_SignGroups_SignGroups] FOREIGN KEY ([ParentGroupId]) REFERENCES [dbo].[SignGroups] ([GroupId])
);

