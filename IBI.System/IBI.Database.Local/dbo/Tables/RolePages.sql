﻿CREATE TABLE [dbo].[RolePages] (
    [RoleId]      INT      NOT NULL,
    [PageId]      INT      NOT NULL,
    [Access]      CHAR (4) NULL,
    [LastUpdated] DATETIME NULL,
    [Read]        BIT      CONSTRAINT [DF_RolePages_Read] DEFAULT ((0)) NULL,
    [Write]       BIT      CONSTRAINT [DF_RolePages_Write] DEFAULT ((0)) NULL,
    [Delete]      BIT      CONSTRAINT [DF_RolePages_Delete] DEFAULT ((0)) NULL,
    [Update]      BIT      CONSTRAINT [DF_RolePages_Update] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_RolePages] PRIMARY KEY CLUSTERED ([RoleId] ASC, [PageId] ASC),
    CONSTRAINT [FK_RolePages_Pages] FOREIGN KEY ([PageId]) REFERENCES [dbo].[Pages] ([PageId]) ON DELETE CASCADE,
    CONSTRAINT [FK_RolePages_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([RoleId]) ON DELETE CASCADE ON UPDATE CASCADE
);

