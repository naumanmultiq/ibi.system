﻿CREATE TABLE [dbo].[JourneyStopEstimations] (
    [JourneyId]               INT      NOT NULL,
    [EstimatedAtStopSequence] INT      NOT NULL,
    [EstimatedStopSequence]   INT      NOT NULL,
    [EstimationTime]          DATETIME NOT NULL,
    [EstimatedArrivalTime]    DATETIME NULL,
    [EstimatedDepartureTime]  DATETIME NULL
);

