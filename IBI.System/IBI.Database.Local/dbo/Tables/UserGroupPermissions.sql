﻿CREATE TABLE [dbo].[UserGroupPermissions] (
    [UserGroupId] INT NOT NULL,
    [GroupId]     INT NOT NULL,
    CONSTRAINT [PK_UserGroupPermissions] PRIMARY KEY CLUSTERED ([UserGroupId] ASC, [GroupId] ASC),
    CONSTRAINT [FK_UserGroupPermissions_Groups] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Groups] ([GroupId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserGroupPermissions_UserGroups] FOREIGN KEY ([UserGroupId]) REFERENCES [dbo].[UserGroups] ([UserGroupId]) ON DELETE CASCADE ON UPDATE CASCADE
);

