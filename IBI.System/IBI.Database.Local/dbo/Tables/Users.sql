﻿CREATE TABLE [dbo].[Users] (
    [UserId]       INT            IDENTITY (1, 1) NOT NULL,
    [Username]     NVARCHAR (50)  NULL,
    [Password]     NVARCHAR (50)  NULL,
    [FullName]     VARCHAR (100)  NULL,
    [ShortName]    VARCHAR (10)   NULL,
    [PhoneNumber]  VARCHAR (15)   NULL,
    [Email]        NVARCHAR (100) NULL,
    [Company]      VARCHAR (50)   NULL,
    [DateAdded]    DATETIME       CONSTRAINT [DF_IBIUsers_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified] DATETIME       CONSTRAINT [DF_IBIUsers_DateModified] DEFAULT (getdate()) NULL,
    [LastLogin]    DATETIME       NULL,
    [Domain]       VARCHAR (100)  NULL,
    [Description]  TEXT           NULL,
    CONSTRAINT [PK_IBIUsers] PRIMARY KEY CLUSTERED ([UserId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_IBIUsers_Username]
    ON [dbo].[Users]([Username] ASC);

