﻿CREATE TABLE [dbo].[Customers] (
    [CustomerId]        INT           NOT NULL,
    [FullName]          VARCHAR (100) NULL,
    [Description]       TEXT          NULL,
    [DateAdded]         DATETIME      CONSTRAINT [DF_Customers_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified]      DATETIME      CONSTRAINT [DF_Customers_DateModified] DEFAULT (getdate()) NULL,
    [AnnouncementVoice] VARCHAR (50)  NULL,
    [ScheduleRef]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);

