﻿CREATE TABLE [dbo].[Journeys] (
    [JourneyId]         INT              IDENTITY (1, 1) NOT NULL,
    [BusNumber]         VARCHAR (50)     NULL,
    [ScheduleId]        INT              NOT NULL,
    [CustomerId]        INT              NOT NULL,
    [PlannedStartTime]  DATETIME         NULL,
    [PlannedEndTime]    DATETIME         NULL,
    [StartTime]         DATETIME         NULL,
    [EndTime]           DATETIME         NULL,
    [Active]            BIT              NULL,
    [ExternalReference] NVARCHAR (MAX)   NULL,
    [ClientRef]         UNIQUEIDENTIFIER NULL,
    [LastUpdated]       DATETIME         CONSTRAINT [DF_Journeys_LastUpdated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Journeys] PRIMARY KEY CLUSTERED ([JourneyId] ASC),
    CONSTRAINT [FK_Journeys_Buses] FOREIGN KEY ([BusNumber], [CustomerId]) REFERENCES [dbo].[Buses] ([BusNumber], [CustomerId]),
    CONSTRAINT [FK_Journeys_Schedules] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[Schedules] ([ScheduleId]) ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [Idx_StartTime]
    ON [dbo].[Journeys]([StartTime] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_ScheduleId]
    ON [dbo].[Journeys]([ScheduleId] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_Active]
    ON [dbo].[Journeys]([Active] ASC);

