﻿CREATE TABLE [dbo].[AudioFiles] (
    [LastModified] DATETIME       NULL,
    [Text]         VARCHAR (255)  NULL,
    [Voice]        VARCHAR (255)  NULL,
    [FileName]     VARCHAR (255)  NULL,
    [FileHash]     VARCHAR (55)   NULL,
    [Approved]     BIT            NULL,
    [Rejected]     BIT            NULL,
    [FileContent]  IMAGE          NULL,
    [Version]      INT            CONSTRAINT [DF_AudioFiles_Version] DEFAULT ((1)) NULL,
    [SourceVoice]  VARCHAR (50)   NULL,
    [SourceText]   VARCHAR (1000) NULL
);

