﻿CREATE TABLE [dbo].[UserRoles] (
    [Roles_RoleId] INT NOT NULL,
    [Users_UserId] INT NOT NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY NONCLUSTERED ([Roles_RoleId] ASC, [Users_UserId] ASC),
    CONSTRAINT [FK_UserRoles_Role] FOREIGN KEY ([Roles_RoleId]) REFERENCES [dbo].[Roles] ([RoleId]),
    CONSTRAINT [FK_UserRoles_User] FOREIGN KEY ([Users_UserId]) REFERENCES [dbo].[Users] ([UserId])
);

