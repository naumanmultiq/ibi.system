﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IBI.MoviaRealtimeJourneys
{
    public partial class Service : ServiceBase
    {
        MoviaRealtimeJourneyController serverInstance;

        public Service()
        {
            InitializeComponent();
        }
         

        protected override void OnStart(string[] args)
        {
            serverInstance = new MoviaRealtimeJourneyController();
        }

        protected override void OnStop()
        {
            serverInstance.Dispose();
        }
    }
}
