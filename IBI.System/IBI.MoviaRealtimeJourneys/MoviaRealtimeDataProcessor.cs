﻿using IBI.DataAccess.DBModels;
using IBI.Shared;
using IBI.Shared.ConfigSections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.MoviaRealtimeJourneys
{
    
    public partial class MoviaRealtimeDataProcessor
    {

        #region Variables
        StringBuilder sb = new StringBuilder();
        Log ParentLog { get; set; }
        Log CustomerLog { get; set; }
        List<JourneyRT> JourneysRT = new List<JourneyRT>();

        private string CustomerLogFileName { get; set; }
        private string ParentLogFileName { get; set; }

        public string GetCustomerLogFileName(int counter, int customerid)
        {
            return string.Format("MoviaRT_{0}_{1}", counter, customerid);
        }

        private int Counter { get; set; }
        public string TargetFile { get; set; }

        VdvSubscriptionConfiguration Config { get; set; }

        Dictionary<string, int> dictionary = new Dictionary<string, int>();// key is column name and index is value
        #endregion


        public MoviaRealtimeDataProcessor(string TargetFile, VdvSubscriptionConfiguration config, string parentLogFileName, int counter)
        {
            this.ParentLogFileName = parentLogFileName;
            this.Counter = counter;

            ParentLog = new Log(parentLogFileName);
            ParentLog.WriteLog(string.Format("Movia Real Time Data Processor initiated to read data from files under [{0}]", TargetFile));

            this.Config = config;
            this.TargetFile = TargetFile;

        }


        public bool DoProcess()
        {
            bool bParsingCompleted = false;
            if (File.Exists(TargetFile))
            {
                bParsingCompleted = ProcessFile(TargetFile);
               
                //save to DB
                if (bParsingCompleted) SaveJourneys2DB();
            }
            return bParsingCompleted;
        }

       
        #region Private Helper
        private int getColumnIndex(string key)
        {
            int indx = 0;
            if (dictionary.ContainsKey(key))
            {
                indx = dictionary[key];
            }
            return indx;
        }

        private bool ProcessFile(string FilePath)
        {
            try
            {
                //Console.WriteLine("ParseCSV IO Started");
                List<string> lines = (File.ReadAllLines(FilePath, Encoding.Default)
                    // leave blank lines
                    .Where(line => !string.IsNullOrEmpty(line))
                  //  .Skip(1)
                .ToList<string>());// convert to list
                //Console.WriteLine("ParseCSV IO Ended");

                // Console.WriteLine("ParseCSV Parsing data...");
                //empty existing list if any
                JourneysRT.Clear();
                dictionary.Clear();
                char delimiter = ';';

                // Parse header and fill dictionary
                var firstOrDefaultRow = lines.Any()?lines.FirstOrDefault():null;
                if (firstOrDefaultRow != null)
                {
                    string[] columnNames = firstOrDefaultRow.Split(delimiter);
                    int colNo = 0;
                    foreach (var columnName in columnNames)
                    {
                        dictionary.Add(columnName, colNo++);
                    }
                }

                // test print
                //foreach (var val in dictionary)
                //{
                //    Console.WriteLine("{0}-{1}",val.Key, val.Value);
                //}
                //Console.WriteLine("lines count {0}", lines.Count());

             //   Dictionary<string, int> tmpJCount = new Dictionary<string, int>();

              //  int ccc = 0;
             //   int stcnt = 0;
                if(lines.Any())
                foreach (var line in lines.Skip(1))
                {
                    

                   // Console.WriteLine("{0}",line.ToString());

                    string[] values = line.Split(delimiter);

                    if (dictionary.Count() != values.Count())
                    {
                        ParentLog.WriteLog(String.Format("This line does not have same no of columns as in header row {0}, skipped.", line.ToString()));
                        continue;
                    }


                    string StopPointName = values[getColumnIndex("StopPointName")];
                    if(StopPointName.Length<=0)
                    {
                        //ParentLog.WriteLog(String.Format("This line is skipped due to empty stop name"));
                        continue;
                    }

                    bool doSave = false;
                   // int customerId = 0;
                    string DepartmentCode = values[getColumnIndex("DepartmentCode")].Trim().Length >= 1 ? values[getColumnIndex("DepartmentCode")].Trim().ToUpper() : "EMPTY";
                    string LineDesignation = values[getColumnIndex("LineDesignation")];
                    foreach (VdvCustomerConfiguration customerConfig in Config.Customers)
                    {
                        //----------------

                        // =>Line,LineDesignation
                        //if ((customerConfig.LineFilter.Count() == 0 || customerConfig.LineFilter.Any(l => l.ToString() == journey.LineDesignation)) && customerConfig.SaveDataInDB == true)
                        if ((customerConfig.DepartmentFilter.Count() == 0 || customerConfig.DepartmentFilter.Any(l => l.ToString() == DepartmentCode)) && (customerConfig.LineFilter.Count() == 0 || customerConfig.LineFilter.Any(l => l.ToString() == LineDesignation)) && customerConfig.SaveDataInDB == true)
                        {
                            doSave = true;
                           // customerId = customerConfig.CustomerId;
                            break;
                        }
                        //----------------

                    }

                    if (!doSave)
                    {
                        //jCounter++;
                        continue;
                    }

                    ////////////////////////////////////////////////////////////////////////


                    string jKey = values[getColumnIndex("OperatingDayDate")]+values[getColumnIndex("JourneyPatternId")] + values[getColumnIndex("JourneyNumber")];
                    //check if this journey already exists
                    //var JRT = JourneysRT.FirstOrDefault(x => (
                    //    x.JourneyPatternId == System.Convert.ToInt64(values[getColumnIndex("JourneyPatternId")]).ToString()

                    //    //&& x.OperatingDayDate == System.Convert.ToDateTime(values[getColumnIndex("OperatingDayDate")])
                    //    && x.JourneyNumber.ToString() == values[getColumnIndex("JourneyNumber")]
                    //    ));

                    var JRT = JourneysRT.Find(item => item.key == jKey);
                        

                    if (JRT == null)
                    {
                        //if (ccc > 2)
                        //    break;
                        //ccc += 1;
                        

                        // Create new Journey
                        JRT = new JourneyRT();
                        //empty journey stops
                        JRT.JourneyStops.Clear();

                        JRT.key = jKey;
                        JRT.OperatingDayDate = System.Convert.ToDateTime(values[getColumnIndex("OperatingDayDate")]);
                        JRT.LineNumber = System.Convert.ToInt32(values[getColumnIndex("LineNumber")]);
                        JRT.LineDesignation = values[getColumnIndex("LineDesignation")];
                        JRT.DirectionCode = System.Convert.ToInt32(values[getColumnIndex("DirectionCode")]);
                        JRT.JourneyNumber = System.Convert.ToInt32(values[getColumnIndex("JourneyNumber")]);
                        JRT.JourneyPatternId = values[getColumnIndex("JourneyPatternId")];
                        JRT.PlannedStartDateTime = System.Convert.ToDateTime(values[getColumnIndex("PlannedStartDateTime")]);
                        JRT.PlannedEndDateTime = System.Convert.ToDateTime(values[getColumnIndex("PlannedEndDateTime")]);
                        JRT.PrimaryDestinationName =  values[getColumnIndex("PrimaryDestinationName")];
                        JRT.PrimaryDestinationShortName = values[getColumnIndex("PrimaryDestinationShortName")];
                        
                        //JRT.SecondaryDestinationName = values[getColumnIndex("SecondaryDestinationName")].Trim();//values[getColumnIndex("SecondaryDestinationName")].Length>0?values[getColumnIndex("SecondaryDestinationName")]:"NULL";
                        JRT.SecondaryDestinationName = (values[getColumnIndex("SecondaryDestinationName")]).Length>0?values[getColumnIndex("SecondaryDestinationName")]:null;
                        
                        JRT.SecondaryDestinationShortName = values[getColumnIndex("SecondaryDestinationShortName")];
                        JRT.ContractorCode = values[getColumnIndex("ContractorCode")];
                        JRT.ContractorName = values[getColumnIndex("ContractorName")];
                        JRT.DepartmentCode = values[getColumnIndex("DepartmentCode")].Trim().Length >= 1 ? values[getColumnIndex("DepartmentCode")].Trim().ToUpper() : "EMPTY";
                        JRT.DepartmentName = values[getColumnIndex("DepartmentName")];

                        //Additional hardcoded
                        JRT.ScheduleNumber = 0;
                        JRT.BusNumber = "0";
                        JRT.StartPoint = values[getColumnIndex("StopPointName")]; //StopPointNumber & StopPointName starting first stop

                        // Add journey to journeys
                        JourneysRT.Add(JRT);

                      //  Console.CursorLeft = 0;
                       // Console.Write("{0}", ++stcnt);
                       // tmpJCount.Add(JRT.OperatingDayDate.ToString("yyyyMMdd") + JRT.JourneyPatternId + JRT.JourneyNumber, stcnt);
                    }

                    //Add stops to this journey
                    JourneyStopsRT JRTS = new JourneyStopsRT();
                    JRTS.SequenceNumber = System.Convert.ToInt32(values[getColumnIndex("SequenceNumber")]);
                    JRTS.StopPointNumber = System.Convert.ToInt32(values[getColumnIndex("StopPointNumber")]);
                    JRTS.StopPointName = values[getColumnIndex("StopPointName")];
                    JRTS.TimingPoint = System.Convert.ToBoolean(values[getColumnIndex("TimingPoint")]);
                    JRTS.ZoneNumber = values[getColumnIndex("ZoneNumber")];
                    JRTS.CoordinateSystemName = values[getColumnIndex("CoordinateSystemName")];//same
                    JRTS.NorthingCoordinate = System.Convert.ToDecimal(values[getColumnIndex("NorthingCoordinate")]);
                    JRTS.EastingCoordinate = System.Convert.ToDecimal(values[getColumnIndex("EastingCoordinate")]);
                    JRTS.Latitude = values[getColumnIndex("Latitude")];
                    JRTS.Longitude = values[getColumnIndex("Longitude")];
                    JRTS.PlannedArrivalDateTime = System.Convert.ToDateTime(values[getColumnIndex("PlannedArrivalDateTime")]);
                    JRTS.PlannedDepartureDateTime = System.Convert.ToDateTime(values[getColumnIndex("PlannedDepartureDateTime")]);

                    //Add this stop to Journey
                    JRT.JourneyStops.Add(JRTS);

                  //  Console.WriteLine("\t\t\t{0}----{1}", JRTS.StopPointNumber, JRTS.StopPointName);
                  //  stcnt += 1;

                   // tmpJCount[JRT.OperatingDayDate.ToString("yyyyMMdd") + JRT.JourneyPatternId + JRT.JourneyNumber] = stcnt;

                 //   break;

                } //foreach

             //   Console.WriteLine("Stop counts in main loop {0}", stcnt);

                ParentLog.WriteLog(String.Format("Process of parsing journeys completed with jouneys count {0}", JourneysRT.Count()));

                //test
                //int c = 0;
                //foreach (var jj in tmpJCount)
                //{
                //    Console.WriteLine("\t{0}-{1}", jj.Key, jj.Value);
                //    if (c++ > 10)
                //        break;
                //}

                //int outerlimit = 0;
                ////  stcnt = 0;
                //foreach (var j in JourneysRT)
                //{
                //    Console.WriteLine("\t\t\t{0}----{1}",j.JourneyPatternId, j.JourneyStops.Count());
                //    //int innerlimit = 0;
                //    //foreach (var s in j.JourneyStops)
                //    //{
                //    //    //    stcnt += 1;
                //    //    Console.WriteLine("\t\t\t{0}----{1}", s.StopPointNumber, s.StopPointName);
                //    //    //if (innerlimit++ > 3)
                //    //    //    break;
                //    //}

                //    if (outerlimit++ > 10)
                //        break;
                //}

             //   Console.WriteLine("Stop counts in test loop {0}", stcnt);

                return true;
            }
            catch (Exception ex)
            {
                //throw new Exception("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                //Console.WriteLine("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                ParentLog.WriteLog(ex);
            }

            return false;

        } //ParseCSV

        private bool SaveJourneys2DB()
        {

            StringBuilder log = new StringBuilder();
            CultureInfo provider = CultureInfo.InvariantCulture;
            //int index = 0;

            ParentLog.WriteLog("Process of saving journeys into IBI System started ... ");

            try
            {
                using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                {

                    //global list of All stops from database
                    var allDBStops = dbContext.Stops.Where(s => s.StopSource == "MOVIA").ToList();

                    int jCounter = 1;
                    foreach (JourneyRT journey in JourneysRT)
                    {

                        bool doSave = false;
                        int customerId = 0;



                        foreach (VdvCustomerConfiguration customerConfig in Config.Customers)
                        {
                            //----------------

                            // =>Line,LineDesignation
                            //if ((customerConfig.LineFilter.Count() == 0 || customerConfig.LineFilter.Any(l => l.ToString() == journey.LineDesignation)) && customerConfig.SaveDataInDB == true)
                            if ((customerConfig.DepartmentFilter.Count() == 0 || customerConfig.DepartmentFilter.Any(l => l.ToString() == journey.DepartmentCode)) && (customerConfig.LineFilter.Count() == 0 || customerConfig.LineFilter.Any(l => l.ToString() == journey.LineDesignation)) && customerConfig.SaveDataInDB == true)
                            {
                                doSave = true;
                                customerId = customerConfig.CustomerId;
                                break;
                            }
                            //----------------

                        }

                        if (!doSave)
                        {
                            //jCounter++;
                            continue;
                        }

                        StringBuilder sbJourney = new StringBuilder();
                        StringBuilder sbStops = new StringBuilder();


                        

                        VdvCustomerConfiguration custConfig = Config.Customers.Where(c => c.CustomerId == customerId).FirstOrDefault();

                        //=> string externalRef = String.Format("{0}-{1}-{2}", Config.ExternalReferencePrefix, journey.JourneyDate.ToString("yyyyMMdd"), journey.TripId); //logic of external ref from MAR

                        string externalRef = String.Format("{0}-{1}-{2}-{3}", Config.ExternalReferencePrefix, journey.OperatingDayDate.ToString("yyyyMMdd"), journey.JourneyPatternId, journey.JourneyNumber); //logic of external ref from MAR

                        sbJourney.AppendLine(String.Format("## Journey # {0} ##", jCounter++.ToString()));

                        bool exists = dbContext.Journeys.Where(j => j.ExternalReference == externalRef).Count() > 0;

                        //----------------
                        if (exists)
                        {
                            // =>journey.TripId,
                            sbJourney.AppendLine(String.Format("## Trip # {0}-{1} already exists / imported to Journeys table ##\n", journey.LineDesignation, journey.LineNumber.ToString()));
                           // jCounter++;

                           // Console.WriteLine(String.Format("## Trip # {0}-{1} already exists / imported to Journeys table ##\n", journey.LineDesignation, journey.LineNumber.ToString()));

                            continue;
                        }
                        //----------------



                        IBI.DataAccess.DBModels.Journey dbJourney = new DataAccess.DBModels.Journey();


                        int stopSequence = 1;
                        List<JourneyStop> jStops = new List<JourneyStop>();

                        foreach (JourneyStopsRT stop in journey.JourneyStops)
                        {
                            JourneyStop jStop = new JourneyStop();
                            jStop.Timestamp = DateTime.Now;

                            // =>StopNumber,StopPointNumber
                            decimal ibiStopNumber = IBI.Shared.AppUtility.IsNullDec(ScheduleHelper.TransformStopNo(stop.StopPointNumber.ToString(), ScheduleHelper.StopPrefix.MOVIA));

                            bool getLiveZone = bool.Parse(ConfigurationManager.AppSettings["GetLiveZonesForStop"].ToString());

                            // =>if (getLiveZone && stop.Zone == "0" && String.IsNullOrEmpty(stop.StopName) == false)
                            // =>{
                            // =>   stop.ZoneNumber = IBI.Cache.JourneyCache.GetZoneName(stop.Latitude, stop.Longitude, true, dbContext);
                            // =>}


                            if (getLiveZone && stop.ZoneNumber == "0" && String.IsNullOrEmpty(stop.StopPointName) == false)
                            {
                                stop.ZoneNumber = IBI.Cache.JourneyCache.GetZoneName(stop.Latitude, stop.Longitude, true, dbContext);
                            }

                            if (stop.ZoneNumber == "0")
                            {
                                stop.ZoneNumber = "";
                            }

                            //Console.Write(stop.Latitude + ", " + stop.Longitude);

                            //var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude.Replace(',', '.'), stop.Latitude.Replace(',', '.'));
                            var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude, stop.Latitude);
                            jStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                            jStop.StopId = ibiStopNumber;

                            //=> jStop.StopName = stop.StopName;
                            jStop.StopName = stop.StopPointName;

                            jStop.StopSequence = stopSequence++;
                            //jStop.StopSequence = stop.SequenceNumber;

                            // => DateTime plannedDepartureTime = String.IsNullOrEmpty(stop.DepartureTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + stop.DepartureTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);
                            //DateTime plannedArrivalTime = String.IsNullOrEmpty(stop.ArrivalTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + stop.ArrivalTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);

                            //if (plannedDepartureTime != DateTime.MinValue)
                            //{
                            //    jStop.PlannedDepartureTime = plannedDepartureTime.AddDays(stop.Day2addDeparture);
                            //}

                            //if (plannedArrivalTime != DateTime.MinValue)
                            //{
                            //    jStop.PlannedArrivalTime = plannedArrivalTime.AddDays(stop.Day2addArrival);
                            //} =>

                            jStop.PlannedDepartureTime = stop.PlannedDepartureDateTime;
                            jStop.PlannedArrivalTime = stop.PlannedArrivalDateTime;

                            //=>jStop.Zone = stop.Zone;
                            jStop.Zone = stop.ZoneNumber;
                            jStop.IsOnline = false;
                            jStop.Timestamp = DateTime.Now;

                            //Console.WriteLine(stop.PlannedArrivalDateTime.ToString("yyyy-MM-ddTHH:mm") + " || " + stop.PlannedDepartureDateTime.ToString("yyyy-MM-ddTHH:mm"));

                            ////SYNC STOPS *******************************************************************
                            bool syncStops = bool.Parse(ConfigurationManager.AppSettings["SyncStops"]);

                            if (syncStops)
                            {
                                try
                                {
                                    var st = allDBStops.Where(s => s.GID == ibiStopNumber).FirstOrDefault();
                                    if (st == null)
                                    {
                                        DataAccess.DBModels.Stop newStop = new DataAccess.DBModels.Stop()
                                        {
                                            StopName = jStop.StopName,
                                            GID = jStop.StopId,
                                            StopGPS = jStop.StopGPS,
                                            StopSource = "MOVIA",
                                            OriginalCreated = DateTime.Now,
                                            OriginalName = jStop.StopName,
                                            OriginalGPS = jStop.StopGPS,
                                            LastUpdated = DateTime.Now
                                        };

                                        dbContext.Stops.Add(newStop);
                                        allDBStops.Add(newStop);//added to global list for better next comparison.
                                        dbContext.SaveChanges();
                                    }
                                    else
                                    {
                                        if (st.StopName != jStop.StopName)
                                        {
                                            //update
                                            st.StopName = jStop.StopName;
                                            st.StopGPS = jStop.StopGPS;
                                            st.StopSource = "MOVIA";
                                            st.LastUpdated = DateTime.Now;

                                            dbContext.SaveChanges();
                                        }
                                    }


                                    jStops.Add(jStop);
                                    //sbStops.AppendLine(string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5} GPS: {6} {7}",
                                    //            jStop.StopId, jStop.StopName, stopSequence, jStop.Zone, jStop.PlannedDepartureTime.ToString(), jStop.PlannedArrivalTime.ToString(), jStop.StopGPS.Latitude.ToString(), jStop.StopGPS.Longitude.ToString()));

                                }
                                catch (Exception exStops)
                                {
                                    //throw new Exception(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
                                    //=> sbStops.AppendLine(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
                                    sbStops.AppendLine(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopPointNumber, stop.StopPointName));

                                    //Console.WriteLine(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopPointNumber, stop.StopPointName));


                                }
                            }
                            ////*********************************************************************************************

                        }// end stops loop

                        
                        //=> string destinationAfterRewrite = journey.Destination;
                        string destinationAfterRewrite = journey.PrimaryDestinationName;

                        int scheduleId = 0;
                        if (journey.ScheduleNumber == 0)
                        {
                            //dbJourney.ScheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(CustomerId, journey.Line, journey.StartPoint, ref destinationAfterRewrite, journey.ViaName, dbJourney.JourneyStops, ref log, true);
                            //=> scheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(custConfig, 0, customerId, journey.Line, journey.StartPoint, journey.Destination, journey.ViaName, jStops, ref sbJourney, ref destinationAfterRewrite, true);
                            scheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(custConfig, 0, customerId, journey.LineDesignation, journey.StartPoint, journey.PrimaryDestinationName, journey.SecondaryDestinationName, jStops, ref sbJourney, ref destinationAfterRewrite, true);
                        }

                    //    Console.WriteLine("\t\t\t Schedule added. scheduleId={0}, customerId={1}, journey.LineDesignation={2}, journey.StartPoint={3}, journey.PrimaryDestinationName={4}, journey.SecondaryDestinationName={5}, jStops.Count()={6}", scheduleId, customerId, journey.LineDesignation, journey.StartPoint, journey.PrimaryDestinationName, journey.SecondaryDestinationName, jStops.Count());

                        ////existing future journey CHECK ***********************
                        bool updateExistingJourneys = bool.Parse(ConfigurationManager.AppSettings["UpdateExistingJourneys"]);
                        if (updateExistingJourneys)
                        {
                            decimal firstStopId = jStops.FirstOrDefault().StopId;
                            
                            //=> var existingJourney = dbContext.GetExistingFutureJourney(customerId, externalRef, journey.Line, destinationAfterRewrite, journey.PlannedDepartureTime, firstStopId).FirstOrDefault();
                            var existingJourney = dbContext.GetExistingFutureJourney(customerId, externalRef, journey.LineDesignation, destinationAfterRewrite, journey.PlannedStartDateTime, firstStopId).FirstOrDefault();

                            if (existingJourney != null)
                            {
                                dbJourney = existingJourney;
                                dbJourney.JourneyStops.Clear();
                            }
                        }
                        ////*******************************************************

                        dbJourney.JourneyStops = jStops;
                        dbJourney.ScheduleId = scheduleId;
                        dbJourney.BusNumber = journey.BusNumber;
                        dbJourney.CustomerId = customerId;
                        dbJourney.JourneyStateId = 0;

                        //=>dbJourney.PlannedEndTime = journey.PlannedArrivalTime;
                        //=>dbJourney.PlannedStartTime = journey.PlannedDepartureTime;

                        dbJourney.PlannedEndTime = journey.PlannedEndDateTime;
                        dbJourney.PlannedStartTime = journey.PlannedStartDateTime;
                        dbJourney.LastUpdated = DateTime.Now;
                        dbJourney.ExternalReference = externalRef;


                        // =>sbJourney.AppendLine(String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, RewriteDestination: {8}, Total Stops: {9}",
                        //               (dbJourney.JourneyId > 0 ? "UPDATED" : "INSERTED"), dbJourney.JourneyId, dbJourney.CustomerId, dbJourney.ExternalReference, dbJourney.ScheduleId, journey.Line, journey.StartPoint, journey.Destination, destinationAfterRewrite, jStops.Count()));
                        //=>sbJourney.AppendLine(sbStops.ToString());

                        sbJourney.AppendLine(String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, RewriteDestination: {8}, Total Stops: {9}, Department:{10}",
                                    (dbJourney.JourneyId > 0 ? "UPDATED" : "INSERTED"), dbJourney.JourneyId, dbJourney.CustomerId, dbJourney.ExternalReference, dbJourney.ScheduleId, journey.LineDesignation, journey.StartPoint, journey.PrimaryDestinationName, destinationAfterRewrite, jStops.Count(), journey.DepartmentCode));
                        //sbJourney.AppendLine(sbStops.ToString());

                       // Console.WriteLine(String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, RewriteDestination: {8}, Total Stops: {9}",
                            //        (dbJourney.JourneyId > 0 ? "UPDATED" : "INSERTED"), dbJourney.JourneyId, dbJourney.CustomerId, dbJourney.ExternalReference, dbJourney.ScheduleId, journey.LineDesignation, journey.StartPoint, journey.PrimaryDestinationName, destinationAfterRewrite, jStops.Count()));

                        if (dbJourney.JourneyId == 0)
                        {
                            dbContext.Journeys.Add(dbJourney);
                        }

                        try
                        {
                            //Save new Journey to DB                    
                            dbContext.SaveChanges();
                            sbJourney.AppendLine(string.Format("Journey {0} saved successfully to DB", dbJourney.JourneyId));
                           // Console.WriteLine(string.Format("Journey {0} saved successfully to DB", dbJourney.JourneyId));
                        }
                        catch (Exception exJourney)
                        {
                            sbJourney.AppendLine(string.Format("ERROR !!! Failed to save Journey [{0}]", IBI.Shared.Diagnostics.Logger.GetDetailedError(exJourney)));
                           // Console.WriteLine(string.Format("ERROR !!! Failed to save Journey [{0}]", IBI.Shared.Diagnostics.Logger.GetDetailedError(exJourney)));
                        }


                    //    Console.WriteLine("\t Journey {0} saved successfully to DB", dbJourney.JourneyId);


                        Log custLog = new Log(GetCustomerLogFileName(Counter, customerId), sbJourney);
                        custLog.WriteLog(sbJourney.ToString());

                        custLog = null;

                        sbJourney.Clear();
                        sbStops.Clear();

                        System.Threading.Thread.Sleep(20);
                    }

                    ParentLog.WriteLog("All journeys successfully imported to the database");
                    //MessageBox.Show("All journeys successfully imported to the database");
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                //Console.WriteLine("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                ParentLog.WriteLog(ex);

            }


            return true;
        }

        

        #endregion

    }

}




































// Hashtable hexaToBinary;

//  List<Journey> Journeys { get; set; }
//  List<KeyValuePair<string, string>> StopNamesCache { get; set; } //value: stopname
//   List<KeyValuePair<string, string>> StopCoordCache { get; set; } //value: lat|lon
//List<CorrectedStop> CorrectedStopsCache { get; set; } //value: lat|lon

//    List<String> Lines { get; set; }

//public int CustomerId { get; set; }

//public string FPlanFilepath { get; set; }
//public string StopCoordFilepath { get; set; }
//public string StopNamesFilepath { get; set; }
//public string OperationDaysFilepath { get; set; }
//public string JourneyDurationFilepath { get; set; }
//public string CorrectedStopsFilepath { get; set; }

//string[] FPlanData { get; set; }
//string[] StopNamesData { get; set; }
//string[] StopCoordData { get; set; }
//string[] JourneyDurationData { get; set; }
//string[] OperationDaysData { get; set; }
//string[] CorrectedStopsData { get; set; }



//public class CorrectedStop
//{

//    public int StopNumber { get; set; }
//    public int MainStop { get; set; }
//    public int Radius { get; set; }
//    public string Longitude { get; set; }
//    public string Latitude { get; set; }
//    public int LaneManagement { get; set; }
//    public int CorrespondanceStop { get; set; }
//    public string StopName { get; set; }

//    public static List<CorrectedStop> CacheCorrectedStopsData(string[] lines)
//    {

//        List<CorrectedStop> data = new List<CorrectedStop>();

//        foreach (string line in lines)
//        {
//            CorrectedStop stopData = new CorrectedStop();
//            stopData.StopNumber = int.Parse(line.Substring(0, 9).Trim());
//            stopData.MainStop = int.Parse(line.Substring(11, 1).Trim());
//            stopData.Radius = int.Parse(line.Substring(14, 4).Trim());

//            //if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "da-DK")
//            //{

//            //    stopData.Longitude = decimal.Parse(line.Substring(20, 9).Trim().Replace('.', ','));
//            //    stopData.Latitude = decimal.Parse(line.Substring(31, 9).Trim().Replace('.', ','));
//            //}
//            //else 
//            { 
//                stopData.Longitude = line.Substring(20, 9).Trim();
//                stopData.Latitude = line.Substring(31, 9).Trim();
//            }

//            stopData.LaneManagement = int.Parse(line.Substring(42, 1).Trim());
//            stopData.CorrespondanceStop = int.Parse(line.Substring(45, 1).Trim());
//            stopData.StopName = line.Substring(50).Trim();

//            data.Add(stopData);

//            //Console.WriteLine("Corrected Long:" + stopData.Longitude);

//            ////if(stopData.StopName == "Skanderborg St.")
//            //{
//            //    Console.WriteLine(string.Format("StopName: {0}, Latitude: {1}, Longitude: {2}", stopData.StopName,line.Substring(31, 9).Trim(), line.Substring(20, 9).Trim()));
//            //}
//        }

//        return data;
//    }
//}

//constructor starts
////Clear existing Cache elements
//IBI.Cache.JourneyCache.ClearZonesCache();
//IBI.Cache.JourneyCache.ClearSchedulesCache();
//IBI.Cache.JourneyCache.ClearRouteCache();

//////Fill cache elements from DB
//IBI.Cache.JourneyCache.FillRouteDirections();
//IBI.Cache.JourneyCache.FillSchedules();


//Journeys = new List<Journey>();

// StopNamesCache = new List<KeyValuePair<string, string>>();
//StopCoordCache = new List<KeyValuePair<string, string>>();
//CorrectedStopsCache = new List<CorrectedStop>();


//Lines = new List<String>();

//#region Hexa2Binary Functions

//public string HexToBinary(string hexValue)
//{

//    ulong number = UInt64.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
//    return Convert.ToString((int)number, 2);
//}

//private void initializeHexToBinary()
//{
//    hexaToBinary = new Hashtable();
//    hexaToBinary["0"] = HexToBinary("0");
//    hexaToBinary["1"] = HexToBinary("1");
//    hexaToBinary["2"] = HexToBinary("2");
//    hexaToBinary["3"] = HexToBinary("3");
//    hexaToBinary["4"] = HexToBinary("4");
//    hexaToBinary["5"] = HexToBinary("5");
//    hexaToBinary["6"] = HexToBinary("6");
//    hexaToBinary["7"] = HexToBinary("7");
//    hexaToBinary["8"] = HexToBinary("8");
//    hexaToBinary["9"] = HexToBinary("9");
//    hexaToBinary["A"] = HexToBinary("A");
//    hexaToBinary["B"] = HexToBinary("B");
//    hexaToBinary["C"] = HexToBinary("C");
//    hexaToBinary["D"] = HexToBinary("D");
//    hexaToBinary["E"] = HexToBinary("E");
//    hexaToBinary["F"] = HexToBinary("F");
//}


//#endregion


//private void ProcessFile()
//{
//ParentLog.WriteLog("Extracting data from fplan file");

//StringBuilder log = new StringBuilder();

//if (String.IsNullOrEmpty(FPlanFilepath) || !File.Exists(FPlanFilepath))
//{
//    throw new Exception("Please provide future plan path");
//}
//if (String.IsNullOrEmpty(StopNamesFilepath) || !File.Exists(StopNamesFilepath))
//{
//    throw new Exception("Please provide stop name file path");
//}
//if (String.IsNullOrEmpty(StopCoordFilepath) || !File.Exists(StopCoordFilepath))
//{
//    throw new Exception("Please provide stop coord file path");
//}
//if (String.IsNullOrEmpty(OperationDaysFilepath) || !File.Exists(OperationDaysFilepath))
//{
//    throw new Exception("Please provide operationdays file path");
//}
//if (String.IsNullOrEmpty(JourneyDurationFilepath) || !File.Exists(JourneyDurationFilepath))
//{
//    throw new Exception("Please provide journey duration file path");
//}
//if (String.IsNullOrEmpty(CorrectedStopsFilepath) || !File.Exists(CorrectedStopsFilepath))
//{
//    throw new Exception("Please provide correctedstops file path");
//}



////Clear existing Cache elements
//IBI.Cache.JourneyCache.ClearZonesCache();
//IBI.Cache.JourneyCache.ClearSchedulesCache();
//IBI.Cache.JourneyCache.ClearRouteCache();

//////Fill cache elements from DB
//IBI.Cache.JourneyCache.FillRouteDirections(this.CustomerId);
//IBI.Cache.JourneyCache.FillSchedules(this.CustomerId);


//FPlanData = File.ReadAllLines(FPlanFilepath, Encoding.Default);

//List<String> lstplan = FPlanData.ToList();
//lstplan.Add("*Z");
//FPlanData = lstplan.ToArray();

//ParentLog.WriteLog("Reading & caching data from HAFAS files");

//StopNamesData = File.ReadAllLines(StopNamesFilepath, Encoding.Default);
//StopCoordData = File.ReadAllLines(StopCoordFilepath, Encoding.Default);
//JourneyDurationData = File.ReadAllLines(JourneyDurationFilepath, Encoding.Default);
//OperationDaysData = File.ReadAllLines(OperationDaysFilepath, Encoding.Default);
//CorrectedStopsData = File.ReadAllLines(CorrectedStopsFilepath, Encoding.Default);

//CorrectedStopsCache = CorrectedStop.CacheCorrectedStopsData(CorrectedStopsData);

//CultureInfo provider = CultureInfo.InvariantCulture;

//initializeHexToBinary();


//bool journeyEnds = false;
//string operationDaysReference = string.Empty;
//DateTime journeyDurationStartTime = DateTime.ParseExact(JourneyDurationData[2].Split(' ')[0], "dd.MM.yyyy", provider);
//DateTime journeyDurationEndTime = DateTime.ParseExact(JourneyDurationData[3].Split(' ')[0], "dd.MM.yyyy", provider);

//string journeyStartTime = string.Empty;
//string journeyEndTime = string.Empty;

//ArrayList journeyOptions = new ArrayList();
////List<JourneyStop> journeyStops = new List<JourneyStop>();// new ArrayList();
//ArrayList journeyStops = new ArrayList();

//Lines.Clear();

//string jLine = "";

//int stopSequence = 1;


//ParentLog.WriteLog("Extracting journeys from fplan files");

//try
//{
//    string currentTripId = string.Empty;


//    foreach (string line in FPlanData)
//    {


//        string lineCode = line.Length > 0 ? line.Substring(0, 2) : "";

//        if (lineCode == "")
//            continue;


//        switch (lineCode)
//        {
//            case "*G":
//            //TODO
//            case "*R":
//                //TODO
//                break;
//            case "*F":
//                //TODO - check if it is valid line
//                break;
//            case "*I":
//                //TODO
//                break;
//            case "*C":
//                //TODO
//                break;
//            case "*A":
//                journeyOptions.Add(line);
//                break;
//            case "*L":
//                string[] lineJourneyElements = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
//                jLine = lineJourneyElements[1];

//                if (String.IsNullOrEmpty(Lines.Find(l => l == jLine)))
//                    Lines.Add(jLine);

//                break;
//            case "*Z":
//                if (journeyEnds)
//                {
//                    // journey ends
//                    foreach (string optionLine in journeyOptions)
//                    {
//                        string[] lineElements = optionLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
//                        operationDaysReference = lineElements[4];
//                        bool bAlldays = operationDaysReference.Equals("000000");
//                        string bitValues = string.Empty; //365 char string to hold indexes of days
//                        if (!bAlldays)
//                        {
//                            var opDaysLine = Array.Find(OperationDaysData, s => s.StartsWith(operationDaysReference));
//                            if (opDaysLine != null)
//                            {
//                                // 3E7CF8000000000000300000000000000000000000000000000000000000000000000000000000000000000000000
//                                string[] opDataColumns = opDaysLine.ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
//                                string daysHexValue = opDataColumns[1];//.Substring(4);

//                                //DateTime journeyStartDateTime = DateTime.Parse(journeyStartTime, "dd.mm.yyyy");

//                                //for (int i = 0; i < daysHexValue.Length; i++)
//                                //{
//                                //    bitValues += hexaToBinary[daysHexValue.Substring(i, 1)];
//                                //    if (bitValues.Length >= (journeyDurationEndTime - journeyDurationStartTime).TotalDays)
//                                //    {
//                                //        break;
//                                //    }
//                                //}

//                                bitValues = String.Join(String.Empty,
//                                  daysHexValue.Select(
//                                    c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')
//                                  )
//                                );

//                                //we need to discard first 2 and last 2 digits (according to hafas documentation) 
//                                bitValues = bitValues.Length>5 ? bitValues.Substring(2, bitValues.Length - 2) : "0"; 

//                            }
//                        }

//                        journeyStartTime = lineElements[5].Substring(1);
//                        journeyEndTime = lineElements[6].Substring(1);
//                        int jDays2AddStart = 0;
//                        int jDays2AddEnd = 0;

//                        journeyStartTime = Adjust24HourTime(journeyStartTime, ref jDays2AddStart);
//                        journeyEndTime = Adjust24HourTime(journeyEndTime, ref jDays2AddEnd);

//                        // enter journey into DB/List/Grid
//                        int dateIndex = 0;
//                        DateTime journeyDate = journeyDurationStartTime;
//                        //for (DateTime dtStart = journeyDurationStartTime; dateIndex < (journeyDurationEndTime - journeyDurationStartTime).TotalDays; dateIndex++)
//                        for (DateTime dtStart = journeyDurationStartTime; dateIndex < bitValues.Length; dateIndex++)
//                        {
//                            if (bAlldays || bitValues[dateIndex] == '1')
//                            {


//                                Journey journey = new Journey();

//                                journey.JourneyDate = journeyDurationStartTime.AddDays(dateIndex);

//                                string jStartPoint = GetStopName(lineElements[2]); //StopNamesData.First(s => s.Split(' ')[0] == lineElements[2]).Split(' ')[2]; 
//                                string jDestination = GetStopName(lineElements[3]); //StopNamesData.First(s => s.Split(' ')[0] == lineElements[3]).Split(' ')[2];

//                                //Console.WriteLine(journey.JourneyDate.ToString("MMddyyyy") + " " + journeyStartTime);
//                                //22-10-2015 00:00:00 0505
//                                DateTime jPlannedDeparture = String.IsNullOrEmpty(journeyStartTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + journeyStartTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);
//                                DateTime jPlannedArrival = String.IsNullOrEmpty(journeyEndTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + journeyEndTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);

//                                if (jPlannedDeparture != DateTime.MinValue)
//                                {
//                                    jPlannedDeparture = jPlannedDeparture.AddDays(jDays2AddStart);
//                                }

//                                if (jPlannedArrival != DateTime.MinValue)
//                                {
//                                    jPlannedArrival = jPlannedArrival.AddDays(jDays2AddEnd);
//                                }

//                                //DateTime jPlannedDeparture = DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " +  journeyStartTime, "MM/dd/yyyy HHmm", provider);
//                                //DateTime jPlannedArrival = DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + journeyEndTime, "MM/dd/yyyy HHmm", provider);



//                                journey.BusNumber = "0";
//                                journey.CustomerId = 0;
//                                journey.JourneyNumber = 0;
//                                journey.StartTime = null;
//                                journey.EndTime = null;

//                                journey.StartPoint = jStartPoint;
//                                journey.Line = jLine;
//                                journey.Destination = jDestination;
//                                journey.PlannedDepartureTime = jPlannedDeparture;
//                                journey.PlannedArrivalTime = jPlannedArrival;
//                                journey.ScheduleNumber = 0;

//                                journey.TripId = currentTripId; //line.Substring(3, 6).Trim();


//                                //Add Stops to journey
//                                journey.JourneyStops.AddRange(journeyStops);


//                                if (journey.PlannedDepartureTime > DateTime.Now && journey.PlannedDepartureTime <= DateTime.Now.AddDays(Config.FutureDaysBuffer))
//                                {
//                                    this.Journeys.Add(journey);
//                                }

//                            }
//                            dtStart = dtStart.AddDays(1);

//                        }
//                    }
//                    //TODO
//                    // initialize journey variables
//                    journeyStartTime = string.Empty;
//                    journeyEndTime = string.Empty;
//                    journeyOptions.Clear();
//                    journeyStops.Clear();

//                }
//                else
//                {
//                    journeyEnds = true;                              

//                }

//                if (line.Length >= 10)
//                {
//                    currentTripId = line.Substring(3, 6).Trim();
//                }


//                break;
//            default:
//                {
//                    if (line.StartsWith("*"))
//                        continue;


//                    // hop line
//                    string stopNumber = line.Substring(0, 9); // stop length can be less than 9 - check it based on setting(PDF)

//                    string stopName = GetStopName(stopNumber); //StopNamesData.First(s => s.Split(' ')[0] == stopNumber).Split(' ')[2];
//                    string arrivalTime = line.Substring(32, 5).Substring(1).Trim();
//                    string departureTime = line.Substring(39, 5).Substring(1).Trim();
//                    int day2addArrival = 0;
//                    int day2addDeparture = 0;

//                    arrivalTime = Adjust24HourTime(arrivalTime, ref day2addArrival);
//                    departureTime = Adjust24HourTime(departureTime, ref day2addDeparture);

//                    string coord;
//                    string[] arrCoord;
//                    string lon;
//                    string lat;
//                    string zone = "0";
//                    string isCheckpoint = "false";

//                    CorrectedStop correctedStop = CorrectedStopsCache.Where(s => s.StopNumber == int.Parse(stopNumber)).FirstOrDefault();
//                    if (correctedStop != null)
//                    {

//                        lon = correctedStop.Longitude;  //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
//                        lat = correctedStop.Latitude;   //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];                                
//                        isCheckpoint = correctedStop.MainStop == 1 ? "true" : "false";


//                    }
//                    else
//                    {

//                        coord = GetStopCoord(stopNumber);
//                        arrCoord = coord.Split('|');
//                        lon = arrCoord.Length > 0 ? arrCoord[0] : "";  //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
//                        lat = arrCoord.Length > 1 ? arrCoord[1] : "";//StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];                                                                

//                        //if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "da-DK")
//                        //{ 
//                        //    lon = lon.Replace('.', ',');
//                        //    lat = lat.Replace('.', ',');

//                        //    if (lon.StartsWith("9"))
//                        //    {
//                        //        Console.WriteLine("Long:" + lon);
//                        //    }
//                        //}
//                    }

//                    journeyStops.Add(new StopHop(stopNumber, stopName, arrivalTime, departureTime, lat, lon, zone, isCheckpoint, day2addArrival, day2addDeparture));

//                    break;
//                }
//        }//end swtich


//    }//end foreach
//}
//catch (Exception ex)
//{
//   // Console.WriteLine("Problem parsing fPlan data lines. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
//    ParentLog.WriteLog("Problem parsing fPlan data lines. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
//}
//finally
//{
//    ParentLog.WriteLog(string.Format("Total {0} journeys found for now onwards to next {1} days", Journeys.Count(), Config.FutureDaysBuffer));
//    SaveJourneys2DB();
//}



//}


//private string GetStopName(string stopNumber)
//{
//    stopNumber = IBI.Shared.ScheduleHelper.UnTransformStopNo(stopNumber, Shared.ScheduleHelper.StopPrefix.MOVIA);

//    //search in cache first
//    var stop = StopNamesCache.FirstOrDefault(s => s.Key == stopNumber);
//    string stopName = "";

//    if (!String.IsNullOrEmpty(stop.Value))
//    {
//        stopName = stop.Value;
//    }
//    else
//    {
//        var sstop = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();

//        if (sstop != null)
//        {
//            //stopName = sstop.Split(' ')[2];
//            stopName = sstop.Substring(14);

//            int endIndex = stopName.IndexOf('(');
//            if (endIndex > 0)
//                stopName = stopName.Substring(0, endIndex).Trim();

//            StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
//        }
//        else
//        {
//            stopName = "";
//        }

//    }

//     return "";
//}

//private string GetStopName(string stopNumber, string stopName)
//{
//    stopNumber = IBI.Shared.ScheduleHelper.UnTransformStopNo(stopNumber, Shared.ScheduleHelper.StopPrefix.MOVIA);

//    //search in cache first
//    var stop = StopNamesCache.FirstOrDefault(s => s.Key == stopNumber);
//    string TempStopName = stopName;

//    if (!String.IsNullOrEmpty(stop.Value))
//    {
//        TempStopName = stop.Value;
//    }
//    else
//    {
//        //var sstop = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();

//        //if (sstop != null)
//        //{
//        //    //stopName = sstop.Split(' ')[2];
//        //    stopName = sstop.Substring(14);

//        //    int endIndex = stopName.IndexOf('(');
//        //    if (endIndex > 0)
//        //        stopName = stopName.Substring(0, endIndex).Trim();

//        //    StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
//        //}
//        //else
//        //{
//        //    stopName = "";
//        //}
//        StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
//    }

//    return TempStopName;
//}
//private string GetStopCoord(string stopNumber)
//{
//    //search in cache first
//    var stop = StopCoordCache.FirstOrDefault(s => s.Key == stopNumber);
//    string stopCoord = "|";
//    if (!String.IsNullOrEmpty(stop.Value))
//    {
//        stopCoord = stop.Value;
//    }
//    else
//    {
//        stopCoord = StopCoordData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();
//        if (!String.IsNullOrEmpty(stopCoord))
//        {
//            string[] arrCoord = stopCoord.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
//            string lat = arrCoord.Length > 1 ? arrCoord[1] : "";
//            string lon = arrCoord.Length > 2 ? arrCoord[2] : "";

//            stopCoord = lat + "|" + lon;
//            StopCoordCache.Add(new KeyValuePair<string, string>(stopNumber, stopCoord));
//        }
//        else
//        {
//            stopCoord = "|";
//        }

//    }


//    return stopCoord;

//}

//private string Adjust24HourTime(string time, ref int day2Add)
//{

//    if (!String.IsNullOrEmpty(time))
//    {
//        int t = 0;
//        bool tResult = int.TryParse(time, out t);

//        if (tResult)
//        {
//            if (t >= 2400)
//            {
//                day2Add = 1;
//                time = (t - 2400).ToString();
//                time = time.Length < 4 ? "00000" + time : time;
//                time = time.Substring(time.Length - 4);

//            }
//        }
//    }

//    return time;
//}

//private bool SaveJourneys2DB()
//{

//    //StringBuilder log = new StringBuilder();
//    //CultureInfo provider = CultureInfo.InvariantCulture;
//    //int index = 0;

//    //ParentLog.WriteLog("Process of saving journeys into IBI System started ... ");

//    //try
//    //{
//    //    using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
//    //    {

//    //        //global list of All stops from database
//    //        var allDBStops = dbContext.Stops.Where(s => s.StopSource == "HAFAS").ToList();

//    //        int jCounter = 1;
//    //        foreach (Journey journey in Journeys)
//    //        {

//    //            bool doSave = false;
//    //            int customerId = 0;



//    //            foreach (VdvCustomerConfiguration customerConfig in Config.Customers)
//    //            {
//    //                //----------------
//    //                if ( (customerConfig.LineFilter.Count()==0 || customerConfig.LineFilter.Any(l => l.ToString() == journey.Line))  && customerConfig.SaveDataInDB == true)
//    //                {
//    //                    doSave = true;
//    //                    customerId = customerConfig.CustomerId;
//    //                    break;
//    //                }
//    //                //----------------

//    //            }

//    //            if (!doSave)
//    //            {
//    //                //jCounter++;
//    //                continue;
//    //            }

//    //            StringBuilder sbJourney = new StringBuilder();
//    //            StringBuilder sbStops = new StringBuilder();


//    //            //Log custLog = new Log(GetCustomerLogFileName(Counter, customerId), sbJourney);

//    //            VdvCustomerConfiguration custConfig = Config.Customers.Where(c => c.CustomerId == customerId).FirstOrDefault();

//    //            string externalRef = String.Format("{0}-{1}-{2}", Config.ExternalReferencePrefix, journey.JourneyDate.ToString("yyyyMMdd"), journey.TripId); //logic of external ref from MAR

//    //            sbJourney.AppendLine(String.Format("## Journey # {0} ##", jCounter.ToString()));

//    //            bool exists = dbContext.Journeys.Where(j => j.ExternalReference == externalRef).Count() > 0;

//    //            //----------------
//    //            if (exists)
//    //            {
//    //                sbJourney.AppendLine(String.Format("## Trip # {0} already exists / imported to Journeys table ##\n", journey.TripId));
//    //                jCounter++;
//    //                continue;
//    //            }
//    //            //----------------



//    //            IBI.DataAccess.DBModels.Journey dbJourney = new DataAccess.DBModels.Journey();


//    //            int stopSequence = 1;
//    //            List<JourneyStop> jStops = new List<JourneyStop>();

//    //            foreach (StopHop stop in journey.JourneyStops)
//    //            {
//    //                JourneyStop jStop = new JourneyStop();
//    //                jStop.Timestamp = DateTime.Now;

//    //                decimal ibiStopNumber = IBI.Shared.AppUtility.IsNullDec(ScheduleHelper.TransformStopNo(stop.StopNumber, ScheduleHelper.StopPrefix.HAFAS));


//    //                //fix for DK Server
//    //                //if (!stop.Latitude.Contains('.'))
//    //                //{
//    //                //    stop.Latitude = stop.Latitude.Insert(2, ".");
//    //                //    stop.Longitude = stop.Longitude.Insert(2, ".");
//    //                //}


//    //                bool getLiveZone = bool.Parse(ConfigurationManager.AppSettings["GetLiveZonesForStop"].ToString());

//    //                if (getLiveZone && stop.Zone == "0" && String.IsNullOrEmpty(stop.StopName) == false)
//    //                {
//    //                    stop.Zone = IBI.Cache.JourneyCache.GetZoneName(stop.Latitude, stop.Longitude, true, dbContext);
//    //                }

//    //                if (stop.Zone == "0") {
//    //                    stop.Zone = "";
//    //                }

//    //                //Console.Write(stop.Latitude + ", " + stop.Longitude);

//    //                //var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude.Replace(',', '.'), stop.Latitude.Replace(',', '.'));
//    //                var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude, stop.Latitude);
//    //                jStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
//    //                jStop.StopId = ibiStopNumber;
//    //                jStop.StopName = stop.StopName;
//    //                jStop.StopSequence = stopSequence++;

//    //                DateTime plannedDepartureTime = String.IsNullOrEmpty(stop.DepartureTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + stop.DepartureTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);
//    //                DateTime plannedArrivalTime = String.IsNullOrEmpty(stop.ArrivalTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + stop.ArrivalTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);

//    //                if (plannedDepartureTime != DateTime.MinValue)
//    //                {
//    //                    jStop.PlannedDepartureTime = plannedDepartureTime.AddDays(stop.Day2addDeparture);
//    //                }

//    //                if (plannedArrivalTime != DateTime.MinValue)
//    //                {
//    //                    jStop.PlannedArrivalTime = plannedArrivalTime.AddDays(stop.Day2addArrival);
//    //                }

//    //                jStop.Zone = stop.Zone;
//    //                jStop.IsOnline = false;
//    //                jStop.Timestamp = DateTime.Now;

//    //                //Console.WriteLine(plannedArrivalTime.ToString("yyyy-MM-ddTHH:mm") + " || " +plannedDepartureTime.ToString("yyyy-MM-ddTHH:mm"));

//    //                ////SYNC STOPS *******************************************************************
//    //                bool syncStops = bool.Parse(ConfigurationManager.AppSettings["SyncStops"]);

//    //                if (syncStops)
//    //                {
//    //                    try
//    //                    {
//    //                        var st = allDBStops.Where(s => s.GID == ibiStopNumber).FirstOrDefault();
//    //                        if (st == null)
//    //                        {
//    //                            DataAccess.DBModels.Stop newStop = new DataAccess.DBModels.Stop()
//    //                            {
//    //                                StopName = jStop.StopName,
//    //                                GID = jStop.StopId,
//    //                                StopGPS = jStop.StopGPS,
//    //                                StopSource = "HAFAS",
//    //                                OriginalCreated = DateTime.Now,
//    //                                OriginalName = jStop.StopName,
//    //                                OriginalGPS = jStop.StopGPS,
//    //                                LastUpdated = DateTime.Now
//    //                            };

//    //                            dbContext.Stops.Add(newStop);
//    //                            allDBStops.Add(newStop);//added to global list for better next comparison.
//    //                            dbContext.SaveChanges();
//    //                        }
//    //                        else
//    //                        {
//    //                            if(st.StopName != jStop.StopName)
//    //                            {
//    //                            //update
//    //                                st.StopName = jStop.StopName;
//    //                                st.StopGPS = jStop.StopGPS;
//    //                                st.StopSource = "HAFAS";
//    //                                st.LastUpdated = DateTime.Now;

//    //                                dbContext.SaveChanges();
//    //                            }
//    //                        }


//    //                        jStops.Add(jStop);
//    //                        sbStops.AppendLine(string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5} GPS: {6} {7}",
//    //                                    jStop.StopId, jStop.StopName, stopSequence, jStop.Zone, jStop.PlannedDepartureTime.ToString(), jStop.PlannedArrivalTime.ToString(), jStop.StopGPS.Latitude.ToString(), jStop.StopGPS.Longitude.ToString()));

//    //                    }
//    //                    catch (Exception exStops)
//    //                    {
//    //                        //throw new Exception(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
//    //                        sbStops.AppendLine(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
//    //                    }
//    //                }
//    //                ////*********************************************************************************************

//    //            }// end stops loop

//    //            string destinationAfterRewrite = journey.Destination;

//    //            int scheduleId = 0;
//    //            if (journey.ScheduleNumber == 0)
//    //            {
//    //                //dbJourney.ScheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(CustomerId, journey.Line, journey.StartPoint, ref destinationAfterRewrite, journey.ViaName, dbJourney.JourneyStops, ref log, true);
//    //                scheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(custConfig, 0, customerId, journey.Line, journey.StartPoint, journey.Destination, journey.ViaName, jStops, ref sbJourney, ref destinationAfterRewrite, true);
//    //            }

//    //            ////existing future journey CHECK ***********************
//    //            bool updateExistingJourneys = bool.Parse(ConfigurationManager.AppSettings["UpdateExistingJourneys"]);
//    //            if (updateExistingJourneys)
//    //            {
//    //                decimal firstStopId = jStops.FirstOrDefault().StopId;
//    //                var existingJourney = dbContext.GetExistingFutureJourney(customerId, externalRef, journey.Line, destinationAfterRewrite, journey.PlannedDepartureTime, firstStopId).FirstOrDefault();

//    //                if (existingJourney != null)
//    //                {
//    //                    dbJourney = existingJourney;
//    //                    dbJourney.JourneyStops.Clear();
//    //                }
//    //            }
//    //            ////*******************************************************

//    //            dbJourney.JourneyStops = jStops;
//    //            dbJourney.ScheduleId = scheduleId;
//    //            dbJourney.BusNumber = journey.BusNumber;
//    //            dbJourney.CustomerId = customerId;
//    //            dbJourney.JourneyStateId = 0;
//    //            dbJourney.PlannedEndTime = journey.PlannedArrivalTime;
//    //            dbJourney.PlannedStartTime = journey.PlannedDepartureTime;
//    //            dbJourney.LastUpdated = DateTime.Now;
//    //            dbJourney.ExternalReference = externalRef;


//    //            sbJourney.AppendLine(String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, RewriteDestination: {8}, Total Stops: {9}",
//    //                           (dbJourney.JourneyId > 0 ? "UPDATED" : "INSERTED"), dbJourney.JourneyId, dbJourney.CustomerId, dbJourney.ExternalReference, dbJourney.ScheduleId, journey.Line, journey.StartPoint, journey.Destination, destinationAfterRewrite, jStops.Count()));
//    //            sbJourney.AppendLine(sbStops.ToString());

//    //            if (dbJourney.JourneyId == 0) 
//    //            { 
//    //                dbContext.Journeys.Add(dbJourney);
//    //            }

//    //            try
//    //            {
//    //                //Save new Journey to DB                    
//    //                dbContext.SaveChanges();
//    //                sbJourney.AppendLine(string.Format("Journey {0} saved successfully to DB", dbJourney.JourneyId));

//    //            }
//    //            catch (Exception exJourney)
//    //            {
//    //                sbJourney.AppendLine(string.Format("ERROR !!! Failed to save Journey [{0}]", IBI.Shared.Diagnostics.Logger.GetDetailedError(exJourney)));
//    //            }


//    //            Log custLog = new Log(GetCustomerLogFileName(Counter, customerId), sbJourney);
//    //            custLog.WriteLog(sbJourney.ToString());

//    //            custLog = null;

//    //            sbJourney.Clear();
//    //            sbStops.Clear();

//    //            System.Threading.Thread.Sleep(20);
//    //        }

//    //        ParentLog.WriteLog("All journeys successfully imported to the database");
//    //        //MessageBox.Show("All journeys successfully imported to the database");
//    //    }
//    //}
//    //catch (Exception ex)
//    //{
//    //    //throw new Exception("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
//    //    //Console.WriteLine("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
//    //    ParentLog.WriteLog(ex);

//    //}


//    return true;
//}



//foreach (var line in lines.Skip(1))
//{
//    ccc += 1;
//    //if (ccc++ > 2)
//    //    break;

//    string[] values = line.Split(delimiter);

//    //check if this journey already exists
//    var JRT = JourneysRT.FirstOrDefault(x => x.JourneyPatternId == values[5]);
//    if (JRT == null)
//    {
//        // Create new Journey
//        JRT = new JourneyRT();
//        //empty journey stops
//        JRT.JourneyStops.Clear();

//        JRT.OperatingDayDate = System.Convert.ToDateTime(values[0]);
//        JRT.LineNumber = System.Convert.ToInt32(values[1]);
//        JRT.LineDesignation = values[2];
//        JRT.DirectionCode = System.Convert.ToInt32(values[3]);
//        JRT.JourneyNumber = System.Convert.ToInt32(values[4]);
//        JRT.JourneyPatternId = values[5];
//        JRT.PlannedStartDateTime = System.Convert.ToDateTime(values[6]);
//        JRT.PlannedEndDateTime = System.Convert.ToDateTime(values[7]);
//        JRT.PrimaryDestinationName = values[20];
//        JRT.PrimaryDestinationShortName = values[21];
//        JRT.SecondaryDestinationName = values[22];
//        JRT.SecondaryDestinationShortName = values[23];
//        JRT.ContractorCode = values[24];
//        JRT.ContractorName = values[25];
//        JRT.DepartmentCode = values[26].Trim().Length >= 1 ? values[26].Trim().ToUpper() : "EMPTY";
//        JRT.DepartmentName = values[27];

//        //Additional hardcoded
//        JRT.ScheduleNumber = 0;
//        JRT.BusNumber = "0";
//        JRT.StartPoint = values[10]; //StopPointNumber & StopPointName starting first stop

//        // Add journey to journeys
//        JourneysRT.Add(JRT);
//    }

//    //Add stops to this journey
//    JourneyStopsRT JRTS = new JourneyStopsRT();
//    JRTS.SequenceNumber = System.Convert.ToInt32(values[8]);
//    JRTS.StopPointNumber = System.Convert.ToInt32(values[9]);
//    JRTS.StopPointName = values[10];
//    JRTS.TimingPoint = System.Convert.ToBoolean(values[11]);
//    JRTS.ZoneNumber = values[12];
//    JRTS.CoordinateSystemName = values[13];//same
//    JRTS.NorthingCoordinate = System.Convert.ToDecimal(values[14]);
//    JRTS.EastingCoordinate = System.Convert.ToDecimal(values[15]);
//    JRTS.Latitude = values[16];
//    JRTS.Longitude = values[17];
//    JRTS.PlannedArrivalDateTime = System.Convert.ToDateTime(values[18]);
//    JRTS.PlannedDepartureDateTime = System.Convert.ToDateTime(values[19]);

//    //Add this stop to Journey
//    JRT.JourneyStops.Add(JRTS);

//} //foreach

//Console.WriteLine("ParseCSV Parsing data done.");

//  Console.WriteLine("lines count {0}", ccc);
//test
//int outerlimit = 0;
//foreach (var j in JourneysRT)
//{
//    Console.WriteLine(j.JourneyPatternId);
//    int innerlimit = 0;
//    foreach (var s in j.JourneyStops)
//    {
//        Console.WriteLine("\t\t\t" + s.StopPointName);
//        if (innerlimit++ > 3)
//            break;
//    }

//    if (outerlimit++ > 3)
//        break;
//}