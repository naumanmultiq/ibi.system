﻿using IBI.Shared.ConfigSections;
using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Linq;
using ICSharpCode.SharpZipLib.Zip;
using IBI.Shared.Common;
using System.IO.Compression;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace IBI.MoviaRealtimeJourneys
{

    public class SubscriptionTimer
    {
        Log log { get; set; }
        MoviaRTFileDownloader FileDownloader { get; set; }
        
        public Timer VdvTimer { get; set; }
        public VdvSubscriptionConfiguration subConfiguration { get; set; }
        bool DataReadyStatus { get; set; }
        

        private static Int64 _subscriptionCounter;
        private static Int64 SubscriptionCounter
        {
            get
            {
                if (_subscriptionCounter != null && _subscriptionCounter >= 0)
                {
                    _subscriptionCounter += 1;
                    return _subscriptionCounter;
                }
                else
                {
                    _subscriptionCounter = 1;
                    return _subscriptionCounter;
                }
            }
            set
            {
                _subscriptionCounter = value;
            }
        }

        private static int Counter { get; set; }

        private string LogFileName
        {
            get { return string.Format("MoviaRT_{0}", Counter); }
        }
        //public CustomerTimer(){}
        private string sourceFTP { get; set; }
        private string user { get; set; }
        private string pwd { get; set; }
        private string localFilePath;

        public SubscriptionTimer(VdvSubscriptionConfiguration config)
        {
            Counter++;

            this.subConfiguration = config;

            this.VdvTimer = new Timer(TimeSpan.FromSeconds(5 * (Counter % 2) + 1).TotalMilliseconds);
            this.VdvTimer.Elapsed += VdvTimer_Elapsed;
            this.VdvTimer.Enabled = true;

            log = new Log(LogFileName);
            FileDownloader = new MoviaRTFileDownloader(log);
            
            sourceFTP = config.ServerAddress.ToString().Replace("ftps", "ftp");

            string[] strList = config.ServerLogin.Split(',');  
            if (strList.Count() == 2) {
                user = strList[0];
                pwd = strList[1];
            }

            localFilePath = Path.GetTempPath();
        }

        public Int64 SubscriptionId { get; set; }

        #region Events


        public void Dispose()
        {
            this.VdvTimer.Enabled = false;
            this.VdvTimer.Dispose();

        }

        private void VdvTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            int jTimerInterval = 5;
            try
            {

                jTimerInterval = this.subConfiguration.JourneyTimerInterval;

                log.WriteLog("MoviaRTJourneyImporterTimer starts");
                this.VdvTimer.Enabled = false;

                if (AppSettings.HaConJourneyImporterEnabled())
                {
                    VdvJourneyImporter();
                }
            }
            catch (Exception ex)
            {
                log.WriteLog(ex);
            }
            finally
            {
                this.VdvTimer.Interval = TimeSpan.FromMinutes(jTimerInterval).TotalMilliseconds;
                this.VdvTimer.Enabled = true;
            }
        }

        #endregion


        #region "Functions"


        /// </summary>
        public void VdvJourneyImporter()
        {
            log.WriteLog("Movia RealTime Journey Importer Timer Started");
            string prodcutKey = subConfiguration.ProductKey;
            try
            {

                //int customerId = ConfigurationManager.AppSettings["DefaultCustomer"];
                using (new IBI.Shared.CallCounter("Synchronizer.MoviaRTJourneyImporter"))
                {
                    log.WriteLog("Requesting data from Remote Server");
                    if (Uri.IsWellFormedUriString(sourceFTP, UriKind.Absolute))
                    {
                        ////Console.WriteLine("Starting download process with params {0}-{1}-{2}-{3}", sourceFTP, user, pwd, localFilePath);
                        localFilePath = Path.GetTempPath();

                        bool bDownloadCompleted = FileDownloader.DownloadSourceFile(sourceFTP, user, pwd, ref localFilePath);
                        if ((bDownloadCompleted) && (File.Exists(localFilePath)))
                        {
                            //Console.WriteLine("Download completed, Starting CSV parser.");
                            MoviaRealtimeDataProcessor hProcessor = new MoviaRealtimeDataProcessor(localFilePath, subConfiguration, LogFileName, Counter);
                            if (hProcessor.DoProcess())
                            {

                                // Delete CSV
                                if (!subConfiguration.SaveDataInFiles)
                                    File.Delete(localFilePath);
                            }
                        }

                        ////test section
                        //// string localFilePath = "C:\\Users\\Shahzad\\AppData\\Local\\Temp\\PlannedData_AR_20160511.gzip.csv";
                        //string localFilePath = "C:\\Users\\Shahzad\\AppData\\Local\\Temp\\PlannedData_AR_20160607.gzip.csv";
                        //MoviaRealtimeDataProcessor hProcessor = new MoviaRealtimeDataProcessor(localFilePath, subConfiguration, LogFileName, Counter);
                        //if (hProcessor.DoProcess())
                        //{

                        //    // Delete CSV
                        //    if (!subConfiguration.SaveDataInFiles)
                        //        File.Delete(localFilePath);
                        //}


                    } //strList.Count
                }
            }
            catch (Exception ex)
            {
                log.WriteLog(ex);
            }
            finally
            {
                log.WriteLog(string.Format("Next Journey Import will occur after {0} minutes", subConfiguration.JourneyTimerInterval));
            }
        }


        #endregion

        #region Private Helper
        

        #endregion

    }
}
