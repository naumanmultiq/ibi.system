﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.MoviaRealtimeJourneys
{
    public class JourneyStopsRT
    {
        public int SequenceNumber { get; set; }
        public int StopPointNumber { get; set; }
        public string StopPointName { get; set; }
        public bool TimingPoint { get; set; }
        public string ZoneNumber { get; set; }
        public string CoordinateSystemName { get; set; }//same
        public decimal NorthingCoordinate { get; set; }
        public decimal EastingCoordinate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime PlannedArrivalDateTime { get; set; }
        public DateTime PlannedDepartureDateTime { get; set; }
    } //JourneyStopsRT

    public class JourneyRT
    {
        public string key { get; set; }
        public DateTime OperatingDayDate { get; set; }
        public int LineNumber { get; set; }
        public string LineDesignation { get; set; }
        public int DirectionCode { get; set; }
        public int JourneyNumber { get; set; }
        public string JourneyPatternId { get; set; }
        public DateTime PlannedStartDateTime { get; set; }
        public DateTime PlannedEndDateTime { get; set; }
        public List<JourneyStopsRT> JourneyStops { get; set; }
        public string PrimaryDestinationName { get; set; }
        public string PrimaryDestinationShortName { get; set; }
        public string SecondaryDestinationName { get; set; }
        public string SecondaryDestinationShortName { get; set; }
        public string ContractorCode { get; set; }
        public string ContractorName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }

        public JourneyRT()
        {
            JourneyStops = new List<JourneyStopsRT>();
        }

        //Additional
        public int ScheduleNumber { get; set; }
        public string BusNumber { get; set; }
        public string StartPoint { get; set; }
       
    } //JourneyRT


    //public class Journey
    //{
    //    public int JourneyNumber { get; set; }
    //    public int ScheduleNumber { get; set; }
    //    public string BusNumber { get; set; }
    //    public int CustomerId { get; set; }
    //    public DateTime JourneyDate { get; set; }
    //    public DateTime? StartTime { get; set; }
    //    public DateTime? EndTime { get; set; }
    //    public string Line { get; set; }
    //    public string StartPoint { get; set; }
    //    public string Destination { get; set; }
    //    public string ViaName { get; set; }
    //    public DateTime? PlannedArrivalTime { get; set; }
    //    public DateTime? PlannedDepartureTime { get; set; }
    //    public string JourneyData { get; set; }
    //    public int? JourneyAhead { get; set; }
    //    public int? JourneyBehind { get; set; }
    //    public bool? IsLastStop { get; set; }
    //    public string TripId { get; set; }


    //    public ArrayList JourneyStops { get; set; }


    //    public Journey()
    //    {
    //        //Stops = new List<JourneyStop>();
    //        JourneyStops = new ArrayList();
    //    }
    //}

    //class StopHop
    //{
    //    public string StopNumber { get; set; }
    //    public string StopName { get; set; }
    //    public string ArrivalTime { get; set; }
    //    public string DepartureTime { get; set; }
    //    public string Latitude { get; set; }
    //    public string Longitude { get; set; }
    //    public string Zone { get; set; }
    //    public string IsCheckpoint { get; set; }
    //    public int Day2addArrival { get; set; }
    //    public int Day2addDeparture { get; set; }


    //    public StopHop(string stopNumber, string stopName, string arrivalTime, string departureTime, string lat, string lon, string zone, string isCheckpoint, int day2AddArrival, int day2AddDeparture)
    //    {
    //        this.StopNumber = stopNumber;
    //        this.StopName = stopName;
    //        this.ArrivalTime = arrivalTime;
    //        this.DepartureTime = departureTime;
    //        this.Latitude = lat;
    //        this.Longitude = lon;
    //        this.Zone = zone;
    //        this.IsCheckpoint = String.IsNullOrEmpty(IsCheckpoint) ? "false" : isCheckpoint;
    //        this.Day2addArrival = day2AddArrival;
    //        this.Day2addDeparture = day2AddDeparture;
    //    }
    //}
}
