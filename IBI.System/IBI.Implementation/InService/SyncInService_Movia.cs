﻿using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IBI.Implementation.InService
{
    class SyncInService_Movia : Shared.Interfaces.ISyncInService
    {
        #region Properties

        private int _customerId;
        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        #endregion


        #region Implementation

        public string SyncInServiceStatus(string busNumber)
        {

            string log = "No Error";
            try
            {

                FileInfo cacheFile = GetCacheFile(busNumber);
                string scheduleNumber = string.Empty;


                // Console.WriteLine(string.Format("bus: {0}, cacheFile: {1}, writeTime: {2}", busNumber, cacheFile.Exists, cacheFile.LastWriteTime));

                if (cacheFile.Exists)
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {

                        var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == CustomerId).FirstOrDefault();

                        if (bus != null)
                        {
                            if (bus.LastUpdatedInService == null)
                            { bus.LastUpdatedInService = DateTime.MinValue; }

                            if (bus.LastTimeInService == null)
                            { bus.LastTimeInService = DateTime.MinValue; }

                            //if it is different from previous only then update it again. otherwise no
                            if (cacheFile.LastWriteTime > bus.LastUpdatedInService)
                            {
                                bus.LastUpdatedInService = cacheFile.LastWriteTime;

                                //string contents = File.ReadAllText(cacheFile.FullName, Encoding.Default);
                                string contents = string.Empty;
                                using (StreamReader sr = new StreamReader(cacheFile.FullName, Encoding.Default))
                                {
                                    // Read and display lines from the file until the end of 
                                    // the file is reached.
                                    contents = sr.ReadToEnd();
                                    sr.DiscardBufferedData();
                                    sr.Dispose();
                                }


                                XDocument xDoc = XDocument.Load(GenerateStreamFromString(contents));
                                var scheduleNode = xDoc.Descendants("ScheduleNumber").FirstOrDefault();


                                if (scheduleNode != null)
                                {
                                    scheduleNumber = scheduleNode.Value;
                                }

                                if (!string.IsNullOrEmpty(scheduleNumber))
                                {
                                    var lastStopExpectedArrival = xDoc.Descendants("ArrivalTime").LastOrDefault();

                                    if (lastStopExpectedArrival != null) 
                                    {   
                                        log = lastStopExpectedArrival.Value;
                                        string lastStopAT = lastStopExpectedArrival.Value;
                                        if (lastStopExpectedArrival.Value.Contains('.'))
                                        {
                                            lastStopAT = lastStopExpectedArrival.Value.Substring(0, lastStopExpectedArrival.Value.IndexOf('.'));
                                        }
                                            
                                        DateTime lastTimeInService = DateTime.ParseExact(lastStopAT , "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                                        

                                        if (lastTimeInService > DateTime.Now.AddYears(-1))
                                        {
                                            bus.LastTimeInService = lastTimeInService;// cacheFile.LastWriteTime;
                                        }
                                        else
                                        {
                                            bus.LastTimeInService = cacheFile.LastWriteTime;
                                        }
                                    }
                                }
                                else
                                {
                                    if (bus.LastTimeInService > DateTime.Now)
                                    {
                                        bus.LastTimeInService = cacheFile.LastWriteTime;
                                    }
                                }

                                if (bus.LastTimeInService == DateTime.MinValue)
                                {
                                    bus.LastTimeInService = null;
                                }

                                dbContext.SaveChanges();
                                xDoc = null;
                            }

                        }

                        return string.Format("LastTimeInService: {0} | LastUpdatedInService: {1} | Log: {2}",
                            bus.LastTimeInService == null ? "" : bus.LastTimeInService.Value.ToString("yyyy-MM-ddTHH:mm"),
                            bus.LastUpdatedInService == null ? "" : bus.LastUpdatedInService.Value.ToString("yyyy-MM-ddTHH:mm"), log);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.InServiceStatus, string.Format("Error syncing InService for Bus [{0}]. {1}", busNumber, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
            }

            return string.Format(@"LastTimeInService: N\A | LastUpdatedInService: N\A | Log: {0}", log);
        }


        #endregion

        #region Private Helper

        private FileInfo GetCacheFile(string busNumber)
        {
            string resourcePath = IBI.Shared.AppSettings.GetResourceDirectory();

            string outputXml = string.Empty;

            FileInfo scheduleFile = new FileInfo(Path.Combine(resourcePath, Path.Combine(CustomerId.ToString(), "Cache"), "Schedule_" + busNumber + ".xml"));

            return scheduleFile;
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        #endregion
    }
}
