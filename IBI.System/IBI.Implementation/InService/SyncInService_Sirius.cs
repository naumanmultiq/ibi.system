﻿using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IBI.Implementation.InService
{
    class SyncInService_Sirius : Shared.Interfaces.ISyncInService
    {
        #region Properties

        private int _customerId;
        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        #endregion


        #region Implementation

        public string SyncInServiceStatus(string busNumber)
        {
             
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == CustomerId).FirstOrDefault();

                    if (bus != null)
                    {
                        if (bus.LastUpdatedInService == null)
                        { bus.LastUpdatedInService = DateTime.MinValue;}

                        if (bus.LastTimeInService == null)
                        { bus.LastTimeInService = DateTime.MinValue; }

                        //we must get sirius status from other DB ... (for this specific bus)
                        using (MermaidTempDBModel tempDbContext = new MermaidTempDBModel())
                        {
                            var siriusStatus = tempDbContext.SiriusStatus.Where(s => s.CustomerID == bus.CustomerId && s.BusNumber == bus.BusNumber).FirstOrDefault(); //dbContext.SiriusStatus.Where(c => c.BusNumber == busNumber && c.CustomerID == CustomerId).FirstOrDefault();
                            
                            if (siriusStatus != null)
                            {

                                if (siriusStatus.Timestamp > bus.LastUpdatedInService) //if it's newer than the info we already have, only then update in DB
                                {

                                    bus.LastUpdatedInService = siriusStatus.Timestamp;


                                    if (siriusStatus.IgnitionOn)
                                    {
                                        bus.LastTimeInService = siriusStatus.Timestamp;
                                    }

                                    if (bus.LastTimeInService == DateTime.MinValue)
                                    {
                                        bus.LastTimeInService = null;
                                    }

                                    dbContext.SaveChanges();
                                }

                            }

                        }


                        return string.Format("LastTimeInService: {0} | LastUpdatedInService: {1}",
                            bus.LastTimeInService == null ? "" : bus.LastTimeInService.Value.ToString("yyyy-MM-ddTHH:mm"),
                            bus.LastUpdatedInService == null ? "" : bus.LastUpdatedInService.Value.ToString("yyyy-MM-ddTHH:mm"));

                    }
                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.InServiceStatus, string.Format("Error syncing InService for Bus [{0}]. {1}", busNumber, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
            }
            
            return string.Format(@"LastTimeInService: N\A | LastUpdatedInService: N\A");
        }


        #endregion

        #region Private Helper

        private FileInfo GetCacheFile(string busNumber)
        {
            string resourcePath = IBI.Shared.AppSettings.GetResourceDirectory();

            string outputXml = string.Empty;

            FileInfo scheduleFile = new FileInfo(Path.Combine(resourcePath, Path.Combine(CustomerId.ToString(), "Cache"), "Schedule_" + busNumber + ".xml"));

            return scheduleFile;
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        #endregion
    }
}