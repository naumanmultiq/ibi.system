﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.Implementation.Movia.Services.Location {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CurrentBusLocationResult", Namespace="http://schemas.movia.dk/2010/02/25/BusLocationService")]
    [System.SerializableAttribute()]
    public partial class CurrentBusLocationResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private IBI.Implementation.Movia.Services.Location.BusLocation CurrentBusLocationField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public IBI.Implementation.Movia.Services.Location.BusLocation CurrentBusLocation {
            get {
                return this.CurrentBusLocationField;
            }
            set {
                if ((object.ReferenceEquals(this.CurrentBusLocationField, value) != true)) {
                    this.CurrentBusLocationField = value;
                    this.RaisePropertyChanged("CurrentBusLocation");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BusLocation", Namespace="http://schemas.movia.dk/2010/02/25/BusLocationService/Entity")]
    [System.SerializableAttribute()]
    public partial class BusLocation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private System.Nullable<System.DateTime> ArrivalAtNextStopField;
        
        private string CurrentStopNameField;
        
        private string DestinationField;
        
        private System.Nullable<System.DateTime> LastUpdatedField;
        
        private string LineField;
        
        private string NextStopNameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.Nullable<System.DateTime> ArrivalAtNextStop {
            get {
                return this.ArrivalAtNextStopField;
            }
            set {
                if ((this.ArrivalAtNextStopField.Equals(value) != true)) {
                    this.ArrivalAtNextStopField = value;
                    this.RaisePropertyChanged("ArrivalAtNextStop");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string CurrentStopName {
            get {
                return this.CurrentStopNameField;
            }
            set {
                if ((object.ReferenceEquals(this.CurrentStopNameField, value) != true)) {
                    this.CurrentStopNameField = value;
                    this.RaisePropertyChanged("CurrentStopName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string Destination {
            get {
                return this.DestinationField;
            }
            set {
                if ((object.ReferenceEquals(this.DestinationField, value) != true)) {
                    this.DestinationField = value;
                    this.RaisePropertyChanged("Destination");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.Nullable<System.DateTime> LastUpdated {
            get {
                return this.LastUpdatedField;
            }
            set {
                if ((this.LastUpdatedField.Equals(value) != true)) {
                    this.LastUpdatedField = value;
                    this.RaisePropertyChanged("LastUpdated");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string Line {
            get {
                return this.LineField;
            }
            set {
                if ((object.ReferenceEquals(this.LineField, value) != true)) {
                    this.LineField = value;
                    this.RaisePropertyChanged("Line");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string NextStopName {
            get {
                return this.NextStopNameField;
            }
            set {
                if ((object.ReferenceEquals(this.NextStopNameField, value) != true)) {
                    this.NextStopNameField = value;
                    this.RaisePropertyChanged("NextStopName");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BusLocationException", Namespace="http://schemas.movia.dk/2010/02/25/BusLocationService/Exception")]
    [System.SerializableAttribute()]
    public partial class BusLocationException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DetailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Detail {
            get {
                return this.DetailField;
            }
            set {
                if ((object.ReferenceEquals(this.DetailField, value) != true)) {
                    this.DetailField = value;
                    this.RaisePropertyChanged("Detail");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://schemas.movia.dk/2010/02/25/BusLocationService", ConfigurationName="Movia.Services.Location.IBusLocationService")]
    public interface IBusLocationService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://schemas.movia.dk/2010/02/25/BusLocationService/IBusLocationService/GetCurr" +
            "entBusLocation", ReplyAction="http://schemas.movia.dk/2010/02/25/BusLocationService/IBusLocationService/GetCurr" +
            "entBusLocationResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(IBI.Implementation.Movia.Services.Location.BusLocationException), Action="http://schemas.movia.dk/2010/02/25/BusLocationService/IBusLocationService/GetCurr" +
            "entBusLocationBusLocationExceptionFault", Name="BusLocationException", Namespace="http://schemas.movia.dk/2010/02/25/BusLocationService/Exception")]
        IBI.Implementation.Movia.Services.Location.CurrentBusLocationResult GetCurrentBusLocation(int busNumber);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IBusLocationServiceChannel : IBI.Implementation.Movia.Services.Location.IBusLocationService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class BusLocationServiceClient : System.ServiceModel.ClientBase<IBI.Implementation.Movia.Services.Location.IBusLocationService>, IBI.Implementation.Movia.Services.Location.IBusLocationService {
        
        public BusLocationServiceClient() {
        }
        
        public BusLocationServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public BusLocationServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BusLocationServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BusLocationServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public IBI.Implementation.Movia.Services.Location.CurrentBusLocationResult GetCurrentBusLocation(int busNumber) {
            return base.Channel.GetCurrentBusLocation(busNumber);
        }
    }
}
