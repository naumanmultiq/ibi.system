﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBI.Shared.Interfaces;
using System.Reflection;
using System.IO;
using System.Xml;
using mermaid.BaseObjects.Diagnostics;
using System.Web;


namespace IBI.Implementation.Schedular
{
    public class SyncScheduleFactory : ISyncScheduleFactory
    {
        public ISyncSchedular GetSyncSchedular(int customerId)
        {
            try
            {
                Assembly currentAssembly = Assembly.GetAssembly(typeof(SyncScheduleFactory));
                if (currentAssembly == null)
                {
                    return null;
                }

                string filePath = Path.Combine(Path.GetDirectoryName(currentAssembly.CodeBase), "Schedular_Configuration.xml");
                filePath = filePath.Replace("file:\\", string.Empty);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                XmlDocument configDocument = new XmlDocument();
                configDocument.Load(filePath);
                XmlNode customerNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]");
                if (customerNode == null)
                {
                    return null;
                }

                XmlNode classNameNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]/Class");
                if (classNameNode == null)
                {
                    return null;
                }

                string schedularClassName = classNameNode.InnerText;
                if (string.IsNullOrEmpty(schedularClassName))
                {
                    return null;
                }

                Type[] types = currentAssembly.GetTypes();
                Type runTimeClass = currentAssembly.GetType(schedularClassName);
                if (runTimeClass == null)
                {
                    return null;
                }

                ISyncSchedular schedular = (ISyncSchedular)Activator.CreateInstance(runTimeClass);
                schedular.CustomerId = customerId;
                return schedular;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("SyncSchedular", ex.Message + ex.StackTrace);
            }
            return null;
        }


        public ISyncInService GetSyncInService(int customerId)
        {
            try
            {
                Assembly currentAssembly = Assembly.GetAssembly(typeof(SyncScheduleFactory));
                if (currentAssembly == null)
                {
                    return null;
                }

                string filePath = Path.Combine(Path.GetDirectoryName(currentAssembly.CodeBase), "Schedular_Configuration.xml");
                filePath = filePath.Replace("file:\\", string.Empty);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                XmlDocument configDocument = new XmlDocument();
                configDocument.Load(filePath);
                XmlNode customerNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]");
                if (customerNode == null)
                {
                    return null;
                }

                XmlNode classNameNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]/InServiceClass");
                if (classNameNode == null)
                {
                    return null;
                }

                string InServiceClassName = classNameNode.InnerText;
                if (string.IsNullOrEmpty(InServiceClassName))
                {
                    return null;
                }

                Type[] types = currentAssembly.GetTypes();
                Type runTimeClass = currentAssembly.GetType(InServiceClassName);
                if (runTimeClass == null)
                {
                    return null;
                }

                ISyncInService inService = (ISyncInService)Activator.CreateInstance(runTimeClass);
                inService.CustomerId = customerId;
                //departureBoard.Url = classNameNode.SelectSingleNode("Url").InnerText;
                //departureBoard.Username = classNameNode.SelectSingleNode("Username").InnerText;
                //departureBoard.Password = classNameNode.SelectSingleNode("Password").InnerText;
                //departureBoard.StopInfoUrl = classNameNode.SelectSingleNode("StopInfoUrl").InnerText;
                return inService;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("SyncDepartureBoard", ex.Message + ex.StackTrace);
            }
            return null;
        }


        public IJourneyPredictionHandler GetJourneyHandler(int customerId)
        {
            try
            {
                Assembly currentAssembly = Assembly.GetAssembly(typeof(SyncScheduleFactory));
                if (currentAssembly == null)
                {
                    return null;
                }

                string filePath = Path.Combine(Path.GetDirectoryName(currentAssembly.CodeBase), "Schedular_Configuration.xml");
                filePath = filePath.Replace("file:\\", string.Empty);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                XmlDocument configDocument = new XmlDocument();
                configDocument.Load(filePath);
                XmlNode customerNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]");
                if (customerNode == null)
                {
                    return null;
                }

                XmlNode classNameNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]/JourneyPredictionHandlerClass");
                if (classNameNode == null)
                {
                    return null;
                }

                string JourneyHandlerClassName = classNameNode.InnerText;
                if (string.IsNullOrEmpty(JourneyHandlerClassName))
                {
                    return null;
                }

                Type[] types = currentAssembly.GetTypes();
                Type runTimeClass = currentAssembly.GetType(JourneyHandlerClassName);
                if (runTimeClass == null)
                {
                    return null;
                }

                IJourneyPredictionHandler handler = (IJourneyPredictionHandler)Activator.CreateInstance(runTimeClass);
                handler.CustomerId = customerId;
                //departureBoard.Url = classNameNode.SelectSingleNode("Url").InnerText;
                //departureBoard.Username = classNameNode.SelectSingleNode("Username").InnerText;
                //departureBoard.Password = classNameNode.SelectSingleNode("Password").InnerText;
                //departureBoard.StopInfoUrl = classNameNode.SelectSingleNode("StopInfoUrl").InnerText;
                return handler;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("SyncDepartureBoard", ex.Message + ex.StackTrace);
            }
            return null;
        }

        public ISyncDepartureBoard GetSyncDepartureBoard(int customerId)
        {
            try
            {
                Assembly currentAssembly = Assembly.GetAssembly(typeof(SyncScheduleFactory));
                if (currentAssembly == null)
                {
                    return null;
                }

                string filePath = Path.Combine(Path.GetDirectoryName(currentAssembly.CodeBase), "Schedular_Configuration.xml");
                filePath = filePath.Replace("file:\\", string.Empty);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                XmlDocument configDocument = new XmlDocument();
                configDocument.Load(filePath);
                XmlNode customerNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]");
                if (customerNode == null)
                {
                    return null;
                }

                XmlNode classNameNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]/DepartureClass");
                if (classNameNode == null)
                {
                    return null;
                }

                string departureBoardClassName = classNameNode.SelectSingleNode("Class").InnerText;
                if (string.IsNullOrEmpty(departureBoardClassName))
                {
                    return null;
                }

                Type[] types = currentAssembly.GetTypes();
                Type runTimeClass = currentAssembly.GetType(departureBoardClassName);
                if (runTimeClass == null)
                {
                    return null;
                }

                ISyncDepartureBoard departureBoard = (ISyncDepartureBoard)Activator.CreateInstance(runTimeClass);
                departureBoard.CustomerId = customerId;
                departureBoard.Url = classNameNode.SelectSingleNode("Url").InnerText;
                departureBoard.Username = classNameNode.SelectSingleNode("Username").InnerText;
                departureBoard.Password = classNameNode.SelectSingleNode("Password").InnerText;
                departureBoard.StopInfoUrl = classNameNode.SelectSingleNode("StopInfoUrl").InnerText;
                return departureBoard;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("SyncDepartureBoard", ex.Message + ex.StackTrace);
            }
            return null;
        }
    }
}
