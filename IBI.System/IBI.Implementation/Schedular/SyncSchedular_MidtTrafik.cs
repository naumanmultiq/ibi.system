﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IBI.Shared.Interfaces;
using IBI.Shared.Models;
using System.Net;
using System.IO;
using IBI.DataAccess;
using IBI.Shared.Diagnostics;
using IBI.Shared;
using System.Configuration;
using IBI.DataAccess.DBModels;

namespace IBI.Implementation.Schedular
{
    public class SyncSchedular_MidtTrafik : ISyncSchedular
    {
        #region "Attributes"

        private int _customerId;

        #endregion
        #region "Properties"

        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        #endregion

        #region "Implementation"

        public ZoneLookup GetZone(string latitude, string longitude)
        {
            String requestString = "http://geo.oiorest.dk/takstzoner/" + latitude + "," + longitude;

            ZoneLookup result = new ZoneLookup();
            result.GPSCoordinate = latitude + "," + longitude;

            try
            {
                if (!String.IsNullOrEmpty(latitude) && !String.IsNullOrEmpty(longitude))
                {
                    WebClient oiorestClient = new WebClient();
                    oiorestClient.Encoding = System.Text.Encoding.UTF8;

                    String oiorestReply = oiorestClient.DownloadString(requestString);

                    XmlDocument oiorestDocument = new XmlDocument();
                    oiorestDocument.LoadXml(oiorestReply);

                    String zone = oiorestDocument.SelectSingleNode("takstzone/nr").InnerText;

                    result.OperatorName = zone;
                }
                else
                {
                    result.Comments = "GPS Coordinates supplied are not valid";
                }
            }
            catch (Exception ex)
            {
                result.Comments = "Error occurred in the communication with geo.oiorest.dk";

                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, new ApplicationException("Zone request failed." + Environment.NewLine +
                                                                                                                       "Request: " + requestString + Environment.NewLine, ex));
            }

            return result;
        }


        public ActiveBusReply GetActiveBus(string resourcepath, int searchSeed, bool preferViaNames)
        {
            throw new Exception("No interface to get active bus");
        }

        public string DownloadCurrentSchedule(string busNumber)
        {
            throw new Exception("No interface to download the current schedule");
        }

        public void SyncAllStops()
        {
            throw new Exception("No interface to sync stops");
        }

        public string GetCurrentSchedule(string resourcePath, string busNumber)
        {
            throw new Exception("No interface to get current schedule");
        }

       
        #endregion

        #region "Helper Functions"

        private string TransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = "9025200" + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }

        public string GetStopName(long stopNo, string originalStopName)
        {
            using (new CallCounter("ScheduleServiceController.GetStopName"))
            {
                string stopName = originalStopName;

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var selectedStops = (from x in dbContext.Stops
                                         where x.GID == stopNo
                                         select x);

                    bool stopExists = false;
                    foreach (var stop in selectedStops)
                    {
                        stopName = stop.StopName;
                        stopExists = true;
                    }

                    if (!stopExists)
                    {
                        if (!string.IsNullOrEmpty(stopName))
                        {
                            Stop newStop = new Stop();
                            newStop.GID = stopNo;
                            newStop.StopName = AppUtility.SafeSqlLiteral(stopName);
                            dbContext.Stops.Add(newStop);
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWSData, new Exception("StopName for StopNumber:[" + stopNo.ToString() + "] is null from Movia WS"));
                        }
                    }
                    return stopName;
                }
                return null;
            }
        }

        #endregion

    }
}
