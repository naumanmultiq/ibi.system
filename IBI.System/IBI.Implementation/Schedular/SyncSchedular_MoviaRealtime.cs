﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IBI.Shared.Interfaces;
using IBI.Shared.Models.Realtime;
using System.Net;
using System.IO;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;
using IBI.Shared;
using System.Configuration;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Data.Entity.Spatial;
using System.Xml.Linq;
using IBI.Shared.Models.Movia.HogiaTypes;
using Newtonsoft.Json;
using IBI.Shared.Common;
using IBI.Shared.Models.Journey;


namespace IBI.Implementation.Schedular
{
    public class SyncSchedular_MoviaRealtime : ISyncSchedular
    {
        #region Attributes

        private int _customerId;
        private static Object _lock = new Object();

        #endregion

        #region Properties

        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        #endregion

        #region Implementation

        public IBI.Shared.Models.ZoneLookup GetZone(string latitude, string longitude)
        {

            String requestString = "http://geo.oiorest.dk/takstzoner/" + latitude + "," + longitude;

            IBI.Shared.Models.ZoneLookup result = new IBI.Shared.Models.ZoneLookup();
            result.GPSCoordinate = latitude + "," + longitude;

            try
            {
                if (!String.IsNullOrEmpty(latitude) && !String.IsNullOrEmpty(longitude))
                {
                    WebClient oiorestClient = new WebClient();
                    oiorestClient.Encoding = System.Text.Encoding.UTF8;

                    String oiorestReply = oiorestClient.DownloadString(requestString);

                    XmlDocument oiorestDocument = new XmlDocument();
                    oiorestDocument.LoadXml(oiorestReply);

                    String zone = oiorestDocument.SelectSingleNode("takstzone/nr").InnerText;

                    result.OperatorName = zone;
                }
                else
                {
                    result.Comments = "GPS Coordinates supplied are not valid";
                }
            }
            catch (Exception ex)
            {
                result.Comments = "Error occurred in the communication with geo.oiorest.dk";

                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, new ApplicationException("Zone request failed." + Environment.NewLine +
                                                                                                                       "Request: " + requestString + Environment.NewLine, ex));
            }

            return result;
        }

        public IBI.Shared.Models.ActiveBusReply GetActiveBus(string resourcepath, int searchSeed, bool preferViaNames)
        {
            IBI.Shared.Models.ActiveBusReply reply = new IBI.Shared.Models.ActiveBusReply();
            IBIDataModel dbContext = new IBIDataModel();
            Movia.Services.JourneyProgress.ServiceClient serviceClient = new Movia.Services.JourneyProgress.ServiceClient();
            serviceClient.ClientCredentials.UserName.UserName = "mermaid";
            serviceClient.ClientCredentials.UserName.Password = "220711Mer";

            for (int busNo = searchSeed; busNo < dbContext.Clients.Count(); busNo++)
            {
                Movia.Services.JourneyProgress.JourneyRow[] serviceResult = null;
                using (new IBI.Shared.CallCounter("Movia WS calls"))
                {
                    try
                    {
                        using (new IBI.Shared.CallCounter("SyncSchedular_MoviaRealtime.GetActiveBus.JourneyProgress"))
                        {
                            serviceResult = serviceClient.GetJourneyProgress(busNo);
                        }

                        if (serviceResult.Length > 0)
                        {
                            Movia.Services.JourneyProgress.JourneyRow currentRow = serviceResult[serviceResult.Length - 1];

                            if (!string.IsNullOrEmpty(currentRow.TADT))
                            {
                                if (DateTime.Parse(currentRow.TADT) > DateTime.Now.AddMinutes(10) && DateTime.Parse(currentRow.TADT).Year > 2010)
                                {
                                    FileInfo scheduleFile = new FileInfo(Path.Combine(resourcepath, "Cache\\Schedule_" + busNo + ".xml"));

                                    if (scheduleFile.Exists)
                                    {
                                        reply.BusNo = busNo.ToString();

                                        Boolean acceptCurrent = true;

                                        if (preferViaNames && string.IsNullOrEmpty(currentRow.Via))
                                            acceptCurrent = false;

                                        if (acceptCurrent)
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWS, ex);

                        throw new ApplicationException("Error communicating with Movia WS", ex);
                    }
                }
            }
            return reply;
        }


        internal static MoviaJourney DownloadMoviaRealtimeJourney(string busNumber, int customerId)
        {
            DateTime stime = DateTime.Now;
            int hMSecs = 0;

            try
            {

                RESTManager rest = RESTManager.Instance;

                rest.BasePath = GetMoviaHogiaJourneyServerPath(busNumber, customerId);

                var journey = rest.Get<IBI.Shared.Models.Movia.HogiaTypes.MoviaJourney>(string.Format("/{0}/journey", busNumber));

                hMSecs = DateTime.Now.Subtract(stime).Milliseconds;

                foreach (var stop in journey.stops)
                {
                    stop.arrivalTime = stop.expectedArrivalDateTime;
                    stop.departureTime = stop.expectedDepartureDateTime;
                }

                return journey;

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("{0} - No Lines/Journeys available for these parameters busNumber=[{1}] in method {2}", "DownloadMoviaRealtimeJourney", busNumber, ex.Message));
            }

            finally
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaHogia, string.Format("Bus {0} - GET JOURNEY - Rest Call response: {1} MS, Hogia Call response: {2} MS", busNumber, DateTime.Now.Subtract(stime).Milliseconds, hMSecs));
            }

            return null;
        }


        private static Uri GetMoviaHogiaJourneyServerPath(string busNumber, int customerId)
        {
            Uri serverAddress = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.GetServerAddress(busNumber, customerId, "HogiaRealtimeJourneyService");
            return serverAddress;

        }

        //private static IBI.Shared.Models.Movia.HogiaTypes.MoviaJourney DownloadMoviaRealtimeJourney(string busNumber, int customerId)
        //{


        //    //REST call to Sync SignItem Images ------------------------------------------------------------
        //    string REST_PATH = System.Configuration.ConfigurationSettings.AppSettings["REST_PATH"].ToString();

        //    Uri baseAddress = new Uri(REST_PATH + "/Journey/JourneyService.svc/");

        //    string uriTemplate = String.Format("DownloadMoviaJourney?busNumber={0}&customerId={1}", busNumber, customerId);

        //    System.Net.WebClient webClient = new System.Net.WebClient();

        //    string url = baseAddress.AbsoluteUri + uriTemplate;

        //    string result = string.Empty; // webClient.DownloadString(url);

        //    try
        //    {
        //        byte[] data = webClient.DownloadData(url);
        //        result = System.Text.Encoding.UTF8.GetString(data);
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.RestManager, string.Format("Communication errors with endpoint '{0}'. Error: {1}", url, Logger.GetDetailedError(e)));
        //        throw e;
        //    }
        //    //T retObj = JsonConvert.DeserializeObject<T>(result);
        //    IBI.Shared.Models.Movia.HogiaTypes.MoviaJourney journey = JsonConvert.DeserializeObject<IBI.Shared.Models.Movia.HogiaTypes.MoviaJourney>(result);


        //    return journey;

        //}

        public string DownloadCurrentSchedule(string busNumber)
        {
            long scheduleJourneyNo = -1;
            string outputXml = string.Empty;

            IBI.Shared.Models.Realtime.Schedule tmpSchedule = new IBI.Shared.Models.Realtime.Schedule();
            List<StopInfo> currentSchedule = new List<StopInfo>();
            try
            {
                tmpSchedule.Cached = false;

                bool isError = false;

                MoviaJourney journey = null;

                try
                {
                    journey = DownloadMoviaRealtimeJourney(busNumber, CustomerId);
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.MoviaHogia, ex);
                }

                using (IBIDataModel dbContext = new IBIDataModel())
                {



                    //[KHI]: start - patch to set ScheduleJourneyNumber = -1 in case of no data from Movia. +add warning to SystemLog.
                    if (journey == null)
                    {
                        scheduleJourneyNo = 0;
                    }
                    //[KHI]: end

                    if (journey != null)
                    {

                        bool nameChanged = false;
                        bool? recreateScheduleStops = false;

                        DateTime journeyPlannedStartTime = DateTime.MinValue;
                        DateTime journeyPlannedEndTime = DateTime.MinValue;


                        tmpSchedule.BusNumber = busNumber.ToString();
                        tmpSchedule.Line = journey.isOnLine.designation;
                        tmpSchedule.JourneyNumber = journey.journeyId;
                        tmpSchedule.FromName = journey.origin;

                        string alternateDestination = journey.lineDirection.Substring(journey.lineDirection.LastIndexOf('-') + 2);

                        tmpSchedule.DestinationName = (journey.destination != null && journey.destination.Length > 0) ? (journey.destination[0] != null ? journey.destination[0] : alternateDestination) : alternateDestination;
                        tmpSchedule.ViaName = (journey.destination != null && journey.destination.Length > 1) ? journey.destination[1] : null;

                        scheduleJourneyNo = long.Parse(journey.journeyId);


                        for (int i = 0; i < journey.stops.Length; i++)
                        {

                            MoviaJourneyStop currentRow = journey.stops[i];

                            StopInfo stopData = new StopInfo();


                            if (i == 0)
                            {
                                if (currentRow.plannedDepartureDateTime != null) //planned departure time
                                    journeyPlannedStartTime = stopData.PlannedDepartureTime = currentRow.plannedDepartureDateTime; //DateTime.Parse(currentRow.plannedDepartureDateTime);

                            }

                            String[] gpsCoordinates = new String[] { currentRow.latitude.ToString().Replace(',', '.'), currentRow.longitude.ToString().Replace(',', '.') };

                            //TODO: Fix midnight time issues
                            //time fix for 24+ hours
                            /*
                             timestamp = new System.TimeSpan(25, 0, 0);
                             System.DateTime parsedDateTime = new DateTime(0, 0, 0);
                             parsedDateTime = parsedDateTime.Add(timestamp);
                             Console.WriteLine(parsedDateTime.ToString("yyyy-MM-dd HH:mm:ss"));
                            */

                            stopData.StopNumber = TransformStopNo(currentRow.stopPointNumber.ToString());

                            stopData.StopName = SyncStop(long.Parse(stopData.StopNumber), currentRow.stopPointName, gpsCoordinates, "DEC", ref nameChanged); //StopName is not 100% correct in reply, so we have to look it up in database

                            if (nameChanged && !recreateScheduleStops.Value)
                            {
                                recreateScheduleStops = true; //not being used so far to delete + add all schedulestops
                            }

                            //if (currentRow.arrivalTime != null)
                            //    stopData.ArrivalTime = currentRow.plannedDepartureDateTime;
                            //if (currentRow.departureTime != null)
                            //    stopData.DepartureTime = currentRow.plannedDepartureDateTime;

                            if (currentRow.plannedDepartureDateTime != null) //planned departure time
                                stopData.PlannedDepartureTime = currentRow.plannedDepartureDateTime; //DateTime.Parse(currentRow.plannedDepartureDateTime);

                            if (currentRow.plannedArrivalDateTime != null) //planned departure time
                                stopData.PlannedArrivalTime = currentRow.plannedArrivalDateTime; //DateTime.Parse(currentRow.plannedDepartureDateTime);

                            if (currentRow.expectedArrivalDateTime != null)
                                stopData.ArrivalTime = currentRow.expectedArrivalDateTime;//DateTime.Parse(currentRow.expectedArrivalDateTime);
                            if (currentRow.expectedDepartureDateTime != null)
                                stopData.DepartureTime = currentRow.expectedDepartureDateTime; //DateTime.Parse(currentRow.expectedDepartureDateTime);



                            //[KHI]: midnight journeys time issue FIX******************** 
                            if (stopData.ArrivalTime < journeyPlannedStartTime)
                                stopData.ArrivalTime = stopData.ArrivalTime.AddDays(1);
                            if (stopData.DepartureTime < journeyPlannedStartTime)
                                stopData.DepartureTime = stopData.DepartureTime.AddDays(1);
                            if (stopData.PlannedDepartureTime < journeyPlannedStartTime)
                                stopData.PlannedDepartureTime = stopData.PlannedDepartureTime.AddDays(1);
                            //    stopData.DepartureTime = stopData.DepartureTime.AddDays(1);
                            if (stopData.PlannedArrivalTime < journeyPlannedStartTime)
                                stopData.PlannedArrivalTime = stopData.PlannedArrivalTime.AddDays(1);
                            //if (stopData.ExpectedArrivalTime < journeyPlannedStartTime)
                            //    stopData.ExpectedArrivalTime = stopData.ExpectedArrivalTime.AddDays(1);
                            //if (stopData.ExpectedDepartureTime < journeyPlannedStartTime)
                            //    stopData.ExpectedDepartureTime = stopData.ExpectedDepartureTime.AddDays(1);

                            //***********************************************************

                            stopData.GPSCoordinateNS = gpsCoordinates[0];
                            stopData.GPSCoordinateEW = gpsCoordinates[1];
                            stopData.StopSequence = (i + 1).ToString();
                            stopData.Zone = currentRow.zoneNumber.ToString();

                            try
                            {
                                /*
                                if (stopData.Zone.Length > 2)
                                {
                                    stopData.Zone = int.Parse(stopData.Zone.Substring(stopData.Zone.Length - 2, 2)).ToString();
                                }
                                */

                                //stopData.Zone = stopData.Zone.TrimStart(new Char[] { '0' });
                            }
                            catch
                            {
                                stopData.Zone = "";
                            }

                            stopData.IsCheckpoint = false; //(currentRow.Checkpoint == 1); //undetermined so far

                            currentSchedule.Add(stopData);
                            //System.Threading.Thread.Sleep(AppSettings.SyncScheduleStopInterval());
                        }



                        tmpSchedule.JourneyStops = currentSchedule.ToArray();

                        if (!String.IsNullOrEmpty(tmpSchedule.Line) && !String.IsNullOrEmpty(tmpSchedule.DestinationName))
                        {
                            //logs += string.Format("SyncDestinations Start {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);
                            // Save all destinations
                            dbContext.SyncDestinations(CustomerId, busNumber.ToString(), tmpSchedule.Line, tmpSchedule.FromName, tmpSchedule.DestinationName, AppUtility.SafeSqlLiteral(tmpSchedule.ViaName));

                            //logs += string.Format("SyncDestinations End {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);

                            string scheduleXML = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(tmpSchedule));
                            scheduleXML = scheduleXML.Substring(scheduleXML.IndexOf("<Schedule"));
                            scheduleXML = scheduleXML.Replace("ScheduleRealtime", "Schedule").Replace("StopInfo", "StopInfo");
                            //logs += string.Format("SyncSchedules Start {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);
                            IBI.DataAccess.DBModels.Schedule sch = dbContext.SyncSchedules(CustomerId, tmpSchedule.Line, tmpSchedule.FromName, tmpSchedule.DestinationName, tmpSchedule.ViaName, scheduleXML, recreateScheduleStops).FirstOrDefault();
                            //logs += string.Format("SyncSchedules End {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);
                            tmpSchedule.ScheduleNumber = sch.ScheduleId.ToString();


                            bool JourneySyncEnabled = AppSettings.MoviaJourneySyn();
                            if (JourneySyncEnabled)
                            {
                                // create journey of a schedule against a bus on MoviaSyn
                                //logs += string.Format("SyncJourney Start {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine); 
                                string externalRef = "MOVIA-" + DateTime.Now.ToString("yyyyMMdd") + "-" + tmpSchedule.Line + "-" + tmpSchedule.JourneyNumber;
                                tmpSchedule.ExternalReference = externalRef;
                                if (!string.IsNullOrEmpty(busNumber))
                                {
                                    tmpSchedule.JourneyNumber = dbContext.SyncJourney(sch.ScheduleId, CustomerId, busNumber.ToString(), sch.Line, sch.Destination, tmpSchedule != null ? tmpSchedule.JourneyStops.First().PlannedDepartureTime : DateTime.MinValue, tmpSchedule != null ? tmpSchedule.JourneyStops.Last().PlannedDepartureTime : DateTime.MinValue, externalRef, scheduleXML, AppSettings.MoviaJourneySynLogEnabled()).FirstOrDefault().ToString();
                                }
                                //logs += string.Format("SyncJourney End {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);

                                //if it's a valid JOURNEY then save all the Stop Estimation for this journey

                                // dbContext.JourneyEstimation_MOVIA(int.Parse(tmpSchedule.JourneyNumber), scheduleXML);
                            }

                            SignController.CheckSignForNewSchedule(dbContext, sch, "IBI SERVER");

                            var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == CustomerId).FirstOrDefault();

                            if (bus != null)
                            {
                                bus.ScheduleJourneyNumber = scheduleJourneyNo;
                            }
                        }

                        dbContext.SaveChanges();
                    }
                }


                //return tmpSchedule;
                //return outputXml;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWSData, new Exception("Bus Number :[" + busNumber.ToString() + "]" + ex.Message.ToString() + ex.StackTrace));
            }


            if (scheduleJourneyNo != -1)
            {
                outputXml = UpdateCacheFile(AppSettings.GetResourceDirectory(), busNumber, tmpSchedule);

                //Import Journey Stop Estimations
                bool checkStopPrediction = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.BusExistsForService(busNumber, CustomerId, "JourneyStopEstimation");
                if (checkStopPrediction)
                {
                    ImportPredictions(tmpSchedule);
                }
            }

            return outputXml;
        }



        public void SyncAllStops()
        {
            using (Movia.Services.Stop.BusStopServiceClient serviceClient = new Movia.Services.Stop.BusStopServiceClient())
            {
                Movia.Services.Stop.AllStopPointsResult serviceResult = null;

                using (new IBI.Shared.CallCounter("Movia WS calls"))
                {
                    try
                    {
                        serviceClient.ClientCredentials.UserName.UserName = "mermaid";
                        serviceClient.ClientCredentials.UserName.Password = "220711Mer";

                        serviceResult = serviceClient.GetAllStopPoints();
                        using (new IBI.Shared.CallCounter("Database connections"))
                        {
                            using (IBIDataModel dbContext = new IBIDataModel())
                            {
                                foreach (var stopEntry in serviceResult.StopPointIdsAndNames)
                                {
                                    String stopNo = TransformStopNo(stopEntry.Key.ToString());
                                    String stopName = stopEntry.Value;

                                    Stop newStop = new Stop();
                                    newStop.GID = decimal.Parse(stopNo);
                                    newStop.StopName = stopName;
                                    dbContext.Stops.Add(newStop);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWS, ex);

                        throw new ApplicationException("Error communicating with Movia WS", ex);
                    }
                }
            }
        }

        public string GetCurrentSchedule(string resourcePath, string busNumber)
        {
            String outputXml = string.Empty;

            IBI.Shared.Models.Realtime.Schedule tmpSchedule = new IBI.Shared.Models.Realtime.Schedule();

            using (new IBI.Shared.CallCounter("ScheduleServiceController.GetCurrentSchedule"))
            {
                FileInfo scheduleFile = new FileInfo(Path.Combine(resourcePath, Path.Combine(CustomerId.ToString(), "Cache"), "Schedule_" + busNumber + ".xml"));

                if (!scheduleFile.Exists || scheduleFile.LastWriteTime.AddMinutes(15) < DateTime.Now)
                {
                    tmpSchedule.JourneyStops = new List<StopInfo>().ToArray();

                    outputXml = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(tmpSchedule));
                }
                else
                {
                    using (new IBI.Shared.CallCounter("Cache deserializations"))
                    {
                        if (scheduleFile.Length == 0)
                        {
                            File.Delete(scheduleFile.FullName);
                            tmpSchedule.JourneyStops = new List<StopInfo>().ToArray();
                            outputXml = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(tmpSchedule));
                        }
                        else
                        {
                            XmlDocument scheduleDocument = new XmlDocument();
                            scheduleDocument.Load(scheduleFile.FullName);

                            //tmpSchedule = (IBI.Shared.Models.Schedule)XmlDeserializer.deserialize(typeof(IBI.Shared.Models.Schedule), (XmlElement)scheduleDocument.SelectSingleNode("Schedule"));


                            XmlElement scheduleNode = (XmlElement)scheduleDocument.SelectSingleNode("Schedule");
                            var cachedNode = scheduleNode.SelectSingleNode("Cached");
                            if (cachedNode != null)
                                cachedNode.InnerText = "true";

                            outputXml = scheduleDocument.OuterXml;
                        }

                    }
                }
            }

            //return tmpSchedule;
            return outputXml;


        }

        private string UpdateCacheFile(string resourcePath, string busNumber, IBI.Shared.Models.Realtime.Schedule schedule)
        {
            string outputXml = string.Empty;
            using (WebClient webClient = new WebClient())
            {
                FileInfo scheduleFile = new FileInfo(Path.Combine(resourcePath, Path.Combine(CustomerId.ToString(), "Cache"), "Schedule_" + busNumber + ".xml"));

                if (!scheduleFile.Directory.Exists)
                    scheduleFile.Directory.Create();

                try
                {
                    String xmlSchedule = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(schedule));


                    XmlDocument xDoc = EmbedAddonsToScheduleAndStops(xmlSchedule); //logic to embed db stopAddons to schedule

                    outputXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xDoc.OuterXml;

                    File.WriteAllText(scheduleFile.FullName, outputXml);

                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI, "SyncSchedular_Movia (ScheduleLoaded - Exception) " + Logger.GetDetailedError(ex));

                }
            }

            return outputXml;

        }


        private static XmlDocument EmbedAddonsToScheduleAndStops(string scheduleXml)
        {
            StringReader rd = new StringReader(scheduleXml);
            XDocument xDoc = XDocument.Load(rd);
            var scheduleNode = xDoc.Descendants("ScheduleNumber").FirstOrDefault();

            if (scheduleNode != null)
            {
                int scheduleId = int.Parse(xDoc.Descendants("ScheduleNumber").FirstOrDefault().Value);

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    var sAddons = dbContext.ScheduleAddons.Where(sa => sa.ScheduleId == scheduleId);
                    var ssAddons = dbContext.ScheduleStopsAddons.Where(ssa => ssa.ScheduleId == scheduleId);

                    //embed schedule addons
                    var schedule = xDoc.Descendants("Schedule").FirstOrDefault();

                    if (schedule != null)
                    {
                        foreach (var sAddon in sAddons)
                        {

                            string elementName = sAddon.Key.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
                            XElement addonNode = new XElement(elementName, new XCData(sAddon.Value.ToString()));
                            XAttribute attr = new XAttribute("isAddon", "true");
                            addonNode.Add(attr);

                            schedule.Add(addonNode);
                        }

                    }

                    //embed schedule stops addons
                    foreach (var stopInfo in xDoc.Descendants("StopInfo"))
                    {
                        int stopSequence = int.Parse(stopInfo.Descendants("StopSequence").FirstOrDefault().Value);

                        var addons = ssAddons.Where(a => a.StopSequence == stopSequence);
                        foreach (var addon in addons)
                        {
                            string elementName = addon.Key.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
                            XElement addonNode = new XElement(elementName, new XCData(addon.Value.ToString()));
                            XAttribute attr = new XAttribute("isAddon", "true");
                            addonNode.Add(attr);

                            stopInfo.Add(addonNode);
                        }

                    }

                }
            }

            XmlDocument retObject = new XmlDocument();
            retObject.LoadXml(xDoc.ToString());


            return retObject;
        }
        #endregion

        #region  Helper Functions

        //private static void SyncDestinationContentImages(int signItemId, int? signGroupId = null)
        //{

        //    if (!String.IsNullOrEmpty(signItemId.ToString()))
        //    {
        //        //REST call to Sync SignItem Images ------------------------------------------------------------
        //        string REST_PATH = System.Configuration.ConfigurationSettings.AppSettings["REST_PATH"].ToString();

        //        Uri baseAddress = new Uri(REST_PATH + "/Schedule/Xml/ScheduleService.svc/");

        //        string uriTemplate = String.Format("SyncDestinationContentImages?signItems={0}&signGroups={1}&userName={2}", signItemId.ToString(), signGroupId == null ? string.Empty : signGroupId.ToString(), "IBI_SERVER");

        //        System.Net.WebClient webClient = new System.Net.WebClient();

        //        string url = baseAddress.AbsoluteUri + uriTemplate;

        //        string result = webClient.DownloadString(url);

        //        //string retObj = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(result);
        //        //------------------------------------------------------------------------------------------------
        //    }

        //}

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private static string ConvertStringtoMD5(string strword)
        {
            return mermaid.BaseObjects.IO.FileHash.FromStream(GenerateStreamFromString(strword)).ToString();
            //MD5 md5 = MD5.Create("MD5");
            //byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(strword);
            //byte[] hash = md5.ComputeHash(inputBytes);
            //StringBuilder sb = new StringBuilder();
            //for (int i = 0; i < hash.Length; i++)
            //{
            //    sb.Append(hash[i].ToString("x2"));
            //}
            //return sb.ToString();
        }

        private string TransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = "9025200" + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }

        private static string UnTransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.unTransformStopNo"))
            {

                //eg. 9025200000028662
                if (stopNo.Length == 16) // it's a transformed stop number
                {
                    stopNo = Convert.ToInt32(stopNo.Replace("9025200", "")).ToString();
                }

                return stopNo;
            }
        }

        private string SyncStop(long stopNo, string originalStopName, string[] gpsCoordinates, string gpsProjection, ref bool nameChanged)
        {
            try
            {

                using (new CallCounter("ScheduleServiceController.GetStopName"))
                {
                    nameChanged = false;
                    string stopName = originalStopName; //.Contains('(') ? originalStopName.Substring(0, originalStopName.IndexOf('(')-1).Trim() : originalStopName;

                    if (string.IsNullOrEmpty(stopName))
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.MoviaWSData, new Exception("StopName for StopNumber:[" + stopNo.ToString() + "] is null from Movia WS"));
                        //return "";
                    }


                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        Stop stop = dbContext.Stops.Where(s => s.GID == stopNo).FirstOrDefault();


                        string lat;
                        string lon;

                        string sqlPoint = string.Format("POINT({0} {1})", gpsCoordinates[1], gpsCoordinates[0]);

                        if (stop != null)
                        {

                            if (!(String.Equals(stop.StopName, stopName)))
                            {
                                nameChanged = true;
                                stop.StopName = AppUtility.SafeSqlLiteral(stopName);
                                //lat = gpsCoordinates[0]; //[KHI: Comment this line if Geography is introduced]
                                //lon = gpsCoordinates[1]; //[KHI: Comment this line if Geography is introduced]
                                //stop.GPSProjection = gpsProjection; //[KHI: Comment this line if Geography is introduced]
                                stop.LastUpdated = DateTime.Now;
                                //lat = stop.GPSCoordinateNS.ToString().Trim();
                                //lon = stop.GPSCoordinateEW.ToString().Trim();
                                //string sqlPoint = string.Format("POINT({0} {1})", gpsCoordinates[1], gpsCoordinates[0]);
                                stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);

                                stop.LastUpdated = DateTime.Now;
                                stop.StopSource = "MOVIA";

                                stopName = stop.StopName;

                                dbContext.SaveChanges();
                            }

                            //dont update stop in any case

                        }

                        else
                        {
                            nameChanged = true;

                            Stop newStop = new Stop();
                            newStop.GID = stopNo;
                            newStop.StopName = AppUtility.SafeSqlLiteral(stopName);
                            lat = gpsCoordinates[0]; //[KHI: Comment this line if Geography is introduced]
                            lon = gpsCoordinates[1]; //[KHI: Comment this line if Geography is introduced]
                            //newStop.GPSProjection = gpsProjection; //[KHI: Comment this line if Geography is introduced]


                            newStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                            newStop.OriginalGPS = newStop.StopGPS;

                            //stop.OriginalGPSCoordinateNS = newStop.OriginalGPSCoordinateNS; //[KHI: Comment this line if Geography is introduced]
                            //stop.OriginalGPSCoordinateEW = newStop.OriginalGPSCoordinateEW; //[KHI: Comment this line if Geography is introduced]

                            newStop.OriginalName = AppUtility.SafeSqlLiteral(originalStopName);
                            newStop.OriginalCreated = newStop.LastUpdated = DateTime.Now;
                            newStop.StopSource = "MOVIA";
                            newStop.LastUpdated = DateTime.Now;

                            stopName = newStop.StopName;

                            dbContext.Stops.Add(newStop);

                            dbContext.SaveChanges();

                            //stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                            //stop.OriginalGPS = stop.StopGPS;

                            //dbContext.SaveChanges();
                        }


                        return stopName;
                    }

                }
            }

            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWS, string.Format("StopId:[{0}] - originalStopName: {1}, gpsCoordinates: {2} {3}, gpsProjection: {4} \n {5}", stopNo, originalStopName, gpsCoordinates[0], gpsCoordinates[1], gpsProjection, Logger.GetDetailedError(ex)));
            }

            return "";

        }

        public string GetStopName(long stopNo, string originalStopName)
        {
            using (new CallCounter("ScheduleServiceController.GetStopName"))
            {
                string stopName = originalStopName;

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var selectedStops = (from x in dbContext.Stops
                                         where x.GID == stopNo
                                         select x);

                    bool stopExists = false;
                    foreach (var stop in selectedStops)
                    {
                        stopName = stop.StopName;
                        stopExists = true;
                    }

                    if (!stopExists)
                    {
                        if (!string.IsNullOrEmpty(stopName))
                        {
                            Stop newStop = new Stop();
                            newStop.GID = stopNo;
                            newStop.StopName = AppUtility.SafeSqlLiteral(stopName);
                            dbContext.Stops.Add(newStop);
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.MoviaWSData, new Exception("StopName for StopNumber:[" + stopNo.ToString() + "] is null from Movia WS"));
                        }

                    }
                    return stopName;
                }
                return null;
            }
        }

        public void ImportPredictions(Shared.Models.Realtime.Schedule schedule)
        {
            List<JourneyPrediction> predictions = ParseJourneyEstimate(schedule);

            int journeyId = int.Parse(schedule.JourneyNumber);

            if (journeyId <= 0)
                return;

            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    //enabling this block keeps only updated stop estimation in JourneyStopEstimations
                    //********************************************************************************* 
                    var jEstimations = dbContext.JourneyStopEstimations.Where(j => j.JourneyId == journeyId);

                    var looper = from e in jEstimations
                                 group e by e.EstimatedStopSequence into g
                                 select g.OrderByDescending(e => e.EstimationTime).FirstOrDefault() into r
                                 select r;

                    int counter = 0;
                    foreach (var l in looper.OrderBy(j => j.EstimatedStopSequence))
                    {
                        var p = predictions.Where(pr => pr.EstimatedStopSequence == l.EstimatedStopSequence).FirstOrDefault();

                        if (p != null && (p.EstimatedDepartureTime == l.EstimatedDepartureTime && p.EstimatedArrivalTime == l.EstimatedArrivalTime))
                        {
                            predictions.Remove(p);
                        }

                        counter++;
                    }
                    //*********************************************************************************


                    foreach (JourneyPrediction jp in predictions)
                    {
                        DataAccess.DBModels.JourneyStopEstimation jEstimate = new JourneyStopEstimation
                        {
                            EstimatedArrivalTime = jp.EstimatedArrivalTime,
                            EstimatedAtStopSequence = jp.EstimatedAtStopSequence,
                            EstimatedDepartureTime = jp.EstimatedDepartureTime,
                            EstimatedStopSequence = jp.EstimatedStopSequence,
                            EstimationSource = jp.EstimationSource,
                            EstimationTime = jp.EstimationTime,
                            JourneyId = jp.JourneyId
                        };

                        dbContext.JourneyStopEstimations.Add(jEstimate);
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, Logger.GetDetailedError(ex));
            }
        }

        private List<IBI.Shared.Models.Journey.JourneyPrediction> ParseJourneyEstimate(Shared.Models.Realtime.Schedule schedule)
        {
            List<IBI.Shared.Models.Journey.JourneyPrediction> jList = new List<IBI.Shared.Models.Journey.JourneyPrediction>();

            if (schedule == null || schedule.JourneyStops.Length == 0)
                return jList;


            foreach (var stop in schedule.JourneyStops)
            {

                DateTime estimationTime = DateTime.Now;

                IBI.Shared.Models.Journey.JourneyPrediction j = new Shared.Models.Journey.JourneyPrediction();

                j.EstimationTime = estimationTime;
                j.EstimationSource = "MOVIA REALTIME";
                j.EstimatedAtStopSequence = null;
                j.JourneyId = int.Parse(schedule.JourneyNumber);


                var stopId = stop.StopNumber;


                j.EstimatedDepartureTime = stop.DepartureTime;
                j.EstimatedArrivalTime = stop.ArrivalTime;
                j.EstimatedStopSequence = int.Parse(stop.StopSequence);

                jList.Add(j);

            }//end foreach 

            return jList;
        }


        #endregion

    }
}
