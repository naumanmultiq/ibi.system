﻿namespace MoviaTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnfetchschedule = new System.Windows.Forms.Button();
            this.chkRetry = new System.Windows.Forms.CheckBox();
            this.busnumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.retrytimes = new System.Windows.Forms.TextBox();
            this.responselogs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gridschedule = new System.Windows.Forms.DataGridView();
            this.StopNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StopName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArrivalTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartureTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedDepartureTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Latitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Longitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StopSequence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridschedule)).BeginInit();
            this.SuspendLayout();
            // 
            // btnfetchschedule
            // 
            this.btnfetchschedule.Location = new System.Drawing.Point(199, 21);
            this.btnfetchschedule.Name = "btnfetchschedule";
            this.btnfetchschedule.Size = new System.Drawing.Size(104, 23);
            this.btnfetchschedule.TabIndex = 0;
            this.btnfetchschedule.Text = "Fetch Schedule";
            this.btnfetchschedule.UseVisualStyleBackColor = true;
            this.btnfetchschedule.Click += new System.EventHandler(this.btnfetchschedule_Click);
            // 
            // chkRetry
            // 
            this.chkRetry.AutoSize = true;
            this.chkRetry.Location = new System.Drawing.Point(320, 25);
            this.chkRetry.Name = "chkRetry";
            this.chkRetry.Size = new System.Drawing.Size(89, 17);
            this.chkRetry.TabIndex = 1;
            this.chkRetry.Text = "Retry Interval";
            this.chkRetry.UseVisualStyleBackColor = true;
            // 
            // busnumber
            // 
            this.busnumber.Location = new System.Drawing.Point(83, 23);
            this.busnumber.Name = "busnumber";
            this.busnumber.Size = new System.Drawing.Size(100, 20);
            this.busnumber.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bus Number";
            // 
            // retrytimes
            // 
            this.retrytimes.Location = new System.Drawing.Point(418, 23);
            this.retrytimes.Name = "retrytimes";
            this.retrytimes.Size = new System.Drawing.Size(45, 20);
            this.retrytimes.TabIndex = 4;
            // 
            // responselogs
            // 
            this.responselogs.Location = new System.Drawing.Point(15, 73);
            this.responselogs.Multiline = true;
            this.responselogs.Name = "responselogs";
            this.responselogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.responselogs.Size = new System.Drawing.Size(813, 156);
            this.responselogs.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Response/Logs";
            // 
            // gridschedule
            // 
            this.gridschedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridschedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StopNumber,
            this.StopName,
            this.ArrivalTime,
            this.DepartureTime,
            this.PlannedDepartureTime,
            this.Latitude,
            this.Longitude,
            this.StopSequence,
            this.Zone});
            this.gridschedule.Location = new System.Drawing.Point(15, 247);
            this.gridschedule.Name = "gridschedule";
            this.gridschedule.Size = new System.Drawing.Size(943, 348);
            this.gridschedule.TabIndex = 7;
            // 
            // StopNumber
            // 
            this.StopNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StopNumber.HeaderText = "StopNumber";
            this.StopNumber.Name = "StopNumber";
            this.StopNumber.ReadOnly = true;
            // 
            // StopName
            // 
            this.StopName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StopName.HeaderText = "StopName";
            this.StopName.Name = "StopName";
            this.StopName.ReadOnly = true;
            // 
            // ArrivalTime
            // 
            this.ArrivalTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ArrivalTime.HeaderText = "ArrivalTime";
            this.ArrivalTime.Name = "ArrivalTime";
            this.ArrivalTime.ReadOnly = true;
            // 
            // DepartureTime
            // 
            this.DepartureTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DepartureTime.HeaderText = "DepartureTime";
            this.DepartureTime.Name = "DepartureTime";
            this.DepartureTime.ReadOnly = true;
            // 
            // PlannedDepartureTime
            // 
            this.PlannedDepartureTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PlannedDepartureTime.HeaderText = "PlannedDepartureTime";
            this.PlannedDepartureTime.Name = "PlannedDepartureTime";
            this.PlannedDepartureTime.ReadOnly = true;
            // 
            // Latitude
            // 
            this.Latitude.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Latitude.HeaderText = "Latitude";
            this.Latitude.Name = "Latitude";
            this.Latitude.ReadOnly = true;
            // 
            // Longitude
            // 
            this.Longitude.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Longitude.HeaderText = "Longitude";
            this.Longitude.Name = "Longitude";
            this.Longitude.ReadOnly = true;
            // 
            // StopSequence
            // 
            this.StopSequence.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StopSequence.HeaderText = "StopSequence";
            this.StopSequence.Name = "StopSequence";
            this.StopSequence.ReadOnly = true;
            // 
            // Zone
            // 
            this.Zone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Zone.HeaderText = "Zone";
            this.Zone.Name = "Zone";
            this.Zone.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 607);
            this.Controls.Add(this.gridschedule);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.responselogs);
            this.Controls.Add(this.retrytimes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.busnumber);
            this.Controls.Add(this.chkRetry);
            this.Controls.Add(this.btnfetchschedule);
            this.Name = "Form1";
            this.Text = "Movie Schedule Tester";
            ((System.ComponentModel.ISupportInitialize)(this.gridschedule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnfetchschedule;
        private System.Windows.Forms.CheckBox chkRetry;
        private System.Windows.Forms.TextBox busnumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox retrytimes;
        private System.Windows.Forms.TextBox responselogs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridschedule;
        private System.Windows.Forms.DataGridViewTextBoxColumn StopNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn StopName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArrivalTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartureTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedDepartureTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Latitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Longitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn StopSequence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Zone;
    }
}

