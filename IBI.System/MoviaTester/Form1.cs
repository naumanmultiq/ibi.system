﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MoviaTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnfetchschedule_Click(object sender, EventArgs e)
        {
            FetchAndUpdateSchedule();
            if (chkRetry.Checked)
            {
                var timer = new Timer();
                timer.Tick += new EventHandler(timer_Tick);
                timer.Interval = int.Parse(retrytimes.Text)*1000; 
                timer.Start();
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            FetchAndUpdateSchedule();
        }

        private void FetchAndUpdateSchedule()
        {
            string logs = string.Empty;
            responselogs.AppendText("\r\n");
            responselogs.AppendText(DateTime.Now.ToString() + " - Trying for bus number: " + busnumber.Text);
            string schedule = IBI.Implementation.Schedular.MoviaImplementation.GetBusSchedule(int.Parse(busnumber.Text), ref logs);
            responselogs.AppendText(DateTime.Now.ToString() + " - Response received");
            if (string.IsNullOrEmpty(schedule))
            {
                responselogs.Text = logs;
            }
            else
            {
                gridschedule.Rows.Clear();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(schedule);
                responselogs.AppendText("\r\n");
                XmlNode rootNode = doc.SelectSingleNode("Schedule");
                if (rootNode == null || rootNode["Line"] == null)
                {
                    responselogs.AppendText(logs);
                    responselogs.AppendText("The XML may not be valid: " + schedule);
                    return;
                }
                responselogs.AppendText("Line: " + rootNode["Line"].InnerText + "\r\n");
                responselogs.AppendText("Destination: " + rootNode["DestinationName"].InnerText + "\r\n");
                responselogs.AppendText("ViaName: " + (rootNode["ViaName"] == null ? "" : rootNode["ViaName"].InnerText) + "\r\n");
                responselogs.AppendText("FromName: " + rootNode["FromName"].InnerText + "\r\n");
                responselogs.AppendText("JourneyNumber: " + rootNode["JourneyNumber"].InnerText + "\r\n");
                XmlNodeList nodes = doc.SelectNodes("Schedule/JourneyStops/StopInfo");
                responselogs.AppendText("Stop Count: " + nodes.Count + "\r\n\r\n");
                if (nodes.Count > 0)
                {
                    int rowIndex = 0;
                    foreach (XmlNode node in nodes)
                    {
                        DataGridViewRow row = (DataGridViewRow)gridschedule.Rows[rowIndex].Clone();
                        row.Cells[0].Value = node.Attributes["StopNumber"] == null ? "" : node.Attributes["StopNumber"].Value;
                        row.Cells[1].Value = node["StopName"] == null ? "" : node["StopName"].InnerText;
                        row.Cells[2].Value = node["ArrivalTime"] == null ? "" : node["ArrivalTime"].InnerText;
                        row.Cells[3].Value = node["DepartureTime"] == null ? "" : node["DepartureTime"].InnerText;
                        row.Cells[5].Value = node["GPSCoordinateNS"] == null ? "" : node["GPSCoordinateNS"].InnerText;
                        row.Cells[6].Value = node["GPSCoordinateEW"] == null ? "" : node["GPSCoordinateEW"].InnerText;
                        row.Cells[7].Value = node["StopSequence"] == null ? "" : node["StopSequence"].InnerText;
                        row.Cells[8].Value = node["Zone"] == null ? "" : node["Zone"].InnerText;
                        row.Cells[4].Value = node["PlannedDepartureTime"] == null ? "" : node["PlannedDepartureTime"].InnerText;
                        gridschedule.Rows.Add(row);
                        rowIndex++;
                    }
                }
            }
        }
    }
}
