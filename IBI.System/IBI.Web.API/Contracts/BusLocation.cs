﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Web;

namespace IBI.Web.API.Contracts
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/ibi/api/1.0")]
    public class BusLocation
    {
        #region Properties

        [DataMember(Order = 1)]
        public string Location
        {
            get;
            set;
        }

        [DataMember(Order = 2)]
        public DateTime LastUpdate
        {
            get;
            set;
        }

        [DataMember(Order = 3)]
        public bool IsOnline
        {
            get;
            set;
        }

        #endregion
    }
}