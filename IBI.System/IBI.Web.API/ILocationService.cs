﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Web.API.Contracts;

namespace IBI.Web.API
{
    [ServiceContract]
    public interface ILocationService
    {
        [OperationContract]
        [WebGet(UriTemplate = "GetBusLocation?customerid={customerID}&busnumber={busNumber}", ResponseFormat = WebMessageFormat.Xml)]
        BusLocation GetBusLocation(int customerID, string busNumber);
    }
}
