﻿using IBI.DataAccess.DBModels;
using IBI.Implementation.Schedular;
using IBI.Shared.Diagnostics;
using IBI.Shared.Interfaces;
using IBI.Shared.Models.Journey;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace IBI.PredictionQueueProcessor
{
    public class PredictionQueueController : IDisposable
      {
        #region Members

        private System.Timers.Timer TmrProcessQueue
        {
            get;
            set;
        }

        #endregion

        public PredictionQueueController()
        {

            AppUtility.Log2File("PredictionQueueManager", "new instance started");

            this.StartProcessTimer();

            //AppUtility.Log2File("PredictionQueueManager", "TmrProcessQueue timer Initializes, timer is " + this.TmrProcessQueue.Enabled); 
        }


        private void StartProcessTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.TmrProcessQueue == null)
            {
                this.TmrProcessQueue = new System.Timers.Timer();
                this.TmrProcessQueue.Elapsed += new System.Timers.ElapsedEventHandler(TmrProcessQueue_Elapsed);
            }

            this.TmrProcessQueue.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(3); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            nextTick = nextTick.AddSeconds(-1 * nextTick.Second);

            this.TmrProcessQueue.Interval = Math.Max(30000, nextTick.Subtract(DateTime.Now).TotalMilliseconds);
            this.TmrProcessQueue.Enabled = true;

            //AppUtility.Log2File("PredictionQueueManager", "TmrProcessQueue Timer initializes");
            //AppUtility.Log2File("PredictionQueueManager", "Next Queue processing at " + nextTick.ToString());

        }


        private void TmrProcessQueue_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.TmrProcessQueue.Enabled = false;
           // AppUtility.Log2File("PredictionQueueManager", "ProcessLog Timer Initializes");


            try
            {
                this.ProcessPredictionQueue();
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("PredictionQueueManager", "Queue Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                this.StartProcessTimer();
                //AppUtility.Log2File("PredictionQueueManager", "Queue Timer initializes again");
            }
        }

        public void ProcessPredictionQueue()
        {
            if (!AppSettings.PredictionQueueEnabled())
            {
                return;
            }
                

            //AppUtility.Log2File("PredictionQueueManager", "Queue --> Loop on Sign Queue");

            IBI.Shared.MSMQ.MSMQManager msgQueue = new IBI.Shared.MSMQ.MSMQManager(ConfigurationManager.AppSettings["PredictionQueueName"].ToString());

            while (true)
            {

                System.Messaging.Message msg = msgQueue.Receive();

                if (msg == null)
                {
                    break;
                }

                msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

                string msgBody = msg.Body.ToString();

                if (!string.IsNullOrEmpty(msgBody))
                {
                    
                    if (!AppSettings.PredictionQueueCall2ThirdParty())
                    {
                        AppUtility.Log2File("PredictionQueueManager", string.Format("Eliminating Queue Item without processing. Journey: {0},", msg.Body.ToString()), true);
                        continue;
                    }


                    string[] info = msg.Body.ToString().Split('|');

                    //int customerId = info.Length > 0 ? int.Parse(info[0]) : 2140;


                    int journeyId  = info.Length > 0 ? int.Parse(info[0]) : 0;
                    //int customerId  = info.Length > 0 ? int.Parse(info[0]) : 2140;
                    //string journeyId = info.Length > 1 ? info[1] : "";
                    //string clientRef = info.Length > 2 ? info[2] : "";
                    
                   
                    AppUtility.Log2File("PredictionQueueManager", string.Format("Processing Item from Queue. Journey: {0},", msg.Body.ToString()), true);
                   // AppUtility.Log2File("PredictionQueueManager", string.Format("Parsed Info:  CustomerId:{0}, JourneyId:{1}, ClientRef:{2}", customerId, journeyId, clientRef), true);
                    try
                    {
                        bool result = false;    
                        //    IBI.Implementation.Journey.JourneyController.CheckPredictions(customerId, journeyId, clientRef);

                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            var journey = dbContext.Journeys.First(j => j.JourneyId == journeyId);

                            if(journey==null)
                            { 
                                return; 
                            }
                            else
                            {
                                AppUtility.Log2File("PredictionQueueManager", string.Format("Parsed Info:  CustomerId:{0}, JourneyId:{1}, ClientRef:{2}", journey.CustomerId, journey.JourneyId, journey.ClientRef.ToString()), true);

                                IJourneyPredictionHandler jpfactory = new SyncScheduleFactory().GetJourneyHandler(journey.CustomerId);
                                
                                if (jpfactory == null)
                                {
                                    AppUtility.Log2File("PredictionQueueManager", string.Format("There is some problem with Prediction Schedular_Configuration.xml for customer: {0}", journey.CustomerId), true);
                                }

                                jpfactory.CustomerId = journey.CustomerId;
                                result = jpfactory.FetchJourneyPredictions(journey.JourneyId, journey.ClientRef.ToString());

                            }
                        }
                         
                        if (result)
                        {
                            AppUtility.Log2File("PredictionQueueManager", string.Format("Prediction fetched Successfully for journey: {0}", journeyId), true);
                        }
                        else
                        {
                            AppUtility.Log2File("PredictionQueueManager",  "Queue Item discarded, [Already exists] OR [Error occured --> System Logs]", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        // throw new Exception(string.Format("Errors while creating new sign during Hogia realtime journey sign on. [{0}]", ex.Message));
                        AppUtility.Log2File("PredictionQueueManager", string.Format("Prediction Queue Timer CRASHES while fetching prediction data for journey: {0} --> {1}", journeyId, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)), true);
                        AppUtility.WriteError(string.Format("Prediction Queue Timer CRASHES while fetching predictions for journey: {0}. {1}", journeyId, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
                    }
                }


            }//end while


        }
         

        public void Dispose()
        {
            this.Dispose();
        }
    }
}
