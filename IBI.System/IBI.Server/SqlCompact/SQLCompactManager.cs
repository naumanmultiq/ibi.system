﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using System.IO;
using IBI.DataAccess.DBModels;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Configuration;


namespace IBI.Server.SqlCompact
{
    class SQLCompactManager
    {

        //in MB
        public static string MaxCompactDBSize
        {
            get
            {
                return ConfigurationManager.AppSettings["MaxCompactDBSize"].ToString();      
            }

        } 
          
        private static SqlCeConnection GetNewDatabase (string dbfileName)
        {
          
             string fileName = Path.Combine(AppSettings.GetSQLCEDirectory(), dbfileName + ".sdf");
             


             if (File.Exists(fileName))
             {
                 File.Delete(fileName);
             }


             string connectionString = string.Format("DataSource=\"{0}\"; Password='{1}'", fileName, "123");
              //connectionString = ConfigurationManager.ConnectionStrings["CompactDatabase"].ToString();
              SqlCeConnection conn = null;

            try
            {
                var en = new SqlCeEngine(connectionString);
                 
                en.CreateDatabase();
                 
                conn = new SqlCeConnection(connectionString);

                conn.Open();

                SqlCeCommand cmd = new SqlCeCommand("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE (TABLE_NAME IN ('BusEventLog'))", conn);
                int totTables = int.Parse(cmd.ExecuteScalar().ToString());

                conn.Close();

                if(totTables==0)
                {
                    CreateTables(conn);  
                }
                //if (createTables)
                //    CreateTables(conn);  

            }
            catch(Exception ex)
            {
                AppUtility.Log2File("LogManager", " SQLCompactManager -- GetNewDatabase() Crashes: --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
                throw ex;
            }
              
              return conn;
        }

        public static SqlCeConnection GetConnection(string dbFileName = "")
        {
            SqlCeConnection conn = null;

            try
            {
                

                string targetDbFile = "";

                if(dbFileName!="")
                {
                    targetDbFile = dbFileName;                
                }
                else
                {
                
                    foreach(string fileName in Directory.GetFiles(AppSettings.GetSQLCEDirectory()))
                    {
                        FileInfo fl = new FileInfo(fileName);
                        if (fl.Extension.ToLower() != ".sdf")
                            continue;

                        if ( ((fl.Length) / 1024 / 1024) >= int.Parse(MaxCompactDBSize))
                            continue;

                        targetDbFile = fl.FullName;
                    }
                }

                if (targetDbFile=="")
                {
                   conn = GetNewDatabase(DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                }
                else
                {
                    string connectionString = string.Format("DataSource=\"{0}\"; Password='{1}';", targetDbFile, "123");
                    conn = new SqlCeConnection(connectionString);                 
                }
                  
            }

            catch(Exception ex)
            {
                AppUtility.Log2File("LogManager", " SQLCompactManager -- GetConnection() Crashes: --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }     

            return conn;
           
        }

        public static void RemoveDatabase(string dbFileName)
        {             
            if (File.Exists(dbFileName))
            {
                try
                {
                    File.Delete(dbFileName);   
                }
                catch(Exception ex1)
                {
                    AppUtility.Log2File("LogManager", " SQLCompactManager -- RemoveDatabase() Crashes: --> " + ex1.Message + Environment.NewLine + ex1.StackTrace, true);
                    throw ex1;
                }
            }
            
        }

        private static void CreateTables(SqlCeConnection conn)
        {
  

            //foreach (DataTable table in sDS.Tables)
            //{
//            string sqlBusEventLogs = @"
//CREATE TABLE [BusEventLog](
//	[SQLIdentity] [int] IDENTITY(1,1) NOT NULL,
//	[Timestamp] [datetime] NOT NULL,
//	[BusNumber] [nvarchar](4000) NOT NULL,
//	[ClientType] [nvarchar](4000) NOT NULL,
//	[IsMaster] [bit] NULL,
//	[IsOverwritten] [bit] NULL,
//	[Source] [nvarchar](4000) NOT NULL,
//	[Line] [nvarchar](4000) NOT NULL,
//	[From] [nvarchar](4000) NOT NULL,
//	[Destination] [nvarchar](4000) NOT NULL,
//	[JourneyNumber] [nvarchar](4000) NOT NULL,
//	[StopNumber] [bigint] NULL,
//	[Zone] [int] NULL,
//	[EventSource] [nvarchar](4000) NOT NULL,
//	[EventId] [nvarchar](4000) NOT NULL,
//	[EventData] [nvarchar](4000) NOT NULL,
//	[Result] [int] NOT NULL,
//	[ExtraData] [nvarchar](4000) NULL,
//	[GPS1_Temp] [nvarchar](4000) NULL,
//	[GPS2_Temp] [nvarchar](4000) NULL
//    )";

            string sqlBusEventLogs = @"
CREATE TABLE [BusEventLog](
	[SQLIdentity] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[BusNumber] [nvarchar](50) NOT NULL,
	[ClientType] [nvarchar](50) NULL,
	[IsMaster] [bit] NULL,
	[IsOverwritten] [bit] NULL,
	[Source] [nvarchar](100) NOT NULL,
	[Line] [nvarchar](50) NOT NULL,
	[From] [nvarchar](500) NOT NULL,
	[Destination] [nvarchar](500) NOT NULL,
	[JourneyNumber] [nvarchar](500) NOT NULL,
	[StopNumber] [bigint] NULL,
	[Zone] [int] NULL,
	[EventSource] [nvarchar](100) NOT NULL,
	[EventId] [nvarchar](100) NOT NULL,
	[EventData] [nvarchar](4000) NOT NULL,
	[Result] [int] NOT NULL,
	[ExtraData] [nvarchar](4000) NULL,
	[GPS1_Temp] [nvarchar](400) NULL,
	[GPS2_Temp] [nvarchar](400) NULL
    )";

            string sqlBusLocationLog = @"CREATE TABLE [BusLocationLog](
	[SQLIdentity] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[BusNumber] [nvarchar](50) NOT NULL,
	[ClientType] [nvarchar](50) NOT NULL,	
	[Speed] [nvarchar](100) NOT NULL,
	[Player_PID] [int] NOT NULL,
	[SystemStatus] [nvarchar](4000) NOT NULL,
	[GPS1_Temp] [nvarchar](400) NULL,
	[GPS2_Temp] [nvarchar](400) NULL)";


            string sqlBusProgressLog = @"CREATE TABLE [BusProgressLog](
	[Timestamp] [datetime] NOT NULL,
	[BusNumber] [int] NOT NULL,
	[ClientType] [nvarchar](50) NOT NULL,
	[IsMaster] [bit] NOT NULL,
	[IsOnline] [bit] NOT NULL,
	[Source] [nvarchar](50) NOT NULL,
	[IsOverwritten] [bit] NOT NULL,
	[JourneyNumber] [bigint] NOT NULL,
	[Line] [nvarchar](50) NOT NULL,
	[From] [nvarchar](500) NOT NULL,
	[Destination] [nvarchar](500) NOT NULL,
	[StopNumber] [bigint] NOT NULL,
	[Zone] [int] NOT NULL,
	[Latitude] [nvarchar](50) NOT NULL,
	[Longitude] [nvarchar](50) NOT NULL,
	[LogTrigger] [nvarchar](50) NOT NULL,
	[Data] [nvarchar](4000) NOT NULL,
	[Result] [int] NOT NULL,
	[SignData] [nvarchar](4000) NULL)";



            SqlCeCommand cmdEnvt;
            SqlCeCommand cmdLoc;
            SqlCeCommand cmdProg;

            cmdEnvt = new SqlCeCommand(sqlBusEventLogs, conn);
            cmdLoc = new SqlCeCommand(sqlBusLocationLog, conn);
            cmdProg = new SqlCeCommand(sqlBusProgressLog, conn);


            try
            {
                conn.Open();

                Console.WriteLine(cmdEnvt.ExecuteNonQuery());
                Console.WriteLine(cmdLoc.ExecuteNonQuery());
                Console.WriteLine(cmdProg.ExecuteNonQuery());   
            }
            catch(Exception ex)
            {
                AppUtility.Log2File("LogManager", " SQLCompactManager -- CreateTables() Crashes: --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
                AppUtility.WriteError("  Error creating DB Tables in .SDF " + conn.Database + ". " + ex.Message);
            }
            finally
            {

                conn.Close(); 
            }
        }
 
    }
        
}
