﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IBI.Handlers
{
    public class CustomMapHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        //public string JourneyId { get; set; }



        public void ProcessRequest(HttpContext context)
        {
            if(context.Request["scheduleId"] != null)
            {
                string scheduleId = context.Request["scheduleId"];
                SendResponse(context, GetJourneyData(int.Parse(scheduleId), int.Parse(context.Request["minutes"])));
            }
            else
            {
                SendResponse(context, "");
            } 
        }

        private void SendResponse(HttpContext context, string data)
        {
            StringBuilder sb = new StringBuilder();
            //sb.Append(GetDestinationTree(int.Parse(customerID)));
            sb.Append(data);

            Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

            //Set the HTTP MIME type of the output stream 
            context.Response.ContentType = "application/json";

            //Write the data out to the client. 
            context.Response.BinaryWrite(fileContent);
        }

        private string GetJourneyData(int scheduleId, int minutes)
        {
            String json = "[";
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                //var buses = dbContext.Buses.Where(b => b.LastKnownLocation != null && b.InOperation == true).ToList();

                int rowCounter = 0;
                //foreach (var bus in buses)
                {
                    //if (bus.Clients != null && bus.Clients.Count() > 0 && bus.Clients.OrderByDescending(c => c.LastPing).FirstOrDefault().LastPing.AddMinutes(5) <= DateTime.Now) continue;

                    //string online1 = (bus.Clients != null && bus.Clients.Count() > 0 && bus.Clients.OrderByDescending(c => c.LastPing).FirstOrDefault().LastPing.AddMinutes(5) >= DateTime.Now ? "true" : "false");
                    var journeys = dbContext.Journeys.Where(s => s.JourneyStateId == 1 && (s.ScheduleId == scheduleId || scheduleId == 0)).OrderByDescending(j => j.JourneyId);

                    foreach (var journey in journeys)
                    {
                        var bus = journey.Bus;
                        //var journey = bus.Journeys.Where(s => s.JourneyStateId == 1 && s.ScheduleId == scheduleId).OrderByDescending(j => j.JourneyId).FirstOrDefault();
                        if (journey == null) continue;
                        var journeystops = journey.JourneyStops.Where(js => js.ActualArrivalTime != null).OrderByDescending(l => l.StopSequence).FirstOrDefault();
                        if (journeystops == null) continue;
                        if (journeystops.PlannedArrivalTime == null || journeystops.ActualArrivalTime == null) continue;
                        string online1 = (journeystops.PlannedArrivalTime.Value - journeystops.ActualArrivalTime.Value).TotalMinutes < -minutes ? "false" : "true";
                        object latitudeObject = bus.LastKnownLocation != null ? bus.LastKnownLocation.Latitude : null;
                        object longitudeObject = bus.LastKnownLocation != null ? bus.LastKnownLocation.Longitude : null;
                        String latitude = latitudeObject is Double ? ((Double)latitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                        String longitude = longitudeObject is Double ? ((Double)longitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                        json += (rowCounter == 0 ? "" : ",");
                        json += "{";
                        json += "\"BusNumber\":\"" + bus.BusNumber.ToString() + " - " + ((int)(journeystops.PlannedArrivalTime.Value - journeystops.ActualArrivalTime.Value).TotalMinutes).ToString() + "\"";
                        json += ",\"Online\":" + online1;
                        json += ",\"Position\":{";
                        json += "\"lat\":" + latitude;
                        json += ",\"long\":" + longitude + "}";
                        json += "}";
                        rowCounter++;
                    }
                   
                }
            }
            json += "]";
            return json;

        }

        private static string ParseDateToString(object d)
        {
            if (d == null)
                return string.Empty;

            DateTime val = DateTime.MinValue;
            bool result = false;
            result = DateTime.TryParse(d.ToString(), out val);

            if (result)
            {
                return val.ToString("yyyy-MM-ddTHH:mm:ss");
            }

            return string.Empty;
        }



        #endregion




    }

}
