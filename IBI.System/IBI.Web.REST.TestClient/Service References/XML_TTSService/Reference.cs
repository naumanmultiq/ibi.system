﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace REST.TestClient.XML_TTSService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://schemas.mermaid.dk/IBI", ConfigurationName="XML_TTSService.ITTSService")]
    public interface ITTSService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://schemas.mermaid.dk/IBI/ITTSService/GetTTSData", ReplyAction="http://schemas.mermaid.dk/IBI/ITTSService/GetTTSDataResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        REST.TestClient.XML_TTSService.TTSData GetTTSData();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://schemas.mermaid.dk/IBI/ITTSService/GetTTSDataFromDate", ReplyAction="http://schemas.mermaid.dk/IBI/ITTSService/GetTTSDataFromDateResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        REST.TestClient.XML_TTSService.TTSData GetTTSDataFromDate(string fromDate);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18033")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.mermaid.dk/IBI")]
    public partial class TTSData : object, System.ComponentModel.INotifyPropertyChanged {
        
        private AudioFile[] filesField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=0)]
        public AudioFile[] Files {
            get {
                return this.filesField;
            }
            set {
                this.filesField = value;
                this.RaisePropertyChanged("Files");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18033")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.mermaid.dk/IBI")]
    public partial class AudioFile : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int versionField;
        
        private string fileField;
        
        private string hashField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.RaisePropertyChanged("Version");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string File {
            get {
                return this.fileField;
            }
            set {
                this.fileField = value;
                this.RaisePropertyChanged("File");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Hash {
            get {
                return this.hashField;
            }
            set {
                this.hashField = value;
                this.RaisePropertyChanged("Hash");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITTSServiceChannel : REST.TestClient.XML_TTSService.ITTSService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TTSServiceClient : System.ServiceModel.ClientBase<REST.TestClient.XML_TTSService.ITTSService>, REST.TestClient.XML_TTSService.ITTSService {
        
        public TTSServiceClient() {
        }
        
        public TTSServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TTSServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TTSServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TTSServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public REST.TestClient.XML_TTSService.TTSData GetTTSData() {
            return base.Channel.GetTTSData();
        }
        
        public REST.TestClient.XML_TTSService.TTSData GetTTSDataFromDate(string fromDate) {
            return base.Channel.GetTTSDataFromDate(fromDate);
        }
    }
}
