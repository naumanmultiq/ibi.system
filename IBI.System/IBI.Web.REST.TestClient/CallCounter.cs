﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace REST.TestClient
{
    public class CallCounter : IDisposable
    {
        private String name;

        PerformanceCounter totalCounter;
        PerformanceCounter activeCounter;

        public CallCounter(String name)
        {
            this.name = name;

            this.RegisterCounters();

            totalCounter = new PerformanceCounter();
            totalCounter.CategoryName = "testibi.mermaid.dk - " + name;
            totalCounter.CounterName = name + " - Total";
            totalCounter.ReadOnly = false;

            totalCounter.Increment();

            activeCounter = new PerformanceCounter();
            activeCounter.CategoryName = "testibi.mermaid.dk - " + name;
            activeCounter.CounterName = name + " - Active";
            activeCounter.ReadOnly = false;

            activeCounter.Increment();
        }

        private void RegisterCounters()
        {
            // Bind the counters to a PerformanceCounterCategory
            // Check if the category already exists or not.
            String categoryName = "testibi.mermaid.dk - " + name;

            if (!PerformanceCounterCategory.Exists(categoryName))
            {
                CounterCreationDataCollection col = new CounterCreationDataCollection();

                // Create two custom counter objects.
                CounterCreationData activeCounterData = new CounterCreationData();
                activeCounterData.CounterName = name + " - Active";
                activeCounterData.CounterHelp = "Active calls";
                activeCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

                CounterCreationData totalCounterData = new CounterCreationData();
                totalCounterData.CounterName = name + " - Total";
                totalCounterData.CounterHelp = "Total calls";
                totalCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

                // Add custom counter objects to CounterCreationDataCollection.
                col.Add(activeCounterData);
                col.Add(totalCounterData);

                PerformanceCounterCategory category = PerformanceCounterCategory.Create(categoryName, "testibi.mermaid.dk", col);
            }
        }

        public void Dispose()
        {
            activeCounter.Decrement();
            activeCounter.Close();
            activeCounter = null;

            totalCounter.Close();
            totalCounter = null;
        }
    }
}