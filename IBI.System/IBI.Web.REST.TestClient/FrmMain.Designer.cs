﻿namespace REST.TestClient
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtServerUrl = new System.Windows.Forms.TextBox();
            this.BtnUploadLog = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnLogsParse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtLogFile = new System.Windows.Forms.TextBox();
            this.BtnLogUploadBrowse = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TxtOutput = new System.Windows.Forms.TextBox();
            this.LogParseWorker = new System.ComponentModel.BackgroundWorker();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(646, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ScheduleService";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "REST Server";
            // 
            // TxtServerUrl
            // 
            this.TxtServerUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtServerUrl.Location = new System.Drawing.Point(100, 6);
            this.TxtServerUrl.Name = "TxtServerUrl";
            this.TxtServerUrl.Size = new System.Drawing.Size(558, 20);
            this.TxtServerUrl.TabIndex = 2;
            this.TxtServerUrl.Text = "http://ibi.mermaid.dk/REST/";
            // 
            // BtnUploadLog
            // 
            this.BtnUploadLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnUploadLog.Location = new System.Drawing.Point(555, 26);
            this.BtnUploadLog.Name = "BtnUploadLog";
            this.BtnUploadLog.Size = new System.Drawing.Size(75, 23);
            this.BtnUploadLog.TabIndex = 0;
            this.BtnUploadLog.Text = "Upload";
            this.BtnUploadLog.UseVisualStyleBackColor = true;
            this.BtnUploadLog.Click += new System.EventHandler(this.BtnUploadLog_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnLogsParse);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.TxtLogFile);
            this.groupBox2.Controls.Add(this.BtnLogUploadBrowse);
            this.groupBox2.Controls.Add(this.BtnUploadLog);
            this.groupBox2.Location = new System.Drawing.Point(12, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(646, 93);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Logs";
            // 
            // BtnLogsParse
            // 
            this.BtnLogsParse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnLogsParse.Location = new System.Drawing.Point(555, 53);
            this.BtnLogsParse.Name = "BtnLogsParse";
            this.BtnLogsParse.Size = new System.Drawing.Size(75, 23);
            this.BtnLogsParse.TabIndex = 8;
            this.BtnLogsParse.Text = "Parse";
            this.BtnLogsParse.UseVisualStyleBackColor = true;
            this.BtnLogsParse.Click += new System.EventHandler(this.BtnLogsParse_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Parse logs";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Log file";
            // 
            // TxtLogFile
            // 
            this.TxtLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtLogFile.Location = new System.Drawing.Point(97, 29);
            this.TxtLogFile.Name = "TxtLogFile";
            this.TxtLogFile.ReadOnly = true;
            this.TxtLogFile.Size = new System.Drawing.Size(386, 20);
            this.TxtLogFile.TabIndex = 4;
            // 
            // BtnLogUploadBrowse
            // 
            this.BtnLogUploadBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnLogUploadBrowse.Location = new System.Drawing.Point(489, 29);
            this.BtnLogUploadBrowse.Name = "BtnLogUploadBrowse";
            this.BtnLogUploadBrowse.Size = new System.Drawing.Size(34, 20);
            this.BtnLogUploadBrowse.TabIndex = 1;
            this.BtnLogUploadBrowse.Text = "..";
            this.BtnLogUploadBrowse.UseVisualStyleBackColor = true;
            this.BtnLogUploadBrowse.Click += new System.EventHandler(this.BtnLogUploadBrowse_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Multiselect = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TxtOutput);
            this.groupBox3.Location = new System.Drawing.Point(12, 197);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(646, 212);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output";
            // 
            // TxtOutput
            // 
            this.TxtOutput.BackColor = System.Drawing.Color.Black;
            this.TxtOutput.ForeColor = System.Drawing.Color.White;
            this.TxtOutput.Location = new System.Drawing.Point(19, 33);
            this.TxtOutput.Multiline = true;
            this.TxtOutput.Name = "TxtOutput";
            this.TxtOutput.ReadOnly = true;
            this.TxtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtOutput.Size = new System.Drawing.Size(612, 161);
            this.TxtOutput.TabIndex = 0;
            // 
            // LogParseWorker
            // 
            this.LogParseWorker.WorkerReportsProgress = true;
            this.LogParseWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.LogParseWorker_DoWork);
            this.LogParseWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.LogParseWorker_ProgressChanged);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(15, 415);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(643, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(505, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 450);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.TxtServerUrl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmMain";
            this.Text = "IBI REST - Test Client";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtServerUrl;
        private System.Windows.Forms.Button BtnUploadLog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtLogFile;
        private System.Windows.Forms.Button BtnLogUploadBrowse;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TxtOutput;
        private System.Windows.Forms.Button BtnLogsParse;
        private System.Windows.Forms.Label label2;
        private System.ComponentModel.BackgroundWorker LogParseWorker;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button button1;
    }
}

