﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_Aspx
{
    public partial class InfotainmentDump_Standalone : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String refreshInterval = Request.QueryString["refresh"];

            if (!String.IsNullOrEmpty(refreshInterval))
            {
                ClientScript.RegisterClientScriptBlock(typeof(String), "RefreshScript", "<script type=\"text/JavaScript\">setTimeout(\"location.reload(true);\", " + refreshInterval + ");</script>");
            }
        }
    }
}