﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusSign.aspx.cs" Inherits="CMS_Aspx.BusSign" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IBI Bus Sign</title>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="277px">
        <tr>
            <td colspan="3" style="width: 277px"><img src="Images/bus_front_top.jpg" alt="" /></td>
        </tr>
        <tr>
            <td style="width: 60px"><img src="Images/bus_front_left.jpg" alt="" /></td>
            <td style="width: 160px">
                <asp:Image ID="ImgSign" runat="server" Height="24px" 
                    ImageUrl="~/Images/BusSign_Empty.jpg" Width="160px" /></td>
            <td style="width: 57px"><img src="Images/bus_front_right.jpg" alt="" /></td>
        </tr>
        <tr>
            <td colspan="3" style="width: 277px"><img src="Images/bus_front_bottom.jpg" alt="" /></td>
        </tr>
    </table>
    </form>
</body>
</html>
