﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class BusSign : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String timestamp = Request["timestamp"];
            String busnumber = Request["busnumber"];
            String clientType = Request["clienttype"];

            if (!String.IsNullOrEmpty(timestamp) && !String.IsNullOrEmpty(busnumber) && !String.IsNullOrEmpty(clientType))
            {
                String imageUrl = "Resources/Signs/" + clientType + "/" + busnumber + "/sign_" + timestamp.Replace(":", "").Replace("-", "").Replace(" ", "") + ".jpg";
                String imagePath = Server.MapPath(imageUrl);

                if (!File.Exists(imagePath))
                {
                    using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                    {
                        connection.Open();

                        String script = "SELECT [SignData]" + Environment.NewLine +
                                        "FROM [BusProgressLog]" + Environment.NewLine +
                                        "WHERE [Timestamp]='" + timestamp + "'" + Environment.NewLine +
                                        "  AND [BusNumber]=" + busnumber + Environment.NewLine +
                                        "  AND [ClientType]='" + clientType + "'";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            using (SqlDataReader dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();

                                    String base64Image = dataReader.GetString(dataReader.GetOrdinal("SignData"));

                                    if (!String.IsNullOrEmpty(base64Image))
                                    {
                                        Byte[] imageBytes = Convert.FromBase64String(base64Image);
                                        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                                        ms.Write(imageBytes, 0, imageBytes.Length);
                                        System.Drawing.Bitmap image = (System.Drawing.Bitmap) System.Drawing.Bitmap.FromStream(ms, true);

                                        //System.Drawing.Color signColor = System.Drawing.Color.FromArgb(157, 102, 54);

                                        //for (int x = 0; x < image.Width; x++)
                                        //{
                                        //    for (int y = 0; y < image.Height; y++)
                                        //    {
                                        //        System.Drawing.Color currentColor = image.GetPixel(x, y);

                                        //        if (currentColor.R < 10 && currentColor.G < 10 && currentColor.B < 10)
                                        //        {
                                        //            image.SetPixel(x, y, signColor);
                                        //        }
                                        //    }
                                        //} 

                                        FileInfo targetFile = new FileInfo(imagePath);

                                        if (!targetFile.Directory.Exists)
                                            targetFile.Directory.Create();

                                        image.Save(imagePath);
                                    }
                                }
                            }
                        }
                    }
                }

                if (File.Exists(imagePath))
                {
                    this.ImgSign.ImageUrl = imageUrl;
                }
            }
        }
    }
}