﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_Aspx
{
    public partial class SystemTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LnkMoviaTest_Click(object sender, EventArgs e)
        {
            this.TxtMoviaResult.Text = "Connecting to Movia WS" + Environment.NewLine;
            
            using (Movia.Services.JourneyProgress.ServiceClient serviceClient = new Movia.Services.JourneyProgress.ServiceClient())
            {
                this.TxtMoviaResult.Text += "Service reference created" + Environment.NewLine;
                
                Movia.Services.JourneyProgress.JourneyRow[] serviceResult = null;

                try
                {
                    serviceClient.ClientCredentials.UserName.UserName = "mermaid";
                    serviceClient.ClientCredentials.UserName.Password = "220711Mer";

                    this.TxtMoviaResult.Text += "Credentials configured" + Environment.NewLine;
                    this.TxtMoviaResult.Text += "Invoking WS" + Environment.NewLine;
                    
                    serviceResult = serviceClient.GetJourneyProgress(1245);

                    this.TxtMoviaResult.Text += "Result returned from WS" + Environment.NewLine;
                    this.TxtMoviaResult.Text += "Movia WS working and running" + Environment.NewLine;     
                }
                catch (Exception ex)
                {
                    this.TxtMoviaResult.Text += "Error communicating with Movia WS" + Environment.NewLine;
                    
                    Exception exception = ex;

                    while (ex != null)
                    {
                        this.TxtMoviaResult.Text += exception.Message + Environment.NewLine;
                        this.TxtMoviaResult.Text += exception.StackTrace + Environment.NewLine;
                        this.TxtMoviaResult.Text += Environment.NewLine;

                        exception = exception.InnerException;
                    }
                }

                //this.TxtMoviaResult.Text += serviceResult.Length + " rows returned" + Environment.NewLine;                  
            }
        }
    }
}