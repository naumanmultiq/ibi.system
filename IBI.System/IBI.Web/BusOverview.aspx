﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BusOverview.aspx.cs"
    Inherits="CMS_Aspx.BusOverview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="javascript" type="text/javascript">
        function setInOperation(busnumber, inoperation) {
            //alert("toggle: " + busnumber);
            document.location = "BusOverview.aspx?command=setInOperation&busnumber=" + busnumber + "&inoperation=" + inoperation;
        }

        function openScreenshots(busnumber) {
            window.open("ScreenshotViewer.aspx?clienttype=VTC&busnumber=" + busnumber, "ScreenshotViewer", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=350,height=320");

            return true;
        }
    </script>
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 30px;
            height: 20px;
        }
        .style2
        {
            width: 300px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table>
            <tr>
                <td style="width:20px">
                    <asp:CheckBox ID="ChkFilterUnaccounted" Text="" Checked="true" runat="server" />
                </td>
                <td style="width: 20px; background-color: #FF0000">
                    &nbsp;
                </td>
                <td style="width: 30px" align="right" id="CllUnaccounted" runat="server">
                    &nbsp;</td>
                <td style="width: 150px">
                    unaccounted
                </td>
                <td style="width:20px">
                    <asp:CheckBox ID="ChkFilterWithServiceCase" Text="" Checked="true" runat="server" />
                </td>
                <td style="width: 20px; background-color: #FFC90E">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" id="CllService" runat="server">
                     &nbsp;</td>
                <td style="width: 150px">
                    has service case</td>
                    <td style="width:20px">
                    <asp:CheckBox ID="ChkOnline" Text="" Checked="true" runat="server" />
                </td>
                <td style="width: 20px; background-color: #008000">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" id="CllOnline" runat="server">
                     &nbsp;</td>
                <td style="width: 150px">
                    online
                </td>
                <td style="width: 20px;">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" id="CllInOperation" runat="server">
                     &nbsp;</td>
                <td style="width: 150px">
                    in operation</td>
            </tr>
            <tr>
                <td style="width:20px">
                    <asp:CheckBox ID="ChkFilterUnaccountedCritical" Text="" Checked="true" runat="server" />
                </td>
                <td style="width: 20px; background-color: #FF4500">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" id="CllUnaccountedCritical" runat="server">
                     &nbsp;</td>
                <td style="width: 150px">
                    unaccounted critical
                </td>
                <td style="width:20px">
                    &nbsp;
                </td>
                <td style="width: 20px;">
                    &nbsp;
                </td>
                <td style="width: 20px;">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" runat="server">
                     &nbsp;</td>
                     <td style="width:20px">
                    <asp:CheckBox ID="ChkFilterWithoutSchedule" Text="" Checked="true" runat="server" />
                </td>
                <td style="width: 20px; background-color: #90EE90">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" id="CllNoSchedule" runat="server">
                     &nbsp;</td>
                <td style="width: 150px">
                    with no schedule
                </td>
                <td style="width: 20px;">
                    &nbsp;
                </td>
                 <td style="width: 30px" align="right" id="CllInactive" runat="server">
                     &nbsp;</td>
                <td style="width: 150px">
                    inactive</td>
            </tr>
            <tr>
                <td colspan="15" align="right">
                    <asp:ImageButton ID="BtnRefresh" runat="server" 
                        ImageUrl="~/Images/refresh_32.png" onclick="BtnRefresh_Click" />
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td class="style1"><img src="Images/Warning_16.png" id="imgMarmkedForService" alt="" /></td>
                <td class="style2">Marked for service</td>
            </tr>
            <tr>
                <td style="width:30px;"><img src="Images/Wrench_16.png" id="img1" alt="" /></td>
                <td style="width:300px;">Has service case</td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="GridOverview" runat="server" AutoGenerateColumns="False" GridLines="None"
            AllowSorting="True" BorderColor="Black" EnableViewState="False">
            <AlternatingRowStyle BackColor="LightGray" />
            <Columns>
                <asp:ImageField>
                    <HeaderStyle Width="15px" />
                </asp:ImageField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("InOperation") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="ChkInOperation" runat="server" Checked='<%# Bind("InOperation") %>' />
                    </ItemTemplate>
                    <HeaderStyle Width="20px" />
                </asp:TemplateField>
                <asp:BoundField DataField="BusNumber" HeaderText="Bus" SortExpression="BusNumber">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:CheckBoxField DataField="IsOK" HeaderText="OK" Visible="False">
                    <HeaderStyle Width="50px" />
                </asp:CheckBoxField>
                <asp:BoundField DataField="LastDCUPing" HeaderText="DCU" SortExpression="LastDCUPing">
                    <HeaderStyle Width="140px" />
                </asp:BoundField>
                <asp:BoundField DataField="LastVTCPing" HeaderText="VTC" SortExpression="LastVTCPing">
                    <HeaderStyle Width="140px" />
                </asp:BoundField>
                <asp:BoundField DataField="LastInfotainmentPing" HeaderText="Infotainment" SortExpression="LastInfotainmentPing">
                    <HeaderStyle Width="140px" />
                </asp:BoundField>
                <asp:BoundField DataField="LastHotspotPing" HeaderText="Hotspot" SortExpression="LastHotspotPing">
                    <HeaderStyle Width="140px" />
                </asp:BoundField>
                <asp:BoundField DataField="LastScheduleTime" HeaderText="Last Schedule" SortExpression="LastScheduleTime">
                    <HeaderStyle Width="140px" />
                </asp:BoundField>
                <asp:BoundField DataField="Journey" HeaderText="Journey (S/A)">
                    <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:ImageButton ID="BtnScreenshots" runat="server" ImageUrl="~/Images/Thumbnail_16.png"
                            CausesValidation="False" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px" />
                </asp:TemplateField>
                <asp:ImageField DataImageUrlField="MarkedForServiceIcon">
                    <HeaderStyle Width="15px" />
                </asp:ImageField>
                <asp:ImageField DataImageUrlField="ServiceLevelIcon">
                    <HeaderStyle Width="15px" />
                </asp:ImageField>
            </Columns>
            <HeaderStyle ForeColor="White" HorizontalAlign="Left" Height="20px" CssClass="TableHeader" />
            <RowStyle BackColor="WhiteSmoke" />
        </asp:GridView>
    </div>
</asp:Content>
