﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class BusProgressHistory : System.Web.UI.Page
    {
        private const int EVENTTYPE_SIGNS = 1;
        private const int EVENTTYPE_VA = 2;
        private const int EVENTTYPE_STOPS = 3;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GridHistory.RowDataBound += new GridViewRowEventHandler(GridHistory_RowDataBound);

            if (!this.IsPostBack)
            {
                this.CtrlHistoryDate.SelectedDate = DateTime.Now;
                this.loadEventTypes();

                this.loadData();
            }

            //this.loadData();
        }

        private void GridHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int selectedEventType = 0;
            Int32.TryParse(this.CboEventType.SelectedValue, out selectedEventType);

            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    if(String.Compare("M", headerCell.Text) != 0 && String.Compare("O", headerCell.Text) != 0)
                        headerCell.HorizontalAlign = HorizontalAlign.Left;

                    headerCell.VerticalAlign = VerticalAlign.Middle;
                }


                DataControlField signImageCell = this.GridHistory.Columns[this.GridHistory.Columns.Count - 1];


                switch (selectedEventType)
                {
                    case EVENTTYPE_SIGNS:
                        signImageCell.Visible = true;

                        break;

                    default:
                        signImageCell.Visible = false;

                        break;
                }
            }

            if (e.Row.DataItem != null)
            {
                if (selectedEventType == EVENTTYPE_SIGNS)
                {
                    DataRow dataRow = ((DataRowView)e.Row.DataItem).Row;

                    String busNumber = dataRow["BusNumber"].ToString();
                    String clientType = dataRow["ClientType"].ToString();
                    String timestamp = ((DateTime)dataRow["Timestamp"]).ToString("yyyy-MM-dd HH:mm:ss.000");

                    ((ImageButton)e.Row.FindControl("BtnBusSign")).Attributes.Add("onclick", "openSignPopup('" + timestamp + "','" + busNumber + "','" + clientType + "')");
                }
            }
        }

        private void loadEventTypes()
        {
            DataTable eventTable = new DataTable();
            eventTable.Columns.Add("EventType", typeof(int));
            eventTable.Columns.Add("EventName", typeof(String));

            DataRow dataRow = eventTable.NewRow();
            dataRow["EventType"] = EVENTTYPE_SIGNS;
            dataRow["EventName"] = "Signs";
            eventTable.Rows.Add(dataRow);

            dataRow = eventTable.NewRow();
            dataRow["EventType"] = EVENTTYPE_VA;
            dataRow["EventName"] = "Voice Announcements";
            eventTable.Rows.Add(dataRow);

            dataRow = eventTable.NewRow();
            dataRow["EventType"] = EVENTTYPE_STOPS;
            dataRow["EventName"] = "Journey progress";
            eventTable.Rows.Add(dataRow);

            this.CboEventType.DataValueField = "EventType";
            this.CboEventType.DataTextField = "EventName";

            this.CboEventType.DataSource = eventTable;
            this.CboEventType.DataBind();

            this.CboEventType.SelectedValue = EVENTTYPE_SIGNS.ToString();
        }

        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            this.loadData();
        }

        private void loadData()
        {
            DataTable dataSource = this.generateProgressTable();

            if (!String.IsNullOrEmpty(this.TxtBusNumber.Text))
            {
                int busNumber = int.Parse(this.TxtBusNumber.Text);
                //DateTime logDate = this.CtrlHistoryDate.SelectedDate.Value;

                DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
                dateFormat.ShortDatePattern = "dd-MM-yyyy";
                dateFormat.LongDatePattern = "dd-MM-yyyy";
                dateFormat.FullDateTimePattern = "dd-MM-yyyy";
                dateFormat.DateSeparator = "-";

                DateTime logDate = DateTime.Parse(this.TxtDate.Text, dateFormat);


                DataRow dataRow;

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    DateTime baseTime = new DateTime(logDate.Year, logDate.Month, logDate.Day, 0, 0, 0);

                    String script = "SELECT [BusNumber], [ClientType], [Timestamp], [IsMaster], [Source], [IsOverwritten], [Line], [Destination], [LogTrigger], [Data], [Result]" + Environment.NewLine +
                                    "FROM [BusProgressLog]" + Environment.NewLine +
                                    "WHERE [BusNumber]=" + busNumber + Environment.NewLine +                                    
                                    "AND [Timestamp] BETWEEN '" + baseTime.ToString("yyyy-MM-dd HH:mm:ss") + "' AND '" + baseTime.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "'" + Environment.NewLine;

                   if(this.ChkOnlyMasterEvents.Checked)
                        script += "AND [IsMaster]=1" + Environment.NewLine;

                    int selectedEventType = Int32.Parse(this.CboEventType.SelectedValue);

                    switch (selectedEventType)
                    {
                        case EVENTTYPE_SIGNS:
                            script += "AND [LogTrigger]='SIGNS'";
                            break;

                        case EVENTTYPE_VA:
                            script += "AND [LogTrigger]='VA'";
                            break;

                        case EVENTTYPE_STOPS:
                            script += "AND ([LogTrigger]='DEPARTURE_EVENT' OR [LogTrigger]='ARRIVE_EVENT')";
                            break;
                    }

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                String clientType = dataReader.GetString(dataReader.GetOrdinal("ClientType"));
                                DateTime timestamp = dataReader.GetDateTime(dataReader.GetOrdinal("Timestamp"));
                                Boolean isMaster = dataReader.GetBoolean(dataReader.GetOrdinal("IsMaster"));
                                String source = dataReader.GetString(dataReader.GetOrdinal("Source"));
                                Boolean isOverwritten = dataReader.GetBoolean(dataReader.GetOrdinal("IsOverwritten"));
                                String line = dataReader.GetString(dataReader.GetOrdinal("Line"));
                                String destination = dataReader.GetString(dataReader.GetOrdinal("Destination"));
                                String trigger = dataReader.GetString(dataReader.GetOrdinal("LogTrigger"));
                                String data = dataReader.GetString(dataReader.GetOrdinal("Data"));
                                int result = dataReader.GetInt32(dataReader.GetOrdinal("Result"));

                                dataRow = dataSource.NewRow();
                                dataRow["BusNumber"] = busNumber;
                                dataRow["ClientType"] = clientType;
                                dataRow["Timestamp"] = timestamp;
                                dataRow["IsMaster"] = isMaster;
                                dataRow["Source"] = source;
                                dataRow["IsOverwritten"] = isOverwritten;
                                dataRow["Line"] = line;
                                dataRow["Destination"] = destination;
                                dataRow["LogTrigger"] = trigger;
                                dataRow["Data"] = data;
                                dataRow["Result"] = result;

                                dataSource.Rows.Add(dataRow);
                            }
                        }
                    }
                }

                this.CtrlHistoryDate.SelectedDate = logDate;
            }

            dataSource.DefaultView.Sort = "Timestamp ASC";

            this.GridHistory.DataSource = dataSource;
            this.GridHistory.DataBind();
        }

        private DataTable generateProgressTable()
        {
            DataTable dataSource = new DataTable("BusProgressLog");
            dataSource.Columns.Add("BusNumber", typeof(int));
            dataSource.Columns.Add("ClientType", typeof(String));
            dataSource.Columns.Add("Timestamp", typeof(DateTime));
            dataSource.Columns.Add("IsMaster", typeof(Boolean));
            dataSource.Columns.Add("IsOverwritten", typeof(Boolean));
            dataSource.Columns.Add("Line", typeof(String));
            dataSource.Columns.Add("Source", typeof(String));
            dataSource.Columns.Add("Destination", typeof(String));
            dataSource.Columns.Add("LogTrigger", typeof(String));
            dataSource.Columns.Add("Data", typeof(String));
            dataSource.Columns.Add("Result", typeof(int));

            return dataSource;
        }
    }
}