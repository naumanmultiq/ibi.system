﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BusProgressHistory.aspx.cs"
    Inherits="CMS_Aspx.BusProgressHistory" %>
    <%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
    
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="javascript" type="text/javascript">
     function openSignPopup(timestamp, busnumber, clienttype) {
         window.open("BusSign.aspx?clienttype=" + clienttype + "&busnumber=" + busnumber + "&timestamp=" + timestamp, "BusSignViewer", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=295,height=295");
     }
    </script>
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table border="0">
            <tr>
                <td>Bus number:
                </td>
                <td><asp:TextBox ID="TxtBusNumber" Width="150px" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>History date:
                </td>
                <td><asp:TextBox ID="TxtDate" Width="150px" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Event type:</td>
                <td>
                    <asp:DropDownList ID="CboEventType" Width="150px" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td>Only Master events:</td>
                <td>
                    
                    <asp:CheckBox ID="ChkOnlyMasterEvents" runat="server" />
                    
                </td>
            </tr>
                        <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:ImageButton ID="BtnSearch" runat="server" 
                        ImageUrl="~/Images/ButtonSearch.png" onclick="BtnSearch_Click" />
                            </td>
            </tr>
            <tr>
             <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridHistory" runat="server" AutoGenerateColumns="False" GridLines="None"
                        AllowSorting="True" BorderColor="Black" EmptyDataText="No data.." 
                        ShowHeaderWhenEmpty="True">
                        <AlternatingRowStyle BackColor="LightGray" />
                        <Columns>
                            <asp:BoundField DataField="BusNumber" HeaderText="Bus">
                                <HeaderStyle Width="40px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ClientType">
                                <HeaderStyle Width="40px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Timestamp" HeaderText="Time">
                                <HeaderStyle Width="140px" />
                            </asp:BoundField>
                            <asp:CheckBoxField DataField="IsMaster" HeaderText="M">
                                <HeaderStyle Width="30px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="IsOverwritten" HeaderText="O">
                            <HeaderStyle HorizontalAlign="Center" Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:BoundField DataField="Source" HeaderText="Src">
                            <HeaderStyle Width="40px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Line" HeaderText="Line">
                                <HeaderStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Destination" HeaderText="Destination">
                                <ItemStyle Width="140px" />
                            <HeaderStyle Width="145px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LogTrigger" HeaderText="Trigger">
                                <HeaderStyle Width="150px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Data" HeaderText="Data">
                                <HeaderStyle Width="220px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Result" HeaderText="R">
                                <HeaderStyle Width="30px" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="BtnBusSign" runat="server" 
                                        ImageUrl="~/Images/Thumbnail_16.png" />
                                </ItemTemplate>
                                <HeaderStyle Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle ForeColor="White" HorizontalAlign="Left" Height="20px" 
                            CssClass="TableHeader" />
                        <RowStyle BackColor="WhiteSmoke" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager" runat="Server" />
        <ajaxToolkit:CalendarExtender ID="CtrlHistoryDate" runat="server"
            TargetControlID="TxtDate"
            Format="dd-MM-yyyy" ViewStateMode="Enabled" />
        <ajaxToolkit:FilteredTextBoxExtender ID="CtrlBusNumber" runat="server"
            TargetControlID="TxtBusNumber"         
            FilterType="Numbers" />
</asp:Content>
