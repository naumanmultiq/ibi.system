﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VAFiles.aspx.cs" Inherits="CMS_Aspx.VAFiles" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
    
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="javascript" type="text/javascript">
        function openAudioFile(filename) {
            var downloadUrl = 'http://ibi.mermaid.dk/REST/TTS/TTSFile.mp3?filename=' + filename;
            window.open(downloadUrl, "AudioDownload", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=295,height=295");
   
        }
    </script>
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table border="0">
            <tr>
                <td>Stop name:
                </td>
                <td><asp:TextBox ID="TxtStopName" Width="150px" runat="server"></asp:TextBox></td>
            </tr>
                        <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:ImageButton ID="BtnSearch" runat="server" 
                        ImageUrl="~/Images/ButtonSearch.png" onclick="BtnSearch_Click" />
                            </td>
            </tr>
            <tr>
             <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridVAFiles" runat="server" AutoGenerateColumns="False" GridLines="None"
                        AllowSorting="True" BorderColor="Black" EmptyDataText="No data.." 
                        ShowHeaderWhenEmpty="True">
                        <AlternatingRowStyle BackColor="LightGray" />
                        <Columns>
                            <asp:BoundField DataField="StopName" HeaderText="Stop name">
                                <HeaderStyle Width="300px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LastModified" HeaderText="Modified">
                                <HeaderStyle Width="140px" />
                            </asp:BoundField>
                            <asp:CheckBoxField DataField="Approved" HeaderText="Approved">
                                <HeaderStyle Width="80px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:BoundField DataField="Filename" HeaderText="File name">
                                <HeaderStyle Width="300px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Version" HeaderText="Version">
                                <HeaderStyle Width="60px" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="BtnDownloadFile" runat="server" 
                                        ImageUrl="~/Images/Audio_16.png" />
                                </ItemTemplate>
                                <HeaderStyle Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle ForeColor="White" HorizontalAlign="Left" Height="20px" 
                            CssClass="TableHeader" />
                        <RowStyle BackColor="WhiteSmoke" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
