﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class CtrlInfotainmentDump : System.Web.UI.UserControl
    {
        public int gridWidth = 920;
        public int columnLayoutCount = 6;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GridDump.RowDataBound += new GridViewRowEventHandler(GridDump_RowDataBound);

            String qGridWidth = Request.QueryString["width"];
            String qColumnLayoutCount = Request.QueryString["columns"];

            if (!String.IsNullOrEmpty(qGridWidth))
                gridWidth = int.Parse(qGridWidth);

            if (!String.IsNullOrEmpty(qColumnLayoutCount))
                columnLayoutCount = int.Parse(qColumnLayoutCount);

            this.LoadDumps();
        }

        private void GridDump_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int cellWidth = (int)((gridWidth - (columnLayoutCount * 10)) / columnLayoutCount);

            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    headerCell.Width = Unit.Pixel(cellWidth);
                    headerCell.Text = String.Empty;
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    TableCell cell = e.Row.Cells[i];
                    String cellValue = cell.Text;

                    if (!String.IsNullOrEmpty(cellValue) && cellValue.IndexOf("|") != -1)
                    {
                        String[] cellValues = cellValue.Split('|');

                        String busNumber = cellValues[0];
                        String imageName = cellValues[1];

                        String imageTimestamp = "No image found";

                        if (imageName.IndexOf("VTC") != -1)
                            imageTimestamp = Path.GetFileNameWithoutExtension(imageName).Substring(Path.GetFileNameWithoutExtension(imageName).Length - 15);

                        int imageWidth = 300;
                        int imageHeight = 188;
                        double imageScale = ((double)(cellWidth)) / ((double)(imageWidth));

                        Image dumpImage = new Image();
                        dumpImage.ImageUrl = "~/" + imageName;
                        dumpImage.Width = Unit.Pixel((int)(imageWidth * imageScale));
                        dumpImage.Height = Unit.Pixel((int)(imageHeight * imageScale));
                        dumpImage.AlternateText = "Bus " + busNumber + " | " + imageTimestamp;
                        dumpImage.ToolTip = dumpImage.AlternateText;

                        cell.Text = string.Empty;
                        cell.Controls.Add(dumpImage);

                        cell.Attributes["onclick"] = "openScreenshots('" + busNumber + "', '" + Path.GetFileNameWithoutExtension(imageName) + "')";

                    }
                }
            }
        }

        private void LoadDumps()
        {
            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                List<int> busNumbers = new List<int>();

                String script = "SELECT DISTINCT [BusNumber] FROM [Clients]" + Environment.NewLine +
                                "WHERE [InOperation]=1 AND [ClientType]='VTC'" + Environment.NewLine +
                                "ORDER BY [BusNumber] ASC";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                busNumbers.Add(int.Parse(dataReader.GetString(dataReader.GetOrdinal("BusNumber"))));
                            }
                        }
                    }
                }

                DataTable table = new DataTable();

                for (int i = 0; i < columnLayoutCount; i++)
                {
                    table.Columns.Add("Col" + i, typeof(String));
                }

                DataRow row = null;

                int colCounter = 0;

                foreach (int busNumber in busNumbers)
                {
                    if (colCounter == 0)
                    {
                        row = table.NewRow();
                        table.Rows.Add(row);
                    }

                    row[colCounter] = busNumber + "|" + ResolveImageName(busNumber);

                    colCounter++;

                    if (colCounter % columnLayoutCount == 0)
                        colCounter = 0;
                }

                this.GridDump.DataSource = table;
                this.GridDump.DataBind();
            }
        }

        private String ResolveImageName(int busNumber)
        {
            String imageName = "Images/NoScreenshot.jpg";

            String folderName = "Resources/Screenshots/VTC/" + busNumber + "/";

            if (Directory.Exists(Server.MapPath(folderName)))
            {
                String[] imagePaths = Directory.GetFiles(Server.MapPath(folderName), "Screenshot_" + busNumber + "_VTC_" + DateTime.Now.ToString("yyyyMMdd_HH") + "*.jpg");

                for (int i = imagePaths.Length - 1; i >= 0; i--)
                {
                    String imagePath = imagePaths[i];
                    String imageDate = Path.GetFileNameWithoutExtension(imagePath).Substring(Path.GetFileNameWithoutExtension(imagePath).Length - 15);

                    int year = int.Parse(imageDate.Substring(0, 4));
                    int month = int.Parse(imageDate.Substring(4, 2));
                    int day = int.Parse(imageDate.Substring(6, 2));
                    int hour = int.Parse(imageDate.Substring(9, 2));
                    int minute = int.Parse(imageDate.Substring(11, 2));
                    int second = int.Parse(imageDate.Substring(13, 2));

                    if (DateTime.Now.Subtract(TimeSpan.FromHours(1)) < new DateTime(year, month, day, hour, minute, second))
                    {
                        imageName = folderName + Path.GetFileName(imagePath);

                        break;
                    }
                }
            }

            return imageName;
        }
    }
}