﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BusStatus.aspx.cs" Inherits="IBI.CMS.BusStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="javascript" type="text/javascript">
        function setInOperation(busnumber, inoperation) {
            //alert("toggle: " + busnumber);
            document.location = "BusOverview.aspx?command=setInOperation&busnumber=" + busnumber + "&inoperation=" + inoperation;
        }

        function openScreenshots(busnumber) {
            window.open("ScreenshotViewer.aspx?clienttype=VTC&busnumber=" + busnumber, "ScreenshotViewer", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=350,height=280");

            return true;
        }
    </script>
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table cellpadding="1" cellspacing="0" width="400px">
        <tr>
            <td class="TableHeader" colspan="2">
                &nbsp;
                Component status</td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" style="width: 200px">
                Enter bus number:
            </td>
            <td class="TableBackground" style="width: 200pX">
                <asp:TextBox ID="txtBusNumber" runat="server" Width="142px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" align="center" colspan="2">
                <asp:ImageButton ID="BtnSearch" runat="server" ImageUrl="~/Images/ButtonSearch.png"
                    OnClick="BtnSearch_Click" />
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table cellpadding="1" cellspacing="0" width="400px">
        <tr>
            <td id="cllBusNumber" runat="server" class="TableHeader" align="center" colspan="2">
                No bus number entered</td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2" style="height: 5px">
                &nbsp;
            </td>
        </tr>
                <tr>
            <td class="TableHeader" style="width: 150px">
                &nbsp; Component&nbsp;</td>
            <td class="TableHeader" style="width: 250px">
                MAC 
            </td>
        </tr>
        <tr>
            <td class="TableBackground">
                &nbsp;
                VTC
            </td>
            <td id="CllVTCMac" runat="server" class="TableBackground">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2" style="height: 5px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground">
                &nbsp;
                DCU
            </td>
            <td id="CllDCUMac" runat="server" class="TableBackground">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
      
        <tr>
            <td class="TableHeader" style="width: 150px">
                &nbsp; Component&nbsp;;</td>
            <td class="TableHeader" style="width: 250px">
                Last online 
            </td>
        </tr>
        <tr>
            <td class="TableBackground">
                &nbsp;
                <asp:Image ID="ImgVTCStatus" runat="server" ImageUrl="~/Images/KnobCancel_16.png" />
                &nbsp;VTC
            </td>
            <td id="cllVTCTimestamp" runat="server" class="TableBackground">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2" style="height: 5px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground">
                &nbsp;
                <asp:Image ID="ImgDCUStatus" runat="server" ImageUrl="~/Images/KnobCancel_16.png" />
                &nbsp;DCU
            </td>
            <td id="cllDCUTimestamp" runat="server" class="TableBackground">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground">
                &nbsp;
                <asp:Image ID="ImgHotspotStatus" runat="server" ImageUrl="~/Images/KnobCancel_16.png" />
                &nbsp;Hotspot
            </td>
            <td id="cllHotspotTimestamp" runat="server" class="TableBackground">
                &nbsp;
            </td>
        </tr>
                <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground">
                &nbsp;
                <asp:Image ID="ImgInfotainmentStatus" runat="server" ImageUrl="~/Images/KnobCancel_16.png" />
                &nbsp;Infotainment
            </td>
            <td id="cllInfotainmentTimestamp" runat="server" class="TableBackground">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
        <table cellpadding="1" cellspacing="0" width="400px">
        <tr>
            <td class="TableHeader" colspan="2">
                &nbsp; Resolve MAC address</td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" style="width: 200px">
                Enter 
                MAC:
            </td>
            <td class="TableBackground" style="width: 200pX">
                <asp:TextBox ID="TxtMACAddress" runat="server" Width="142px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
                <tr>
            <td class="TableBackground" style="width: 200px">
                Bus number:
            </td>
            <td class="TableBackground" style="width: 200pX">
                <asp:TextBox ID="TxtMACBusNumber" runat="server" Width="142px" ReadOnly="True" 
                    BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" align="center" colspan="2">
                <asp:ImageButton ID="BtnResolveMAC" runat="server" 
                    ImageUrl="~/Images/ButtonSearch.png" onclick="BtnResolveMAC_Click" />
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Content>