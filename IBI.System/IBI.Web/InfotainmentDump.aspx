﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InfotainmentDump.aspx.cs" Inherits="CMS_Aspx.InfotainmentDump" %>
<%@ Register src="CtrlInfotainmentDump.ascx" tagname="CtrlInfotainmentDump" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <script language="javascript" type="text/javascript">
     function openScreenshots(busnumber, imagename) {
         window.open("ScreenshotViewer.aspx?clienttype=VTC&busnumber=" + busnumber + "&imagename=" + imagename, "ScreenshotViewer", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=350,height=320");

         return true;
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <uc1:CtrlInfotainmentDump ID="CtrlInfotainmentDump1" runat="server" />
     <br />
</asp:Content>
