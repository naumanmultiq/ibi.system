﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace IBI.CMS
{
    public partial class BusStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            this.resetFields();

            int busNumber = 0;

            if (int.TryParse(this.txtBusNumber.Text, out busNumber))
            {
                this.cllBusNumber.InnerText = "Bus " + busNumber;
                this.checkVTC(busNumber);
                this.checkDCU(busNumber);
                this.checkHotspot(busNumber);
                this.checkInfotainment(busNumber);
            }
        }

        private void checkVTC(int busNumber)
        {
            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT [MACAddress], [LastPing] FROM [Clients] WHERE [BusNumber]='" + busNumber + "' AND [ClientType]='VTC'";
         
                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        String macAddress = dataReader.GetString(dataReader.GetOrdinal("MACAddress"));
                        DateTime lastPing = dataReader.GetDateTime(dataReader.GetOrdinal("LastPing"));

                        this.CllVTCMac.InnerText = macAddress;
                        this.ImgVTCStatus.ImageUrl = "Images/KnobValidGreen_16.png";
                        this.cllVTCTimestamp.InnerText = lastPing.ToString("dd-MM-yyyy HH:mm:ss");                                                
                    }
                }
            }
        }

        private void checkDCU(int busNumber)
        {
            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT [MACAddress], [LastPing] FROM [Clients] WHERE [BusNumber]='" + busNumber + "' AND [ClientType]='DCU'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        String macAddress = dataReader.GetString(dataReader.GetOrdinal("MACAddress"));
                        DateTime lastPing = dataReader.GetDateTime(dataReader.GetOrdinal("LastPing"));

                        this.CllDCUMac.InnerText = macAddress;
                        this.ImgDCUStatus.ImageUrl = "Images/KnobValidGreen_16.png";
                        this.cllDCUTimestamp.InnerText = lastPing.ToString("dd-MM-yyyy HH:mm:ss");
                    }
                }
            }
        }

        private void checkHotspot(int busNumber)
        {
            using (SqlConnection connection = AppSettings.GetHotspotDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT [LastPing] FROM [Mermaid.Hotspot.Server.Model.AccessPoint] WHERE [Name] like '%" + busNumber + "%'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        DateTime lastPing = (DateTime)dataReader.GetValue(0);

                        this.ImgHotspotStatus.ImageUrl = "Images/KnobValidGreen_16.png";
                        this.cllHotspotTimestamp.InnerText = lastPing.ToString("dd-MM-yyyy HH:mm:ss");
                    }
                }
            }
        }

        private void checkInfotainment(int busNumber)
        {
            String macAddress = this.getMacAddress(busNumber);

            if (!String.IsNullOrEmpty(macAddress))
            {
                using (SqlConnection connection = AppSettings.GetvTouchDatabaseConnection())
                {
                    connection.Open();

                    String script = "SELECT [LastOnline] FROM [vTouch.Server.Model.IPPlayer] WHERE [MACAddress] like '%" + macAddress + "%'";

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        SqlDataReader dataReader = command.ExecuteReader();

                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            DateTime lastOnline = (DateTime)dataReader.GetValue(0);

                            this.ImgInfotainmentStatus.ImageUrl = "Images/KnobValidGreen_16.png";
                            this.cllInfotainmentTimestamp.InnerText = lastOnline.ToString("dd-MM-yyyy HH:mm:ss");
                        }
                    }
                }
            }
        }

        private String getMacAddress(int busNumber)
        {
            String macAddress = "";

            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT [MacAddress] FROM [Clients] WHERE [BusNumber]='" + busNumber + "' AND [ClientType]='VTC'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        
                        macAddress = (String)dataReader.GetValue(0);

                        if (!String.IsNullOrEmpty(macAddress))
                            macAddress = macAddress.Replace(":", "");
                        else
                            macAddress = "";                        
                    }
                }
            }

            return macAddress;
        }

        private void resetFields()
        {
            this.cllBusNumber.InnerText = "No bus number entered";
            this.cllVTCTimestamp.InnerText = "";
            this.cllDCUTimestamp.InnerText = "";
            this.cllHotspotTimestamp.InnerText = "";
            this.cllInfotainmentTimestamp.InnerText = "";

            this.ImgVTCStatus.ImageUrl = "Images/KnobCancel_16.png";
            this.ImgDCUStatus.ImageUrl = "Images/KnobCancel_16.png";
            this.ImgHotspotStatus.ImageUrl = "Images/KnobCancel_16.png";
            this.ImgInfotainmentStatus.ImageUrl = "Images/KnobCancel_16.png";
        }

        protected void BtnResolveMAC_Click(object sender, ImageClickEventArgs e)
        {
            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT [BusNumber], [ClientType] FROM [Clients] WHERE [MacAddress] like '%" + this.TxtMACAddress.Text + "%'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    SqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        String busNumber = (String)dataReader.GetValue(0);
                        String clientType = (String)dataReader.GetValue(1);

                        this.TxtMACBusNumber.Text = busNumber;

                        if (!String.IsNullOrEmpty(clientType))
                            this.TxtMACBusNumber.Text += " - " + clientType;
                    }
                    else
                    {
                        this.TxtMACBusNumber.Text = "Not found..";
                    }
                }
            }
        }
    }
}