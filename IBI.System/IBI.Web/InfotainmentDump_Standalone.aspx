﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InfotainmentDump_Standalone.aspx.cs" Inherits="CMS_Aspx.InfotainmentDump_Standalone" %>

<%@ Register src="CtrlInfotainmentDump.ascx" tagname="CtrlInfotainmentDump" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
     <script language="javascript" type="text/javascript">
         function openScreenshots(busnumber, imagename) {
             window.open("ScreenshotViewer.aspx?clienttype=VTC&busnumber=" + busnumber + "&imagename=" + imagename, "ScreenshotViewer", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=350,height=320");

             return true;
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:CtrlInfotainmentDump ID="CtrlInfotainmentDump1" runat="server" />
    
    </div>
    </form>
</body>
</html>
