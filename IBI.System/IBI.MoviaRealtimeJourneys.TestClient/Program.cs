﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Compression;

namespace IBI.MoviaRealtimeJourneys.TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IBI.MoviaRealtimeJourneys.MoviaRealtimeJourneyController server = new MoviaRealtimeJourneyController();
                System.Console.WriteLine("Movia RealTime Journey Fetcher started");
                System.Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
