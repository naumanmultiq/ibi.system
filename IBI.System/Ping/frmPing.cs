﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ping
{
    public partial class frmPing : Form
    {
        public frmPing()
        {
            InitializeComponent();
        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            DoPing(busNo.Text, String.IsNullOrEmpty(clientTypes.SelectedText) ? "DCU" : clientTypes.SelectedText, pingData.Text);
        }


        public void DoPing(string busNo, string clientType, string pingData)
        {
            try
            {

                IBIService.IBIServiceClient client = new IBIService.IBIServiceClient();
                client.Ping(busNo, clientType, pingData);

                MessageBox.Show("Pinged successfully without errors");
            }
            catch (Exception ex)
            {
                errors.Text = ex.Message;
                errors.Text += ex.StackTrace;
                MessageBox.Show("Ping Failed");
            }

        }


        private void frmPing_Load(object sender, EventArgs e)
        {

        }
    }
}
