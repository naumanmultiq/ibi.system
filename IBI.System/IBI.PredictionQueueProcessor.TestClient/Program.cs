﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.PredictionQueueProcessor.TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IBI.PredictionQueueProcessor.PredictionQueueController server = new PredictionQueueController();
                System.Console.WriteLine("IBI Journey Prediction Queue Controller Started");
                System.Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
