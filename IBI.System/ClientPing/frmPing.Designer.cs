﻿namespace ClientPing
{
    partial class frmPing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPing = new System.Windows.Forms.Button();
            this.busNo = new System.Windows.Forms.TextBox();
            this.pingData = new System.Windows.Forms.TextBox();
            this.clientTypes = new System.Windows.Forms.ComboBox();
            this.errors = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPing
            // 
            this.btnPing.Location = new System.Drawing.Point(16, 214);
            this.btnPing.Name = "btnPing";
            this.btnPing.Size = new System.Drawing.Size(75, 23);
            this.btnPing.TabIndex = 0;
            this.btnPing.Text = "Ping";
            this.btnPing.UseVisualStyleBackColor = true;
            this.btnPing.Click += new System.EventHandler(this.btnPing_Click);
            // 
            // busNo
            // 
            this.busNo.Location = new System.Drawing.Point(16, 40);
            this.busNo.Name = "busNo";
            this.busNo.Size = new System.Drawing.Size(100, 20);
            this.busNo.TabIndex = 1;
            this.busNo.Text = "9999";
            // 
            // pingData
            // 
            this.pingData.Location = new System.Drawing.Point(16, 136);
            this.pingData.Multiline = true;
            this.pingData.Name = "pingData";
            this.pingData.Size = new System.Drawing.Size(344, 62);
            this.pingData.TabIndex = 3;
            this.pingData.Text = "customerid=2140;mac=123123123;journeynumber=1;simid=123;lat=55.90909;lon=12.09099" +
    "";
            // 
            // clientTypes
            // 
            this.clientTypes.FormattingEnabled = true;
            this.clientTypes.Items.AddRange(new object[] {
            "DCU",
            "VTC"});
            this.clientTypes.Location = new System.Drawing.Point(16, 84);
            this.clientTypes.Name = "clientTypes";
            this.clientTypes.Size = new System.Drawing.Size(121, 21);
            this.clientTypes.TabIndex = 4;
            // 
            // errors
            // 
            this.errors.Location = new System.Drawing.Point(59, 321);
            this.errors.Multiline = true;
            this.errors.Name = "errors";
            this.errors.Size = new System.Drawing.Size(344, 154);
            this.errors.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 305);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Error (if any)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.busNo);
            this.groupBox1.Controls.Add(this.btnPing);
            this.groupBox1.Controls.Add(this.pingData);
            this.groupBox1.Controls.Add(this.clientTypes);
            this.groupBox1.Location = new System.Drawing.Point(43, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(377, 256);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Bus Number"; 
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Client Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Ping Data";
            // 
            // frmPing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 499);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.errors);
            this.Name = "frmPing";
            this.Text = "frmPing";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPing;
        private System.Windows.Forms.TextBox busNo;
        private System.Windows.Forms.TextBox pingData;
        private System.Windows.Forms.ComboBox clientTypes;
        private System.Windows.Forms.TextBox errors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}