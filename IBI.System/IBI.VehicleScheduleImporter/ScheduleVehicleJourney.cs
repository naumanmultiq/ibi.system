﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.VehicleScheduleImporter
{
    class ScheduleVehicleJourney
    {
        public string TripType
        {
            get;
            set;
        }
        public string DC
        {
            get;
            set;
        }
        public string VehicleScheduleExternalRef
        {
            get;
            set;
        }
        public string TripId
        {
            get;
            set;
        }
        public string FromName
        {
            get;
            set;
        }
        public string Line
        {
            get;   
            set;
        }
        public string Destination
        {
            get;
            set;
        }
        public string ViaName
        {
            get;
            set;
        }
        public DateTime PlannedStartTime
        {
            get;
            set;
        }
        public DateTime PlannedEndTime
        {
            get;
            set;
        }
        public List<StopInfo> StopsInfo
        {
            get;
            set;
        }
    }
}
