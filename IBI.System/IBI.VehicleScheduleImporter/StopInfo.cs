﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.VehicleScheduleImporter
{
    class StopInfo
    {
        public string StopId
        {
            get;  
            set;
        }
        public string LongName
        {
            get;
            set;
        }
        public string ShortName
        {
            get;
            set;
        }
        public string Latitude
        {
            get;
            set;
        }
        public string Longitude
        {
            get;
            set;
        }
        public bool HasAnnouncement
        {
            get;
            set;
        }
        public int AnnouncementRadius
        {
            get;
            set;
        }
        public string AnnouncementText
        {
            get;
            set;
        }

        public DateTime PlannedDeparturetime
        {
            get;
            set;
        }
        public int Zone
        {
            get;
            set;
        }

        public StopInfo()
        {

        }
        public StopInfo(StopInfo copyObject)
        {
            this.StopId = copyObject.StopId;
            this.LongName = copyObject.LongName;
            this.ShortName = copyObject.ShortName;
            this.Latitude = copyObject.Latitude;
            this.Longitude = copyObject.Longitude;
            this.HasAnnouncement = copyObject.HasAnnouncement;
            this.AnnouncementRadius = copyObject.AnnouncementRadius;
            this.AnnouncementText = copyObject.AnnouncementText;
            this.Zone = copyObject.Zone;
        }
    }
}
