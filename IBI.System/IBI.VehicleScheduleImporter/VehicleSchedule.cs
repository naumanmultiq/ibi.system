﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.VehicleScheduleImporter
{
    class VehicleSchedule
    {
        public string VehicleScheduleExternalRef
        {
            get;
            set;
        }
        public List<ScheduleVehicleJourney> Journeys
        {
            get;
            set;
        }
    }
}
