﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.AdibusTimeTables.TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IBI.AdiBusTimetables.AdibusController server = new AdiBusTimetables.AdibusController();
                System.Console.WriteLine("IBI AdiBus Timetables started");
                System.Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
