﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace IBI.SyncInService
{
    public class AppSettings
    {
        public static SqlConnection GetIBIDatabaseConnection()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["IBIDatabaseConnection"].ConnectionString;

            return new SqlConnection(connectionString);
        }

        public static String GetResourceDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["ResourceDirectory"];

            return resourceDirectory;
        }


        public static String GetSQLCEDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["SQLCEDirectory"];

            return resourceDirectory;
        }


      

        public static int SQLBulkCopyTimeout()
        {
            int retVal = int.Parse(ConfigurationManager.AppSettings["SQLBulkCopyTimeout"]);

            return retVal;

        }

        public static double ScreenshotCleanUpInterval()
        {
            double retVal = 14;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ScreenshotCleanUpAge"]))
            {
                retVal = double.Parse(ConfigurationManager.AppSettings["ScreenshotCleanUpAge"]);
            }

            return retVal;

        }

        public static bool InServiceSyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["InServiceSync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["InServiceSync"]);
            }

            return retVal;
        }

        public static bool JourneySyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["JourneySync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["JourneySync"]);
            }

            return retVal;
        }

        public static bool AudioSyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AudioSync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["AudioSync"]);
            }

            return retVal;
        }

        public static bool NovoTicketSynEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NovoTicketSynchronization"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["NovoTicketSynchronization"]);
            }

            return retVal;
        }

        public static bool NavisionTicketSynEnabled()
        {
            return (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NavisionTicketSynchronization"])) ? bool.Parse(ConfigurationManager.AppSettings["NavisionTicketSynchronization"]) : false;
        }

        public static int NavisionSupportSyncTimer()
        {
            return (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NavisionSupportSyncTimer"])) ? Int32.Parse(ConfigurationManager.AppSettings["NavisionSupportSyncTimer"]) : 15;
        }

        public static int NovoSupportSyncTimer()
        {
            return (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NovoSupportSyncTimer"])) ? Int32.Parse(ConfigurationManager.AppSettings["NovoSupportSyncTimer"]) : 15;
        }


        public static bool ScreenShotCleanupEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ScreenShotCleanup"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["ScreenShotCleanup"]);
            }

            return retVal;
        }

        public static bool DumpStatusEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DumpStatus"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["DumpStatus"]);
            }

            return retVal;
        }

        public static bool BusesSyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["BusesSync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["BusesSync"]);
            }

            return retVal;
        }

        public static bool LogProcessingEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogProcessing"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["LogProcessing"]);
            }

            return retVal;
        }

        public static bool LogBulkCopyingEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogBulkCopying"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["LogBulkCopying"]);
            }

            return retVal;
        }


        public static bool JourneyCompletionEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["JourneyCompletionTimer"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["JourneyCompletionTimer"]);
            }

            return retVal;
        }
         

        public static int JourneyStatusUpdateTimerInterval()
        {
            int retVal = 10;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["JourneyStatusUpdateTimerInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["JourneyStatusUpdateTimerInterval"]);
            }

            return retVal;
        }

        public static bool LogAllMessages()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
            }

            return retVal;
        }

        public static int SyncInServiceBusInterval()
        {
            int retVal = 5000;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SyncInServiceBusInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["SyncInServiceBusInterval"]);
            }

            return retVal;
        }

       

        public static int LogProcessingFileInterval()
        {
            int retVal = 100;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogProcessingFileInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["LogProcessingFileInterval"]);
            }

            return retVal;
        }

        public static int LogProcessingLineInterval()
        {
            int retVal = 100;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogProcessingLineInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["LogProcessingLineInterval"]);
            }

            return retVal;
        }

        public static int BulkCopyFileInterval()
        {
            int retVal = 100;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["BulkCopyFileInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["BulkCopyFileInterval"]);
            }

            return retVal;
        }


        #region HaCon

        public static bool HaConJourneyImporterEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConJourneyImporter"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["HaConJourneyImporter"]);
            }

            return retVal;
        }

        public static int HaConJourneyTimerInterval()
        {
            int retVal = 3600;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaCon_JourneyTimerInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["HaCon_JourneyTimerInterval"]);
            }

            return retVal;
        }

        public static int HaConFutureMinutes()
        {
            int retVal = 60;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaCon_FutureMinutes"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["HaCon_FutureMinutes"]);
            }

            return retVal;
        }

        public static int HaConSubscriptionValidityMinutes()
        {
            int retVal = 60;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaCon_SubscriptionValidityMinutes"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["HaCon_SubscriptionValidityMinutes"]);
            }

            return retVal;
        }

         public static bool HaConSaveDataInFiles()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaCon_SaveDataInFiles"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["HaCon_SaveDataInFiles"]);
            }

            return retVal;
        }

         public static bool HaConSaveDataInDB()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaCon_SaveDataInDB"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["HaCon_SaveDataInDB"]);
            }

            return retVal;
        } 

        #endregion

       

    }
}