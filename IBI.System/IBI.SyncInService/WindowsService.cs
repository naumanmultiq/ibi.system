﻿using System.ServiceProcess;

namespace IBI.SyncInService
{
    public partial class WindowsService : ServiceBase
    {
        private Service serverInstance;

        public WindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.serverInstance = new Service();
        }

        protected override void OnStop()
        {
            this.serverInstance.Dispose();
        }
    }
}
