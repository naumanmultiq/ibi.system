﻿<?xml version="1.0" encoding="utf-8"?>
<!-- DWXMLSource="source.xml" -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <CorrespondingTraficData>
      <FetchTime>
        <xsl:value-of select="CorrespondingTraficData/FetchTime" />
      </FetchTime>
      <FetchFromTime>
        <xsl:value-of select="CorrespondingTraficData/FetchFromTime" />      
      </FetchFromTime>
      <StopNumber>
        <xsl:value-of select="CorrespondingTraficData/StopNumber" />
      </StopNumber>
      <StopName>
        <xsl:value-of select="CorrespondingTraficData/StopName" />
      </StopName>
      <Checkpoint>
        <xsl:value-of select="CorrespondingTraficData/Checkpoint" />
      </Checkpoint>
      <PlaybackInformation>
        <TemplateType>
          <xsl:value-of select="CorrespondingTraficData/PlaybackInformation/TemplateType" />
        </TemplateType>
        <NumberOfBuses><xsl:value-of select="CorrespondingTraficData/PlaybackInformation/NumberOfBuses" /></NumberOfBuses>
        <NumberOfTrains><xsl:value-of select="CorrespondingTraficData/PlaybackInformation/NumberOfTrains" /></NumberOfTrains>       
      </PlaybackInformation>
      <Departures>
        <Buses>
          <xsl:for-each select="CorrespondingTraficData/Departures/Buses/Bus">
          <Bus>
            <Line>
              <xsl:value-of select="Line" />
            </Line>
            <Type>
              <xsl:value-of select="Type" />
            </Type>
            <Destination>
              <xsl:value-of select="Destination" />
            </Destination>

            <xsl:for-each select="Departure">
              <Departure>
                <PlanedDepartureTime>
                  <xsl:value-of select="PlanedDepartureTime" />
                </PlanedDepartureTime>
                <EstimatedDepartureTime>
                  <xsl:value-of select="EstimatedDepartureTime" />
                </EstimatedDepartureTime>    
              </Departure>
            </xsl:for-each>
            
          </Bus>
           </xsl:for-each>
        </Buses>
        <Trains>
          <xsl:for-each select="CorrespondingTraficData/Departures/Trains/Train">
          <Train>
            <Line>
              <xsl:value-of select="Line" />
            </Line>
            <Type>
              <xsl:value-of select="Type" />
            </Type>
            <Destination>
              <xsl:value-of select="Destination" />
            </Destination>

            <xsl:for-each select="Departure">
              <Departure>
                <PlanedDepartureTime>
                  <xsl:value-of select="PlanedDepartureTime" />
                </PlanedDepartureTime>
                <EstimatedDepartureTime>
                  <xsl:value-of select="EstimatedDepartureTime" />
                </EstimatedDepartureTime>
                <PlanedDepartureTrack>
                  <xsl:value-of select="PlanedDepartureTrack" />
                </PlanedDepartureTrack>
                <EstimatedDepartureTrack>
                  <xsl:value-of select="EstimatedDepartureTrack" />
                </EstimatedDepartureTrack>
              </Departure>
            </xsl:for-each>
          </Train>
          </xsl:for-each>
        </Trains>
      </Departures>
    </CorrespondingTraficData>     
  </xsl:template>
</xsl:stylesheet>