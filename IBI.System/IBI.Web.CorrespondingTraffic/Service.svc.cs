﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Shared.Models.CorrespondingTraffic;
using System.ServiceModel.Activation;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Xsl;

namespace IBI.Web.CorrespondingTraffic
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
               ConcurrencyMode = ConcurrencyMode.Single, IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service : IService
    {

        public string GetCorrespondingTrafficOLD(int customerId, string busNumber, string stopNumber, string line, string dateTime)
        {
            dateTime = dateTime.Substring(0, dateTime.IndexOf('/'));
            DateTime serviceDateTime = DateTime.Now;

            if (!String.IsNullOrEmpty(dateTime))            {
                serviceDateTime = DateTime.ParseExact(dateTime, "yyyy-MM-ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture);
            }

            IBI.Web.CorrespondingTraffic.OldDepartureBoard.SyncDepartureBoard_Rajseplanen dBoard = new OldDepartureBoard.SyncDepartureBoard_Rajseplanen();
            dBoard.Url = ConfigurationManager.AppSettings["CTraffic_Url"].ToString();

            dBoard.StopInfoUrl = ConfigurationManager.AppSettings["CTraffic_StopInfoUrl"].ToString();
            dBoard.Username = ConfigurationManager.AppSettings["CTraffic_Username"].ToString();
            dBoard.Password = ConfigurationManager.AppSettings["CTraffic_Password"].ToString();

            string strTraffic = dBoard.GetDepartureBoard(busNumber, stopNumber, line, int.Parse(ConfigurationManager.AppSettings["CTraffic_ShowJourneysOld"].ToString()) , serviceDateTime);

            //return strTraffic;

            string result = "";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strTraffic);

            return doc.OuterXml;

            XslCompiledTransform xslTransform = new XslCompiledTransform();
            StringWriter writer = new StringWriter();
            //xslTransform.Load(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/OldCorrespondingTraffic.xslt"));
            xslTransform.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin\\OldCorrespondingTraffic.xslt"));

            xslTransform.Transform(doc.CreateNavigator(), null, writer);

            result = writer.ToString();

            return result;

        }

        public string GetCorrespondingTraffic(int customerId, string busNumber, string stopNumber, string line, string dateTime)
        {
            return CorrespondingTrafficController.GetCorrespondingTraffic(customerId, busNumber, stopNumber, line, dateTime);
        }
             
        public string GetClientConfigurations(int customerId)
        {
            string configs = "";

            configs = CorrespondingTrafficController.GetClientConfigurations(customerId);

            return configs;
        }

        public string GetBusesOnSchedule(int scheduleId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["IBIDatabaseConnectionLIVE"].ToString()))
            {
                SqlCommand cmd = new SqlCommand("A_GetBusesOfSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("ScheduleId", scheduleId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                da.Fill(ds);
                
                StringBuilder sb = new StringBuilder();

                //sb.AppendLine("BusNumber \t Lat \t Lon");
                sb.AppendLine("<Buses>");
                foreach(DataRow dr in ds.Tables[0].Rows)
                {
                    sb.AppendLine(String.Format("<Bus BusNumber='{0}' Lat='{1}' Lon='{2}' />", dr["BusNumber"], dr["Lat"], dr["Lon"]));
                }
                sb.AppendLine("</Buses>");
                return sb.ToString();
            }

        }
    }
}
