﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" />   

  <xsl:param name="FetchTime"></xsl:param>
  <xsl:param name="FetchFromTime"></xsl:param>
  <xsl:param name="StopNumber"></xsl:param>
  <xsl:param name="TransformedStopNumber"></xsl:param>
  <xsl:param name="StopName"></xsl:param>
  <xsl:param name="CheckPoint"></xsl:param>
  <xsl:param name="LastChanceToPlay"></xsl:param>
  <xsl:param name="ExcludeLowPriorityMedia"></xsl:param> 
 
  <!--<xsl:key name="groups" match="DepartureBoard/Departure[@type='BUS' or @type='EXB' or @type='Bus' or @type='TB' or @type='EXB' or @type='NB' or @type='TB']" use="ss"/>-->
    <xsl:key name="GroupData" match="Departure" use="concat(substring-after(@name, ' '), '|', @direction)" />
    <xsl:key name="STrainData" match="Departure" use="concat(@type, '|', @direction)" />
    <xsl:key name="TrainData" match="Departure" use="concat(@type, '|', @direction)" />
    <xsl:key name="MetroData" match="Departure" use="concat(@type, '|', @direction)" />
    <xsl:key name="FerryData" match="Departure" use="concat(@type, '|', @direction)" />
  
  
  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  
  <xsl:template match="/">
    <CorrespondingTraficData>
      <FetchTime>
          <xsl:value-of select="$FetchTime" />
      </FetchTime>
      <FetchFromTime>
          <xsl:value-of select="$FetchFromTime" />
      </FetchFromTime>
      <StopNumber>
        <xsl:value-of select="$StopNumber" />
      </StopNumber>
        <TransformedStopNumber>
        <xsl:value-of select="$TransformedStopNumber" />
      </TransformedStopNumber>
      <StopName>
        <xsl:value-of select="$StopName" />
      </StopName>
      <Checkpoint>
        <xsl:value-of select="$CheckPoint" />
      </Checkpoint>
      <LastChanceToPlay>
        <xsl:value-of select="$LastChanceToPlay" />
      </LastChanceToPlay>
      <ExcludeLowPrioirtyMedia>
        <xsl:value-of select="$ExcludeLowPriorityMedia" />
      </ExcludeLowPrioirtyMedia> 
      <Information>
        <xsl:variable name="totalBuses" select="DepartureBoard/Departure[@type='BUS' or @type='EXB' or @type='Bus' or @type='TB' or @type='EXB' or @type='NB' or @type='TB']"/>
        <xsl:variable name="totalTrains" select="DepartureBoard/Departure[@type='REG'  or @type='TOG'  or @type='IC'  or @type='LYN']"/>
        <xsl:variable name="totalSTrains" select="DepartureBoard/Departure[@type='S']"/>
        <xsl:variable name="totalMetro" select="DepartureBoard/Departure[@type='M']"/>
        <xsl:variable name="totalFerrys" select="DepartureBoard/Departure[@type='F']"/>
        <xsl:variable name="totalPlanes" select="DepartureBoard/Departure[@type='Plane']"/>


        <NumberOf TransportType="bus"><xsl:value-of select="count($totalBuses)" /></NumberOf>
        <NumberOf TransportType="train"><xsl:value-of select="count($totalTrains)" /></NumberOf>
        <NumberOf TransportType="s-train"><xsl:value-of select="count($totalSTrains)" /></NumberOf>
        <NumberOf TransportType="metro">
          <xsl:value-of select="count($totalMetro)" />
        </NumberOf>
        <NumberOf TransportType="ferry">
          <xsl:value-of select="count($totalFerrys)" />
        </NumberOf>
        <!--<NumberOf TransportType="planes">
          <xsl:value-of select="count($totalPlanes)" />
        </NumberOf>-->      
      </Information>
      <Departures>
        
        <TransportType TransportType='bus'>
           
            <xsl:for-each select="DepartureBoard/Departure[(@type='BUS' or @type='EXB' or @type='Bus' or @type='TB' or @type='EXB' or @type='NB' or @type='TB') and count(. | key('GroupData', concat(substring-after(@name, ' '), '|', @direction))[1]) = 1  ]">
                <xsl:sort select="substring-after(@name, ' ')" />
 
                 <!--<test>
                  <xsl:value-of select="substring-before(@direction, ' (')"/>
                </test>--> 
                
                
              <!--calculate Values-->
              <xsl:variable name="line" select="substring-after(@name, ' ')" />

              <xsl:variable name="destination">
                <xsl:choose>
                  <xsl:when test="string-length(substring-before(@direction, ' ('))>1">
                    <xsl:value-of select="substring-before(@direction, ' (')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@direction"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
                  
              <xsl:variable name="subType"> 
                    <xsl:choose>
                      
                        <xsl:when test="(@type='BUS' or @type='Bus') and substring(@name, string-length(@name)) != 'A'">
                          <xsl:value-of  select="'Bus'" />                           
                        </xsl:when>
                        <xsl:when test="@type='BUS' and substring(@name, string-length(@name)) = 'A'">
                          <xsl:value-of  select="'A-Bus'" />
                        </xsl:when>
                        <xsl:when test="@type='TB' and substring(@name, 1, 8) = 'Havnebus'">
                          <xsl:value-of  select="'H-Bus'" />                           
                        </xsl:when>                                     
                      <xsl:when test="@type='TB' and starts-with(@name, 'Bus')">
                          <xsl:value-of  select="'T-Bus'" />                           
                        </xsl:when>
                      <xsl:when test="@type='EXB' and substring(@name, string-length(@name)) != 'S'">
                          <xsl:value-of  select="'EX-Bus'" />                           
                        </xsl:when>                                     
                      <xsl:when test="@type='EXB' and substring(@name, string-length(@name)) = 'S'">
                          <xsl:value-of  select="'S-Bus'" />                           
                        </xsl:when>
                      <xsl:when test="@type='NB'">
                          <xsl:value-of  select="'NB'" />
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="'Bus'"/>
                        </xsl:otherwise>
                      </xsl:choose> 
              </xsl:variable>
               
                
                <xsl:element name="Line"> 
                    <xsl:attribute name="line">
                      <xsl:value-of select="$line"/>
                    </xsl:attribute>
                  <xsl:attribute name="destination">
                      <xsl:value-of select="$destination"/>
                    </xsl:attribute>
                  <xsl:attribute name="subType">                      
                      <xsl:value-of select="$subType"/>
                    </xsl:attribute>                  
                  <xsl:attribute name="numberofdepartures">
                      <xsl:value-of select="count(key('GroupData', concat(substring-after(@name, ' '), '|', @direction)))"/>
                    </xsl:attribute>
                  
                   
                     <xsl:for-each select="key('GroupData', concat(substring-after(@name, ' '), '|', @direction))">
                        <xsl:sort select="@type" />
                       
                       <xsl:variable name="myVar">
                          <xsl:call-template name="string-replace-all">
                            <xsl:with-param name="text" select="@date" />
                            <xsl:with-param name="replace" select="'.'" />
                            <xsl:with-param name="by" select="'/'" />
                          </xsl:call-template>
                        </xsl:variable>
                       <xsl:variable name="plannedDateTime" select="concat(@date, 'T', @time)" />   
                         
                       <Departure>
						            <PlanedDepartureTime><xsl:value-of select="$plannedDateTime" /></PlanedDepartureTime>
                          <xsl:choose>
                           <xsl:when test="@rtDate != ''">
                             <xsl:variable name="estimatedDateTime" select="concat(@rtDate, 'T', @rtTime)" />   
                             <EstimatedDepartureTime><xsl:value-of select="$estimatedDateTime" /></EstimatedDepartureTime>
                           </xsl:when>
                          </xsl:choose>
					            </Departure>                        
                    </xsl:for-each>
                  
                  
                  
                </xsl:element>
            </xsl:for-each>

        </TransportType>

        <TransportType TransportType='s-train'>
           
            <xsl:for-each select="DepartureBoard/Departure[(@type='S') and count(. | key('STrainData', concat(@type, '|', @direction))[1]) = 1  ]">
                <xsl:sort select="@name" />
 
                 <!--<test>
                  <xsl:value-of select="@name"/>
                </test>--> 
              
              <!--calculate Values-->
              <xsl:variable name="line" select="@name" />  
              
              <xsl:variable name="destination">
                <xsl:choose>
                  <xsl:when test="string-length(substring-before(@direction, ' ('))>1">
                    <xsl:value-of select="substring-before(@direction, ' (')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@direction"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
               
              <xsl:variable name="subType" select="'S-Train'" />  
                           
                
                <xsl:element name="Line">
                    <!--perserve the @htn for the new element -->
                    <xsl:attribute name="line">
                      <xsl:value-of select="$line"/>
                    </xsl:attribute>
                  <xsl:attribute name="destination">
                      <xsl:value-of select="$destination"/>
                    </xsl:attribute>
                  <xsl:attribute name="subType">                      
                      <xsl:value-of select="$subType"/>
                    </xsl:attribute>                  
                  <xsl:attribute name="numberofdepartures">
                      <xsl:value-of select="count(key('STrainData', concat(@type, '|', @direction)))"/>
                    </xsl:attribute>
                  
                
                      <xsl:for-each select="key('STrainData', concat(@type, '|', @direction))">
                        <xsl:sort select="@type" />
                       
                       <xsl:variable name="plannedDateTime" select="concat(@date, 'T', @time)" />   
                         
                         <Departure>
						            <PlanedDepartureTime><xsl:value-of select="$plannedDateTime" /></PlanedDepartureTime> 
                          <xsl:choose>
                           <xsl:when test="@rtDate != ''">
                             <xsl:variable name="estimatedDateTime" select="concat(@rtDate, 'T', @rtTime)" />   
                             <EstimatedDepartureTime><xsl:value-of select="$estimatedDateTime" /></EstimatedDepartureTime>                           
                           </xsl:when>
                          <xsl:when test="@track != ''">
                             <xsl:variable name="plannedDepartureTrackDateTime" select="@track" />   
                             <PlanedDepartureTrack><xsl:value-of select="$plannedDepartureTrackDateTime" /></PlanedDepartureTrack>
                           </xsl:when>
                           <xsl:when test="@rtTrack != ''">
                             <xsl:variable name="estimatedDepartureTrackDateTime" select="@rtTrack" />   
                             <EstimatedDepartureTrack><xsl:value-of select="$estimatedDepartureTrackDateTime" /></EstimatedDepartureTrack>
                           </xsl:when>
                          </xsl:choose>
					            </Departure>                        
                      
                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each>

        </TransportType>
          
      <TransportType TransportType='train'>
           
            <xsl:for-each select="DepartureBoard/Departure[(@type='REG'  or @type='TOG'  or @type='IC'  or @type='LYN') and count(. | key('TrainData',  concat(@type, '|', @direction))[1]) = 1]">
                <xsl:sort select="substring-after(@name, ' ')" />
 
               <!--<test>
                  <xsl:value-of select="concat( @type, '|', @direction)"/>
                </test>-->  
              
              
              <!--calculate Values-->
              
              <xsl:variable name="line">
                <xsl:choose>                      
                        <xsl:when test="@type='REG'">
                          <xsl:value-of  select="'Re'" />                           
                        </xsl:when> 
                        <xsl:when test="@type='TOG' or @type='IC' or @type='LYN'">
                          <xsl:value-of  select="substring-before(@name, ' ')" />                           
                        </xsl:when> 
                      </xsl:choose> 
              </xsl:variable>
              
               <xsl:variable name="destination">
                <xsl:choose>
                  <xsl:when test="string-length(substring-before(@direction, ' ('))>1">
                    <xsl:value-of select="substring-before(@direction, ' (')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@direction"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
                
              <xsl:variable name="subType"> 
                    <xsl:choose>                      
                        <xsl:when test="@type='REG'">
                          <xsl:value-of  select="'REG'" />                           
                        </xsl:when> 
                        <xsl:when test="@type='TOG'">
                          <xsl:value-of  select="'IRØR'" />                           
                        </xsl:when> 
                        <xsl:when test="@type='IC' or @type='LYN'">
                          <xsl:value-of select="substring-before(@name, ' ')"/>
                        </xsl:when>
                      </xsl:choose> 
              </xsl:variable> 
 
              
                <xsl:element name="Line"> 
                    <xsl:attribute name="line">
                      <xsl:value-of select="$line"/>
                    </xsl:attribute>
                  <xsl:attribute name="destination">
                      <xsl:value-of select="$destination"/>
                    </xsl:attribute>
                  <xsl:attribute name="subType">                      
                      <xsl:value-of select="$subType"/>
                    </xsl:attribute>                  
                  <xsl:attribute name="numberofdepartures">
                      <xsl:value-of select="count(key('TrainData', concat(@type, '|', @direction)))"/>
                    </xsl:attribute>
                   
                     <xsl:for-each select="key('TrainData', concat(@type, '|', @direction))">
                        <xsl:sort select="@type" />
                       
                       <xsl:variable name="plannedDateTime" select="concat(@date, 'T', @time)" />   
                         
                       <Departure>
						            <PlanedDepartureTime><xsl:value-of select="$plannedDateTime" /></PlanedDepartureTime> 
                          <xsl:choose>
                           <xsl:when test="@rtDate != ''">
                             <xsl:variable name="estimatedDateTime" select="concat(@rtDate, 'T', @rtTime)" />   
                             <EstimatedDepartureTime><xsl:value-of select="$estimatedDateTime" /></EstimatedDepartureTime>                           
                           </xsl:when>
                          <xsl:when test="@track != ''">
                             <xsl:variable name="plannedDepartureTrackDateTime" select="@track" />   
                             <PlanedDepartureTrack><xsl:value-of select="$plannedDepartureTrackDateTime" /></PlanedDepartureTrack>
                           </xsl:when>
                           <xsl:when test="@rtTrack != ''">
                             <xsl:variable name="estimatedDepartureTrackDateTime" select="@rtTrack" />   
                             <EstimatedDepartureTrack><xsl:value-of select="$estimatedDepartureTrackDateTime" /></EstimatedDepartureTrack>
                           </xsl:when>
                          </xsl:choose>
					            </Departure>                        
                    </xsl:for-each>
                  
                  
                  
                </xsl:element>
            </xsl:for-each>

        </TransportType>
      
     <TransportType TransportType='metro'>
           
            <xsl:for-each select="DepartureBoard/Departure[(@type='M') and count(. | key('MetroData', concat(@type, '|', @direction))[1]) = 1  ]">
                <xsl:sort select="substring-after(@name, ' ')" />
 
                 <!--<test>
                  <xsl:value-of select="substring-after('khurram-imtiaz', ' ')"/>
                </test>-->  
              
              <!--calculate Values-->
              <xsl:variable name="line" select="substring-after(@name, ' ')" />
              
               <xsl:variable name="destination">
                <xsl:choose>
                  <xsl:when test="string-length(substring-before(@direction, ' ('))>1">
                    <xsl:value-of select="substring-before(@direction, ' (')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@direction"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
                
              <xsl:variable name="subType" select="'Metro'" />
              
                
                <xsl:element name="Line">
                    <!--perserve the @htn for the new element -->
                    <xsl:attribute name="line">
                      <xsl:value-of select="$line"/>
                    </xsl:attribute>
                  <xsl:attribute name="destination">
                      <xsl:value-of select="$destination"/>
                    </xsl:attribute>
                  <xsl:attribute name="subType">                      
                      <xsl:value-of select="$subType"/>
                    </xsl:attribute>                  
                  <xsl:attribute name="numberofdepartures">
                      <xsl:value-of select="count(key('MetroData', concat(@type, '|', @direction)))"/>
                    </xsl:attribute>
                   
                     <xsl:for-each select="key('MetroData', concat(@type, '|', @direction))">
                        <xsl:sort select="@type" />
                       
                       <xsl:variable name="plannedDateTime" select="concat(@date, 'T', @time)" />   
                         
                         <Departure>
						            <PlanedDepartureTime><xsl:value-of select="$plannedDateTime" /></PlanedDepartureTime> 
                          <xsl:choose>
                           <xsl:when test="@rtDate != ''">
                             <xsl:variable name="estimatedDateTime" select="concat(@rtDate, 'T', @rtTime)" />   
                             <EstimatedDepartureTime><xsl:value-of select="$estimatedDateTime" /></EstimatedDepartureTime>                           
                           </xsl:when>
                          <xsl:when test="@track != ''">
                             <xsl:variable name="plannedDepartureTrackDateTime" select="@track" />   
                             <PlanedDepartureTrack><xsl:value-of select="$plannedDepartureTrackDateTime" /></PlanedDepartureTrack>
                           </xsl:when>
                           <xsl:when test="@rtTrack != ''">
                             <xsl:variable name="estimatedDepartureTrackDateTime" select="@rtTrack" />   
                             <EstimatedDepartureTrack><xsl:value-of select="$estimatedDepartureTrackDateTime" /></EstimatedDepartureTrack>
                           </xsl:when>
                          </xsl:choose>
					            </Departure>                               
                    </xsl:for-each>
                   
                </xsl:element>
            </xsl:for-each>

        </TransportType>
      
    <TransportType TransportType='ferry'>
           
            <xsl:for-each select="DepartureBoard/Departure[(@type='F') and count(. | key('FerryData', concat(@type, '|', @direction))[1]) = 1  ]">
                <xsl:sort select="substring-after(@name, ' ')" />
 
                <!--<test>
                  <xsl:value-of select=" substring(@name, string-length(@name))"/>
                </test>--> 
              
              <!--calculate Values-->
              <xsl:variable name="line" select="@name" />
              
               <xsl:variable name="destination">
                <xsl:choose>
                  <xsl:when test="string-length(substring-before(@direction, ' ('))>1">
                    <xsl:value-of select="substring-before(@direction, ' (')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@direction"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
                
              <xsl:variable name="subType" select="'F'" />
              
                
                <xsl:element name="Line">
                    <!--perserve the @htn for the new element -->
                    <xsl:attribute name="line">
                      <xsl:value-of select="$line"/>
                    </xsl:attribute>
                  <xsl:attribute name="destination">
                      <xsl:value-of select="$destination"/>
                    </xsl:attribute>
                  <xsl:attribute name="subType">                      
                      <xsl:value-of select="$subType"/>
                    </xsl:attribute>                  
                  <xsl:attribute name="numberofdepartures">
                      <xsl:value-of select="count(key('FerryData', concat(@type, '|', @direction)))"/>
                    </xsl:attribute>
                   
                     <xsl:for-each select="key('FerryData', concat(@type, '|', @direction))">
                        <xsl:sort select="@type" />
                       
                       <xsl:variable name="plannedDateTime" select="concat(@date, 'T', @time)" />   
                         
                       <Departure>
						            <PlanedDepartureTime><xsl:value-of select="$plannedDateTime" /></PlanedDepartureTime>
                           <xsl:choose>
                           <xsl:when test="@rtDate != ''">
                             <xsl:variable name="estimatedDateTime" select="concat(@rtDate, 'T', @rtTime)" />   
                             <EstimatedDepartureTime><xsl:value-of select="$estimatedDateTime" /></EstimatedDepartureTime>
                           </xsl:when>
                          </xsl:choose>
					            </Departure>                        
                    </xsl:for-each>
                   
                </xsl:element>
            </xsl:for-each>

        </TransportType>
      </Departures>
    </CorrespondingTraficData>     
  </xsl:template>
</xsl:stylesheet>
 