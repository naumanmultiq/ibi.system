﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IBI.Web.CorrespondingTraffic
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface IService
    { 

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        string GetCorrespondingTraffic(int customerId, string busNumber, string stopNumber, string line, string dateTime);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        string GetCorrespondingTrafficOLD(int customerId, string busNumber, string stopNumber, string line, string dateTime);

        
    //[OperationContract]
    //    [WebGet(
    //        BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
    //    List<IBI.Shared.Models.CorrespondingTraffic.ClientConfiguration> GetClientConfigurations(int customerId);
    //} 


    [OperationContract]
    [AspNetCacheProfile("ClientConfigurationCache")]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        string GetClientConfigurations(int customerId); 
        
        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        string GetBusesOnSchedule(int scheduleId);
    
    } 

     

    
}
