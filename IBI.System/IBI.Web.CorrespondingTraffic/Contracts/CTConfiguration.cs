﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.CorrespondingTraffic.Contracts
{

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ClientConfigurations
    {
 
        [System.Xml.Serialization.XmlElement()]
        public Config Config
        {
            get;
            set;
        }

        [System.Xml.Serialization.XmlAttribute()]
        public int CustomerId
        {
            get;
            set;
        }
    }
     
    public partial class Config
    {


        [System.Xml.Serialization.XmlAttribute()]
        public string Key
        {
            get;
            set;
        }

        [System.Xml.Serialization.XmlElement()]
        public string Value
        {
            get;
            set;
        }


    }


}