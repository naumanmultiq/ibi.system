﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using IBI.DataAccess.DBModels;
using IBI.Shared;

namespace CreateViewedBase64Images
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        /*

        private static string GetLEDPreviewXmlData(string imagexml)
        {
            string dataXml = string.Empty;
            try
            {
                var doc = XDocument.Parse(imagexml);
                var targetNodes = doc.Descendants("image");
                foreach (var targetNode in targetNodes)
                {
                    Byte[] bytes = AppUtility.ConvertBase64toLEDImage(targetNode.Attribute("base64").Value);
                    string ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute = new XAttribute("base64View", ledbase64);
                    targetNode.Add(attribute);

                    bytes = AppUtility.ConvertBase64toLEDImageLarge(targetNode.Attribute("base64").Value);
                    ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute1 = new XAttribute("base64ViewLarge", ledbase64);
                    targetNode.Add(attribute1);

                }
                return string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", doc.ToString());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return dataXml;
        }
        */

        private static Byte[] GetLEDPreviewXmlData(string imagexml)
        {
            string dataXml = string.Empty;
            try
            {
                var doc = XDocument.Parse(imagexml);
                var targetNodes = doc.Descendants("image");
                foreach (var targetNode in targetNodes)
                {
                    Byte[] bytes = ConvertBase64toLEDImage(targetNode.Attribute("base64").Value);
                    //return bytes;
                    string ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute = new XAttribute("base64View", ledbase64);
                    targetNode.Add(attribute);

                    bytes = AppUtility.ConvertBase64toLEDImageLarge(targetNode.Attribute("base64").Value);
                    ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute1 = new XAttribute("base64ViewLarge", ledbase64);
                    targetNode.Add(attribute1);

                }
                return null;//string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", doc.ToString());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null; ;
        }


        private static Byte[] GetLEDPreviewXmlDataLarge(string imagexml)
        {
            string dataXml = string.Empty;
            try
            {
                var doc = XDocument.Parse(imagexml);
                var targetNodes = doc.Descendants("image");
                foreach (var targetNode in targetNodes)
                {
                    Byte[] bytes = ConvertBase64toLEDImageLarge(targetNode.Attribute("base64").Value);
                    return bytes;
                    string ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute = new XAttribute("base64View", ledbase64);
                    targetNode.Add(attribute);

                    bytes = AppUtility.ConvertBase64toLEDImageLarge(targetNode.Attribute("base64").Value);
                    ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute1 = new XAttribute("base64ViewLarge", ledbase64);
                    targetNode.Add(attribute1);

                }
                return null;//string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", doc.ToString());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null; ;
        }

        public static Bitmap Base64StringToBitmap(string base64String)
        {
            try
            {
                Bitmap bmpReturn = null;

                byte[] byteBuffer = Convert.FromBase64String(base64String);
                MemoryStream memoryStream = new MemoryStream(byteBuffer);

                bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);

                memoryStream.Close();
                memoryStream = null;
                byteBuffer = null;

                return bmpReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("Message " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }
        public static byte[] ConvertBase64toLEDImage(string base64Image)
        {
            try
            {
                if (string.IsNullOrEmpty(base64Image)) return null;

                string Image = base64Image;

                string grayScaleImage = string.Empty;
                Bitmap img = Base64StringToBitmap(Image);

                Bitmap finalImage = null;

                string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"].ToString();

                System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on1.png"));
                System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off1.png"));

                int width = img.Width * 2;
                int height = img.Height * 2;

                //create a bitmap to hold the combined image
                finalImage = new System.Drawing.Bitmap(width, height);
                                
                //get a graphics object from the image so we can draw on it
                using (Graphics graphics = Graphics.FromImage(finalImage))
                {
                    //set background color
                    //graphics.Clear(Color.Black);

                    int x = 0;
                    int y = 0;

                    for (int j = 0; j < img.Height; j++)
                    {
                        //string tmpRow = "";
                        for (int i = 0; i < img.Width; i++)
                        {
                            Color pixel = img.GetPixel(i, j);

                            int r = pixel.R;
                            int g = pixel.G;
                            int b = pixel.B;

                            x = i * 2;
                            y = j * 2;

                            if ((r < 100) && (g < 100) && (b < 100))
                            {
                                graphics.DrawImage(goldenPixel, new System.Drawing.Rectangle(x, y, goldenPixel.Width, goldenPixel.Height));
                            }
                            else
                            {
                                graphics.DrawImage(blackPixel, new System.Drawing.Rectangle(x, y, blackPixel.Width, blackPixel.Height));
                            }
                        }
                    }
                }
                ImageConverter converter = new ImageConverter();
                return (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
            }
            catch (Exception ex)
            {
                throw new Exception("Message " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

        }

        public static byte[] ConvertBase64toLEDImageLarge(string base64Image)
        {
            try
            {
                if (string.IsNullOrEmpty(base64Image)) return null;

                string Image = base64Image;

                string grayScaleImage = string.Empty;
                Bitmap img = Base64StringToBitmap(Image);

                Bitmap finalImage = null;

                string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"].ToString();

                System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on.png"));
                System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off.png"));

                int width = img.Width * 4;
                int height = img.Height * 4;

                //create a bitmap to hold the combined image
                finalImage = new System.Drawing.Bitmap(width, height);

                //get a graphics object from the image so we can draw on it
                using (Graphics graphics = Graphics.FromImage(finalImage))
                {
                    //set background color
                    //graphics.Clear(Color.Black);

                    int x = 0;
                    int y = 0;

                    for (int j = 0; j < img.Height; j++)
                    {
                        //string tmpRow = "";
                        for (int i = 0; i < img.Width; i++)
                        {
                            Color pixel = img.GetPixel(i, j);

                            int r = pixel.R;
                            int g = pixel.G;
                            int b = pixel.B;

                            x = i * 4;
                            y = j * 4;

                            if ((r < 100) && (g < 100) && (b < 100))
                            {
                                graphics.DrawImage(goldenPixel, new System.Drawing.Rectangle(x, y, goldenPixel.Width, goldenPixel.Height));
                            }
                            else
                            {
                                graphics.DrawImage(blackPixel, new System.Drawing.Rectangle(x, y, blackPixel.Width, blackPixel.Height));
                            }
                        }
                    }
                }
                ImageConverter converter = new ImageConverter();
                return (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
            }
            catch (Exception ex)
            {
                throw new Exception("Message " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    //var signDatas = dbContext.SignDatas.Select(s => !string.IsNullOrEmpty(s.ViewedPicture)).ToList();
                    foreach (var signData in dbContext.SignDatas)
                    {
                        if (!string.IsNullOrEmpty(signData.ViewedPicture))
                            continue;



                        Byte[] LEDPreviewXmlData = GetLEDPreviewXmlData(signData.Picture);
                        MemoryStream ms = new MemoryStream(LEDPreviewXmlData);
                        pictureBox1.Image = Image.FromStream(ms);


                        Byte[] LEDPreviewXmlDataLarge = GetLEDPreviewXmlDataLarge(signData.Picture);
                        MemoryStream ms1 = new MemoryStream(LEDPreviewXmlDataLarge);
                        pictureBox2.Image = Image.FromStream(ms1);




                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                }
                //dbContext.SaveChanges();
            }
        }


        

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
