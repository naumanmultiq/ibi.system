﻿namespace HogiaRealtimeJourneyService.TestClient
{
    partial class VehicleJourney
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VehicleId = new System.Windows.Forms.TextBox();
            this.lblVehicleId = new System.Windows.Forms.Label();
            this.btnFetchLines = new System.Windows.Forms.Button();
            this.Lines = new System.Windows.Forms.ComboBox();
            this.lblLine = new System.Windows.Forms.Label();
            this.btnSignOnJourney = new System.Windows.Forms.Button();
            this.Journeys = new System.Windows.Forms.ComboBox();
            this.lblJourneys = new System.Windows.Forms.Label();
            this.grdJourney = new System.Windows.Forms.DataGridView();
            this.Latitude = new System.Windows.Forms.TextBox();
            this.lblLat = new System.Windows.Forms.Label();
            this.lblLongitude = new System.Windows.Forms.Label();
            this.Longitude = new System.Windows.Forms.TextBox();
            this.IsCustomGPS = new System.Windows.Forms.CheckBox();
            this.Stops = new System.Windows.Forms.ComboBox();
            this.chkForced = new System.Windows.Forms.CheckBox();
            this.Response_Lines = new System.Windows.Forms.TextBox();
            this.Response_Journeys = new System.Windows.Forms.TextBox();
            this.Response_SignOn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStops = new System.Windows.Forms.Label();
            this.LinesUrl = new System.Windows.Forms.TextBox();
            this.JourneysUrl = new System.Windows.Forms.TextBox();
            this.JourneySignOnUrl = new System.Windows.Forms.TextBox();
            this.grpResponses = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdJourney)).BeginInit();
            this.grpResponses.SuspendLayout();
            this.SuspendLayout();
            // 
            // VehicleId
            // 
            this.VehicleId.Location = new System.Drawing.Point(94, 22);
            this.VehicleId.Name = "VehicleId";
            this.VehicleId.Size = new System.Drawing.Size(100, 20);
            this.VehicleId.TabIndex = 0;
            this.VehicleId.Text = "9999";
            // 
            // lblVehicleId
            // 
            this.lblVehicleId.AutoSize = true;
            this.lblVehicleId.Location = new System.Drawing.Point(23, 25);
            this.lblVehicleId.Name = "lblVehicleId";
            this.lblVehicleId.Size = new System.Drawing.Size(65, 13);
            this.lblVehicleId.TabIndex = 1;
            this.lblVehicleId.Text = "Bus Number";
            // 
            // btnFetchLines
            // 
            this.btnFetchLines.BackColor = System.Drawing.Color.Silver;
            this.btnFetchLines.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFetchLines.Location = new System.Drawing.Point(94, 137);
            this.btnFetchLines.Name = "btnFetchLines";
            this.btnFetchLines.Size = new System.Drawing.Size(153, 55);
            this.btnFetchLines.TabIndex = 2;
            this.btnFetchLines.Text = "Fetch Lines & Journeys";
            this.btnFetchLines.UseVisualStyleBackColor = false;
            this.btnFetchLines.Click += new System.EventHandler(this.btnFetchLines_Click);
            // 
            // Lines
            // 
            this.Lines.FormattingEnabled = true;
            this.Lines.Items.AddRange(new object[] {
            "No lines available"});
            this.Lines.Location = new System.Drawing.Point(97, 235);
            this.Lines.Name = "Lines";
            this.Lines.Size = new System.Drawing.Size(100, 21);
            this.Lines.TabIndex = 4;
            this.Lines.SelectedIndexChanged += new System.EventHandler(this.Lines_SelectedIndexChanged);
            // 
            // lblLine
            // 
            this.lblLine.AutoSize = true;
            this.lblLine.Location = new System.Drawing.Point(64, 239);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(27, 13);
            this.lblLine.TabIndex = 5;
            this.lblLine.Text = "Line";
            // 
            // btnSignOnJourney
            // 
            this.btnSignOnJourney.BackColor = System.Drawing.Color.Silver;
            this.btnSignOnJourney.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignOnJourney.Location = new System.Drawing.Point(275, 260);
            this.btnSignOnJourney.Name = "btnSignOnJourney";
            this.btnSignOnJourney.Size = new System.Drawing.Size(102, 23);
            this.btnSignOnJourney.TabIndex = 2;
            this.btnSignOnJourney.Text = "Sign On";
            this.btnSignOnJourney.UseVisualStyleBackColor = false;
            this.btnSignOnJourney.Click += new System.EventHandler(this.btnSignOnJourney_Click);
            // 
            // Journeys
            // 
            this.Journeys.FormattingEnabled = true;
            this.Journeys.Location = new System.Drawing.Point(97, 262);
            this.Journeys.Name = "Journeys";
            this.Journeys.Size = new System.Drawing.Size(100, 21);
            this.Journeys.TabIndex = 4;
            // 
            // lblJourneys
            // 
            this.lblJourneys.AutoSize = true;
            this.lblJourneys.Location = new System.Drawing.Point(42, 266);
            this.lblJourneys.Name = "lblJourneys";
            this.lblJourneys.Size = new System.Drawing.Size(49, 13);
            this.lblJourneys.TabIndex = 5;
            this.lblJourneys.Text = "Journeys";
            // 
            // grdJourney
            // 
            this.grdJourney.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdJourney.Location = new System.Drawing.Point(97, 291);
            this.grdJourney.Name = "grdJourney";
            this.grdJourney.Size = new System.Drawing.Size(444, 356);
            this.grdJourney.TabIndex = 6;
            // 
            // Latitude
            // 
            this.Latitude.BackColor = System.Drawing.Color.White;
            this.Latitude.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Latitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Latitude.Location = new System.Drawing.Point(94, 88);
            this.Latitude.Name = "Latitude";
            this.Latitude.Size = new System.Drawing.Size(153, 22);
            this.Latitude.TabIndex = 0;
            // 
            // lblLat
            // 
            this.lblLat.AutoSize = true;
            this.lblLat.Location = new System.Drawing.Point(43, 91);
            this.lblLat.Name = "lblLat";
            this.lblLat.Size = new System.Drawing.Size(45, 13);
            this.lblLat.TabIndex = 7;
            this.lblLat.Text = "Latitude";
            // 
            // lblLongitude
            // 
            this.lblLongitude.AutoSize = true;
            this.lblLongitude.Location = new System.Drawing.Point(253, 91);
            this.lblLongitude.Name = "lblLongitude";
            this.lblLongitude.Size = new System.Drawing.Size(54, 13);
            this.lblLongitude.TabIndex = 7;
            this.lblLongitude.Text = "Longitude";
            // 
            // Longitude
            // 
            this.Longitude.BackColor = System.Drawing.Color.White;
            this.Longitude.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Longitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Longitude.Location = new System.Drawing.Point(313, 88);
            this.Longitude.Name = "Longitude";
            this.Longitude.Size = new System.Drawing.Size(159, 22);
            this.Longitude.TabIndex = 8;
            // 
            // IsCustomGPS
            // 
            this.IsCustomGPS.AutoSize = true;
            this.IsCustomGPS.Checked = true;
            this.IsCustomGPS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IsCustomGPS.Location = new System.Drawing.Point(94, 65);
            this.IsCustomGPS.Name = "IsCustomGPS";
            this.IsCustomGPS.Size = new System.Drawing.Size(108, 17);
            this.IsCustomGPS.TabIndex = 9;
            this.IsCustomGPS.Text = "Use Custom GPS";
            this.IsCustomGPS.UseVisualStyleBackColor = true;
            this.IsCustomGPS.CheckedChanged += new System.EventHandler(this.IsLiveGPS_CheckedChanged);
            // 
            // Stops
            // 
            this.Stops.FormattingEnabled = true;
            this.Stops.Location = new System.Drawing.Point(300, 61);
            this.Stops.Name = "Stops";
            this.Stops.Size = new System.Drawing.Size(121, 21);
            this.Stops.TabIndex = 10;
            this.Stops.Visible = false;
            this.Stops.SelectedIndexChanged += new System.EventHandler(this.Stops_SelectedIndexChanged);
            // 
            // chkForced
            // 
            this.chkForced.AutoSize = true;
            this.chkForced.Location = new System.Drawing.Point(205, 265);
            this.chkForced.Name = "chkForced";
            this.chkForced.Size = new System.Drawing.Size(59, 17);
            this.chkForced.TabIndex = 11;
            this.chkForced.Text = "Forced";
            this.chkForced.UseVisualStyleBackColor = true;
            // 
            // Response_Lines
            // 
            this.Response_Lines.Location = new System.Drawing.Point(11, 58);
            this.Response_Lines.Multiline = true;
            this.Response_Lines.Name = "Response_Lines";
            this.Response_Lines.ReadOnly = true;
            this.Response_Lines.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Response_Lines.Size = new System.Drawing.Size(448, 162);
            this.Response_Lines.TabIndex = 12;
            this.Response_Lines.TextChanged += new System.EventHandler(this.Response_Lines_TextChanged_1);
            // 
            // Response_Journeys
            // 
            this.Response_Journeys.Location = new System.Drawing.Point(11, 267);
            this.Response_Journeys.Multiline = true;
            this.Response_Journeys.Name = "Response_Journeys";
            this.Response_Journeys.ReadOnly = true;
            this.Response_Journeys.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Response_Journeys.Size = new System.Drawing.Size(448, 162);
            this.Response_Journeys.TabIndex = 13;
            // 
            // Response_SignOn
            // 
            this.Response_SignOn.Location = new System.Drawing.Point(10, 477);
            this.Response_SignOn.Multiline = true;
            this.Response_SignOn.Name = "Response_SignOn";
            this.Response_SignOn.ReadOnly = true;
            this.Response_SignOn.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Response_SignOn.Size = new System.Drawing.Size(448, 162);
            this.Response_SignOn.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Lines (Response)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Journeys";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 439);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Journey Sign On ";
            // 
            // lblStops
            // 
            this.lblStops.AutoSize = true;
            this.lblStops.Location = new System.Drawing.Point(216, 65);
            this.lblStops.Name = "lblStops";
            this.lblStops.Size = new System.Drawing.Size(78, 13);
            this.lblStops.TabIndex = 16;
            this.lblStops.Text = "Select Stations";
            this.lblStops.Visible = false;
            // 
            // LinesUrl
            // 
            this.LinesUrl.Location = new System.Drawing.Point(11, 39);
            this.LinesUrl.Name = "LinesUrl";
            this.LinesUrl.Size = new System.Drawing.Size(448, 20);
            this.LinesUrl.TabIndex = 17;
            // 
            // JourneysUrl
            // 
            this.JourneysUrl.Location = new System.Drawing.Point(11, 248);
            this.JourneysUrl.Name = "JourneysUrl";
            this.JourneysUrl.Size = new System.Drawing.Size(447, 20);
            this.JourneysUrl.TabIndex = 18;
            // 
            // JourneySignOnUrl
            // 
            this.JourneySignOnUrl.Location = new System.Drawing.Point(10, 458);
            this.JourneySignOnUrl.Name = "JourneySignOnUrl";
            this.JourneySignOnUrl.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.JourneySignOnUrl.Size = new System.Drawing.Size(448, 20);
            this.JourneySignOnUrl.TabIndex = 19;
            // 
            // grpResponses
            // 
            this.grpResponses.Controls.Add(this.Response_Lines);
            this.grpResponses.Controls.Add(this.JourneySignOnUrl);
            this.grpResponses.Controls.Add(this.Response_Journeys);
            this.grpResponses.Controls.Add(this.JourneysUrl);
            this.grpResponses.Controls.Add(this.Response_SignOn);
            this.grpResponses.Controls.Add(this.LinesUrl);
            this.grpResponses.Controls.Add(this.label1);
            this.grpResponses.Controls.Add(this.label2);
            this.grpResponses.Controls.Add(this.label3);
            this.grpResponses.Location = new System.Drawing.Point(557, 12);
            this.grpResponses.Name = "grpResponses";
            this.grpResponses.Size = new System.Drawing.Size(472, 660);
            this.grpResponses.TabIndex = 20;
            this.grpResponses.TabStop = false;
            this.grpResponses.Text = "Service Response";
            this.grpResponses.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // VehicleJourney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 680);
            this.Controls.Add(this.grpResponses);
            this.Controls.Add(this.lblStops);
            this.Controls.Add(this.chkForced);
            this.Controls.Add(this.Stops);
            this.Controls.Add(this.IsCustomGPS);
            this.Controls.Add(this.Longitude);
            this.Controls.Add(this.lblLongitude);
            this.Controls.Add(this.lblLat);
            this.Controls.Add(this.grdJourney);
            this.Controls.Add(this.lblJourneys);
            this.Controls.Add(this.lblLine);
            this.Controls.Add(this.Journeys);
            this.Controls.Add(this.Lines);
            this.Controls.Add(this.btnSignOnJourney);
            this.Controls.Add(this.btnFetchLines);
            this.Controls.Add(this.lblVehicleId);
            this.Controls.Add(this.Latitude);
            this.Controls.Add(this.VehicleId);
            this.Name = "VehicleJourney";
            this.Text = "Hogia Service Test";
            this.Load += new System.EventHandler(this.VehicleJourney_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdJourney)).EndInit();
            this.grpResponses.ResumeLayout(false);
            this.grpResponses.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox VehicleId;
        private System.Windows.Forms.Label lblVehicleId;
        private System.Windows.Forms.Button btnFetchLines;
        private System.Windows.Forms.ComboBox Lines;
        private System.Windows.Forms.Label lblLine;
        private System.Windows.Forms.Button btnSignOnJourney;
        private System.Windows.Forms.ComboBox Journeys;
        private System.Windows.Forms.Label lblJourneys;
        private System.Windows.Forms.DataGridView grdJourney;
        private System.Windows.Forms.TextBox Latitude;
        private System.Windows.Forms.Label lblLat;
        private System.Windows.Forms.Label lblLongitude;
        private System.Windows.Forms.TextBox Longitude;
        private System.Windows.Forms.CheckBox IsCustomGPS;
        private System.Windows.Forms.ComboBox Stops;
        private System.Windows.Forms.CheckBox chkForced;
        private System.Windows.Forms.TextBox Response_Lines;
        private System.Windows.Forms.TextBox Response_Journeys;
        private System.Windows.Forms.TextBox Response_SignOn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStops;
        private System.Windows.Forms.TextBox LinesUrl;
        private System.Windows.Forms.TextBox JourneysUrl;
        private System.Windows.Forms.TextBox JourneySignOnUrl;
        private System.Windows.Forms.GroupBox grpResponses;
    }
}

