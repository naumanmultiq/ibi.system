﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace IBI.DataAccess.Helper
{
    class LinqHelper
    {
        /// <summary>
        /// Creates a LINQ expression which can be used to compare multiple Items from a list item.
        /// e.g: for a Groups table u can compare multiple GroupIDs
        /// Expression<Func<Group, bool>> whereClause =  BuildOrExpressionTree<Group, int>(wantedGrpIds, grp => grp.GroupID);
        /// IQueryable<Group> groups = context.Groups.Where(whereClause);
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <typeparam name="TCompareAgainst"></typeparam>
        /// <param name="wantedItems"></param>
        /// <param name="convertBetweenTypes"></param>
        /// <returns></returns>
        public static Expression<Func<TValue, bool>> BuildOrExpressionTree<TValue, TCompareAgainst>(IEnumerable<TCompareAgainst> wantedItems, Expression<Func<TValue, TCompareAgainst>> convertBetweenTypes)
        {
            ParameterExpression inputParam = convertBetweenTypes.Parameters[0];

            Expression binaryExpressionTree = BuildBinaryOrTree(wantedItems.GetEnumerator(), convertBetweenTypes.Body, null);

            return Expression.Lambda<Func<TValue, bool>>(binaryExpressionTree, new[] { inputParam });
        }

        private static Expression BuildBinaryOrTree<T>(
                       IEnumerator<T> itemEnumerator,
                       Expression expressionToCompareTo,
                       Expression expression)
        {
            if (itemEnumerator.MoveNext() == false)
                return expression;

            ConstantExpression constant = Expression.Constant(itemEnumerator.Current, typeof(T));
            BinaryExpression comparison = Expression.Equal(expressionToCompareTo, constant);

            BinaryExpression newExpression;

            if (expression == null)
                newExpression = comparison;
            else
                newExpression = Expression.OrElse(expression, comparison);

            return BuildBinaryOrTree(itemEnumerator, expressionToCompareTo, newExpression);

        }

    }
}
