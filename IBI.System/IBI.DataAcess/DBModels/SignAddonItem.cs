//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class SignAddonItem
    {
    	 
        public SignAddonItem()
        {
            this.SignAddonDatas = new HashSet<SignAddonData>();
        }
    
        public int SignAddonItemId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string SignText { get; set; }
        public int SortValue { get; set; }
        public bool Excluded { get; set; }
        public System.DateTime DateAdded { get; set; }
        public System.DateTime DateModified { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual ICollection<SignAddonData> SignAddonDatas { get; set; }
    }
}
