//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class SpecialDestination
    {
        public int Id { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Line { get; set; }
        public string Destination { get; set; }
        public string SelectionStructure { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<bool> Excluded { get; set; }
    }
}
