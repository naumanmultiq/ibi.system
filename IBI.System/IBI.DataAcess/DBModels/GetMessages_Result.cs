//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    
    public partial class GetMessages_Result
    {
        public int MessageCategoryId { get; set; }
        public string CategoryName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public int MessageId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> MessageCategoryId1 { get; set; }
        public string Subject { get; set; }
        public string MessageText { get; set; }
        public System.DateTime ValidFrom { get; set; }
        public System.DateTime ValidTo { get; set; }
        public Nullable<int> Sender { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public Nullable<int> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public bool IsArchived { get; set; }
        public Nullable<int> MessageReplyTypeId { get; set; }
        public bool ShowAsPopup { get; set; }
    }
}
