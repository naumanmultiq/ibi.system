﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using IBI.DataAcess;
//using IBI.DataAcess.DBModels;
//using System.Data.Entity;
using IBI.Server;
using System.Threading;
using System.Net;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.Globalization;

namespace IBI.Server.TestClient
{
    class Program
    {

        static void Main(string[] args)
        {

            try
            {
                
                IBIServer server = new IBIServer();
                System.Console.WriteLine("IBI Server started");
                System.Console.ReadLine();
            }
            catch (Exception ex)
            {

            }
             

        }
    }

    class TestThread
    {
        public int myValue = 0;

        public TestThread(int value)
        {
            myValue = value;
        }

        public static void StartThread(Object obj)
        {
            TestThread tt = (TestThread)obj;
            System.Console.WriteLine("Thread started: " + tt.myValue);
            Thread.Sleep(tt.myValue * 1000);
            System.Console.WriteLine("Thread sleep ended");
            ThreadPool.QueueUserWorkItem(TestThread.StartThread, tt);
        }
    }
}
