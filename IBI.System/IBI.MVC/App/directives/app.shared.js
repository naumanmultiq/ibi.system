﻿// Application level shared directives

angular.module("App.Shared", []).directive('infiniteScroll', ["$window", function ($window) {
	return {
		link: function (scope, element, attrs) {
			var offset = parseInt(attrs.threshold) || 0;
			var e = element[0];

			element.bind('scroll', function () {
				if (scope.$eval(attrs.canLoad) && e.scrollTop + e.offsetHeight >= e.scrollHeight - offset) {
					scope.$apply(attrs.infiniteScroll);
				}
			});
		}
	};
}]);

angular.module("App.Shared").directive('resizer', function ($document, $window) {


	return function ($scope, $element, $attrs) {

		$scope.onResize = function () {

			var vv = parseInt(window.innerHeight) - (parseInt(document.getElementById("themes").offsetHeight)
             + parseInt(document.getElementById("menus").offsetHeight) + 10);

			angular.element(document.getElementById("scrollloaderlist")).css({
				height: (vv - 70) + 'px'
			});

			angular.element(document.getElementById("BodyContentsArea")).css({
				height: vv + 'px'
			});

			angular.element(document.getElementById("DeviceGroupsContainer")).css({
				height: vv + 'px'
			});

			angular.element(document.getElementById("DevicesListContainer")).css({
				height: vv + 'px'
			});

			angular.element(document.getElementById("VerticalResizer")).css({
				height: vv + 'px'
			});


			//var wid = parseInt(window.innerWidth);

			//angular.element(document.getElementById("MenuModulesLayout")).css({
			//    width: wid+ 'px'
			//});

			//angular.element(document.getElementById("MenuModulesContent")).css({
			//    width: wid + 'px'
			//});

		}

		//$scope.onResize();

		angular.element($window).bind('resize', function () {
			$scope.onResize();
		});


		if ($attrs.resizer == 'DeviceList') {
			$scope.onResize();
		}

		$element.on('mousedown', function (event) {
			event.preventDefault();

			$document.on('mousemove', mousemove);
			$document.on('mouseup', mouseup);
		});

		function mousemove(event) {

			if ($attrs.resizer == 'vertical') {

				// Handle vertical resizer
				var x = event.pageX;
				var v = x - parseInt(document.getElementById("BodyContentsArea").offsetLeft);
				if ((v > (parseInt(document.getElementById("BodyContentsArea").offsetWidth - 100))) || (v <= 100))
					return;

				$element.css({
					left: x + 'px'
				});

				angular.element(document.querySelector($attrs.resizerLeft)).css({
					width: v + 'px'
				});



				angular.element(document.querySelector($attrs.resizerRight)).css({
					left: (v + parseInt($attrs.resizerWidth)) + 'px',

					width: ((parseInt(document.getElementById("BodyContentsArea").offsetWidth - (parseInt($attrs.resizerWidth)))) - v) + 'px',

				});





			} else {
				// Handle horizontal resizer
				var y = window.innerHeight - event.pageY;

				$element.css({
					bottom: y + 'px'
				});

				angular.element(document.querySelector($attrs.resizerTop)).css({
					bottom: (y + parseInt($attrs.resizerHeight)) + 'px'
				});
				angular.element(document.querySelector($attrs.resizerBottom)).css({
					height: y + 'px'
				});
			}
		}

		function mouseup() {
			$document.unbind('mousemove', mousemove);
			$document.unbind('mouseup', mouseup);
		}
	};
});

angular.module("App.Shared").directive('loadingBar', function () {
	return {
		restrict: 'AE',
		replace: true,
		template: '<div class="loading bar">' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                    '</div>',

		link: function (scope, elem, attrs) {

		}
	};
});

