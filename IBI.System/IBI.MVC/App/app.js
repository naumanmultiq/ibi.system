﻿var app = angular.module("ibiApp", ['ui.bootstrap', 'ngQuickDate', 'infinite-scroll']);

////CONFIG KEYS
//var REST_PATH = 'http://localhost/ibi/rest/';
//var APP_PATH = 'http://localhost/ibi/';


app.directive('bindHtmlUnsafe', function ($parse, $compile) {
    return function ($scope, $element, $attrs) {
        var compile = function (newHTML) {
            newHTML = $compile(newHTML)($scope);
            $element.html('').append(newHTML);
        };

        var htmlName = $attrs.bindHtmlUnsafe;

        $scope.$watch(htmlName, function (newHTML) {
            if (!newHTML) return;
            compile(newHTML);
        });

    };
});

app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

var controllers = {};
 
//angular.module('myModule').service('browserDetectionService', function () {

//    return {
//        isCompatible: function () {

//            var browserInfo = navigator.userAgent;
//            var browserFlags = {};

//            browserFlags.ISFF = browserInfo.indexOf('Firefox') != -1;
//            browserFlags.ISOPERA = browserInfo.indexOf('Opera') != -1;
//            browserFlags.ISCHROME = browserInfo.indexOf('Chrome') != -1;
//            browserFlags.ISSAFARI = browserInfo.indexOf('Safari') != -1 && !browserFlags.ISCHROME;
//            browserFlags.ISWEBKIT = browserInfo.indexOf('WebKit') != -1;

//            browserFlags.ISIE = browserInfo.indexOf('Trident') > 0 || navigator.userAgent.indexOf('MSIE') > 0;
//            browserFlags.ISIE6 = browserInfo.indexOf('MSIE 6') > 0;
//            browserFlags.ISIE7 = browserInfo.indexOf('MSIE 7') > 0;
//            browserFlags.ISIE8 = browserInfo.indexOf('MSIE 8') > 0;
//            browserFlags.ISIE9 = browserInfo.indexOf('MSIE 9') > 0;
//            browserFlags.ISIE10 = browserInfo.indexOf('MSIE 10') > 0;
//            browserFlags.ISOLD = browserFlags.ISIE6 || browserFlags.ISIE7 || browserFlags.ISIE8 || browserFlags.ISIE9; // MUST be here

//            browserFlags.ISIE11UP = browserInfo.indexOf('MSIE') == -1 && browserInfo.indexOf('Trident') > 0;
//            browserFlags.ISIE10UP = browserFlags.ISIE10 || browserFlags.ISIE11UP;
//            browserFlags.ISIE9UP = browserFlags.ISIE9 || browserFlags.ISIE10UP;

//            return !browserFlags.ISOLD;
//        }
//    };

//});

controllers.JourneyController = function ($scope, $interval, $http, $timeout, $filter, $window,$q) {

    ////var msgBrowser = "Dear user of ibi\r\nWe have seen some issues when running ibi on Internet Explorer, some features that does not work as intended.\r\nIf you have the option, we recommend that you use google chrome browser, until the issues have been resolved.\r\nWe are working on fixing this, and apologize for the inconvenience this may cause.";
    ////var browserInfo = $window.navigator.userAgent;
    ////if (browserInfo.indexOf('Chrome') == -1)
    ////    alert(msgBrowser);

    //// alert(1);
    //var msgBrowser = "Dear user of IBI\r\nWe are currently experiences some issues when running ibi in this browser, there are some features that does not perform as intended.\r\nIf you have the option of doing so, we recommend that you use google chrome browser, until the issue have been resolved.\r\nWe are working on fixing this, and apologize for the inconveniences this may cause you.";
    //var browserInfo = $window.navigator.userAgent;
    //console.info("Browser is: ");
    //console.info(browserInfo);

    //var bIE = false;
    //var msie = browserInfo.indexOf('MSIE ');
    //if (msie > 0) {
    //    // IE 10 or older => return version number
    //    bIE = true;
    //    //return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    //}
    //// alert(2);
    //var trident = browserInfo.indexOf('Trident/');
    //if (trident > 0) {
    //    // IE 11 => return version number
    //    //var rv = ua.indexOf('rv:');
    //    //return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    //    bIE = true;
    //}
    //// alert(3);
    //var edge = browserInfo.indexOf('Edge/');
    //if (edge > 0) {
    //    // Edge (IE 12+) => return version number
    //    // return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    //    bIE = true;
    //}
    ////alert();
    ////  alert(4);

    //// alert(bIE);
    //if (bIE == true) {

    //    alert(msgBrowser);
    //}
    //else {

    //    //  alert(browserInfo.indexOf('Chrome'));

    //    if (browserInfo.indexOf('Chrome') == -1)
    //        alert(msgBrowser);
    //}

    // alert(5);



    var sDate = new Date();
    var eDate = new Date();

    //sDate.setHours(0, 0, 0, 0);
    //eDate.setHours(23, 59, 59, 0);

    sDate.setHours(sDate.getHours() - 6);
    eDate.setHours(eDate.getHours() + 6);

    $scope.list = [];
    $scope.dateFilter = {
        startDate: sDate,
        endDate: eDate
    }

    //$scope.dateFilter.startDate = date;
    //$scope.dateFilter.endDate.setDate(date.getDate()+3);
    $scope.refreshInterval = 10000;
    $scope.interval_fast = 10000;
    $scope.interval_slow = 15000;
    $scope.pauseInterval = false;
    $scope.doUpdate = true;
    $scope.isFilterApplied = false;
    $scope.loop = 0;
    //$scope.interval = {};
    $scope.rowCount = 0;
    $scope.currentPage = 1; //current page
    $scope.entryLimit = 50; //max no of items to display in a page
    $scope.sortOrder = "D";
    $scope.sortFilter = "JourneyStartTime";
    $scope.predicate = ['JourneyStartTime', 'JourneyId']//"JourneyStartTime";
    $scope.reverse = true;
    $scope.selectedCustomer = {};
    $scope.search = { "lines": [], "buses": [], "destinations": [] };
    $scope.lines = "";
    $scope.buses = "";
    $scope.destinations = "";
    $scope.customers = [];
    var canceler_restcall = {};

    $scope.pageIndex = 1;
    $scope.pageSize = 100;

    $scope.journeys = {
        dataLoaded: false,
        preloaderMsg: '',
        errorMessage: '',
        loadingNextPage: false
    }

    $scope.selectedCustomer_old = {};
    var promise;

    //$scope.fillCustomerList();

    //fill customers
    //$scope.init = function () {
    //    $scope.fillCustomerList();
    //}

    //$scope.fillCustomerList = function () {

    $scope.journeys.preloaderMsg = "Loading Customer's data";
    $http.get(APP_PATH + 'Operations/GetCustomerList')
        .success(function (data) {
            $scope.customers = data;

            if (data && data.length == 1) {
                $scope.selectedCustomer = { CustomerName: data[0].CustomerName, CustomerId: data[0].CustomerId }
                $scope.fetchData();
                $("#lstCustomers").css("display", "none");
                $("#lblCustomer").css("display", "none");
            }

            $scope.journeys.dataLoaded = true;
        })
        .error(function (data) {
            alert('You have no access on the buses of any customers');
            $scope.journeys.dataLoaded = true;
        });
    // };


    $scope.fetchData = function (pIndex) {

        try{
        

        //$interval.cancel($scope.interval);
        if ($scope.pauseInterval) {
            return;
        }


        if ($scope.selectedCustomer.CustomerId) {

            $scope.pauseInterval = true;

            if ($scope.selectedCustomer.CustomerId != $scope.selectedCustomer_old) {
                $scope.pageIndex = 1;
                $scope.pageSize = 100;
                $scope.list = [];

                $scope.filteredItems = $scope.rowCount = 0;

                $interval.cancel(promise);
                promise = null;
            }

            $scope.selectedCustomer_old = $scope.selectedCustomer.CustomerId;

            $scope.journeys.dataLoaded = false;
            $scope.journeys.preloaderMsg = "Loading Journeys ...";

            if (pIndex) {
                $scope.pageIndex = pIndex;
                $scope.journeys.loadingNextPage = true;
            }

            // alert($scope.dateFilter.startDate);
            // alert($scope.dateFilter.endDate);

            $scope.journeys.errorMessage = "";

            //$http.get(REST_PATH + 'Journey/JourneyService.svc/GetJourneys?customerId=' + $scope.selectedCustomer.CustomerId)
            sDate = new Date($scope.dateFilter.startDate);
            //sDate.setHours($scope.timeRangeFilter.min);

            eDate = new Date($scope.dateFilter.endDate);
            //eDate.setHours($scope.timeRangeFilter.max);

            if (sDate > eDate) {
                $scope.journeys.errorMessage = "Invalid date range selected";
                $scope.list = [];

                $scope.journeys.dataLoaded = true;
                $scope.journeys.preloaderMsg = "";
                $scope.journeys.loadingNextPage = false;
                $scope.pauseInterval = false;

                $scope.filteredItems = $scope.rowCount = 0;

                return;
            }


            sDateString = sDate.getFullYear() + '-' + $scope.lPad(sDate.getMonth() + 1, 2) + '-' + $scope.lPad(sDate.getDate(), 2) + 'T' + $scope.lPad(sDate.getHours(), 2) + ':' + $scope.lPad(sDate.getMinutes(), 2);
            eDateString = eDate.getFullYear() + '-' + $scope.lPad(eDate.getMonth() + 1, 2) + '-' + $scope.lPad(eDate.getDate(), 2) + 'T' + $scope.lPad(eDate.getHours(), 2) + ':' + $scope.lPad(eDate.getMinutes(), 2);

            // abort previous requests if they are still running
            if (canceler_restcall.resolve)
                canceler_restcall.resolve();
            // create a new canceler
            canceler_restcall = $q.defer();

            //$http.get(REST_PATH + 'Journey/JourneyService.svc/GetJourneys?customerId=' + $scope.selectedCustomer.CustomerId + '&sDate=' + sDateString + '&eDate=' + eDateString + '&pageIndex=' + $scope.pageIndex + '&pageSize=' + $scope.pageSize + '&jsonp=JSON_CALLBACK')
            //$http.get(REST_PATH + 'Journey/JourneyService.svc/GetJourneys?customerId=' + $scope.selectedCustomer.CustomerId + '&sDate=' + sDateString + '&eDate=' + eDateString + '&pageIndex=1' + '&pageSize=' + $scope.pageIndex * $scope.pageSize + '&jsonp=JSON_CALLBACK')
            $http.get(APP_PATH + 'Operations/GetActiveJourneys?customerId=' + $scope.selectedCustomer.CustomerId + '&sDate=' + sDateString + '&eDate=' + eDateString + '&pageIndex=1' + '&pageSize=' + $scope.pageIndex * $scope.pageSize + '&jsonp=JSON_CALLBACK', { timeout: canceler_restcall.promise })
                .success(function (data) {

                    //tmpList = data;

                    //for (var i = 0; i < tmpList.length; i++) {
                    //    $scope.list.push(tmpList[i]);
                    //}
                    var tmpList = data;

                    var sList = $scope.list;

                    //assign rowCount vars
                    if (tmpList && tmpList.length > 0)
                        $scope.rowCount = tmpList[0].TotalRows;

                    tmpList = $scope.lineFilter(tmpList);
                    tmpList = $scope.busFilter(tmpList);
                    tmpList = $scope.destFilter(tmpList);

                    sList = $scope.lineFilter(sList);
                    sList = $scope.busFilter(sList);
                    sList = $scope.destFilter(sList);

                    //control pageIndex to avoid going OutOfBounds
                    if ($scope.pageIndex > 1 && sList.length > ($scope.pageIndex * $scope.pageSize)) {
                        $scope.pageIndex--;
                    }



                    if (!promise)
                        promise = $interval($scope.looper, $scope.interval_fast);
                    else
                        if ((sList && sList.length >= $scope.rowCount) || ($scope.isFilterApplied || $scope.pageIndex > 20)) {
                            //no need to increase page index anymore 
                            $interval.cancel(promise);
                            promise = null;
                            $scope.pageIndex -= 1;
                            //increase default refresh interval to 10sec 
                            promise = $interval($scope.looper, $scope.interval_slow);
                        }
                        else {
                            $interval.cancel(promise);
                            promise = $interval($scope.looper, $scope.interval_fast);
                        }



                    if (tmpList == null || tmpList.length == 0) {

                        $scope.journeys.errorMessage = "No journey matches your filter criteria";
                        $scope.journeys.dataLoaded = true;
                        $scope.journeys.preloaderMsg = "";
                        $scope.journeys.loadingNextPage = false;

                        $scope.filteredItems = slist.length; //Initially for no filter
                        $scope.totalItems = sList.length;

                        $scope.list = sList;
                        $scope.pauseInterval = false;
                        return;
                    }



                    if (sList.length == 0) {
                        sList = tmpList;
                    }
                    else {

                        //remove existing journeys which didn't come with latest data.
                        for (var n = 0; n < sList.length; n++) {
                            source = sList[n];

                            journeyId = source.JourneyId;

                            target = $filter('filter')(tmpList, { JourneyId: journeyId }, false)[0];
                            if (target == null) {
                                //remove that item from sList
                                sList.splice(n, 1);
                            }

                        }

                        //sync older journeys, append new journeys to the list
                        for (var n = 0; n < tmpList.length; n++) {
                            source = tmpList[n];
                            sIndex = n;

                            journeyId = source.JourneyId;

                            target = $filter('filter')(sList, { JourneyId: journeyId }, false)[0];
                            var tIndex = sList.indexOf(target);

                            if (target == null) {
                                sList.push(source);
                                continue;
                            }

                            if (target && target.LastUpdated != source.LastUpdated) {
                                sList[tIndex] = tmpList[sIndex];
                                continue;
                            }
                        }


                    }




                    $scope.filteredItems = sList.length; //Initially for no filter
                    $scope.totalItems = sList.length;

                    $scope.journeys.dataLoaded = true;
                    $scope.journeys.loadingNextPage = false;

                    sList = $filter('orderBy')(sList, $scope.predicate, $scope.reverse);

                    $scope.list = sList;
                    //$scope.sort_by("JourneyStartTime");

                    //if (sList.length < $scope.pageIndex * $scope.pageSize) {
                    //    $scope.pageIndex = Math.round(sList.length / $scope.pageSize);
                    //}

                    sList = null;
                    tmpList = null;

                    $scope.pauseInterval = false;
                })
                .error(function (data) {
                    //alert('Loading Stopped');
                    $scope.journeys.errorMessage = "Problems in fetching Journey's data :(";
                    $scope.journeys.dataLoaded = true;
                    $scope.pauseInterval = false;
                });

            sDate = null;
            eDate = null;
            sDateString = '';
            eDateString = '';

            $timeout(function () {
                if (canceler_restcall.resolve)
                    canceler_restcall.resolve();// this aborts the request!

                $scope.journeys.dataLoaded = true;
                $scope.pauseInterval = false;

            }, 9*1000);

           // $scope.pauseInterval = false;

        }//end selectedCustomer check

        }
        catch (err) {
            $scope.journeys.errorMessage = "Problems in fetching Journey's data, wait :(";
            $scope.journeys.dataLoaded = true;
            $scope.pauseInterval = false;
        }


    }//end fetchData

    $scope.filterChange = function (data) {

        //$scope.isFilterApplied = true;
        var lineFilter = $("#lineFilter").tagsinput('items')
        var busFilter = $("#busFilter").tagsinput('items')
        var destFilter = $("#destFilter").tagsinput('items')

        $scope.isFilterApplied = ((lineFilter && lineFilter.length > 0) || (busFilter && busFilter.length > 0) || (destFilter && destFilter.length > 0)) ? true : false;

        $scope.pageIndex = 1;
        $scope.fetchData(1);
    }

    $scope.tRows_last = 0;
    $scope.tRows_new = 0;
    $scope.looper = function () {

        //alert($scope.doUpdate);
        if (!($scope.selectedCustomer.CustomerId))
            return;


        if (!$scope.pauseInterval && $scope.doUpdate) {

            //$scope.fetchData();
            $scope.fetchData($scope.pageIndex + 1);
            $scope.loop++;
        }

        //$scope.pauseInterval = false;

    } // end interval function




    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.lineFilter = function (jList) {
        $scope.search.lines = $("#lineFilter").tagsinput('items');
        if ($scope.search.lines.length) {
            for (var i = jList.length - 1; i >= 0; i--) {
                var doSplice = true;
                for (var l in $scope.search.lines) {
                    if (jList[i].Line.toLowerCase() == $scope.search.lines[l].toLowerCase()) {
                        doSplice = false;
                    }//end if
                }//end for   

                if (doSplice) {
                    jList.splice(i, 1);
                }
            }//end for
        }

        return jList;
    };

    $scope.busFilter = function (jList) {
        $scope.search.buses = $("#busFilter").tagsinput('items');
        if ($scope.search.buses.length) {
            for (var i = jList.length - 1; i >= 0; i--) {
                var doSplice = true;
                for (var l in $scope.search.buses) {
                    if (jList[i].BusNumber.toLowerCase().indexOf($scope.search.buses[l].toLowerCase()) != -1) {
                        doSplice = false;
                    }//end if
                }//end for    

                if (doSplice) {
                    jList.splice(i, 1);
                }
            }//end for
        }

        return jList;
        //}, 10);
    };

    $scope.destFilter = function (jList) {
        $scope.search.destinations = $("#destFilter").tagsinput('items');
        if ($scope.search.destinations.length) {
            for (var i = jList.length - 1; i >= 0; i--) {
                var doSplice = true;
                for (var l in $scope.search.destinations) {
                    if (jList[i].Destination.toLowerCase().indexOf($scope.search.destinations[l].toLowerCase()) != -1) {
                        doSplice = false;
                    }//end if
                }//end for   

                if (doSplice) {
                    jList.splice(i, 1);
                }
            }//end for
        }


        return jList;
        //}, 10);
    };

    $scope.sort_by = function (predicate) {
        $scope.predicate = [predicate, 'JourneyId'];
        $scope.reverse = !$scope.reverse;
        $scope.sortFilter = predicate;
        toggleSortOrder();
    };

    $scope.isSelectedFilter = function (currentFilter) {
        if (currentFilter === $scope.sortFilter)
            return true;
        else
            return false;
    }

    $scope.selectedFilterStyle = function (currentFilter) {
        if (currentFilter === $scope.sortFilter) {
            return { color: "#000", backgroundColor: "#ccc" }
        }
    }

    toggleSortOrder = function () {
        $scope.sortOrder = $scope.sortOrder == "A" ? "D" : "A";
    }

    $scope.setCriticalCss = function (j) {
        if (j) {
            return { color: "#fff", backgroundColor: "#ff0000" }
        }
    }

    $scope.formatJSONDate = function (jsonDate) {

        if (jsonDate) {
            var myDate = new Date(jsonDate.match(/\d+/)[0] * 1);
            return myDate;
        }
        else
            return "";
    }

    $scope.toDate = function (value) {
        if (value) {
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));
            //return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
            return dt.getFullYear() + "-" + $scope.lPad((dt.getMonth() + 1), 2) + "-" + $scope.lPad(dt.getDate(), 2) + "T" + $scope.lPad(dt.getHours(), 2) + ":" + $scope.lPad(dt.getMinutes(), 2) + ":" + $scope.lPad(dt.getSeconds(), 2);
        }
        else {
            return "";
        }
    }

    $scope.lPad = function (number, digits) {
        return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
    }
};





controllers.JourneyController2 = function ($scope, $interval, $http, $timeout, $filter,$window) {

    //var msgBrowser = "Dear user of ibi\r\nWe have seen some issues when running ibi on Internet Explorer, some features that does not work as intended.\r\nIf you have the option, we recommend that you use google chrome browser, until the issues have been resolved.\r\nWe are working on fixing this, and apologize for the inconvenience this may cause.";
    //var browserInfo = $window.navigator.userAgent;
    //if (browserInfo.indexOf('Chrome') == -1)
    //    alert(msgBrowser);

   // alert(1);
    var msgBrowser = "Dear user of IBI\r\nWe are currently experiences some issues when running ibi in this browser, there are some features that does not perform as intended.\r\nIf you have the option of doing so, we recommend that you use google chrome browser, until the issue have been resolved.\r\nWe are working on fixing this, and apologize for the inconveniences this may cause you.";
    var browserInfo = $window.navigator.userAgent;
    console.info("Browser is: ");
    console.info(browserInfo);

    var bIE = false;
    var msie = browserInfo.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        bIE = true;
        //return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
   // alert(2);
    var trident = browserInfo.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        //var rv = ua.indexOf('rv:');
        //return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        bIE = true;
    }
   // alert(3);
    var edge = browserInfo.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        // return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        bIE = true;
    }
    //alert();
  //  alert(4);

   // alert(bIE);
    if (bIE == true) {

        alert(msgBrowser);
    }
    else {

      //  alert(browserInfo.indexOf('Chrome'));

        if (browserInfo.indexOf('Chrome') == -1)
            alert(msgBrowser);
    }

   // alert(5);



    var sDate = new Date();
    var eDate = new Date();

    //sDate.setHours(0, 0, 0, 0);
    //eDate.setHours(23, 59, 59, 0);
    
    sDate.setHours(sDate.getHours() - 6);
    eDate.setHours(eDate.getHours() + 6);

    $scope.list = [];
    $scope.dateFilter = {
        startDate: sDate,
        endDate: eDate
    }
    
    //$scope.dateFilter.startDate = date;
    //$scope.dateFilter.endDate.setDate(date.getDate()+3);
    $scope.refreshInterval = 10000;
    $scope.interval_fast = 10000;
    $scope.interval_slow = 15000;
    $scope.pauseInterval = false;
    $scope.doUpdate = true;
    $scope.isFilterApplied = false;
    $scope.loop = 0;
    //$scope.interval = {};
    $scope.rowCount = 0;
    $scope.currentPage = 1; //current page
    $scope.entryLimit = 50; //max no of items to display in a page
    $scope.sortOrder = "D";
    $scope.sortFilter = "JourneyStartTime";
    $scope.predicate = ['JourneyStartTime', 'JourneyId']//"JourneyStartTime";
    $scope.reverse = true;
    $scope.selectedCustomer = {};
    $scope.search = { "lines": [], "buses": [], "destinations": [] };
    $scope.lines = "";
    $scope.buses = "";
    $scope.destinations = "";
    $scope.customers = [];

    $scope.pageIndex = 1;
    $scope.pageSize = 100;

    $scope.journeys = {
        dataLoaded: false,
        preloaderMsg: '',
        errorMessage: '',
        loadingNextPage: false
    }

    $scope.selectedCustomer_old = {};
    var promise;
     
    //$scope.fillCustomerList();

    //fill customers
    //$scope.init = function () {
    //    $scope.fillCustomerList();
    //}

    //$scope.fillCustomerList = function () {
     
    $scope.journeys.preloaderMsg = "Loading Customer's data";
        $http.get(APP_PATH + 'Operations/GetCustomerList')
            .success(function (data) {
                $scope.customers = data;

                if (data && data.length == 1) {
                    $scope.selectedCustomer = { CustomerName: data[0].CustomerName, CustomerId: data[0].CustomerId }
                    $scope.fetchData();
                    $("#lstCustomers").css("display", "none");
                    $("#lblCustomer").css("display", "none");
                }

                $scope.journeys.dataLoaded = true;
            })
            .error(function (data) {
                alert('You have no access on the buses of any customers');
                $scope.journeys.dataLoaded = true;
            });
   // };
    
       
        $scope.fetchData = function (pIndex) {

            //$interval.cancel($scope.interval);
            if ($scope.pauseInterval) {
                return;
            }
            

            if ($scope.selectedCustomer.CustomerId) {

                $scope.pauseInterval = true;

                if ($scope.selectedCustomer.CustomerId != $scope.selectedCustomer_old) {
                    $scope.pageIndex = 1;
                    $scope.pageSize = 100;
                    $scope.list = [];

                    $scope.filteredItems = $scope.rowCount = 0;
                    
                    $interval.cancel(promise);
                    promise = null;
                }

                $scope.selectedCustomer_old = $scope.selectedCustomer.CustomerId;

                $scope.journeys.dataLoaded = false;
                $scope.journeys.preloaderMsg = "Loading Journeys ...";

                if (pIndex) {
                    $scope.pageIndex = pIndex;
                    $scope.journeys.loadingNextPage = true;
                }

               // alert($scope.dateFilter.startDate);
               // alert($scope.dateFilter.endDate);

                $scope.journeys.errorMessage = "";

                //$http.get(REST_PATH + 'Journey/JourneyService.svc/GetJourneys?customerId=' + $scope.selectedCustomer.CustomerId)
                sDate = new Date($scope.dateFilter.startDate);
                //sDate.setHours($scope.timeRangeFilter.min);

                eDate =  new Date($scope.dateFilter.endDate);
                //eDate.setHours($scope.timeRangeFilter.max);

                if (sDate > eDate) {
                    $scope.journeys.errorMessage = "Invalid date range selected";
                    $scope.list = [];

                    $scope.journeys.dataLoaded = true;
                    $scope.journeys.preloaderMsg = "";
                    $scope.journeys.loadingNextPage = false;
                    $scope.pauseInterval = false;

                    $scope.filteredItems = $scope.rowCount = 0;

                    return;
                }


                sDateString = sDate.getFullYear() + '-' + $scope.lPad(sDate.getMonth() + 1, 2) + '-' + $scope.lPad(sDate.getDate(), 2) + 'T' + $scope.lPad(sDate.getHours(), 2) + ':' + $scope.lPad(sDate.getMinutes(), 2);
                eDateString = eDate.getFullYear() + '-' + $scope.lPad(eDate.getMonth() + 1, 2) + '-' + $scope.lPad(eDate.getDate(), 2) + 'T' + $scope.lPad(eDate.getHours(), 2) + ':' + $scope.lPad(eDate.getMinutes(), 2);

                //$http.get(REST_PATH + 'Journey/JourneyService.svc/GetJourneys?customerId=' + $scope.selectedCustomer.CustomerId + '&sDate=' + sDateString + '&eDate=' + eDateString + '&pageIndex=' + $scope.pageIndex + '&pageSize=' + $scope.pageSize + '&jsonp=JSON_CALLBACK')
                //$http.get(REST_PATH + 'Journey/JourneyService.svc/GetJourneys?customerId=' + $scope.selectedCustomer.CustomerId + '&sDate=' + sDateString + '&eDate=' + eDateString + '&pageIndex=1' + '&pageSize=' + $scope.pageIndex * $scope.pageSize + '&jsonp=JSON_CALLBACK')
                $http.get(APP_PATH + 'Operations/GetActiveJourneys?customerId=' + $scope.selectedCustomer.CustomerId + '&sDate=' + sDateString + '&eDate=' + eDateString + '&pageIndex=1' + '&pageSize=' + $scope.pageIndex * $scope.pageSize + '&jsonp=JSON_CALLBACK')
                    .success(function (data) {

                        //tmpList = data;

                        //for (var i = 0; i < tmpList.length; i++) {
                        //    $scope.list.push(tmpList[i]);
                        //}
                        var tmpList = data;

                        var sList = $scope.list;

                        //assign rowCount vars
                        if (tmpList && tmpList.length > 0)
                            $scope.rowCount = tmpList[0].TotalRows;

                        tmpList = $scope.lineFilter(tmpList);
                        tmpList = $scope.busFilter(tmpList);
                        tmpList = $scope.destFilter(tmpList);

                        sList = $scope.lineFilter(sList);
                        sList = $scope.busFilter(sList);
                        sList = $scope.destFilter(sList);

                        //control pageIndex to avoid going OutOfBounds
                        if ($scope.pageIndex>1 && sList.length > ($scope.pageIndex * $scope.pageSize)) {
                            $scope.pageIndex--;
                        }
                         


                        if (!promise)
                            promise = $interval($scope.looper, $scope.interval_fast);
                        else
                            if ((sList && sList.length >= $scope.rowCount) || ($scope.isFilterApplied || $scope.pageIndex > 20)) {
                                //no need to increase page index anymore 
                                $interval.cancel(promise);
                                promise = null;
                                $scope.pageIndex -= 1;
                                //increase default refresh interval to 10sec 
                                promise = $interval($scope.looper, $scope.interval_slow);
                            }
                            else {
                                $interval.cancel(promise);
                                promise = $interval($scope.looper, $scope.interval_fast);
                            }



                        if (tmpList == null || tmpList.length == 0) {
                            
                            $scope.journeys.errorMessage = "No journey matches your filter criteria";
                            $scope.journeys.dataLoaded = true;
                            $scope.journeys.preloaderMsg = "";
                            $scope.journeys.loadingNextPage = false;

                            $scope.filteredItems = slist.length; //Initially for no filter
                            $scope.totalItems = sList.length;
                                                
                            $scope.list = sList;
                            $scope.pauseInterval = false;
                            return;
                        }



                        if (sList.length == 0) {
                            sList = tmpList;
                        }
                        else {

                            //remove existing journeys which didn't come with latest data.
                            for (var n = 0; n < sList.length; n++) {
                                source = sList[n];
                            
                                journeyId = source.JourneyId;

                                target = $filter('filter')(tmpList, { JourneyId: journeyId }, false)[0];
                                if (target == null) {
                                    //remove that item from sList
                                    sList.splice(n, 1);
                                }

                            }

                            //sync older journeys, append new journeys to the list
                            for(var n=0; n<tmpList.length; n++) {                            
                                source = tmpList[n];
                                sIndex = n;

                                journeyId = source.JourneyId;

                                target = $filter('filter')(sList, { JourneyId: journeyId }, false)[0];
                                var tIndex = sList.indexOf(target);

                                if (target == null)
                                {
                                    sList.push(source);
                                    continue;
                                }

                                if(target && target.LastUpdated != source.LastUpdated)
                                { 
                                    sList[tIndex] = tmpList[sIndex];
                                    continue;
                                }                          
                            }

                       
                        }

                    
                    

                        $scope.filteredItems = sList.length; //Initially for no filter
                        $scope.totalItems = sList.length;

                        $scope.journeys.dataLoaded = true;
                        $scope.journeys.loadingNextPage = false;

                        sList = $filter('orderBy')(sList, $scope.predicate, $scope.reverse);

                        $scope.list = sList;
                        //$scope.sort_by("JourneyStartTime");
                        
                        //if (sList.length < $scope.pageIndex * $scope.pageSize) {
                        //    $scope.pageIndex = Math.round(sList.length / $scope.pageSize);
                        //}

                        sList = null;
                        tmpList = null;
                         
                    })
                    .error(function (data) {
                        //alert('Loading Stopped');
                        $scope.journeys.errorMessage = "Problems in fetching Journey's data :(";
                        $scope.journeys.dataLoaded = true;
                    });
                
                sDate = null;
                eDate = null;
                sDateString = '';
                eDateString = '';

              
                $scope.pauseInterval = false;

            }//end selectedCustomer check
        }//end fetchData
      
        $scope.filterChange = function (data) {

            //$scope.isFilterApplied = true;
            var lineFilter = $("#lineFilter").tagsinput('items')
            var busFilter = $("#busFilter").tagsinput('items')
            var destFilter = $("#destFilter").tagsinput('items')

            $scope.isFilterApplied = ((lineFilter && lineFilter.length > 0) || (busFilter && busFilter.length > 0) || (destFilter && destFilter.length > 0)) ? true : false;
            
            $scope.pageIndex = 1;
            $scope.fetchData(1);
        }

        $scope.tRows_last = 0;
        $scope.tRows_new = 0;
    $scope.looper = function () {

        //alert($scope.doUpdate);
        if (!($scope.selectedCustomer.CustomerId)) 
            return;
        

        if (!$scope.pauseInterval && $scope.doUpdate) {
              
            //$scope.fetchData();
            $scope.fetchData($scope.pageIndex + 1); 
            $scope.loop++;
        }

        //$scope.pauseInterval = false;
        
    } // end interval function
            

    

    $scope.setPage = function(pageNo) {
         $scope.currentPage = pageNo;
    };

    $scope.lineFilter = function (jList) {
        $scope.search.lines = $("#lineFilter").tagsinput('items');        
        if ($scope.search.lines.length) {
            for (var i = jList.length - 1; i >= 0; i--) {
                var doSplice = true;
                for (var l in $scope.search.lines) {
                    if (jList[i].Line.toLowerCase()==$scope.search.lines[l].toLowerCase()) {
                        doSplice = false;        
                    }//end if
                }//end for   

                if (doSplice) {
                    jList.splice(i, 1);
                }
            }//end for
        }
        
        return jList; 
    };

    $scope.busFilter = function (jList) {
        $scope.search.buses = $("#busFilter").tagsinput('items');  
        if ($scope.search.buses.length) {
            for (var i = jList.length - 1; i >= 0; i--) {
                var doSplice = true;
                for (var l in $scope.search.buses) {
                    if (jList[i].BusNumber.toLowerCase().indexOf($scope.search.buses[l].toLowerCase()) != -1) { 
                        doSplice = false;
                    }//end if
                }//end for    

                if (doSplice) {
                    jList.splice(i, 1);
                }
            }//end for
        }

        return jList;
        //}, 10);
    };

    $scope.destFilter = function (jList) {
        $scope.search.destinations = $("#destFilter").tagsinput('items');      
        if ($scope.search.destinations.length) {
            for (var i = jList.length - 1; i >= 0; i--) {
                var doSplice = true;
                for (var l in $scope.search.destinations) {
                    if (jList[i].Destination.toLowerCase().indexOf($scope.search.destinations[l].toLowerCase()) != -1) {
                        doSplice = false;
                    }//end if
                }//end for   

                if (doSplice) {
                    jList.splice(i, 1);
                }
            }//end for
        }
        
       
        return jList;
        //}, 10);
    };

    $scope.sort_by = function(predicate) {
        $scope.predicate = [predicate, 'JourneyId'];
        $scope.reverse = !$scope.reverse;
        $scope.sortFilter = predicate;
        toggleSortOrder();
    };

    $scope.isSelectedFilter = function (currentFilter) {
        if (currentFilter === $scope.sortFilter)
            return true;
        else
            return false;
    }

    $scope.selectedFilterStyle = function (currentFilter) {
        if (currentFilter === $scope.sortFilter) {
            return { color: "#000", backgroundColor: "#ccc" }
        }
    }

    toggleSortOrder = function () {
        $scope.sortOrder = $scope.sortOrder == "A" ? "D" : "A";
    }

    $scope.setCriticalCss = function (j) {
        if (j) {
            return { color: "#fff", backgroundColor: "#ff0000" }
        }
    }

    $scope.formatJSONDate = function (jsonDate) {

        if (jsonDate) {
            var myDate = new Date(jsonDate.match(/\d+/)[0] * 1);
            return myDate;
        }
        else
            return ""; 
    }

    $scope.toDate = function (value) {
        if (value) {
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));
            //return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
            return dt.getFullYear() + "-" + $scope.lPad((dt.getMonth() + 1), 2) + "-" + $scope.lPad(dt.getDate(), 2) + "T" + $scope.lPad(dt.getHours(), 2) + ":" + $scope.lPad(dt.getMinutes(), 2) + ":" + $scope.lPad(dt.getSeconds(), 2);
        }
        else {
            return "";
        }
    }

    $scope.lPad = function (number, digits) {
        return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
    }
};
    
controllers.SharedController = function () {
    this.formatJSONDate = function(jsonDate) {
        var newDate = dateFormat(jsonDate, "mm/dd/yyyy");
        return newDate;
    }
};
 

app.controller(controllers, []);
    
 