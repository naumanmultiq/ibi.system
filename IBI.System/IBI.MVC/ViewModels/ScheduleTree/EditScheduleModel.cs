﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.Web.ViewModels.ScheduleTree
{
    public class EditScheduleModel
    {
        public IBI.Shared.Models.ScheduleTreeItems.Schedule Schedule { get; set; }

        public string PostResult { get; set; }
        public string CancelAction { get; set; } //"back" OR "close"
        public bool LoadAfterSave { get; set; }
        public string ErrorMessage { get; set; }

    }
}