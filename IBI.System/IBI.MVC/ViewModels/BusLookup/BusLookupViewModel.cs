﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.Shared.Models.BusTree;

namespace IBI.Web.ViewModels.BusLookup
{
    public class BusLookupViewModel
    {
        public string BusNumber { get; set; }
        public string MacAddress { get; set; }
        public List<BusDetail> BusDetail { get; set; }
        public List<Client> ClientDetail { get; set; }
        public string BusError { get; set; }
        public string MacError { get; set; }

        public BusLookupViewModel()
        {
            BusNumber = "";
            MacAddress = "";
            this.BusDetail = null;
            this.ClientDetail = null;
            BusError = "";
            MacError = "";

        }
    }
}