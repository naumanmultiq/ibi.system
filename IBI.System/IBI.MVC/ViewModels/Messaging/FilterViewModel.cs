﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;

namespace IBI.Web.ViewModels.Messaging
{
    public class FilterViewModel
    {  
            private int selectedCategoryID = -1;
            

            public SelectList MessageCategories { get; set; }

            public string BusNumber { get; set; }
            public int CustomerId { get; set; }
            public string MessageSubjectFilter { get; set; }
            public string MessageTextFilter { get; set; }

            public int MessageCategoryIDFilter
            {
                get
                {
                    return selectedCategoryID;
                }
                set
                {
                    selectedCategoryID = value;
                }
            }
                            
            public void Fill()
            {
                MessageCategories = GetMessageCategories(this.CustomerId, MessageCategoryIDFilter);
                
            }

            private static SelectList GetMessageCategories(int customerId, int selectedCategoryId)
            {
                //[KHI]: temporary Ugly fix for PBI" 20554 //////
                customerId = Common.Settings.DefaultCustomerId;
                /////////////////////////////////////////////////

                return new SelectList(ServiceManager.GetMessageCategories(customerId),
                                "MessageCategoryId",
                                "CategoryName",
                                selectedCategoryId);
            }
        }

             
}