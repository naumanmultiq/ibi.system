﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;

namespace IBI.Web.ViewModels.Messaging.Incoming
{
    public class FilterViewModel
    {  
            private int selectedGroupID = -1;
            

            public SelectList Groups { get; set; }
            public string BusNumber { get; set; }
            public DateTime Date { get; set; }        
            public string MessageText { get; set; }
            public string Position { get; set; }
            public string Line { get; set; }
            public string Destination { get; set; }
            public string NextStop { get; set; }
            public int GroupId
            {
                get
                {
                    return selectedGroupID;
                }
                set
                {
                    selectedGroupID = value;
                }
            }
                            
            public void Fill()
            {
                Groups = GetGroups(GroupId);                
            }

            private static SelectList GetGroups(int selectedGroupId)
            {
                return new SelectList(Common.CurrentUser.Groups(),
                                "GroupId",
                                "Description",
                                selectedGroupId);
            }
        }

             
}