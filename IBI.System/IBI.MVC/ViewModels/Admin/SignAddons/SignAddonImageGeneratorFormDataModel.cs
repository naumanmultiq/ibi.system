﻿using IBI.Shared.Models.Signs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    [DataContract]
    public class SignAddonImageGeneratorFormDataModel
    {
        //[DataMember]
        //public SignGraphicRule SignGraphRule { get; set; }


        #region Simple Properties

        [DataMember]
        public int GraphicsRuleId
        { get; set; }


        [DataMember]
        public string MainFont
        { get; set; }


        [DataMember]
        public string SubFont
        { get; set; }


        [DataMember]
        public string MainStartX
        { get; set; }


        [DataMember]
        public string MainStartY
        { get; set; }



        [DataMember]
        public string SubStartX
        { get; set; }


        [DataMember]
        public string SubStartY
        { get; set; }


        [DataMember]
        public string AllCaps
        { get; set; }


        [DataMember]
        public string ShowGrid
        { get; set; }


        [DataMember]
        public string LineStartX
        { get; set; }


        [DataMember]
        public string LineStartY
        { get; set; }


        [DataMember]
        public string LineDivider
        { get; set; }


        [DataMember]
        public string LineArea
        { get; set; }


        [DataMember]
        public string LineFont
        { get; set; }


        [DataMember]
        public string SelectedTemplate
        { get; set; }

        [DataMember]
        public string SelectedTemplateName
        { get; set; }
               

        [DataMember]
        public string MainFontSize
        { get; set; }


        [DataMember]
        public string SubFontSize
        { get; set; }


        [DataMember]
        public string MainCenter
        { get; set; }


        [DataMember]
        public string UpScale
        { get; set; }


        [DataMember]
        public string DownScale
        { get; set; }


        [DataMember]
        public string AniDelay
        { get; set; }


        [DataMember]
        public string ShowErrors
        { get; set; }


        [DataMember]
        public string LineFontSize
        { get; set; }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        #endregion

        [DataMember]
        public int layoutId { get; set; }

        [DataMember]
        public int SignId { get; set; }
    }
}