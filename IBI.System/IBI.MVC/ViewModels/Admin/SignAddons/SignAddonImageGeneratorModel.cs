﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;
using System.Web.Mvc;
using IBI.Shared.Models.Signs;
using IBI.Web.Infrastructure;
using IBI.Web.Common;
using System.Xml.Linq;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    public class SignAddonImageGeneratorModel
    {
        [DataMember]
        public List<SelectListItem> Templates { get; set; }

        [DataMember]
        public string SelectedTemplate { get; set; }

        [DataMember]
        public string SelectedTemplateName { get; set; }


        [DataMember]
        public SignAddonImageGeneratorPager ImagePager { get; set; }

        [DataMember]
        public SignAddonImageTextSettings LineText { get; set; }

        [DataMember]
        public SignAddonImageTextSettings MainText { get; set; }

        [DataMember]
        public SignAddonImageTextSettings SubText { get; set; }

        [DataMember]
        public SignAddonImageAdditionalSettings Additional { get; set; }

        private const string LINE = "Line";
        private const string MAIN = "Main";
        private const string SUB = "Sub";
        private const string SELECT = "SELECT";

        private void InitializeObjects()
        {
           // this.SelectedTemplate = SELECT;
            this.LineText = new SignAddonImageTextSettings(LINE);
            this.MainText = new SignAddonImageTextSettings(MAIN);
            this.SubText = new SignAddonImageTextSettings(SUB);
            this.Additional = new SignAddonImageAdditionalSettings();
        }

        public SignAddonImageGeneratorModel()
        {
            this.InitializeObjects();
        }

        //public void ImageGeneratorModelByRule(int graphicsRuleId)
        //{
        //    SignGraphicRule SignGraphicRule = ServiceManager.GetSignGraphicRule(graphicsRuleId);
        //    this.InitializeObjects();
        //    if (SignGraphicRule != null)
        //    {
        //        this.LineText = new ImageTextSettings(LINE,
        //            SignGraphicRule.LineFont,
        //            SignGraphicRule.LineFontSize,
        //            this.CheckBool(SignGraphicRule.Center, true),
        //            SignGraphicRule.LineX,
        //            SignGraphicRule.LineY);

        //        this.MainText = new ImageTextSettings(MAIN,
        //            SignGraphicRule.MainFont,
        //            SignGraphicRule.MainFontSize,
        //            this.CheckBool(SignGraphicRule.Center, true),
        //            SignGraphicRule.MainStartX,
        //            SignGraphicRule.MainStartY);

        //        this.SubText = new ImageTextSettings(SUB,
        //            SignGraphicRule.SubFont,
        //            SignGraphicRule.SubFontSize,
        //            this.CheckBool(SignGraphicRule.Center, true),
        //            SignGraphicRule.SubStartX,
        //            SignGraphicRule.SubStartY);

        //        this.Additional = new ImageAdditionalSettings(this.CheckBool(SignGraphicRule.AllCaps),
        //            SignGraphicRule.LineDivider, SignGraphicRule.LineDivider,
        //            this.CheckBool(SignGraphicRule.ShowGrid));
        //    }
        //    this.Templates = ImageGeneratorSettings.GetTemplates(graphicsRuleId.ToString());
        //    string selectedItem = this.Templates.Where(x => x.Selected == true).Select(y => y.Text).FirstOrDefault();
        //    if (!string.IsNullOrEmpty(selectedItem))
        //        this.SelectedTemplateName = selectedItem;
        //    else
        //        this.SelectedTemplateName = "No Template";
        //}


        public void ImageGeneratorModelByRule(int graphicsRuleId)
        {
            SignGraphicRule SignGraphicRule = ServiceManager.GetSignGraphicRule(graphicsRuleId);
            this.InitializeObjects();

            //if ((SignGraphicRule != null && string.IsNullOrEmpty(SignGraphicRule.template)) || SignGraphicRule.template.ToLower() == "manual")
            {
                this.LineText = new SignAddonImageTextSettings(LINE,
                    SignGraphicRule.LineFont,
                    SignGraphicRule.LineFontSize,
                    this.CheckBool(SignGraphicRule.Center, true),
                    SignGraphicRule.LineX,
                    SignGraphicRule.LineY);

                this.MainText = new SignAddonImageTextSettings(MAIN,
                    SignGraphicRule.MainFont,
                    SignGraphicRule.MainFontSize,
                    this.CheckBool(SignGraphicRule.Center, true),
                    SignGraphicRule.MainStartX,
                    SignGraphicRule.MainStartY);

                this.SubText = new SignAddonImageTextSettings(SUB,
                    SignGraphicRule.SubFont,
                    SignGraphicRule.SubFontSize,
                    this.CheckBool(SignGraphicRule.Center, true),
                    SignGraphicRule.SubStartX,
                    SignGraphicRule.SubStartY);

                this.Additional = new SignAddonImageAdditionalSettings(this.CheckBool(SignGraphicRule.AllCaps),
                    SignGraphicRule.LineDivider, 
                    SignGraphicRule.LineArea,
                    this.CheckBool(SignGraphicRule.ShowGrid));

                string mainFont = string.IsNullOrEmpty(SignGraphicRule.MainFont)? string.Empty : SignGraphicRule.MainFont.Replace(".ttf", "");
                this.MainText.FontList = ImageGeneratorSettings.MainFonts(mainFont);
                string mainFontSize = string.IsNullOrEmpty(SignGraphicRule.MainFontSize) ? string.Empty : SignGraphicRule.MainFontSize;
                this.MainText.FontSizeList = ImageGeneratorSettings.MainFontsSizes(mainFont, mainFontSize);

                string subFont = string.IsNullOrEmpty(SignGraphicRule.SubFont) ? string.Empty : SignGraphicRule.SubFont.Replace(".ttf", "");
                this.SubText.FontList = ImageGeneratorSettings.SubFonts(subFont);
                string subFontSize = string.IsNullOrEmpty(SignGraphicRule.SubFontSize) ? string.Empty : SignGraphicRule.SubFontSize;
                this.SubText.FontSizeList = ImageGeneratorSettings.SubFontsSizes(subFont, subFontSize);

                string lineFont = string.IsNullOrEmpty(SignGraphicRule.LineFont) ? string.Empty : SignGraphicRule.LineFont.Replace(".ttf", "");
                this.LineText.FontList = ImageGeneratorSettings.LineFonts(lineFont);
                string lineFontSize = string.IsNullOrEmpty(SignGraphicRule.LineFontSize) ? string.Empty : SignGraphicRule.LineFontSize;
                this.LineText.FontSizeList = ImageGeneratorSettings.LineFontsSizes(lineFont, lineFontSize);
                
            }

            this.Templates = ImageGeneratorSettings.GetTemplates(graphicsRuleId.ToString());
            string selectedItem = this.Templates.Where(x => x.Selected == true).Select(y => y.Text).FirstOrDefault();
            if (!string.IsNullOrEmpty(selectedItem))
                this.SelectedTemplateName = selectedItem;
            else
            {
                var templateSelected = this.Templates.Where(x => x.Value == "0").First();
                templateSelected.Selected = true;
                this.SelectedTemplateName = "0";
            }
        }

        private bool CheckBool(string val, bool defaultValue = false)
        {
            bool retValue = false;
            if (string.IsNullOrEmpty(val)) retValue = defaultValue;
            else
                switch (val.ToLower())
                {
                    case "true":
                    case "1":
                        retValue = true;
                        break;
                    case "false":
                    case "0":
                        retValue = false;
                        break;
                    default:
                        retValue = defaultValue;
                        break;
                }
            return retValue;
        }

        public void ImageGeneratorModelByTemplate(int CustomerId)
        {
            ImageGeneratorSettings.ImageSettingParsing(CustomerId);
            this.InitializeObjects();

            this.MainText.FontList = ImageGeneratorSettings.MainFonts();
            this.MainText.FontSizeList = ImageGeneratorSettings.MainFontsSizes(this.MainText.FontList.ToList()[0].Text);

            this.LineText.FontList = ImageGeneratorSettings.LineFonts();
            this.LineText.FontSizeList = ImageGeneratorSettings.LineFontsSizes(this.LineText.FontList.ToList()[0].Text);

            this.SubText.FontList = ImageGeneratorSettings.SubFonts();
            this.SubText.FontSizeList = ImageGeneratorSettings.SubFontsSizes(this.SubText.FontList.ToList()[0].Text);
        }
        
        public void GetBase64Images(int customerId, int layoutId, int signId)
        {
            this.ImagePager = new SignAddonImageGeneratorPager();
            this.ImagePager.Img = new List<string>();
            this.ImagePager.Index = 0;

            List<SignDataImageItem> signImages = Infrastructure.ServiceManager.GetSignAddonImages(customerId, layoutId, signId);

            if (signImages != null && signImages.Count() > 0)
            {
                foreach (SignDataImageItem signData in signImages)
                {
                    this.ImagePager.Img.Add(signData.base64ViewString);
                }
                this.ImagePager.Count = signImages.Count;
            }
            else
            {
                this.ImagePager.Img = new List<string>{string.Empty};
                this.ImagePager.Count = 0;
            }
        }

        public SignAddonImageGeneratorModel(int ObjectId, string ObjectType)
        {
            try
            {
                switch (ObjectType)
                {
                    case "R":
                        this.ImageGeneratorModelByRule(ObjectId);
                        break;
                    case "T":
                        this.ImageGeneratorModelByTemplate(ObjectId);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}