﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using IBI.Shared.Models.Signs;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    [DataContract]
    public class SignAddonManualSignDataModel
    {
        [DataMember]
        public List<SignDataImageItem> Signs
        {
            get;
            set;
        }

        [DataMember]
        public bool PostResult
        {
            get;
            set;
        }
        [DataMember]
        public string ErrorMessage
        {
            get;
            set;
        }
        [DataMember]
        public int LayoutWidth
        {
            get;
            set;
        }
        [DataMember]
        public int LayoutHeight
        {
            get;
            set;
        }

        [DataMember]
        public int SignId
        {
            get;
            set;
        }

        [DataMember]
        public int LayoutId
        {
            get;
            set;
        }


    }
}