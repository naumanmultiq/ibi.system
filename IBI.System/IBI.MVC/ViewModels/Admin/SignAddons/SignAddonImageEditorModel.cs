﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;
using System.Web.Mvc;
using System.Xml.Linq;
using IBI.Web.Common;
using IBI.Shared.Models.Signs;
using IBI.Web.Infrastructure;
using IBI.Shared.Models.SignAddons; 
using IBI.Web.ViewModels.Admin.Destinations;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    public class SignAddonImageEditorModel
    {
        #region Constants
        private const string IMAGE_GENERATOR = "Image generator";
        private const string MANUAL = "Manual";
        #endregion

        #region Parameters To Model
        [DataMember]
        public int SignId { get; set; }

        [DataMember]
        public int layoutId { get; set; }

        #endregion

        #region Top Section
        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public string Line { get; set; }

        [DataMember]
        public string MainText { get; set; }

        [DataMember]
        public string SubText { get; set; }

        [DataMember]
        public string Type { get; set; }


        [DataMember]
        public string Size { get; set; }


        [DataMember]
        public int LayoutWidth { get; set; }

        [DataMember]
        public int LayoutHeight { get; set; }

        [DataMember]
        public string Manufacturer { get; set; }


        [DataMember]
        public string Technology { get; set; }


        [DataMember]
        public SelectList EditorMode { get; set; }


        [DataMember]
        public int DefaultGraphicRule { get; set; }


        [DataMember]
        public string Mode { get; set; }


       


        #endregion

        #region Bottom Region
        [DataMember]
        public SignAddonManualSignDataModel ManualSignData { get; set; }

        [DataMember]
        public SignAddonImageGeneratorModel ImageGenerator { get; set; }
        #endregion

        #region Constructors
        public SignAddonImageEditorModel()
        {
            this.layoutId = 0;
            this.SignId = 0;

            this.CustomerId = 0;
            this.Line = "";
            this.MainText = "";
            this.SubText = "";
            this.Type = "";
            this.Size = "";
            this.LayoutWidth = 0;
            this.LayoutHeight = 0;
            this.Manufacturer = "";
            this.Technology = "";
            this.DefaultGraphicRule = 0;
            this.SetModes();
        }

        public SignAddonImageEditorModel(int signId, int signLayoutId, int graphidRuleId,string mode)
        {
            SignAddon Sign = Infrastructure.ServiceManager.GetSignAddonItem(signId);
            SignXmlLayout SignLayout = Infrastructure.ServiceManager.GetSignLayout(signLayoutId);

            this.Line = Sign.Line;
            this.MainText = Sign.SignText;
            this.SubText = "";// Sign.UdpText;
            this.Type = SignLayout.Type;
            this.Technology = SignLayout.Technology;
            this.LayoutWidth = SignLayout.Width ?? 0;
            this.LayoutHeight = SignLayout.Height ?? 0;
            this.Size = SignLayout.Width + " x " + SignLayout.Height;
            this.Manufacturer = SignLayout.Manufacturer;
            this.DefaultGraphicRule = SignLayout.GraphicRuleId ?? 0;
            this.layoutId = SignLayout.LayoutId;
            this.SignId = Sign.SignAddonItemId;
            this.CustomerId = Sign.CustomerId;
                        
            if (!string.IsNullOrEmpty(mode) && mode.ToLower() == MANUAL.ToLower())
            {
                this.ManualSignData = SetManualMode(this.CustomerId, signLayoutId, signId);
                this.Mode = MANUAL;
            }
            else
            {
                ImageGeneratorSettings.ImageSettingParsing(this.CustomerId);
                this.ImageGeneratorModel(graphidRuleId);
                this.Base64Images(this.CustomerId, signLayoutId, signId);
                this.Mode = IMAGE_GENERATOR;
            }

            this.SetModes();
        }
        #endregion


        SignAddonManualSignDataModel SetManualMode(int customerId, int layoutId, int signId)
        {
            List<SignDataImageItem> signImages = ServiceManager.GetSignAddonImages(customerId, layoutId, signId);
            SignAddonManualSignDataModel model = new SignAddonManualSignDataModel();
            model.LayoutHeight = this.LayoutHeight;
            model.LayoutWidth = this.LayoutWidth;
            model.LayoutId = layoutId;
            model.Signs = signImages;
            model.SignId = signId;//signDataId;
            return model;
        }

        public void ImageGeneratorModel(int graphicRuleId)
        {
            this.ImageGenerator = new SignAddonImageGeneratorModel(graphicRuleId, "R");
        }
        public void Base64Images(int customerId, int layoutId, int signId)
        {
            this.ImageGenerator.GetBase64Images(customerId, layoutId, signId);
        }
        public void SetModes()
        {
            List<String> modes = new List<String>() { IMAGE_GENERATOR, MANUAL };
            this.EditorMode = new SelectList(modes, IMAGE_GENERATOR);

            //this.Mode = IMAGE_GENERATOR;
        }
    }
}