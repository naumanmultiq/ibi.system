﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Logger;
using IBI.Shared.Models.Signs;

namespace IBI.Web.ViewModels.Admin.Schedules
{
    public class ScheduleModel
    {                                 

        //public ScheduleTree ScheduleTree
        //{
        //        get
        //    {
 
        //        SignTree tree;

        //        tree = SessionManager.Get<SignTree>("SignTree");
        //        int lastCustomerId = SessionManager.Get<int>("LastCustomerId");


        //        if (this.CustomerId>0 && (tree==null || lastCustomerId!=this.CustomerId))
        //        {
        //            WebLogger.Log("GetSignTree - start", new object[] { this.CustomerId, Common.CurrentUser.UserId.ToString() });

        //            IBI.Shared.Models.Signs.SignTree tmpTree = new Shared.Models.Signs.SignTree()
        //            {
        //                 CustomerID = this.CustomerId,
        //                 UserID = Common.CurrentUser.UserId
        //            };

        //            tree = ServiceManager.GetSignTree(tmpTree, true, true);

        //            WebLogger.Log("GetSignTree - end");

        //            SessionManager.Store("SignTree", tree);
        //            SessionManager.Store("LastCustomerId", this.CustomerId);
        //        }

        //        return tree;
        //    } 
                
        //}
                                                     

        public SelectList CustomerList { get; set; }

        public int CustomerId { get; set; }

        public string PostResult { get; set; }
        public string CancelAction { get; set; } //"back" OR "close"

        public bool LoadAfterSave { get; set; }

        
        public string CustSelectionStatus { get; set; }
        
        //Constructor
        public ScheduleModel(int customerId) 
        {
            this.CustomerList = GetCustomers(customerId);
            
            CustSelectionStatus = this.CustomerList.Count() > 1 ? "1" : "0";

            this.CustomerId = customerId;

            if (this.CustomerList.Count() == 1)
            {
                this.CustomerId = int.Parse(this.CustomerList.FirstOrDefault().Value);
                CustSelectionStatus = "1";
            }

            if(this.CustomerList.Count()>1 && customerId<=0 )
            {
                this.CustomerId = -1;
                CustSelectionStatus = "0";
            }

        
            PostResult = "";

            //call just to populate session's tree in order to be used by ajax calls 
           // ScheduleTree tree = ScheduleTree;
            
        }
        public ScheduleModel()
        {

           
        }
          

        [HiddenInput(DisplayValue = false)]
        public string CustomerChanged { get; set; }



        public static SelectList GetSignGroups(int customerId, string selectedGroup = "")
        {
            //[KHI]: temporary Ugly fix for PBI" 20554 //////
             customerId = customerId == 0 ? Common.Settings.DefaultCustomerId : customerId;
            /////////////////////////////////////////////////

            //return new SelectList( ServiceManager.GetDestinationGroups(customerId),
            //                "GroupId",
            //                "GroupName",
            //                selectedGroupId ?? 0);
            return null;
        }

        public static SelectList GetCustomers(int? selectedCustomerId)
        {
            List<IBI.Shared.Models.Customer> custs = Common.CurrentUser.CustomerList;
            
            return new SelectList(custs,
                            "CustomerId",
                            "CustomerName",
                            selectedCustomerId ?? 0);
        }
    }
}   
