﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;
using IBI.Shared.Models.Signs;

namespace IBI.Web.ViewModels.Admin.Schedules
{
    public class Schedule 
    {
        [DataMember]
        public int ScheduleId { get; set; }
        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public string Line { get; set; }
        [DataMember]
        public string FromName { get; set; }
        [DataMember]
        public string Destination { get; set; }
        [DataMember]
        public string ViaName { get; set; }
        [DataMember]
        public DateTime LastUpdated { get; set; }
        [DataMember]
        public List<ScheduleStop> ScheduleStops { get; set; }
         
    }

    public class ScheduleStop  {
        [DataMember]
        public int StopSequence { get; set; }
        [DataMember]
        public string StopName { get; set; }
        [DataMember]
        public decimal StopId { get; set; }
        [DataMember]
        public string Latitude {get; set;}
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Zone { get; set; }

    }


    public class CsvScheduleStop
    {

        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public string Line { get; set; }
        [DataMember]
        public string FromName { get; set; }
        [DataMember]
        public string Destination { get; set; }
        [DataMember]
        public string ViaName { get; set; }
        [DataMember]
        public int StopSequence { get; set; }
        [DataMember]
        public decimal StopId { get; set; }
        [DataMember]
        public string StopName { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        

    }

}