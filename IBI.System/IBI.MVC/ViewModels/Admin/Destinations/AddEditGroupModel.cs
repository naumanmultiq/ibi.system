﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Logger;
using IBI.Shared.Models.Destinations;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    public class AddEditGroupModel
    {

        public IBI.Shared.Models.Signs.Group Groups
        {
            get;
            set;
        }

        public int CustomerId { get; set; }
        public string ParentGroup { get; set; }
        public int? OldParentId { get; set; }

        public int SortValue { get; set; }

        public string PostResult { get; set; }
        public string CancelAction { get; set; } //"back" OR "close"
        public bool LoadAfterSave { get; set; }
        public string ErrorMessage { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CustomerChanged { get; set; }

        public IBI.Shared.Models.Signs.Group Group { get; set; }

        //Constructor
        public AddEditGroupModel(int customerId, int groupId)
        {
            this.CustomerId = customerId;

            this.PostResult = "";
            this.Group = ServiceManager.GetSignGroup(groupId);

            if (this.Group == null)
            {
                this.Group = new IBI.Shared.Models.Signs.Group
                {
                    CustomerId = customerId,
                    Excluded = false
                };
            }
            else
            {
                OldParentId = this.Group.ParentGroupId;
            }
            SortValue = this.Group.SortValue;
        }

        public AddEditGroupModel() { }



    }
}

