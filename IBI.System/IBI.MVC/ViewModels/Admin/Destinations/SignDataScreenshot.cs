﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    public class SignDataScreenshot
    {
        [DataMember]
        public string Layout { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string ImageIndex { get; set; }
    }
}