﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    [DataContract]
    public class ImageAdditionalSettings
    {
        #region Data
        [DataMember]
        public bool AllCaps { get; set; }
        [DataMember]
        public string LineDivider { get; set; }
        [DataMember]
        public string MainSubDivider { get; set; }
        [DataMember]
        public bool ShowGrid { get; set; }
        #endregion

        #region Label
        [DataMember]
        public string LabelHeading { get; set; }
        [DataMember]
        public string LabelAllCaps { get; set; }
        [DataMember]
        public string LabelLineDivider { get; set; }
        [DataMember]
        public string LabelMainSubDivider { get; set; }
        [DataMember]
        public string LabelShowGrid { get; set; }
        #endregion

        #region Constructors
        public ImageAdditionalSettings(bool pAllCaps=false, string pLineDivider="0", string pMainSubDivider="0", bool pShowGrid=false)
        {
            this.AllCaps = pAllCaps;
            this.LineDivider = pLineDivider;
            this.MainSubDivider = pMainSubDivider;
            this.ShowGrid = pShowGrid;

            this.LabelHeading = "Additional settings";
            this.LabelAllCaps = "All Caps:";
            this.LabelLineDivider = "Line divider:";
            this.LabelMainSubDivider = "Main/Sub divider:";
            this.LabelShowGrid = "Show grid:";
        }
        #endregion

        #region Implementation 
        #endregion
    }
}