﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Logger;
using IBI.Shared.Models.Signs;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    public class AddEditSignModel
    {

        public Sign Groups
        {
            get;
            set;
        }

        public int CustomerId { get; set; }

        //public string Name { get; set; }
        public string ParentGroup { get; set; }
        //public string Line {get; set;}

        //public string Text {get; set;}
        //public string Schedule {get; set;}

        //public string SelectionStructure{get; set;}
        //public bool Excluded{get; set;}

        public string PostResult { get; set; }
        public string CancelAction { get; set; } //"back" OR "close"
        public bool LoadAfterSave { get; set; }
        public string ErrorMessage { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CustomerChanged { get; set; }
         
        public Sign Sign { get; set; }

        //Constructor
        public AddEditSignModel(int customerId, int signId)
        {
            this.CustomerId = customerId;

            this.PostResult = ""; 
            this.Sign = ServiceManager.GetSignItem(signId); 

            if (this.Sign==null)
            {
                this.Sign = new Sign
                {
                    CustomerId = customerId,
                    IsActive = true
                };
            }
        }

        public AddEditSignModel() { }



    }
}

