﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    [DataContract]
    public class ImageTextSettings
    {
        #region Data
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Font { get; set; }
        [DataMember]
        public string FontSize { get; set; }
        [DataMember]
        public bool IsCenter { get; set; }
        [DataMember]
        public string XAdjustment { get; set; }
        [DataMember]
        public string YAdjustment { get; set; }
        [DataMember]
        public SelectList FontList { get; set; }
        [DataMember]
        public SelectList FontSizeList { get; set; }
        #endregion

        
        #region Labels
        [DataMember]
        public string LabelHeading{get;set;}
        [DataMember]
        public string LabelFont { get; set; }
        [DataMember]
        public string LabelFontSize { get; set; }
        [DataMember]
        public string LabelLineCenter { get; set; }
        [DataMember]
        public string LabelXAdjustment { get; set; }
        [DataMember]
        public string LabelYAdjustment { get; set; }
        #endregion

        #region Constructors
        public ImageTextSettings(string pPrefix, string pFont="", string pFontSize="", bool pIsCenter=true, string pXAdjustment="0", string pYAdjustment="0")
        {
            this.Type = pPrefix;
            this.Font = pFont;
            this.FontSize = pFontSize;
            this.IsCenter = pIsCenter;
            this.XAdjustment = pXAdjustment;
            this.YAdjustment = pYAdjustment;
            this.FontList = new SelectList(new List<string>() { pFont });
            this.FontSizeList = new SelectList(new List<string>() { pFontSize });
            this.SetLabels(pPrefix);
            
        }
        #endregion

        #region Implementation 
        public void SetLabels(string prefix)
        {
            this.LabelHeading = string.Format("{0} text settings", prefix);
            this.LabelFont = string.Format("{0} text font:", prefix);
            this.LabelFontSize = string.Format("{0} text font size:", prefix);
            this.LabelLineCenter = string.Format("{0} text center:", prefix);
            this.LabelXAdjustment = string.Format("{0} text x adjustment:", prefix);
            this.LabelYAdjustment = string.Format("{0}  text y adjustment:", prefix);
        }
        
        #endregion
    }
}