﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    [DataContract]
    public class ImageGeneratorTextSettings
    {
        [DataMember]
        private ImageTextSettings LineText { get; set; }
        [DataMember]
        private ImageTextSettings MainText { get; set; }
        [DataMember]
        private ImageTextSettings SubText { get; set; }
        [DataMember]
        private ImageAdditionalSettings Additional { get; set; }
    }
}