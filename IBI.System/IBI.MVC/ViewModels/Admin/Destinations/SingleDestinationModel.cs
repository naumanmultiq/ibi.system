﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Logger;
using IBI.Shared.Models.Destinations;

namespace IBI.Web.ViewModels.Admin.Destinations
{
    public class SingleDestinationModel
    {                                 

        public Destination DestinationGroup
        {
            get;
            set;                
        } 

        public int CustomerId { get; set; }

        public string PostResult { get; set; }  

        public string CancelAction { get; set; } //"back" OR "close"

        public bool LoadAfterSave { get; set; }

         
        
        //Constructor
        public SingleDestinationModel(int customerId, Destination dest) 
        {
         
            this.CustomerId = customerId;
  
            //this.Groups = GetDestinationGroups(this.CustomerId);
           
            PostResult = "";

            //call just to populate session's tree in order to be used by ajax calls 
            Destination tmpDest = this.DestinationGroup;
            
        }
        public SingleDestinationModel()
        {

           
        }
           
         
    }
}   
