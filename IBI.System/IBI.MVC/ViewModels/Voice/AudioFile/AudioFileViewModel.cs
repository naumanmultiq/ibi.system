﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MvcContrib.Pagination;
using MvcContrib.Filters;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;

 

namespace IBI.Web.ViewModels.Voice.AudioFile
{
    public class AudioFileViewModel
    {
        public string Text { get; set; }
        public string Voice{ get; set; }
        public string SourceText { get; set; }
        public string Filename { get; set; }
        public int Version { get; set; }
        public string Status { get; set; }
        public string AlternateSourceVoiceText { get; set; }
        public string UploadedMp3Path { get; set; }
        public bool LoadAfterSave { get; set; }
        public string PostResult { get; set; }
        public string ErrorMessage { get; set; }
    }
}