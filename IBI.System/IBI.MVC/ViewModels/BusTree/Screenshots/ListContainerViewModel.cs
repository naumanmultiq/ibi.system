﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MvcContrib.Pagination;
using MvcContrib.Filters;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;


namespace IBI.Web.ViewModels.BusTree.Screenshots

{

    public class ListContainerViewModel
    {
        public IPagination<IBI.Web.ViewModels.BusTree.Screenshots.Screenshot> PagedList { get; set; } 
        public GridSortOptions GridSortOptions { get; set; }
    }
}