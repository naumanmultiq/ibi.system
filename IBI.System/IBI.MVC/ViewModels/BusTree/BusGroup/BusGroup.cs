﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.Web.ViewModels.BusTree.BusGroup
{
    public class BusGroupModel
    {

        public string BusNumber {get;set; }
        public int GroupId {get;set; }
        public int CustomerId {get;set; }
        public bool loadAfterSave { get; set; }
      
    }
}