﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MvcContrib.Pagination;
using MvcContrib.Filters;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;


namespace IBI.Web.ViewModels.Statistics.Punctuality

{

    public class ListContainerViewModel
    {
        public IPagination<IBI.Shared.Models.BusJourney.Punctuality> PagedList { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public GridSortOptions GridSortOptions { get; set; }
        public String ErrorMessage { get; set; }
    }
}