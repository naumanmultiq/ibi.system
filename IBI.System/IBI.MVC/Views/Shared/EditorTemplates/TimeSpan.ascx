﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.TimeSpan>" %>
<%: Html.TextBox("", new DateTime(Model.Ticks).ToString("HH:mm"), new { @class = "timedropdown", @maxlength = "8" })%>
