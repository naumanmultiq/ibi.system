﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace IBI.Web.Common
{
    public class WebUtil
    {
        public static string AppPath 
        {
            get 
            {
                if (HttpContext.Current != null)
                {
                    HttpRequest request = HttpContext.Current.Request;
                    string appPath = HttpContext.Current.Request.ApplicationPath;
                    appPath = appPath.Length == 1 ? appPath.TrimEnd('/') : appPath;
                    return appPath;
                }
                else 
                {
                    return "";                
                }
                
            }
        }


        public static string GetYesNo(bool val){
            return val ? "Yes" : "No";
        }

#region Infotainment

        //returns html
        public static string FormatVTCScreenDumb(string text)
        {
            string[] cols = text.Split('|');
            string busNumber = cols.Length > 0 ? cols[0] : "";
            string imagePath = cols.Length > 1 ? cols[1] : "";
            string imageDate = cols.Length > 2 ? cols[2] : "";

            if (busNumber == "")
                return "";

            imagePath = imagePath == "" ? "../images/infotainment/no-screenshot.jpg" : imagePath;
            imageDate = imageDate == "" ? "no preview" : imageDate;

            return string.Format(@"<div style='text-align: center;'><a href='javascript:void(0);' onclick=""ShowScreenshots('{1}')""><img src='{0}' class='vtc-screen-dumb' /></a><br><b>{1}</b> - [{2}]</div>", imagePath, busNumber, imageDate);
        }

        public static string FormatScreenshot(string text)
        {
            string[] cols = text.Split('|');
            string busNumber = cols.Length > 0 ? cols[0] : "";
            string clientType = cols.Length > 1 ? cols[1] : "";
            string imageIndex = cols.Length > 2 ? cols[2] : "";
            string imagePath = cols.Length > 3 ? cols[3] : "";
            string imageDate = cols.Length > 4 ? cols[4] : "";

            
            imagePath = imagePath == "" ? "../images/infotainment/no-screenshot.jpg" : imagePath;
            imageDate = imageDate == "" ? "No image found" : imageDate;

            return string.Format(@"<div style='text-align: center; background-color:#efefef;'><img src='{0}' class='screenshot' /><br/><br/><b>{1}</b> -- [{2}]<br/></div>", imagePath, busNumber, imageDate);
        }

        public static Bitmap Base64StringToBitmap(string base64String)
        {
            Bitmap bmpReturn = null;

            byte[] byteBuffer = Convert.FromBase64String(base64String);
            MemoryStream memoryStream = new MemoryStream(byteBuffer);

            bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);

            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;

            return bmpReturn;
        }

        public static string CreateBase64Image(Stream fileBytesStream)
        {
            if (fileBytesStream.CanSeek)
            {
                fileBytesStream.Seek(0, SeekOrigin.Begin);
                fileBytesStream.Flush();
            }
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] buffer = new byte[16 * 1024];
                int read;
                //read = fileBytesStream.Read(buffer, 0, buffer.Length);
                while ((read = fileBytesStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        public static string FormatSignDataScreenshot(string text)
        {
            /*
            string[] cols = text.Split('|');
            string Layout = cols.Length > 0 ? cols[0] : "";
            string Image = cols.Length > 1 ? cols[1] : "";
            string ImageIndex = cols.Length > 2 ? cols[2] : "";

            Image = Image == "" ? "../images/infotainment/no-screenshot.jpg" : Image;
            Image = Image == "" ? "No image found" : Image;

            string grayScaleImage = string.Empty;
            Bitmap img = Base64StringToBitmap(Image);

            for (int j = 0; j < img.Height; j++)
            {
                for (int i = 0; i < img.Width; i++)
                {
                    Color pixel = img.GetPixel(i, j);

                    int r = pixel.R;
                    int g = pixel.G;
                    int b = pixel.B;

                    if ((r < 100) && (g < 100) && (b < 100))
                        grayScaleImage += "<img src='../images/dot_on.png'/>";
                    else
                        grayScaleImage += "<img src='../images/dot_off.png'/>";

                }
                grayScaleImage += "<br/>";  
            }
             * */
            return string.Format(@"<div id='SignDataScreenShot' style='text-align: center; '><img src='data:image/png;base64,{0}'/></div>", text);
        }

        public static Stream createBlankImage(int imageWidth, int imageHeight)
        {
            Image resultImage = new Bitmap(imageWidth, imageHeight, PixelFormat.Format24bppRgb);
            using (Graphics grp = Graphics.FromImage(resultImage))
            {
                grp.FillRectangle(
                    Brushes.White, 0, 0, imageWidth, imageHeight);
            }
            return ToStream(resultImage,ImageFormat.Png);
        }

        public static Stream ToStream(Image image, ImageFormat formaw)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, formaw);
            stream.Position = 0;
            return stream;
        }


        public static string ConvertBase64toLEDImage(string base64Image)
        {
            if (string.IsNullOrEmpty(base64Image)) return null;

            string Image = base64Image;

            string grayScaleImage = string.Empty;
            Bitmap img = Base64StringToBitmap(Image);

            Bitmap finalImage = null;

            string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["AppDirectory"].ToString();

            System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on1.png"));
            System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off1.png"));

            int width = img.Width * 2;
            int height = img.Height * 2;
                
            //create a bitmap to hold the combined image
            finalImage = new System.Drawing.Bitmap(width, height);



            float brightness = 1.0f; // no change in brightness
            float contrast = 2.0f; // twice the contrast
            float gamma = 1.0f; // no change in gamma

            float adjustedBrightness = brightness - 1.0f;
            // create matrix that will brighten and contrast the image
            float[][] ptsArray ={
            new float[] {contrast, 0, 0, 0, 0}, // scale red
            new float[] {0, contrast, 0, 0, 0}, // scale green
            new float[] {0, 0, contrast, 0, 0}, // scale blue
            new float[] {0, 0, 0, 1.0f, 0}, // don't scale alpha
            new float[] {adjustedBrightness, adjustedBrightness, adjustedBrightness, 0, 1}};

            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.ClearColorMatrix();
            imageAttributes.SetColorMatrix(new ColorMatrix(ptsArray), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);

            //get a graphics object from the image so we can draw on it
            using (Graphics graphics = Graphics.FromImage(finalImage))
            {
                //set background color
                //graphics.Clear(Color.Black);

                int x = 0;
                int y = 0;

                for (int j = 0; j < img.Height; j++)
                {
                    //string tmpRow = "";
                    for (int i = 0; i < img.Width; i++)
                    {
                        Color pixel = img.GetPixel(i, j);

                        int r = pixel.R;
                        int g = pixel.G;
                        int b = pixel.B;

                        x = i * 2;
                        y = j * 2;


                        //if ((r < 100) && (g < 100) && (b < 100))
                        //{
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x, y, goldenPixel.Width, goldenPixel.Height));
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x+1, y, goldenPixel.Width, goldenPixel.Height));
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x, y+1, goldenPixel.Width, goldenPixel.Height));
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x+1, y+1, goldenPixel.Width, goldenPixel.Height));
                        //}
                        //else
                        //{
                        //    graphics.DrawImage(blackPixel, new Rectangle(x, y, blackPixel.Width, blackPixel.Height));
                        //    graphics.DrawImage(blackPixel, new Rectangle(x+1, y, blackPixel.Width, blackPixel.Height));
                        //    graphics.DrawImage(blackPixel, new Rectangle(x, y+1, blackPixel.Width, blackPixel.Height));
                        //    graphics.DrawImage(blackPixel, new Rectangle(x+1, y+1, blackPixel.Width, blackPixel.Height));
                        //}


                        if ((r < 100) && (g < 100) && (b < 100))
                        {

                            graphics.DrawImage(goldenPixel, new Rectangle(x, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                        }
                        else
                        {
                            graphics.DrawImage(blackPixel, new Rectangle(x, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                        }



                        //if ((r < 100) && (g < 100) && (b < 100))
                        //    graphics.DrawImage(goldenPixel, new Rectangle(i*2, j*2, goldenPixel.Width, goldenPixel.Height));
                        //else
                        //    graphics.DrawImage(blackPixel, new Rectangle(i*2, j*2, blackPixel.Width, blackPixel.Height));

                        //tmpRow += "<img src='../images/dot_off1.png'/>";
                        //tmpRow += "<div class='OFFCell'></div>";
                        //tmpRow += "<img src='../images/dot_off1.png'/>";
                    }
                }
            }
            ImageConverter converter = new ImageConverter();
            byte[] imgBytes = (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
            string base64String = Convert.ToBase64String(imgBytes, 0, imgBytes.Length);
            return "<img src='data:image/png;base64," + base64String + "'/>";
            //return finalImage.;
        }


        public static string ConvertBase64toPicture(string base64Image)
        {
            if (string.IsNullOrEmpty(base64Image)) return string.Empty;

            string Image = base64Image;

            string grayScaleImage = string.Empty;
            Bitmap img = Base64StringToBitmap(Image);


            for (int j = 0; j < img.Height; j++)
            {
                string tmpRow = "";
                for (int i = 0; i < img.Width; i++)
                {
                    Color pixel = img.GetPixel(i, j);
                   

                    int r = pixel.R;
                    int g = pixel.G;
                    int b = pixel.B;

                    if ((r < 100) && (g < 100) && (b < 100))
                        tmpRow += "<img src='../images/dot_on1.png'/>";
                    else
                        tmpRow += "<img src='../images/dot_off1.png'/>";
                        //tmpRow += "<div class='OFFCell'></div>";
                        //tmpRow += "<img src='../images/dot_off1.png'/>";
                }
                grayScaleImage += string.Format("<div class='PixelRow'>{0}</div>", tmpRow);
            }
            return grayScaleImage;
        }

        public static byte[] BlackAndWhite(string base64Image)
        {
            Bitmap img = Base64StringToBitmap(base64Image);
            for (int j = 0; j < img.Height; j++)
            {
                string tmpRow = "";
                for (int i = 0; i < img.Width; i++)
                {
                    Color pixel = img.GetPixel(i, j);

                    int r = pixel.R;
                    int g = pixel.G;
                    int b = pixel.B;

                    if ((r < 100) && (g < 100) && (b < 100))
                        img.SetPixel(i, j, System.Drawing.Color.Black);
                    else
                        img.SetPixel(i, j, System.Drawing.Color.White);
                }
            }
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        public static string ConvertBase64toPicture1(string base64Image)
        {
            string Image = base64Image;

            string grayScaleImage = string.Empty;
            Bitmap img = Base64StringToBitmap(Image);

            for (int j = 0; j < img.Height; j++)
            {
                string tmpRow = "";
                for (int i = 0; i < img.Width; i++)
                {
                    Color pixel = img.GetPixel(i, j);

                    int r = pixel.R;
                    int g = pixel.G;
                    int b = pixel.B;

                    if ((r < 100) && (g < 100) && (b < 100))
                        tmpRow += "<img src='../images/dot_on.png'/>";
                    else
                        tmpRow += "<img src='../images/dot_off.png'/>";
                }
                grayScaleImage += string.Format("<div class='PixelRow1'>{0}</div>", tmpRow);
            }
            return grayScaleImage;
        }
        public static bool IsTrue(string value)
        {
            try
            {
                // 1
                // Avoid exceptions
                if (value == null)
                {
                    return false;
                }

                // 2
                // Remove whitespace from string
                value = value.Trim();

                // 3
                // Lowercase the string
                value = value.ToLower();

                // 4
                // Check for word true
                if (value == "true")
                {
                    return true;
                }

                // 5
                // Check for letter true
                if (value == "t")
                {
                    return true;
                }

                // 6
                // Check for one
                if (value == "1")
                {
                    return true;
                }

                // 7
                // Check for word yes
                if (value == "yes")
                {
                    return true;
                }

                // 8
                // Check for letter yes
                if (value == "y")
                {
                    return true;
                }

                // 9
                // It is false
                return false;
            }
            catch
            {
                return false;
            }
        }

#endregion



        public static object GetFormattedGPS(string lat, string lon)
        {
            if (lat != "" && lon != "")
                return lat + "; " + lon;

            return "";
        }

        public static string IsNullStr(object obj, string defaultVal)
        {
            if (obj == null)
                return defaultVal;
            else
                return obj.ToString();
        }
    }
}