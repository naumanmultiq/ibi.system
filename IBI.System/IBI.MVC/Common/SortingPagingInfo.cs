﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.Web.Common
{
    public class SortingPagingInfo
    {
        public string SearchKeyword { get; set; }
        public bool SearchFilterA { get; set; }
        public bool SearchFilterR { get; set; }
        public bool SearchFilterU { get; set; }
        public string SortField { get; set; }
        public string SortDirection { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int CurrentPageIndex { get; set; }

        public SortingPagingInfo() {            
            PageSize = 30;
            PageCount = 0;
            CurrentPageIndex = 0;
        }
    }
}