﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace IBI.Web.Common
{
    public static class Settings
    {
        public static String AppPath
        {
            get
            {
                return ConfigurationManager.AppSettings["AppPath"].ToString();
            }
        }

        public static String ForcedStartupUrl
        {
            get
            {
                var lastPage = CurrentUser.UserActivityConfigurations.Where(uac => uac.Key == "LastPage").FirstOrDefault();
                if (!string.IsNullOrEmpty(lastPage.Value))
                {
                    return lastPage.Value.TrimStart('/');
                }
                else
                {
                    return ConfigurationManager.AppSettings["ForcedStartupUrl"].ToString();
                }
            }
        }


        public static String RestPath
        {
            get
            {
                return ConfigurationManager.AppSettings["REST_PATH"].ToString();
            }
        }

        public static String DatePickerFormat
        {
            get
            {
                return ConfigurationManager.AppSettings["DatePickerFormat"].ToString();
            }
        }

        public static String DefaultCulture
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultCulture"].ToString();
            }
        }

        public static String JqueryCulture
        {
            get
            {
                return ConfigurationManager.AppSettings["JqueryCulture"].ToString();
            }
        }


        public static String RestApiKey
        {
            get
            {
                return ConfigurationManager.AppSettings["RestApiKey"].ToString();
            }
        }

        public static String DateTimeFormat
        {
            get
            {
                return ConfigurationManager.AppSettings["DateTimeFormat"].ToString();
            }
        }

        public static int DefaultCustomerId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["DefaultCustomerId"].ToString());
            }
        }



        public static bool LogsEnabled
        {
            get
            {
                return ConfigurationManager.AppSettings["LogsEnabled"].ToString().ToLower() == "true" ? true : false;
            }
        }

    }
}