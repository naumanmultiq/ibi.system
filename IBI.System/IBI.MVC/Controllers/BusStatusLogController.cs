﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;
using IBI.Web.Infrastructure;
using IBI.Web.ViewModels.BusStatusLog;
using IBI.Web.Infrastructure.CustomAttributes;

namespace IBI.Web.Controllers
{
    
    [AuthorizeLogin]
    public class BusStatusLogController : Controller
    { 
         
        public ActionResult Index(
            int? busNumber,
            GridSortOptions gridSortOptions,
            int? page)
        {

            var list = ServiceManager.GetBusStatusLog();

            // Set default sort column
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "TimeStamp";
                gridSortOptions.Direction = SortDirection.Descending;
            }


            // Filter on subject 
            if (busNumber != null)
            {
                list = list.Where(a => a.BusNumber == busNumber).ToList();
            }


            // Create all filter data and set current values if any
            // These values will be used to set the state of the select list and textbox
            // by sending it back to the view.
            var filterViewModel = new FilterViewModel();
            filterViewModel.BusNumber = busNumber;
            filterViewModel.Fill();

            // Order and page the product list
            var pagedList = list
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                   .AsPagination(page ?? 1, 20);


            var listContainer = new ListContainerViewModel
            {
                PagedList = pagedList,
                FilterViewModel = filterViewModel,
                GridSortOptions = gridSortOptions
            };

            return View(listContainer);
        }

    }
}
