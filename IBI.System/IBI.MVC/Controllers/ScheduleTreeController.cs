﻿using IBI.Shared.Diagnostics;
using IBI.Web.Infrastructure;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IBI.Web.Controllers
{
    public class ScheduleTreeController : Controller
    {
        //
        // GET: /ScheduleTree/


        public ActionResult ScheduleStops(int scheduleId = 0)
        {
            ViewData["ScheduleId"] = scheduleId;
            var model = ServiceManager.GetSchedule(scheduleId);
            return View(model);
        }

        public ActionResult EditSchedule(int scheduleId = 0)
        {
            var model = ServiceManager.GetSchedule(scheduleId);
            return View(model);
        }

        [HttpPost]
        public JsonResult EditSchedule(int scheduleId, int customerId, string line, string fromName, string destination, string viaName, string forcedOption)
        {

            //checks 

            if (scheduleId <= 0 || customerId <= 0)
            {
                return Json(new { result = "Failure", msg = "Invalid Schedule" }, JsonRequestBehavior.AllowGet);
            }

            //if (String.IsNullOrEmpty(line))
            //{
            //    return Json(new { result = "Failure", msg = "Line cannot be empty" }, JsonRequestBehavior.AllowGet);
            //}

            //if (String.IsNullOrEmpty(fromName))
            //{
            //    return Json(new { result = "Failure", msg = "From name cannot be empty" }, JsonRequestBehavior.AllowGet);
            //}

            if (String.IsNullOrEmpty(destination))
            {
                return Json(new { result = "Failure", msg = "Destination cannot be empty" }, JsonRequestBehavior.AllowGet);
            }


            //bool alreadyExists = ServiceManager.CheckExistingSchedule(model.ScheduleId, model.CustomerId, model.Line, model.FromName, model.Destination, model.ViaName);

            //if (alreadyExists)
            //{
            //    return Json(new { result = "Failure", msg = "A similar schedule with same data already exists, you cannot add duplicate schedules" }, JsonRequestBehavior.AllowGet);
            //}


            IBI.Shared.Models.ScheduleTreeItems.Schedule schedule = new Shared.Models.ScheduleTreeItems.Schedule
            {
                CustomerId = customerId,
                ScheduleId = scheduleId,
                Line = line,
                FromName = fromName,
                Destination = destination,
                ViaName = viaName,
                ScheduleStops = new List<Shared.Models.ScheduleTreeItems.ScheduleStop>()
            };

            scheduleId = ServiceManager.UpdateSchedule(schedule, forcedOption);

            if (scheduleId < 0)
            {
                return Json(new { result = "Failure|Duplicate", msg = "A similar schedule with same data already exists, you cannot add duplicate schedules" }, JsonRequestBehavior.AllowGet);
            }

            if (scheduleId > 0)
            {
                return Json(new { result = "Success", msg = "Schedule updated successfully" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "Failure", msg = "Failed to update this schedule at this time, please try later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSchedule(int scheduleId)
        {

            if (ServiceManager.DeleteSchedule(scheduleId))
            {
                return Json(new { result = "Success", msg = "Schedule deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "Failure", msg = "Cannot delete this schedule, It is being used by some signs or journeys" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult UploadSchedule(int customerId = 0)
        {
            ViewData["CustomerId"] = customerId;

            return View();
        }

        [HttpPost]
        public ActionResult UploadSchedule(object fl)
        {
            try
            {

                if (Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = Request.Files["UploadedCSV"];
                    int customerId = int.Parse(Request["customerId"]);
                    string forcedOption = Request["forcedOption"];

                    if (httpPostedFile != null)
                    {
                        // Validate
                        if (!httpPostedFile.FileName.ToLower().EndsWith(".csv"))
                            return Json("WrongFileType", JsonRequestBehavior.AllowGet);

                        // Get the complete file path
                        //var fileSavePath = Path.Combine(Server.MapPath("../Temp"), httpPostedFile.FileName);

                        // Save the uploaded file to "UploadedFiles" folder
                        //httpPostedFile.SaveAs(fileSavePath);

                        //read csv file contents


                        StringBuilder result = new StringBuilder();

                        if (Request.InputStream.Length > 0)
                        {

                            byte[] contents = ReadToEnd(httpPostedFile.InputStream);

                            string data = System.Text.Encoding.UTF8.GetString(contents);

                            using (TextReader reader = new StringReader(data))
                            {
                                //LumenWorks.Framework.IO.Csv.CsvReader csv = new CsvReader(reader, true, ',','"', '~', '~', ValueTrimmingOptions.All);
                                LumenWorks.Framework.IO.Csv.CsvReader csv = new CsvReader(reader, true, ',', '"', '~', '/', ValueTrimmingOptions.None);



                                int fieldCount = csv.FieldCount;


                                List<ViewModels.Admin.Schedules.CsvScheduleStop> stops = new List<ViewModels.Admin.Schedules.CsvScheduleStop>();

                                IBI.Shared.Models.ScheduleTreeItems.Schedule schedule = new Shared.Models.ScheduleTreeItems.Schedule();

                                string[] headers = csv.GetFieldHeaders();
                                int rowNum = 0;

                                ScheduleService.ScheduleServiceClient scheduleClient = new ScheduleService.ScheduleServiceClient();

                                try
                                {

                                    while (csv.ReadNextRecord())
                                    {

                                        if (rowNum == 0)
                                        {
                                            schedule.CustomerId = customerId;
                                            schedule.Line = csv[1] == "NULL" ? "" : csv[1];
                                            schedule.FromName = csv[2] == "NULL" ? "" : csv[2];
                                            schedule.Destination = csv[3] == "NULL" ? "" : csv[3];
                                            schedule.ViaName = csv[4] == "NULL" ? "" : csv[4];
                                            schedule.ScheduleStops = new List<Shared.Models.ScheduleTreeItems.ScheduleStop>();
                                            schedule.LastUpdated = DateTime.Now;
                                        }


                                        schedule.ScheduleStops.Add(new Shared.Models.ScheduleTreeItems.ScheduleStop
                                        {
                                            StopSequence = int.Parse(csv[0]),
                                            StopId = decimal.Parse(csv[5]),
                                            StopName = csv[6],
                                            Longitude = csv[7].Replace(",", "."),
                                            Latitude = csv[8].Replace(",", "."),
                                            Zone = csv[9] == "NULL" ? "" : csv[9]
                                        });


                                        rowNum++;
                                    }

                                    //adding schedule to DB
                                    //TO DO :[KHI] 
                                    int scheduleId = ServiceManager.SaveSchedule(schedule, forcedOption);


                                    ///  > 0 : new schedule created
                                    ///  0 : error, no schedule saved
                                    /// -1 : schedule already exists - no changes made (may not occur)
                                    /// -2 : schedule already exists - with changes
                                    /// -3 : schedule already exists - no changes
                                    /// -4 : schedule already exists - updated successfully                                    

                                    return Json(scheduleId, JsonRequestBehavior.AllowGet);

                                }
                                catch (Exception ex)
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, ex);
                                    return Json("Failure", JsonRequestBehavior.AllowGet);
                                }



                            }


                        }

                    }

                    return Json("Failure", JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json("Unknown Server Error", JsonRequestBehavior.AllowGet);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);

            return View();
        }



        public FileResult DownloadSchedule(int scheduleId)
        {

            try
            {
                char separator = ',';

                var schedule = ServiceManager.GetSchedule(scheduleId);

                string sb = string.Empty; // new StringBuilder();

                sb += "StopSequence,Line,FromName,Destination,ViaName,StopId,StopName,Longitude,Latitude,Zone";

                foreach (var stop in schedule.ScheduleStops)
                {
                    sb += Environment.NewLine;
                    sb += string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}"
                        , stop.StopSequence
                        , (!string.IsNullOrEmpty(schedule.Line) && schedule.Line.Contains(separator)) ? string.Format("\"{0}\"", schedule.Line) : schedule.Line
                        , (!string.IsNullOrEmpty(schedule.FromName) && schedule.FromName.Contains(separator)) ? string.Format("\"{0}\"", schedule.FromName) : schedule.FromName
                        , (!string.IsNullOrEmpty(schedule.Destination) && schedule.Destination.Contains(separator)) ? string.Format("\"{0}\"", schedule.Destination) : schedule.Destination
                        , (!string.IsNullOrEmpty(schedule.ViaName) && schedule.ViaName.Contains(separator)) ? string.Format("\"{0}\"", schedule.ViaName) : schedule.ViaName
                        , stop.StopId
                        , (!string.IsNullOrEmpty(stop.StopName) && stop.StopName.Contains(separator)) ? string.Format("\"{0}\"", stop.StopName) : stop.StopName
                        , (!string.IsNullOrEmpty(stop.Longitude) && stop.Longitude.Contains(separator)) ? string.Format("\"{0}\"", stop.Longitude) : stop.Longitude
                        , (!string.IsNullOrEmpty(stop.Latitude) && stop.Latitude.Contains(separator)) ? string.Format("\"{0}\"", stop.Latitude) : stop.Latitude
                        , (!string.IsNullOrEmpty(stop.Zone) && stop.Zone.Contains(separator)) ? string.Format("\"{0}\"", stop.Zone) : stop.Zone
                        );
                }

                //create CSV Schedule                

                byte[] fileContent = System.Text.Encoding.UTF8.GetBytes(sb); //IBI.Shared.AppUtility.Text2Bytes(sb);
                return File(fileContent, "text/csv", string.Format("Schedule_{0}.csv", scheduleId));
            }
            catch (Exception ex)
            {
                byte[] fileContent = new byte[] { };
                return File(fileContent, "text/csv", string.Format("Schedule_{0}.csv", scheduleId));
            }
        }


        [HttpPost]
        public JsonResult UpdateScheduleStops(int scheduleId, List<IBI.Shared.Models.ScheduleTreeItems.ScheduleStop> stops)
        {

            IBI.Shared.Models.ScheduleTreeItems.Schedule schedule = ServiceManager.GetSchedule(scheduleId);
            schedule.ScheduleStops.Clear();
            schedule.ScheduleStops = stops;

            int id = ServiceManager.UpdateScheduleStops(schedule);

            if (id == -1)
            {
                return Json(new { result = "Failure", msg = "Failed to update this schedule, A duplicate schedule might exists" }, JsonRequestBehavior.AllowGet);
            }

            if (id > 0)
            {
                return Json(new { result = "Success", msg = "Stops updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "Failure", msg = "Failed to update stops, there is some issue in provided data. Please fix it and try again" }, JsonRequestBehavior.AllowGet);
            }
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        [HttpGet]
        public JsonResult TestData()
        {
            string data = System.IO.File.ReadAllText(Server.MapPath("~/App_Data/data.json"));

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public FileResult GetCsv(string fileName)
        //{
        //    try
        //    {
        //        string targetFile = Server.MapPath("~") + "Services\\Logs\\Bank\\To\\" + DateTime.Now.ToString("yyyyMM") + "\\" + fileName;
        //        byte[] fileContent = System.IO.File.ReadAllBytes(targetFile);
        //        return File(fileContent, "application/csv", fileName);
        //    }
        //    catch (Exception ex)
        //    {
        //        string targetFile = Server.MapPath("~") + "\\Content\\temp.csv";
        //        byte[] fileContent = System.IO.File.ReadAllBytes(targetFile);
        //        return File(fileContent, "application/csv", fileName);
        //    }
        //}


    }
}
