﻿using IBI.Shared.Models.Destinations;
using IBI.Shared.Models.ScheduleTreeItems;
using IBI.Shared.Models.SignAddons;
using IBI.Shared.Models.Signs;
using IBI.Shared.Models;
using IBI.Web.Common;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.Logger;
using IBI.Web.ViewModels.Admin.Destinations;
using IBI.Web.ViewModels.Admin.SignAddons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IBI.Web.Controllers
{
    [AuthorizeLogin]
    public class AdminController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }



        #region Destinations

        [HttpGet]
        public ActionResult Destinations()
        {
            var model = new ViewModels.Admin.Destinations.SignModel(0);
            return View(model);
        }

        [HttpPost]
        public ActionResult Destinations(int? customerId, bool? status)
        {
            var model = new ViewModels.Admin.Destinations.SignModel(customerId??0);
            return View(model);
        }


        [HttpGet]
        public ActionResult GetSignTree(string custAndType)
        {
            //DestinationTree tree = SessionManager.Get<DestinationTree>("DestinationTree");

            string[] arr = custAndType.Split('|');
            int customerId = int.Parse(arr[0]);
            int type = int.Parse(arr[1]);
            bool showInactive = false;
                
            if(arr.Length>2)
            {
                showInactive = arr[2] == "1" ? true : false; 
            }

            if (customerId > 0)
            {
                SignTree tree = ServiceManager.GetSignTree(new SignTree { CustomerID = customerId, UserID = Common.CurrentUser.UserId }, showInactive, true);

                SessionManager.Store("SignTree", tree);

                if (tree != null)
                {

                    if (type==0)
                        return Json(tree.Signs[0].children, JsonRequestBehavior.AllowGet);
                    else
                        return Json(tree.Signs[1].children, JsonRequestBehavior.AllowGet);

                }
            }

            return Json(new List<Sign>(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSpecialSignTree(int customerId)
        {
            Sign dest = ServiceManager.GetSignGroups(customerId, Common.CurrentUser.UserId);

            return Json(dest.children, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddEditDestination(int customerId, int signId)
        {
            signId = (signId == null || signId <= 0) ? -1 : signId;

            AddEditSignModel model;
                        
            model = new AddEditSignModel(customerId,signId);
           
            return View(model);
        }


        [HttpPost]
        public JsonResult AddEditDestination(AddEditSignModel model)
        {  

            model.Sign.Line = model.Sign.Line ?? "";
            model.Sign.Name = model.Sign.Name ?? "";
            model.Sign.MainText = model.Sign.MainText ?? "";
            model.Sign.SubText = model.Sign.SubText ?? "";


            Sign sign = new Sign();
            if(model.Sign.SignId>0)
            {
                sign = ServiceManager.GetSignItem(model.Sign.SignId);
            }


            sign.CustomerId = model.CustomerId;
            sign.Line= model.Sign.Line;
            sign.MainText = model.Sign.MainText;
            sign.SubText = model.Sign.SubText;
            sign.Name = model.Sign.Name;
            sign.IsActive = model.Sign.IsActive;
            sign.GroupId = model.Sign.GroupId;
            sign.ParentGroupId = model.Sign.ParentGroupId ?? null;
            sign.IsActive = model.Sign.IsActive;
            sign.IsGroup = model.Sign.IsGroup;
            sign.Priority = model.Sign.Priority;
            
            sign.ScheduleId = model.Sign.ScheduleId ?? null;
            
            sign.SetNode();

            
            model.LoadAfterSave = true;

            //checks
            if (String.IsNullOrEmpty(sign.Name)) 
            {
                model.PostResult =  "false";
                model.ErrorMessage = "Sign name cannot be empty";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            if (String.IsNullOrEmpty(sign.MainText))
            {
                model.PostResult = "false";
                model.ErrorMessage = "Main Text cannot be empty";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            if (sign.ParentGroupId == null)
            {
                model.PostResult = "false";
                model.ErrorMessage = "You must select a parent group node";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            if (sign.ParentGroupId == 10000001) // trying to add SignItem at Special Tree's Root
            {
                sign.ParentGroupId = null;
            }

            if(sign.ParentGroupId == 10000002) // trying to add SignItem at Line Items Tree's Root
            {
                model.PostResult = "false";
                model.ErrorMessage = "You cannot put signs at the root level of 'Line selection buttons', please select a sub group";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

             
            Sign existingSign = ServiceManager.FindSignItem(sign.CustomerId, sign.ParentGroupId, sign.Line, sign.Name, sign.MainText, sign.SubText);

                bool existScheduleId = ServiceManager.FindScheduleId(sign.ScheduleId??0);

            if (sign.ScheduleId!=null && sign.ScheduleId!=0 && !existScheduleId)
            {
                model.PostResult = "false";
                model.ErrorMessage = "Failed to save. Invalid Schedule Id";
            
            } 
            else if (existingSign != null && existingSign.SignId!=sign.SignId)
            {
                //already similar destination exist at that level
                model.PostResult = "false";
                model.ErrorMessage = "Failed to save. Destination already exists at that level";
                //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");
                
            }
            else 
            { 
            
                bool result = ServiceManager.SaveSign(sign);

                model.Sign = sign;

                model.PostResult = result ? "true" : "false";
                //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult DeleteSign(int signId) {
             
            return Json(ServiceManager.DeleteSign(signId), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult DestinationImages(string id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddEditGroup(int customerId, int groupId)
        {
            groupId = (groupId == null || groupId <= 0) ? -1 : groupId;
            AddEditGroupModel model;            
            model = new AddEditGroupModel(customerId, groupId);
             
            return View(model);
        }


        [HttpPost]
        public JsonResult AddEditGroup(AddEditGroupModel model)
        {

            model.Group.GroupName = model.Group.GroupName ?? "";


            Group group = new Group();
            if (model.Group.GroupId > 0)
            {
                group = ServiceManager.GetSignGroup(model.Group.GroupId);
            }


            group.CustomerId = model.CustomerId;
            group.GroupName = model.Group.GroupName;
            group.Excluded  = model.Group.Excluded;
            group.ParentGroupId = model.Group.ParentGroupId ?? null;
            group.SortValue  = model.SortValue;

            model.LoadAfterSave = true;

            //checks
            if (String.IsNullOrEmpty(group.GroupName))
            {
                model.PostResult = "false";
                model.ErrorMessage = "Group name cannot be empty";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            if (group.ParentGroupId==null)
            {
                model.PostResult = "false";
                model.ErrorMessage = "You must select a parent group node";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            if (group.ParentGroupId == 10000001)
            {
                group.ParentGroupId = null;
                group.GroupName = group.GroupName.Replace("#", "");
            }

            if (group.ParentGroupId == 10000002)
            {
                group.ParentGroupId = null;
                group.GroupName = "#" + group.GroupName.Replace("#", "");
            }
             
            Group existingSignGroup = ServiceManager.FindSignGroup(group.CustomerId, group.ParentGroupId, group.GroupName);


            if (group.GroupId == group.ParentGroupId || (existingSignGroup != null && existingSignGroup.GroupId != group.GroupId))
            {
                //already similar destination exist at that level
                model.PostResult = "false";
                model.ErrorMessage = "Failed to save. Group already exists at that level";
                //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");

            }
            else
            {

                bool result = ServiceManager.SaveSignGroup(group,model.OldParentId);

                model.Group = group;

                model.PostResult = result ? "true" : "false";
                //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteSignGroup(int groupId)
        {
            return Json(ServiceManager.DeleteSignGroup(groupId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SortSignNode(int groupId, int signId, int sortValue, int lastGroupId, int lastSignId, int lastSortValue)
        {
            bool retVal = false;

            if (signId != null && signId > 0)
            {
                Sign sign = ServiceManager.GetSignItem(signId);

                if (sign != null)
                {
                    sign.Priority = sortValue;

                    retVal = ServiceManager.SaveSign(sign);
                }

            }

            if (groupId != null && groupId > 0)
            {
                Group group = ServiceManager.GetSignGroup(groupId);

                if (group != null)
                {
                    group.SortValue = sortValue;

                    retVal = ServiceManager.SaveSignGroup(group);
                }

            }

            if (lastSignId != null && lastSignId > 0 && retVal == true)
            {
                Sign lastSign = ServiceManager.GetSignItem(lastSignId);

                if (lastSign != null)
                {
                    lastSign.Priority = lastSortValue;

                    retVal = ServiceManager.SaveSign(lastSign);
                }
            }

            if (lastGroupId != null && lastGroupId > 0 && retVal == true)
            {
                Group lastGroup = ServiceManager.GetSignGroup(lastGroupId);

                if (lastGroup != null)
                {
                    lastGroup.SortValue = lastSortValue;

                    retVal = ServiceManager.SaveSignGroup(lastGroup);
                }
            }

            return Json(retVal, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region SignAddons


        [HttpGet]
        public ActionResult SignAddons()
        {
            var model = new ViewModels.Admin.SignAddons.SignAddonModel(0);
            return View(model);
        }

        [HttpPost]
        public ActionResult SignAddons(int? customerId, bool? status)
        {
            var model = new ViewModels.Admin.SignAddons.SignAddonModel(customerId ?? 0);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetSignAddonTree(string custAndType)
        {
            //DestinationTree tree = SessionManager.Get<DestinationTree>("DestinationTree");

            string[] arr = custAndType.Split('|');
            int customerId = int.Parse(arr[0]);
            int type = int.Parse(arr[1]);
            bool showInactive = false;

            if (arr.Length > 2)
            {
                showInactive = arr[2] == "1" ? true : false;
            }

            if (customerId > 0)
            {
                SignAddonTree tree = ServiceManager.GetSignAddonTree(new SignAddonTree { CustomerID = customerId, UserID = Common.CurrentUser.UserId }, showInactive, true);

                SessionManager.Store("SignAddonTree", tree);

                if (tree != null)
                {

                    return Json(tree.SignAddons[0].children, JsonRequestBehavior.AllowGet);

                }
            }

            return Json(new List<Sign>(), JsonRequestBehavior.AllowGet);
        }
         

        [HttpGet]
        public ActionResult AddEditSignAddon(int customerId, int signAddonItemId)
        {
            signAddonItemId = (signAddonItemId == null || signAddonItemId <= 0) ? -1 : signAddonItemId;

            AddEditSignAddonModel model;

            model = new AddEditSignAddonModel(customerId, signAddonItemId);

            return View(model);
        }


        [HttpPost]
        public JsonResult AddEditSignAddon(AddEditSignAddonModel model)
        {
             
            model.SignAddon.Name = model.SignAddon.Name ?? "";
            model.SignAddon.SignText = model.SignAddon.SignText ?? "";
            model.SignAddon.UdpText = model.SignAddon.UdpText ?? "";


            SignAddon signAddon = new SignAddon();
            if (model.SignAddon.SignAddonItemId > 0)
            {
                signAddon = ServiceManager.GetSignAddonItem(model.SignAddon.SignAddonItemId);
            }


            signAddon.CustomerId = model.CustomerId;

            signAddon.SignText = model.SignAddon.SignText;
            signAddon.UdpText = model.SignAddon.UdpText;
            signAddon.Name = model.SignAddon.Name;
            signAddon.IsActive = model.SignAddon.IsActive; 
            signAddon.IsActive = model.SignAddon.IsActive; 
            signAddon.Priority = model.SignAddon.Priority;
              
            signAddon.SetNode();


            model.LoadAfterSave = true;

            //checks
            if (String.IsNullOrEmpty(signAddon.Name))
            {
                model.PostResult = "false";
                model.ErrorMessage = "Sign Addon name cannot be empty";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            if (String.IsNullOrEmpty(signAddon.SignText))
            {
                model.PostResult = "false";
                model.ErrorMessage = "Sign Text cannot be empty";
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            //if (String.IsNullOrEmpty(signAddon.UdpText))
            //{
            //    model.PostResult = "false";
            //    model.ErrorMessage = "Udp Text cannot be empty";
            //    return Json(model, JsonRequestBehavior.AllowGet);
            //}

            SignAddon existingSignAddon = ServiceManager.FindSignAddonItem(signAddon.CustomerId, signAddon.ParentGroupId, signAddon.Line, signAddon.Name, signAddon.SignText, "");

            
            if (existingSignAddon != null && existingSignAddon.SignAddonItemId != signAddon.SignAddonItemId)
            {
                //already similar destination exist at that level
                model.PostResult = "false";
                model.ErrorMessage = "Failed to save. This addon already exists for this customer";
                //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");

            }
            else
            {

                bool result = ServiceManager.SaveSignAddon(signAddon);

                model.SignAddon = signAddon;

                model.PostResult = result ? "true" : "false";
                //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult DeleteSignAddon(int signAddonItemId)
        {

            return Json(ServiceManager.DeleteSignAddon(signAddonItemId), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult SignAddonImages(string id)
        {
            return View();
        }

      
        [HttpGet]
        public JsonResult SortSignAddonNode(int groupId, int signAddonId, int sortValue, int lastGroupId, int lastSignAddonId, int lastSortValue)
        {
            bool retVal = false;

            if (signAddonId != null && signAddonId > 0)
            {
                SignAddon signAddon = ServiceManager.GetSignAddonItem(signAddonId);

                if (signAddon != null)
                {
                    signAddon.Priority = sortValue;

                    retVal = ServiceManager.SaveSignAddon(signAddon);
                }

            }

              
            if (lastSignAddonId != null && lastSignAddonId > 0 && retVal == true)
            {
                SignAddon lastSignAddon = ServiceManager.GetSignAddonItem(lastSignAddonId);

                if (lastSignAddon != null)
                {
                    lastSignAddon.Priority = lastSortValue;

                    retVal = ServiceManager.SaveSignAddon(lastSignAddon);
                }
            }
             

            return Json(retVal, JsonRequestBehavior.AllowGet);
        }


        #endregion


        #region Schedules

        [HttpGet]
        public ActionResult Schedules()
        {
            var model = new ViewModels.Admin.Schedules.ScheduleModel(0);
            return View(model);
        }

        [HttpPost]
        public ActionResult Schedules(int? customerId, bool? status)
        {
            var model = new ViewModels.Admin.Schedules.ScheduleModel(customerId ?? 0);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetScheduleTree(string custAndType)
        {
            //DestinationTree tree = SessionManager.Get<DestinationTree>("DestinationTree");
            string[] arr = custAndType.Split('|');
            int customerId = int.Parse(arr[0]);
            int type = int.Parse(arr[1]);
            bool showInactive = false;

            if (arr.Length > 2)
            {
                showInactive = arr[2] == "1" ? true : false;
            }

            if (customerId > 0)
            {
                ScheduleTree tree = ServiceManager.GetScheduleTree(customerId, Common.CurrentUser.UserId);

                SessionManager.Store("ScheduleTree", tree);

                if (tree != null)
                {

                    //return Json(tree.Schedules[0].children, JsonRequestBehavior.AllowGet);
                    var schedules = tree.Schedules[0].children;
                    var jsonResult = Json(schedules, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;

                }
            }

            return Json(new List<Sign>(), JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public ActionResult ScheduleStops(int? scheduleId)
        {
            var model = new ViewModels.Admin.Schedules.ScheduleModel(scheduleId ?? 0);
            return View(model);
        }
        //[HttpGet]
        //public ActionResult AddEditSchedule(int customerId, int signAddonItemId)
        //{
        //    signAddonItemId = (signAddonItemId == null || signAddonItemId <= 0) ? -1 : signAddonItemId;

        //    AddEditSignAddonModel model;

        //    model = new AddEditSignAddonModel(customerId, signAddonItemId);

        //    return View(model);
        //}


        //[HttpPost]
        //public JsonResult AddEditSchedule(AddEditScheduleModel model)
        //{

        //    model.SignAddon.Name = model.SignAddon.Name ?? "";
        //    model.SignAddon.SignText = model.SignAddon.SignText ?? "";
        //    model.SignAddon.UdpText = model.SignAddon.UdpText ?? "";


        //    SignAddon signAddon = new SignAddon();
        //    if (model.SignAddon.SignAddonItemId > 0)
        //    {
        //        signAddon = ServiceManager.GetSignAddonItem(model.SignAddon.SignAddonItemId);
        //    }


        //    signAddon.CustomerId = model.CustomerId;

        //    signAddon.SignText = model.SignAddon.SignText;
        //    signAddon.UdpText = model.SignAddon.UdpText;
        //    signAddon.Name = model.SignAddon.Name;
        //    signAddon.IsActive = model.SignAddon.IsActive;
        //    signAddon.IsActive = model.SignAddon.IsActive;
        //    signAddon.Priority = model.SignAddon.Priority;

        //    signAddon.SetNode();


        //    model.LoadAfterSave = true;

        //    //checks
        //    if (String.IsNullOrEmpty(signAddon.Name))
        //    {
        //        model.PostResult = "false";
        //        model.ErrorMessage = "Sign Addon name cannot be empty";
        //        return Json(model, JsonRequestBehavior.AllowGet);
        //    }

        //    if (String.IsNullOrEmpty(signAddon.SignText))
        //    {
        //        model.PostResult = "false";
        //        model.ErrorMessage = "Sign Text cannot be empty";
        //        return Json(model, JsonRequestBehavior.AllowGet);
        //    }

        //    //if (String.IsNullOrEmpty(signAddon.UdpText))
        //    //{
        //    //    model.PostResult = "false";
        //    //    model.ErrorMessage = "Udp Text cannot be empty";
        //    //    return Json(model, JsonRequestBehavior.AllowGet);
        //    //}

        //    SignAddon existingSignAddon = ServiceManager.FindSignAddonItem(signAddon.CustomerId, signAddon.ParentGroupId, signAddon.Line, signAddon.Name, signAddon.SignText, "");


        //    if (existingSignAddon != null && existingSignAddon.SignAddonItemId != signAddon.SignAddonItemId)
        //    {
        //        //already similar destination exist at that level
        //        model.PostResult = "false";
        //        model.ErrorMessage = "Failed to save. This addon already exists for this customer";
        //        //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");

        //    }
        //    else
        //    {

        //        bool result = ServiceManager.SaveSignAddon(signAddon);

        //        model.SignAddon = signAddon;

        //        model.PostResult = result ? "true" : "false";
        //        //ModelState.AddModelError("AlreadyExists", "Destination Already exists at this level");
        //    }

        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}


        //[HttpGet]
        //public JsonResult DeleteScheudle(int scheduleId)
        //{

        //    return Json(ServiceManager.DeleteSchedule(scheduleId), JsonRequestBehavior.AllowGet);
        //}

        #endregion



        #region Client Configurations
        //[HttpGet]
        //public ActionResult Configurations()
        //{
        //    var model = new ViewModels.Admin.Configurations.SignModel(0);
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult Configurations()
        //{
        //   var model = new ViewModels.Admin.Destinations.SignModel(0);
        //    return View(model);
        //}

        [HttpGet]
        public ActionResult Configurations()
        {
            var model = new ViewModels.Admin.Configurations.ConfigurationModel();
            return View(model);
        }


        [HttpPost]
        //[Ajax(true)]
        public JsonResult SetClientConfigurations(int ConfigurationPropertyId, string PropertyValue, int GroupId, int CustomerID, string BusNumber)
        {
            ServiceResponse<bool> response = ServiceManager.SetClientConfigurations(ConfigurationPropertyId, PropertyValue, GroupId, CustomerID, BusNumber);
            return Json(response.Data, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
