﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using IBI.Shared.Models.Messages;
using IBI.Web.ViewModels;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using MvcContrib.UI.Grid;
using IBI.Web.ViewModels.Messaging;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using System.Globalization;
using IBI.Web.Infrastructure.CustomAttributes;

namespace IBI.Web.Controllers
{


    [AuthorizeLogin]
    public class MessageController : Controller
    {

        private const int OUTGOING_DEFAULT_VALIDITY_HOURS = 24;

        [HttpGet]
        public ActionResult List(
         string busNumber,
         string subject,
         int? messageCategoryID,
         int? customerId,
         GridSortOptions gridSortOptions,
         int? page,
         bool loadAfterSave = false)
        {

            if (page != null) //[KHI]: HACK to avoid Save Message dialog again.
                loadAfterSave = false;


            var messageList = ServiceManager.GetMessages(busNumber, customerId ?? 0);

            // Set default sort column
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "DateModified";
                gridSortOptions.Direction = SortDirection.Descending;
            }

            // Filter on MessageCategory 
            if (messageCategoryID.HasValue)
            {
                messageList = messageList.Where(a => a.MessageCategoryId == messageCategoryID).ToList();
            }

            // Filter on subject 
            if (!(string.IsNullOrEmpty(subject)))
            {
                messageList = messageList.Where(a => a.Subject.Contains(subject)).ToList();
            }



            // Create all filter data and set current values if any
            // These values will be used to set the state of the select list and textbox
            // by sending it back to the view.
            var filterViewModel = new FilterViewModel();
            filterViewModel.CustomerId = customerId.Value;
            filterViewModel.BusNumber = busNumber;
            filterViewModel.MessageCategoryIDFilter = messageCategoryID ?? -1;
            filterViewModel.Fill();

            // Order and page the product list
            var pagedList = messageList
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                   .AsPagination(page ?? 1, 6);


            var listContainer = new ListContainerViewModel
            {
                PagedList = pagedList,
                FilterViewModel = filterViewModel,
                GridSortOptions = gridSortOptions,
                LoadAfterSave = loadAfterSave
            };

            return View(listContainer);
        }


        //[HttpPost]
        ////[Ajax(true)]
        //public ActionResult Edit(ViewModels.Messaging.MessageEditModel model)
        //{
        //    Message msgToEdit = model.MessageToEdit;

        //    List<String> BusNumbers = IBI.Shared.AppUtility.CommaValuesToList(model.BusNumbers);

        //    if (model.CategoryChanged == "true")
        //    {
        //        model.CategoryChanged = "";



        //        int hours = OUTGOING_DEFAULT_VALIDITY_HOURS;

        //        if (model.MessageToEdit.MessageCategoryId != null)                    
        //            hours = GetCategoryValidityTime(model.MessageToEdit.CustomerId, model.MessageToEdit.MessageCategoryId.Value);


        //        model.MessageToEdit.ValidFrom = DateTime.Now;
        //        model.MessageToEdit.ValidTo = DateTime.Now.AddHours(hours);
        //        model.MessageToEdit.Buses = BusNumbers;
        //        model.MessageCategories = ViewModels.Messaging.MessageEditModel.GetMessageCategories(msgToEdit.CustomerId, msgToEdit.MessageCategoryId);


        //        ModelState.Remove("MessageToEdit.MessageCategoryId");
        //        ModelState.Remove("MessageToEdit.Subject");
        //        ModelState.Remove("MessageToEdit.ValidFrom");
        //        ModelState.Remove("MessageToEdit.ValidTo");
        //        ModelState.Remove("MessageToEdit.MessageText");

        //        //ModelState.AddModelError("isValid", "Category Changed");

        //        return View("Edit", model);
        //    }

        //    //check Valid From / Valid To. 
        //    if (ModelState.IsValid)
        //    {
        //        if (msgToEdit.SenderId == null || msgToEdit.SenderId <= 0)
        //           msgToEdit.SenderId = UserManager.CurrentUser.UserId;

        //        msgToEdit.LastModifiedBy = UserManager.CurrentUser.UserId;

        //        msgToEdit.Buses = BusNumbers; //new List<string> { msgToEdit.DefaultBus };

        //        int msgId = ServiceManager.SaveMessage(msgToEdit);
        //        ModelState.Clear();
        //        model.PostResult = "success";
        //        //return PartialView("List", new { customerId = msgToEdit.CustomerId, busNumber = msgToEdit.BusNumber });
        //        //return RedirectToAction()

        //        return RedirectToAction("List", new { customerId = msgToEdit.CustomerId, busNumber = msgToEdit.DefaultBus, loadAfterSave = true });

        //    }

        //    model.MessageCategories = ViewModels.Messaging.MessageEditModel.GetMessageCategories(msgToEdit.CustomerId, msgToEdit.MessageCategoryId);
        //    model.MessageToEdit.Buses = BusNumbers;

        //    return View("Edit", model);
        //}

        //[HttpGet]
        ////[Ajax(true)]
        //public ActionResult Edit(int customerId, string busNumber, int id = 0){

        //    ViewModels.Messaging.MessageEditModel model = new ViewModels.Messaging.MessageEditModel(customerId, id);

        //    if (id == 0)//new message
        //    {
        //        model.MessageToEdit.DefaultBus = busNumber;
        //        model.MessageToEdit.CustomerId = customerId;
        //        model.MessageToEdit.Buses = new List<string> { busNumber };
        //        model.MessageToEdit.ValidFrom = DateTime.Now;
        //        model.MessageToEdit.ValidTo = DateTime.Now.AddDays(1);
        //    }
        //    else //edit message
        //    {
        //        model.MessageToEdit.DefaultBus = busNumber;
        //    }

        //    return View("Edit", model); 

        //}

        private MessageEditModel HtmlDecode(MessageEditModel model)
        {
            model.MessageToEdit.Subject = WebUtility.HtmlDecode(model.MessageToEdit.Subject);
            model.MessageToEdit.MessageText = WebUtility.HtmlDecode(model.MessageToEdit.MessageText);
            return model;
        }

        private MessageEditModel HtmlEncode(MessageEditModel model)
        {
            model.MessageToEdit.Subject = WebUtility.HtmlEncode(model.MessageToEdit.Subject);
            model.MessageToEdit.MessageText = WebUtility.HtmlEncode(model.MessageToEdit.MessageText);
            return model;
        }

        public string TellMeDate()
        {
            return DateTime.Today.ToString();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public String GetCategoryValue(string categoryId, DateTime validFrom)
        {
            MessageCategory category = ViewModels.Messaging.MessageEditModel.GetCategory(int.Parse(categoryId));
            if (category != null)
            {
                DateTime validTo = CalculateValidTo(validFrom, category);
            }
            return "";
        }
        [HttpPost]
        [ValidateInput(false)]
        //[Ajax(true)]
        public ActionResult AddEdit(ViewModels.Messaging.MessageEditModel model)
        {
            //Encode the values
            model = HtmlEncode(model);
            Message msgToEdit = model.MessageToEdit;


            List<String> BusNumbers = IBI.Shared.AppUtility.CommaValuesToList(model.BusNumbers);


            if (model.CustomerChanged == "true")
            {
                model.CustomerChanged = "";
                model.MessageToEdit.CustomerId = model.CustomerId;
                model.CustomerList = ViewModels.Messaging.MessageEditModel.GetCustomers(msgToEdit.CustomerId);
                model.MessageCategories = ViewModels.Messaging.MessageEditModel.GetMessageCategories(msgToEdit.CustomerId, 0);

                model.MessageReplyType = ViewModels.Messaging.MessageEditModel.GetMessageReplyType(msgToEdit.MessageReplyTypeId);
                model.MessageToEdit.MessageReplyTypeId = null;

                model.MessageToEdit.MessageCategoryId = null;
                model.BusNumbers = "";
                model.MessageToEdit.MessageText = "";
                model.MessageToEdit.Subject = "";
                model.MessageToEdit.ValidFrom = DateTime.MinValue;
                //model.MessageToEdit.ValidTo = DateTime.MinValue;

                ModelState.Remove("MessageToEdit.MessageCategoryId");
                ModelState.Remove("MessageToEdit.Subject");
                ModelState.Remove("MessageToEdit.ValidFrom");
                ModelState.Remove("MessageToEdit.ValidTo");
                ModelState.Remove("MessageToEdit.MessageText");
                ModelState.Remove("CustomerId");

                if (model.CustomerId > 0)
                    model.FormStatus = "1";
                else
                    model.FormStatus = "0";

                model = HtmlDecode(model);
                Infrastructure.CacheManager.Store("CurrentPageCustomerId", msgToEdit.CustomerId);
                return View("AddEdit", model);
            }


            if (model.CategoryChanged == "true")
            {
                model.CategoryChanged = "";
                model.MessageToEdit.CustomerId = model.CustomerId;

                if (model.MessageToEdit.ValidFrom == DateTime.MinValue)
                {
                    model.MessageToEdit.ValidFrom = DateTime.Now;
                }

                int hours = OUTGOING_DEFAULT_VALIDITY_HOURS;

                if (model.MessageToEdit.MessageCategoryId != null)
                    model.MessageToEdit.ValidTo = GetCategoryValidityTime(model.MessageToEdit.ValidFrom, model.MessageToEdit.CustomerId, model.MessageToEdit.MessageCategoryId.Value);


                model.CustomerList = ViewModels.Messaging.MessageEditModel.GetCustomers(msgToEdit.CustomerId);

                //model.MessageToEdit.ValidFrom = DateTime.Now; 
                model.MessageToEdit.Buses = BusNumbers;
                model.MessageCategories = ViewModels.Messaging.MessageEditModel.GetMessageCategories(msgToEdit.CustomerId, msgToEdit.MessageCategoryId);
                model.MessageReplyType = ViewModels.Messaging.MessageEditModel.GetMessageReplyType(msgToEdit.MessageReplyTypeId);


                ModelState.Remove("MessageToEdit.MessageReplyTypeId");
                ModelState.Remove("MessageToEdit.MessageCategoryId");
                ModelState.Remove("MessageToEdit.Subject");
                ModelState.Remove("MessageToEdit.ValidFrom");
                ModelState.Remove("MessageToEdit.ValidTo");
                ModelState.Remove("MessageToEdit.MessageText");
                ModelState.Remove("CustomerId");



                //ModelState.AddModelError("isValid", "Category Changed");
                model = HtmlDecode(model);

                Infrastructure.CacheManager.Store("CurrentPageCustomerId", msgToEdit.CustomerId);
                return View("AddEdit", model);
            }



            if (BusNumbers.Count <= 0)
            {
                ModelState.AddModelError("BusNumbers", "Please select at least one bus");
            }


            //check Valid From / Valid To. 
            if (ModelState.IsValid)
            {
                if (msgToEdit.SenderId == null || msgToEdit.SenderId <= 0)
                    msgToEdit.SenderId = UserManager.CurrentUser.UserId;

                msgToEdit.LastModifiedBy = UserManager.CurrentUser.UserId;

                msgToEdit.Buses = BusNumbers; //new List<string> { msgToEdit.DefaultBus };

                msgToEdit.CustomerId = model.CustomerId;
                if (msgToEdit.MessageReplyTypeId == 0) msgToEdit.MessageReplyTypeId = null;
                int msgId = ServiceManager.SaveMessage(msgToEdit);
                ModelState.Clear();


                ViewModels.Messaging.MessageEditModel editModel = new ViewModels.Messaging.MessageEditModel(model.MessageToEdit.CustomerId, msgId);
                editModel.LoadAfterSave = true;
                editModel.MessageToEdit.DefaultBus = "0";
                editModel.PostResult = "success";

                model = HtmlDecode(model);

                Infrastructure.CacheManager.Store("CurrentPageCustomerId", model.MessageToEdit.CustomerId);
                return View("AddEdit", editModel);
            }


            model.MessageToEdit.CustomerId = model.CustomerId;

            model.CustomerList = ViewModels.Messaging.MessageEditModel.GetCustomers(msgToEdit.CustomerId);

            model.MessageCategories = ViewModels.Messaging.MessageEditModel.GetMessageCategories(msgToEdit.CustomerId, msgToEdit.MessageCategoryId);

            model.MessageReplyType = ViewModels.Messaging.MessageEditModel.GetMessageReplyType(msgToEdit.MessageReplyTypeId);

            model.MessageToEdit.Buses = BusNumbers;

            model = HtmlDecode(model);

            Infrastructure.CacheManager.Store("CurrentPageCustomerId", model.CustomerId);
            return View("AddEdit", model);
        }

        [HttpGet]
        [ValidateInput(false)]
        //[Ajax(true)]
        public ActionResult AddEdit(string busNumber, int customerId = 0, int messageId = 0)
        {
            ViewModels.Messaging.MessageEditModel model = new ViewModels.Messaging.MessageEditModel(customerId, messageId);

            model = HtmlDecode(model);
            if (messageId == 0)//new message
            {
                model.MessageToEdit.DefaultBus = busNumber ?? "0";
                model.MessageToEdit.CustomerId = customerId;
                model.MessageToEdit.Buses = new List<string> { busNumber };
                model.MessageToEdit.ValidFrom = DateTime.Now;
                model.MessageToEdit.ValidTo = DateTime.Now.AddDays(1);
            }
            else //edit message
            {
                model.MessageToEdit.DefaultBus = busNumber;
            }

            return View("AddEdit", model);

        }

        [HttpGet]
        public ActionResult IndexList(int customerId, string busNumber)
        {
            ViewData["customerId"] = customerId;
            ViewData["busNumber"] = busNumber;
            return View();
        }

        [HttpGet]
        public ActionResult IndexNew(int customerId, string busNumber, int messageId = -1)
        {
            ViewData["customerId"] = customerId;
            ViewData["busNumber"] = busNumber;
            ViewData["messageId"] = messageId;
            return View();
        }



        public ActionResult Incoming(
            string busNumber,
            DateTime? date,
            string messageText,
            string position,
            string line,
            string destination,
            string nextStop,
            int? groupId,
            GridSortOptions gridSortOptions,
            int? page
            )
        {
            // Create all filter data and set current values if any
            // These values will be used to set the state of the select list and textbox
            // by sending it back to the view.            
            var filterViewModel = new ViewModels.Messaging.Incoming.FilterViewModel();

            filterViewModel.BusNumber = busNumber;
            filterViewModel.GroupId = groupId ?? -1;
            filterViewModel.Date = date ?? DateTime.MinValue;
            filterViewModel.Position = position;
            filterViewModel.Destination = destination;
            filterViewModel.Line = line;
            filterViewModel.MessageText = messageText;
            filterViewModel.NextStop = nextStop;


            filterViewModel.Fill();

            List<IBI.Shared.Models.Messages.IncomingMessage> list = ServiceManager.GetIncomingMessages(
                Common.CurrentUser.UserId,
                filterViewModel.Date,
                filterViewModel.BusNumber,
                filterViewModel.MessageText,
                filterViewModel.Position,
                filterViewModel.Destination,
                filterViewModel.Line,
                filterViewModel.NextStop,
                filterViewModel.GroupId, false);

            // Order and page the product list
            var pagedList = list
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                   .AsPagination(page ?? 1, 15);


            var listContainer = new ViewModels.Messaging.Incoming.ListContainerViewModel
            {
                PagedList = pagedList,
                FilterViewModel = filterViewModel,
                GridSortOptions = gridSortOptions
            };

            return View(listContainer);
        }

        [ValidateInput(false)]
        public ActionResult Outgoing(
           string busNumber,
           string subject,
           string messageText,
           string messageCategoryName,
           DateTime? validFrom,
           DateTime? validTo,
           int? groupId,
           string senderName,
           GridSortOptions gridSortOptions,
           int? page,
           bool showExpiredOnly = false,
           string selectedMsgs = ""
           )
        {

            // Create all filter data and set current values if any
            // These values will be used to set the state of the select list and textbox
            // by sending it back to the view.            
            var filterViewModel = new ViewModels.Messaging.Outgoing.FilterViewModel();

            filterViewModel.BusNumber = busNumber;
            filterViewModel.GroupId = groupId ?? -1;
            filterViewModel.ValidFrom = validFrom ?? DateTime.MinValue;
            filterViewModel.ValidTo = validTo ?? DateTime.MinValue;
            filterViewModel.Title = subject;
            filterViewModel.MessageText = WebUtility.HtmlEncode(messageText);
            filterViewModel.MessageCategoryName = messageCategoryName;
            filterViewModel.SenderName = senderName;
            filterViewModel.ShowExpiredOnly = showExpiredOnly;
            filterViewModel.SelectedMsgs = selectedMsgs;


            filterViewModel.Fill();

            List<IBI.Shared.Models.Messages.Message> list = ServiceManager.GetOutgoingMessages(
                Common.CurrentUser.UserId,
                filterViewModel.BusNumber,
                filterViewModel.ValidFrom,
                filterViewModel.ValidTo,
                filterViewModel.Title,
                filterViewModel.MessageText,
                filterViewModel.MessageCategoryName,
                filterViewModel.GroupId,
                filterViewModel.SenderName,
                filterViewModel.ShowExpiredOnly);

            for (int messageIndex = 0; messageIndex < list.Count; messageIndex++)
            {
                list[messageIndex].Subject = HttpUtility.HtmlDecode(list[messageIndex].Subject);
                list[messageIndex].MessageText = HttpUtility.HtmlDecode(list[messageIndex].MessageText);
            }

            // Order and page the product list
            var pagedList = list
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                   .AsPagination(page ?? 1, 15);

            filterViewModel.MessageText = WebUtility.HtmlDecode(messageText);
            var listContainer = new ViewModels.Messaging.Outgoing.ListContainerViewModel
            {
                PagedList = pagedList,
                FilterViewModel = filterViewModel,
                GridSortOptions = gridSortOptions
            };

            return View(listContainer);
        }


        [HttpPost]
        public ActionResult MessageReplies(int messageId, string busNumber)
        {
            List<IBI.Shared.Models.Messages.IncomingMessage> model = ServiceManager.GetMessageReplies(messageId, busNumber);
            return PartialView("PartialMessageReplies", model);
        }

        [HttpPost]
        public ActionResult MessageConfirmations(int messageId, string busNumber)
        {
            List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> model = ServiceManager.GetMessageConfirmations(messageId, busNumber);
            return PartialView("PartialDisplayedConfirmations", model);
        }

        [HttpPost]
        public ActionResult MessageConfirmationsAndReplies(int messageId, string busNumber)
        {
            IBI.Shared.Models.Messages.MessageConfirmationsAndReplys msgConfAndReplys = ServiceManager.GetMessageConfirmationsAndReplys(messageId, busNumber);
            return View(msgConfAndReplys);
        }

        [HttpGet]
        public ActionResult MessageConfirmationsNReplies(int messageId,string messageTitle="")
        {
            IBI.Shared.Models.Messages.MessageConfirmationsAndReplys msgConfAndReplys = ServiceManager.GetMessageConfirmationsAndReplys(messageId);
            msgConfAndReplys.MessageTitle = messageTitle;
            return View("MessageConfirmationsAndReplies", msgConfAndReplys);
        }


        public ActionResult Archive()
        {
            return View();
        }


        public ActionResult Administration()
        {
            return View();
        }


        [HttpGet]
        public JsonResult SetExpireMessage(int messageId)
        {
            var model = "";
            bool result = ServiceManager.SetExpireMessage(messageId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        //public DateTime GetCategoryValidityTime(DateTime validFrom, int customerId = 2140, int messageCategoryId = 0)
        //{
        //    int hours = 24;
        //    var cat = ServiceManager.GetMessageCategories(customerId).Where(mc => mc.MessageCategoryId == messageCategoryId).ToList();
        //    if (cat != null)
        //    {
        //        hours = cat.First().Validity ?? 24;
        //        return validFrom.AddHours(hours);
        //    }

        //    return validFrom.AddHours(24); //default message validitiy 24 hours
        //}

        public DateTime GetCategoryValidityTime(DateTime validFrom, int customerId = 2140, int messageCategoryId = 0)
        {
            var cats = ServiceManager.GetMessageCategories(customerId).Where(mc => mc.MessageCategoryId == messageCategoryId).ToList();
            if (cats != null && cats.Count() > 0)
            {
                IBI.Shared.Models.Messages.MessageCategory cat = cats.FirstOrDefault();
                DateTime validTo = CalculateValidTo(validFrom, cat);
                return validTo;
            }
             return validFrom.AddHours(24); //default message validitiy 24 hours
        }

        private static DateTime CalculateValidTo(DateTime validFrom, IBI.Shared.Models.Messages.MessageCategory cat)
        {



            TimeSpan ts = TimeSpan.Zero;
            DateTime validTo = new DateTime();

            int vHours1 = 0;
            int vMins1 = 0;
            int vHours2 = 0;
            int vMins2 = 0;

            //int vHours2 = int.Parse(cat.ValidityValue.Substring(0, 2).Trim());
            //int vMins2 = int.Parse(cat.ValidityValue.Substring(3, 2).Trim());

            int vDays = 0;
            int vWeeks = 0;


            if (cat.ValidityRule.StartsWith("DURATION"))
            {
                vHours1 = int.Parse(cat.ValidityValue.Trim());

                validTo = validFrom.AddHours(vHours1);

            }

            if (cat.ValidityRule.StartsWith("CLOCK") || cat.ValidityRule.StartsWith("DAY"))
            {
                string[] arrTime = cat.ValidityValue.Split(',');

                int counter = 0;
                foreach (string itm in arrTime)
                {

                    switch (counter)
                    {
                        case 0:
                            {
                                vHours1 = int.Parse(itm.Trim().Substring(0, 2).Trim());
                                vMins1 = int.Parse(itm.Trim().Substring(3, 2).Trim());
                                break;
                            }
                        case 1:
                            {
                                vHours2 = int.Parse(itm.Trim().Substring(0, 2).Trim());
                                vMins2 = int.Parse(itm.Trim().Substring(3, 2).Trim());
                                break;
                            }

                    }
                    counter++;
                }

                if (cat.ValidityRule.StartsWith("DAY+"))
                {
                    vDays = int.Parse(cat.ValidityRule.Substring(4).Trim());
                }

                int year1 = validFrom.AddDays(vDays).Year;  //(vHours1>0 && vDays==0) ? validFrom.AddHours(vHours1).Year : validFrom.AddDays(vDays).Year;
                int month1 = validFrom.AddDays(vDays).Month; //(vHours1>0 && vDays==0) ? validFrom.AddHours(vHours1).Month : validFrom.AddDays(vDays).Month;
                int day1 = validFrom.AddDays(vDays).Day; //(vHours1>0 && vDays==0) ? validFrom.AddHours(vHours1).Day : validFrom.AddDays(vDays).Day;
                int hour1 = vHours1;
                int min1 = vMins1;

                DateTime date1 = new DateTime(year1, month1, day1, hour1, min1, 0);


                if (date1 > validFrom)
                {
                    validTo = date1;

                    if (cat.ValidityRule.StartsWith("DAY"))
                    {
                        if (date1.AddDays(-(vDays)) <= validFrom)
                        {
                            validTo = validTo.AddDays(1);
                        }
                    }
                }

                if (arrTime.Length > 1 && date1 <= validFrom)
                {
                    int year2 = validFrom.AddDays(vDays).Year; //(vHours2>0 && vDays==0) ? validFrom.AddHours(vHours2).Year : validFrom.AddDays(vDays).Year;
                    int month2 = validFrom.AddDays(vDays).Month; //(vHours2>0 && vDays==0) ? validFrom.AddHours(vHours2).Month : validFrom.AddDays(vDays).Month;
                    int day2 = validFrom.AddDays(vDays).Day; //(vHours2>0 && vDays==0) ? validFrom.AddHours(vHours2).Day : validFrom.AddDays(vDays).Day;
                    int hour2 = vHours2;
                    int min2 = vMins2;
                    DateTime date2 = new DateTime(year2, month2, day2, hour2, min2, 0);

                    if (date2 > validFrom)
                    {
                        validTo = date2;
                    }
                    else
                    {
                        validTo = date1.AddDays(1);
                    }
                }
                else
                {
                    if (date1 <= validFrom)
                    {
                        date1 = date1.AddDays(1);
                        validTo = date1;
                    }
                }

            }
            return validTo;
        }
    }
}