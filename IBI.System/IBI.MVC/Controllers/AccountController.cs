﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IBI.Web.ViewModels.UserAccounts;
using IBI.Shared.Models.Accounts;
using Newtonsoft.Json;
using User = IBI.Shared.Models.Accounts.User;
using System.Security.Principal;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Keys;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.Logger;

namespace IBI.Web.Controllers
{

    [Authorize] 
    public class AccountController : Controller
    {
 
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "")
        {

            SessionManager.Clear(); // forcefully clear session if user reaches login page.
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                SessionManager.Store(SessionKeys.LogFileName, model.UserName);

                WebLogger.Log("REST Authenticate - start", new object[] { model.UserName, "****", Common.Settings.RestApiKey });
                
                Auth auth = RESTManager.Authenticate(model.UserName, model.Password, Common.Settings.RestApiKey);

                WebLogger.Log("REST Authentication - end");


                if (auth != null && auth.UserId > 0)
                {

                    WebLogger.Log("AuthorizeLogin.Login - start", new object[] { auth});

                    AuthorizeLogin.Login(auth);
                    
                    WebLogger.Log("AuthorizeLogin.Login - end");

                    Common.CurrentUser.UserPages = Infrastructure.ServiceManager.GetUserPages(auth.UserId);
                     
                    //SessionManager.Store(SessionKeys.CurUser_UserPages, usrPages);

                    if (returnUrl != null && returnUrl != "")
                    {
                        Response.Redirect(returnUrl);
                        return null;
                    }

                    else
                    {

                        string startUpUrl = Common.Settings.ForcedStartupUrl;
                        
                        if (String.IsNullOrEmpty(startUpUrl) == false && Common.CurrentUser.UserPages.Where(up=>up.Path.Contains(startUpUrl)).Count()>0)
                        {
                            Response.Redirect(startUpUrl);   
                        }
                        else
                        {
                            //redirect to first action on which current user has Read Access.
                            string[] defaultAction = Common.CurrentUser.GetDefaultAction();
                            return RedirectToAction(defaultAction[0], defaultAction[1]); 

                        }

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Login failed: " + auth.ErrorMessage);
                }
            }
            else {
                ModelState.AddModelError("", "Login failed: Please provide Username & Password"); 
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff


        [AllowAnonymous]
        public ActionResult LogOff1()
        {
            AuthorizeLogin.LogoutFromPopup("");
            return null;
        }



        [AllowAnonymous]
        public ActionResult LogOff()
        {

          //AuthorizeLogin.Logout(Request.ServerVariables["HTTP_REFERER"]);
          AuthorizeLogin.Logout("");
          return null;
          // FormsAuthentication.SignOut();
          // Session.Abandon();                        
          //return RedirectToAction("BusTree", "Bus"); 
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, passwordQuestion: null, passwordAnswer: null, isApproved: true, providerUserKey: null, status: out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        public ActionResult ChangePassword2()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [HttpPost]        
        public ActionResult ChangePassword1(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, userIsOnline: true);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        

        [AllowAnonymous]
        public ActionResult UserMenu()
        {
            if (Common.CurrentUser.UserId > 0)
            {
                List<UserPage> userNavMenu = SessionManager.Get<List<UserPage>>(SessionKeys.CurUser_UserNavManu);
                if (userNavMenu == null)
                {
                    userNavMenu = SessionManager.Get<List<UserPage>>(SessionKeys.CurUser_UserPages); //ServiceManager.GetUserPages(Common.CurrentUser.UserId);
                    //SessionManager.Store(SessionKeys.CurUser_UserNavManu, userNavMenu);
                    //userNavMenu = ServiceManager.GetUserPages(Common.CurrentUser.UserId);
                    
                    SessionManager.Store(SessionKeys.CurUser_UserNavManu, userNavMenu);
                }

                return View(userNavMenu);
                //return Json(userPages, JsonRequestBehavior.AllowGet);
            }

            else
            {
               return LogOff();
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult AccountSettings()
        {
            IBI.Shared.Models.Accounts.User usr = IBI.Web.Infrastructure.ServiceManager.GetUser(SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser).Username);
            AccountInfoModel accountInfo = new AccountInfoModel();
            accountInfo.Username = usr.Username;
            accountInfo.FullName = usr.FullName;
            accountInfo.ShortName = usr.ShortName;
            accountInfo.PhoneNumber = usr.PhoneNumber;
            accountInfo.Email = usr.Email;
            accountInfo.Company = usr.Company;
            accountInfo.Domain = usr.Domain;
            return View(accountInfo);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            IBI.Shared.Models.Accounts.User usr = IBI.Web.Infrastructure.ServiceManager.GetUser(SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser).Username);
            AccountSettingsModel accountSettings = new AccountSettingsModel();
            accountSettings.UserId = usr.UserId;
            accountSettings.Username = usr.Username;
            accountSettings.OldPassword = usr.Password;
            accountSettings.NewPassword = string.Empty;
            accountSettings.ConfirmPassword = string.Empty;

            return View(accountSettings);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SavePassword(AccountSettingsModel model)
        {
            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = IBI.Web.Infrastructure.ServiceManager.ChangePassword(model.Username, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    model.PostResult = "true";
                    model.ErrorMessage = model.NewPassword;
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.PostResult = "false";
                    model.ErrorMessage = "The current password is incorrect or the new password is invalid.";
                    return Json(model, JsonRequestBehavior.AllowGet);

                    //ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }
            else
            {
                if (model.OldTypePassword != model.OldPassword)
                {
                    model.PostResult = "false";
                    model.ErrorMessage = "The current password is incorrect";
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else if(model.NewPassword != model.ConfirmPassword)
                {
                    model.PostResult = "false";
                    model.ErrorMessage = "The new password and confirmation password do not match.";
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.PostResult = "false";
                    model.ErrorMessage = "The new password  must be at least 6 characters long or the new password is invalid.";
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult EditAccountSettings(AccountInfoModel model)
        {
            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = IBI.Web.Infrastructure.ServiceManager.EditAccountSettings(model);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    model.PostResult = "true";
                    //model.ErrorMessage = model.NewPassword;
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.PostResult = "false";
                    model.ErrorMessage = "Some Entered information is invalid!";
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string error = string.Empty;
                foreach (var item in ModelState)
                {
                    if (item.Value.Errors.Any())
                    {
                        error += string.Join("**  ", item.Value.Errors.Select(x => x.ErrorMessage));
                    }
                }
                model.PostResult = "false";
                model.ErrorMessage = error;
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

     

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
