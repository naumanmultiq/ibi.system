﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.Keys;

namespace IBI.Web.Controllers
{

    public class ExampleController : Controller
    {
        //
        // GET: /Example/

        public ActionResult SlickGrid()
        {
            return View();
        }

        public ActionResult SlickGridPunctuality(
            string busNumber,
            DateTime? journeyDate,
            int? journeyId)
        {
            ViewData["busNumber"] = busNumber;
            ViewData["journeyDate"] = journeyDate ?? DateTime.Now;
            ViewData["journeyId"] = journeyId;
            return View("Punctuality");
        }

        public JsonResult Journey(
           string busNumber,
           DateTime? journeyDate
          )
        {
            IBI.Shared.Models.BusJourney.Journey[] list = ServiceManager.GetJourneys(
                journeyDate ?? DateTime.MinValue,
                busNumber
                ).ToArray();
            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


        public JsonResult Punctuality(
            string busNumber,
            DateTime? journeyDate,
            int? journeyId
          )
        {
            List<IBI.Shared.Models.BusJourney.Punctuality> list = ServiceManager.GetPunctuality(
                 journeyDate ?? DateTime.MinValue,
                 busNumber,
                 journeyId??0
                );

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }



        [HttpGet]
        //[Ajax(true)]
        public ActionResult BusMap()
        {
            return View("FullMap");
        }

        [HttpPost]
        public ActionResult GetUserSpecificBusesData(int groupId=0, string busNumber = "")
        {
            string busesData = ServiceManager.GetAllUsersBuses(groupId, busNumber);
            Response.ContentType = "application/json";
            Response.Write(busesData);
            return null;
        }




    }
}
