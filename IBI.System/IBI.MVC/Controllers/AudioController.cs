﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using IBI.Shared.Models.Messages;
using IBI.Web.ViewModels;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using MvcContrib.UI.Grid;
using IBI.Web.ViewModels.Messaging;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using System.Globalization;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.CustomActionResults;
using mermaid.BaseObjects.IO;
using System.IO;
using IBI.Web.Common;

namespace IBI.Web.Controllers
{
    [AuthorizeLogin]
    public class AudioController : Controller
    {
        //
        // GET: /Voice/

        #region StopSearch

        [HttpGet]
        public ActionResult SearchIndex()
        {
            ViewData["StopName"] = "";
            return View();
        }

        //[HttpGet]
        //public ActionResult SearchContents(
        //   string stopName,
        //   GridSortOptions gridSortOptions = null,
        //   int? page = null)
        //{


        //    gridSortOptions = gridSortOptions == null ? new GridSortOptions() : gridSortOptions;

        //    var model = new ViewModels.Voice.Search.ListContainerViewModel();

        //    var filterViewModel = new ViewModels.Voice.Search.FilterViewModel();
        //    filterViewModel.StopName = stopName;

        //    List<IBI.Shared.Models.TTS.AudioFile> list = ServiceManager.GetStopAudioFiles(stopName, "");


        //    // Order and page the product list
        //    var pagedList = list
        //        .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
        //        .AsPagination(page ?? 1, 9999);


        //    var listContainer = new ViewModels.Voice.Search.ListContainerViewModel
        //    {
        //        PagedList = pagedList,
        //        FilterViewModel = filterViewModel,
        //        GridSortOptions = gridSortOptions
        //    };

        //    return View(listContainer);

        //}
        [HttpPost]
        public ActionResult SearchContents(
            string stopName,
            SortingPagingInfo info)
        {

            if (string.IsNullOrEmpty(stopName) || stopName.Length < 3)
            {
                ViewBag.SortingPagingInfo = info;
                return View(new List<IBI.Shared.Models.TTS.AudioFile>());
            }

            stopName = string.IsNullOrEmpty(stopName) ? info.SearchKeyword : stopName;
            info.SearchKeyword = stopName;

            List<IBI.Shared.Models.TTS.AudioFile> list = ServiceManager.GetStopAudioFiles(stopName, true,false,false);

            if (info.SortField == null)
                info.SortField = "Text";

            if (info.SortDirection == null)
                info.SortDirection = "ascending";

            switch (info.SortField)
            {
                case "Text":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Text).ToList() :
                             list.OrderByDescending(c => c.Text).ToList());
                    break;
                case "Voice":
                    list = (info.SortDirection == "ascending" ?
                            list.OrderBy(c => c.Voice).ToList() :
                            list.OrderByDescending(c => c.Voice).ToList());
                    break;
                case "Version":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Version).ToList() :
                             list.OrderByDescending(c => c.Version).ToList());
                    break;
                case "Filename":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Filename).ToList() :
                             list.OrderByDescending(c => c.Filename).ToList());
                    break;
                case "Approved":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Approved).ToList() :
                             list.OrderByDescending(c => c.Approved).ToList());
                    break;
                case "Rejected":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Rejected).ToList() :
                             list.OrderByDescending(c => c.Rejected).ToList());
                    break;
                case "LastModified":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.LastModified).ToList() :
                             list.OrderByDescending(c => c.LastModified).ToList());
                    break;
            }


            //info.PageCount = Convert.ToInt32(Math.Ceiling(list.Count() / Convert.ToDouble(info.PageSize)));

            //list = list.Skip(info.CurrentPageIndex * info.PageSize).Take(info.PageSize).ToList();


            ViewBag.SortingPagingInfo = info;

            return View(list);

        }

        #endregion


        #region Approve / Reject Audio Files


        public ActionResult ApprovalIndex()
        {
            return View();
        }

        public ActionResult ApprovalContents(
            string stopName,
            SortingPagingInfo info)
        {

            if(string.IsNullOrEmpty(stopName) || stopName.Length<3)
            {
                ViewBag.SortingPagingInfo = info;
                return View(new List<IBI.Shared.Models.TTS.AudioFile>());
            }

            stopName = string.IsNullOrEmpty(stopName) ? info.SearchKeyword : stopName;
            info.SearchKeyword = stopName;

            List<IBI.Shared.Models.TTS.AudioFile> list = ServiceManager.GetStopAudioFiles(stopName, info.SearchFilterA, info.SearchFilterR, info.SearchFilterU);

            // Order and page the product list
              
            if (info.SortField == null)
                info.SortField = "Text";

            if (info.SortDirection == null)
                info.SortDirection = "ascending";
             

            switch (info.SortField)
            {
                case "Text":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Text).ToList() :
                             list.OrderByDescending(c => c.Text).ToList());
                    break;
                case "Voice":
                    list = (info.SortDirection == "ascending" ?
                            list.OrderBy(c => c.Voice).ToList() :
                            list.OrderByDescending(c => c.Voice).ToList());
                    break;
                case "Version":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Version).ToList() :
                             list.OrderByDescending(c => c.Version).ToList());
                    break;
                case "Filename":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Filename).ToList() :
                             list.OrderByDescending(c => c.Filename).ToList());
                    break;
                case "Approved":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Approved).ToList() :
                             list.OrderByDescending(c => c.Approved).ToList());
                    break;
                case "Rejected":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.Rejected).ToList() :
                             list.OrderByDescending(c => c.Rejected).ToList());
                    break;
                case "LastModified":
                    list = (info.SortDirection == "ascending" ?
                             list.OrderBy(c => c.LastModified).ToList() :
                             list.OrderByDescending(c => c.LastModified).ToList());
                    break;
            }


            //info.PageCount = Convert.ToInt32(Math.Ceiling(list.Count() / Convert.ToDouble(info.PageSize)));

            //list = list.Skip(info.CurrentPageIndex * info.PageSize).Take(info.PageSize).ToList();

            ViewBag.SortingPagingInfo = info;

            return View(list);

        }

        //public ActionResult ApprovalContents(string ListFilter, string stopName,
        //    GridSortOptions gridSortOptions,
        //    int? page)
        //{

        //    var model = new ViewModels.Voice.Approval.ListContainerViewModel();

        //    var filterViewModel = new ViewModels.Voice.Approval.FilterViewModel();
        //    filterViewModel.StopName = stopName;
        //    filterViewModel.ListFilter = ListFilter;

        //    List<IBI.Shared.Models.TTS.AudioFile> list = ServiceManager.GetStopAudioFiles(stopName, ListFilter);
        //    //.Select(x => new IBI.Shared.Models.TTS.AudioFile
        //    //{
        //    //     Approved=x.Approved,
        //    //     SourceText = x.SourceText.Replace("\\", "\\\\"),
        //    //      FileContent=x.FileContent,
        //    //       FileHash=x.FileHash,
        //    //        Filename =x.Filename,
        //    //         LastModified=x.LastModified,
        //    //          Rejected=x.Rejected,
        //    //           SourceVoice=x.SourceVoice,
        //    //            Text= x.Text,
        //    //             Version=x.Version, 
        //    //             Voice =x.Voice

        //    //}).ToList();


        //    //foreach(IBI.Shared.Models.TTS.AudioFile file in list)
        //    //{
        //    //    file.SourceText = file.SourceText.Replace("\\", "\\\\");
        //    //}

        //    // Order and page the product list
        //    var pagedList = list
        //        .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
        //        .AsPagination(page ?? 1, 9999);


        //    var listContainer = new ViewModels.Voice.Approval.ListContainerViewModel
        //    {
        //        PagedList = pagedList,
        //        FilterViewModel = filterViewModel,
        //        GridSortOptions = gridSortOptions
        //    };

        //    return View(listContainer);
        //}

        public ActionResult AudioFile(string fileName, string voice, string status)
        {
            IBI.Shared.Models.TTS.AudioFile audioFile = null;

            if (String.IsNullOrEmpty(fileName) == false && String.IsNullOrEmpty(voice) == false)
            {
                audioFile = ServiceManager.GetAudioFile(fileName, voice);
            }

            ViewModels.Voice.AudioFile.AudioFileViewModel model = new ViewModels.Voice.AudioFile.AudioFileViewModel();
            model.Text = audioFile.Text;
            model.Voice = audioFile.Voice;
            model.SourceText = audioFile.SourceText; //String.IsNullOrEmpty(audioFile.SourceText) ? audioFile.Text : audioFile.SourceText;
            model.Filename = audioFile.Filename.Substring(0, audioFile.Filename.LastIndexOf("."));
            model.Version = audioFile.Version ?? 0;
            model.Status = status;
            model.AlternateSourceVoiceText = audioFile.SourceVoice;

            string uploadedMp3Path = CacheManager.Get<string>("UploadedMp3ForStopPath");
            string uploadedMp3forStopSourceVoiceText = CacheManager.Get<string>("UploadedMp3ForStopSourceVoiceText");
            model.UploadedMp3Path = uploadedMp3Path;

            if (!string.IsNullOrEmpty(uploadedMp3Path))
            {
                model.AlternateSourceVoiceText = uploadedMp3forStopSourceVoiceText;
            }


            CacheManager.Destroy("UploadedMp3ForStopPath");
            CacheManager.Destroy("UploadedMp3ForStopSourceVoiceText");

            return View(model);
        }

        [HttpPost]
        public JsonResult AudioFile(IBI.Web.ViewModels.Voice.AudioFile.AudioFileViewModel model)
        {
            if (ModelState.IsValid)
            {

                string uploadedMp3Path = model.UploadedMp3Path;
                string uploadedMp3SourceVoiceText = model.AlternateSourceVoiceText;

                byte[] mp3 = null;
                if (!string.IsNullOrEmpty(uploadedMp3Path))
                {
                    mp3 = System.IO.File.ReadAllBytes(uploadedMp3Path);
                }

                //model.SourceText = String.IsNullOrEmpty(model.SourceText) ? model.Text : model.SourceText;

                bool result = false;
                result = string.IsNullOrEmpty(uploadedMp3Path)
                   ? ServiceManager.SaveAudioFile(model.Text, model.SourceText, model.Voice, model.Status)
                   : ServiceManager.SaveAudioFileMp3(model.Text, model.SourceText, model.Voice, model.Status, mp3, model.AlternateSourceVoiceText);

                if (result)
                {
                    model.LoadAfterSave = true;
                    model.PostResult = "true";
                    model.ErrorMessage = "Audio File saved successfully";
                }
                else
                {
                    model.LoadAfterSave = true;
                    model.PostResult = "false";
                    model.ErrorMessage = "Failed to save this audio file";
                }

            }

            CacheManager.Destroy("UploadedMp3ForStopPath");
            CacheManager.Destroy("UploadedMp3ForStopSourceVoiceText");

            //return View(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }



        public FileResult PlayAudio(string filename, string voice, string sourceText)
        {
            byte[] fileContent = RESTManager.GetTtsFile(filename, voice);
            return File(fileContent, "audio/mp3", sourceText + ".mp3");
        }


        public ActionResult AudioPreview(string voice, string fileName)
        {


            //ViewData["stopName"] = stopName ?? "";
            ViewData["voice"] = voice ?? "";
            //ViewData["sourceText"] = sourceText ?? "";
            ViewData["fileName"] = fileName ?? "";

            //byte[] fileContent = ServiceManager.GenerateAudioFile(stopName, sourceText, voice);

            return View();
        }

        public static string GenerateFilePath(string voice, string fileName)
        {

            //if (String.IsNullOrEmpty(stopName))
            //    return "../Temp/sound.mp3";

            //sourceText = String.IsNullOrEmpty(sourceText) ? stopName : sourceText;


            byte[] fileContent = RESTManager.GetTtsFile(fileName, voice);

            // string fileName = FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName))).ToString() + ".mp3";
            String filePath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Temp/"), fileName);
            ByteArrayToFile(filePath, fileContent);
            return "../Temp/" + fileName + "?cachetimestamp=" + DateTime.Now.ToString("yyyyMMd-HH:mm:ss");

            //----------------------------------------
            //if (String.IsNullOrEmpty(stopName))
            //    return "../Temp/sound.mp3";

            //sourceText = String.IsNullOrEmpty(sourceText) ? stopName : sourceText;


            //byte[] fileContent = ServiceManager.GenerateAudioFile(stopName, sourceText, voice);

            //string fileName = FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName))).ToString() + ".mp3";
            //String filePath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Temp/"), fileName);
            //ByteArrayToFile(filePath, fileContent);
            //return "../Temp/" + fileName;

        }

        //*********************************************************

        public FileResult GeneratePreview(string stopName, string voice, string sourceText)
        {
            byte[] fileContent = ServiceManager.GenerateAudioFile(stopName, sourceText, voice);
            return File(fileContent, "audio/mp3", sourceText + ".mp3");
            //return new AudioResult(fileContent);

        }

        public ActionResult AudioPreviewGenerate(string stopName, string voice, string sourceText)
        {


            ViewData["stopName"] = Server.UrlDecode(Server.HtmlDecode(stopName)) ?? "";
            ViewData["voice"] = voice ?? "";
            ViewData["sourceText"] = sourceText ?? "";

            //byte[] fileContent = ServiceManager.GenerateAudioFile(stopName, sourceText, voice);

            return View();
        }

        public static string GeneratePreviewFilePath(string stopName, string voice, string sourceText)
        {

            if (String.IsNullOrEmpty(stopName))
                return "../Temp/sound.mp3";

            sourceText = String.IsNullOrEmpty(sourceText) ? stopName : sourceText;


            byte[] fileContent = ServiceManager.GenerateAudioPreview(stopName, sourceText, voice);

            string fileName = FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName))).ToString() + ".mp3";
            String filePath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Temp/"), fileName);
            ByteArrayToFile(filePath, fileContent);
            return "../Temp/" + fileName + "?cachetimestamp=" + DateTime.Now.ToString("yyyyMMd-HH:mm:ss");

        }

        [HttpGet]
        public ActionResult UploadAudioFile()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadAudioFile(object fl)
        {

            try
            {

                if (Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = Request.Files["UploadedMp3"];
                    string sourceVoiceText = Request["sourceVoiceText"] != null ? Request["sourceVoiceText"] : "";

                    if (httpPostedFile != null)
                    {
                        // Validate
                        if (!httpPostedFile.FileName.ToLower().EndsWith(".mp3"))
                            return Json("WrongFileType", JsonRequestBehavior.AllowGet);

                        // Get the complete file path
                        var fileSavePath = Path.Combine(Server.MapPath("../Temp"), httpPostedFile.FileName);

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);


                        CacheManager.Store("UploadedMp3ForStopPath", fileSavePath);
                        CacheManager.Store("UploadedMp3ForStopSourceVoiceText", sourceVoiceText);

                    }

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json("Unknown Server Error", JsonRequestBehavior.AllowGet);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }



        // [HttpGet]
        //public virtual ActionResult DownloadFileReady(string fileName, string voice)
        //{ 
        //   byte[]  = RESTManager.GetTtsFile(fileName, voice)

        //   //return File(fileContents, "application/vnd.ms-excel", fileName);
        //}
        [HttpGet]
        public virtual ActionResult Download(string fileName, string voice, string stopName)
        {
            try
            {
                byte[] fileContents = RESTManager.GetTtsFile(fileName, voice);
                return File(fileContents, "audio/mpeg", stopName + ".mp3");
            }
            catch (Exception ex)
            {
                // do nothing
                return File(new byte[] { 1 }, "audio/mpeg", "InvalidStop.mp3");
            }
        }

        public JsonResult SetApprovalForAudioFile(string fileName, string voice, string status)
        {
            return Json("test");
        }

        [HttpGet]
        public JsonResult GetAllSourceVoices()
        {
            //return Json("[\"bente22k\",\"DANF064-GRETE\",\"kari22k\",\"mette22k\",\"olav22k\",\"test\"]", JsonRequestBehavior.AllowGet);
            List<string> voices = ServiceManager.GetAllSourceVoices();
            return Json(voices, JsonRequestBehavior.AllowGet);
        }


        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream =
                   new System.IO.FileStream(_FileName, System.IO.FileMode.Create,
                                            System.IO.FileAccess.Write);
                // Writes a block of bytes to this stream using data from
                // a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}",
                                  _Exception.ToString());
            }

            // error occured, return false
            return false;
        }
        #endregion

    }
}
