﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using IBI.Web.ViewModels;
using MvcContrib;
using MvcContrib.UI.Grid;
using IBI.Web.Infrastructure;
using IBI.Web.ViewModels.Messaging;
using MvcContrib.Sorting;
using MvcContrib.Pagination;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Shared.Models.BusTree;

namespace IBI.Web.Controllers
{

     
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //List<IBI.Shared.Models.BusTree.BusDetail> list = Infrastructure.ServiceManager.GetBuses(2140);
            return View();  
        }
         
        public ActionResult WebGrid()
        {
            List<IBI.Shared.Models.BusTree.BusDetail> list = Infrastructure.ServiceManager.GetBuses(2140);
            return View(list);                                    

        }

        public JsonResult BusToLandMessage()
        {
            //string pingData = "busNumber=1123;customerId=2140;timestamp=12/06/2012 13:15:00;line=130, 134, 135;designation=test;nextStop=no stop;messageText=this is test message from bus to land;mac=123123123;lat=52.3343434;lon=12.93744";


            //ServiceManager.MessagePing(pingData);

            IBIService.IBIServiceClient client = new IBIService.IBIServiceClient();
            //string pingData = "bus2land:customerid=2140;mac=mac123;busNumber=1223;timestamp=12/12/2012 11:11:55;journeynumber=;title=Mere end 5 minutter forsinket;messageText=Mere end 5 minutter forsinket;lat=0.0;lon=0.0;line=6A;destination=Emdrup Torv;nextStop=null;";

            string pingData = "customerId=2140;mac=0060e04fe271;busNumber=9999;timestamp=" + DateTime.Now.AddMinutes(-10).ToString("MM/dd/yyyy HH:mm:ss") + ";journeynumber=;title=Mere end 5 minutter forsinket;messageText=Mere end 5 minutter forsinket;lat=;lon=;line=6A;destination=Emdrup Torv;nextStop=;";


            return Json(client.MessagePing(pingData), JsonRequestBehavior.AllowGet);
        }


        public JsonResult JourneyPing() {

            string pingData = "busNumber=1223;customerId=2140;mac=0060e04fe271;line=201A;journeyNumber=23;fromName=Svogerslev, Søbredden;destinationName=Trekroner st.;stopNumber=9025200000028563;stopName=Kongemarksvej;clientTime=02/01/2013 10:23:39;lat=55.6336847666371;lon=12.0087277520711;zone=8;isOnDestination=0;arrivalTime=02/01/2013 10:23:28;departureTime=02/01/2013 10:23:39;timeDifference=6825;currentPosLine=null;currentPosDestination=null;currentPosZone=null;currentPosLatitude=0.0;currentPosLongitude=0.0;noScheduleWarning=false;showScheduleNotice=true;plannedArrivalTime=02/01/2013 10:23:29;plannedDepartureTime=null;stopPlannedDepartureTime=02/01/2013 10:23:29";
            
            IBIService.IBIServiceClient client = new IBIService.IBIServiceClient();

            return Json(client.JourneyPing(pingData), JsonRequestBehavior.AllowGet);
        
        }


        [HttpGet]
        public JsonResult GetTree(string groupId = "0")
        {

            BusTreeService.BusTreeServiceClient srv = new BusTreeService.BusTreeServiceClient();
            Tree t = srv.GetSimpleBusTree("3", groupId, "0", "false");
            return Json(t.Groups, JsonRequestBehavior.AllowGet);

            //Node Tree= new Node("0", "Root");
            //Node nd1 = Tree.Add("1", "1st");
            //Node nd2 = Tree.Add("2", "2nd");
            //Node nd3 = Tree.Add("3", "3rd");

            //nd1.Add("11", "11");
            //nd1.Add("111", "111");
            //nd1.Add("1111", "1111");


            //nd2.Add("22", "22");
            //nd2.Add("222", "222");
            //nd2.Add("2222", "2222");


            //nd3.Add("33", "33");
            //nd3.Add("333", "333");
            //nd3.Add("3333", "3333");
             
            //return Json(Tree, JsonRequestBehavior.AllowGet);
        }
         
        public ActionResult Tree()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SimpleTree(int[] checkedNodes)
        {
            return View(checkedNodes);
        }

        public JsonResult IncomingMessages()
        {
            List<IBI.Shared.Models.Messages.IncomingMessage> list =  ServiceManager.GetIncomingMessages(3);

            return Json(list, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AudioFiles(string stopName)
        {
            List<IBI.Shared.Models.TTS.AudioFile> model= ServiceManager.GetStopAudioFiles(stopName,true, false, false);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ContribGrid()
        //{

        //    List<IBI.Shared.Models.BusTree.BusDetail> list = Infrastructure.ServiceManager.GetBuses(2140);
        //    return View(list);
        //}
         
         
        public ActionResult About()
        {
            return View();
        }

        public ActionResult ComingSoon()
        {

            return View();
        }

        
       
           
    }


    public class Node
    {
        public string Id{get; set;}
        public string Text{get; set;}
        public bool hasChildren { get; set; }

        public List<Node> Items { get; set; }

        public Node() { }

        public Node(string id, string text)
        {
            this.Id = id;
            this.Text = text;
            this.Items = new List<Node>();
        }

        public Node Add(Node item) 
        {
            this.Items.Add(item);
            return item;
        }

        public Node Add(string id, string text)
        {
            Node n = new Node(id, text);
            this.Items.Add(n);
            return n;
        }
    }

}
