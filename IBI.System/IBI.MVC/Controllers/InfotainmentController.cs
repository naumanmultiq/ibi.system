﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.ViewModels.Infotainment;
using MvcContrib.Pagination;

namespace IBI.Web.Controllers
{
    [AuthorizeLogin]
    public class InfotainmentController : Controller
    {
        private int columnLayoutCount = 6;

        public ActionResult Status()
        {
            DataTable tbl = GetDumps();

            List<VTCScreenDumb> list = new List<VTCScreenDumb>();
            foreach (DataRow dr in tbl.Rows)
            {
                VTCScreenDumb dumb = new VTCScreenDumb();
                dumb.C1 = dr[0].ToString();
                dumb.C2 = dr[1].ToString();
                dumb.C3 = dr[2].ToString();
                dumb.C4 = dr[3].ToString();
                dumb.C5 = dr[4].ToString();
                dumb.C6 = dr[5].ToString();

                list.Add(dumb);
            }

            var pagedList = list.AsPagination(1, 1000);

            return View(pagedList);
        }

        #region Helper functions

        private DataTable GetDumps()
        {
            List<string> busNumbers = Common.CurrentUser.BusListHavingVTC; //ServiceManager.GetBusNumbers(0, true, true);

            DataTable table = new DataTable();

            for (int i = 0; i < columnLayoutCount; i++)
            {
                table.Columns.Add("Col" + i, typeof(String));
            }

            DataRow row = null;

            int colCounter = 0;

            foreach (string busNumber in busNumbers)
            {
                if (colCounter == 0)
                {
                    row = table.NewRow();
                    table.Rows.Add(row);
                }

                row[colCounter] = busNumber + "|" + ResolveImageName(busNumber);

                colCounter++;

                if (colCounter % columnLayoutCount == 0)
                    colCounter = 0;
            }

            return table;
        }

        private String ResolveImageName(string busNumber)
        {
            String latestImageName = "";
            String latestImageDate = "";
            String folderName = "../Resources/Screenshots/VTC/" + busNumber + "/";

            if (Directory.Exists(Server.MapPath(folderName)))
            {
                String[] imagePaths = Directory.GetFiles(Server.MapPath(folderName), "Screenshot_" + busNumber + "_VTC_" + DateTime.Now.ToString("yyyyMMdd") + "*.jpg");

                DateTime latest = DateTime.MinValue;

                for (int i = imagePaths.Length - 1; i >= 0; i--)
                {
                    String imagePath = imagePaths[i];
                    string date2Compare = Path.GetFileNameWithoutExtension(imagePath).Substring(Path.GetFileNameWithoutExtension(imagePath).Length - 15);

                    int year = int.Parse(date2Compare.Substring(0, 4));
                    int month = int.Parse(date2Compare.Substring(4, 2));
                    int day = int.Parse(date2Compare.Substring(6, 2));
                    int hour = int.Parse(date2Compare.Substring(9, 2));
                    int minute = int.Parse(date2Compare.Substring(11, 2));
                    int second = int.Parse(date2Compare.Substring(13, 2));

                    DateTime curImgDate = new DateTime(year, month, day, hour, minute, second);

                    if (curImgDate > latest)
                    {
                        latest = curImgDate;

                        if (DateTime.Now.Subtract(TimeSpan.FromHours(1)) < curImgDate)
                        {
                            latestImageName = folderName + Path.GetFileName(imagePath);

                            latestImageDate = curImgDate.ToString("HH:mm");
                        }
                    }
                }
            }

            return latestImageName + "|" + latestImageDate;
        }

        #endregion Helper functions
    }
}