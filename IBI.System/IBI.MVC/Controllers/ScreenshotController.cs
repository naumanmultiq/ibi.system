﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using MvcContrib.UI.Grid;
using IBI.Web.ViewModels.BusTree.Screenshots;
using System.Data;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Common;

namespace IBI.Web.Controllers
{ 
    [AuthorizeLogin]
    public class ScreenshotController : Controller
    {
        //
        // GET: /Screenshot/

        //public ActionResult Index(string busNumber)
        //{
        //    //ViewData["BusNumber"] = busNumber;
        //    return View();
        //}
         
        [HttpGet]
        public ActionResult Frame(string busNumber, string clientType)
        {
            ViewData["BusNumber"] = busNumber;
            ViewData["ClientType"] = clientType;
            return View();
        }
         
        public ActionResult Index(
            string busNumber,
            GridSortOptions gridSortOptions,
            int? page,
            string clientType = "DCU")
        {

            if (busNumber == "" && ViewData["BusNumber"] != null)
                busNumber = ViewData["BusNumber"].ToString();
            
            if (clientType == "" && ViewData["ClientType"] != null)            
                clientType = ViewData["ClientType"].ToString();

            ViewData["BusNumber"] = busNumber;
            ViewData["ClientType"] = clientType;

            var list = GetImages(busNumber, clientType);

            // Set default sort column
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "ImagePath";
                gridSortOptions.Direction = SortDirection.Ascending;
            }


            int defaultPage = list.Count() > 0 ? list.Count : 1;
            // Order and page the product list
            var pagedList = list
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                .AsPagination(page ?? defaultPage, 1);


            var listContainer = new ListContainerViewModel
            {
                PagedList = pagedList,
                GridSortOptions = gridSortOptions
            };

            return View(listContainer);
        }  

       
        #region private helper functions

        private List<Screenshot> GetImages( string busNumber, string clientType, string wantedImage="")
        {
            List<Screenshot> screenshotList = new List<Screenshot>();

            String folderName = "../Resources/Screenshots/" + clientType + "/" + busNumber + "/";

            //this.CllImageName.InnerText = Server.MapPath(folderName);
            string imageIndex = "0";

            if (Directory.Exists(Server.MapPath(folderName)))
            {
                String[] imageList = Directory.GetFiles(Server.MapPath(folderName), "*.jpg");
                
                for (int i = 0; i < imageList.Length; i++)
                {
                    String imagePath = imageList[i];

                    imageIndex = i.ToString();

                    //extract time from image
                   
                        string date2Compare = Path.GetFileNameWithoutExtension(imagePath).Substring(Path.GetFileNameWithoutExtension(imagePath).Length - 15);

                        int year = int.Parse(date2Compare.Substring(0, 4));
                        int month = int.Parse(date2Compare.Substring(4, 2));
                        int day = int.Parse(date2Compare.Substring(6, 2));
                        int hour = int.Parse(date2Compare.Substring(9, 2));
                        int minute = int.Parse(date2Compare.Substring(11, 2));
                        int second = int.Parse(date2Compare.Substring(13, 2));

                       // DateTime newDate = new DateTime(year, month, day, hour, minute, second);

                        
                     DateTime imgDt = new DateTime(year, month, day, hour, minute, second);

                    Screenshot sshot = new Screenshot();
                    sshot.BusNumber = busNumber;
                    sshot.ClientType = clientType;
                    sshot.ImageIndex = imageIndex;
                    sshot.ImagePath = folderName + Path.GetFileName(imagePath);
                    sshot.ImageDate = imgDt.ToString(Settings.DateTimeFormat);       

                    screenshotList.Add(sshot); 

                }
            }

            return screenshotList.OrderByDescending(x => x.ImagePath).ToList(); 
        } 
         
        
        #endregion
    }
}
