﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
//using IBI.Web.ViewModels.Admin.Destinations;
using MvcContrib.Pagination;
using IBI.Shared.Models.Signs;
using System.Xml.Linq;
using System.Xml;
using System.Collections;

using MvcContrib.Sorting;
using MvcContrib.UI.Grid;
using IBI.Web.Infrastructure;
using IBI.Shared.Models.Destinations;
using System.IO;
using IBI.Web.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.CustomActionResults;
using IBI.Shared;
using IBI.Shared.Models.SignAddons;
using IBI.Web.ViewModels.Admin.SignAddons;

namespace IBI.Web.Controllers
{
    [AuthorizeLogin]
    public class SignAddonController : Controller
    {
        public ActionResult SignAddonImages(int signAddonDataItemId)
        {
            List<SignXmlLayout> signXmlLayouts = ServiceManager.GetSignAddonImages(signAddonDataItemId);

            SignAddon signAddon = ServiceManager.GetSignAddonItem(signAddonDataItemId);

            var model = new SignAddonDataModel();
            model.MainText = signAddon.SignText;
            model.SubText = signAddon.UdpText;
            model.Line = signAddon.Line;
            model.CustomerId = signAddon.CustomerId;
            model.SignXmlLayout = signXmlLayouts;
            Session["signLayoutImagesData"] = signXmlLayouts;

            /*

            Hashtable signLayoutImagesData = new Hashtable();

            if (signXmlLayouts != null)
            {
                if (signXmlLayouts.Count > 0)
                {
                    foreach (SignXmlLayout signXmlLayout in signXmlLayouts)
                    {
                        string signLayout = signXmlLayout.Type + "-  " + signXmlLayout.Width + "× " + signXmlLayout.Height + "  " + signXmlLayout.Manufacturer + "  -  " + signXmlLayout.Technology ;
                        signLayout += "$" + signId + "," + signXmlLayout.LayoutId + "," + signXmlLayout.GraphicRuleId + "," + signXmlLayout.CustomerId;
                        //string signLayout = signId + "," + signXmlLayout.LayoutId + "," + signXmlLayout.GraphicRuleId + "," + signXmlLayout.CustomerId;
                        List<string> base64Images;

                        if (signLayoutImagesData.ContainsKey(signLayout))
                            base64Images = (List<string>)signLayoutImagesData[signLayout];
                        else
                            base64Images = new List<string>();

                        base64Images = base64Images.Union(Getbase64Images(signXmlLayout.Signs)).ToList();
                        signLayoutImagesData[signLayout] = base64Images;
                    }
                    Session["signLayoutImagesData"] = signLayoutImagesData;
                    model.Base64Images = signLayoutImagesData;
                }
            }
             * */
            return View(model);
        }

        public ActionResult ManualSignEditor()
        {
            return View();
        }
        
        public ActionResult SignEditor()
        {
            return View();
        }
                
        [HttpPost]
        public ActionResult UploadSign(IEnumerable<HttpPostedFileBase> files, FormCollection formCollection)
        {
            foreach (HttpPostedFileWrapper file in files)
            {
                Stream stream = file.InputStream;

                int imageposition = int.Parse(formCollection["imageposition"].ToString());
                int layoutHeight = int.Parse(formCollection["layoutHeight"].ToString());
                int layoutWidth = int.Parse(formCollection["layoutWidth"].ToString());

                SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];

                string grayScalePNGFormat = IsBinaryImageAndPNGFormat(stream, layoutHeight, layoutWidth);

                if (string.IsNullOrEmpty(grayScalePNGFormat))
                {
                    string base64Image = WebUtil.CreateBase64Image(stream);
                    string SignImage = Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(base64Image)); // WebUtil.ConvertBase64toPicture(base64Image);
                    string base64ImageLarge = Convert.ToBase64String(AppUtility.ConvertBase64toLEDImageLarge(base64Image));                   
                    
                    model.Signs[imageposition].base64ViewString = SignImage;
                    model.Signs[imageposition].Base64String = base64Image;
                    model.Signs[imageposition].base64ViewLargeString = base64ImageLarge;
                    model.Signs[imageposition].SignImage = SignImage;


                    Session["ManualSignDataModel"] = model;
                    return Json(new { PostResult = true, SignImage = SignImage });
                }
                else
                {
                    return Json(new SignAddonManualSignDataModel { PostResult = false, ErrorMessage = grayScalePNGFormat });
                }
            }
            return Json(new SignAddonManualSignDataModel { PostResult = false, ErrorMessage = "Unknown error" });
        }

        [HttpPost]
        public ActionResult AddSign(int layoutWidth, int layoutHeight, int signAddonItemId)
        {
            SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];
            Stream stream = WebUtil.createBlankImage(layoutWidth, layoutHeight);
            string base64Image = WebUtil.CreateBase64Image(stream);

            //string SignImage = WebUtil.ConvertBase64toPicture(base64Image);
            //imgItem.SignImage = SignImage;

            SignDataImageItem imgItem = new SignDataImageItem();
            imgItem.Base64String = base64Image;

            Byte[] bytes = AppUtility.ConvertBase64toLEDImage(base64Image);
            string ledbase64 = Convert.ToBase64String(bytes);
            imgItem.base64ViewString = ledbase64;
            imgItem.SignImage = ledbase64;

            bytes = AppUtility.ConvertBase64toLEDImageLarge(base64Image);
            ledbase64 = Convert.ToBase64String(bytes);
            imgItem.base64ViewLargeString = ledbase64;

            imgItem.SignDataId = signAddonItemId; //model.Signs.FirstOrDefault().SignDataId;

            model.Signs.Add(imgItem);
            Session["ManualSignDataModel"] = model;
            return PartialView("PartialManualSignEditor", model);
        }
        
        [HttpPost]
        public ActionResult DeleteSign(string imagposition)
        {
            try
            {
                int signPos = int.Parse(imagposition);
                SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];
                model.Signs.RemoveAt(signPos);
                Session["ManualSignDataModel"] = model;
                return PartialView("PartialManualSignEditor", model);
            }
            catch(Exception ex)
            {
                return PartialView("PartialManualSignEditor", Session["ManualSignDataModel"]);
            }
        }
        
        [HttpPost]
        public ActionResult SwapSign(string swapId)
        {
            int sId = int.Parse(swapId);
            SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];

            //model.Signs.RemoveAt(sId);

            SignDataImageItem imgItemsOne = model.Signs[sId - 1];
            SignDataImageItem imgItemsTwo = model.Signs[sId];

            model.Signs[sId - 1] = imgItemsTwo;
            model.Signs[sId] = imgItemsOne;

            Session["ManualSignDataModel"] = model;
            return PartialView("PartialManualSignEditor", model);

        }

        [HttpPost]
        public ActionResult SaveSign()
        {
            SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];

            XDocument xdoc = new XDocument(
                             new XDeclaration("1.0", "utf-8", "yes"),
                             new XElement("signs"));

            xdoc.Root.Add(new XElement("images", new XAttribute("count", model.Signs.Count()),
                             model.Signs.Select((x, index) => new XElement("image", new XAttribute("page", index + 1), new XAttribute("base64", x.Base64String)))
                            ));



            XDocument xdocLarge = new XDocument(
                 new XDeclaration("1.0", "utf-8", "yes"),
                 new XElement("signs"));

            xdocLarge.Root.Add(new XElement("images", new XAttribute("count", model.Signs.Count()),
                             model.Signs.Select((x, index) => new XElement("image", new XAttribute("page", index + 1), new XAttribute("base64", x.Base64String), new XAttribute("base64View", x.base64ViewString), new XAttribute("base64ViewLarge", x.base64ViewLargeString)))
                            ));


            SignXmlData signData = new SignXmlData();
            signData.SignId = model.SignId.ToString();

            signData.Base64 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xdoc.ToString(SaveOptions.DisableFormatting);//signXML.ToString();
            signData.Page = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xdoc.ToString(SaveOptions.DisableFormatting);//signXML.ToString();

            ServiceManager.SaveSignAddonDataImages(model.LayoutId,signData);
            
            return PartialView("PartialManualSignEditor", model);

        }

        [HttpGet]
        public FileResult Download(string imageposition)
        {
            int imagpos = int.Parse(imageposition);
            SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];
            string base64String = model.Signs[imagpos].Base64String;
            byte[] fileBytes = Convert.FromBase64String(base64String);
            string fileName = "SignData.png";
            return base.File(fileBytes, "image/png", fileName);
        }

        //[HttpGet]
        //public FileResult Download(string imageposition)
        //{
        //    int imagpos = int.Parse(imageposition);
        //    SignAddonManualSignDataModel model = (SignAddonManualSignDataModel)Session["ManualSignDataModel"];
        //    string base64String = model.Signs[imagpos].Base64String;
        //    byte[] fileBytes = WebUtil.BlackAndWhite(base64String);
        //    //byte[] filee = fileByte;
        //    //byte[] fileBytes = WebUtil.ConvertBase64toBWPicture(base64String); //Convert.FromBase64String(base64String);
        //    string fileName = "SignData";
        //    return File(fileBytes, "image/png", fileName);
        //}
        
        [HttpGet]
        public ActionResult SignDataFrame(int LayoutId, string layoutCurrent, string base64Image)
        {
            ViewData["LayoutId"] = LayoutId;
            ViewData["LayoutCurrent"] = layoutCurrent;
            ViewData["Base64Image"] = base64Image;
            Session["SignDataScreenshot"] = null;
            return  View();
            //return RedirectToAction(defaultAction[0], defaultAction[1]); 
        }

        [HttpGet]
        public ActionResult SignDataIndex(
            string LayoutId, 
            string layoutCurrent,
            string base64Image,
            GridSortOptions gridSortOptions,
            int? page
            )
        {

            if (layoutCurrent == "" && ViewData["LayoutCurrent"] != null)
                layoutCurrent = ViewData["LayoutCurrent"].ToString();

            //if (base64Image == "" && ViewData["Base64Image"] != null)
            //    base64Image = ViewData["Base64Image"].ToString();

            if (LayoutId == "" && ViewData["LayoutId"] != null)
                LayoutId = ViewData["LayoutId"].ToString();

            ViewData["LayoutCurrent"] = layoutCurrent;
            ViewData["Base64Image"] = base64Image;
            ViewData["LayoutId"] = LayoutId;

            var list = GetSignDataScreenshots(int.Parse(LayoutId));

            // Set default sort column
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "Image";
                //gridSortOptions.Direction = SortDirection.Ascending;
            }
            if (page == null)
            {
                int index = int.Parse(base64Image); ;// list.IndexOf(list.Where(x => x.Layout == layoutCurrent && x.Image == base64Image).FirstOrDefault());
                page = index + 1;
            }


            int defaultPage = list.Count() > 0 ? list.Count : 1;
            // Order and page the product list
            var pagedList = list
                //.OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                .AsPagination(page ?? defaultPage, 1);


            var listContainer = new SignAddonDataListContainerModel
            {
                PagedList = pagedList,
                //GridSortOptions = gridSortOptions
            };

            return View(listContainer);
        }
        
        [HttpGet]
        public ActionResult SignImageEditor(int signAddonItemId, int layoutId,int graphicRuleId)
        {
            //string[] arr = key.Split(',');

            //int signId;
            //int layoutId;
            //int graphicRuleId;

            //signId = arr.Length > 1 ? int.Parse(arr[0]) : -1;
            //layoutId = arr.Length > 2 ? int.Parse(arr[1]) : -1;
            //graphicRuleId = arr.Length > 3 ? int.Parse(arr[2]) : -1;

            Session["graphicRuleId"] = graphicRuleId;

            SignGraphicRule SignGraphicRule = Infrastructure.ServiceManager.GetSignGraphicRule(graphicRuleId);
            var model = new ViewModels.Admin.SignAddons.SignAddonImageEditorModel(signAddonItemId, layoutId, graphicRuleId, SignGraphicRule.template);

            if (model.ImageGenerator != null)
                Session["ImageGeneratorPager"] = model.ImageGenerator.ImagePager;
            else
                Session["ManualSignDataModel"] = model.ManualSignData;
            return View("SignEditor", model);
        }


        [HttpPost]
        public ActionResult GetNextImage()
        {
            SignAddonImageGeneratorPager imagepager = (SignAddonImageGeneratorPager)Session["ImageGeneratorPager"];
            imagepager.Index = (imagepager.Index + 1 == imagepager.Img.Count) ? imagepager.Index : imagepager.Index + 1;  //(imagepager.Index + 1 ) % imagepager.Img.Count;
            Session["ImageGeneratorPager"] = imagepager;
            return PartialView("PartialPager", imagepager);
        }

        [HttpPost]
        public ActionResult GetPreImage()
        {
            SignAddonImageGeneratorPager imagepager = (SignAddonImageGeneratorPager)Session["ImageGeneratorPager"];
            imagepager.Index = (imagepager.Index - 1) < 0 ? 0 : imagepager.Index - 1;
            Session["ImageGeneratorPager"] = imagepager;
            return PartialView("PartialPager", imagepager);
        }


        [HttpPost]
        public ActionResult GetImagesPreview(SignAddonImageGeneratorFormDataModel model)
        {
            SignGraphicRule signGraphicRule;
            int graphicRuleId = int.Parse(model.SelectedTemplate);
            if (graphicRuleId == 0)
            {
                signGraphicRule = new SignGraphicRule
                {
                    AllCaps = model.AllCaps,
                    AniDelay = model.AniDelay,
                    Center = model.MainCenter,
                    DownScale = model.DownScale,
                    GraphicsRuleId = model.GraphicsRuleId,
                    LineArea = string.IsNullOrEmpty(model.LineArea) ? "0" : model.LineArea,
                    LineDivider = string.IsNullOrEmpty(model.LineDivider) ? "0" : model.LineDivider,
                    LineFont = (model.LineFont == null || model.LineFont.EndsWith(".ttf")) ? model.LineFont : (model.LineFont + ".ttf"),
                    LineFontSize = model.LineFontSize,
                    LineX = model.LineStartX,
                    LineY = model.LineStartY,
                    MainFont = (model.MainFont == null || model.MainFont.EndsWith(".ttf")) ? model.MainFont : (model.MainFont + ".ttf"),
                    MainFontSize = model.MainFontSize,
                    MainStartX = model.MainStartX,
                    MainStartY = model.MainStartY,
                    Note = model.Note,
                    ShowErrors = model.ShowErrors,
                    ShowGrid = model.ShowGrid,
                    SubFont = (model.SubFont == null || model.SubFont.EndsWith(".ttf")) ? model.SubFont : (model.SubFont + ".ttf"),
                    SubFontSize = model.LineFontSize,
                    SubStartX = model.SubStartX,
                    SubStartY = model.SubStartY,
                    template = (model.SelectedTemplate != "0") ? model.SelectedTemplateName : null
                };
            }
            else
                signGraphicRule = ServiceManager.GetSignGraphicRule(graphicRuleId);

            List<string> ImageList = ServiceManager.GetSignAddonImagePreview(model.SignId, signGraphicRule, model.layoutId);

            SignAddonImageGeneratorPager imagepager = new SignAddonImageGeneratorPager();
            imagepager.Img = ImageList;
            imagepager.Index = 0;
            imagepager.Count = ImageList.Count;
            Session["ImageGeneratorPager"] = imagepager;
            return PartialView("PartialPager", imagepager);

        }

        [HttpPost]
        public ActionResult SaveImageGeneratorData(SignAddonImageGeneratorFormDataModel model)
        {
            int graphicRuleId;
            if (model.SelectedTemplate == "0")
            {
                SignGraphicRule signGraphicRule = new SignGraphicRule
                {
                    AllCaps = model.AllCaps,
                    AniDelay = model.AniDelay,
                    Center = model.MainCenter,
                    DownScale = model.DownScale,
                    GraphicsRuleId = model.GraphicsRuleId,
                    //LineArea = string.IsNullOrEmpty(model.LineArea) ? "0" : model.LineArea,
                    //LineDivider = string.IsNullOrEmpty(model.LineDivider) ? "0" : model.LineDivider,
                    //LineFont = (model.LineFont.EndsWith(".ttf")) ? model.LineFont : (model.LineFont + ".ttf"),
                    //LineFontSize = model.LineFontSize,
                    //LineX = model.LineStartX,
                    //LineY = model.LineStartY,
                    MainFont = (model.MainFont.EndsWith(".ttf")) ? model.MainFont : (model.MainFont + ".ttf"),
                    MainFontSize = model.MainFontSize,
                    MainStartX = model.MainStartX,
                    MainStartY = model.MainStartY,
                    Note = model.Note,
                    ShowErrors = model.ShowErrors,
                    //ShowGrid = model.ShowGrid,
                    //SubFont = (model.SubFont.EndsWith(".ttf")) ? model.SubFont : (model.SubFont + ".ttf"),
                    //SubFontSize = model.LineFontSize,
                    //SubStartX = model.SubStartX,
                    //SubStartY = model.SubStartY,
                    //template = model.SelectedTemplateName
                    template = (model.SelectedTemplate != "0") ? model.SelectedTemplateName : null
                };
                graphicRuleId = ServiceManager.SaveGraphicRule(signGraphicRule);
            }
            else
                graphicRuleId = int.Parse(model.SelectedTemplate);
            Session["graphicRuleId"] = graphicRuleId;
            ServiceManager.SaveSignAddonImageGeneratorData(model.layoutId, model.SignId, graphicRuleId);
            return Content("true");

        }

        [HttpPost]
        public ActionResult GetFontSizes(int customerId , string fontType , string fontName)
        {
            //ImageGeneratorSettings.ImageSettingParsing(customerId);
            SelectList retValue = null;
            switch (fontType.ToLower())
            {
                case "main":
                    retValue = ImageGeneratorSettings.MainFontsSizes(fontName);
                    break;
                case "line":
                    retValue = ImageGeneratorSettings.LineFontsSizes(fontName);
                    break;
                case "sub":
                    retValue = ImageGeneratorSettings.SubFontsSizes(fontName);
                    break;
                default:
                    retValue = new SelectList(new List<string>());
                    break;
            }
            string complete_html = "";
            foreach(SelectListItem item in retValue){
                complete_html += string.Format("<option>{0}</option>", item.Text);
            }
            return Content(complete_html);
        }


        [HttpPost]
        public ActionResult ImageGeneratorEditorSettings(int ObjectId,string ObjectType)
        {
            SignAddonImageGeneratorModel ImageGenerator = new SignAddonImageGeneratorModel(ObjectId, ObjectType);
            //return PartialView("PartialImageEditorMode", ImageGenerator);
            return PartialView("PartialImageGeneratorSettings", ImageGenerator);
        }

        [HttpPost]
        public ActionResult ImageGeneratorEditor(int layoutId, int signId,string mode)
        {
            int graphicRuleId = (int)Session["graphicRuleId"];
            var model = new ViewModels.Admin.SignAddons.SignAddonImageEditorModel(signId, layoutId, graphicRuleId, mode);
            Session["ImageGeneratorPager"] = model.ImageGenerator.ImagePager;
            


            //    ImageGeneratorSettings.ImageSettingParsing(this.CustomerId);
            //    this.ImageGeneratorModel(graphidRuleId);
            //    this.Base64Images(this.CustomerId, signLayoutId, signId);
            //    this.Mode = IMAGE_GENERATOR;
                //("PartialImageEditorMode",Model.ImageGenerator)

            return PartialView("PartialImageEditorMode", model.ImageGenerator);
        }


        SignAddonManualSignDataModel ManualSignEditorModel(int customerId, int layoutId, int signId)
        {
            List<SignDataImageItem> signImages = ServiceManager.GetSignAddonImages(customerId, layoutId, signId);
            SignAddonManualSignDataModel model = new SignAddonManualSignDataModel();
            model.LayoutHeight = 500;
            model.LayoutWidth = 500;
            model.Signs = signImages;
            model.LayoutId = layoutId;
            model.SignId = signId; //signDataId
            return model;
        }


        [HttpPost]
        public ActionResult ManualSignEditor(int customerId, int layoutId, int signId)
        {
           Session["ManualSignDataModel"] = ManualSignEditorModel(customerId, layoutId, signId);
           return PartialView("PartialManualSignEditorMode", (SignAddonManualSignDataModel)Session["ManualSignDataModel"]);
        }
        
        [HttpPost]
        public JsonResult SignImageEditor()
        {
            return Json("", JsonRequestBehavior.AllowGet);
        }

        #region Helper functions

        private List<SignAddonDataScreenshot> GetSignDataScreenshots(int layoutId)
        {
            if (Session["SignDataScreenshot"] != null)
            {
                return (List<SignAddonDataScreenshot>)Session["SignDataScreenshot"];
            }
            List<SignAddonDataScreenshot> screenshotList = new List<SignAddonDataScreenshot>();

            List<SignXmlLayout> signXmlLayouts = (List<SignXmlLayout>)Session["signLayoutImagesData"];
            signXmlLayouts = signXmlLayouts.Where(x => x.LayoutId == layoutId).ToList();
            if (signXmlLayouts != null)
            {
                if (signXmlLayouts.Count > 0)
                {
                    string imageIndex = "1";

                    foreach (var signXmlLayout in signXmlLayouts)
                    {
                        SignXmlData[] signXmlDatas = signXmlLayout.Signs;
                        if (signXmlDatas != null && signXmlDatas.Length > 0)
                        {
                            foreach (SignXmlData signXmlData in signXmlDatas)
                            {
                                XDocument xdoc = XDocument.Parse(signXmlData.Base64.ToString());
                            
                                var base64Images = (from x in xdoc.Descendants("image")
                                                        select new
                                                        {
                                                            base64 =  x.Attribute("base64").Value,
                                                            base64View = (x.Attribute("base64View") == null ? Convert.ToBase64String(IBI.Shared.AppUtility.ConvertBase64toLEDImage(x.Attribute("base64").Value)) : x.Attribute("base64View").Value),
                                                            base64ViewLarge = (x.Attribute("base64ViewLarge") == null ? Convert.ToBase64String(IBI.Shared.AppUtility.ConvertBase64toLEDImageLarge(x.Attribute("base64").Value)) : x.Attribute("base64ViewLarge").Value)
                                                        }
                                                   ).ToList();
                                foreach (var base64Image in base64Images)
                                {
                                    SignAddonDataScreenshot sshot = new SignAddonDataScreenshot();
                                    //sbOutput += "<div class='PixelImage'><a href='javascript:void(0);' onclick=\"ShowScreenshotsSignData('" + signXmlLayout.Type + "-  " + signXmlLayout.Width + "× " + signXmlLayout.Height + "  " + signXmlLayout.Manufacturer + "  -  " + signXmlLayout.Technology + "','" + base64Image.base64 + "')\"><img src='data:image/png;base64," + base64Image.base64View + "'/></a></div>";
                                    sshot.Layout = signXmlLayout.Type + "-  " + signXmlLayout.Width + "× " + signXmlLayout.Height + "  " + signXmlLayout.Manufacturer + "  -  " + signXmlLayout.Technology;  //signLayout;
                                    sshot.Image = base64Image.base64ViewLarge; //"<img src='data:image/png;base64," + base64Image.base64View + "'/>";
                                    sshot.ImageIndex = imageIndex;
                                    screenshotList.Add(sshot);
                                    imageIndex = (Convert.ToInt32(imageIndex) + 1).ToString();
                                }
                            }
                        }
                    }
                }
            }
            Session["SignDataScreenshot"] = screenshotList.OrderBy(x => x.Layout).ToList();
            return (List<SignAddonDataScreenshot>)Session["SignDataScreenshot"];
        } 

        private List<string> Getbase64Images(SignXmlData[] signs)
        {
            List<string> base64Images = new List<string>();
            foreach(SignXmlData sign in signs)
            {
                if (!string.IsNullOrEmpty(sign.Base64))
                {
                    XDocument xDoc = XDocument.Parse(sign.Base64);
                    List<string> images = (from c in xDoc.Descendants("image") select c.Attribute("base64").Value).ToList<string>();
                    base64Images.AddRange(images);
                }
            }
            return base64Images;
        }
        

        private static string IsBinaryImageAndPNGFormat(Stream image, int layoutHeight, int layoutWidth)
        {
            string strRet = string.Empty;
            try
            {
                using (Image img = Image.FromStream(image))
                {
                    if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png))
                    {
                        Bitmap bmp = (Bitmap)img;
                        int bmpHeight = bmp.Height;
                        int bmpWidth = bmp.Width;

                        if (bmpHeight == layoutHeight && bmpWidth == layoutWidth)
                        {
                            for (int j = 0; j < bmpHeight; j++)
                            {
                                for (int i = 0; i < bmpWidth; i++)
                                {
                                    Color color = bmp.GetPixel(i, j);
                                    //if (color.A != 0 && (color.R != color.G || color.G != color.B))
                                    if ((!(color.R == 0 && color.G == 0 && color.B == 0)) && (!(color.R == 255 && color.G == 255 && color.B == 255)))
                                    {
                                        strRet = "Uploaded picture is not pure black and white";
                                        return strRet;
                                    }
                                }
                            }
                        }
                        else
                            strRet = "Uploading image is not as per layout size";
                    }
                    else
                        strRet = "Uploading image is not PNG Format";
                }
            }
            catch (Exception ex)
            {
                strRet = "Uploading image is not PNG Format/ Invalid Image";
            }
            return strRet;
        }

        public static string RenderRazorViewToString(string viewName, object model, ControllerContext controllerContext, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            viewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion Helper functions
    }
}
