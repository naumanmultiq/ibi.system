﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using IBI.Shared;
using IBI.Shared.Models;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Shared.Models.Accounts;
//using System.Web.Http;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Converters;
using IBI.Web.Common;
using System.Xml.Serialization; 
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace IBI.Web.Infrastructure
{
    public enum PostFormat
    {
        Json = 1,
        Xml = 2
    }
    
   
    public class RESTBase 
    {
        //For REST -> GET call only
        protected  virtual T Get<T>(Uri baseAddress, string uriTemplate)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();

            string url = baseAddress.AbsoluteUri  + uriTemplate; 

            webClient.Encoding = System.Text.Encoding.UTF8; //fix for invalid danish chars
            
            string result = webClient.DownloadString(url);

            T retObj = JsonConvert.DeserializeObject<T>(result);

            return retObj;
        }

        //For REST -> POST call only
        protected  virtual T Post<T, TPost>(Uri baseAddress, string uriTemplate, TPost data, PostFormat postFormat) 
        {
            //POST THROUGH HttpClient   *******************************

            HttpClient client = new HttpClient();
            client.BaseAddress = baseAddress;

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/" + postFormat));

            HttpResponseMessage response = client.PostAsJsonAsync<TPost>(uriTemplate, data).Result;
                                                   
            if (response.IsSuccessStatusCode)
            {
                return (response.Content.ReadAsAsync<T>().Result);
            }
            else
                return default(T);           
        }
    }

    //Sample uriTemplate = bustree/" + uid
    public class RESTManager : RESTBase
    {

        private static string REST_PATH = System.Configuration.ConfigurationManager.AppSettings["REST_PATH"].ToString();

        //BaseAddresses
        private static Uri SystemService = new Uri(REST_PATH + "/Core/Xml/SystemService.svc/");
        private static Uri BusTreeService = new Uri(REST_PATH + "/BusTree/BusTreeService.svc/");
        private static Uri UserAuthService = new Uri(REST_PATH + "/Auth/UserAuthService.svc/");
        private static Uri MessageService = new Uri(REST_PATH + "/Messaging/MessageService.svc/");
        private static Uri JourneyService = new Uri(REST_PATH  + "/Journey/JourneyService.svc/");

        private static Uri TTSFile = new Uri(REST_PATH + "/TTSFile.mp3");

        protected override T Get<T>(Uri baseAddress, string uriTemplate)
        {
            return base.Get<T>(baseAddress, uriTemplate);
        }

        protected override T Post<T, TPost>(Uri baseAddress, string uriTemplate, TPost postData, PostFormat postFormat)
        { 

            string data = "";
            switch (postFormat)
            {
                case PostFormat.Json:
                    {
                        data = JsonConvert.SerializeObject(postData, new IsoDateTimeConverter());
                        break;
                    }
                case PostFormat.Xml:
                    {
                        
                        //data = SerializeInXML<Message>(postData);
                        data = XmlUtility.Serialize<TPost>(postData); 
                        break;
                    }
            }
            
            return base.Post<T, String>(baseAddress, uriTemplate, data, postFormat);
        }

        public static RESTManager Instance
        {
            get
            {
                return new RESTManager();
            }
        }

                          

        #region User Management

        public static Shared.Models.Accounts.Auth Authenticate(string userName, string password, string apiKey)
        {
            return Instance.Get<Auth>(UserAuthService, "Json/Authenticate?u=" + userName + "&p=" + password + "&apiKey=" + apiKey);
        }

        public static Auth Validate(string authToken)
        {
            return Instance.Get<Auth>(UserAuthService, "Json/Validate?authToken=" + authToken);
        }

        #endregion

        #region Journey

        public static List<IBI.Shared.Models.Journey.Journey> GetActiveJourneys(int customerId, string sDate, string eDate, int pageIndex, int pageSize)
        {
            var data = Instance.Get<List<IBI.Shared.Models.Journey.Journey>>(JourneyService, string.Format("GetJourneys?customerId={0}&sDate={1}&eDate={2}&pageIndex={3}&pageSize={4}&jsonp=JSON_CALLBACK", customerId, sDate, eDate, pageIndex, pageSize));
            return data;
        }

        #endregion

        public static Byte[] GetTtsFile(string fileName, string voice)
        {
           // return Instance.Get<Byte[]>(TTSFile, "?voice=" + voice + "&filename=" + fileName);

            System.Net.WebClient webClient = new System.Net.WebClient();

            string url = REST_PATH + "/TTSFile.mp3?voice=" + voice + "&filename=" + fileName;


            byte[] retObj = webClient.DownloadData(url);
            

            return retObj;
        }

        public static string GetCustomMapData(string scheduleId, string minutes)
        {
            // return Instance.Get<Byte[]>(TTSFile, "?voice=" + voice + "&filename=" + fileName);

            System.Net.WebClient webClient = new System.Net.WebClient();

            string url = "http://ibi.mermaid.dk/rest/custommapdata.xml?scheduleId=" + scheduleId + "&minutes=" + minutes;


            string retObj = webClient.DownloadString(url);


            return retObj;
        }


        //not in use .. just for testing.
        public static bool SaveMessage(Message msg){
            return Instance.Post<bool, Message>(MessageService, "XML/SaveMessage", msg, PostFormat.Xml);
        } 

        #region Helper Functions
         
        #endregion
    }      
           
}