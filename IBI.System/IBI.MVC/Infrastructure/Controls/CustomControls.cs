﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.Web.Infrastructure.Controls
{
    public class CheckBox
    {
        public string Text { get; set; }
        public bool Checked { get; set; }
        public string title { get; set; }
    }
}