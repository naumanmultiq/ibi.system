﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using IBI.Shared.Models.Accounts;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Keys;


namespace IBI.Web.Infrastructure
{
    public class UserManager
    {
        public static User CurrentUser
        {
            get
            {
                return SessionManager.Get<User>(SessionKeys.LoggedInUser);
            }
        }
    }
}