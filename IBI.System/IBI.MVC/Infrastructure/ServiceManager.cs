﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using IBI.Shared.Models;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Shared.Models.Accounts;
//using System.Web.Http;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Converters;
using IBI.Web.Common;
using System.Reflection;
using IBI.Shared.Models.Signs;
using IBI.Web.Infrastructure.Logger;
using IBI.Shared.Common.Types;
using IBI.Shared.Models.SignAddons;
using IBI.Shared.Models.ScheduleTreeItems;



namespace IBI.Web.Infrastructure
{
    public class ServiceBase
    {
        //for future enhancements          
    }

    public class ServiceManager : ServiceBase
    {
        //returns the instance of given service type
        private static T Get<T>()
        {
            T instance = (T)Activator.CreateInstance(typeof(T));
            return instance;
        }

        #region User Auth

        //Authenticates User Credentials
        public static Auth Authenticate(string username, string password, string apiKey)
        {
            UserAuthService.UserAuthServiceClient client = Get<UserAuthService.UserAuthServiceClient>();
            Auth retValue = client.Authenticate(username, password, apiKey);
            client.Close();
            return retValue;
        }

        public static Auth Validate(string authToken)
        {
            UserAuthService.UserAuthServiceClient client = Get<UserAuthService.UserAuthServiceClient>();
            Auth retValue = client.Validate(authToken);
            client.Close();
            return retValue;
        }

        public static User GetUser(string userName)
        {
            UserAuthService.UserAuthServiceClient client = Get<UserAuthService.UserAuthServiceClient>();
            User retValue = client.GetUser(userName);
            client.Close();
            return retValue;
        }

        public static bool ChangePassword(string userName, string newPassword)
        {
            UserAuthService.UserAuthServiceClient client = Get<UserAuthService.UserAuthServiceClient>();
            bool retValue = client.ChangePassword(userName, newPassword);
            client.Close();
            return retValue;

        }

        public static bool EditAccountSettings(IBI.Web.ViewModels.UserAccounts.AccountInfoModel model)
        {
            UserAuthService.UserAuthServiceClient client = Get<UserAuthService.UserAuthServiceClient>();
            User usr = new User();
            usr.Username = model.Username;
            usr.FullName = model.FullName;
            usr.ShortName = model.ShortName;
            usr.PhoneNumber = model.PhoneNumber;
            usr.Email = model.Email;
            usr.Company = model.Company;
            bool retValue = client.EditAccountSettings(usr);
            client.Close();
            return retValue;
        }


        internal static List<IBI.Shared.Models.BusTree.Group> GetUserAccessGroups(string userId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<IBI.Shared.Models.BusTree.Group> retValue = client.GetGroups(userId.ToString());
            client.Close();
            return retValue;
        }

        internal static List<String> GetUserAccessBuses(int userId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<String> retValue = client.GetUserBuses(userId);
            client.Close();
            return retValue;
        }

        internal static List<String> GetUserAccessBusesHavingVTC(int userId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<String> retValue = client.GetUserBusesHavingVTC(userId);
            client.Close();
            return retValue;
        }

        internal static List<String> GetAllBuses()
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<String> retValue = client.GetAllBusNumbers(0, true, true);
            client.Close();
            return retValue;
        }


        internal static string GetUserSpecificBuses(string filter, string customerIds, bool offline, bool online)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            string retValue = client.GetUserSpecificBuses(filter, customerIds, offline, online);
            client.Close();
            return retValue;
        }

        internal static string GetBusClientsLastPings(string busNumber, int customerId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            string retValue = client.GetBusClientsLastPings(busNumber, customerId);
            client.Close();
            return retValue;
        }

        internal static string GetAllUsersBuses(int groupId = 0, string busNumber = "")
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            string retValue = client.GetAllUsersBuses(groupId, busNumber);
            client.Close();
            return retValue;
        }

        internal static bool ConfirmAdminPassword(string username, string password)
        {
            UserAuthService.UserAuthServiceClient client = Get<UserAuthService.UserAuthServiceClient>();
            bool retValue = client.ConfirmAdminPassword(username, password);
            client.Close();
            return retValue;
        }


        //internal static string GetBusesDataForMap(int scheduleId)
        //{
        //    BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
        //    string retValue = client.GetBusesDataForMap(scheduleId);
        //    client.Close();
        //    return retValue;
        //}



        internal static List<IBI.Shared.Models.Customer> GetUserCustomers(int userId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<IBI.Shared.Models.Customer> retValue = client.GetUserCustomers(userId);
            client.Close();
            return retValue;
        }

        internal static List<IBI.Shared.Models.Configuration> GetUserConfigurations(int userId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<IBI.Shared.Models.Configuration> retValue = client.GetUserConfigurationProperties(userId);
            client.Close();
            return retValue;
        }



        #endregion

        #region User Management

        //get current users' access pages.
        internal static List<IBI.Shared.Models.Accounts.UserPage> GetUserPages(int userId)
        {
            UserManagementService.UserManagementServiceClient client = Get<UserManagementService.UserManagementServiceClient>();
            List<IBI.Shared.Models.Accounts.UserPage> retValue = client.GetUserPages(userId);
            client.Close();
            return retValue;

        }

        //internal static List<KeyValuePair<string, string>> GetUserActivityConfigurations(int userId)
        internal static List<IBIKeyValuePair> GetUserActivityConfigurations(int userId)
        {
            List<IBIKeyValuePair> result = new List<IBIKeyValuePair>();

            try
            {
                UserManagementService.UserManagementServiceClient client = new UserManagementService.UserManagementServiceClient();
                result = client.GetUserActivityConfigurations(userId).Select(uac => new IBIKeyValuePair
                {
                    Key = uac.Key,
                    Value = uac.Value
                }).ToList();
            }
            catch (Exception ex)
            {
                WebLogger.LogError(ex);
            }

            return result;
        }

        internal static void SetUserActivityConfiguration(int userId, string property, string value)
        {
            try
            {
                UserManagementService.UserManagementServiceClient client = new UserManagementService.UserManagementServiceClient();
                client.SetUserActivityConfiguration(userId, property, value);
            }
            catch (Exception ex)
            {
                WebLogger.LogError(ex);
            }
        }


        #endregion


        #region Bus Tree


        //Gets BusTree
        public static Tree GetSimpleBusTree(int userId, string groupId = "0", string customerId = "0", bool inOperation = false)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            Tree retValue = client.GetSimpleBusTree(userId.ToString(), groupId, customerId, inOperation.ToString().ToLower());
            client.Close();
            return retValue;
        }


        //Gets BusTree
        public static Tree GetBusGroupTree(int userId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            Tree retValue = client.GetBusGroupTree(userId.ToString(), "0");
            client.Close();
            return retValue;
        }

        //Change busGroup
        public static bool ChangeBusGroup(int customerId, string busNumber, int groupId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            return client.ChangeBusGroup(customerId, busNumber, groupId);
        }


        //Change busGroup
        public static bool ChangeBusStatus(int customerId, string busNumber, bool InOperation)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            return client.ChangeBusStatus(customerId, busNumber, InOperation);
        }

        //Change busAlias
        public static bool ModifyBusAlias(int customerId, string busNumber, string busAlias)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            return client.ModifyBusAlias(customerId, busNumber, busAlias);
        }



        //Gets BusTree
        //public static async void GetBusTree(int userId, List<BusStatusColor> filters, Tree tree)
        //{ 
        //    BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
        //    //Tree retValue = client.GetBusTree(tree);
        //    tree = await client.GetBusTreeAsync(tree);            
        //    client.Close();
        //    //return retValue;
        //}

        public static Tree GetBusTree(int userId, List<BusStatusColor> filters)
        {
            Tree tree = new Tree(null, filters, null, userId);
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            Tree retValue = client.GetBusTree(tree);
            //tree = await client.GetBusTreeAsync(tree);
            client.Close();
            return retValue;
        }


        //Gets a single Group's detail
        public static IBI.Shared.Models.BusTree.Group GetGroup(int groupId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            IBI.Shared.Models.BusTree.Group retValue = client.GetGroup(groupId.ToString());
            client.Close();
            return retValue;
        }

        //Gets a Single Bus Detail
        public static IBI.Shared.Models.BusTree.Bus GetBus(int customerId, string busNumber)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            IBI.Shared.Models.BusTree.Bus retValue = client.GetBus(customerId.ToString(), busNumber);
            client.Close();
            return retValue;

        }

        public static List<IBI.Shared.Models.BusTree.BusDetail> GetBuses(int customerId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<IBI.Shared.Models.BusTree.BusDetail> retValue = client.GetBuses(customerId);
            client.Close();
            return retValue;
        }

        public static List<BusDetail> GetBusLookUpDetail(string busNumber)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<BusDetail> retValue = client.GetBusLookUpDetail(busNumber);
            client.Close();
            return retValue;
        }

        public static BusDetail GetBusDetail(string busNumber)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            BusDetail retValue = client.GetBusDetail(busNumber);
            client.Close();
            return retValue;
        }

        public static List<Client> GetClient(string macAddress)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<Client> retValue = client.GetClient(macAddress);
            client.Close();
            return retValue;
        }

        public static List<String> GetBusNumbers(int customerId, bool inOperation, bool checkInOperation)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<String> retValue = client.GetAllBusNumbers(customerId, inOperation, checkInOperation);
            client.Close();
            return retValue;
        }

        public static string GetBusServiceTickets(string busNumber, int customerId)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            string retValue = client.GetBusServiceTickets(busNumber, customerId);
            client.Close();
            return retValue;
        }


        public static ServiceResponse<bool> DeleteBus(string busNumber, int customerId, string username)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool>();

            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            response = client.DeleteBus(busNumber, customerId, username);
            client.Close();
            return response;
        }
        #endregion

        #region BusStatusLog

        public static List<IBI.Shared.Models.BusTree.BusStatusLog> GetBusStatusLog()
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<IBI.Shared.Models.BusTree.BusStatusLog> retValue = client.GetBusStatusLog();
            client.Close();
            return retValue;
        }

        #endregion

        #region BusEventLog

        public static List<IBI.Shared.Models.BusTree.BusEventLog> GetBusEventLog(string busNumber, DateTime date)
        {
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            List<IBI.Shared.Models.BusTree.BusEventLog> retValue = client.GetBusEventLog(busNumber, date);
            client.Close();
            return retValue;
        }

        #endregion

        #region Messaging

        //Gets a list of messages
        public static List<Message> GetMessages(string busNumber, int customerId)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<Message> retValue = client.GetMessages(busNumber, customerId);
            client.Close();
            return retValue;
        }

        //Gets a list of MessageCategories with nested Messages
        public static List<MessageCategory> GetMessagesByCategories(string busNumber, int customerId)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<MessageCategory> retValue = client.GetMessagesByCategories(busNumber, customerId);
            client.Close();
            return retValue;
        }


        //Gets a list of MessageCategories with nested Messages
        public static List<MessageReplyType> GetMessagesReplyTypes()
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<MessageReplyType> retValue = client.GetMessageReplyTypes();
            client.Close();
            return retValue;
        }

        //Gets a Single Message record
        public static Message GetMessage(int messageId)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            Message retValue = client.GetMessage(messageId);
            client.Close();
            return retValue;
        }

        //Saves a Message
        public static int SaveMessage(Message msg)
        {

            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            int retValue = client.SaveMessage(msg);
            client.Close();
            return retValue;

        }

        //Expires a Message
        public static bool SetExpireMessage(int messageId)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            bool retValue = client.SetExpireMessage(messageId);
            client.Close();
            return retValue;

        }



        //Gets List of MessageCategory

        public static MessageCategory GetCategory(int categoryId)
        {
            return null;
        }

        public static List<MessageCategory> GetMessageCategories(int customerId)
        {
            //UGLY FIX
            customerId = customerId == 0 ? Common.Settings.DefaultCustomerId : customerId;

            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<MessageCategory> retValue = client.GetMessageCategories(customerId);
            client.Close();
            return retValue;
        }

        public static void MessagePing(string pingData)
        {
            IBIService.IBIServiceClient client = Get<IBIService.IBIServiceClient>();
            client.Ping("9999", "DCU", pingData);
            client.Close();
        }

        public static List<IBI.Shared.Models.Messages.IncomingMessage> GetIncomingMessages(int userId)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<IBI.Shared.Models.Messages.IncomingMessage> retValue = client.GetIncomingMessages(userId.ToString());
            client.Close();
            return retValue;
        }

        public static List<IBI.Shared.Models.Messages.IncomingMessage> GetIncomingMessages(int userId, DateTime date, string busNumber, string messageText, string position, string destination, string line, string nextStop, int groupId, bool isArchived)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<IBI.Shared.Models.Messages.IncomingMessage> retValue = client.GetIncomingMessagesByFilters(userId, date, busNumber, messageText, position, destination, line, nextStop, groupId, isArchived);
            client.Close();
            return retValue;

        }

        public static List<IBI.Shared.Models.Messages.Message> GetOutgoingMessages(int userId, string busNumber, DateTime validFrom, DateTime validTo, string title, string messageText, string messageCategoryName, int groupId, string senderName, bool showExpiredOnly)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<IBI.Shared.Models.Messages.Message> retValue = client.GetOutgoingMessages(userId, busNumber, validFrom, validTo, title, messageText, messageCategoryName, groupId, senderName, showExpiredOnly);
            client.Close();
            return retValue;
        }



        public static IBI.Shared.Models.Messages.MessageConfirmationsAndReplys GetMessageConfirmationsAndReplys(int messageId, string busNumber = "")
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            IBI.Shared.Models.Messages.MessageConfirmationsAndReplys retValue = client.GetMessageConfirmationsAndReplys(messageId, busNumber);
            client.Close();
            return retValue;
        }

        public static List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> GetMessageConfirmations(int messageId, string busNumber)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> retValue = client.GetMessageConfirmations(messageId, busNumber);
            client.Close();
            return retValue;
        }


        public static List<IBI.Shared.Models.Messages.IncomingMessage> GetMessageReplies(int messageId, string busNumber)
        {
            MessageService.MessageServiceClient client = Get<MessageService.MessageServiceClient>();
            List<IBI.Shared.Models.Messages.IncomingMessage> retValue = client.GetMessageReplies(messageId, busNumber);
            client.Close();
            return retValue;
        }

        #endregion


        #region Schedule

        //public static List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string plannedDeparture, string plannedArrival, string actualDeparture, string actualArrival)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
        //    List<IBI.Shared.Models.BusJourney.Journey> retValue = client.GetJourneys(
        //        line,
        //        busNumber,
        //        journeyNumber,
        //        journeyDate,
        //        from,
        //        to,
        //        plannedDeparture,
        //        plannedArrival,
        //        actualDeparture,
        //        actualArrival
        //        );

        //    client.Close();
        //    return retValue;
        //}


        //public static List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string stopName, string plannedDeparture, string actualDeparture, string actualArrival, string planDifference, string status)
        //{

        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
        //    List<IBI.Shared.Models.BusJourney.Punctuality> retValue = client.GetPunctuality(
        //        line,
        //        busNumber,
        //        journeyNumber,
        //        journeyDate,
        //        from,
        //        to,
        //        stopName,
        //        plannedDeparture,
        //        actualDeparture,
        //        actualArrival,
        //        planDifference,
        //        status);

        //    client.Close();
        //    return retValue;
        //}

        public static IBI.Shared.Models.UserScheduleModels.Schedule GetUserSchedule(int scheduleId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.UserScheduleModels.Schedule retValue = client.GetUserSchedule(Common.CurrentUser.UserId, scheduleId);
            client.Close();
            return retValue;
        }


        public static List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(DateTime startDate, string busNumber)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            List<IBI.Shared.Models.BusJourney.Journey> retValue = client.GetJourneys(
                startDate, busNumber
                );

            client.Close();
            return retValue;
        }


        public static List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(DateTime startDate, string busNumber, int journeyNumber)
        {

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            List<IBI.Shared.Models.BusJourney.Punctuality> retValue = client.GetPunctuality(
                startDate, busNumber, journeyNumber
                );

            client.Close();
            return retValue;
        }



        public static bool FindScheduleId(int scheduleId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.FindScheduleId(scheduleId);

            client.Close();

            return retValue;
        }


        #region Signs

        public static IBI.Shared.Models.Signs.SignTree GetSignTree(IBI.Shared.Models.Signs.SignTree tree, bool showInactive, bool setAlphNumericGrouping)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            IBI.Shared.Models.Signs.SignTree retValue = client.GetSignTree(tree, true, showInactive, setAlphNumericGrouping);

            client.Close();

            return retValue;
        }


        public static bool SaveSign(IBI.Shared.Models.Signs.Sign sign)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            bool retValue = client.SaveSign(sign, CurrentUser.Username);

            client.Close();

            return retValue;

        }

        public static bool SaveSignGroup(IBI.Shared.Models.Signs.Group group, int? oldParentId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            bool retValue = client.SaveSignGroup(group, oldParentId, CurrentUser.Username);

            client.Close();

            return retValue;

        }
        public static bool SaveSignGroup(IBI.Shared.Models.Signs.Group group)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            bool retValue = client.SaveSignGroupSimple(group, CurrentUser.Username);

            client.Close();

            return retValue;

        }

        public static bool SaveSignDataImages(int layoutId, SignXmlData signData)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            bool retValue = client.SaveSignDataImages(layoutId, signData, CurrentUser.Username);

            client.Close();

            return retValue;
        }


        public static int SaveGraphicRule(SignGraphicRule graphicRule)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            int retValue = client.SaveGraphicRule(graphicRule, CurrentUser.Username);
            client.Close();
            return retValue;
        }


        public static bool SaveImageGeneratorData(int layoutId, int signItemId, int signRuleId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.SaveImageGeneratorData(layoutId, signItemId, signRuleId, CurrentUser.Username);
            client.Close();
            return retValue;

        }

        public static List<string> GetImagePreview(int signItemId, SignGraphicRule signGraphicRule, int layoutId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            List<string> retValue = client.GetImagePreview(signItemId, signGraphicRule, layoutId, CurrentUser.Username);

            client.Close();

            return retValue;
        }


        public static IBI.Shared.Models.Signs.Sign GetSignGroups(int customerId, int userId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            IBI.Shared.Models.Signs.SignTree tree = new Shared.Models.Signs.SignTree()
            {
                CustomerID = customerId,
                UserID = userId
            };

            IBI.Shared.Models.Signs.Sign retValue = new Sign(0, 0, customerId, "", -1, "", "", "", "", true, null, "", false, 0);

            // retValue.children.Add(new Sign(-1, -1, customerId, "Special Destinations", -1, "", "", "", "", true, null, "", false, 0));
            // retValue.children.Add(new Sign(-2, -1, customerId, "Line Groups", -1, "", "", "", "", true, null, "", false, 0));

            SignTree signTree = client.GetSignTree(tree, false, false, false);
            retValue.children.Add(signTree.Signs[0]);
            retValue.children.Add(signTree.Signs[1]);

            //client.GetSignTree(tree, false, false, false).Signs[0];

            client.Close();

            return retValue;
        }


        public static bool DeleteSign(int signId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.DeleteSign(signId, CurrentUser.Username);

            client.Close();

            return retValue;
        }


        public static IBI.Shared.Models.Signs.Sign GetSignItem(int signId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.Signs.Sign retValue = client.GetSign(signId);

            client.Close();

            return retValue;
        }

        public static List<SignDataImageItem> GetSignImages(int customerId, int layoutid, int contentid)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            List<SignDataImageItem> retValue = client.GetSignImages(customerId, layoutid, contentid);
            client.Close();
            return retValue;
        }

        public static IBI.Shared.Models.Signs.Group GetSignGroup(int groupId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.Signs.Group retValue = client.GetGroup(groupId);

            client.Close();

            return retValue;
        }

        public static IBI.Shared.Models.Signs.Sign FindSignItem(int customerId, int? groupId, string line, string name, string mainText, string subText)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.Signs.Sign retValue = client.FindSign(customerId, groupId, line, name, mainText, subText);

            client.Close();

            return retValue;
        }


        public static IBI.Shared.Models.Signs.Group FindSignGroup(int customerId, int? parentGroupId, string groupName)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.Signs.Group retValue = client.FindSignGroup(customerId, parentGroupId, groupName);

            client.Close();

            return retValue;
        }

        public static bool HasChildSignGroup(int groupId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.HasChildSignGroup(groupId);

            client.Close();

            return retValue;
        }


        public static List<SignXmlLayout> GetSignImages(int signId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            List<SignXmlLayout> retValue = client.GetSignsData(signId);

            client.Close();

            return retValue;
        }

        public static object DeleteSignGroup(int groupId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.DeleteSignGroup(groupId, CurrentUser.Username);

            client.Close();

            return retValue;
        }

        public static IBI.Shared.Models.Signs.SignXmlLayout GetSignLayout(int layoutId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.Signs.SignXmlLayout retValue = client.GetSignLayout(layoutId);

            client.Close();

            return retValue;
        }

        public static IBI.Shared.Models.Signs.SignGraphicRule GetSignGraphicRule(int graphicRuleId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.Signs.SignGraphicRule retValue = client.GetSignGraphicRule(graphicRuleId);

            client.Close();

            return retValue;
        }

        #endregion

        #region SignAddons


        public static IBI.Shared.Models.SignAddons.SignAddonTree GetSignAddonTree(IBI.Shared.Models.SignAddons.SignAddonTree tree, bool showInactive, bool setAlphNumericGrouping)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            IBI.Shared.Models.SignAddons.SignAddonTree retValue = client.GetSignAddonTree(tree, true, showInactive, setAlphNumericGrouping);

            client.Close();

            return retValue;            
        }

        public static bool SaveSignAddon(IBI.Shared.Models.SignAddons.SignAddon signAddon)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            bool retValue = client.SaveSignAddon(signAddon, CurrentUser.Username);

            client.Close();

            return retValue;

        }

        //public static bool SaveSignGroup(IBI.Shared.Models.Signs.Group group, int? oldParentId)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

        //    bool retValue = client.SaveSignGroup(group, oldParentId, CurrentUser.Username);

        //    client.Close();

        //    return retValue;

        //}
        //public static bool SaveSignGroup(IBI.Shared.Models.Signs.Group group)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

        //    bool retValue = client.SaveSignGroupSimple(group, CurrentUser.Username);

        //    client.Close();

        //    return retValue;

        //}

        //public static bool SaveSignDataImages(int layoutId, SignXmlData signData)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

        //    bool retValue = client.SaveSignDataImages(layoutId, signData, CurrentUser.Username);

        //    client.Close();

        //    return retValue;
        //}


        //public static int SaveGraphicRule(SignGraphicRule graphicRule)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
        //    int retValue = client.SaveGraphicRule(graphicRule, CurrentUser.Username);
        //    client.Close();
        //    return retValue;
        //}


        public static bool SaveSignAddonImageGeneratorData(int layoutId, int signAddonItemId, int signRuleId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.SaveSignAddonImageGeneratorData(layoutId, signAddonItemId, signRuleId, CurrentUser.Username);
            client.Close();
            return retValue;

        }

        //public static List<string> GetImagePreview(int signItemId, SignGraphicRule signGraphicRule, int layoutId)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

        //    List<string> retValue = client.GetImagePreview(signItemId, signGraphicRule, layoutId, CurrentUser.Username);

        //    client.Close();

        //    return retValue;
        //}


        //public static IBI.Shared.Models.Signs.Sign GetSignGroups(int customerId, int userId)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

        //    IBI.Shared.Models.Signs.SignTree tree = new Shared.Models.Signs.SignTree()
        //    {
        //        CustomerID = customerId,
        //        UserID = userId
        //    };

        //    IBI.Shared.Models.Signs.Sign retValue = new Sign(0, 0, customerId, "", -1, "", "", "", "", true, null, "", false, 0);

        //    // retValue.children.Add(new Sign(-1, -1, customerId, "Special Destinations", -1, "", "", "", "", true, null, "", false, 0));
        //    // retValue.children.Add(new Sign(-2, -1, customerId, "Line Groups", -1, "", "", "", "", true, null, "", false, 0));

        //    SignTree signTree = client.GetSignTree(tree, false, false, false);
        //    retValue.children.Add(signTree.Signs[0]);
        //    retValue.children.Add(signTree.Signs[1]);

        //    //client.GetSignTree(tree, false, false, false).Signs[0];

        //    client.Close();

        //    return retValue;
        //}


        public static bool DeleteSignAddon(int signAddonItemId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.DeleteSignAddon(signAddonItemId, CurrentUser.Username);

            client.Close();

            return retValue;
        }


        public static IBI.Shared.Models.SignAddons.SignAddon GetSignAddonItem(int signAddonItemId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.SignAddons.SignAddon retValue = client.GetSignAddon(signAddonItemId);

            client.Close();

            return retValue;
        }

        public static List<SignDataImageItem> GetSignAddonImages(int customerId, int layoutid, int contentid)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            List<SignDataImageItem> retValue = client.GetSignAddonImages(customerId, layoutid, contentid);
            client.Close();
            return retValue;
        }


        public static List<SignXmlLayout> GetSignAddonImages(int signAddonItemId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            List<SignXmlLayout> retValue = client.GetSignAddonsData(signAddonItemId);

            client.Close();

            return retValue;
        }
        

        public static IBI.Shared.Models.SignAddons.SignAddon FindSignAddonItem(int customerId, int? groupId, string line, string name, string signText, string udpText)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.SignAddons.SignAddon retValue = client.FindSignAddon(customerId, groupId, line, name, signText, udpText);

            client.Close();

            return retValue;
        }

        public static bool SaveSignAddonDataImages(int layoutId, SignXmlData signData)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            bool retValue = client.SaveSignAddonDataImages(layoutId, signData, CurrentUser.Username);

            client.Close();

            return retValue;
        }

        public static List<string> GetSignAddonImagePreview(int signAddonItemId, SignGraphicRule signGraphicRule, int layoutId)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();

            List<string> retValue = client.GetSignAddonImagePreview(signAddonItemId, signGraphicRule, layoutId, CurrentUser.Username);

            client.Close();

            return retValue;
        }

        //public static List<SignXmlLayout> GetSignAddonImages(int signAddonItemId)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
        //    List<SignXmlLayout> retValue = client.GetSignAddonImages(signAddonItemId);

        //    client.Close();

        //    return retValue;
        //}
         

        //public static IBI.Shared.Models.Signs.SignXmlLayout GetSignLayout(int layoutId)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
        //    IBI.Shared.Models.Signs.SignXmlLayout retValue = client.GetSignLayout(layoutId);

        //    client.Close();

        //    return retValue;
        //}

        //public static IBI.Shared.Models.Signs.SignGraphicRule GetSignGraphicRule(int graphicRuleId)
        //{
        //    ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
        //    IBI.Shared.Models.Signs.SignGraphicRule retValue = client.GetSignGraphicRule(graphicRuleId);

        //    client.Close();

        //    return retValue;
        //}

        #endregion


        #region Schedule Tree

        public static ScheduleTree GetScheduleTree(int customerId, int userId) {

            ScheduleTree tree = new ScheduleTree(CurrentUser.UserId);
            tree.CustomerID = customerId;
            

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            ScheduleTree retValue = client.GetScheduleTree(tree, true, true, true);

            client.Close();

            return retValue;
        }


        public static IBI.Shared.Models.ScheduleTreeItems.Schedule GetSchedule(int scheduleId) {

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            IBI.Shared.Models.ScheduleTreeItems.Schedule retValue = client.GetSchedule(scheduleId);

            client.Close();

            return retValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sch"></param>
        /// <returns>
        ///  > 0 : new schedule created
        ///  0 : error, no schedule saved
        /// -1 : schedule already exists - no changes made (may not occur)
        /// -2 : schedule already exists - with changes
        /// -3 : schedule already exists - no changes
        /// -4 : schedule already exists - updated successfully
        /// </returns>
        public static int SaveSchedule(Shared.Models.ScheduleTreeItems.Schedule schedule, string forcedOption)
        {

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            int retValue = client.SaveSchedule(schedule, forcedOption);

            client.Close();

            return retValue;
        }

        internal static bool CheckExistingSchedule(int scheduleId, int customerId, string line, string fromName, string destination, string viaName)
        {
            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.CheckExistingSchedule(scheduleId, customerId, line, fromName, destination, viaName);

            client.Close();

            return retValue;
        }

        public static int UpdateSchedule(Shared.Models.ScheduleTreeItems.Schedule schedule, string forcedOption)
        {

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            int retValue = client.UpdateSchedule(schedule, forcedOption);

            client.Close();

            return retValue;
        }

        public static int UpdateScheduleStops(Shared.Models.ScheduleTreeItems.Schedule schedule)
        {

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            int retValue = client.UpdateScheduleStops(schedule);

            client.Close();

            return retValue;
        }

        public static bool DeleteSchedule(int scheduleId)
        {

            ScheduleService.ScheduleServiceClient client = Get<ScheduleService.ScheduleServiceClient>();
            bool retValue = client.DeleteSchedule(scheduleId);

            client.Close();

            return retValue;
        }

        #endregion

        #endregion

        #region TTSService

        public static List<IBI.Shared.Models.TTS.AudioFile> GetStopAudioFiles(string stopName, bool searchFilterA, bool searchFilterR, bool searchFilterU )
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            List<IBI.Shared.Models.TTS.AudioFile> retValue = client.GetStopAudioFiles(stopName, searchFilterA, searchFilterR, searchFilterU);
            client.Close();
            return retValue;
        }

        public static IBI.Shared.Models.TTS.AudioFile GetAudioFile(string fileName, string voice)
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            IBI.Shared.Models.TTS.AudioFile retValue = client.GetAudioFileByVoice(fileName, voice);
            client.Close();
            return retValue;
        }

        public static byte[] GenerateAudioFile(string stopName, string sourceText, string voice)
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            byte[] retValue = client.GenerateAudioFile(stopName, sourceText, voice);
            client.Close();
            return retValue;
        }

        public static byte[] GenerateAudioPreview(string stopName, string sourceText, string voice)
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            byte[] retValue = client.GenerateAudioPreview(stopName, sourceText, voice);
            client.Close();
            return retValue;
        }

        //public static string GenerateAudioFilePath(string stopName, string sourceText, string voice)
        //{
        //    TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
        //    string retValue = client.GenerateAudioFilePath(stopName, sourceText, voice);
        //    client.Close();
        //    return retValue;
        //}

        public static bool SaveAudioFile(string stopName, string sourceText, string voice, string status)
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            bool retValue = client.SaveAudioFile(stopName, sourceText, voice, status);
            client.Close();
            return retValue;
        }

        public static bool SaveAudioFileMp3(string stopName, string sourceText, string voice, string status, byte[] mp3, string alternateSourceVoiceText)
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            bool retValue = client.SaveAudioFileMp3(stopName, sourceText, voice, status, mp3, alternateSourceVoiceText);
            client.Close();
            return retValue;
        }

        public static List<string> GetAllSourceVoices()
        {
            TTSService.TTSServiceClient client = Get<TTSService.TTSServiceClient>();
            List<string> retValue = client.GetAllSourceVoices();
            client.Close();
            return retValue;
        }
        #endregion


        #region journeys

        /*
        internal static List<IBI.Shared.Models.Journey.Journey> GetActiveJourneys(int customerId)
        {
            List<IBI.Shared.Models.Journey.Journey> data = new List<Shared.Models.Journey.Journey>();
            try
            {
                JourneyService.JourneyServiceClient client = new JourneyService.JourneyServiceClient();
                data = client.GetJourneys(customerId);
            }
            catch (Exception ex)
            {
                WebLogger.LogError(ex);
            }

            return data;
        }
        */
        #endregion

        #region Client Configurations

        public static Tree GetClientConfigurationsTree(int userId, List<BusStatusColor> filters)
        {
            Tree tree = new Tree(null, filters, null, userId);
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            Tree retValue = client.GetClientConfigurationsTree(tree);
            client.Close();
            return retValue;
        }

        public static ServiceResponse<bool> SetClientConfigurations(int ConfigurationPropertyId, string PropertyValue, int GroupId, int CustomerID, string BusNumber)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool>();
            BusTreeService.BusTreeServiceClient client = Get<BusTreeService.BusTreeServiceClient>();
            response = client.SetClientConfigurations(ConfigurationPropertyId, PropertyValue,GroupId,CustomerID,BusNumber);
            client.Close();
            return response;
        }

        #endregion



    }

}