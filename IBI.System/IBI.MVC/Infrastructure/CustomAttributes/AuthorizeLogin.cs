﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Globalization;
using IBI.Web.Common;
using System.Threading;
using System.IO;
using IBI.Web.Infrastructure.Keys;
using IBI.Shared.Models.Accounts;
using System.Security.Principal;
using IBI.Shared.Models.Accounts;

namespace IBI.Web.Infrastructure.CustomAttributes
{
    public class AuthorizeLogin : ActionFilterAttribute, IAuthorizationFilter
    { 

        public static string AuthToken
        {        
            get
            {
                return SessionManager.Get<string>(SessionKeys.AuthToken);
            }
            set
            {
                SessionManager.Store(SessionKeys.AuthToken, value);        
            }
        }

        public static User LoggedInUser
        {
            get
            {
                return SessionManager.Get<User>(SessionKeys.LoggedInUser);
            }
            set
            {
                SessionManager.Store(SessionKeys.LoggedInUser, value);
                SessionManager.Store(SessionKeys.LoggedInUsername, value.Username);
            }
        }

        public static void Login(Shared.Models.Accounts.Auth auth)
        { 
            FormsAuthentication.SetAuthCookie(auth.Username, true);
            System.Web.HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(auth.Username, "Forms"), null);

            User usr = ServiceManager.GetUser(auth.Username);

            AuthorizeLogin.AuthToken = auth.AuthToken;
            AuthorizeLogin.LoggedInUser = usr;
        }

        public static void LogoutFromPopup(string returnUrl)
        {
            FormsAuthentication.SignOut();
            SessionManager.Clear();
        }


        public static void Logout(string returnUrl)
        {
            FormsAuthentication.SignOut();

            SessionManager.Clear();

            System.Web.HttpContext.Current.Response.Write("<script>window.open('" + Settings.AppPath + "/Account/Login?returnUrl=" + returnUrl + "', '_top');</script>");
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string curUrl = HttpContext.Current.Request.ServerVariables["URL"];            
            //if(curUrl.StartsWith("/IBI/"))
            //{
            //    curUrl = curUrl.Replace("/IBI/", "/");
            //}

            string returnUrl = string.Empty;

            returnUrl = string.Empty;
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                returnUrl = curUrl;
            }

            if (string.IsNullOrEmpty(AuthToken))
            {
                Logout(returnUrl);
                return;
            }

            Auth auth = RESTManager.Validate(AuthToken);
            if (auth.IsValid)
            {
                AuthToken = auth.AuthToken;

                //keep the login alive here.                
                FormsAuthentication.SetAuthCookie(auth.Username, true);
                filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(auth.Username, "Forms"), null);
            }
            else
            {                
                //string curUrl = filterContext.ParentActionViewContext.HttpContext.Request.Url.AbsoluteUri; //not using                
                //returnUrl = curUrl; // "Bus/BusTree";

                FormsAuthentication.SignOut();
                System.Web.HttpContext.Current.Session.Abandon();

                filterContext.Result = new RedirectResult("/Account/Login?returnUrl=" + returnUrl);
                //Logout(returnUrl);
            }


            //CurrentUser.PushAVisistedPage(filterContext.HttpContext.Request.RawUrl);

            //var loggedInUser = HttpContext.Current.Session[SessionKeys.LoggedInUser];

            //if (filterContext.HttpContext.Session.IsNewSession || loggedInUser==null)
            //{       
            //}        


            //if (filterContext.IsChildAction == false)
            //{
            //    //check if current url is valid for current user or not.
            //    string curUrl = HttpContext.Current.Request["URL"];
            //    if (curUrl.StartsWith("/IBI/"))
            //    {
            //        curUrl = curUrl.Replace("/IBI/", "/");
            //    }


            //    bool validUrl = IBI.Web.Common.CurrentUser.IsValidUserPageUrl(curUrl);

            //    if (!validUrl)
            //    {
            //        throw new FileNotFoundException();
            //    }
            //}
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            UrlHelper url = new UrlHelper(context.RequestContext);

            string path = string.Empty;
            
            //var userPages = CurrentUser.IsValidUserPageUrl(HttpContext.Current.Request.Ra)

            ////if(url.RequestContext.HttpContext.Request.IsLocal)
            //{
            //    path = url.RequestContext.HttpContext.Request.Url.LocalPath;

            //    ServiceManager.SetUserActivityConfiguration(CurrentUser.UserId, "LastPage", path.Replace("/IBI/", ""));
            //}
                

            string s = path;
        }


        //for IAuthorizeFilter
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            // TODO: set the culture here so that it's picked up by the model binder

            CultureInfo culture = new CultureInfo(Settings.DefaultCulture);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        } 
 
      
    }
}