﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using mermaid.BaseObjects.IO;
using System.Configuration;
using IBI.Shared.Models.Destinations;
using IBI.Shared.Models.Schedules;

namespace IBI.UtilityConsole
{
    public class SignSortOrder
    {
        public static void UpdateSignOrder()
        {

                try
            {

                string customers = ConfigurationSettings.AppSettings["customers"];
                string[] list = customers.Split(new char[] { ',' });
                //int[] custs = {2038,2040,2116,2120,2140,2141,2164,2176,2198,112164};


                foreach (string custId in list)
                {
                    int customerId = int.Parse(custId);
        
                    ScheduleService.ScheduleServiceClient client = new ScheduleService.ScheduleServiceClient();

                    //IBI.Shared.Models.Destinations.SignTree signTree = client.GetSignTree(new IBI.Shared.Models.Destinations.SignTree { CustomerID = customerId, Signs = null, UserID = 3 }, true, true);

                    List<IBI.Shared.Models.Schedules.DestListModel> signs = new List<Shared.Models.Schedules.DestListModel>();
                    List<IBI.Shared.Models.Schedules.DestListModel> signGroups = new List<Shared.Models.Schedules.DestListModel>();
                    List<IBI.Shared.Models.Schedules.DestListModel> signItems = new List<Shared.Models.Schedules.DestListModel>();

                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        var custSignGroups = dbContext.SignGroups.Where(s => s.CustomerId == customerId);

                        signGroups = custSignGroups.Select(s => new DestListModel
                        {
                            Id = s.GroupId,
                            Line = s.GroupName.Replace("#", ""),
                            Priority = s.SortValue,
                            MakeGroup = true

                        }).ToList();


                        var custSignItems = dbContext.SignItems.Where(s => s.CustomerId == customerId);
                            
                        signItems = custSignItems.Select(s => new DestListModel
                        {
                            Id=s.SignId,
                            Line = s.Line.Replace("#", ""),
                            Destination = s.MainText,
                            Via = s.SubText,
                            Priority = s.SortValue,
                            MakeGroup = false
                        }).ToList();


                        signs.AddRange(signGroups);
                        signs.AddRange(signItems);

                        int sortValue = 1;

                        

                        foreach (DestListModel sign in signs.OrderBy(s=>s.Priority).ThenBy(s => s.Line, new ManualDestinationLineComparer()).ThenBy(s => s.Destination).ThenBy(s => s.Via))
                        {
                            if(sign.MakeGroup)
                            {
                                //update sign group record
                                custSignGroups.Single(s=>s.GroupId==sign.Id).SortValue = sortValue;

                            }
                            else
                            {
                                //update sign item record
                                custSignItems.Single(s=>s.SignId==sign.Id).SortValue = sortValue;

                            }
                            sortValue++;
                        }

                        dbContext.SaveChanges();

                    }

                      
                }
            } 
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

        }

       

    }


    public class ManualDestinationLineComparer : IComparer<string>
    {
        public int Compare(string s1, string s2)
        {
            int s1_i;
            int s2_i;
            string s1_s;
            string s2_s;

            int intPartLength = 0;

            foreach (char c in s1.ToCharArray())
            {
                if (IsNumeric(c))
                    intPartLength++;
                else
                    break;
            }

            bool result;
            result = int.TryParse(s1.Substring(0, intPartLength), out s1_i);

            if (intPartLength > 0)
                s1_s = s1.Substring(intPartLength - 1);


            intPartLength = 0;
            foreach (char c in s2.ToCharArray())
            {
                if (IsNumeric(c))
                    intPartLength++;
                else
                    break;
            }

            result = int.TryParse(s2.Substring(0, intPartLength), out s2_i);

            if (intPartLength > 0)
                s2_s = s2.Substring(intPartLength - 1);


            if (s1_i > 0 && s2_i > 0)
            {
                if (s1_i > s2_i)
                    return 1;
                if (s1_i < s2_i)
                    return -1;
                if (s1_i == s2_i)
                {
                    return string.Compare(s1, s2, true);
                }
            }

            //if (IsNumeric(s1) && IsNumeric(s2))
            //{
            //    if (Convert.ToInt32(s1) > Convert.ToInt32(s2)) return 1;
            //    if (Convert.ToInt32(s1) < Convert.ToInt32(s2)) return -1;
            //    if (Convert.ToInt32(s1) == Convert.ToInt32(s2)) return 0;
            //}

            //if (IsNumeric(s1) && !IsNumeric(s2))
            //    return -1;

            //if (!IsNumeric(s1) && IsNumeric(s2))
            //    return 1;

            return string.Compare(s1, s2, true);
        }

        public static bool IsNumeric(object value)
        {
            try
            {
                int i = Convert.ToInt32(value.ToString());
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }

}
