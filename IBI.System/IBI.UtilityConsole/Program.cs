﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using System.Xml;
using System.IO;
using System.Xml.Linq;

namespace IBI.UtilityConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            bool breakLoop = false;

            WCFTest.TestCorrespndingTraffice();

            /*while (!breakLoop)
            {
             
                Console.WriteLine("1 - Import DestinationSelectionList.xml");
                Console.WriteLine("2 - Fix Sign Order");
                
                Console.WriteLine("1 - Re-Generate MD5 for Schedule");
             
                Console.WriteLine("2 - Fix Schedule's XML ViaName Node & MD5");
                 
                Console.WriteLine("1 - Test WCF call");
                Console.WriteLine("10 - Exit");
                Console.WriteLine("Please select option:");
                string option = Console.ReadLine();
                switch (option)
                {
                    
                    case "1":
                        Console.WriteLine("Importing DestinationSelectionList.xml...");
                        XmlImporter.ImportDestinationSelectionList();
                        Console.WriteLine("Imported DestinationSelectionList.xml successfully");
                        Console.WriteLine("Option Obsolete .. cannot be used.");
                        break;
                    case "2":
                        Console.WriteLine("Updating Signs Order...");
                        SignSortOrder.UpdateSignOrder();
                        Console.WriteLine("Signs Order updated");
                        break;
                    
                    case "1":
                        Console.WriteLine("Generating MD5 for Schedule....");
                        ScheduleMD5.GenerateMD5ForAllSchedules();
                        Console.WriteLine("MD5 generated successfully");
                        break;
                    case "2":
                        Console.WriteLine("Fixing ScheduleXML for Schedules....");
                        ScheduleMD5.FixScheduleXML();
                        Console.WriteLine("Schedule XML generated successfully");
                        break;
                    case "3":
                        Console.WriteLine(SampleXMLParsing());                       
                        break;
                   
                    default:
                         do nothing
                        break;
                        */
                //}
            //}
            
           // ScheduleMD5.GenerateMD5ForAllSchedules();           
           //Destinations.UpdateDestinationStructure(true);

            //ScheduleMD5.GenerateMD5ForAllSchedules();
            //ScheduleMD5.RefreshMD5();
            //SignSortOrder.UpdateSignOrder();
            //Console.WriteLine("Completed, Enter to Exit");
            //Console.ReadLine();
          // IBI.Shared.Interfaces.ISyncScheduleFactory dBoardFactory = new IBI.Implementation.Schedular.SyncScheduleFactory();
          // IBI.Shared.Interfaces.ISyncDepartureBoard dBoard= dBoardFactory.GetSyncDepartureBoard(2140);
          //string str = dBoard.GetDepartureBoard("9999", "8600054", "6A");

          //int i = 0;

        }

        private static string SampleXMLParsing()
        {
            XDocument xDoc = XDocument.Load( Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "journey.xml"));
            var elements = (from p in xDoc.Descendants("PlannedArrivalTime")
                           select p.Value).FirstOrDefault();

            return elements.ToString();
        }

         
    }
}
