﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using IBI.DataAccess.DBModels;
using System.Xml;

namespace IBI.UtilityConsole
{
    class XmlImporter
    {

        private static void ProcessNode(XmlNode node, int parentGroupId, bool bSpecial, int level, int customerId, int sortValue)
        {
            string nodeName = node.Name;
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                if (string.Compare(nodeName, "Destination", true) == 0)
                {
                    string line = node.Attributes["line"].Value;
                    string destination = node.Attributes["destination"].Value;
                    string mainText = string.Empty;
                    string subText = string.Empty;
                    if (destination.Contains("/"))
                    {
                        mainText = destination.Substring(0, destination.IndexOf("/"));
                        subText = destination.Substring(destination.IndexOf("/") + 1);
                        subText = subText.Replace("via ", "");
                    }
                    else
                    {
                        mainText = destination;
                    }
                    string schedule = node.Attributes["schedule"]==null?string.Empty:node.Attributes["schedule"].Value;
                    string innerValue = node.InnerText;
                    SignItem signItem = dbContext.SignItems.Where(m => m.CustomerId == customerId && m.MainText == mainText && m.SubText == subText && m.Line == line).FirstOrDefault();
                    if (signItem == null)
                    {
                        signItem = new SignItem();
                        signItem.Name = innerValue;
                        signItem.Line = line;
                        signItem.MainText = mainText;
                        signItem.SubText = subText;
                        signItem.CustomerId = customerId;
                        if (!string.IsNullOrEmpty(schedule))
                        {
                            signItem.ScheduleId = int.Parse(schedule);
                        }
                        
                        if (parentGroupId != 0)
                        {
                            signItem.GroupId = parentGroupId;
                        }
                        
                        signItem.SortValue = sortValue;
                        signItem.Excluded = false;
                        signItem.DateAdded = DateTime.Now;
                        signItem.DateModified = DateTime.Now;
                        dbContext.SignItems.Add(signItem);

                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    string groupName = string.Empty;
                    if (nodeName == "Line")
                    {
                        groupName = "#" + node.Attributes["line"].Value;
                    }
                    else
                    {
                        groupName = node.Attributes["name"].Value;
                    }
                    SignGroup group = dbContext.SignGroups.Where(m => m.CustomerId == customerId && m.GroupName == groupName && m.ParentGroupId == parentGroupId).FirstOrDefault();
                    if (group == null)
                    {
                        group = new SignGroup();
                        group.GroupName = groupName;
                        group.CustomerId = customerId;
                        if (parentGroupId != 0)
                        {
                            group.ParentGroupId = parentGroupId;
                        }
                        group.Excluded = false;
                        group.SortValue = sortValue;
                        group.DateAdded = DateTime.Now;
                        group.DateModified = DateTime.Now;
                        dbContext.SignGroups.Add(group);
                        dbContext.SaveChanges();
                    }
                    level++;
                    int sortNumber = 1;
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        ProcessNode(child, group.GroupId, bSpecial, level, customerId, sortNumber++);
                    }
                }
            }
        }

        public static void ImportDestinationSelectionList()
        {
            try
            {
                string customers = ConfigurationSettings.AppSettings["customers"];
                if (string.IsNullOrEmpty(customers))
                {
                    customers = "2140,2038,2040,2116,2120,2141,2164,2176,2198";
                }
                string[] list = customers.Split(new char[] { ',' });
                string importurl = ConfigurationSettings.AppSettings["importurl"];
                if (string.IsNullOrEmpty(importurl))
                {
                    importurl = "http://ibi.mermaid.dk/REST/Schedule/DestinationSelectionList.xml?customerId=";
                }

                foreach (string custId in list)
                {
                    int customerId = int.Parse(custId);
                    string finalUrl = importurl + custId;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(finalUrl);

                    XmlNode parentNode = doc.SelectSingleNode("/Data/SpecialDestinations");
                    if (parentNode != null)
                    {
                        int sortValue = 1;
                        foreach (XmlNode child in parentNode.ChildNodes)
                        {
                            ProcessNode(child, 0, true, 1, customerId, sortValue++);
                        }
                    }

                    parentNode = doc.SelectSingleNode("/Data/Destinations");
                    if (parentNode != null)
                    {
                        int sortValue = 1;
                        foreach (XmlNode child in parentNode.ChildNodes)
                        {
                            ProcessNode(child, 0, false, 1, customerId, sortValue++);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine();
            }
        }
    }
}


