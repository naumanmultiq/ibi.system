﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using mermaid.BaseObjects.IO;
using System.Configuration;
using System.Xml;

namespace IBI.UtilityConsole
{
    public class ScheduleMD5
    {
        public static void RefreshMD5()
        {
            try
            {

                string customers = ConfigurationSettings.AppSettings["customers"];
                string[] list = customers.Split(new char[] { ',' });
                //int[] custs = {2038,2040,2116,2120,2140,2141,2164,2176,2198,112164};


                foreach (string customerId in list)
                {
                            
                    DataSet ds = new DataSet();
                     

                    using (SqlConnection conTTS = new SqlConnection(ConfigurationSettings.AppSettings["TTSDatabaseConnection"]))
                    { 
                        using (SqlCommand cmdTTS = new SqlCommand())
                        {

                            cmdTTS.Connection = conTTS;
                            cmdTTS.CommandText = "SELECT Text, Voice FROM AudioFiles";
                            cmdTTS.CommandType = CommandType.Text;

                            SqlDataAdapter da = new SqlDataAdapter(cmdTTS);

                            da.Fill(ds);

                            foreach(DataRow dr in ds.Tables[0].Rows)
                            {
                                string stopName = dr["Text"].ToString();
                                string voice = dr["voice"].ToString();

                                string fileName = FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3";

                                string sql = "UPDATE AudioFiles set LastModified=GetDate(), FileName='" + fileName + "' WHERE Text='" +  stopName.Replace("\'", "\''") + "' AND Voice='" + voice + "'";
                         
                                // int result = dbContext.ExecuteStoreCommand(sql, null);    


                                cmdTTS.Connection = conTTS;
                                cmdTTS.CommandText = sql;
                                cmdTTS.CommandType = CommandType.Text;

                                conTTS.Open();
                                try
                                {
                                   cmdTTS.ExecuteNonQuery();   
                                }
                                catch
                                {
                                    Console.WriteLine("Error Executing: " + sql);
                                }
                                conTTS.Close();
                              
                            }
                           

                        } 
                    }
                }
            } 
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// Press '0' to generate MD5 for all the schedules
        /// Press '[SCHEDULE ID]' to generate MD5 for that schedule only
        /// </summary>
        /// <param name="schId"></param>
        public  static void GenerateMD5ForAllSchedules()
        {
            //try
            //{
            //    int schId = 1;

            //    Console.Write("Please enter a schedule number to regenerate MD5 checksum: ");

            //    Console.SetIn(new StreamReader(Console.OpenStandardInput(999999)));

            //    List<int> scheduleList = Console.ReadLine().Split(new char[]{','} , StringSplitOptions.RemoveEmptyEntries).Select(s=>s.Trim()).Select(int.Parse).ToList();
            //    //List<int> scheduleList = list.ToList<int>();
                
            //        using (IBIDataModel dbContext = new IBIDataModel())
            //        {

            //            var schedules = new List<Schedule>();

            //            if (schId == 0)
            //                schedules = dbContext.Schedules.ToList();
            //            else
            //            {   
            //                schedules = dbContext.Schedules.Where(s => scheduleList.Contains(s.ScheduleId)).ToList();
            //            }
                            


            //            foreach (Schedule schedule in schedules)
            //            {

            //                string xmlSchedule = schedule.Schedule1;
            //                string xmlScheduleXML = "";


            //                //generating StopSequence MD5

            //                string stopSequence = "";

            //                System.Collections.Generic.IEnumerable<int> indexes = Regex.Matches(xmlSchedule, @"""\d{16}\""").Cast<Match>().Select(m => m.Index);
            //                foreach (int i in indexes)
            //                {
            //                    stopSequence += UnTransformStopNo(xmlSchedule.Substring(i, 18).Trim('"')) + ",";
            //                }
            //                stopSequence = stopSequence.TrimEnd(',');

            //                string curStopSequenceMD5 = ConvertStringtoMD5(stopSequence);




            //                //XmlDocument scheduleDocument = new XmlDocument();
            //                //scheduleDocument.LoadXml(xmlSchedule);

            //                ////INSERT SCHEDULE ID into Offline Schedule/////
            //                //XmlNode root = scheduleDocument.DocumentElement;
            //                //XmlElement elem = scheduleDocument.CreateElement("ScheduleNumber");
            //                //elem.InnerText = schedule.ScheduleID.ToString();
            //                //root.InsertBefore(elem, root.FirstChild);

            //                //xmlSchedule = scheduleDocument.OuterXml;
            //                //xmlScheduleXML = xmlSchedule.Length > 38 ? xmlSchedule.Substring(38).Trim() : "";
            //                ////////////////////////////////////////////////

            //                string md5 = "";

            //                try
            //                {
            //                    XmlDocument scheduleDocument = new XmlDocument();
            //                    scheduleDocument.LoadXml(xmlSchedule);
                                  

            //                    List<String> invalidTags = new List<string>();
            //                    invalidTags.Add("/Schedule/ScheduleNumber");
            //                    invalidTags.Add("/Schedule/BusNumber");
            //                    invalidTags.Add("/Schedule/JourneyNumber");
            //                    invalidTags.Add("/Schedule/Cached");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/ArrivalTime");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/DepartureTime");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/PlannedArrivalTime");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/PlannedDepartureTime");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/AADT_ArrivalTime");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/ADDT_DepartureTime");
            //                    invalidTags.Add("/Schedule/JourneyStops/StopInfo/LMDT_Time");



            //                    foreach (String invalidNodePath in invalidTags)
            //                    {
            //                        foreach (XmlNode invalidNode in scheduleDocument.SelectNodes(invalidNodePath))
            //                        {
            //                            invalidNode.ParentNode.RemoveChild(invalidNode);
            //                        }
            //                    }

            //                    //INSERT SCHEDULE ID into Offline Schedule/////
                                
            //                    XmlNode root = scheduleDocument.DocumentElement;
            //                    XmlElement elem = scheduleDocument.CreateElement("ScheduleNumber");
            //                    elem.InnerText = schedule.ScheduleID.ToString();
            //                    root.InsertBefore(elem, root.FirstChild);


            //                    xmlSchedule = scheduleDocument.OuterXml;

            //                    md5 = ConvertStringtoMD5(xmlSchedule);
            //                }
            //                catch (Exception ex)
            //                {
            //                    Console.WriteLine("Schedule # " + schedule.ScheduleID + ": Error [" + ex.Message + "]");
            //                }


            //                ////generate StopSequenceMD5                                               

            //                //string stopSequence = "";
            //                //foreach (StopHop stop in journey.JourneyStops)
            //                //{
            //                //    stopSequence += stop.StopNumber + ",";
            //                //}
            //                //stopSequence = stopSequence.TrimEnd(',');

            //                //string curStopSequenceMD5 = Util.ConvertStringtoMD5(stopSequence);

            //                //IBI.DataAccess.DBModels.Schedule sch = dbContext.Schedules.Where(s => s.ScheduleID == schId.Value).FirstOrDefault();

            //                //if (schedule.MD5 != md5)
            //                //{
            //                xmlScheduleXML = xmlSchedule.Length > 38 ? xmlSchedule.Substring(38).Trim() : "";
            //                schedule.Schedule1 = xmlSchedule;
            //                schedule.MD5 = md5;
            //                schedule.StopsSequenceMD5 = curStopSequenceMD5;
            //                schedule.Updated = DateTime.Now;
            //                schedule.ScheduleXML = xmlScheduleXML;

            //                // sch.StopCount = tmpSchedule.JourneyStops.Length;
            //                //}

            //                //var values2 = dbContext.SyncSchedules(busNumber, tmpSchedule.Line, tmpSchedule.FromName, tmpSchedule.DestinationName, tmpSchedule.ViaName, tmpSchedule.JourneyStops.Length, xmlSchedule, xmlScheduleXML, md5);

            //            }

            //            dbContext.SaveChanges();
            //        }  


                 
                
                
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
             

        }
         
        
        #region "Test Utility to fix"

        static public string Beautify(XmlDocument doc)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.Default;
            settings.IndentChars = "\t";
            settings.NewLineChars = "\r\n";
            settings.NewLineHandling = NewLineHandling.Replace;
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }
            return sb.ToString();
        }

        public static void FixScheduleXML()
        {

            ////string tmp = ConvertStringtoMD51("d:\\schedule_90_B.xml");

            ////string var1 = File.ReadAllText("D:\\schedule_90_B.xml");
            ////string var2 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>  <Schedule xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">   <ScheduleNumber>1112</ScheduleNumber>   <Line/>   <FromName>Oslo Lufthavn Gardermoen</FromName>   <DestinationName>SAS Flybussen</DestinationName>   <ViaName>Oslo sentrum|Ekstra-avgang</ViaName>   <JourneyStops>    <StopInfo StopNumber=\"1000902\">     <StopName>Oslo Lufthavn Gardermoen</StopName>     <GPSCoordinateNS>60.193542</GPSCoordinateNS>     <GPSCoordinateEW>11.096488</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000802\">     <StopName>Furuset</StopName>     <GPSCoordinateNS>59.938021</GPSCoordinateNS>     <GPSCoordinateEW>10.891878</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000702\">     <StopName>Trosterud</StopName>     <GPSCoordinateNS>59.929674</GPSCoordinateNS>     <GPSCoordinateEW>10.864845</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000602\">     <StopName>Ulvenkrysset</StopName>     <GPSCoordinateNS>59.919052</GPSCoordinateNS>     <GPSCoordinateEW>10.816842</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000502\">     <StopName>Teisenkrysset</StopName>     <GPSCoordinateNS>59.91774</GPSCoordinateNS>     <GPSCoordinateEW>10.811804</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000402\">     <StopName>Helsfyr</StopName>     <GPSCoordinateNS>59.912811</GPSCoordinateNS>     <GPSCoordinateEW>10.79924</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000302\">     <StopName>Oslo Bussterminal</StopName>     <GPSCoordinateNS>59.91154154726846</GPSCoordinateNS>     <GPSCoordinateEW>10.75835747821061</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000202\">     <StopName>Prof. Aschaugs plass</StopName>     <GPSCoordinateNS>59.914592</GPSCoordinateNS>     <GPSCoordinateEW>10.741188</GPSCoordinateEW>    </StopInfo>    <StopInfo StopNumber=\"1000101\">     <StopName>Radisson BLU Scandinavia Hotel</StopName>     <GPSCoordinateNS>59.918886</GPSCoordinateNS>     <GPSCoordinateEW>10.733982</GPSCoordinateEW>    </StopInfo>   </JourneyStops>  </Schedule>";

            ////XmlDocument sdoc = new XmlDocument();
            ////sdoc.LoadXml(var2);
            //////var2 = Beautify(sdoc);
            //////var2 = var2.Replace("utf-16", "utf-8");


            ////string tmp2 = ConvertStringtoMD5(File.ReadAllText("D:\\schedule_90_B.xml"));
            ////string tmp3 = ConvertStringtoMD5(sdoc.OuterXml);
            ////return;


            //Console.WriteLine("Enter Customer ID = ");
            //string custId = Console.ReadLine();
            //int customerId= int.Parse(custId);

            //    using (IBIDataModel dbContext = new IBIDataModel())
            //    {                      
            //        var schedules = dbContext.Schedules.Where(s=>s.CustomerID==customerId && (s.ViaName!=null && s.ViaName!=""));

            //        foreach(DataAccess.DBModels.Schedule schedule in schedules)
            //        {  
            //            string xmlSchedule = schedule.Schedule1;
            //            string xmlScheduleXml = "";

            //            XmlDocument xDoc = new XmlDocument();
            //            xDoc.LoadXml(xmlSchedule);

            //            XmlNode root = xDoc.DocumentElement;


            //            XmlNode destNode = root.SelectSingleNode("DestinationName");
            //            XmlNode viaNode = root.SelectSingleNode("ViaName");

            //            if(viaNode==null && String.IsNullOrEmpty(schedule.ViaName)==false)
            //            {
            //                XmlElement viaElement = xDoc.CreateElement("ViaName");
            //                viaElement.InnerText = schedule.ViaName;
                             
            //                root.InsertAfter(viaElement, destNode);    
            //            }
                           
            //                xmlSchedule = xDoc.OuterXml;
            //                xmlScheduleXml = xmlSchedule.Length > 38 ? xmlSchedule.Substring(38).Trim() : "";
            //                //////////////////////////////////////////////
                            
            //                string md5 = ConvertStringtoMD5(xmlSchedule);
                         
            //                if (schedule.MD5 != md5)
            //                {
            //                    schedule.ScheduleXML = xmlScheduleXml;
            //                    schedule.Schedule1 = xmlSchedule;
            //                    schedule.MD5 = md5;
            //                    schedule.Updated = DateTime.Now;
            //                }
            //        }
                     

            //        dbContext.SaveChanges();
            //    }
           
        }

        #endregion

        public static string ConvertStringtoMD51(string path)
        {
            return mermaid.BaseObjects.IO.FileHash.FromFile( new FileInfo( path), false).ToString();
        }

        public  static string ConvertStringtoMD5(string strword)
        {
            return mermaid.BaseObjects.IO.FileHash.FromStream(GenerateStreamFromString(strword)).ToString();
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;            
            return stream;
        }


        private static String PrintXML(String XML)
        {
            String Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.UTF8);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(XML);

                writer.Formatting = Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                String FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException)
            {
            }

            mStream.Close();
            writer.Close();

            return Result;
        }

        private string TransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = "9025200" + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }

        private static string UnTransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.unTransformStopNo"))
            {

                //eg. 9025200000028662
                if (stopNo.Length == 16) // it's a transformed stop number
                {
                    stopNo = Convert.ToInt32(stopNo.Replace("9025200", "")).ToString();
                }

                return stopNo;
            }
        }

    }
}
