﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IBI.DataAccess.DBModels;
using IBI.DataAccess;
using IBI.Shared;
using System.Data.Entity.Spatial;
using System.Globalization;

namespace UTM2DecimalConverter
{
    public partial class GPSConversion : Form
    {
        public GPSConversion()
        {
            InitializeComponent();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            try
            {

                using (IBI.DataAccess.DBModels.IBIDataModel dbContext = new IBI.DataAccess.DBModels.IBIDataModel())
                {
                    var stops = dbContext.Stops;

                    foreach (var stop in stops)
                    {
                        string lat = string.Empty;
                        string lng = string.Empty;

                        /*****following 2 lines must not be commented for the first time we are going to build this utility*****/
                        //lat = stop.OriginalGPSCoordinateNS;
                        //lng = stop.OriginalGPSCoordinateEW;


                        string latitude = "";
                        string longitude = "";

                        string errorDetail = "";
                        try
                        {
                            if (lat.IndexOf('.') >= 4 || lng.IndexOf('.') >= 4)
                                {
                                    errorDetail = "UMT32 Conversion";

                                    string[] gps = GPSConverter.UTMToDec(lng, lat, 32);
                                    latitude = gps[0];
                                    longitude = gps[1];

                                    string sqlPoint = string.Format("POINT({0} {1})", longitude.ToString().Replace(',', '.'), latitude.ToString().Replace(',', '.'));
                                    stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                                    stop.OriginalGPS = stop.StopGPS;
                                    stop.LastUpdated = DateTime.Now;

                                }
                                else
                                {
                                    errorDetail = "Simple GPS Conversion";

                                    string sqlPoint = string.Format("POINT({0} {1})", lng, lat);
                                    stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                                    stop.OriginalGPS = stop.StopGPS;
                                    stop.LastUpdated = DateTime.Now;
                                }

                        }
                        catch (Exception exInner)
                        {
                            MessageBox.Show(String.Format("Error Transforming GPS of GID : {0}, Lat: {1} to {2} , Lon: {3} to {4} \n\n {5} \n\n {6}", stop.GID.ToString(), lat, latitude, lng, longitude, errorDetail, IBI.Shared.Diagnostics.Logger.GetDetailedError(exInner)));
                        }
                    }

                    dbContext.SaveChanges();
                    MessageBox.Show("GPS of all stops transformed successfully");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error While transforming Stop GPS erro: {0}", IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
            }
        }
    }
}
