﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using IBI.Shared;
using IBI.Shared.Models.BusTree;

namespace UTM2DecimalConverter
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IBI.Shared.Models.BusTree.Bus b = GetBus(2140, "1225");
            txt.Text = b.LastKnownLocation.Latitude.ToString();

        }

        public static IBI.Shared.Models.BusTree.Bus GetBus(int customerId, string busNumber)
        {

            ////#hack to save geography information
            //var point = SqlGeography.Point(double.Parse(latitude), double.Parse(longitude), 4326);
            //var poly = point.BufferWithTolerance(1, 0.01, true);

            string sql = @"SELECT * FROM vBuses WHERE CustomerId=" + customerId + " AND BusNumber='" + busNumber + "'";

            using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["IBIConnectionString"].ToString()))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    //var param = new SqlParameter(@"Location", poly);
                    //param.UdtTypeName = "Geography";
                    //command.Parameters.Add(param);

                    IBI.Shared.Models.BusTree.Bus bus = new IBI.Shared.Models.BusTree.Bus();

                    try
                    {

                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            bus.BusNumber = AppUtility.IsNullStr(reader["BusNumber"]);
                            bus.BusAlias = AppUtility.IsNullStr(reader["BusAlias"]);
                            bus.CustomerId = AppUtility.IsNullInt(reader["CustomerId"]);
                            bus.CustomerName = AppUtility.IsNullStr(reader["CustomerName"]);
                            bus.ParentGroupId = AppUtility.IsNullInt(reader["ParentGroupId"]);
                            bus.ParentGroupName = AppUtility.IsNullStr(reader["ParentGroupName"]);
                            bus.CurrentJourneyNumber = AppUtility.IsNullLong(reader["CurrentJourneyNumber"]);
                            bus.Description = AppUtility.IsNullStr(reader["Description"]);
                            bus.InOperation = AppUtility.IsNullBool(reader["InOperation"]);
                            bus.LastTimeInService = AppUtility.IsNullDate(reader["LastTimeInService"], DateTime.MinValue);
                            bus.DateAdded = AppUtility.IsNullStr(reader["DateAdded"]);
                            bus.DateModified = AppUtility.IsNullStr(reader["DateModified"]);

                            dynamic poly = reader["LastKnownLocation"];

                            Microsoft.SqlServer.Types.SqlGeography geo = new Microsoft.SqlServer.Types.SqlGeography();

                            try
                            {
                                geo = (Microsoft.SqlServer.Types.SqlGeography)poly;

                                if (!geo.IsNull)
                                {
                                    //convert DB data to custom "IBI.Shared.Models.BusTree.Geography" type. 
                                    //We will enhance "IBI.Shared.Models.BusTree.Geography" type, once we get more details about its implementation.
                                    Geography lastKnownLocation = new Geography()
                                    {
                                        Latitude = !geo.EnvelopeCenter().Lat.IsNull ? geo.EnvelopeCenter().Lat.Value : 0.00,
                                        Longitude = !geo.EnvelopeCenter().Long.IsNull ? geo.EnvelopeCenter().Long.Value : 0.00

                                    };

                                    bus.LastKnownLocation = lastKnownLocation;
                                }
                            }
                            catch (Exception ex)
                            {
                               // Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                                //ignore exception and set default Geography info.
                                bus.LastKnownLocation = new Geography(0.00, 0.00);
                            }



                            reader.Close();
                            connection.Close();

                            return bus;

                        }
                    }
                    catch (Exception ex) { throw ex; }

                    finally
                    {
                        if (connection.State != System.Data.ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
            return null;

        }
    }
}
