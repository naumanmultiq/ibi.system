﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IBI.HaConVDV
{
    public partial class HaConVDVService : ServiceBase
    {

        HaConVDVController serverInstance;
        public HaConVDVService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            serverInstance = new HaConVDVController();
        }

        protected override void OnStop()
        {
            serverInstance.Dispose();
        }
    }
}
