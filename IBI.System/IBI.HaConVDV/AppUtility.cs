﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Diagnostics;
using IBI.Shared.Diagnostics;

namespace IBI.HaConVDV
{
    class AppUtility
    {
        public static IFormatProvider GetDateFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd";
            dateFormat.LongDatePattern = "yyyy-MM-dd";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd";
            dateFormat.DateSeparator = "-";

            return dateFormat;
        }

        public static IFormatProvider GetDateTimeFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.LongDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.DateSeparator = "-";
            dateFormat.TimeSeparator = ":";

            return dateFormat;
        }

        public static string SafeSqlLiteral(string inputSql)
        {
            if (!String.IsNullOrEmpty(inputSql))
                return inputSql.Replace("'", "''");
            else
                return "";
        }

        public static string ConvertToSqlPoint(String gps)
        {
            String point = "NULL";

            if (!String.IsNullOrEmpty(gps.Trim()))
            {
                String[] gpsSplit = gps.Split(',');

                if (gpsSplit.Length == 2)
                {
                    String longitude = gpsSplit[1].Trim();
                    String latitude = gpsSplit[0].Trim();

                    if (!String.IsNullOrEmpty(longitude) && !String.IsNullOrEmpty(latitude))
                        point = "geography::STGeomFromText('POINT(" + longitude + " " + latitude + ")', 4326)";
                }
            }

            return point;
        }

         
        public static void WriteLine(String line)
        {
            //Trace.TraceInformation(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss> ") + line);
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI_SERVER, line);
        }

        public static void WriteError(String line)
        {
            //Trace.TraceError(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss> ") + line);
            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, line);
        }

        public static void WriteError(Exception ex)
        {
            String message = "";

            Exception currentException = ex;

            while (currentException != null)
            {
                message += string.Format("{0}{1}", currentException.Message, Environment.NewLine);
                message += currentException.StackTrace + Environment.NewLine;
                message += "--" + Environment.NewLine;

                currentException = currentException.InnerException;
            }

            //Trace.TraceError(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss> ") + message);
            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, ex);
        }

        public static TimeSpan DifferenceToMidnight()
        {
            DateTime midNight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            return (midNight - DateTime.Now);
        }

        public static void AppendLog(ref StringBuilder log, string textLine2Append, bool isError = false)
        {
            if (AppSettings.LogAllMessages() || isError)
                log.AppendLine(textLine2Append);          

        }

        public static void Log2File(string logName, string message, bool doWrite = false, bool isError = false)
        {

            //mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI_" + logName, message);

            if (doWrite || AppSettings.LogAllMessages() || isError)
                mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI_" + logName, message);
        }

        //public static void Log2File(string logName, Exception ex, bool doWrite = false)
        //{
        //    if (doWrite || AppSettings.LogAllMessages())
        //        mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI_" + logName, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex));
        //}
    }
}
