﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Net;
using System.Xml.Linq;


namespace IBI.HaConVDV
{
    public class VDV : IDisposable
    {

        #region Members

        private int CustomerId { get; set; }
        private string Sender { get; set; }
        private string Line { get; set; }
        private int PostIntervalInSeconds { get; set; }
        private int PingToleranceInSeconds {get; set;}
        private string HRX_Url { get; set; }
        private string HRX_Username { get; set; }
        private string HRX_Password { get; set; }

        private System.Timers.Timer tmrHRXPost
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public HRXDataPost()
        {
            AppUtility.Log2File("DataService", "HRXDataPost new instance started");

            this.PostIntervalInSeconds = AppSettings.PostIntervalInSeconds();

            int[] customerIds = AppSettings.HaConHRXPostCustomerIds();
            if(customerIds.Length>0)
            {
                this.CustomerId = customerIds[0];
            }

            string[] senders = AppSettings.HaConHRXPostSenders();
            if (senders.Length > 0)
            {
                this.Sender = senders[0];
            }

            string[] lines = AppSettings.HaConHRXPostLines();
            if (lines.Length > 0)
            {
                this.Line = lines[0];
            }


            this.PingToleranceInSeconds = AppSettings.PingToleranceInSeconds();

            this.HRX_Url = AppSettings.HaConHRXPost_EndPoint_Url();
            this.HRX_Username = AppSettings.HaConHRXPost_EndPoint_Username();
            this.HRX_Password = AppSettings.HaConHRXPost_EndPoint_Password();

            this.tmrHRXPost = new System.Timers.Timer();
            this.tmrHRXPost.Interval = TimeSpan.FromSeconds(PostIntervalInSeconds).TotalMilliseconds;
            this.tmrHRXPost.Elapsed += new System.Timers.ElapsedEventHandler(tmrHRXPost_Elapsed);
            this.tmrHRXPost.Enabled = true;

            AppUtility.Log2File("DataService", "DataService timer Initializes, timer is " + this.tmrHRXPost.Enabled); 
        }

        #endregion

        #region Events

        private void tmrHRXPost_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.tmrHRXPost.Enabled = false;
            AppUtility.Log2File("DataService", "DataService Timer Stops + ProcessLog Starts");


            try
            {
               string data = this.GetHRXData();
               PostHRX(data);

            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("DataService", "DataService Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                this.StartDataServiceTimer();
                AppUtility.Log2File("DataService", "DataService Timer initializes again");
            }
        }


        public void Dispose()
        {
            this.tmrHRXPost.Enabled = false;
            this.tmrHRXPost.Dispose();

        }

        #endregion

        #region Methods

        private void StartDataServiceTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.tmrHRXPost == null)
            {
                this.tmrHRXPost = new System.Timers.Timer();
                this.tmrHRXPost.Elapsed += new System.Timers.ElapsedEventHandler(tmrHRXPost_Elapsed);
            }

            this.tmrHRXPost.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(this.PostIntervalInSeconds); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            this.tmrHRXPost.Interval = TimeSpan.FromSeconds(PostIntervalInSeconds).TotalMilliseconds; ;
            this.tmrHRXPost.Enabled = true;

            AppUtility.Log2File("DataService", "DataService Timer initializes");
            AppUtility.Log2File("DataService", "Next DataService processing at " + nextTick.ToString());

        }


        private string GetHRXData()
        {
            StringBuilder log = new StringBuilder();
            String result = string.Empty;

            if(AppSettings.HaConHRXPostEnabled())
            {
                    
                try
                {
                    using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                    {
                        connection.Open();

                        using (new IBI.Shared.CallCounter("HRX Data Post"))
                        {
                            String xmlData = "";
                            String script = "GetHaConHRXData";

                            using (SqlCommand command = new SqlCommand(script, connection))
                            {
                                command.CommandType = System.Data.CommandType.StoredProcedure;

                                command.Parameters.AddWithValue("CustomerId", this.CustomerId);
                                command.Parameters.AddWithValue("Line", this.Line);
                                command.Parameters.AddWithValue("PingToleranceInSeconds", this.PingToleranceInSeconds);


                                XmlReader reader = command.ExecuteXmlReader();

                                bool bHasData = reader.Read();
                                while (reader.ReadState != System.Xml.ReadState.EndOfFile)
                                {
                                    xmlData += reader.ReadOuterXml();
                                }

                                reader.Close();
                                connection.Close();

                                if (!bHasData)
                                {
                                    log.Append("No records found from DB" + Environment.NewLine);
                                }
                                else
                                {
                                    log.Append("Data: " + Environment.NewLine + xmlData + Environment.NewLine);

                                    //XmlDocument doc = new XmlDocument();
                                    //doc.LoadXml(xmlData);

                                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                                    MemoryStream stream = new MemoryStream(encoding.GetBytes(xmlData));
                                    XDocument xDoc = XDocument.Load(stream);
                                    xDoc.Descendants("PlannedDepartureTime").ToList().ForEach(b => b.Value = string.Concat(b.Value, "+02:00"));

                                    log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> XSLT Transformation starts" + Environment.NewLine);



                                    XsltArgumentList argsList = new XsltArgumentList();
                                    argsList.AddParam("FetchTime", "", DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss+02:00"));
                                    argsList.AddParam("Sender", "", this.Sender);
                                    //argsList.AddParam("Line", "", this.Line);
                                    argsList.AddParam("Operator", "", "Movia");
                                     
                                    

                                    XslCompiledTransform transform = new XslCompiledTransform(true);
                                    string xsltFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data\\HRX.xslt");

                                    transform.Load(xsltFilePath);

                                    using (StringWriter writer = new StringWriter())
                                    {
                                        //xDoc.create
                                            
                                        //transform.Transform(xDoc.Create.CreateNavigator(), argsList, writer);
                                        transform.Transform(xDoc.CreateReader(), argsList, writer);
                                        result = writer.ToString();

                                        writer.Dispose();
                                    }

                                    transform = null;
                                    xDoc = null;
                                    //xmlData = string.Empty;


                                    log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> XSLT Transformation ends" + Environment.NewLine);
                                }

                            }
                        }
                        
                        if(connection.State!= ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
                catch(Exception ex)
                {
                    log.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> ERROR !!!" + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine);
                } 
             

            }
            else
            {
                //AppUtility.Log2File("DataService", "DataService Not enabled");
            }


            AppUtility.Log2File("DataService", log.ToString());

            return result;
        }


        private string PostHRX(string data)
        {
            string result = string.Empty;
            try
            {              
            
               

                //WebClient webClient = new WebClient();
                //ICredentials credentials = null;
                ////webClient.Credentials = new NetworkCredential(this.HRX_Username, this.HRX_Password);

                //webClient.Encoding = System.Text.Encoding.UTF8;
                 


                DateTime serverStartTime = DateTime.Now;

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(data);

                
                result =  SOAPRequest.CallWebService(this.HRX_Url, this.HRX_Url, xDoc.DocumentElement.OuterXml);

                AppUtility.Log2File("DataService", String.Format("Result From HaCon:\n{0}\n\nWebClient Call Completed\n\n", result));
                // result = webClient.UploadString(this.HRX_Url, xDoc.DocumentElement.OuterXml);

                //webClient.Dispose();

                //AppUtility.Log2File("DataService", "WebClient Call Succeeded" + Environment.NewLine);
            }
            catch(Exception ex)
            {
                AppUtility.Log2File("DataService", "WebClient Call fails::" + ex.Message + " . " + ex.StackTrace + Environment.NewLine);
            }

            return result;
        }

        #endregion

    }


public class SOAPRequest
{
public static string CallWebService(string _url, string _action, string data)
{
//var _url = "http://xxxxxxxxx/Service1.asmx";
 //   var _action = "http://xxxxxxxx/Service1.asmx?op=HelloWorld";

    XmlDocument soapEnvelopeXml = CreateSoapEnvelope(data);
    HttpWebRequest webRequest = CreateWebRequest(_url, _action);
    InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);
    
    // begin async call to web request.
    IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

    // suspend this thread until call is complete. You might want to
    // do something usefull here like update your UI.
    asyncResult.AsyncWaitHandle.WaitOne();

    // get the response from the completed web request.
    string soapResult;
    using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
    {
        using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
        {
            soapResult = rd.ReadToEnd();
        }
        return soapResult;        
    }

    return string.Empty;
}

private static HttpWebRequest CreateWebRequest(string url, string action)
{
    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
    webRequest.Headers.Add("SOAPAction", action);
    webRequest.ContentType = "text/xml;charset=\"utf-8\"";
    webRequest.Accept = "text/xml";
    webRequest.Method = "POST";
    return webRequest;
}

private static XmlDocument CreateSoapEnvelope(string data)
{
    XmlDocument soapEnvelop = new XmlDocument();
    //soapEnvelop.LoadXml(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body><HelloWorld xmlns=""http://tempuri.org/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><int1 xsi:type=""xsd:integer"">12</int1><int2 xsi:type=""xsd:integer"">32</int2></HelloWorld></SOAP-ENV:Body></SOAP-ENV:Envelope>");
    soapEnvelop.LoadXml(string.Format(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body>{0}</SOAP-ENV:Body></SOAP-ENV:Envelope>", data));

    //soapEnvelop.LoadXml(string.Format("<soap:Envelope xmlnsTongue Tiedoap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body>{0}</soap:Body></soap:Envelope>", data));
    return soapEnvelop;
}

private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
{
    using (Stream stream = webRequest.GetRequestStream())
    {
        soapEnvelopeXml.Save(stream);
    }
}
}

}
