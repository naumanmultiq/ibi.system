﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace IBI.Web.VDV.Controllers
{
    public class VdvController : Controller
    {


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SenderResponse(string sender)
        {

            string response = string.Empty;


            StringBuilder log = new StringBuilder();
            var postData = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Logger.Log(string.Format("Call received for Sender \"{0}\". Posted Data: {1} ", sender, postData));

            string[] validSenders = System.Configuration.ConfigurationManager.AppSettings["Senders"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim().ToLower()).ToArray();

            bool isValidSender = false;
            foreach (string validSender in validSenders)
            {
                if (sender.ToLower() == validSender)
                {
                    isValidSender = true;
                    response = ProcessRequest(sender);
                    break;
                }
            }

            if (!isValidSender)
            {
                Logger.Log(string.Format("No response sent back ", sender));
                response = string.Empty;
            }

            string mimeType = "text/xml";
            return this.Content(response, mimeType, Encoding.Default);

        }


        #region "Private Helper"

        private string ProcessRequest(string sender)
        {

            //Logger.Log(string.Format("Call received for Sender \"{0}\"", sender));

            try
            { 

                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("<?xml version=\"1.0\" encoding=\"iso-8859-15\"?><DataReadyAnswer><Acknowledge TimeStamp=\"{0}\" Result=\"ok\" ErrorNumber=\"0\"></Acknowledge></DataReadyAnswer>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")));

                //IBI.Shared.MSMQ.VDVQueueManager queue = new Shared.MSMQ.VDVQueueManager(sender);
                //queue.AppendToQueue(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
               
                
               
                System.IO.File.WriteAllText(Path.Combine(Server.MapPath("~"), string.Format(ConfigurationManager.AppSettings["DataReadyStatusFile"].ToString().Replace("{SENDER}", "{0}"), sender)),
                        DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss"),
                        Encoding.Default);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);

            }

            return string.Empty;
        }

        #endregion

    }
}
