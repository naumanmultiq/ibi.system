﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace IBI.Web.VDV.Handlers
{
    public class StopSyncHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        #endregion

         
        public void ProcessRequest(HttpContext context)
        {
            string response = string.Empty;


            StringBuilder log = new StringBuilder();
            var postData = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();

            Logger.Log(string.Format("Call received for Host \"{0}\". Posted Data: {1} ", context.Request.UserHostAddress , postData));

            
            response = ProcessRequest(postData);
            
            SendResponse(context, response);

        }


        #region "Private Helper"

        private void SendResponse(HttpContext context, string data)
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            //sb.Append(GetDestinationTree(int.Parse(customerID)));
            sb.Append(data);

            Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

            //Set the HTTP MIME type of the output stream 
            context.Response.ContentType = "text/xml";

            //Write the data out to the client. 
            context.Response.BinaryWrite(fileContent);
        }

        private string ProcessRequest(string postData)
        {

            //Logger.Log(string.Format("Call received for Sender \"{0}\"", sender));

            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("<?xml version=\"1.0\" encoding=\"iso-8859-15\"?><StopDataAnswer><Acknowledge TimeStamp=\"{0}\" Result=\"ok\" ErrorNumber=\"0\"></Acknowledge></StopDataAnswer>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")));
 
                //System.IO.File.WriteAllText(Path.Combine(Server.MapPath("~"), string.Format(ConfigurationManager.AppSettings["DataReadyStatusFile"].ToString().Replace("{SENDER}", "{0}"), sender)),
                //        DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"),
                //        Encoding.Default);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);

            }

            return string.Empty;
        }

        #endregion


    }
}