﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IBI.SignQueueProcessor
{
    public partial class SignQueueProcessorService : ServiceBase
    {
        SignQueueController serverInstance;
        public SignQueueProcessorService()
        {
            InitializeComponent();
        }
         

        protected override void OnStart(string[] args)
        {
            serverInstance = new SignQueueController();
        }

        protected override void OnStop()
        {
            serverInstance.Dispose();
        }
    }
}
