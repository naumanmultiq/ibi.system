﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace IBI.SignQueueProcessor
{
    public class AppSettings
    {
        public static bool SignQueueEnabled() {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SignQueueEnabled"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["SignQueueEnabled"]);
            }

            return retVal;
        }

         public static bool LogAllMessages()
         {
             bool retVal = true;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
             {
                 retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
             }

             return retVal;
         }

         public static bool SeparateViaNameFromDestination()
         {
             bool retVal = true;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SeparateViaNameFromDestination"]))
             {
                 retVal = bool.Parse(ConfigurationManager.AppSettings["SeparateViaNameFromDestination"]);
             }

             return retVal;
         }
        
        
         
    }
}