﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;

namespace IBI.SignQueueProcessor
{
    public class SignQueueController
    {
        #region Members

        private System.Timers.Timer TmrProcessQueue
        {
            get;
            set;
        }

        #endregion

        public SignQueueController()
        {

            AppUtility.Log2File("SignQueueManager", "new instance started");

            this.StartProcessTimer();

            //AppUtility.Log2File("SignQueueManager", "TmrProcessQueue timer Initializes, timer is " + this.TmrProcessQueue.Enabled); 
        }


        private void StartProcessTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.TmrProcessQueue == null)
            {
                this.TmrProcessQueue = new System.Timers.Timer();
                this.TmrProcessQueue.Elapsed += new System.Timers.ElapsedEventHandler(TmrProcessQueue_Elapsed);
            }

            this.TmrProcessQueue.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(30); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            nextTick = nextTick.AddSeconds(-1 * nextTick.Second);

            this.TmrProcessQueue.Interval = Math.Max(30000, nextTick.Subtract(DateTime.Now).TotalMilliseconds);
            this.TmrProcessQueue.Enabled = true;

            //AppUtility.Log2File("SignQueueManager", "TmrProcessQueue Timer initializes");
            //AppUtility.Log2File("SignQueueManager", "Next Queue processing at " + nextTick.ToString());

        }


        private void TmrProcessQueue_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.TmrProcessQueue.Enabled = false;
           // AppUtility.Log2File("SignQueueManager", "ProcessLog Timer Initializes");


            try
            {
                this.ProcessSignQueue();
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("SignQueueManager", "Queue Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                this.StartProcessTimer();
                //AppUtility.Log2File("SignQueueManager", "Queue Timer initializes again");
            }
        }

        public void ProcessSignQueue()
        {
            if (!AppSettings.SignQueueEnabled())
                return;

            //AppUtility.Log2File("SignQueueManager", "Queue --> Loop on Sign Queue");

            IBI.Shared.MSMQ.MSMQManager msgQueue = new IBI.Shared.MSMQ.MSMQManager(ConfigurationManager.AppSettings["SignQueueName"].ToString());

            while (msgQueue.Count() > 0)
            {

                System.Messaging.Message msg = msgQueue.Receive();

                msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });

                string msgBody = msg.Body.ToString();

                if (!string.IsNullOrEmpty(msgBody))
                {

                    string[] info = msg.Body.ToString().Split('|');

                    int customerId = info.Length > 0 ? int.Parse(info[0]) : 2140;
                    string line = info.Length > 1 ? info[1] : "";
                    string destination = info.Length > 2 ? info[2] : "";
                    string viaName = info.Length > 3 ? info[3] : "";

                    string viaSeparator = ConfigurationManager.AppSettings["ViaSeparator"].ToString();

                    //if (AppSettings.LogAllMessages() &&
                    //    string.IsNullOrEmpty(viaName) &&
                    //    destination.Contains(", via "))
                    //{
                    //    viaName = destination.Substring(destination.IndexOf(", via ") + 6);
                    //    destination = destination.Substring(0, destination.IndexOf(", via "));
                    //}

                    if (AppSettings.SeparateViaNameFromDestination() && 
                        string.IsNullOrEmpty(viaName) &&
                        destination.Contains(viaSeparator))
                    {
                        viaName = destination.Substring(destination.IndexOf(viaSeparator) + viaSeparator.Length);
                        destination = destination.Substring(0, destination.IndexOf(viaSeparator));
                    }

                    AppUtility.Log2File("SignQueueManager", string.Format("Processing Item from Sign Queue -->{0},", msg.Body.ToString()), true);
                    AppUtility.Log2File("SignQueueManager", string.Format("Parsed Info:  CustomerId:{0}, Line:{1}, Destination:{2}, ViaName:{3}", customerId, line, destination, viaName), true);
                    try
                    {
                        bool result = IBI.Implementation.Schedular.SignController.CheckSignForNewSchedule(customerId, line, destination, viaName, "Hogia_Journey_SignOn");

                        if (result)
                        {
                            AppUtility.Log2File("SignQueueManager", "Sign Created Successfully", true);
                        }
                        else
                        {
                            AppUtility.Log2File("SignQueueManager", "Queue Item discarded, [Already exists] OR [Error occured --> System Logs]", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        // throw new Exception(string.Format("Errors while creating new sign during Hogia realtime journey sign on. [{0}]", ex.Message));
                        AppUtility.Log2File("SignQueueManager", "Sign Queue Timer CRASHES while generating Signs/Sign Images -->" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex), true);
                        AppUtility.WriteError("Sign Queue Timer CRASHES while generating Signs/Sign Images. " + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex));
                    }
                }


            }//end while


        }

        public void Dispose()
        {
            this.Dispose();
        }
    }
}
