﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Spatial;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FixZoneInfoForSchedulesAndJourneys
{

    public partial class Form1 : Form
    {
        Log log = new Log("UtilityLogs");

        int CUSTOMER_ID = int.Parse(ConfigurationSettings.AppSettings["CustomerId"]);
        string STOP_SOURCE = "HAFAS";



        List<KeyValuePair<string, string>> StopNamesCache { get; set; } //value: stopname
        List<KeyValuePair<string, string>> StopCoordCache { get; set; } //value: lat|lon
        List<CorrectedStop> CorrectedStopsCache { get; set; } //value: lat|lon

        List<String> Lines { get; set; }

        //public int CustomerId { get; set; }

        public string TargetDirectory { get; set; }
        public string StopCoordFilepath { get; set; }
        public string StopNamesFilepath { get; set; }
        public string CorrectedStopsFilepath { get; set; }

        string[] StopNamesData { get; set; }
        string[] StopCoordData { get; set; }
        string[] CorrectedStopsData { get; set; }

        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {

            StopNamesCache = new List<KeyValuePair<string, string>>();
            StopCoordCache = new List<KeyValuePair<string, string>>();
            CorrectedStopsCache = new List<CorrectedStop>();

            TargetDirectory = Environment.CurrentDirectory;
            if (Directory.Exists(TargetDirectory))
            {

                StopCoordFilepath = System.IO.Path.Combine(TargetDirectory, "bfkoord");
                StopNamesFilepath = System.IO.Path.Combine(TargetDirectory, "bahnhof");
                CorrectedStopsFilepath = System.IO.Path.Combine(TargetDirectory, "correctedstops");


                if (String.IsNullOrEmpty(StopNamesFilepath) || !File.Exists(StopNamesFilepath))
                {
                    throw new Exception("Please provide stop name file path");
                }
                if (String.IsNullOrEmpty(StopCoordFilepath) || !File.Exists(StopCoordFilepath))
                {
                    throw new Exception("Please provide stop coord file path");
                }
                if (String.IsNullOrEmpty(CorrectedStopsFilepath) || !File.Exists(CorrectedStopsFilepath))
                {
                    throw new Exception("Please provide correctedstops file path");
                }

                StopNamesData = File.ReadAllLines(StopNamesFilepath, Encoding.Default);
                StopCoordData = File.ReadAllLines(StopCoordFilepath, Encoding.Default);
                CorrectedStopsData = File.ReadAllLines(CorrectedStopsFilepath, Encoding.Default);
                CorrectedStopsCache = CorrectedStop.CacheCorrectedStopsData(CorrectedStopsData);
            }

            DoProcess();
        }

        public void DoProcess()
        {
            //StringBuilder log = new StringBuilder();

            log.WriteLog("Opening Database");

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<Stop> stops = new List<Stop>();

                //string[] stps = txtStopIds.Text.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                var stps = dbContext.Stops.Where(s => s.StopSource == "HAFAS").Select(s => s.GID.ToString()).ToArray();
                foreach (var stop in stps)
                {
                    var stopId = stop;

                    string hafasStopId = IBI.Shared.ScheduleHelper.UnTransformStopNo(stopId, IBI.Shared.ScheduleHelper.StopPrefix.HAFAS);
                    string coord = GetStopCoord(hafasStopId);
                    string lat = string.Empty;
                    string lon = string.Empty;


                    if (coord.Length > 0 && coord.Contains('|'))
                    {
                        string[] gps = coord.Split('|');

                        lat = gps.Length > 0 ? gps[0] : "1";
                        lon = gps.Length > 1 ? gps[1] : "1";
                    }

                    Stop s = new Stop
                    {
                        StopId = decimal.Parse(stopId),
                        Latitude = lat,
                        Longitude = lon
                    };

                    stops.Add(s);
                }
                log.WriteLog(string.Format("Prepared {0} stops", stops.Count()));


                try
                {

                    log.WriteLog("Upading Stops and ScheduleStops table");
                    foreach (var stop in stops)
                    {
                        try
                        {
                            stop.Zone = IBI.REST.Schedule.ScheduleServiceController.GetZone(stop.Latitude, stop.Longitude).Zone;

                            var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude, stop.Latitude);
                            stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);

                            log.WriteLog(string.Format("StopId: {0}, GPS: {1}", stop.StopId, sqlPoint));

                            //fix gps info in stops table
                            var st = dbContext.Stops.Where(s => s.GID == stop.StopId && s.StopSource == STOP_SOURCE).FirstOrDefault();

                            //if (st != null)
                            //{
                            //    st.StopGPS = st.OriginalGPS = stop.StopGPS;
                            //    st.LastUpdated = DateTime.Now;


                            //   // dbContext.SaveChanges();
                            //} 
                            var scheduleStops = dbContext.ScheduleStops.Where(ss => ss.StopId == stop.StopId && (ss.Zone == null || ss.Zone == ""));
                            foreach (var scheduleStop in scheduleStops)
                            {
                                //scheduleStop.StopGPS = stop.StopGPS;
                                scheduleStop.Zone = stop.Zone;
                            }

                            dbContext.SaveChanges();

                        }

                        catch (Exception exStops)
                        {
                            log.WriteLog(string.Format("Lat: {0}, Lon: {1}. Zone: {2}, ERROR: {3}", stop.Latitude, stop.Longitude, stop.Zone, exStops.Message));
                        }

                    }//);

                    log.WriteLog("Data of Stops and ScheduleStops tables updated successfully");

                    //fix gps info in journeystops table
                    var journeys = dbContext.Journeys.Where(j => j.CustomerId == CUSTOMER_ID);

                    //log.WriteLog("Looping Journeys");
                    //foreach (var journey in journeys)
                    //{
                    //    try
                    //    {
                    //        log.WriteLog(string.Format("J: {0}", journey.JourneyId));

                    //        var journeyStops = journey.JourneyStops.Where(js => (js.Zone == null || js.Zone == ""));
                    //        foreach (var journeyStop in journeyStops)
                    //        {
                    //            var stop = stops.FirstOrDefault(s => s.StopId == journeyStop.StopId);
                    //            journeyStop.StopGPS = stop.StopGPS;
                    //            journeyStop.Zone = stop.Zone;
                    //            log.WriteLog(string.Format("--> Stop: {0}, Lat: {1}, Long: {2}, Zone: {3}", journeyStop.StopId, journeyStop.StopGPS.Latitude, journeyStop.StopGPS.Longitude, journeyStop.Zone));
                    //        }

                    //        dbContext.SaveChanges();
                    //        System.Threading.Thread.Sleep(10);
                    //        //log.WriteLog("Success");

                    //    }
                    //    catch (Exception exInner)
                    //    {
                    //        log.WriteLog(exInner);
                    //    }
                    //}

                }
                catch (Exception ex)
                {
                    log.WriteLog(ex);
                }
            }

            MessageBox.Show("All GPS fixed now");

        }

        private string GetStopName(string stopNumber)
        {
            stopNumber = IBI.Shared.ScheduleHelper.UnTransformStopNo(stopNumber, IBI.Shared.ScheduleHelper.StopPrefix.HAFAS);

            //search in cache first
            var stop = StopNamesCache.FirstOrDefault(s => s.Key == stopNumber);
            string stopName = "";

            if (!String.IsNullOrEmpty(stop.Value))
            {
                stopName = stop.Value;
            }
            else
            {
                var sstop = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();

                if (sstop != null)
                {
                    //stopName = sstop.Split(' ')[2];
                    stopName = sstop.Substring(14);

                    int endIndex = stopName.IndexOf('(');
                    if (endIndex > 0)
                        stopName = stopName.Substring(0, endIndex).Trim();

                    StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
                }
                else
                {
                    stopName = "";
                }

            }

            return stopName;
        }

        private string GetStopCoord(string stopNumber)
        {
            string stopCoord = "|";
            //search in correctedStops first

            CorrectedStop correctedStop = CorrectedStopsCache.Where(s => s.StopNumber == stopNumber).FirstOrDefault();
            if (correctedStop != null)
            {
                stopCoord = correctedStop.Latitude + "|" + correctedStop.Longitude;
            }
            else
            {
                stopCoord = StopCoordData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();
                if (!String.IsNullOrEmpty(stopCoord))
                {
                    string[] arrCoord = stopCoord.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string lat = arrCoord.Length > 1 ? arrCoord[1] : "";
                    string lon = arrCoord.Length > 2 ? arrCoord[2] : "";

                    stopCoord = lat + "|" + lon;
                }
                else
                {
                    stopCoord = "|";
                }

            }




            return stopCoord;

        }

       
    }


    public class Stop
    {
        public decimal StopId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Zone { get; set; }
        public DbGeography StopGPS { get; set; }
    }

    public class CorrectedStop
    {

        public string StopNumber { get; set; }
        public int MainStop { get; set; }
        public int Radius { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int LaneManagement { get; set; }
        public int CorrespondanceStop { get; set; }
        public string StopName { get; set; }

        public static List<CorrectedStop> CacheCorrectedStopsData(string[] lines)
        {

            List<CorrectedStop> data = new List<CorrectedStop>();

            foreach (string line in lines)
            {
                CorrectedStop stopData = new CorrectedStop();
                stopData.StopNumber = line.Substring(0, 9).Trim();
                stopData.MainStop = int.Parse(line.Substring(11, 1).Trim());
                stopData.Radius = int.Parse(line.Substring(14, 4).Trim());

                //if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "da-DK")
                //{

                //    stopData.Longitude = decimal.Parse(line.Substring(20, 9).Trim().Replace('.', ','));
                //    stopData.Latitude = decimal.Parse(line.Substring(31, 9).Trim().Replace('.', ','));
                //}
                //else 
                {
                    stopData.Longitude = line.Substring(20, 9).Trim();
                    stopData.Latitude = line.Substring(31, 9).Trim();
                }

                stopData.LaneManagement = int.Parse(line.Substring(42, 1).Trim());
                stopData.CorrespondanceStop = int.Parse(line.Substring(45, 1).Trim());
                stopData.StopName = line.Substring(50).Trim();

                data.Add(stopData);

                //Console.WriteLine("Corrected Long:" + stopData.Longitude);

                ////if(stopData.StopName == "Skanderborg St.")
                //{
                //    Console.WriteLine(string.Format("StopName: {0}, Latitude: {1}, Longitude: {2}", stopData.StopName,line.Substring(31, 9).Trim(), line.Substring(20, 9).Trim()));
                //}
            }

            return data;
        }
    }
}
