﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixZoneInfoForSchedulesAndJourneys
{
    public class Log
    {
        public StringBuilder sb { get; set; }
        private string LogFileName { get; set; }

        public Log(string logFileName = "" , StringBuilder sBuilder = null) {
            this.LogFileName = logFileName;
            this.sb = sBuilder;
        }

        
        public void WriteLog(string message)
        {
            Console.WriteLine(message);
            Write(LogFileName, message);
        }


        public void WriteLog(Exception ex)
        {
            Console.WriteLine(string.Format("ERROR : {0}", ex.Message));
            Write(LogFileName, string.Format("ERROR : {0} - {1}", ex.Message, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
        }


        public void AppendLog(string message)
        {
            Console.WriteLine(message);
            sb.AppendLine(message);            
        }


        public void AppendLog(Exception ex)
        {
            Console.WriteLine(string.Format("ERROR : {0}", ex.Message));
            sb.AppendLine(string.Format("ERROR : {0} - {1}", ex.Message, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));            
        }

        public void Write(string logName, string message) {
            //if (AppSettings.LogAllMessages())
            //{
                mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI_" + logName, message);
                //AppUtility.WriteLine(sb.ToString());             
            //}

            //sb.Clear();
        }


    }
}
