﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBI.VehicleScheduleImporter;
using IBI.Shared.VehicleSchedules.Interfaces;
using System.Configuration;

namespace IBI.VehicleScheduleImporter.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            IVehicleScheduleImporter importer = new VehicleScheduleImporter();
            importer.Url = "http://app.ekspress.norgesbuss.no/sas_flybussen_ruteplan_mermaid.json";
            importer.CustomerId = int.Parse(ConfigurationSettings.AppSettings["DefaultCustomerId"]);
            importer.ImportVehicleSchedules();
            System.Console.WriteLine("Press any key");
            System.Console.ReadLine();
        }
    }
}
