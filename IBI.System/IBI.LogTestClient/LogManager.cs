﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.SqlServerCe;
using System.Timers;
using System.Data;
using System.Configuration;
using System.Windows.Forms;

  
    public class LogManager
    {
        #region Members

        private System.Timers.Timer TmrProcessLogs
        {
            get;
            set;
        }

        private System.Timers.Timer tmrBulkCopyLog
        {
            get;
            set;
        }

        private bool Continuous
        {
            get;
            set;
        }
        #endregion

        public LogManager(bool continuous)
        {
            this.Continuous=continuous;

            AppUtility.Log2File("LogManager", "Log Manager's new instance started");

            //this.StartProcessTimer();
             

            this.tmrBulkCopyLog = new System.Timers.Timer(TimeSpan.FromSeconds(2).TotalMilliseconds);
            this.tmrBulkCopyLog.Elapsed += new System.Timers.ElapsedEventHandler(tmrBulkCopyLog_Elapsed);
            this.tmrBulkCopyLog.Enabled = true;

            AppUtility.Log2File("BulkCopy", "BulkCopy timer Initializes");
        }
         
                
        private void tmrBulkCopyLog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!AppSettings.GetLogProcessingEnabled())
                return; //stop bulk copying (skipped)

             
            this.tmrBulkCopyLog.Enabled = false;
            AppUtility.Log2File("BulkCopy", "BulkCopy Timer Stops + BulkCopying Starts");


            SqlCeConnection con;
            try
            {
                //BULK COPY OPERATION HERE

                AppUtility.Log2File("BulkCopy", "BulkCopy Looping All SQLCE Files in Data Directory");
                    foreach(string fileName in Directory.GetFiles(AppSettings.GetSQLCEDirectory())) 
                    {
                        FileInfo fl = new FileInfo(fileName);
                        if (fl.Extension.ToLower() != ".sdf")
                            continue;

                        if (((fl.Length) / 1024 / 1024) < int.Parse(SQLCompactManager.MaxCompactDBSize))
                            continue;

                        AppUtility.Log2File("BulkCopy", "BulkCopy --> " + fl.FullName);     

                        con = SQLCompactManager.GetConnection(fl.FullName);

                        con.Open();
                        AppUtility.Log2File("BulkCopy", "  DB Connection Opens");

                        //SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM BusEventLog; SELECT * FROM BusProgressLog; SELECT * FROM BusLocationLog;", con);
                        SqlCeCommand cmdEvnt = new SqlCeCommand(@"SELECT [Timestamp]
                                                                          ,[BusNumber]
                                                                          ,[ClientType]
                                                                          ,[IsMaster]
                                                                          ,[IsOverwritten]
                                                                          ,[Source]
                                                                          ,[Line]
                                                                          ,[From]
                                                                          ,[Destination]
                                                                          ,[JourneyNumber]
                                                                          ,[StopNumber]
                                                                          ,[Zone]
                                                                          ,[GPS1_Temp]
                                                                          ,[GPS2_Temp]
                                                                          ,[EventSource]
                                                                          ,[EventId]
                                                                          ,[EventData]
                                                                          ,[Result]
                                                                          ,[ExtraData]
                                                                      FROM BusEventLog ", con);
                        SqlCeCommand cmdProg = new SqlCeCommand(@"SELECT [Timestamp]
                                                                        ,[BusNumber]
                                                                        ,[ClientType]
                                                                        ,[IsMaster]
                                                                        ,[IsOnline]
                                                                        ,[Source]
                                                                        ,[IsOverwritten]
                                                                        ,[JourneyNumber]
                                                                        ,[Line]
                                                                        ,[From]
                                                                        ,[Destination]
                                                                        ,[StopNumber]
                                                                        ,[Zone]
                                                                        ,[Latitude]
                                                                        ,[Longitude]
                                                                        ,[LogTrigger]
                                                                        ,[Data]
                                                                        ,[Result]
                                                                        ,[SignData]
                                                                    FROM [BusProgressLog]", con);
                        SqlCeCommand cmdLoc = new SqlCeCommand(@"SELECT [Timestamp]
                                                                          ,[BusNumber]
                                                                          ,[ClientType]
                                                                          ,[GPS1_Temp]
                                                                          ,[GPS2_Temp]
                                                                          ,[Speed]
                                                                          ,[Player_PID]
                                                                          ,[SystemStatus]
                                                                      FROM  [BusLocationLog]", con);

                        DataSet ds = new DataSet();
                        SqlCeDataAdapter da = new SqlCeDataAdapter();
                        
                        da.SelectCommand = cmdEvnt;
                        da.Fill(ds, "BusEventLog");

                        da.SelectCommand = cmdProg;
                        da.Fill(ds, "BusProgressLog");

                        da.SelectCommand = cmdLoc;
                        da.Fill(ds, "BusLocationLog");
                        
                        con.Close();
                        AppUtility.Log2File("BulkCopy", "  DB Connection Closes");

                        AppUtility.Log2File("BulkCopy", "BulkCopy Started");
                        bool bulkCopySuccess = BulkCopy(ds, ConfigurationManager.ConnectionStrings["IBIDatabaseConnection"].ToString());
                        AppUtility.Log2File("BulkCopy", "BulkCopy Completed -- > Status: " + bulkCopySuccess);
                        
                        if (bulkCopySuccess)
                        {
                            //dont delete file
                            //SqlCompact.SQLCompactManager.RemoveDatabase(fl.FullName);
                            AppUtility.Log2File("BulkCopy", "BulkCopy SQLCE File removed (not physically removing in test app) -- > " + fl.FullName);
                            
                        }
                            
                    }
                                
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("BulkCopy", ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                
                con = null;

                if(Continuous)
                {

                    this.tmrBulkCopyLog.Interval = DateTime.Now.AddSeconds(5).Millisecond; // (DateTime.Now.AddMinutes(2) - DateTime.Now).TotalMilliseconds; // AppUtility.DifferenceToMidnight().TotalMilliseconds;
                    this.tmrBulkCopyLog.Enabled = true;

                    AppUtility.Log2File("BulkCopy", "BulkCopy Timer started again. Next tick is:" + DateTime.Now.AddMinutes(2));
    
                }
                else
                {
                   this.tmrBulkCopyLog.Enabled=false;
                   AppUtility.Log2File("BulkCopy", "BulkCopy Timer Stopped.");

                   MessageBox.Show("SDF File processed sucessfully");
                }

            }
        }
         
 

        #region Private helpers

        private List<String> ExtractFiles(FileInfo file)
        {
            String targetPath = file.Directory.FullName;

            List<String> fileList = new List<String>();

            FastZip zipEngine = new FastZip();
            zipEngine.ExtractZip(file.FullName, targetPath, "");

            AppUtility.Log2File("LogManager", "  Extracting files");
            foreach (String logFilePath in Directory.GetFiles(targetPath))
            {
                if (String.Compare(Path.GetFileName(file.FullName), Path.GetFileName(logFilePath), true) != 0)
                {
                    fileList.Add(logFilePath);
                    AppUtility.Log2File("LogManager", "    Extracte file: " + logFilePath);
                }
                
            }

            return fileList;
        }


        public static bool BulkCopy(DataSet dataSet, string destConnectionString)
        {
            
            try
            {

                using (SqlConnection destConn = new SqlConnection(destConnectionString))
                {

                    destConn.Open();
                    AppUtility.Log2File("BulkCopy", "Bulk Copy --> Open IBI Database Coonection");

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConn, SqlBulkCopyOptions.FireTriggers, null))
                    {
                        bulkCopy.BulkCopyTimeout = AppSettings.SQLBulkCopyTimeout();
                          
                        bulkCopy.ColumnMappings.Clear();
                        //BUS EVENT LOG COPYING
                        bulkCopy.DestinationTableName = "BusEventLog";
                        
                        bulkCopy.ColumnMappings.Add("Timestamp", "Timestamp");
                        bulkCopy.ColumnMappings.Add("BusNumber", "BusNumber");
                        bulkCopy.ColumnMappings.Add("ClientType", "ClientType");
                        bulkCopy.ColumnMappings.Add("IsMaster", "IsMaster");
                        bulkCopy.ColumnMappings.Add("IsOverwritten", "IsOverwritten");
                        bulkCopy.ColumnMappings.Add("Source", "Source");
                        bulkCopy.ColumnMappings.Add("Line", "Line");
                        bulkCopy.ColumnMappings.Add("From", "From");
                        bulkCopy.ColumnMappings.Add("Destination", "Destination");
                        bulkCopy.ColumnMappings.Add("JourneyNumber", "JourneyNumber");
                        bulkCopy.ColumnMappings.Add("StopNumber", "StopNumber");
                        bulkCopy.ColumnMappings.Add("Zone", "Zone");
                        bulkCopy.ColumnMappings.Add("GPS1_Temp", "GPS1_Temp");
                        bulkCopy.ColumnMappings.Add("GPS2_Temp", "GPS2_Temp");
                        bulkCopy.ColumnMappings.Add("EventSource", "EventSource");
                        bulkCopy.ColumnMappings.Add("EventId", "EventId");
                        bulkCopy.ColumnMappings.Add("EventData", "EventData");
                        bulkCopy.ColumnMappings.Add("Result", "Result");
                        //bulkCopy.ColumnMappings.Add("ExtraData ", "ExtraData");

                        AppUtility.Log2File("BulkCopy", " --> Start writing BusEventLog");
                        bulkCopy.WriteToServer(dataSet.Tables["BusEventLog"], DataRowState.Unchanged);
                        AppUtility.Log2File("BulkCopy", " --> End writing BusEventLog");

                         
                        bulkCopy.ColumnMappings.Clear();
                        //BusLocationLog COPYING
                        bulkCopy.DestinationTableName = "dbo.BusLocationLog";

                        bulkCopy.ColumnMappings.Add("Timestamp", "Timestamp");
                        bulkCopy.ColumnMappings.Add("BusNumber", "BusNumber");
                        bulkCopy.ColumnMappings.Add("ClientType", "ClientType");
                        bulkCopy.ColumnMappings.Add("GPS1_Temp", "GPS1_Temp");
                        bulkCopy.ColumnMappings.Add("GPS2_Temp", "GPS2_Temp");
                        bulkCopy.ColumnMappings.Add("Speed", "Speed");
                        bulkCopy.ColumnMappings.Add("Player_PID", "Player_PID");
                        bulkCopy.ColumnMappings.Add("SystemStatus", "SystemStatus");

                        AppUtility.Log2File("BulkCopy", " --> Start writing BusLocationLog");
                        bulkCopy.WriteToServer(dataSet.Tables["BusLocationLog"], DataRowState.Unchanged);
                        AppUtility.Log2File("BulkCopy", " --> End writing BusLocationLog");

                        bulkCopy.ColumnMappings.Clear();
                        //BusProgressLog COPYING
                        bulkCopy.DestinationTableName = "dbo.BusProgressLog";

                        bulkCopy.ColumnMappings.Add("Timestamp", "Timestamp");
                        bulkCopy.ColumnMappings.Add("BusNumber", "BusNumber");
                        bulkCopy.ColumnMappings.Add("ClientType", "ClientType");
                        bulkCopy.ColumnMappings.Add("IsMaster", "IsMaster");
                        bulkCopy.ColumnMappings.Add("IsOnline", "IsOnline");
                        bulkCopy.ColumnMappings.Add("Source", "Source");
                        bulkCopy.ColumnMappings.Add("IsOverwritten", "IsOverwritten");
                        bulkCopy.ColumnMappings.Add("JourneyNumber", "JourneyNumber");
                        bulkCopy.ColumnMappings.Add("Line", "Line");
                        bulkCopy.ColumnMappings.Add("From", "From");
                        bulkCopy.ColumnMappings.Add("Destination", "Destination");
                        bulkCopy.ColumnMappings.Add("StopNumber", "StopNumber");
                        bulkCopy.ColumnMappings.Add("Zone", "Zone");
                        bulkCopy.ColumnMappings.Add("Latitude", "Latitude");
                        bulkCopy.ColumnMappings.Add("Longitude", "Longitude");
                        bulkCopy.ColumnMappings.Add("LogTrigger", "LogTrigger");
                        bulkCopy.ColumnMappings.Add("Data", "Data");
                        bulkCopy.ColumnMappings.Add("Result", "Result");
                        bulkCopy.ColumnMappings.Add("SignData", "SignData");


                        AppUtility.Log2File("BulkCopy", " --> Start writing BusProgressLog");
                        bulkCopy.WriteToServer(dataSet.Tables["BusProgressLog"], DataRowState.Unchanged);
                        AppUtility.Log2File("BulkCopy", " --> End writing BusProgressLog");

                    }
                    //    if (append)
                    //        AppendBulkLoaded(destConn, TIMEOUT); // 45 minutes
                }
            }
            catch(Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("BulkCopy", " BulkCopying CRASHES: --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);
                return false;
            }

            return true;
        }

        static void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            //throw new NotImplementedException();
            //e.RowsCopied
        }
 

        #endregion
    } 
