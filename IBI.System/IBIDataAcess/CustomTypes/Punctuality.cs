﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;




namespace IBI.DataAccess.CustomTypes
{
    [DataContract(Namespace = "")]
    public class Punctuality
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string BusNumber { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public String Line { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime JourneyDate { get; set; }

        [DataMember]
        public int JourneyNumber { get; set; }

        [DataMember]
        public String StartPoint { get; set; }

        [DataMember]
        public String Destination { get; set; }

        [DataMember]
        public String StopName{ get; set; }


        //[DataMember(EmitDefaultValue = false)]
        //public DateTime PlannedArrivalTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime PlannedDepartureTime { get; set; }


        [DataMember(EmitDefaultValue=false)]        
        public DateTime? ActualArrival { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public DateTime? ActualDeparture { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PlanDifference { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Status { get; set; }
         
    }
}
