﻿using IBI.DataAccess.DBModels;
using IBI.Shared.Models.Journey;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IBI.Journey.PredictionImporter
{
    public class HafasImporter
    {
        private static List<IBI.DataAccess.DBModels.Stop> _stops;
        public static List<IBI.DataAccess.DBModels.Stop> Stops
        {
            get
            {
                if (_stops == null)
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        _stops = (from s in dbContext.Stops
                                  //where s.GID.ToString().StartsWith("91")
                                  where s.StopSource.ToUpper() == "HAFAS"
                                  select s).ToList();
                    }
                }

                return _stops;
            }
        }

        private static List<IBI.DataAccess.DBModels.Schedule> _schedules;
        public static List<IBI.DataAccess.DBModels.Schedule> Schedules
        {
            get
            {
                if (_schedules == null)
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        int defaultCustomerId = int.Parse(ConfigurationSettings.AppSettings["DefaultCustomerId"]);
                        _schedules = (from s in dbContext.Schedules
                                      where s.CustomerId == defaultCustomerId
                                      select s).ToList();
                    }
                }

                return _schedules;
            }
        }


        public static bool HistoryMode
        {
            get { return bool.Parse(ConfigurationSettings.AppSettings["HistoryMode"].ToString()); }
        }

        public static void ImportPredictions(string journeyXml)
        {
            List<JourneyPrediction> predictions = ParseJourneyEstimate(journeyXml);

            int journeyId = predictions.Count() > 0 ? predictions[0].JourneyId : 0;

            if (journeyId <= 0)
                return;

            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    if (!HistoryMode)
                    {
                        /*
                         select JourneyId, EstimatedStopSequence, MAX(EstimationTime) from JourneyStopEstimations
                            WHERE JourneyId = 13096
                            group by JourneyId, EstimatedStopSequence
                         */


                        var jEstimations = dbContext.JourneyStopEstimations.Where(j => j.JourneyId == journeyId);

                        //var looper = jEstimations.Select(j => new
                        //{
                        //     j.EstimatedStopSequence,
                        //     j.EstimatedArrivalTime,
                        //     j.EstimatedDepartureTime,
                        //     j.EstimationTime
                        //}).Max(m=>m.EstimationTime).GroupBy(j => j.EstimatedStopSequence);

                        var looper = from e in jEstimations
                                     group e by e.EstimatedStopSequence into g
                                     select g.OrderByDescending(e => e.EstimationTime).FirstOrDefault() into r
                                     select r;
                        //{
                        //    SqlId = r.SqlId,
                        //    JourneyId = r.JourneyId,
                        //    EstimatedStopSequence = r.EstimatedStopSequence,
                        //    EstimatedTime = r.EstimationTime,
                        //    EstimatedArrivalTime = r.EstimatedArrivalTime,
                        //    EstimatedDepartureTime = r.EstimatedDepartureTime
                        //};

                        int counter = 0;
                        foreach (var l in looper.OrderBy(j => j.EstimatedStopSequence))
                        {
                            var p = predictions.Where(pr => pr.EstimatedStopSequence == l.EstimatedStopSequence).FirstOrDefault();

                            if (p != null && (p.EstimatedDepartureTime == l.EstimatedDepartureTime && p.EstimatedArrivalTime == l.EstimatedArrivalTime))
                            {
                                predictions.Remove(p);
                            }


                            counter++;
                        }

                    }

                    foreach (JourneyPrediction jp in predictions)
                    {
                        DataAccess.DBModels.JourneyStopEstimation jEstimate = new JourneyStopEstimation
                        {
                            EstimatedArrivalTime = jp.EstimatedArrivalTime,
                            EstimatedAtStopSequence = jp.EstimatedAtStopSequence,
                            EstimatedDepartureTime = jp.EstimatedDepartureTime,
                            EstimatedStopSequence = jp.EstimatedStopSequence,
                            EstimationSource = jp.EstimationSource,
                            EstimationTime = jp.EstimationTime,
                            JourneyId = jp.JourneyId
                        };

                        dbContext.JourneyStopEstimations.Add(jEstimate);
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //private static IEnumerable<string> EnumeratePropertyDifferences<T>(this T obj1, T obj2)
        //{
        //    PropertyInfo[] properties = typeof(T).GetProperties();
        //    List<string> changes = new List<string>();

        //    foreach (PropertyInfo pi in properties)
        //    {
        //        object value1 = typeof(T).GetProperty(pi.Name).GetValue(obj1, null);
        //        object value2 = typeof(T).GetProperty(pi.Name).GetValue(obj2, null);

        //        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
        //        {
        //            changes.Add(string.Format("Property {0} changed from {1} to {2}", pi.Name, value1, value2));
        //        }
        //    }
        //    return changes;
        //}

        private static List<IBI.Shared.Models.Journey.JourneyPrediction> ParseJourneyEstimate(string journeyXml)
        {
            List<IBI.Shared.Models.Journey.JourneyPrediction> jList = new List<IBI.Shared.Models.Journey.JourneyPrediction>();

            if (string.IsNullOrEmpty(journeyXml))
                return jList;


            //tripData = tripData.Replace("<S:Body>", "<Body>")
            string fileText = journeyXml; //File.ReadAllText(filePath, System.Text.Encoding.UTF8);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            MemoryStream memStream = new MemoryStream(encoding.GetBytes(fileText));

            XDocument xDoc = XDocument.Load(memStream);


            XNamespace ns = "urn:hrx";

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                foreach (var tripInfo in xDoc.Descendants(ns + "RealtimeInfo"))
                {


                    string timestamp = tripInfo.Attribute("timestamp").Value;
                    string timeZone = timestamp.Substring(timestamp.LastIndexOf('+'));
                    string timeFormat = string.Format("yyyy-MM-ddTHH:mm:ss.fff{0}", timeZone);

                    DateTime estimationTime = DateTime.ParseExact(timestamp, timeFormat, CultureInfo.InvariantCulture);


                    string operatingDay = xDoc.Descendants(ns + "OperatingDay").FirstOrDefault().Value;
                    string line = xDoc.Descendants(ns + "TripName").FirstOrDefault().Value;
                    string[] uniqueId = xDoc.Descendants(ns + "UniqueID").FirstOrDefault().Value.Split('#');

                    string sender = uniqueId[0];

                    string firstStopId = xDoc.Descendants(ns + "StartStopID").FirstOrDefault().Value;
                    firstStopId = IBI.Shared.ScheduleHelper.TransformStopNo(firstStopId, Shared.ScheduleHelper.StopPrefix.HACON);
                    decimal fStopId = decimal.Parse(firstStopId);
                    var firstStop = Stops.Where(s => s.GID == fStopId).FirstOrDefault();
                    //var firstStop = dbContext.Stops.Where(s => s.GID == fStopId).FirstOrDefault();

                    string lastStopId = xDoc.Descendants(ns + "EndStopID").FirstOrDefault().Value;
                    lastStopId = IBI.Shared.ScheduleHelper.TransformStopNo(lastStopId, Shared.ScheduleHelper.StopPrefix.HACON);
                    decimal lStopId = decimal.Parse(lastStopId);
                    var lastStop = Stops.Where(s => s.GID == lStopId).FirstOrDefault();
                    //var lastStop = dbContext.Stops.Where(s => s.GID == lStopId).FirstOrDefault();


                    if (firstStop == null || lastStop == null)
                        return jList;

                    string fromName = firstStop.StopName;
                    string destination = lastStop.StopName;

                    //timeFormat = string.Format("yyyy-MMddHHmm");
                    DateTime plannedStartTime = DateTime.ParseExact(xDoc.Descendants(ns + "StartTime").FirstOrDefault().Value, timeFormat, CultureInfo.InvariantCulture);
                    DateTime plannedEndTime = DateTime.ParseExact(xDoc.Descendants(ns + "EndTime").FirstOrDefault().Value, timeFormat, CultureInfo.InvariantCulture);

                    var schedule = Schedules.Where(s => s.Line == line && s.FromName == fromName && s.Destination == destination).FirstOrDefault();
                    //var schedule = dbContext.Schedules.Where(s => s.Line == line && s.FromName == fromName && s.Destination == destination).FirstOrDefault();

                    if (schedule == null)
                        return jList;


                    var journeys = dbContext.Journeys.Where(jr => jr.JourneyStateId == 1);

                    int journeyId = 0;

                    foreach (var journey in journeys)
                    {
                        if (journey.ScheduleId == schedule.ScheduleId && journey.PlannedStartTime == plannedStartTime)
                        {
                            journeyId = journey.JourneyId;
                            break;
                        }
                    }

                    if (journeyId <= 0)
                    {
                        if (bool.Parse(ConfigurationSettings.AppSettings["AutoAddActiveJourneys"]))
                        {

                            using (JourneyService.JourneyServiceClient jClient = new JourneyService.JourneyServiceClient())
                            {
                                string defaultCustomerBus = ConfigurationSettings.AppSettings["DefaultCustomerBus"].ToString();
                                string jXml = jClient.GetActiveJourney(schedule.ScheduleId, schedule.CustomerId, defaultCustomerBus);
                                MemoryStream jStream = new MemoryStream(encoding.GetBytes(jXml));
                                XDocument jXDoc = XDocument.Load(jStream);

                                journeyId = int.Parse(jXDoc.Descendants("JourneyNumber").FirstOrDefault().Value);
                                var journey = dbContext.Journeys.Where(j => j.JourneyId == journeyId).FirstOrDefault();
                                journey.PlannedStartTime = plannedStartTime;
                                journey.PlannedEndTime = plannedEndTime;

                                dbContext.SaveChanges();

                            }
                        }
                        else
                        {
                            return jList;
                        }

                    }



                    timeFormat = string.Format("yyyy-MM-ddTHH:mm:ss.fff{0}", timeZone);

                    int stopSequence = 1;
                    foreach (var stopNode in xDoc.Descendants(ns + "RealStop"))
                    {
                        IBI.Shared.Models.Journey.JourneyPrediction j = new Shared.Models.Journey.JourneyPrediction();

                        j.EstimationTime = estimationTime;
                        j.EstimationSource = "HACON";
                        j.EstimatedAtStopSequence = null;
                        j.JourneyId = journeyId;


                        var stopId = IBI.Shared.ScheduleHelper.TransformStopNo(stopNode.Descendants(ns + "StopID").FirstOrDefault().Value, Shared.ScheduleHelper.StopPrefix.HACON);

                        var departurePrediction = stopNode.Descendants(ns + "RealDeparturePrediction").FirstOrDefault();
                        var arrivalPrediction = stopNode.Descendants(ns + "RealArrivalPrediction").FirstOrDefault();

                        DateTime? departureTime = null;
                        DateTime? arrivalTime = null;

                        if (departurePrediction != null)
                            departureTime = DateTime.ParseExact(departurePrediction.Value, timeFormat, CultureInfo.InvariantCulture);

                        if (arrivalPrediction != null)
                            arrivalTime = DateTime.ParseExact(arrivalPrediction.Value, timeFormat, CultureInfo.InvariantCulture);


                        j.EstimatedDepartureTime = departureTime;
                        j.EstimatedArrivalTime = arrivalTime;
                        j.EstimatedStopSequence = stopSequence++;

                        jList.Add(j);
                    }

                }//end foreach
            }//end ibiDataModel

            return jList;
        }

        public static void AddEstimateToQueue(string journeyData)
        {
            QueueManager.QueueEstimate(journeyData);

        }
    }
}
