﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Journey.PredictionImporter
{
    public enum QueueStatus
    {
        EMPTY ,
        NOTEMPTY
    }
    public class QueueManager : IDisposable
    {
    #region Members

        private const string QueueName = @".\Private$\HRXJourneyEstimations";

        private System.Timers.Timer tmrEstimation
        {
            get;
            set;
        }

        private int QueueProcessIntervalInSeconds
        {
            get
            {
            return int.Parse(ConfigurationSettings.AppSettings["QueueProcessIntervalInSeconds"]);
            }
                
        }

        private StringBuilder log = new StringBuilder();

        private static QueueStatus _status;
        public static QueueStatus Status
        {
            get
            {
                if(_status != QueueStatus.EMPTY && _status != QueueStatus.NOTEMPTY)
                {
                    return QueueStatus.EMPTY;
                }

                return _status;
            }
            set
            {
                _status = value;
            }
                
        }

        #endregion



        #region Constructors

        public QueueManager()
        {
            log.AppendLine(DateTime.Now.ToString() + ": QueueManager new instance started");

              
            this.tmrEstimation = new System.Timers.Timer();
            this.tmrEstimation.Interval = TimeSpan.FromSeconds(this.QueueProcessIntervalInSeconds).TotalMilliseconds;
            this.tmrEstimation.Elapsed += new System.Timers.ElapsedEventHandler(tmrEstimation_Elapsed);
            this.tmrEstimation.Enabled = true;

            log.AppendLine(DateTime.Now.ToString() + ": Queue timer Initializes, timer is " + this.tmrEstimation.Enabled);

        }

        #endregion

        #region Events


        private void tmrEstimation_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            if (log == null)
                log = new StringBuilder();


            this.tmrEstimation.Enabled = false;
            log.AppendLine(DateTime.Now.ToString() + ": Queue Timer Stops + Process Starts");


            try
            {
               ProcessQueue();
            }
            catch (Exception ex)
            {
                log.AppendLine(DateTime.Now.ToString() + ": Queue Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace);
                //throw ex;
            }
            finally
            {
                log.AppendLine(DateTime.Now.ToString() + ": Queue Timer initializes again");

                AppUtility.Log2File("Queue", log.ToString(), true);

                log.Clear();
                log = null;

                if(Status == QueueStatus.EMPTY)
                {
                    Dispose();                        
                }
                else
                {
                    this.StartQueueTimer();    
                }
                
            }

        }


        public void Dispose()
        {
            this.tmrEstimation.Enabled = false;
            this.tmrEstimation.Dispose(); 
 
        }

        #endregion

        #region Methods

          internal static void QueueEstimate(string journeyData)
        {
            MessageQueue msgQueue = null;

            if(!MessageQueue.Exists(QueueName))
            {
                msgQueue = MessageQueue.Create(QueueName);  
            }
            else
            {
                msgQueue = new MessageQueue(QueueName);                              
            }

            msgQueue.Send(journeyData);

            if (QueueManager.Status == QueueStatus.EMPTY)
            {
                QueueManager qm = new QueueManager();
            }
        }

        private void StartQueueTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.tmrEstimation == null)
            {
                this.tmrEstimation = new System.Timers.Timer();
                this.tmrEstimation.Elapsed += new System.Timers.ElapsedEventHandler(tmrEstimation_Elapsed);
            }

            this.tmrEstimation.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(this.QueueProcessIntervalInSeconds); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            this.tmrEstimation.Interval = TimeSpan.FromSeconds(this.QueueProcessIntervalInSeconds).TotalMilliseconds; ;
            this.tmrEstimation.Enabled = true;

            log.AppendLine(Environment.NewLine + "#########################################################" + Environment.NewLine + Environment.NewLine + "Queue Timer initializes");

        }


        private string ProcessQueue()
        {
            log.Append(Environment.NewLine + "#########################################################" + Environment.NewLine + Environment.NewLine);
            String result = string.Empty;

            MessageQueue mq = null;

            if (MessageQueue.Exists(QueueName))
            {
                mq = new System.Messaging.MessageQueue(QueueName);
            }
                
            else
                return string.Empty;

            string j = string.Empty;

            try
            {
                Message msg = mq.Receive(new TimeSpan(0, 0, 3));
                msg.Formatter = new XmlMessageFormatter(
                  new String[] { "System.String,mscorlib" });
                j = msg.Body.ToString();
            }
            catch
            {
                j = string.Empty;
            }
             
            
            Status = j == string.Empty ? QueueStatus.EMPTY : QueueStatus.NOTEMPTY;

            if(j != string.Empty)
            {
                Importer.ImportPredictions(j);   
            }

            return "SUCCESS";
        }
         

        #endregion




      
    }

     


}
