﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBI.SignQueueProcessor;

namespace IBI.SignQueueProcessor.TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IBI.SignQueueProcessor.SignQueueController server = new IBI.SignQueueProcessor.SignQueueController();
                System.Console.WriteLine("IBI Sign Queue Controller Started");
                System.Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
