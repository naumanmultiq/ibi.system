﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Diagnostics;
using IBI.Shared.Diagnostics;

namespace IBI.LogProcessor
{
    class AppUtility
    {
        public static IFormatProvider GetDateFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd";
            dateFormat.LongDatePattern = "yyyy-MM-dd";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd";
            dateFormat.DateSeparator = "-";

            return dateFormat;
        }

        public static IFormatProvider GetDateTimeFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.LongDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.DateSeparator = "-";
            dateFormat.TimeSeparator = ":";

            return dateFormat;
        }

        public static string SafeSqlLiteral(string inputSql)
        {
            if (!String.IsNullOrEmpty(inputSql))
                return inputSql.Replace("'", "''");
            else
                return "";
        }

        public static string ConvertToSqlPoint(String gps)
        {
            String point = "NULL";

            if (!String.IsNullOrEmpty(gps.Trim()))
            {
                String[] gpsSplit = gps.Split(',');

                if (gpsSplit.Length == 2)
                {
                    String longitude = gpsSplit[1].Trim();
                    String latitude = gpsSplit[0].Trim();

                    if (!String.IsNullOrEmpty(longitude) && !String.IsNullOrEmpty(latitude))
                        point = "geography::STGeomFromText('POINT(" + latitude + " " + longitude + ")', 4326)";
                }
            }

            return point;
        }


        public static string ValidateGPS(String gps)
        {
            string point = "1.1,1.1";


            if (!String.IsNullOrEmpty(gps.Trim()))
            {
                String[] gpsSplit = gps.Split(',');

                if (gpsSplit.Length == 2)
                {
                    String longitude = gpsSplit[1].Trim();
                    String latitude = gpsSplit[0].Trim();

                    try
                    {

                        if (!String.IsNullOrEmpty(longitude) && !String.IsNullOrEmpty(latitude))
                        {
                            IFormatProvider provider = System.Globalization.CultureInfo.InvariantCulture;

                            double lon;
                            double lat;

                            bool res1 = double.TryParse(longitude, NumberStyles.AllowDecimalPoint, provider, out lon);
                            bool res2 = double.TryParse(latitude, NumberStyles.AllowDecimalPoint, provider, out lat);
                             

                            if ((res1 && res2) && lon >= -90 && lon <= 90 && lat >= -180 && lat <= 180) //valid ranges of Lon & Lat
                            {
                                //point = longitude + "," + latitude; //Microsoft.SqlServer.Types.SqlGeography.Point(lat, lon, 4326);
                                point = latitude + "," + longitude; 
                            }
                            else
                            {
                                AppUtility.Log2File("LogManager", " --> Warning: Invalid GPS info found. lon = " + lon.ToString() + ", lat = " + lat.ToString());
                            }

                        }

                    }
                    catch
                    {
                        AppUtility.Log2File("LogManager", " --> Error: Invalid GPS info found. lon = " + longitude + ", lat = " + latitude);
                    }

                }
            }

            return point;
        }
        
        //public static Microsoft.SqlServer.Types.SqlGeography ConvertToSqlGeographyPoint(String gps)
        //{
        //    Microsoft.SqlServer.Types.SqlGeography point = null;

             
        //    if (!String.IsNullOrEmpty(gps.Trim()))
        //    {
        //        String[] gpsSplit = gps.Split(',');

        //        if (gpsSplit.Length == 2)
        //        {
        //            String longitude = gpsSplit[1].Trim();
        //            String latitude = gpsSplit[0].Trim();
                        
        //            try
        //            {
             
        //                if (!String.IsNullOrEmpty(longitude) && !String.IsNullOrEmpty(latitude))
        //                {
        //                    IFormatProvider provider = System.Globalization.CultureInfo.InvariantCulture;

        //                    double lon; 
        //                    double lat;

        //                    bool res1 = double.TryParse(longitude, NumberStyles.AllowDecimalPoint, provider, out lon);
        //                    bool res2 = double.TryParse(latitude, NumberStyles.AllowDecimalPoint, provider, out lat);

        //                    if (lon >= -180 && lon <= 180 && lat >= -90 && lat <= 90) //valid ranges of Lon & Lat
        //                    {
        //                        point = Microsoft.SqlServer.Types.SqlGeography.Point(lat, lon, 4326);
        //                    }
        //                    else
        //                    {
        //                        AppUtility.Log2File("BulkCopy", " --> Warning: Invalid GPS info found. lon = " + lon.ToString() + ", lat = " + lat.ToString());
        //                    }
                        
        //                }
                        
        //            }
        //            catch
        //            {
        //                AppUtility.Log2File("BulkCopy", " --> Error: Invalid GPS info found. lon = "+ longitude + ", lat = " + latitude);
        //            }
                         
        //        }
        //    }

        //    return point;
        //}

        public static void WriteLine(String line)
        {
            //Trace.TraceInformation(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss> ") + line);
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI_SERVER, line);
        }

        public static void WriteError(String line)
        {
            //Trace.TraceError(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss> ") + line);
            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, line);
        }

        public static void WriteError(Exception ex)
        {
            String message = "";

            Exception currentException = ex;

            while (currentException != null)
            {
                message += string.Format("{0}{1}", currentException.Message, Environment.NewLine);
                message += currentException.StackTrace + Environment.NewLine;
                message += "--" + Environment.NewLine;

                currentException = currentException.InnerException;
            }

            //Trace.TraceError(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss> ") + message);
            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, ex);
        }

        public static TimeSpan DifferenceToMidnight()
        {
            DateTime midNight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            return (midNight - DateTime.Now);
        }
        
        public static void Log2File(string logName, string message, bool doWrite = false)
        {
            if(doWrite || AppSettings.LogAllMessages())
                mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI_Server_" + logName, message);
        }
    }
}
