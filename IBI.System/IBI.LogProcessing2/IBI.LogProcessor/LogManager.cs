﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.SqlServerCe;
using System.Timers;
using System.Data;
using System.Configuration;
using IBI.Shared.Diagnostics;
using Nest;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace IBI.LogProcessor
{
    public enum LogType
    {
        Event, Location, Progress
    }

    public interface ILog { }

    public partial class BusProgressLog : ILog
    {
        public System.DateTime Timestamp { get; set; }
        public int BusNumber { get; set; }
        public string ClientType { get; set; }
        public bool IsMaster { get; set; }
        public bool IsOnline { get; set; }
        public string Source { get; set; }
        public bool IsOverwritten { get; set; }
        public long JourneyNumber { get; set; }
        public string Line { get; set; }
        public string From { get; set; }
        public string Destination { get; set; }
        public long StopNumber { get; set; }
        public int Zone { get; set; }
        public string GPS { get; set; } // lat,long
        public string LogTrigger { get; set; }
        public string Data { get; set; }
        public int Result { get; set; }
        public string SignData { get; set; }
    }

    public partial class BusLocationLog : ILog
    {
        public System.DateTime Timestamp { get; set; }
        public string BusNumber { get; set; }
        public string ClientType { get; set; }
        public string GPS1 { get; set; } // lat,long
        public string GPS2 { get; set; } // lat,long
        public string Speed { get; set; }
        public int Player_PID { get; set; }
        public string SystemStatus { get; set; }
    }

    public partial class BusEventLog : ILog
    {
        public System.DateTime Timestamp { get; set; }
        public string BusNumber { get; set; }
        public string ClientType { get; set; }
        public Nullable<bool> IsMaster { get; set; }
        public Nullable<bool> IsOverwritten { get; set; }
        public string Source { get; set; }
        public string Line { get; set; }
        public string From { get; set; }
        public string Destination { get; set; }
        public string JourneyNumber { get; set; }
        public Nullable<long> StopNumber { get; set; }
        public Nullable<int> Zone { get; set; }
        public string GPS1 { get; set; } // lat,long
        public string GPS2 { get; set; } // lat,long
        public string EventSource { get; set; }
        public string EventId { get; set; }
        public string EventData { get; set; }
        public int Result { get; set; }
        public string ExtraData { get; set; }
    }

    public class LogManager : IDisposable
    {
        #region Members

        private System.Timers.Timer TmrProcessLogs
        {
            get;
            set;
        }

        private System.Timers.Timer tmrBulkCopyLog
        {
            get;
            set;
        }

        #endregion

        public LogManager()
        {
            int workerThreads = int.Parse(ConfigurationManager.AppSettings["WorkerThreads"]);
            int maxAsyncThreads = int.Parse(ConfigurationManager.AppSettings["MaxAsyncThreads"]);

            ThreadPool.SetMaxThreads(workerThreads, maxAsyncThreads);


            AppUtility.Log2File("LogManager", "Log Manager's new instance started");

            this.StartProcessTimer();

        }

        private void StartProcessTimer()
        {
            // Start or initialize hte process timer
            // Since logs are reported from busses at   .00, .15, .30 and .45
            // we set the timer ticks at                .05, .20, .35 and .50
            if (this.TmrProcessLogs == null)
            {
                this.TmrProcessLogs = new System.Timers.Timer();
                this.TmrProcessLogs.Elapsed += new System.Timers.ElapsedEventHandler(TmrProcessLogs_Elapsed);
            }

            this.TmrProcessLogs.Enabled = false;
            //DateTime nextTick = DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); temporarily commented . should be uncomment in live
            DateTime nextTick = DateTime.Now.AddSeconds(15); //DateTime.Now.AddMinutes(20 - (DateTime.Now.Minute % 15)); // just for testing to speed up the log processing to monitor + debug. should be commented on Live.

            nextTick = nextTick.AddSeconds(-1 * nextTick.Second);

            this.TmrProcessLogs.Interval = Math.Max(5000, nextTick.Subtract(DateTime.Now).TotalMilliseconds);
            this.TmrProcessLogs.Enabled = true;

            AppUtility.Log2File("LogManager", "LogProcessing Timer initializes");
            AppUtility.Log2File("LogManager", "Next log processing at " + nextTick.ToString());

        }


        private void TmrProcessLogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.TmrProcessLogs.Enabled = false;
            AppUtility.Log2File("LogManager", "Deleting Expired Log indexes");

            try
            {
                DeleteExpiredIndices(); //delete expired log indices             
            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("LogManager", "Deleting Expired Indexes CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }

            //--------

            try
            {

                this.ProcessLogs();

            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("LogManager", "ProcessLog Timer CRASHES -->" + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
            finally
            {
                this.StartProcessTimer();
                AppUtility.Log2File("LogManager", "ProcessLog Timer initializes again");
            }




        }
        public void ProcessLogs()
        {

            if (!AppSettings.LogProcessingEnabled())
                return;


            AppUtility.Log2File("LogManager", "Log Processing --> Loop on Log directories");

            DirectoryInfo logsDirectory = new DirectoryInfo(Path.Combine(AppSettings.GetResourceDirectory(), "Logs"));

            //Parallel.ForEach<DirectoryInfo>(logsDirectory.GetDirectories(), directory =>
            //{
            //    AppUtility.Log2File("LogManager", "Log Processing --> Processing directory: " + directory.Name);

            //    LogManager.ProcessLog(directory);
            //});

            foreach(DirectoryInfo directory in logsDirectory.GetDirectories().OrderBy(d=>d.Name))
            {
                AppUtility.Log2File("LogManager", "Log Processing --> Processing directory: " + directory.Name);

                LogManager.ProcessLog(directory);
            }


        }



        public static List<KeyValuePair<string, IndexStatus>> AllIndexes(string indexPrefix)
        {

            var settings = new ConnectionSettings(Node);
            ElasticClient client = new Nest.ElasticClient(settings);

            var indexes = client.Status().Indices.Where(i => i.Key.StartsWith(indexPrefix)).ToList();
            return indexes;

        }

        private static void DeleteExpiredIndices()
        {
            //delete expired indexes
            try
            {
                int daysToKeepLogs = int.Parse(ConfigurationManager.AppSettings["DaysToKeepLogs"]);
                DateTime expiryDate = DateTime.Now.AddDays(-daysToKeepLogs);
                 
                var settings = new ConnectionSettings(Node);
                ElasticClient client = new Nest.ElasticClient(settings);
                 
                foreach (KeyValuePair<string, IndexStatus> indexStatus in AllIndexes("ibi-"))
                {

                    string index = indexStatus.Key;

                    try
                    {

                        int year = int.Parse(index.Substring(index.LastIndexOf('-') + 1, 4));
                        int month = int.Parse(index.Substring(index.IndexOf('.') + 1, 2));
                        int day = int.Parse(index.Substring(index.LastIndexOf('.') + 1, 2));

                        DateTime indexDate = new DateTime(year, month, day);

                        if (indexDate < expiryDate)
                        {
                            //string index2Remove = string.Format("{0}{1}", indexPrefix, date.ToString("yyyy.MM.dd"));


                            var indexExists = client.IndexExists(index);
                            if (indexExists.Exists)
                            {
                                var indexDelResponse = client.DeleteIndex(index);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.LOG_PROCESSING, string.Format("Failed to remove index: {0}", index));
                    }
                }
            }

            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.LOG_PROCESSING, string.Format("Error occured while removing expired indices", Logger.GetDetailedError(ex)));
            }

        }

        private static void ProcessLog(DirectoryInfo logDirectory)
        {
            try
            {
                AppUtility.Log2File("LogManager", "Log Processing --> Looping through all the files of : " + logDirectory);


                //var files = logDirectory.GetFiles().ToList();
                int year, month, day;
                year = int.Parse(logDirectory.Name.Substring(0, 4));
                month = int.Parse(logDirectory.Name.Substring(4, 2));
                day = int.Parse(logDirectory.Name.Substring(6, 2));

                DateTime fileDate = new DateTime(year, month, day);

                Queue<FileInfo> files = new Queue<FileInfo>();
                Parallel.ForEach<FileInfo>(logDirectory.GetFiles(), file =>
                {
                    ProcessLog(file, fileDate);
                });

                if (logDirectory.Exists && logDirectory.GetFiles().Length == 0)
                {
                    logDirectory.Delete(true);
                    AppUtility.Log2File("LogManager", "  Log Directory Deleted --> " + logDirectory);
                }
                else
                {
                    ProcessLog(logDirectory);
                }

            }
            catch (Exception ex)
            {
                AppUtility.WriteError(ex);
                AppUtility.Log2File("LogManager", " ProcessLog() CRASHES 2 --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);

            }
        }


        public static void ProcessLog(FileInfo logFile, DateTime fileDate)
        {

            FileInfo currentLogFile = null;


            //if (logFile.LastWriteTime < DateTime.Now.AddMinutes(-5)) //Make sure that the file is fully written
            if (logFile.LastWriteTime < DateTime.Now.AddMinutes(5)) //Make sure that the file is fully written
            {
                AppUtility.WriteLine("Processing log: " + logFile.FullName);

                String targetDirectory = Path.Combine(Path.GetTempPath(), "IBI_Logs\\" + Guid.NewGuid());

                if (!Directory.Exists(targetDirectory))
                    Directory.CreateDirectory(targetDirectory);


                AppUtility.Log2File("LogManager", "  Using temporary directory: " + targetDirectory);

                try
                {

                    String targetLogName = Path.Combine(targetDirectory, logFile.Name);

                    List<String> logFiles = null;

                    switch (logFile.Extension.ToLower())
                    {
                        case ".tmp":
                            AppUtility.WriteLine("  Extracting files");

                            logFile.CopyTo(targetLogName);
                            logFiles = LogManager.ExtractFiles(new FileInfo(targetLogName));

                            AppUtility.Log2File("LogManager", "  Extracting files to: " + targetLogName);

                            break;

                        case ".log":
                            logFiles = new List<string>();

                            logFile.CopyTo(targetLogName);
                            logFiles.Add(targetLogName);

                            AppUtility.Log2File("LogManager", "  .log file copied --> " + targetLogName);

                            break;

                        default:
                            break;
                    }

                    foreach (String workingLogFile in logFiles)
                    {
                        System.Threading.Thread.Sleep(AppSettings.LogProcessingFileInterval());

                        switch (Path.GetFileName(workingLogFile.ToLower()))
                        {
                            case "progressevent.log":
                                {
                                    AppUtility.Log2File("LogManager", "  Start importing Progress Log");
                                    LogManager.ImportProgressLog(workingLogFile, fileDate);
                                    AppUtility.Log2File("LogManager", "  End importing Progress Log");
                                    break;
                                }

                            case "event.log":
                                {
                                    AppUtility.Log2File("LogManager", "  Start importing Event Log");
                                    LogManager.ImportEventLog(workingLogFile, fileDate);
                                    AppUtility.Log2File("LogManager", "  End importing Event Log");
                                    break;
                                }

                            case "location.log":
                                {
                                    AppUtility.Log2File("LogManager", "  Start importing Location Log");
                                    LogManager.ImportLocationLog(workingLogFile, fileDate);
                                    AppUtility.Log2File("LogManager", "  End importing Location Log");

                                    break;
                                }
                            default:
                                break;
                        }

                        if (logFile.Exists)
                        {
                            logFile.Delete();
                        }

                        AppUtility.Log2File("LogManager", "  Log File deleted -> " + logFile.FullName);
                    }
                }
                catch (Exception ex)
                {
                    AppUtility.WriteError(ex);
                    AppUtility.Log2File("LogManager", "  ProcessLog() CRASHES 1 --> " + ex.Message + Environment.NewLine + ex.StackTrace, true);

                    //if you are here, there is some problem with current tmp file. Simply move it to badLogFolder
                    ////////////if (logFile != null && logFile.Exists)
                    ////////////    MoveLogFileToBadLogFolder(logFile.FullName, true);

                    //if (File.Exists(logFile.FullName))
                    //    File.Delete(logFile.FullName);
                }
                finally
                {
                    Directory.Delete(targetDirectory, true);
                    AppUtility.Log2File("LogManager", "  Target Directory Deleted --> " + targetDirectory);
                }


                GC.Collect();
            }

        }


        private static void ImportProgressLog(String filePath, DateTime fileDate)
        {
            AppUtility.WriteLine("  Importing ProgressLog: " + filePath);
            AppUtility.Log2File("LogManager", "  Importing ProgressLog: " + filePath);



            AppUtility.Log2File("LogManager", "   ImportProgressLog()  Looping through all the lines of files");

            //foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
            Parallel.ForEach<string>(File.ReadAllLines(filePath, System.Text.Encoding.UTF8), line =>
            {
                System.Threading.Thread.Sleep(AppSettings.LogProcessingLineInterval());

                try
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        DateTime timestamp = DateTime.MinValue;
                        int busNumber = 0;
                        String clientType = "";
                        Boolean isMaster = false;
                        Boolean isOnline = false;
                        String source = "";
                        Boolean isOverwritten = false;
                        long journeyNumber = 0;
                        String currentLine = "";
                        String from = "";
                        String destination = "";
                        long stopNumber = 0;
                        int zone = 0;
                        String latitude = "";
                        String longitude = "";
                        String logTrigger = "";
                        String data = "";
                        int result = 0;
                        String signData = "";

                        switch (values[0].ToLower())
                        {
                            case "ver1":
                                //Ignoring VER1
                                AppUtility.WriteLine("  Error reading line (Ignoring VER1): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Ignoring VER1): " + line);

                                return; //equivalent to continue; of foreach here

                            case "ver2":
                                timestamp = DateTime.Parse(values[1], AppUtility.GetDateTimeFormatProvider());
                                int.TryParse(values[2], out busNumber);
                                clientType = values[3];
                                isMaster = (values[4] == "1");
                                isOnline = (values[5] == "1");
                                source = values[6];
                                isOverwritten = (values[7] == "1");
                                long.TryParse(values[8], out journeyNumber);
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                long.TryParse(values[12], out stopNumber);
                                int.TryParse(values[13], out zone);
                                latitude = values[14];
                                longitude = values[15];
                                logTrigger = values[16];
                                data = values[17];
                                int.TryParse(values[18], out result);

                                if (busNumber <= 0 || String.IsNullOrEmpty(clientType))
                                    return; //equivalent to continue; of foreach here

                                break;

                            case "ver3":
                                timestamp = DateTime.Parse(values[1], AppUtility.GetDateTimeFormatProvider());
                                int.TryParse(values[2], out busNumber);
                                clientType = values[3];
                                isMaster = (values[4] == "1");
                                isOnline = (values[5] == "1");
                                source = values[6];
                                isOverwritten = (values[7] == "1");
                                long.TryParse(values[8], out journeyNumber);
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                long.TryParse(values[12], out stopNumber);
                                int.TryParse(values[13], out zone);
                                latitude = values[14];
                                longitude = values[15];
                                logTrigger = values[16];
                                data = values[17];
                                int.TryParse(values[18], out result);

                                if (values.Length > 19)
                                    signData = values[19];

                                if (busNumber <= 0 || String.IsNullOrEmpty(clientType)) //Error parsing line. No BusNumber or ClientType
                                    return; //equivalent to continue; of foreach here

                                break;

                            default:
                                //Unknown version
                                AppUtility.WriteLine("  Error reading line (Unknown version): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Unknown version): " + line);

                                return; //equivalent to continue; of foreach here
                        }


                        BusProgressLog log = new BusProgressLog()
                        {
                            BusNumber = busNumber,
                            ClientType = clientType,
                            Data = data,
                            Destination = destination,
                            From = from,
                            IsMaster = isMaster,
                            IsOnline = isOnline,
                            IsOverwritten = isOverwritten,
                            JourneyNumber = journeyNumber,
                            GPS = string.Format("{0},{1}", latitude, longitude),
                            Line = currentLine,
                            LogTrigger = logTrigger,
                            Result = result,
                            SignData = signData,
                            Source = source,
                            StopNumber = stopNumber,
                            Timestamp = timestamp,
                            Zone = zone
                        };

                        AddLog<BusProgressLog>(LogType.Progress, log, fileDate);
                    }
                }
                catch (Exception ex)
                {
                    //Error reading line
                    AppUtility.WriteError("  Error reading line: " + line);
                    AppUtility.Log2File("LogManager", " ImportProgressLog() -->  Error reading line: " + line + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + "File: " + filePath, true);

                    //move this log file to BadLogFiles location
                    //MoveLogFileToBadLogFolder(filePath, true);
                    //return;


                    MoveBadLinesToBadLogFolder(filePath, "ProgressLog", line);
                }
            });
        }

        private static void ImportEventLog(String filePath, DateTime fileDate)
        {
            AppUtility.WriteLine("  Importing EventLog: " + filePath);
            AppUtility.Log2File("LogManager", "  Importing EventLog: " + filePath);



            //foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
            Parallel.ForEach<string>(File.ReadAllLines(filePath, System.Text.Encoding.UTF8), line =>
            {
                System.Threading.Thread.Sleep(AppSettings.LogProcessingLineInterval());

                try
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        String busNumber = "";
                        String clientType = "";
                        DateTime timestamp = DateTime.MinValue;
                        String gps1 = "1.1,1.1";
                        String gps2 = "1.1,1.1";
                        String isMaster = "NULL";
                        String isOverwritten = "NULL";
                        String source = "";
                        String currentLine = "";
                        String from = "";
                        String destination = "";
                        String journeyNumber = "NULL";
                        long stopNumber = 0;
                        String zone = "NULL";
                        String eventSource = "";
                        String eventId = "";
                        String eventData = "";
                        int result = 0;
                        String extraData = "";

                        switch (values[0].ToLower())
                        {
                            case "ver1":
                                busNumber = values[1];
                                clientType = values[2];
                                timestamp = DateTime.Parse(values[3], AppUtility.GetDateTimeFormatProvider());
                                gps1 = AppUtility.ValidateGPS(values[4]);
                                gps2 = AppUtility.ValidateGPS(values[5]);
                                isMaster = String.IsNullOrEmpty(values[6]) ? "NULL" : values[6];
                                isOverwritten = String.IsNullOrEmpty(values[7]) ? "NULL" : values[7];
                                source = values[8];
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                journeyNumber = String.IsNullOrEmpty(values[12]) ? "NULL" : values[12];
                                long.TryParse(values[13], out stopNumber);
                                zone = String.IsNullOrEmpty(values[14]) ? "0" : values[14];
                                eventSource = values[16];
                                eventId = values[15];
                                eventData = values[17];
                                int.TryParse(values[18], out result);
                                extraData = values[19];

                                break;

                            default:
                                //Unknown version
                                AppUtility.WriteError("  Error reading line (Unknown version): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Unknown version): " + line);
                                return; //equivalent to continue; of foreach here
                        }

                        if (String.IsNullOrEmpty(busNumber) || String.IsNullOrEmpty(clientType))
                            return; //equivalent to continue; of foreach here

                        BusEventLog log = new BusEventLog()
                        {
                            BusNumber = busNumber,
                            ClientType = clientType,
                            Destination = destination,
                            EventData = eventData,
                            EventId = eventId,
                            EventSource = eventSource,
                            ExtraData = extraData,
                            From = from,
                            GPS1 = gps1,
                            GPS2 = gps2,
                            IsMaster = (isMaster == "1" ? true : false),
                            IsOverwritten = (isOverwritten == "1" ? true : false),
                            JourneyNumber = journeyNumber,
                            Line = currentLine,
                            Result = result,
                            Source = source,
                            StopNumber = stopNumber,
                            Timestamp = timestamp,
                            Zone = string.IsNullOrEmpty(zone) ? 0 : int.Parse(zone)
                        };

                        AddLog<BusEventLog>(LogType.Event, log, fileDate);



                    }

                }
                catch (Exception ex)
                {
                    //Error reading line
                    AppUtility.WriteError("  Error reading line: " + line);
                    AppUtility.Log2File("LogManager", " ImportEventLog() -->  Error reading line: " + line + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + "File: " + filePath, true);

                    //move this log file to BadLogFiles location
                    //MoveLogFileToBadLogFolder(filePath, true);
                    MoveBadLinesToBadLogFolder(filePath, "EventLog", line);

                }
                finally
                {

                }
            });
        }

        private static void ImportLocationLog(String filePath, DateTime fileDate)
        {
            AppUtility.WriteLine("  Importing LocationLog: " + filePath);
            AppUtility.Log2File("LogManager", "  Importing LocationLog" + filePath);


            //foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
            Parallel.ForEach<string>(File.ReadAllLines(filePath, System.Text.Encoding.UTF8), line =>
            {
                System.Threading.Thread.Sleep(AppSettings.LogProcessingLineInterval());

                try
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        //Fix: invalid versions deployed
                        if (values.Length != 9)
                            return; //equivalent to continue; of foreach here

                        String busNumber = "";
                        String clientType = "";
                        DateTime timestamp = DateTime.MinValue;
                        String gps1 = "1.1,1.1";
                        String gps2 = "1.1,1.1";
                        String speed = "";
                        int playerPid = 0;
                        String systemStatus = "";

                        switch (values[0].ToLower())
                        {
                            case "ver1":
                                busNumber = values[1];
                                clientType = values[2];
                                timestamp = DateTime.Parse(values[3], AppUtility.GetDateTimeFormatProvider());
                                gps1 = AppUtility.ValidateGPS(values[4]);
                                gps2 = AppUtility.ValidateGPS(values[5]);
                                speed = values[6];
                                int.TryParse(values[7], out playerPid);
                                systemStatus = values[8];

                                //Console.WriteLine("BusNumber: " + busNumber + " - GPS 1:" + gps1 + " - GPS 2:" + gps2 + " - Timestamp: " + timestamp + " - Speed:" + speed + " - Status: " + systemStatus);

                                break;

                            default:
                                //Unknown version
                                AppUtility.WriteError("  Error reading line (Unknown version): " + line);
                                AppUtility.Log2File("LogManager", "  Error reading line (Unknown version): " + line);
                                return; //equivalent to continue; of foreach here
                        }

                        if (String.IsNullOrEmpty(busNumber) || String.IsNullOrEmpty(clientType))
                            return; //equivalent to continue; of foreach here


                        BusLocationLog log = new BusLocationLog()
                        {
                            BusNumber = busNumber,
                            ClientType = clientType,
                            GPS1 = gps1,
                            GPS2 = gps2,
                            Player_PID = playerPid,
                            Speed = speed,
                            SystemStatus = systemStatus,
                            Timestamp = timestamp
                        };

                        AddLog<BusLocationLog>(LogType.Location, log, fileDate);

                    }
                }
                catch (Exception ex)
                {
                    //Error reading line
                    AppUtility.WriteError("  Error reading line: " + line);
                    AppUtility.Log2File("LogManager", " ImportLocationLog() -->  Error reading line: " + line + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + "File: " + filePath, true);

                    //move this log file to BadLogFiles location
                    //MoveLogFileToBadLogFolder(filePath, true);
                    //return;

                    MoveBadLinesToBadLogFolder(filePath, "Location", line);
                }
                finally
                {

                }
            });
        }


        private static void MoveLogFileToBadLogFolder(string filePath, bool delete = false)
        {

            try
            {

                if (File.Exists(filePath))
                {
                    DateTime fileTime = File.GetLastWriteTime(filePath);

                    string badFileName = fileTime.ToString("yyyyMMdd_HHmmssfff") + "_" + Path.GetFileName(filePath);


                    string badFolder = Path.Combine(ConfigurationManager.AppSettings["BadLogDirectory"], fileTime.ToString("yyyyMMdd"));

                    if (!Directory.Exists(badFolder))
                    {
                        DirectoryInfo dirInfo = Directory.CreateDirectory(badFolder);
                    }

                    File.Copy(filePath, Path.Combine(badFolder, badFileName));

                    if (delete)
                        File.Delete(filePath);


                    AppUtility.Log2File("LogManager", "--> Problematic file moved to BadLogArchive folder: " + Path.Combine(badFolder, badFileName));
                    AppUtility.WriteError("LogManager --> Problematic file moved to BadLogArchive folder: " + Path.Combine(badFolder, badFileName));
                }
            }
            catch (Exception ex)
            {
                //do nothing
                AppUtility.Log2File("LogManager", " Error while moving a log file (" + filePath + ") to BadLogFolder. " + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }

        }



        private static void MoveBadLinesToBadLogFolder(string filePath, string fileType, string badLine)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    DateTime fileTime = File.GetLastWriteTime(filePath);

                    AppUtility.Log2File("LogManager", "--> File.GetLastWriteTime: " + fileTime.ToString());

                    string badFolder = Path.Combine(ConfigurationManager.AppSettings["BadLogDirectory"], fileTime.ToString("yyyyMMdd"));
                    string badFileName = fileTime.ToString("yyyyMMdd") + "_" + fileType + "_BadLines.log";

                    if (!Directory.Exists(badFolder)) Directory.CreateDirectory(badFolder);

                    badFileName = Path.Combine(badFolder, badFileName);
                    //badLine += Environment.NewLine + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + Environment.NewLine;
                    File.AppendAllText(badFileName, badLine + Environment.NewLine);

                    AppUtility.Log2File("LogManager", "--> Problematic Line moved to BadLog File: " + Path.Combine(badFolder, badFileName));
                    AppUtility.WriteError("LogManager --> Problematic Line moved to BadLog File: " + Path.Combine(badFolder, badFileName));
                }
            }
            catch (Exception ex)
            {
                //do nothing
                AppUtility.Log2File("LogManager", " Error while moving a Bad Lines (" + badLine + ") to BadLogFile. " + ex.Message + Environment.NewLine + ex.StackTrace, true);
            }
        }

        private static void AddLog<T>(LogType type, T log, DateTime fileDate) where T : class
        {

            ElasticClient client = null;
            client = GetElasticClient(type, fileDate);

            if (client != null)
            {
                client.Index<T>(log);
            }
            System.Threading.Thread.Sleep(1);
        }

        public void Dispose()
        {
            this.tmrBulkCopyLog.Enabled = false;
            this.tmrBulkCopyLog.Dispose();

            this.TmrProcessLogs.Enabled = false;
            this.TmrProcessLogs.Dispose();
        }
        #region Private helpers

        private static List<String> ExtractFiles(FileInfo file)
        {
            String targetPath = file.Directory.FullName;

            List<String> fileList = new List<String>();

            FastZip zipEngine = new FastZip();
            zipEngine.ExtractZip(file.FullName, targetPath, "");

            AppUtility.Log2File("LogManager", "  Extracting files");
            foreach (String logFilePath in Directory.GetFiles(targetPath))
            {
                if (String.Compare(Path.GetFileName(file.FullName), Path.GetFileName(logFilePath), true) != 0)
                {
                    fileList.Add(logFilePath);
                    AppUtility.Log2File("LogManager", "    Extracte file: " + logFilePath);
                }

            }

            return fileList;
        }

        private void UpdateGPS()
        {
            try
            {

                using (SqlConnection conLogs = new SqlConnection(ConfigurationManager.ConnectionStrings["IBILogsDatabaseConnection"].ToString()))
                {
                    using (SqlCommand cmdLogs = new SqlCommand())
                    {

                        cmdLogs.Connection = conLogs;
                        cmdLogs.CommandText = "UpdateGPS";
                        cmdLogs.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            conLogs.Open();
                            cmdLogs.ExecuteNonQuery();
                            conLogs.Close();
                        }
                        catch (Exception err)
                        {
                            Console.WriteLine("Error Executing:  UpdateGPS() --> " + err.Message);
                            AppUtility.Log2File("BulkCopy", "Error Executing: UpdateGPS() --> " + err.Message + " --- " + err.StackTrace);
                        }
                        conLogs.Close();

                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Executing:  UpdateGPS() --> " + ex.Message);
                AppUtility.WriteError("Error Executing: UpdateGPS() --> " + ex.Message + " --- " + ex.StackTrace);
                AppUtility.Log2File("BulkCopy", "Error Executing: UpdateGPS() --> " + ex.Message + " --- " + ex.StackTrace);

            }


        }


        static void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {

            //throw new NotImplementedException();
            //e.RowsCopied
        }


        #endregion

        public static string ElasticSearchUri
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticSearchUri"].ToString();
            }
        }

        public static Uri Node
        {
            get
            {
                return new Uri(ElasticSearchUri);
            }
        }


        private static ElasticClient GetElasticClient(LogType type, DateTime fileDate)
        {

            ElasticClient client = null;
            switch (type)
            {
                case LogType.Event:
                    {
                        var settings = new ConnectionSettings(Node, defaultIndex: string.Format("ibi-eventlogs-{0}", fileDate.ToString("yyyy.MM.dd")));
                        client = new Nest.ElasticClient(settings);
                        break;
                    }
                case LogType.Location:
                    {
                        var settings = new ConnectionSettings(Node, defaultIndex: string.Format("ibi-locationlogs-{0}", fileDate.ToString("yyyy.MM.dd")));
                        client = new Nest.ElasticClient(settings);
                        break;
                    }
                case LogType.Progress:
                    {
                        var settings = new ConnectionSettings(Node, defaultIndex: string.Format("ibi-progresslogs-{0}", fileDate.ToString("yyyy.MM.dd")));
                        client = new Nest.ElasticClient(settings);
                        break;
                    }
            }

            return client;

        }

        public static string GetIndexName(DateTime date, string baseIndex)
        {
            //string quarter = string.Empty;
            //if (date.Month >= 4 && date.Month <= 6)
            //    quarter = "Q1";
            //else if (date.Month >= 7 && date.Month <= 9)
            //    quarter = "Q2";
            //else if (date.Month >= 10 && date.Month <= 12)
            //    quarter = "Q3";
            //else
            //    quarter = "Q4";

            return string.Format("{0}-{1}", baseIndex, date.ToString("YYYY.MM"));

        }

        private static ElasticClient _locationClient;
        private static ElasticClient LocationClient
        {
            get
            {
                if (_locationClient == null)
                {
                    var settings = new ConnectionSettings(Node, defaultIndex: "ibi-buslocationlogs");
                    _locationClient = new Nest.ElasticClient(settings);
                }
                return _locationClient;
            }
        }

        private static ElasticClient _progressClient;
        private static ElasticClient ProgressClient
        {
            get
            {
                if (_progressClient == null)
                {
                    var settings = new ConnectionSettings(Node, defaultIndex: "ibi-busprogresslogs");
                    _progressClient = new Nest.ElasticClient(settings);
                }

                return _progressClient;
            }
        }

        private static ElasticClient _eventClient;
        private static ElasticClient EventClient
        {
            get
            {
                if (_eventClient == null)
                {
                    var settings = new ConnectionSettings(Node, defaultIndex: "ibi-buseventlogs");
                    _eventClient = new Nest.ElasticClient(settings);
                }

                return _eventClient;
            }
        }
    }
}
