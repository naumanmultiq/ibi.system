﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace WebHandlers
{
    class ConfigHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            AppendToLog(context, "Client requested HotspotConfig: " + context.Request.Url.ToString());

            String configRequest = context.Request.Url.ToString();

            configRequest = configRequest.Substring(configRequest.LastIndexOf("/") + 1);
            configRequest = configRequest.Substring(0, configRequest.IndexOf("?"));

            String mac = MakeMacPretty(context.Request.QueryString["mac"].ToString());

            String config = "";

            switch (configRequest.ToLower())
            {
                case "hotspotconfig.xml":
                    config = this.RebuildHotspotConfig(mac);

                    AppendToLog(context, "Client downloaded HotspotConfig: " + mac);

                    break;

                default:
                    throw new HttpException(404, "Unknown config request");

            }

            Byte[] buffer = System.Text.Encoding.UTF8.GetBytes(config);

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=" & tmpFileInfo.Name)

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", buffer.Length.ToString());
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            //Set the HTTP MIME type of the output
            context.Response.ContentType = "application/xml";

            //Write the data out to the client. 
            context.Response.BinaryWrite(buffer);

            context.Response.End();

            return;
        }

        private String RebuildHotspotConfig(String mac)
        {
            String hotspotID = "";
            String accessPointID = "";
            String accessPoint_ssid = "";
            String accessPointGroup_ssid = "";
            String hotspot_ssid = "";
            String authServer = "http://hotspot-v2.mermaid.dk/services/";
            Boolean enabled = true;
            String accessPoint_LandingPage = "";
            String accessPointGroup_LandingPage = "";
            String hotspot_LandingPage = "";

            String connectionString = ConfigurationManager.AppSettings["HotspotDBConnectionString"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = String.Format(@"SELECT 
                                                    hots.[HotspotID]
                                                    ,aps.[ID] AS [AccessPointID]
                                                    ,aps.[SSID] AS [AccessPoint_SSID]
                                                    ,apgroups.[SSID] AS [AccessPointGroup_SSID]
                                                    ,hots.[SSID] AS [Hotspot_SSID]
                                                    ,aps.[Enabled]
                                                    ,aps.[LandingPage] AS [AccessPoint_LandingPage]
                                                    ,apgroups.[LandingPage] AS [AccessPointGroup_LandingPage]
                                                    ,hots.[LandingPage] AS [Hotspot_LandingPage]
                                                FROM 	    
	                                                [MermaidHotspotServer_GRANDPA].[dbo].[AccessPoints] aps
	                                                    LEFT JOIN
	                                                [MermaidHotspotServer_GRANDPA].[dbo].[AccessPointGroups] apgroups
	                                                    ON aps.[AccessPointGroup_ID]=apgroups.[ID]
	                                                    LEFT JOIN
	                                                [MermaidHotspotServer_GRANDPA].[dbo].[Hotspots] hots
	                                                    ON apgroups.[Hotspot_ID]=hots.[ID]
                                                WHERE 
	                                                hots.[HotspotID]!='PRODUCTION' 
	                                                AND
	                                                aps.[IsDeleted]=0
	                                                AND
	                                                aps.[MAC]='{0}'
                                                ORDER BY aps.[LastPing] DESC", mac);

                DataTable data = new DataTable();

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();

                            hotspotID = reader.GetString(reader.GetOrdinal("HotspotID"));
                            accessPointID = reader.GetInt32(reader.GetOrdinal("AccessPointID")).ToString();
                            accessPoint_ssid = reader.GetString(reader.GetOrdinal("AccessPoint_SSID"));
                            accessPointGroup_ssid = reader.GetString(reader.GetOrdinal("AccessPointGroup_SSID"));
                            hotspot_ssid = reader.GetString(reader.GetOrdinal("Hotspot_SSID"));
                            enabled = reader.GetBoolean(reader.GetOrdinal("Enabled"));
                            accessPoint_LandingPage = reader.GetString(reader.GetOrdinal("AccessPoint_LandingPage"));
                            accessPointGroup_LandingPage = reader.GetString(reader.GetOrdinal("AccessPointGroup_LandingPage"));
                            hotspot_LandingPage = reader.GetString(reader.GetOrdinal("Hotspot_LandingPage"));
                        }
                        else
                        {
                            throw new HttpException(404, "AccessPoint not found");
                        }
                    }
                }
            }

            XDocument config = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"),
                                                new XElement("Settings",
                                                new XAttribute("name", "com.lancer.hotspotservice"),
                                                new XElement("Setting", mac.ToUpper(),
                                                    new XAttribute("name", "Name"),
                                                    new XAttribute("default", mac.ToUpper())
                                                ),
                                                 new XElement("Setting", hotspotID,
                                                    new XAttribute("name", "HotspotID"),
                                                    new XAttribute("default", hotspotID)
                                                ),
                                                 new XElement("Setting", accessPointID,
                                                    new XAttribute("name", "AccessPointID"),
                                                    new XAttribute("default", accessPointID)
                                                ),
                                                 new XElement("Setting", !String.IsNullOrEmpty(accessPoint_ssid) ? accessPoint_ssid : !String.IsNullOrEmpty(accessPointGroup_ssid) ? accessPointGroup_ssid : hotspot_ssid,
                                                    new XAttribute("name", "SSID"),
                                                    new XAttribute("default", "HS_PROD")
                                                ),
                                                 new XElement("Setting", authServer,
                                                    new XAttribute("name", "AuthServer"),
                                                    new XAttribute("default", authServer)
                                                ),
                                                 new XElement("Setting", enabled ? "true" : "false",
                                                    new XAttribute("name", "Enabled"),
                                                    new XAttribute("type", "boolean"),
                                                    new XAttribute("default", "true")
                                                ),
                                                 new XElement("Setting", "60",
                                                    new XAttribute("name", "PingInterval"),
                                                    new XAttribute("type", "int"),
                                                    new XAttribute("default", "60")
                                                ),
                                                 new XElement("Setting", !String.IsNullOrEmpty(accessPoint_LandingPage) ? accessPoint_LandingPage : !String.IsNullOrEmpty(accessPointGroup_LandingPage) ? accessPointGroup_LandingPage : hotspot_LandingPage,
                                                    new XAttribute("name", "LandingPage"),
                                                    new XAttribute("default", "https://hotspot-v2.mermaid.dk/account/login")
                                                ),
                                                 new XElement("Setting", "wlan0",
                                                    new XAttribute("name", "WLANDevice"),
                                                    new XAttribute("default", "wlan0")
                                                ),
                                                 new XElement("Setting", "192.168.0.1",
                                                    new XAttribute("name", "WLANIpAddr"),
                                                    new XAttribute("default", "192.168.0.1")
                                                ),
                                                 new XElement("Setting", "ppp0",
                                                    new XAttribute("name", "WWANDevice"),
                                                    new XAttribute("default", "ppp0")
                                                ),
                                                 new XElement("Setting", "true",
                                                    new XAttribute("name", "RouterEnabled"),
                                                    new XAttribute("type", "boolean"),
                                                    new XAttribute("default", "true")
                                                )
                                            )
                                            );

            return String.Concat(config.Declaration.ToString(), config.ToString(SaveOptions.DisableFormatting));
        }

        private String RebuildCommonConfig(String mac)
        {
            return "";
        }

        private void AppendToLog(HttpContext context, String text)
        {
            String logEntry = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "> " + text;

            try
            {
                String logPath = Path.Combine(context.Request.PhysicalApplicationPath, @"Logs\rescuelog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");

                File.AppendAllText(logPath, logEntry + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //SILENT
            }
        }

        public static String MakeMacPretty(String value)
        {
            return Regex.Replace(value.ToUpper(), "[^.0-9A-F]", "");
        }    
    }
}
