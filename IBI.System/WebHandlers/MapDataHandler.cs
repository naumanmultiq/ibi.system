﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

/// <summary>
/// Summary description for MapDataHandler
/// </summary>
/// 
namespace WebHandlers
{
    public class MapDataHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            String json = "[";

            String connectionString = ConfigurationManager.AppSettings["IBIDatabaseConnection"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = @"SELECT
	                                [BusNumber]
	                                ,[LastKnownLocation].[Lat] AS [Latitude]
	                                ,[LastKnownLocation].[Long] AS [Longitude] 
	                                ,(SELECT TOP 1 [LastPing] FROM [Clients] WHERE [BusNumber]=b.[BusNumber] ORDER BY [LastPing] DESC) AS LastPing
	                              FROM [Buses] b		
	                              WHERE [InOperation]=1" + Environment.NewLine;

                if (!String.IsNullOrEmpty(context.Request["customerids"]))
                {
                    String[] customerIDs = context.Request["customerids"].Split(',');

                    if (customerIDs.Length > 0)
                    {
                        script += "AND (";

                        for (int i = 0; i < customerIDs.Length; i++)
                        {
                            if (i > 0)
                                script += "OR ";

                            script += "[CustomerID]=" + customerIDs[i];

                        }

                        script += ")" + Environment.NewLine;
                    }
                }

                DataTable data = new DataTable();

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int rowCounter = 0;

                        while (reader.Read())
                        {
                            object lastPing = reader["LastPing"];
                            object latitudeObject = reader[1];
                            object longitudeObject = reader[2];

                            String online = lastPing is DateTime ? ((DateTime)lastPing).AddMinutes(10) > DateTime.Now ? "true" : "false" : "false";
                            String latitude = latitudeObject is Double ? ((Double)latitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                            String longitude = longitudeObject is Double ? ((Double)longitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";

                            json += (rowCounter == 0 ? "" : ",");
                            json += "{";
                            json += "\"BusNumber\":\"" + reader["BusNumber"].ToString() + "\"";
                            json += ",\"Online\":" + online;
                            json += ",\"Position\":{";
                            json += "\"lat\":" + latitude;
                            json += ",\"long\":" + longitude + "}";
                            json += "}";

                            rowCounter++;
                        }
                    }
                }
            }

            json += "]";

            Byte[] buffer = System.Text.Encoding.UTF8.GetBytes(json);

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=" & tmpFileInfo.Name)

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", buffer.Length.ToString());
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            //Set the HTTP MIME type of the output
            context.Response.ContentType = "application/json";

            //Write the data out to the client. 
            context.Response.BinaryWrite(buffer);

            context.Response.End();

            return;
        }
    }
}