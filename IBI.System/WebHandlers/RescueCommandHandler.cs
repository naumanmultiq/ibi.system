﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml.Linq;
using System.IO;

namespace WebHandlers
{
    class RescueCommandHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            String rescueCommand = ConfigurationManager.AppSettings["RescueCommand"];

            if(String.IsNullOrEmpty(rescueCommand))
                throw new HttpException(404, "No rescue command found");

            //AppendToLog(context, "RescueCommand requested: " + context.Request.Url.ToString());

            Byte[] buffer = System.Text.Encoding.UTF8.GetBytes(rescueCommand);

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=" & tmpFileInfo.Name)

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", buffer.Length.ToString());
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            //Set the HTTP MIME type of the output
            context.Response.ContentType = "text/plain";

            //Write the data out to the client. 
            context.Response.BinaryWrite(buffer);

            context.Response.End();

            return;
        }

        private void AppendToLog(HttpContext context, String text)
        {
            String logEntry = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "> " + text;

            try
            {
                String logPath = Path.Combine(context.Request.PhysicalApplicationPath, @"Logs\rescuelog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");

                File.AppendAllText(logPath, logEntry + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //SILENT
            }
        }
    }
}
