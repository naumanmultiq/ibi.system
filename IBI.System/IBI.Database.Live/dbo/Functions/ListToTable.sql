﻿-- =============================================
-- Author:		KHI	
-- Create date: 24th october 2012
-- Description:	splits comma list into a table (rows)
-- =============================================
CREATE FUNCTION [dbo].[ListToTable]
(
	-- Add the parameters for the function here
	@CommaList varchar(2000) --format = 'val, val, val'
)
RETURNS 
@tbl TABLE 
(
	-- Add the column definitions for the TABLE variable here
	Value varchar(100)
)
AS
	BEGIN
		-- Fill the table variable with the rows for your result set
	DECLARE @s TABLE (Val varchar(100))
	INSERT  INTO @s
	VALUES  (@CommaList)

	INSERT INTO @tbl 
	SELECT  T.xmlVal.value('.', 'int') AS Value
	FROM    ( SELECT  CONVERT(XML, '<row>' + REPLACE(Val, ',', '</row><row>') + '</row>') AS xmlVal
			  FROM      @s 
			) s
			CROSS APPLY s.xmlVal.nodes('row') AS T(xmlVal)
		
		RETURN 
	END

 