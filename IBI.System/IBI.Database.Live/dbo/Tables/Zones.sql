﻿CREATE TABLE [dbo].[Zones] (
    [ZoneId]      INT               IDENTITY (1, 1) NOT NULL,
    [Timestamp]   DATETIME          CONSTRAINT [DF_Zones_Timestamp] DEFAULT (getdate()) NOT NULL,
    [ZoneName]    NVARCHAR (100)    NOT NULL,
    [Description] NVARCHAR (100)    NULL,
    [Operator]    NVARCHAR (100)    NULL,
    [GEO]         [sys].[geography] NULL,
    [KML]         XML               NULL,
    CONSTRAINT [PK_Zones] PRIMARY KEY CLUSTERED ([ZoneId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Zones]
    ON [dbo].[Zones]([ZoneName] ASC, [Operator] ASC);


GO
CREATE SPATIAL INDEX [Idx_Zones_GEO]
    ON [dbo].[Zones] ([GEO])
    USING GEOGRAPHY_GRID
    WITH  (
            GRIDS = (LEVEL_1 = MEDIUM, LEVEL_2 = MEDIUM, LEVEL_3 = MEDIUM, LEVEL_4 = MEDIUM)
          );

