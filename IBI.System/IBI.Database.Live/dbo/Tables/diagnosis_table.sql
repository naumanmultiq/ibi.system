﻿CREATE TABLE [dbo].[diagnosis_table] (
    [JourneyNumber]      INT           NULL,
    [StartDate]          VARCHAR (100) NULL,
    [EndDate]            VARCHAR (100) NULL,
    [ScheduleNumber]     INT           NULL,
    [BusNumber]          VARCHAR (100) NULL,
    [MissedStopsXml]     VARCHAR (MAX) NULL,
    [MissedJourneyStops] VARCHAR (MAX) NULL,
    [LastStop]           BIT           NULL
);

