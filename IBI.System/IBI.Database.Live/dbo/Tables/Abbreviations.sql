﻿CREATE TABLE [dbo].[Abbreviations] (
    [sqlid]            INT           IDENTITY (1, 1) NOT NULL,
    [Voice]            VARCHAR (50)  NULL,
    [AbbreviationText] VARCHAR (150) NULL,
    [FullText]         VARCHAR (200) NULL,
    CONSTRAINT [PK_Abbreviations] PRIMARY KEY CLUSTERED ([sqlid] ASC)
);

