﻿CREATE TABLE [dbo].[Apps] (
    [ApiId]           INT          NOT NULL,
    [ApiKey]          VARCHAR (50) NOT NULL,
    [ApplicationName] VARCHAR (50) NULL,
    CONSTRAINT [PK_Apps_1] PRIMARY KEY CLUSTERED ([ApiId] ASC)
);

