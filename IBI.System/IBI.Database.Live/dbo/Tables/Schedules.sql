﻿CREATE TABLE [dbo].[Schedules] (
    [ScheduleId]       INT            IDENTITY (1, 1) NOT NULL,
    [CustomerId]       INT            NOT NULL,
    [RouteDirectionId] INT            NOT NULL,
    [Line]             NVARCHAR (MAX) NULL,
    [FromName]         NVARCHAR (MAX) NOT NULL,
    [Destination]      NVARCHAR (MAX) NOT NULL,
    [ViaName]          NVARCHAR (MAX) NULL,
    [LastUpdated]      DATETIME       NULL,
    CONSTRAINT [PK_Schedules] PRIMARY KEY CLUSTERED ([ScheduleId] ASC),
    CONSTRAINT [FK_Schedules_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]) ON UPDATE CASCADE,
    CONSTRAINT [FK_Schedules_RouteDirections] FOREIGN KEY ([RouteDirectionId]) REFERENCES [dbo].[RouteDirections] ([RouteDirectionId]) ON UPDATE CASCADE
);

