﻿CREATE TABLE [dbo].[LanguageDefinitionsDCU] (
    [Id]      VARCHAR (500)  NOT NULL,
    [Culture] NCHAR (5)      NOT NULL,
    [Text]    NVARCHAR (500) NULL
);

