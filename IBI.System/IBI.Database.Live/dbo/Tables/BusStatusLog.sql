﻿CREATE TABLE [dbo].[BusStatusLog] (
    [Timestamp]              DATETIME      NOT NULL,
    [BusNumber]              INT           NOT NULL,
    [IsOK]                   BIT           NOT NULL,
    [LastDCUPing]            DATETIME      NULL,
    [LastVTCPing]            DATETIME      NULL,
    [LastInfotainmentPing]   DATETIME      NULL,
    [LastHotspotPing]        DATETIME      NULL,
    [LastSchedule]           DATETIME      NULL,
    [ScheduledJourneyNumber] BIGINT        NULL,
    [ActualJourneyNumber]    BIGINT        NULL,
    [Latitude]               VARCHAR (50)  NULL,
    [Longitude]              VARCHAR (50)  NULL,
    [Remarks]                VARCHAR (MAX) NULL,
    [MarkedForService]       BIT           NULL,
    [ServiceLevel]           INT           NULL,
    [IgnitionOn]             BIGINT        CONSTRAINT [DF_BusStatusLog_IgnitionOn] DEFAULT (NULL) NULL
);


GO
CREATE NONCLUSTERED INDEX [Idx_BusStatusLog_BusNumber_TimeStamp]
    ON [dbo].[BusStatusLog]([BusNumber] ASC, [Timestamp] ASC);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[BusStatusLog_INSERT]
   ON [dbo].[BusStatusLog]
   AFTER INSERT
AS 
	BEGIN			
		UPDATE [BusStatusLog]
		SET [IgnitionOn] = (SELECT TOP 1 [IgnitionOn] 
							FROM [MermaidTempDB].[dbo].[SiriusStatus] 
							WHERE [BusNumber]=INSERTED.[BusNumber]
							AND [Timestamp] BETWEEN DATEADD(minute, -10, INSERTED.[Timestamp]) 
							AND DATEADD(minute, 10, INSERTED.[Timestamp])
							)
		FROM [BusStatusLog]
			 INNER JOIN INSERTED
			 ON [BusStatusLog].[BusNumber]=INSERTED.[BusNumber] AND [BusStatusLog].[Timestamp]=INSERTED.[Timestamp]
	END