﻿CREATE TABLE [dbo].[SpecialDestinations] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]         INT           NULL,
    [Line]               VARCHAR (50)  NULL,
    [Destination]        VARCHAR (500) NULL,
    [SelectionStructure] VARCHAR (500) NULL,
    [Priority]           INT           CONSTRAINT [DF_SpecialDestinations_Priority] DEFAULT ((0)) NULL,
    [Excluded]           BIT           CONSTRAINT [DF_SpecialDestinations_Excluded] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_SpecialDestinations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

