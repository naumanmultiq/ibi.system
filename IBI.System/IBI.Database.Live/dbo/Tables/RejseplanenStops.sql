﻿CREATE TABLE [dbo].[RejseplanenStops] (
    [GID]                     DECIMAL (19) NOT NULL,
    [CheckPoint]              BIT          NOT NULL,
    [RejseplanenStopNumber]   VARCHAR (50) NULL,
    [LastTimeToShow]          INT          CONSTRAINT [DF_RejseplanenStops_LastTimeToShow] DEFAULT ((30)) NULL,
    [ExcludeLowPriorityMedia] BIT          CONSTRAINT [DF_RejseplanenStops_ExdlueLowPriorityMedia] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_RejseplanenStops] PRIMARY KEY CLUSTERED ([GID] ASC),
    CONSTRAINT [FK_RejseplanenStops_Stops] FOREIGN KEY ([GID]) REFERENCES [dbo].[Stops] ([GID]) ON DELETE CASCADE ON UPDATE CASCADE
);

