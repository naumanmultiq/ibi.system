﻿CREATE TABLE [dbo].[JourneyStops] (
    [JourneyId]            INT               NOT NULL,
    [StopSequence]         INT               NOT NULL,
    [StopId]               DECIMAL (19)      NOT NULL,
    [StopName]             NVARCHAR (MAX)    COLLATE Danish_Norwegian_CI_AS NOT NULL,
    [StopGPS]              [sys].[geography] NOT NULL,
    [Zone]                 NVARCHAR (MAX)    COLLATE Danish_Norwegian_CI_AS NULL,
    [PlannedArrivalTime]   DATETIME          NULL,
    [PlannedDepartureTime] DATETIME          NULL,
    [ActualArrivalTime]    DATETIME          NULL,
    [ActualDepartureTime]  DATETIME          NULL,
    [JourneyBehind]        INT               NULL,
    [JourneyAhead]         INT               NULL,
    CONSTRAINT [PK_JourneyStops] PRIMARY KEY CLUSTERED ([JourneyId] ASC, [StopSequence] ASC),
    CONSTRAINT [FK_JourneyStops_Journeys] FOREIGN KEY ([JourneyId]) REFERENCES [dbo].[Journeys] ([JourneyId]),
    CONSTRAINT [FK_JourneyStops_JourneysAhead] FOREIGN KEY ([JourneyAhead]) REFERENCES [dbo].[Journeys] ([JourneyId]),
    CONSTRAINT [FK_JourneyStops_JourneysBehind] FOREIGN KEY ([JourneyBehind]) REFERENCES [dbo].[Journeys] ([JourneyId])
);

