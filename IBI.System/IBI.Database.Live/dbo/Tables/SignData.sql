﻿CREATE TABLE [dbo].[SignData] (
    [SqlId]          INT          IDENTITY (1, 1) NOT NULL,
    [LayoutId]       INT          NULL,
    [SignId]         INT          NULL,
    [CustomerId]     INT          NULL,
    [Page]           INT          NULL,
    [Picture]        TEXT         NULL,
    [MD5]            VARCHAR (50) NULL,
    [GraphicsRuleId] INT          NULL,
    CONSTRAINT [PK_SignData] PRIMARY KEY CLUSTERED ([SqlId] ASC),
    CONSTRAINT [FK_SignData_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]),
    CONSTRAINT [FK_SignData_SignGraphicsRules] FOREIGN KEY ([GraphicsRuleId]) REFERENCES [dbo].[SignGraphicsRules] ([GraphicsRuleId]),
    CONSTRAINT [FK_SignData_SignItems] FOREIGN KEY ([SignId]) REFERENCES [dbo].[SignItems] ([SignId]) ON DELETE CASCADE
);

