﻿CREATE TABLE [dbo].[Pages] (
    [PageId]       INT           IDENTITY (0, 1) NOT NULL,
    [ParentPageId] INT           NULL,
    [Path]         VARCHAR (255) NULL,
    [Name]         VARCHAR (100) NULL,
    [MenuInitials] VARCHAR (50)  NULL,
    [Controller]   VARCHAR (50)  NULL,
    [Action]       VARCHAR (50)  NULL,
    [ShowMenu]     BIT           CONSTRAINT [DF_Pages_ShowMenu] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Pages] PRIMARY KEY CLUSTERED ([PageId] ASC),
    CONSTRAINT [FK_ParentPage_ChildPages] FOREIGN KEY ([ParentPageId]) REFERENCES [dbo].[Pages] ([PageId])
);

