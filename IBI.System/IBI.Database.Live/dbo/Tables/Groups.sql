﻿CREATE TABLE [dbo].[Groups] (
    [GroupId]       INT           IDENTITY (1, 1) NOT NULL,
    [GroupName]     VARCHAR (100) NULL,
    [ParentGroupId] INT           NULL,
    [CustomerId]    INT           NULL,
    [Description]   TEXT          NULL,
    [DateAdded]     DATETIME      CONSTRAINT [DF_Groups_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified]  DATETIME      CONSTRAINT [DF_Groups_DateModified] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED ([GroupId] ASC),
    CONSTRAINT [FK_Groups_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]),
    CONSTRAINT [FK_Groups_Groups] FOREIGN KEY ([ParentGroupId]) REFERENCES [dbo].[Groups] ([GroupId])
);

