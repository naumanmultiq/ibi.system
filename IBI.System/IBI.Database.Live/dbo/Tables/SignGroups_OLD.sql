﻿CREATE TABLE [dbo].[SignGroups_OLD] (
    [GroupId]       INT           IDENTITY (1, 1) NOT NULL,
    [GroupName]     VARCHAR (100) NOT NULL,
    [ParentGroupId] INT           NULL,
    [CustomerId]    INT           NOT NULL,
    [SortValue]     INT           NOT NULL,
    [Excluded]      BIT           NOT NULL,
    [DateAdded]     DATETIME      NULL,
    [DateModified]  DATETIME      NULL
);

