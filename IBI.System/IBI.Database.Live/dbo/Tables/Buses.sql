﻿CREATE TABLE [dbo].[Buses] (
    [BusNumber]             VARCHAR (50)      NOT NULL,
    [CustomerId]            INT               NOT NULL,
    [Description]           TEXT              NULL,
    [InOperation]           BIT               CONSTRAINT [DF_Buses_InOperation] DEFAULT ((0)) NULL,
    [CurrentJourneyNumber]  BIGINT            NULL,
    [ScheduleJourneyNumber] BIGINT            NULL,
    [LastTimeInService]     DATETIME          NULL,
    [LastKnownLocation]     [sys].[geography] NULL,
    [DateAdded]             DATETIME          CONSTRAINT [DF_Buses_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified]          DATETIME          CONSTRAINT [DF_Buses_DateModified] DEFAULT (getdate()) NULL,
    [ServiceLevel]          INT               NULL,
    [ServiceDetail]         VARCHAR (500)     NULL,
    CONSTRAINT [PK_Buses] PRIMARY KEY CLUSTERED ([BusNumber] ASC, [CustomerId] ASC),
    CONSTRAINT [FK_Buses_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

