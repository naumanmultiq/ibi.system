﻿CREATE TABLE [dbo].[Journeys_OLD] (
    [JourneyNumber]        INT           IDENTITY (227522, 1) NOT NULL,
    [BusNumber]            VARCHAR (50)  NOT NULL,
    [CustomerId]           INT           NOT NULL,
    [ScheduleNumber]       INT           NOT NULL,
    [JourneyDate]          DATETIME      CONSTRAINT [DF_Journeys_JourneyDate] DEFAULT (getdate()) NULL,
    [StartTime]            DATETIME      NULL,
    [EndTime]              DATETIME      NULL,
    [Line]                 VARCHAR (100) NULL,
    [StartPoint]           VARCHAR (100) NULL,
    [Destination]          VARCHAR (100) NULL,
    [PlannedArrivalTime]   DATETIME      NULL,
    [PlannedDepartureTime] DATETIME      NULL,
    [JourneyData]          XML           NULL,
    [JourneyAhead]         INT           NULL,
    [JourneyBehind]        INT           NULL,
    [IsLastStop]           BIT           CONSTRAINT [DF_Journeys_IsLastStop] DEFAULT ((0)) NOT NULL,
    [ExternalReference]    VARCHAR (100) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Journeys_CustomerId]
    ON [dbo].[Journeys_OLD]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Journeys_History_ScheduleNumber]
    ON [dbo].[Journeys_OLD]([ScheduleNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Journeys_IsLastStop]
    ON [dbo].[Journeys_OLD]([IsLastStop] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Journeys_JourneyDate]
    ON [dbo].[Journeys_OLD]([JourneyDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Journeys_LineDestination]
    ON [dbo].[Journeys_OLD]([Line] ASC, [Destination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Journeys_ScheduleNumber]
    ON [dbo].[Journeys_OLD]([ScheduleNumber] ASC);

