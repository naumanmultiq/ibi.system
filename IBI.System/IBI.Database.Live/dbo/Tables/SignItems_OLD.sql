﻿CREATE TABLE [dbo].[SignItems_OLD] (
    [SignId]       INT            IDENTITY (1, 1) NOT NULL,
    [CustomerId]   INT            NOT NULL,
    [GroupId]      INT            NULL,
    [ScheduleId]   INT            NULL,
    [Line]         VARCHAR (50)   NULL,
    [MainText]     VARCHAR (255)  NULL,
    [SubText]      VARCHAR (1000) NULL,
    [Name]         VARCHAR (255)  NULL,
    [SortValue]    INT            NOT NULL,
    [Excluded]     BIT            NOT NULL,
    [DateAdded]    DATETIME       NOT NULL,
    [DateModified] DATETIME       NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'destination', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignItems_OLD', @level2type = N'COLUMN', @level2name = N'MainText';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'via', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignItems_OLD', @level2type = N'COLUMN', @level2name = N'SubText';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'destination', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignItems_OLD', @level2type = N'COLUMN', @level2name = N'Name';

