﻿CREATE TABLE [dbo].[JourneyStops_OLD] (
    [Id]               INT           IDENTITY (457137, 1) NOT NULL,
    [JourneyNumber]    INT           NULL,
    [StopName]         VARCHAR (100) NULL,
    [Timestamp]        DATETIME      CONSTRAINT [DF_JourneyStops_Timestamp] DEFAULT (getdate()) NULL,
    [AheadData]        XML           NULL,
    [BehindData]       XML           NULL,
    [StopData]         XML           NULL,
    [IsLastStop]       BIT           CONSTRAINT [DF_JourneyStops_IsLastStop] DEFAULT ((0)) NOT NULL,
    [PlanDifference]   INT           CONSTRAINT [DF_JourneyStops_PlanDifference] DEFAULT ((0)) NULL,
    [Status]           VARCHAR (255) NULL,
    [Error]            TEXT          NULL,
    [ActualArrival]    DATETIME      NULL,
    [ActualDeparture]  DATETIME      NULL,
    [PlannedArrival]   DATETIME      NULL,
    [PlannedDeparture] DATETIME      NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_JourneyStops_ActualArrival]
    ON [dbo].[JourneyStops_OLD]([ActualArrival] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_JourneyStops_ActualDeparture]
    ON [dbo].[JourneyStops_OLD]([ActualDeparture] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_JourneyStops_IsLastStop]
    ON [dbo].[JourneyStops_OLD]([IsLastStop] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_JourneyStops_Status]
    ON [dbo].[JourneyStops_OLD]([Status] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_JourneyStops_StopName]
    ON [dbo].[JourneyStops_OLD]([StopName] ASC);

