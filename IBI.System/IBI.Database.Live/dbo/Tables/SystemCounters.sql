﻿CREATE TABLE [dbo].[SystemCounters] (
    [Name]  VARCHAR (500) NOT NULL,
    [Value] INT           NOT NULL,
    CONSTRAINT [PK_SystemCounters] PRIMARY KEY CLUSTERED ([Name] ASC)
);

