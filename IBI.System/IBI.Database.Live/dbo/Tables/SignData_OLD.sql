﻿CREATE TABLE [dbo].[SignData_OLD] (
    [SqlId]          INT          IDENTITY (1, 1) NOT NULL,
    [LayoutId]       INT          NULL,
    [SignId]         INT          NULL,
    [CustomerId]     INT          NULL,
    [Page]           INT          NULL,
    [Picture]        TEXT         NULL,
    [MD5]            VARCHAR (50) NULL,
    [GraphicsRuleId] INT          NULL
);

