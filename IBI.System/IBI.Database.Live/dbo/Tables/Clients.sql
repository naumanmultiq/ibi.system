﻿CREATE TABLE [dbo].[Clients] (
    [ClientId]         INT          IDENTITY (1, 1) NOT NULL,
    [BusNumber]        VARCHAR (50) NOT NULL,
    [ClientType]       VARCHAR (50) NOT NULL,
    [CustomerId]       INT          NOT NULL,
    [MacAddress]       VARCHAR (50) NULL,
    [LastPing]         DATETIME     NOT NULL,
    [SimID]            VARCHAR (50) NULL,
    [MarkedForService] BIT          NULL,
    CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED ([ClientId] ASC),
    CONSTRAINT [FK_Clients_Buses] FOREIGN KEY ([BusNumber], [CustomerId]) REFERENCES [dbo].[Buses] ([BusNumber], [CustomerId]) ON DELETE CASCADE ON UPDATE CASCADE
);

