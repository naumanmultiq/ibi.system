﻿CREATE TABLE [dbo].[RouteDirections] (
    [RouteDirectionId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Route] PRIMARY KEY CLUSTERED ([RouteDirectionId] ASC)
);

