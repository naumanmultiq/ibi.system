﻿CREATE PROC [dbo].[FindGeoPointInZones]
@lat varchar(20),
@lon varchar(20)
AS
	BEGIN
				
	DECLARE @h geography;
	 SET @h = geography::Point(@lat, @lon, 4326);
 
	--DECLARE @tbl TABLE(
	--	ZoneId int,
	--	Timestamp datetime, 
	--	ZoneName varchar(50),
	--	Description varchar(max),
	--	Operator varchar(50),
	--	GEO geography,
	--	KML xml)
	
	--INSERT INTO @tbl
	 
		SELECT z.ZoneId,z.Timestamp, z.ZoneName, z.Description, z.Operator, GEO, z.KML
		FROM Zones z
		WHERE @h.STIntersects(z.GEO) <> 0 
	 
	-- select 'jkhurram' as test
	 --SELECT ZoneId,zTimestamp, z.ZoneName, z.Description, z.Operator, z.KML FROM @tbl
	  
	END
