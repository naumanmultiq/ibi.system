﻿ 
 
 CREATE PROC [dbo].[A_MissingStopOccurances]
	@ScheduleNumber int
 AS

BEGIN

	DECLARE @jXmlData xml
	SET @jXmlData = (SELECT ScheduleXml FROM Schedules WHERE ScheduleID=@ScheduleNumber)


	DECLARE cur CURSOR FOR
			SELECT --JData.Stops.value('(StopName/text())[1]', 'varchar(100)') StopName,
				DISTINCT len(JData.Stops.value('(StopName/text())[1]', 'varchar(100)')) StopLength
			FROM 
				
				@jXmlData.nodes('/Schedule/JourneyStops/StopInfo') AS JData(Stops)
				order by StopLength DESC
			
			
		DECLARE @StopName varchar(100)
		DECLARE @StopLength int
		
		 declare @T table
			(
			  StopName varchar(100),
			  Occurance int		 
			)
		
		OPEN cur
				FETCH NEXT FROM cur INTO 
					@StopLength
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						-- Check stop data
						 INSERT INTO @t
						select top 500 substring(T.MissedStopsXml, N.Number, @StopLength+2) as Word,count(distinct JourneyNumber) as counter
						from [diagnosis_table] as T
						  cross apply (select N.Number
									   from master..spt_values as N
									   where N.type = 'P' and
											 N.number between 1 and len(T.MissedStopsXml)-@StopLength+1) as N
						                           
						WHERE  substring(T.MissedStopsXml, N.Number, @StopLength+2) LIKE '<%>' and 
						not substring(T.MissedStopsXml, N.Number, @StopLength+2) LIKE '%><%'    
						group by substring(T.MissedStopsXml, N.Number, @StopLength+2)                      
						order by count(distinct JourneyNumber) desc
						--END

					
						FETCH NEXT FROM cur INTO 
							@StopLength		
					END
			CLOSE cur   
			DEALLOCATE cur

			


	select * from @t

END