﻿-- =============================================
-- Author:		nak
-- Created:		14 April 2013
-- =============================================
create PROCEDURE [dbo].[UpdateJourneyStop] 
	@journeyNumber int,
	@journeyAhead int,
	@journeyBehind int,
	@stopData varchar(1000) 
AS
BEGIN
DECLARE @journeyData XML
DECLARE @aheadStopData XML
DECLARE @behindStopData XML

SET @journeyData = (SELECT JourneyData FROM Journey_JourneyDetail WHERE JourneyNumber=@journeyNumber and StopName=@stopData)
IF @journeyAhead != 0
BEGIN
	SET @aheadStopData = (SELECT StopData FROM Journey_JourneyDetail WHERE JourneyNumber=@journeyAhead and StopName=@stopData)
	SET @journeyData.modify('insert <Ahead></Ahead> as last into (/Journey/JourneyStops/StopInfo[StopName = sql:variable("@stopData")])[1]')
	SET @journeyData.modify('insert sql:variable("@aheadStopData") as last into (/Journey/JourneyStops/StopInfo[StopName = sql:variable("@stopData")]/Ahead)[1]')
END
IF @journeyBehind != 0
BEGIN
	SET @behindStopData = (SELECT StopData FROM Journey_JourneyDetail WHERE JourneyNumber=@journeyBehind and StopName=@stopData)
	SET @journeyData.modify('insert <Behind></Behind> as last into (/Journey/JourneyStops/StopInfo[StopName = sql:variable("@stopData")])[1]')
	SET @journeyData.modify('insert sql:variable("@behindStopData") as last into (/Journey/JourneyStops/StopInfo[StopName = sql:variable("@stopData")]/Behind)[1]')
END
UPDATE Journey_JourneyDetail SET JourneyData = @journeyData WHERE JourneyNumber=@journeyNumber and StopName=@stopData
UPDATE Journey_Journeys SET JourneyData = @journeyData WHERE JourneyNumber=@journeyNumber
SELECT @journeyData
END
