﻿CREATE PROC [dbo].[xml_SubmitJourney]
 @JourneyData text
AS

BEGIN

DECLARE @JourneyXML xml
SET @JourneyXML = @JourneyData

/*

	SELECT  	
		@JourneyId,
			
		JData.Stops.value('@StopNumber', 'varchar(max)') as StopId,
		IsNUll(JData.Stops.value('(StopName/text())[1]', 'varchar(100)'), '') StopName,		
		geography::Point(JData.Stops.value('(GPSCoordinateNS/text())[1]', 'varchar(100)'), JData.Stops.value('(GPSCoordinateEW/text())[1]', 'varchar(100)'), 4326),
		JData.Stops.value('(Zone/text())[1]', 'varchar(100)') Zone,
		CASE JData.Stops.value('(IsCheckpoint/text())[1]', 'varchar(100)') WHEN 'true' THEN 1 ELSE 0 END AS IsCheckpoint,
		JData.Stops.value('(PlannedArrivalTime/text())[1]', 'datetime') PlannedArrivalTime,
		JData.Stops.value('(PlannedDepartureTime/text())[1]', 'datetime') PlannedDepartureTime,
		JData.Stops.value('(ActualArrivalTime/text())[1]', 'datetime') ActualArrivalTime,
		JData.Stops.value('(ActualDepartureTime/text())[1]', 'datetime') ActualDepartureTime,
		null, --ahead journey
		null  --behind journey
		
		FROM 
			@JourneyXML.nodes('/Journey/JourneyStops/StopInfo') AS JData(Stops)

*/
END

 