﻿create PROCEDURE [dbo].[SaveAudioEntry] (@text varChar(255), @sourcetext varChar(255), @sourcevoice varChar(255), @voice varChar(255), @filename varChar(255), @filehash varChar(55), @approved bit, @rejected bit, @version int, @filecontent Image)
AS
IF NOT EXISTS(SELECT [Text] FROM [AudioFiles] WHERE [Text] like @text AND [Voice] like @voice)
BEGIN
  INSERT INTO [AudioFiles]([LastModified], [Text], [SourceText], [SourceVoice], [Voice], [FileName], [FileHash], [Approved], [Rejected], [Version], [FileContent]) VALUES(GETDATE(), @text, @sourcetext, @sourcevoice, @voice, @filename, @filehash, @approved, @rejected, @version, @filecontent)
END
ELSE
BEGIN
  UPDATE [AudioFiles] SET [LastModified]=GETDATE(), [FileName]=@filename, [FileHash]=@filehash, [Approved]=@approved, [Rejected]=@rejected, [Version]=@version, [FileContent]=@filecontent, [SourceText] = @sourcetext, [SourceVoice] = @sourcevoice WHERE [Text] like @text AND [Voice] like @voice
END
