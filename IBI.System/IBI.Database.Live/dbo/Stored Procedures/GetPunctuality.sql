﻿-- EXEC GetPunctuality @Line = '6A', @BusNumber = 1103 , @JourneyDate = '01/29/2014'
  
CREATE PROC [dbo].[GetPunctuality]
	@Line varchar(50)='',
	@BusNumber varchar(50)='',
	@JourneyNumber varchar(50)='',
	@JourneyDate varchar(20)='',
	@From varchar(50)='',
	@To varchar(50)='',
	@StopName varchar(50)='',
	@Planned varchar(10)='',
	@ActualArrival varchar(10)='',
	@ActualDeparture varchar(10)='',
	@PlanDifference varchar(10)='',
	@Status varchar(10) = ''-- by default all

AS
	BEGIN
		  
		DECLARE @jdate  datetime
	
		SET @jdate = CONVERT(datetime, @JourneyDate, 102)
		 
		SELECT 
		j.BusNumber,
		s.Line,
		j.CustomerID,
		j.StartTime,
		s.FromName,
		s.Destination,  
		j.JourneyId,
		--C.value('(JourneyNumber)[1]','varchar(100)') AS JourneyNumber,
		js.StopName AS StopName, 
		js.PlannedDepartureTime AS PlannedDepartureTime,
		js.ActualArrivalTime AS ArrivalTime,		
		js.ActualDepartureTime AS DepartureTime,	
		NULL AS Latitude,
		NULL AS Longitude,
		NULL as Zone,
		DATEDIFF(minute, js.PlannedDepartureTime, js.ActualDeparturetime) AS PlanDifference,
		'' AS Status
		FROM Journeys j 
			INNER JOIN Schedules s ON s.ScheduleId = j.ScheduleId
			INNER JOIN JourneyStops js ON j.JourneyId = js.JourneyId
		WHERE 
			 (@Line = '' OR s.Line = @Line) AND
			 (@BusNumber = '' OR j.BusNumber = @BusNumber) AND
			 (@JourneyNumber = '' OR j.JourneyId = @JourneyNumber) AND
			 (@JourneyDate='' OR (j.StartTime BETWEEN @jdate AND DateAdd(minute, 1439, @jdate))) AND
			 (@From='' OR s.FromName = @From) AND
			 (@To='' OR s.Destination = @To) AND
			 (@StopName='' OR js.StopName = @StopName) AND
			 (@Planned='' OR Substring(CONVERT(varchar, js.PlannedDepartureTime, 14), 1, 5) = @Planned) AND
			 (@ActualArrival='' OR Substring(CONVERT(varchar, js.ActualArrivalTime, 14), 1, 5) = @ActualArrival) AND
			 (@ActualDeparture='' OR Substring(CONVERT(varchar, js.ActualDepartureTime, 14), 1, 5) = @ActualDeparture) 
		 
		ORDER BY j.StartTime DESC, js.PlannedDepartureTime DESC
		
		 
	END
