﻿-- =============================================
-- Author:  NAK
-- Create date: 21 Feb 2013
-- Description: It returns list of all stop names that have no audio file
-- =============================================
CREATE PROCEDURE [dbo].[GetStopName](
@customerId int,
@stopNumber varchar(50)
) 
 
AS
BEGIN
 WITH allstops([StopName], [StopNumber])
AS
(
 SELECT DISTINCT TOP 1
  StopInfo.data.value('(StopName/text())[1]', 'varchar(260)') AS StopName,
  StopInfo.data.value('@StopNumber', 'varchar(260)') AS StopNumber
 FROM [dbo].[Schedules] schedules
  CROSS APPLY [ScheduleXML].nodes('/Schedule/JourneyStops/StopInfo') AS StopInfo(data) 
 WHERE StopInfo.data.value('(StopName/text())[1]', 'varchar(260)') IS NOT NULL
  AND StopInfo.data.value('@StopNumber', 'varchar(260)') = @stopNumber
  AND CustomerID = @customerId
) 

SELECT [StopName] 
FROM allstops 
END
 
