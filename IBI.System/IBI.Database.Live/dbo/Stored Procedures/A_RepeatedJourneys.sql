﻿-- =============================================
-- Author:		nak
-- Description:	Find invalid journeys
-- =============================================
CREATE PROCEDURE [dbo].[A_RepeatedJourneys] 
	@customerID int,
	@minuteDiff int,
	@fromdate datetime,
	@todate datetime,
	@bno varchar(100) = NULL	
AS
BEGIN
	DECLARE @jtable table
	(
		JourneyNumber int,
		BusNumber varchar(100),
		StartTime datetime, 
		ScheduleNumber int,
		EndTime datetime
		--,JourneyData xml
	)
	DECLARE @tmpBusNumber varchar(100)
	DECLARE @JourneyNumber int
	DECLARE @BusNumber varchar(100)
	DECLARE @StartTime datetime
	DECLARE @ScheduleNumber int
	DECLARE @EndTime datetime
	DECLARE @JourneyData xml
	
	SET @tmpBusNumber = NULL
	DECLARE jcursor CURSOR FOR
		--SELECT JourneyNumber,BusNumber,StartTime, ScheduleNumber, EndTime,JourneyData FROM Journeys WHERE CustomerID=@customerID AND (@bno IS NULL OR BusNumber=@bno) AND StartTime >= @fromdate AND StartTime <= @todate ORDER BY CustomerID,BusNumber
		SELECT JourneyNumber,BusNumber,StartTime, ScheduleNumber, EndTime FROM Journeys_History WHERE CustomerID=@customerID AND (@bno IS NULL OR BusNumber=@bno) AND StartTime >= @fromdate AND StartTime <= @todate ORDER BY  CustomerID,BusNumber,StartTime desc
	OPEN jcursor
		--FETCH NEXT FROM jcursor INTO @JourneyNumber,@BusNumber,@StartTime,@ScheduleNumber, @EndTime,@JourneyData
		FETCH NEXT FROM jcursor INTO @JourneyNumber,@BusNumber,@StartTime,@ScheduleNumber, @EndTime
		WHILE @@FETCH_STATUS = 0   
		BEGIN
			PRINT 'INSERTING....'
			INSERT INTO @jtable
			--SELECT @JourneyNumber,@BusNumber,@StartTime,@ScheduleNumber, @EndTime,@JourneyData
			SELECT @JourneyNumber,@BusNumber,@StartTime,@ScheduleNumber, @EndTime
			IF @tmpBusNumber IS NOT NULL AND @tmpBusNumber != @BusNumber
			BEGIN
				PRINT 'PROCESSING....'
				PRINT @tmpBusNumber;
				PRINT @BusNumber;
				WITH StartDates AS (
				SELECT 
					ROW_NUMBER() OVER (ORDER BY StartTime DESC) AS RowNumber,
					--StartTime, JourneyNumber, BusNumber, ScheduleNumber, EndTime, JourneyData
					StartTime, JourneyNumber, BusNumber, ScheduleNumber, EndTime
				FROM @jtable 
				)
				SELECT
				  --O1.JourneyNumber,O1.BusNumber, O1.ScheduleNumber, O1.StartTime, O1.EndTime,O1.JourneyData, DATEDIFF(MINUTE, O2.StartTime, O1.StartTime) AS AverageFrequency
				  O1.JourneyNumber,O1.BusNumber, O1.ScheduleNumber, O1.StartTime, O1.EndTime, DATEDIFF(MINUTE, O2.StartTime, O1.StartTime) AS Frequency
				FROM StartDates O1
				LEFT JOIN StartDates O2
					ON O2.RowNumber = O1.RowNumber + 1
					WHERE DATEDIFF(MINUTE, O2.StartTime, O1.StartTime) < @minuteDiff
					ORDER BY O1.JourneyNumber desc
				PRINT 'DELETING....'	
				DELETE FROM @jtable	
			END
			SET @tmpBusNumber = @BusNumber
			PRINT 'FETCHINH....'
			PRINT @tmpBusNumber
			--FETCH NEXT FROM jcursor INTO @JourneyNumber,@BusNumber,@StartTime,@ScheduleNumber, @EndTime,@JourneyData
			FETCH NEXT FROM jcursor INTO @JourneyNumber,@BusNumber,@StartTime,@ScheduleNumber, @EndTime
		END
			PRINT '...';
			WITH StartDates AS (
			SELECT 
				ROW_NUMBER() OVER (ORDER BY StartTime DESC) AS RowNumber,
				--StartTime, JourneyNumber, BusNumber, ScheduleNumber, EndTime, JourneyData
				StartTime, JourneyNumber, BusNumber, ScheduleNumber, EndTime
			FROM @jtable 
			)
			SELECT
			  --O1.JourneyNumber,O1.BusNumber, O1.ScheduleNumber, O1.StartTime, O1.EndTime,O1.JourneyData, DATEDIFF(MINUTE, O2.StartTime, O1.StartTime) AS AverageFrequency
			  O2.JourneyNumber,O2.BusNumber, O2.ScheduleNumber, O2.StartTime, O2.EndTime, DATEDIFF(MINUTE, O2.StartTime, O1.StartTime) AS Frequency
			FROM StartDates O1
			LEFT JOIN StartDates O2
				ON O2.RowNumber = O1.RowNumber + 1
				WHERE DATEDIFF(MINUTE, O2.StartTime, O1.StartTime) < @minuteDiff
				ORDER BY O1.JourneyNumber desc

	CLOSE jcursor
	DEALLOCATE jcursor		

END
