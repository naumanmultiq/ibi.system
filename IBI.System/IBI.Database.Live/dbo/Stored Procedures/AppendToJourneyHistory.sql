﻿CREATE PROC [dbo].[AppendToJourneyHistory]
			@BusNumber VARCHAR(50)
           ,@CustomerId INT
           ,@ScheduleNumber INT
           ,@JourneyDate DATETIME = NULL
           ,@StartTime DATETIME = NULL
           ,@EndTime DATETIME = NULL
           ,@Line VARCHAR(100) = NULL
           ,@StartPoint VARCHAR(100) = NULL
           ,@Destination VARCHAR(100) = NULL
           ,@PlannedArrivalTime DATETIME = NULL
           ,@PlannedDepartureTime DATETIME = NULL
           ,@JourneyData xml = NULL
           ,@JourneyAhead INT = NULL
           ,@JourneyBehind INT = NULL
           ,@IsLastStop BIT = 0
           ,@ExternalReference VARCHAR(100) = NULL
           ,@JourneyNumber INT

AS
	BEGIN

INSERT INTO [Journeys_History]
           ([BusNumber]
           ,[CustomerId]
           ,[ScheduleNumber]
           ,[JourneyDate]
           ,[StartTime]
           ,[EndTime]
           ,[Line]
           ,[StartPoint]
           ,[Destination]
           ,[PlannedArrivalTime]
           ,[PlannedDepartureTime]
           ,[JourneyData]
           ,[JourneyAhead]
           ,[JourneyBehind]
           ,[IsLastStop]
           ,[ExternalReference]
           ,[JourneyNumber])
     VALUES
           (@BusNumber
           ,@CustomerId
           ,@ScheduleNumber
           ,@JourneyDate
           ,@StartTime
           ,@EndTime
           ,@Line
           ,@StartPoint
           ,@Destination
           ,@PlannedArrivalTime
           ,@PlannedDepartureTime
           ,@JourneyData
           ,@JourneyAhead
           ,@JourneyBehind
           ,@IsLastStop
           ,@ExternalReference
           ,@JourneyNumber)
           
           
    END
