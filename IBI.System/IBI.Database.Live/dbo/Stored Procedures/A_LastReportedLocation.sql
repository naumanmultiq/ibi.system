﻿-- =============================================
-- Author:		NAK
-- =============================================
CREATE PROCEDURE A_LastReportedLocation 
AS
BEGIN
	DECLARE @journeyNumber int
	DECLARE @busNumber varchar(100)
	DECLARE @scheduleNumber int
	DECLARE @scheduleXml xml

	DECLARE @CompareTable table
	(
		journeyNo int,
		busnumber varchar(100),
		stopname varchar(100),
		actualarrival datetime,
		actualdeparture datetime,
		plannedarrival datetime,
		planneddeparture datetime,
		latitude varchar(30),
		longitude varchar(30)
	) 

		DECLARE commalist CURSOR FOR
			SELECT MAX(JourneyNumber) JourneyNumber, BusNumber FROM Journeys 
				WHERE JourneyNumber IN (SELECT JourneyNumber From JourneyStops)
				GROUP BY BusNumber
				OPEN commalist
				FETCH NEXT FROM commalist INTO @journeyNumber, @busNumber
				WHILE @@FETCH_STATUS = 0   
					BEGIN
						SET @scheduleNumber = (SELECT ScheduleNumber FROM Journeys WHERE JourneyNumber=@journeyNumber)
						SET @scheduleXml = (SELECT ScheduleXml FROM Schedules WHERE ScheduleId=@scheduleNumber)

						DECLARE @tmpActualArrival datetime
						DECLARE @tmpActualDeparture datetime
						DECLARE @tmpPlannedArrival datetime
						DECLARE @tmpPlannedDeparture datetime
						DECLARE @tmpStopName varchar(100)
						DECLARE @tmpStopData xml
						DECLARE @tmpLatitude varchar(50)
						DECLARE @tmoLongitude varchar(50)

						DECLARE lastknown CURSOR FOR
							SELECT TOP 1 ActualArrival, ActualDeparture, StopName, StopData, PlannedArrival, PlannedDeparture FROM JourneyStops 
							WHERE JourneyNumber = @journeyNumber
							ORDER BY ActualArrival
							OPEN lastknown
							FETCH NEXT FROM lastknown INTO @tmpActualArrival, @tmpActualDeparture,@tmpStopName,@tmpStopData,@tmpPlannedArrival,@tmpPlannedDeparture
							WHILE @@FETCH_STATUS = 0   
							BEGIN

							SELECT 
								@tmpLatitude = JData.Stops.value('(GPSCoordinateNS/text())[1]', 'varchar(50)'),
								@tmoLongitude = JData.Stops.value('(GPSCoordinateEW/text())[1]', 'varchar(50)')
							FROM 
								@scheduleXml.nodes('/Schedule/JourneyStops/StopInfo') AS JData(Stops)
							WHERE 
								JData.Stops.value('(StopName/text())[1]', 'varchar(50)') = @tmpStopName	

								INSERT INTO @CompareTable
									SELECT @journeyNumber,@busNumber,@tmpStopName,@tmpActualArrival, @tmpActualDeparture,@tmpPlannedArrival,@tmpPlannedDeparture,@tmpLatitude,@tmoLongitude
							FETCH NEXT FROM lastknown INTO @tmpActualArrival, @tmpActualDeparture,@tmpStopName,@tmpStopData,@tmpPlannedArrival,@tmpPlannedDeparture
							END
						CLOSE lastknown
						DEALLOCATE lastknown
					FETCH NEXT FROM commalist INTO @journeyNumber, @busNumber		
					END
		CLOSE commalist   
		DEALLOCATE commalist

		SELECT * FROM @CompareTable
END
