﻿-- =============================================
-- Author:		nak
-- =============================================
CREATE PROCEDURE [dbo].[LinkedJourneys] 
	@ScheduleNumber int,
	@CustomerId int,
	@JourneyNumber int,
	@BusNumber varchar(20),
	@Line varchar(100),
	@Destination varchar(100)
AS
BEGIN
	DECLARE @JourneyBehind int
	DECLARE @JourneyAhead int
		--To Close Previous Journey for this bus.

	update Journey_Journeys
		SET IsLastStop = 1
		WHERE BusNumber = @BusNumber
			AND JourneyNumber != @JourneyNumber
	
	SET @JourneyAhead = (SELECT TOP 1 [JourneyNumber] from [Journey_Journeys]
		 where BusNumber!=@BusNumber 
			AND Line = @Line
			AND Destination = @Destination
			--AND ScheduleNumber=@ScheduleNumber 
			and CustomerId=@CustomerId 
			and IsLastStop=0 
			and JourneyDate < (SELECT JourneyDate from [Journey_Journeys] where JourneyNumber=@JourneyNumber) order by JourneyDate desc)
	
	SET @JourneyBehind = (SELECT TOP 1 [JourneyNumber] from [Journey_Journeys]
	 where BusNumber!=@BusNumber 
		AND Line = @Line
		AND Destination = @Destination
		--AND ScheduleNumber=@ScheduleNumber 
		and CustomerId=@CustomerId 
		and IsLastStop=0 
		and JourneyDate > (SELECT JourneyDate from [Journey_Journeys] where JourneyNumber=@JourneyNumber) order by JourneyDate asc)



	select @JourneyBehind as JourneyBehind, @JourneyAhead as JourneyAhead
END
