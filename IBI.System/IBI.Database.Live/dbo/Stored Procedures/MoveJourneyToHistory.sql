﻿
CREATE PROC [dbo].[MoveJourneyToHistory]
	@journeyNumber int
AS
	BEGIN
	
	--SET XACT_ABORT ON;
	
	DECLARE @TranName VARCHAR(20);
	SELECT @TranName = 'JourneyHistoryTransaction';

	--EXEC xp_logevent 52000, 'MoveJourneyToHistory Started', informational

	
	BEGIN TRY
    
    
		BEGIN TRANSACTION @TranName;
	
		
		INSERT INTO [Journeys_History]
           ([BusNumber]
           ,[CustomerId]
           ,[ScheduleNumber]
           ,[JourneyDate]
           ,[StartTime]
           ,[EndTime]
           ,[Line]
           ,[StartPoint]
           ,[Destination]
           ,[PlannedArrivalTime]
           ,[PlannedDepartureTime]
           ,[JourneyData]
           ,[JourneyAhead]
           ,[JourneyBehind]
           ,[IsLastStop]
           ,[ExternalReference]
           ,[JourneyNumber])
			SELECT 		
			 [BusNumber]
           ,[CustomerId]
           ,[ScheduleNumber]
           ,[JourneyDate]
           ,[StartTime]
           ,[EndTime]
           ,[Line]
           ,[StartPoint]
           ,[Destination]
           ,[PlannedArrivalTime]
           ,[PlannedDepartureTime]
           ,[JourneyData]
           ,[JourneyAhead]
           ,[JourneyBehind]
           ,[IsLastStop]
           ,[ExternalReference]
           ,[JourneyNumber]  FROM Journeys Where JourneyNumber = @journeyNumber
			
		
	 INSERT INTO [JourneyStops_History]
           ([JourneyNumber]
           ,[StopName]
           ,[Timestamp]
           ,[AheadData]
           ,[BehindData]
           ,[StopData]
           ,[IsLastStop]
           ,[PlanDifference]
           ,[Status]
           ,[Error]
           ,[ActualArrival]
           ,[ActualDeparture]
           ,[PlannedArrival]
           ,[PlannedDeparture])
           SELECT 
            [JourneyNumber]
           ,[StopName]
           ,[Timestamp]
           ,[AheadData]
           ,[BehindData]
           ,[StopData]
           ,[IsLastStop]
           ,[PlanDifference]
           ,[Status]
           ,[Error]
           ,[ActualArrival]
           ,[ActualDeparture]
           ,[PlannedArrival]
           ,[PlannedDeparture] FROM JourneyStops WHERE JourneyNumber = @journeyNumber
      
      
      DELETE FROM Journeys WHERE JourneyNumber = @journeyNumber
      DELETE FROM JourneyStops  WHERE JourneyNumber = @journeyNumber
      
      COMMIT TRANSACTION @TranName;
            
      --EXEC xp_logevent 52000, 'MoveJourneyToHistory Successfully completed', informational
      
END TRY

BEGIN CATCH
	 

  --IF (XACT_STATE()) = -1
    --BEGIN
        -- The transaction is in an uncommittable state Rolling back transaction.'        
		ROLLBACK TRAN @TranName;
    --END;

    -- Test whether the transaction is committable.
    --IF (XACT_STATE()) = 1
    --BEGIN
        -- The transaction is committable. Committing transaction.'
        --COMMIT TRAN @TranName;   
    --END;
	 
		
	DECLARE @error varchar(max)
	--SET @error =  CAST(ISNULL(ERROR_MESSAGE(), 'Unknown Error') AS VARCHAR(max))   
	SELECT @error = 'MoveJourneyToHistory SP Failed [' + ISNULL(Error_Message(), 'undefined error') + ']'     
	PRINT @error
	EXEC xp_logevent 51000, @error, error
	
		
END CATCH

END


