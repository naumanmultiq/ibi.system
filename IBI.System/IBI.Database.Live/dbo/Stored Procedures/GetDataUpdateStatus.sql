﻿/*AUTHOR: KHI*/
CREATE PROC [dbo].[GetDataUpdateStatus]

@CustomerId int,
@BusNumber varchar(50)

AS

SELECT DataKey, BusNumber, Timestamp, Value 
FROM DataUpdateStatuses d
WHERE 
		 CustomerId = @CustomerId 
	AND (BusNumber = @BusNumber or BusNumber=0)
FOR XML AUTO
