﻿CREATE PROC [dbo].[ImportLiveDB]

AS
	BEGIN
	
		--ALTER DATABASE IBI_Data_2 SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		-- GO
		 
		--SYNC ZONES
		/*
		delete from Zones
		insert into Zones
		  select * from IBI_Data..Zones

		--SYNC STOPS
		delete from Stops
		insert into Stops
		  select * from IBI_Data..Stops

		--SYNC DESTINATIONS
		delete from Destinations
		insert into Destinations
		  select * from IBI_Data..Destinations
		
		--SYNC SCHEDULES
		delete from Schedules
		DBCC CHECKIDENT('Schedules', RESEED, 0)
		  INSERT INTO Schedules ([CustomerID],[Line],[FromName],[Destination],[ViaName],[StopCount],[Schedule],[Updated])
			SELECT [CustomerID],[Line],[FromName],[Destination],[ViaName],[StopCount],[Schedule],[Updated] FROM IBI_Data..Schedules
		*/  
		
		--SYNC CLIENTS 
		DELETE FROM Clients
		DBCC CHECKIDENT('Clients', RESEED, 0)

		DECLARE @tblClients TABLE (
			[BusNumber] [varchar](50) NOT NULL,
			[ClientType] [varchar](50) NOT NULL,
			[InOperation] [bit] NULL,
			[CustomerID] [int] NULL,
			[MacAddress] [varchar](50) NULL,
			[LastPing] [datetime] NOT NULL,
			[CurrentJourneyNumber] [bigint] NULL,
			[LastScheduleTime] [datetime] NULL,
			[SimID] [varchar](50) NULL,
			[Latitude] [varchar](50) NULL,
			[Longitude] [varchar](50) NULL,
			[ServiceLevel] [int] NULL,
			[MarkedForService] [bit] NULL
		 )
		 
		 INSERT INTO @tblClients
		   SELECT * FROM IBI_Data..Clients
		 
		  
		 --UPDATE BUSES
		 --Cursor
		DECLARE @busNumber int
		DECLARE @customerId int
		DECLARE @inOperation bit
		DECLARE @currentJourneyNumber bigint
		DECLARE @lastTimeInService datetime
		DECLARE @lastKnownLocation geography
		DECLARE @serviceLevel int
		DECLARE @lat varchar(20)
		DECLARE @lon varchar(20)

		DECLARE c CURSOR LOCAL FAST_FORWARD FOR
		   SELECT c.BusNumber, c.CustomerID, c.InOperation, c.CurrentJourneyNumber, c.LastScheduleTime,
				  c.Latitude, c.Longitude,
				  c.ServiceLevel
			FROM @tblClients c
		    
		OPEN c

		FETCH NEXT FROM c INTO @busNumber, @customerId, @inOperation, @currentJourneyNumber, @lastTimeInService, @lat, @lon, @serviceLevel
		WHILE @@FETCH_STATUS = 0
		 BEGIN

			SET @lastKnownLocation = null

			if(@lat IS NOT NULL AND @lon IS NOT NULL AND @lat!='' AND @lon!='')
				SET @lastKnownLocation= geography::STPointFromText('POINT(' + @lon + ' ' + @lat + ')', 4326)
			
			IF (NOT Exists(SELECT CustomerId FROM Customers WHERE CustomerId = @customerId))  
				INSERT into Customers (CustomerId) VALUES(@customerId)
				 
			
			
			IF (Exists(SELECT BusNumber FROM Buses WHERE BusNumber = @busNumber AND CustomerId = @customerId))  
				BEGIN
					--update bus record
					UPDATE Buses SET
						InOperation = @inOperation,
						CurrentJourneyNumber = @currentJourneyNumber,
						LastTimeInService = @lastTimeInService,
						LastKnownLocation = @lastKnownLocation,
						DateModified = GETDATE(),
						ServiceLevel = @serviceLevel
					WHERE BusNumber = @busNumber	
						
				END
			ELSE
				BEGIN
					--insert bus record
					INSERT INTO Buses (BusNumber, CustomerId, InOperation, CurrentJourneyNumber, LastTimeInService, LastKnownLocation, DateAdded, ServiceLevel)
						VALUES(@busNumber, @customerId, @inOperation, @currentJourneyNumber, @lastTimeInService, @lastKnownLocation, GETDATE(), @serviceLevel)
					 
				END
		 
		   FETCH NEXT FROM c INTO  @busNumber, @customerId, @inOperation, @currentJourneyNumber, @lastTimeInService, @lat, @lon, @serviceLevel
		 END

		CLOSE c
		DEALLOCATE c
		  
		 
		 --UPDATE CLIENTS
		 INSERT INTO Clients (BusNumber, ClientType, CustomerId, MacAddress, LastPing, SimID, MarkedForService)
		   SELECT c.BusNumber, c.ClientType, c.CustomerId, c.MacAddress, c.LastPing, c.SimID, c.MarkedForService
		   FROM @tblClients c
		   
		--GO
		 
		 
		--ALTER DATABASE IBI_Data_2 SET MULTI_USER
		--GO
	
	END
