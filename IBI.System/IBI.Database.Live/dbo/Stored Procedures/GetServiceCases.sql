﻿CREATE PROC [dbo].[GetServiceCases] 
	@AccountIds varchar(1000),
	@BusNumber varchar(10)
AS 
	BEGIN
	
		SELECT cases.[CASE_ID], levels.[DEPART_ID] AS [LEVEL_ID], levels.[DEPART_NAME] AS [LEVEL] 
		 ,customValues.[DEFINITION_REF], customValues.[UDF_VALUES_VARCHAR] as BusNumber, accounts.ACCOUNT_ID, cases.CASE_STATUS
 
            FROM 
              NovoSupport_Grandpa..[UDF_VALUES] customValues 
              INNER JOIN 
              NovoSupport_Grandpa..[CUSTOMERS] customers 
              ON customValues.[TABLE_REF]=customers.[CUSTOMER_ID] 
              INNER JOIN 
              NovoSupport_Grandpa..[ACCOUNTS] accounts 
              ON customers.[ACCOUNT_REF]=accounts.[ACCOUNT_ID] 
              INNER JOIN 
              NovoSupport_Grandpa..[CASES] cases 
              ON customValues.[TABLE_REF]=cases.[CUSTOMER_REF] 
              INNER JOIN 
              NovoSupport_Grandpa..[DEPARTMENTS] levels 
              ON cases.[DEPART_REF]=levels.[DEPART_ID] 
            WHERE customValues.[DEFINITION_REF]=2 
              AND customValues.[UDF_VALUES_VARCHAR]=@BusNumber 
              AND cases.[CASE_STATUS]<>8 
              AND (  accounts.[ACCOUNT_ID] IN (SELECT Value FROM dbo.ListToTable(@AccountIds)) )
              
    END
