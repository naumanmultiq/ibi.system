﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Mvc;




namespace IBI.Models.Messages
{
    [DataContract(Namespace="")] 
    public class IncomingMessage
    {
        [DataMember]
        [ScaffoldColumn(true)]
        [HiddenInput(DisplayValue = false)]
        public int MessageId { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public string BusNumber { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int CustomerId { get; set; }
                                                
        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public string CustomerName { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int? ClientId { get; set; }
         
        [DataMember]
        [DisplayName("Message Text")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Message text required")]
        public string MessageText { get; set; } 

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public DateTime? DateAdded { get; set; }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public DateTime? DateModified { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Position { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string Line { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Destination { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NextStop { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string MacAddress { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Latitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Longitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string GPS { get; set; }

        [DataMember]
        [DefaultValue(false)]
        public bool IsArchived { get; set; }

        [DataMember]
        public int BusGroupId { get; set; }

        [DataMember]
        public string BusGroupName { get; set; }

    }
}


