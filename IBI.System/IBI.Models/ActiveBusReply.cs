﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class ActiveBusReply
    {      
        [DataMember(Order = 0)]
        public String BusNo
        {
            get;
            set;
        }
    }
}