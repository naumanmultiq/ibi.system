﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace IBI.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class Schedule
    {
        private String busNumber;
        private String line;
        private String journeyNumber;
        private String fromName;
        private String destinationName;

        StopInfo[] journeyStops;

        [DataMember(Order = 0)]
        public String BusNumber
        {
            get { return busNumber; }
            set { busNumber = value; }
        }

        [DataMember(Order = 1)]
        public String Line
        {
            get { return line; }
            set { line = value; }
        }

        [DataMember(Order = 2)]
        public String JourneyNumber
        {
            get { return journeyNumber; }
            set { journeyNumber = value; }
        }

        [DataMember(Order = 3)]
        public String FromName
        {
            get { return fromName; }
            set { fromName = value; }
        }

        [DataMember(Order = 4)]
        public String DestinationName
        {
            get { return destinationName; }
            set { destinationName = value; }
        }

        [DataMember(IsRequired = true, Order = 5)]
        public String ViaName
        {
            get;
            set;
        }

        [DataMember(IsRequired = true, Order = 6)]
        public Boolean Cached
        {
            get;
            set;
        }
        
        [DataMember(Order = 7)]
        public StopInfo[] JourneyStops
        {
            get { return this.journeyStops; }
            set { this.journeyStops = value; }
        }
    }
}