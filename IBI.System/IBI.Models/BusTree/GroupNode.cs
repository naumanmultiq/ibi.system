using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IBI.Models.BusTree
{
    /// <summary>
    /// Tree Item acting as a bus group.
    /// </summary>c 
    [DataContract]      
    [KnownType(typeof(IBusNode))]
    public class GroupNode : BusNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeItem"/> class.
        /// </summary>
        public GroupNode()
            : base()
        {
        }

        /// <summary>
        /// only public constructor to instantiate a GroupNode item for the GUI Tree.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="GroupName"></param>
        /// <param name="children"></param>
        public GroupNode(Guid id, string groupName, int groupId, int customerID, List<BusNode> children) : base(id, groupName, customerID, children)
        {
            this.Type = 1;
            this.GroupId = groupId;
            isFolder = true;
            isLazy = true;
            GroupName = groupName;
        }

        /// <summary>
        /// Static function to Obtain a new Group Node
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public static GroupNode CreateGroup(string groupName, int groupId, int customerID, List<BusNode> children)
        {
            return new GroupNode(Guid.NewGuid(), groupName, groupId, customerID, children);
        } 
         
    }
}