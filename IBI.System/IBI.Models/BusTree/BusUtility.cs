﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.Models.BusTree
{
    public class BusUtility
    {
        public static string AppPath 
        {
            get 
            {
                if (HttpContext.Current != null)
                {
                    HttpRequest request = HttpContext.Current.Request;
                    string appPath = HttpContext.Current.Request.ApplicationPath;
                    appPath = appPath.Length == 1 ? appPath.TrimEnd('/') : appPath;
                    return appPath;
                }
                else 
                {
                    return "";                
                }
                
            }
        }
    }
}