﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Models.BusTree
{
    public class BusStatusLog
    {
        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public int BusNumber{ get; set; }

        [DataMember]
        public bool IsOK { get; set; }
        
        [DataMember]
        public DateTime? LastDCUPing { get; set; }

        [DataMember]
        public DateTime? LastVTCPing { get; set; }
        
        [DataMember]
        public DateTime? LastInfotainmentPing { get; set; }
        
        [DataMember]
        public DateTime? LastHotspotPing { get; set; }
        
        [DataMember]
        public DateTime? LastSchedule { get; set; }
        
        [DataMember]
        public int ScheduledJourneyNumber { get; set; }

        [DataMember]
        public int ActualJourneyNumber { get; set; }

        [DataMember]
        public string Latitude{ get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public bool MarkedForService { get; set; }

        [DataMember]
        public int ServiceLevel { get; set; }
    }
}
