﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Models.BusTree
{
    [DataContract]
    [KnownType(typeof(IBI.Models.BusTree.Geography))]
    public class Bus
    {
        [DataMember]
        public int BusNumber { get; set; }

        [DataMember]
        public int? CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool? InOperation { get; set; }

        [DataMember]
        public Int64? CurrentJourneyNumber { get; set; }

        [DataMember]
        public DateTime LastTimeInService { get; set; }

        [DataMember]
        public Geography LastKnownLocation { get; set; }

        [DataMember]
        public string DateAdded { get; set; }

        [DataMember]
        public string DateModified { get; set; }

        [DataMember]
        public int? ParentGroupId { get; set; }

        [DataMember]
        public string ParentGroupName { get; set; }

    }

    [DataContract]
    public class Geography{
        
        [DataMember]        
        public double Latitude{get; set;}

        [DataMember]
        public double Longitude{get; set;}

        public Geography() { }
        public Geography(double latitude, double longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
    }
}
