namespace IBI.Models.BusTree
{
    /// <summary>
    /// color representing the status of a Unit inside Bus
    /// </summary>
    public enum BusStatusColor
    {
        NONE = 0,
        GREEN = 1,
        LIGHT_GREEN = 2,
        RED = 3,
        YELLOW = 4,
        ORANGE = 5,
        GREY = 6,
        LIGHT_GREY = 7
    }

    public enum BusStatus 
    { 
        NONE = 0,
        GREEN = 1,
        LIGHT_GREEN = 2,
        RED = 3,        
        YELLOW = 4,
        ORANGE = 5,
        GREY = 6,
        LIGHT_GREY = 7
    }

    public enum MessageType
    { 
        LAND_TO_BUS = 1,
        BUS_TO_LAND = 2
    }

    public enum MessageStatus 
    {
        UNREAD = 1,
        READ = 2
    }

}