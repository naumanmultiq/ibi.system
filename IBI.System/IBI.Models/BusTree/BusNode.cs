using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json; 

namespace IBI.Models.BusTree
{
    /// <summary>
    /// The Leaf Tree Item, acting as a bus node
    /// </summary>
    [DataContract]
    [KnownType(typeof(GroupNode))]
    [KnownType(typeof(BusDetail))]
    [KnownType(typeof(BusUnitStatus))]    
    public class BusNode : IBusNode
    {
        private const string IMAGE_ROOT_PATH = "/images/bustree";

        //basic properties from interface --> restricted to keep naming convention bcoz of JQuery Tree internal naming structure.

        [JsonProperty]
        [DataMember(Order=1)]
        public string title { get; set; }


        [JsonProperty] 
        [DataMember(Order = 2)]
        public bool isFolder { get; set; }

        [JsonProperty]
        [DataMember(Order = 3)]
        public bool isLazy { get; set; }


        [JsonProperty]
        [DataMember(Order = 4)]
        public string key { get; set; }

        [JsonProperty]
        [DataMember(Order = 5)]
        public bool expand { get; set; }


        [JsonProperty]
        [DataMember(Order = 6)]
        public List<BusNode> children { get; set; }

        //additional properties

        [DataMember(Order = 7)]
        public int Type { get; set; }//1:group, 2:bus


        [JsonProperty]
        [DataMember(Order = 8)]
        public int CustomerID { get; set; }


        [JsonProperty]
        [DataMember(Order = 9)]
        public string GroupName { get; set; }

        [JsonProperty]
        [DataMember(Order = 10)]
        public int GroupId { get; set; }

        [JsonProperty]
        [DataMember(Order = 11)]
        public string BusNumber { get; set; }

        

        [DataMember]
        public string DCU_Status { get; set; }
        [DataMember]
        public string VTC_Status { get; set; }
        [DataMember]
        public string Hotspot_Status { get; set; }
        [DataMember]
        public string Infotainment_Status { get; set; }
        [DataMember]
        public string Bus_Status { get; set; }
        

        [DataMember]
        public BusDetail BusDetail { get; set; }

        [DataMember]
        public String CustomerName { get; set; }

        [DataMember]
        public String AlternateRowCss { get; set; }


        [DataMember]
        public String ClientInformation {get; set; }

        private static int rowNum;
        /// <summary>
        /// a default protected constructor
        /// </summary>
        public BusNode()
        {
            this.Type = 1;
            children = new List<BusNode>();
            this.BusNumber = "0";
            this.CustomerID = 0;
            this.isFolder = false;
            this.isLazy = false;
            this.key = "";
            this.title = "No Groups Found";
            this.expand = true;

            this.GroupId = 0;
            this.BusNumber = "0";
            this.BusDetail = new BusDetail();
            this.BusDetail.DCU.Description = "DCU";
            this.BusDetail.VTC.Description = "VTC";
            this.BusDetail.Hotspot.Description = "Hotspot";
            this.BusDetail.Infotainment.Description = "Infotainment";
            
            
        }

        /// <summary>
        /// Protected constructor::will only be called from derived class
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nodeTitle"></param>
        /// <param name="children"></param>
        protected BusNode(Guid id, string nodeTitle, int customerID, List<BusNode> children)
        {
            key = id.ToString();
            isFolder = false;
            isLazy = false;
                
            this.children = children;
            this.CustomerID = CustomerID;

            this.BusNumber = "0";
            this.BusDetail = new BusDetail(); 

            SetNode(nodeTitle);
        }
      
        /// <summary>
        /// the only public constructor to instantiate a new Bus node.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="busNumber"></param>
        /// <param name="busDetail"></param>
        /// <param name="children"></param>
        public BusNode(Guid id, string busNumber, BusDetail busDetail, List<BusNode> children)
        {
            key = id.ToString();
            isFolder = false;
            isLazy = false;
            this.children = children;

            this.BusNumber = busNumber;
            this.CustomerID = busDetail.CustomerID;
            this.CustomerName = busDetail.CustomerName;

            this.BusDetail = busDetail;

            SetNode(busNumber);
        }

        public void SetNode() {
            if (this.GetType() == typeof(GroupNode))
                SetNode(this.GroupName);
            else
                SetNode(this.BusNumber);
        }

        //this function needs to be changed if any requirements changes for Tree GUI.
        private void SetNode(string nodeTitle)
        {
            if (this.GetType() == typeof(GroupNode))
                this.Type = 1;
            else
                this.Type = 2;

            
            this.expand = true;

            //set alternate rowCSS

            if (this.Type == 2)
            {
                this.Bus_Status = this.BusDetail.BusStatus.ToString();

                this.AlternateRowCss = rowNum % 2 == 0 ? " grey" : " white";
                //increment rowNum
                rowNum++;
            }
            else { this.AlternateRowCss = ""; }



            if (this.BusDetail != null)
                this.title = @"<div class='" + GetStatusColorIcon(this.BusDetail.BusStatus) + "' title='Last Time In Service: " + this.BusDetail.LastTimeInService.ToString() + "' /></div>" + ((this.Type == 2) ? "&nbsp;" : "") + nodeTitle +
                             "~" + GetStatusImage(this.BusDetail.DCU) +                                         //DCU
                             "~" + GetStatusImage(this.BusDetail.VTC) +                                         //VTC    
                             "~" + GetStatusImage(this.BusDetail.Hotspot) +                                     //Hotspot
                             "~" + GetStatusImage(this.BusDetail.Infotainment) +                                //Infotainment
                             "~" + GetSnapImage(this.BusDetail.Infotainment.LastPing.ToShortDateString()) +     //Snap
                             "~" + GetStatusImage(this.BusDetail.InService) +                                   //IsInService
                             "~" + GetServiceCaseImage(this.BusDetail.IsServiceCase)                           //IsServiceCase 
                    
                             ;

            else
                this.title = @"" + nodeTitle +
                             "~" +  //DCU
                             "~" +  //VTC
                             "~" +  //Hotspot
                             "~" +  //Infotainment
                             "~" +  //Snap
                             "~" +  //IsInService
                             "~";   //IsServiceCase
                             
        }



        /// <summary>
        /// Gets the colored icon for tree status item
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private string GetStatusImage(BusUnitStatus status)
        {
          
            string img = "";

            switch (status.Color)
            {
                case BusStatusColor.GREEN:
                    img = "/green.png";
                    break;
                case BusStatusColor.LIGHT_GREEN:
                    img = "/light-green.png";
                    break;
                case BusStatusColor.GREY:
                    img = "/grey.png";
                    break;
                case BusStatusColor.LIGHT_GREY:
                    img = "/light-grey.png";
                    break;
                case BusStatusColor.RED:
                    img = "/red.png";
                    break;
                case BusStatusColor.YELLOW:
                    img = "/yellow.png";
                    break;
                case BusStatusColor.ORANGE:
                    img = "/orange.png";
                    break;
                default:
                    img = "/spacer.gif";
                    break;
            }

            if (img == "")
                return "";

            string tooltip = status.Description + " - " + status.LastPing;

            
            string imgPath = BusUtility.AppPath + IMAGE_ROOT_PATH + img;

            string retImg = @"<img title='" + tooltip +
                            "' src='" + imgPath +
                            "' style='vertical-align: middle; width: 12px; height: 12px;' />";
            return retImg;
        }

        private string GetStatusColorIcon(BusStatusColor color)
        {
            string cssClassName = "";

            switch (color)
            {
                case BusStatusColor.GREEN:
                    cssClassName = "BUS_GREEN";
                    break;
                case BusStatusColor.LIGHT_GREEN:
                    cssClassName = "BUS_LIGHT_GREEN";
                    break;
                case BusStatusColor.GREY:
                    cssClassName = "BUS_GREY";
                    break;
                case BusStatusColor.LIGHT_GREY:
                    cssClassName = "BUS_LIGHT_GREY";
                    break;
                case BusStatusColor.RED:
                    cssClassName = "BUS_RED";
                    break;
                case BusStatusColor.YELLOW:
                    cssClassName = "BUS_YELLOW";
                    break;
                case BusStatusColor.ORANGE:
                    cssClassName = "BUS_ORANGE";
                    break;
                default:
                    cssClassName = "BUS_NONE";
                    break;
            }
              
            return  cssClassName;
        }

        /// <summary>
        /// Gets the Icon for infotainment snap
        /// </summary>
        /// <param name="infoSnap"></param>
        /// <returns></returns>
        private string GetSnapImage(string infoSnap)
        {
            if (this.Type == 1)
                return "";


            string img = "/infosnap.png";
            string path =  BusUtility.AppPath + IMAGE_ROOT_PATH + img;
              
            if (infoSnap != "")
                return @"<img src='" + path + "' style='width: 15px; cursor: pointer; cursor: hand;' onclick='ShowScreenshots(" + this.BusNumber + ");' />";
            else
                return "";
        }

        /// <summary>
        /// get the icon for InService
        /// </summary>
        /// <param name="infoSnap"></param>
        /// <returns></returns>
        private string GetInServiceImage(bool isInService)
        {
            if (this.Type == 1)
                return "";
            
            string img = "/not-in-service.png";
            string path = BusUtility.AppPath + IMAGE_ROOT_PATH + img;

            if (!isInService)
                return "<img src='" + path + "' style='width: 16px;' />";
            else
                return "";
        }

        /// <summary>
        /// get the icon for ServiceCase
        /// </summary>
        /// <param name="infoSnap"></param>
        /// <returns></returns>
        private string GetServiceCaseImage(bool isServiceCase)
        {
            if (this.Type == 1)
                return "";

            string img = "/warning.png";
            string path = BusUtility.AppPath + IMAGE_ROOT_PATH + img;

            if (isServiceCase)
            {
                string[] tickets = this.BusDetail.ServiceDetail.Split('|');
                string htmlTickets = "";

                    foreach(string ticket in tickets){
                        string[] arrTicket = ticket.Split('~');
                        htmlTickets += string.Format(@" 
                                                    <tr>
                                                        <td align = 'right'>{0}</td>
                                                        <td align = 'left'>{1}</td>
                                                    </tr>", 
                                                          arrTicket[0]!=null ? arrTicket[0] + ": " : "",
                                                          arrTicket[1]!=null ? arrTicket[1] : ""
                                              );
                    }

                string html = string.Format(@"
                            <div class='bubbleInfo'>
                                 <img class='trigger' src='{0}' style='width: 16px;' />
                                 <div class='popup'>
                                   <table>                                        
                                        {1}
                                   </table>
                                </div>
                          </div>", path, htmlTickets);

                return html;
            }                
            else
                return "";
        }

       
        /// <summary>
        /// Static function to obtain a new new Bus Node.
        /// </summary>
        /// <param name="busNumber"></param>
        /// <param name="busDetail"></param>
        /// <returns></returns>
        public static BusNode CreateBusNode(string busNumber, BusDetail busDetail)
        {
            DateTime now = DateTime.Now;

            return new BusNode(Guid.NewGuid(), busNumber, busDetail, new List<BusNode>());
        }

        protected static void ResetCurrentTree(){
            rowNum = 0;
        }
    }
}