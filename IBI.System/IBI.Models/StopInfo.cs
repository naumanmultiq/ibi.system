﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Models
{
    [DataContract(Namespace="http://schemas.mermaid.dk/IBI")]
    public class StopInfo
    {
        private String stopNumber;
        private String stopName;
        private DateTime arrivalTime;
        private DateTime departureTime;
        private String gpsCoordinateNS;
        private String gpsCoordinateEW;
        private String zone;

        [XmlAttribute]
        [DataMember(Order = 0)]
        public String StopNumber
        {
            get { return stopNumber; }
            set { stopNumber = value; }
        }

        [DataMember(Order = 1)]
        public String StopName
        {
            get { return stopName; }
            set { stopName = value; }
        }

        [DataMember(Order = 2)]
        public DateTime ArrivalTime
        {
            get { return arrivalTime; }
            set { arrivalTime = value; }
        }

        [DataMember(Order = 3)]
        public DateTime DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }
        

        [DataMember(Order = 4)]
        public String GPSCoordinateNS
        {
            get { return gpsCoordinateNS; }
            set { gpsCoordinateNS = value; }
        }

        [DataMember(Order = 5)]
        public String GPSCoordinateEW
        {
            get { return gpsCoordinateEW; }
            set { gpsCoordinateEW = value; }
        }

        [DataMember(Order = 6)]
        public String Zone
        {
            get { return zone; }
            set { zone = value; }
        }
        
        [DataMember(Order = 7)]
        public Boolean IsCheckpoint
        {
            get;
            set;
        }

        [DataMember(Order = 8)]
        public DateTime PlannedDepartureTime { get; set; }

        [DataMember(Order = 9)]
        public DateTime AADT_ArrivalTime { get; set; }
        [DataMember(Order = 10)]
        public DateTime ADDT_DepartureTime { get; set; }

        [DataMember(Order = 11)]
        public DateTime LMDT_Time { get; set; }  
        

    }
}