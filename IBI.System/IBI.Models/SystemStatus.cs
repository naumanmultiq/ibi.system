﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class SystemStatus
    {
        [DataMember(Order = 0)]
        public Bus[] BusStatus
        {
            get;
            set;
        }
    }
}