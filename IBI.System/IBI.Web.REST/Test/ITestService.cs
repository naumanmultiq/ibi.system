﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IBI.REST.Test
{

    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    [XmlSerializerFormat] 
    public interface ITestService
    {
        [OperationContract]
        [WebGet]
        List<NAK_Q1_Result> NakQ1 (int customerId);

        [WebGet]
        [OperationContract]
        List<NAK_Q2_Result> NakQ2(int scheduleId);
    }
}
