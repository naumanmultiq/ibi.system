﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace IBI.REST.Test
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TestService : ITestService
    {

        public List<NAK_Q1_Result> NakQ1 (int customerId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<NAK_Q1_Result> data = dbContext.NAK_Q1(customerId).ToList();
                return data;
            }
        }


        public List<NAK_Q2_Result> NakQ2(int scheduleId)
        {

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<NAK_Q2_Result> data = dbContext.NAK_Q2(scheduleId).ToList();
                return data;
            }
        }
    }
}
