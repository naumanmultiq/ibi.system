﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using IBI.Shared.Models;
using IBI.DataAccess;
using IBI.Shared.Models.BusTree;
using System.ServiceModel.Web;
using IBI.Shared;
using System.Data;
using System.Data.SqlClient;
using IBI.Shared.Diagnostics;
using IBI.REST.Shared;
using IBI.DataAccess.DBModels;
using System.Globalization;
using System.IO;
using IBI.Shared.ConfigSections;

namespace IBI.REST.BusTree
{
    public class BusServiceController
    {

        #region Simple BusTree

        //function to get the Hierarcal Bus Tree
        //if inOperation == true then only those buses will be fetched which are InOperation, otherwise all the buses. By Default all the buses
        public static Tree CreateSimpleBusTree(int userId, int groupId = 0, int customerId = 0, bool inOperation = false)
        {
            try
            {
                List<BusNode> rootGroups = new List<BusNode>();

                IBIDataModel dbContext = new IBIDataModel();

                var uGroups = dbContext.Users.FirstOrDefault(x => x.UserId == userId).UserGroups;

                foreach (var uGroup in uGroups)
                {
                    var groups = groupId > 0 ? uGroup.Groups.Where(x => x.GroupId == groupId && (x.CustomerId == customerId || customerId == 0)) : uGroup.Groups.Where(g => g.CustomerId == customerId || customerId == 0);

                    foreach (var group in groups)
                    {
                        GroupNode gNode = SetSimpleGroupNode(group, inOperation);
                        GroupNode rootGNode = GetParentGroupNode(group, gNode);

                        MergeWithExistingRootNodes(rootGroups, rootGNode);
                    }
                }
                return new Tree(rootGroups, null, null, userId);
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }

        //recursive function to populate every group and underlying buses.
        private static GroupNode SetSimpleGroupNode(IBI.DataAccess.DBModels.Group group, bool inOperation = false)
        {
            try
            {
                using (IBIDataModel db = new IBIDataModel())
                {
                    List<BusNode> children = new List<BusNode>(); //for tree node children

                    //get child groups
                    List<IBI.DataAccess.DBModels.Group> childGroups = group.Groups.ToList();

                    foreach (IBI.DataAccess.DBModels.Group grp in childGroups)
                    {
                        //set every child group
                        children.Add(SetSimpleGroupNode(grp, inOperation));

                    }

                    //get buses 
                    List<IBI.DataAccess.DBModels.Bus> childBuses = group.Buses.Where(b => (b.InOperation == true || inOperation == false)).ToList();

                    foreach (IBI.DataAccess.DBModels.Bus bus in childBuses)
                    {
                        //set every child bus node
                        int customerId = Convert.ToInt32(bus.CustomerId);
                        BusDetail busDetail = new BusDetail
                        {
                            //some vars to fill bus properties
                            BusNumber = bus.BusNumber,
                            BusAlias = bus.BusAlias,
                            CustomerID = customerId,
                            CustomerName = bus.Customer.FullName,
                            InOperation = AppUtility.ToBool(bus.InOperation),
                            HasDCU = bus.Clients.Any(c => c.ClientType == "DCU"),
                            HasCCU = bus.Clients.Any(c => c.ClientType == "CCU")
                        };

                        //prepare a busNode for this bus
                        BusNode busNode = BusNode.CreateBusNode(bus.BusNumber, busDetail);
                        children.Add(busNode); // add bus to result set
                    }

                    //set current node
                    GroupNode node = GroupNode.CreateGroup(group.GroupName, group.GroupId, Convert.ToInt32(group.CustomerId), children);
                    return node;

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;
        }


        #endregion

        #region Simple BusTree With Group only

        //function to get the Hierarcal Bus Tree
        public static Tree CreateGroupTree(int userId, int groupId = 0)
        {
            try
            {
                List<BusNode> rootGroups = new List<BusNode>();

                IBIDataModel dbContext = new IBIDataModel();

                var uGroups = dbContext.Users.FirstOrDefault(x => x.UserId == userId).UserGroups;

                foreach (var uGroup in uGroups)
                {
                    var groups = groupId > 0 ? uGroup.Groups.Where(x => x.GroupId == groupId) : uGroup.Groups;

                    foreach (var group in groups)
                    {
                        GroupNode gNode = PopulateGroupNode(group);
                        GroupNode rootGNode = GetParentGroupNode(group, gNode);

                        MergeWithExistingRootNodes(rootGroups, rootGNode);
                    }
                }
                return new Tree(rootGroups, null, null, userId);
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }

        //recursive function to populate every group .
        private static GroupNode PopulateGroupNode(IBI.DataAccess.DBModels.Group group)
        {

            using (IBIDataModel db = new IBIDataModel())
            {
                List<BusNode> children = new List<BusNode>(); //for tree node children

                //get child groups
                List<IBI.DataAccess.DBModels.Group> childGroups = group.Groups.ToList();

                foreach (IBI.DataAccess.DBModels.Group grp in childGroups)
                {
                    //set every child group
                    children.Add(PopulateGroupNode(grp));
                }
                //set current node
                GroupNode node = GroupNode.CreateGroup(group.GroupName, group.GroupId, Convert.ToInt32(group.CustomerId), children);
                node.isLazy = false;
                return node;
            }
        }

        #endregion

        #region BusTree

        //a test function
        public static List<BusNode> CreateBusTree()
        {
            //return  CreateBusTree(3, 0);
            return new List<BusNode>();
        }



        public static Tree CreateBusTree(int userId, List<BusStatusColor> filters, int groupId = 0)
        {
            try
            {
                List<BusNode> rootGroups = new List<BusNode>();

                IBIDataModel dbContext = new IBIDataModel();

                Dictionary<string, int> counters = new Dictionary<string, int>();
                counters.Add("GREEN", 0);
                counters.Add("GREY", 0);
                counters.Add("LIGHT_GREY", 0);
                counters.Add("ORANGE", 0);
                counters.Add("RED", 0);
                counters.Add("YELLOW", 0);
                counters.Add("PURPLE", 0);
                counters.Add("IN_OPERATION", 0);
                counters.Add("INACTIVE", 0);

                var uGroups = dbContext.Users.FirstOrDefault(x => x.UserId == userId).UserGroups;

                foreach (var uGroup in uGroups)
                {
                    var groups = groupId > 0 ? uGroup.Groups.Where(x => x.GroupId == groupId) : uGroup.Groups;

                    foreach (var group in groups)
                    {
                        GroupNode gNode = SetGroupNode(group, filters, counters);
                        GroupNode rootGNode = GetParentGroupNode(group, gNode);

                        MergeWithExistingRootNodes(rootGroups, rootGNode);
                    }
                }


                counters["IN_OPERATION"] = counters["GREEN"] + counters["GREY"] + counters["ORANGE"] + counters["RED"] + counters["YELLOW"] + counters["PURPLE"];
                counters["INACTIVE"] = counters["LIGHT_GREY"];

                return new Tree(rootGroups, filters, counters, userId);
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }
        //function to get the Hierarcal Bus Tree
        public static GroupNode GetGroupNode(int groupId)
        {
            try
            {
                IBIDataModel dbContext = new IBIDataModel();

                var group = dbContext.Groups.Where(x => x.GroupId == groupId).FirstOrDefault();


                if (group != null)
                {
                    GroupNode gNode = SetGroupNode(group);
                    return gNode;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;
        }


        //Recursive function to merge current GroupNode with the existing nodes.
        private static void MergeWithExistingRootNodes(List<BusNode> rootGroups, BusNode rootGNode)
        {
            try
            {
                //if there is no leaf node, then there is no need to add that group
                if (rootGNode == null)
                    return;

                //if rootNode already exists. don't add another root, rather embed inside it.
                BusNode existingGroupNode = rootGroups.Where(x => x.GroupName == rootGNode.GroupName).FirstOrDefault();

                if (existingGroupNode == null)
                    rootGroups.Add(rootGNode);
                else
                    MergeWithExistingRootNodes(existingGroupNode.children, (BusNode)rootGNode.children.FirstOrDefault());
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }

        }

        //recursive function to get the Parent Hierarchy of Group Nodes
        private static GroupNode GetParentGroupNode(IBI.DataAccess.DBModels.Group group, GroupNode gNode)
        {
            try
            {
                IBI.DataAccess.DBModels.Group pGroup = group.ParentGroup;

                if (pGroup != null)
                {
                    GroupNode pGroupNode = new GroupNode(Guid.NewGuid(), pGroup.GroupName, pGroup.GroupId, group.CustomerId ?? 0, new List<BusNode>());
                    pGroupNode.children.Add(gNode);

                    if (pGroup.ParentGroupId == null)
                    {
                        return pGroupNode;
                    }
                    else
                    {
                        return GetParentGroupNode(pGroup, pGroupNode);

                    }

                }
                else
                {
                    return gNode;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }

        //recursive function to populate every group and underlying buses.
        private static GroupNode SetGroupNode(IBI.DataAccess.DBModels.Group group, List<BusStatusColor> filters = null, Dictionary<string, int> counters = null)
        {
            try
            {
                using (IBIDataModel db = new IBIDataModel())
                {
                    List<BusNode> children = new List<BusNode>(); //for tree node children

                    //get child groups
                    List<IBI.DataAccess.DBModels.Group> childGroups = group.Groups.ToList();

                    foreach (IBI.DataAccess.DBModels.Group grp in childGroups)
                    {
                        //set every child group
                        children.Add(SetGroupNode(grp, filters, counters));

                    }

                    //get customer config for BusTreeService
                    BusTreeCustomer custConfig = BusTreeServiceConfigurations.GetCustomerSettings(group.CustomerId.Value);



                    //get buses 
                    List<IBI.DataAccess.DBModels.Bus> childBuses = group.Buses.ToList();

                    foreach (IBI.DataAccess.DBModels.Bus bus in childBuses)
                    {
                        //set every child bus node

                        BusDetail busDetail = GetBusDetail(bus, custConfig);

                        //prepare a busNode for this bus
                        BusNode busNode = BusNode.CreateBusNode(bus.BusNumber, busDetail);

                        //increment color counters here
                        if (busDetail.BusStatus != null && counters != null)
                        {
                            int valCounter = 0;
                            counters.TryGetValue(busDetail.BusStatus.ToString(), out valCounter);
                            valCounter++;
                            counters[busDetail.BusStatus.ToString()] = valCounter;
                        }

                        //decide here to either show this bus in result set or not?
                        if (filters != null)
                        {

                            foreach (BusStatusColor color in filters)
                            {
                                if (busDetail.BusStatus == color)
                                    children.Add(busNode); // only add those buses to result set, which has colors matched in filters.
                            }
                        }

                    }

                    //set current node
                    GroupNode node = GroupNode.CreateGroup(group.GroupName, group.GroupId, Convert.ToInt32(group.CustomerId), children);
                    return node;

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }

        private static IBI.Shared.Models.BusTree.BusDetail GetBusDetail(DataAccess.DBModels.Bus bus, BusTreeCustomer custConfig)
        {
            try
            {
                //some vars to fill bus properties
                int customerId = Convert.ToInt32(bus.CustomerId);

                //BusTreeCustomer[] bConfig = IBI.Shared.ConfigSections.BusTreeServiceConfigurations.ReadAllConfigurations();
                //BusTreeCustomer custConfig = BusTreeServiceConfigurations.GetCustomerSettings(customerId);


                BusDetail busDetail = new BusDetail();

                busDetail.BusNumber = bus.BusNumber;
                busDetail.BusAlias = bus.BusAlias;
                busDetail.CustomerID = customerId;
                busDetail.CustomerName = bus.Customer.FullName;
                busDetail.InOperation = AppUtility.ToBool(bus.InOperation);
                busDetail.LastTimeInService = AppUtility.ToDateTime(bus.LastTimeInService);
                busDetail.LastUpdatedInService = AppUtility.ToDateTime(bus.LastUpdatedInService);
                busDetail.CurrentJourney = AppUtility.IsNullInt(bus.CurrentJourneyNumber);
                busDetail.LastTimeInService = AppUtility.ToDateTime(bus.LastTimeInService); //client..LastScheduleTime; [KHI]: LastScheduleTime not known
                busDetail.Remarks = string.Empty;
                busDetail.ServiceLevel = AppUtility.IsNullInt(bus.ServiceLevel);

                busDetail.InService = GetBusInServiceStatus(bus.BusNumber, bus.CustomerId, busDetail.InOperation, busDetail.LastTimeInService, AppUtility.ToInt64(bus.ScheduleJourneyNumber), bus.LastUpdatedInService ?? DateTime.MinValue);

                if (busDetail.InService.Exists)
                    busDetail.ScheduledJourney = AppUtility.ToInt64(bus.ScheduleJourneyNumber);


                busDetail.IsServiceCase = busDetail.ServiceLevel != 0;
                busDetail.ServiceDetail = bus.ServiceDetail;

                foreach (var client in bus.Clients)
                {
                    //some vars to fill client's properties
                    bool client_MarkedForService;

                    switch (client.ClientType)
                    {
                        case "DCU":
                            {
                                busDetail.DCU.LastPing = client.LastPing;
                                busDetail.DCU.Exists = true;
                                busDetail.DCU.MacAddress = client.MacAddress;
                                break;
                            }
                        case "VTC":
                            {
                                busDetail.VTC.LastPing = client.LastPing;
                                busDetail.VTC.Exists = true;
                                busDetail.VTC.MacAddress = client.MacAddress;
                                break;
                            }
                        case "CCU":
                            {
                                busDetail.CCU.LastPing = client.LastPing;
                                busDetail.CCU.Exists = true;
                                busDetail.CCU.MacAddress = client.MacAddress;
                                break;
                            }
                    }

                    client_MarkedForService = AppUtility.ToBool(client.MarkedForService);

                    //TODO: Ask from Martin
                    if (string.Compare(client.ClientType, "CCU", true) == 0)
                    {
                        if (client.LastPing > busDetail.DCU.LastPing)
                        {
                            busDetail.CCU.MarkedForService = client_MarkedForService;
                        }
                        busDetail.CCU.LastPing = client.LastPing;
                    }

                    if (string.Compare(client.ClientType, "VTC", true) == 0)
                    {
                        if (client.LastPing > busDetail.DCU.LastPing)
                        {
                            busDetail.VTC.MarkedForService = client_MarkedForService;

                            //busDetail.VTC.LastPing = client.LastPing;

                        }

                        busDetail.VTC.LastPing = client.LastPing;

                        busDetail.InfotainmentMacAddress = client.MacAddress;
                    }

                    if (string.Compare(client.ClientType, "DCU", true) == 0)
                    {
                        if (client.LastPing > busDetail.VTC.LastPing)
                        {
                            //[KHI] shifted to Geography
                            //tmpBus.Longitude = client.Longitude;
                            //tmpBus.Latitude = client.Latitude;
                        }

                        busDetail.DCU.LastPing = client.LastPing;

                    }
                }




                if (custConfig.CheckInfotainment && !string.IsNullOrEmpty(busDetail.InfotainmentMacAddress))
                {
                    busDetail.Infotainment.LastPing = IBI.Shared.AppSettings.InfotainmentPing ? IBI.REST.Core.SystemServiceController.getLastInfotainmentPing(busDetail.InfotainmentMacAddress) : DateTime.Now;
                    busDetail.Infotainment.Exists = true;

                }

                //if (custConfig.CheckHotspot)
                {
                    busDetail.Hotspot.LastPing = IBI.Shared.AppSettings.HotspotPing ? IBI.REST.Core.SystemServiceController.getLastHotspotPing(bus.BusNumber) : DateTime.Now;
                    busDetail.Hotspot.Exists = busDetail.Hotspot.LastPing > DateTime.MinValue;
                }


                if (AppSettings.ShouldValidateSchedules())
                {
                    //[KHI]: bus_SceduledJourney ???? where to get value from
                    busDetail.ScheduleOk = (busDetail.CurrentJourney == busDetail.ScheduledJourney);

                    if (!busDetail.ScheduleOk)
                        busDetail.Remarks += " - Wrong journey" + Environment.NewLine;
                }


                //conclude each client's Status color here.                    

                TimeSpan offlineToleranceTime = AppSettings.GetOfflineTolerance();

                //DCU

                bool isInService = busDetail.InService.Color == BusStatusColor.GREEN ? true : false;

                List<DateTime> otherSubSystemsLastPing = new List<DateTime>();
                otherSubSystemsLastPing.Add(busDetail.DCU.LastPing);
                otherSubSystemsLastPing.Add(busDetail.CCU.LastPing);
                otherSubSystemsLastPing.Add(busDetail.VTC.LastPing);
                otherSubSystemsLastPing.Add(busDetail.Hotspot.LastPing);
                otherSubSystemsLastPing.Add(busDetail.Infotainment.LastPing);

                if (busDetail.DCU.Exists)
                    busDetail.DCU.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.DCU.LastPing, offlineToleranceTime, otherSubSystemsLastPing, AppSettings.GetDCUVSInService(), AppSettings.GetDCUVSOtherSubSystem());

                if (busDetail.CCU.Exists)
                    busDetail.CCU.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.CCU.LastPing, offlineToleranceTime, otherSubSystemsLastPing, AppSettings.GetCCUVSInService(), AppSettings.GetCCUVSOtherSubSystem());

                if (busDetail.VTC.Exists)
                    busDetail.VTC.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.VTC.LastPing, offlineToleranceTime, otherSubSystemsLastPing, AppSettings.GetVTCVSInService(), AppSettings.GetVTCVSOtherSubSystem());

                if (busDetail.Hotspot.Exists)
                    busDetail.Hotspot.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.Hotspot.LastPing, offlineToleranceTime, otherSubSystemsLastPing, AppSettings.GetHotspotVSInService(), AppSettings.GetHotspotVSOtherSubSystem());

                if (busDetail.Infotainment.Exists)
                    busDetail.Infotainment.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.Infotainment.LastPing, offlineToleranceTime, otherSubSystemsLastPing, AppSettings.GetInfotainmentVSInService(), AppSettings.GetVTCVSOtherSubSystem());

                BusUnitStatus[] clientStatuses = new BusUnitStatus[]
                {
                    busDetail.CCU,
                    busDetail.DCU,
                    busDetail.VTC,
                    busDetail.Hotspot,
                    busDetail.Infotainment
                };

                busDetail.BusStatus = GetBusStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, ref clientStatuses, busDetail.IsServiceCase);

                if (busDetail.DCU.Exists)
                    busDetail.Clients.Add(busDetail.DCU);

                if (busDetail.CCU.Exists)
                    busDetail.Clients.Add(busDetail.CCU);

                if (busDetail.VTC.Exists)
                    busDetail.Clients.Add(busDetail.VTC);

                if (busDetail.Hotspot.Exists)
                    busDetail.Clients.Add(busDetail.Hotspot);

                if (busDetail.Infotainment.Exists)
                    busDetail.Clients.Add(busDetail.Infotainment);

                return busDetail;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;
        }

        private static BusUnitStatus GetBusInServiceStatus(string busNumber, int customerId, bool inOperation, DateTime busLastTimeInService, long scheduledJourneyNumber, DateTime busLastUpdatedInService)
        {
            BusUnitStatus busInService = new BusUnitStatus("In Service");



            busInService.LastPing = busLastTimeInService;
            busInService.LastPingUpdated = busLastUpdatedInService;

            if (busLastTimeInService == DateTime.MinValue)
            {
                busInService.Exists = false;
                busInService.Color = BusStatusColor.NONE;
            }
            else
            {
                busInService.Exists = true;

                if (!inOperation) //extreme priority case - Light Grey [if bus is marked as Not in operation]
                {
                    busInService.Color = BusStatusColor.LIGHT_GREY;
                }
                else
                {

                    if (busLastUpdatedInService < DateTime.Now.AddMinutes(-60)) // || scheduledJourneyNumber == -1 )
                    {
                        busInService.Color = BusStatusColor.RED;
                    }
                    else
                    {
                        //if busLastTimeInService = busLastUpdatedInService and () ... then Green.. else Grey
                        //if (busLastTimeInService < busLastUpdatedInService)
                        if (busLastUpdatedInService.ToString("yyyy-MM-ddTHH:mm:ss") == busLastTimeInService.ToString("yyyy-MM-ddTHH:mm:ss")
                            || busLastTimeInService >= busLastUpdatedInService)
                        {
                            busInService.Color = BusStatusColor.GREEN;
                        }
                        else
                        {
                            busInService.Color = BusStatusColor.GREY;
                        }

                    }
                }
            }
            return busInService;
        }

        private static DateTime GetCacheFileLastModifiedTime(string busNumber, int customerId)
        {
            string resourcePath = AppSettings.GetResourceDirectory();

            string outputXml = string.Empty;

            FileInfo scheduleFile = new FileInfo(Path.Combine(resourcePath, Path.Combine(customerId.ToString(), "Cache"), "Schedule_" + busNumber + ".xml"));

            return (scheduleFile.Exists ? scheduleFile.LastWriteTime : DateTime.MinValue);
        }

        //helper function to get the Client's status color          
        private static BusStatusColor GetClientStatus(bool inOperation, bool isBusInService, DateTime busLastTimeInService, DateTime clientLastPing, TimeSpan offlineTolerance, List<DateTime> otherSubSystemsLastPing, TimeSpan toleranceVSInServices, TimeSpan toleranceVSSubSystem)
        {
            try
            {
                if (!inOperation && clientLastPing > DateTime.MinValue)
                    return BusStatusColor.LIGHT_GREY;

                bool IsAnyotherSubSystemOnline = otherSubSystemsLastPing.Any(x => x.Add(offlineTolerance) >= DateTime.Now);  //!(busDetail.DCU.LastPing.Add(offlineToleranceTime) < DateTime.Now) || !(busDetail.VTC.LastPing.Add(offlineToleranceTime) < DateTime.Now) || !(busDetail.Hotspot.LastPing.Add(offlineToleranceTime) < DateTime.Now) || !(busDetail.Infotainment.LastPing.Add(offlineToleranceTime) < DateTime.Now) ? true : false;

                bool isClientOnline = !(clientLastPing.Add(offlineTolerance) < DateTime.Now);

                if (!isClientOnline)
                {
                    if (!isBusInService)
                    {
                        if (!IsAnyotherSubSystemOnline)
                        {
                            if (busLastTimeInService != DateTime.MinValue && clientLastPing >= busLastTimeInService) return BusStatusColor.GREY;
                            if (busLastTimeInService != DateTime.MinValue && clientLastPing < busLastTimeInService.AddHours(-24)) return BusStatusColor.ORANGE;
                            bool checkOrangwithAllSubSystems = false;
                            foreach (DateTime otherSubSystemLastPing in otherSubSystemsLastPing)
                            {
                                if (otherSubSystemLastPing != DateTime.MinValue && clientLastPing < otherSubSystemLastPing.AddHours(-24))
                                {
                                    checkOrangwithAllSubSystems = true;
                                    //break;
                                }
                                else
                                {
                                    checkOrangwithAllSubSystems = false;
                                    break;
                                }
                            }
                            if (checkOrangwithAllSubSystems)
                                return BusStatusColor.ORANGE;
                            else
                            {
                                if (clientLastPing.Add(toleranceVSInServices) < busLastTimeInService) return BusStatusColor.PURPLE;
                                bool checkPurplewithAllSubSystems = false;
                                foreach (DateTime otherSubSystemLastPing in otherSubSystemsLastPing)
                                {
                                    if (clientLastPing.Add(toleranceVSSubSystem) < otherSubSystemLastPing)
                                    {
                                        checkPurplewithAllSubSystems = true;
                                        break;
                                    }
                                    else
                                    {
                                        checkPurplewithAllSubSystems = false;
                                    }
                                }
                                if (checkPurplewithAllSubSystems)
                                    return BusStatusColor.PURPLE;
                            }
                            return BusStatusColor.GREY;
                        }
                        else
                        {
                            if (clientLastPing == DateTime.MinValue || (clientLastPing.AddHours(24) < DateTime.Now))
                            {
                                return BusStatusColor.ORANGE;
                            }

                            otherSubSystemsLastPing = otherSubSystemsLastPing.Where(x => x.Add(offlineTolerance) >= DateTime.Now).ToList();
                            return otherSubSystemsLastPing.Any(d => d < clientLastPing.Add(toleranceVSSubSystem)) ? BusStatusColor.GREY : BusStatusColor.RED;

                            //foreach (DateTime otherSubSystemLastPing in otherSubSystemsLastPing)
                            //{
                            //    if (clientLastPing != otherSubSystemLastPing && clientLastPing.Add(toleranceVSSubSystem) > otherSubSystemLastPing)
                            //    {
                            //        return BusStatusColor.GREY;
                            //    }
                            //}
                            //return BusStatusColor.RED;

                        }
                    }
                    else
                    {
                        if (!IsAnyotherSubSystemOnline)
                        {
                            if (clientLastPing < busLastTimeInService.AddHours(-24)) return BusStatusColor.ORANGE;
                            bool checkOrangwithAllSubSystems = false;
                            foreach (DateTime otherSubSystemLastPing in otherSubSystemsLastPing)
                            {
                                if (otherSubSystemLastPing != DateTime.MinValue && clientLastPing < otherSubSystemLastPing.AddHours(-24))
                                    checkOrangwithAllSubSystems = true;
                                else
                                {
                                    checkOrangwithAllSubSystems = false;
                                    break;
                                }
                            }
                            if (checkOrangwithAllSubSystems)
                                return BusStatusColor.ORANGE;
                            else
                            {
                                //return BusStatusColor.RED;
                                return busLastTimeInService < clientLastPing.Add(toleranceVSInServices) ? BusStatusColor.GREY : BusStatusColor.RED;
                            }
                        }
                        else
                        {
                            if (clientLastPing == DateTime.MinValue || (clientLastPing.AddHours(24) < DateTime.Now)) return BusStatusColor.ORANGE;
                            otherSubSystemsLastPing = otherSubSystemsLastPing.Where(x => x.Add(offlineTolerance) >= DateTime.Now).ToList();
                            if (otherSubSystemsLastPing.Any(d => d < clientLastPing.Add(toleranceVSSubSystem)))
                            {
                                return BusStatusColor.GREY;
                            }
                            else
                            {
                                return busLastTimeInService < clientLastPing.Add(toleranceVSInServices) ? BusStatusColor.GREY : BusStatusColor.RED;
                            }
                        }
                    }
                }
                else
                {
                    if (!isBusInService)
                    {
                        return BusStatusColor.LIGHT_GREEN;
                    }
                    else
                    {
                        return BusStatusColor.GREEN;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return BusStatusColor.NONE;
        }

        //private static BusStatusColor GetClientStatus(bool inOperation, bool isBusInService, DateTime busLastTimeInService, DateTime clientLastPing, TimeSpan offlineTolerance)
        //{

        //    if (!inOperation && clientLastPing > DateTime.MinValue)
        //        return BusStatusColor.LIGHT_GREY;

        //    bool isClientOnline = !(clientLastPing.Add(offlineTolerance) < DateTime.Now); //!(clientLastPing.Add(offlineTolerance) < busLastTimeInService);


        //    if (isBusInService && isClientOnline)
        //        return BusStatusColor.GREEN;

        //    if (!isBusInService && isClientOnline)
        //        return BusStatusColor.LIGHT_GREEN;

        //    if (!isBusInService && !isClientOnline)
        //        return BusStatusColor.GREY;

        //    if (isBusInService && !isClientOnline || (clientLastPing.Add(offlineTolerance) < busLastTimeInService && !isBusInService))
        //        return BusStatusColor.RED;



        //    return BusStatusColor.NONE;
        //}

        //helper function to get the overall Bus Status color.

        private static BusStatusColor GetBusStatus(bool inOperation, bool isBusInService, DateTime busLastTimeInService, ref BusUnitStatus[] clientStatuses, bool busHasServiceCase)
        {
            try
            {
                if (!inOperation) //max priority - Light grey
                {
                    foreach (BusUnitStatus clientStatus in clientStatuses)
                    {
                        clientStatus.Color = (clientStatus.Color == BusStatusColor.NONE) ? BusStatusColor.NONE : BusStatusColor.LIGHT_GREY;
                    }

                    return BusStatusColor.LIGHT_GREY;
                }

                if (busHasServiceCase)
                {
                    //turns all RED clients into YELLOW
                    foreach (BusUnitStatus clientStatus in clientStatuses)
                    {
                        if (clientStatus.Color == BusStatusColor.RED || clientStatus.Color == BusStatusColor.ORANGE || clientStatus.Color == BusStatusColor.PURPLE)
                            clientStatus.Color = BusStatusColor.YELLOW;
                    }
                    return BusStatusColor.YELLOW;
                }
                bool existRedColor = false;
                //turns BusStatus to RED/Orange base upon SubSystem Status RED/Orange
                foreach (BusUnitStatus clientStatus in clientStatuses)
                {
                    if (clientStatus.Color == BusStatusColor.ORANGE)
                        return BusStatusColor.ORANGE;
                    else if (clientStatus.Color == BusStatusColor.RED)
                        existRedColor = true;
                }
                if (existRedColor) return BusStatusColor.RED;


                //if(busLastTimeInService > DateTime.MinValue)
                //{
                //    bool anyClientWith24HrsOldPing;

                //    try
                //    {
                //        anyClientWith24HrsOldPing = clientStatuses.Where(x => x.Exists && x.LastPing < busLastTimeInService.AddHours(-24) && x.LastPing!=DateTime.MinValue).Count() > 0;
                //    }
                //    catch (Exception ex)
                //    {
                //        throw ex;
                //    }

                //    if (anyClientWith24HrsOldPing)
                //        return BusStatusColor.ORANGE;
                //}


                //if all the subsystems installed in bus are online (green or light-green) then BusStatusColor should be GREEN .. otherwise GREY
                if (clientStatuses.All(cs => (cs.Color == BusStatusColor.GREEN || cs.Color == BusStatusColor.LIGHT_GREEN || cs.Color == BusStatusColor.NONE)) &&
                    clientStatuses.All(cs => cs.Color == BusStatusColor.NONE) == false)
                {
                    return BusStatusColor.GREEN;
                }


                if (!isBusInService || busLastTimeInService == DateTime.MinValue)
                {
                    return BusStatusColor.GREY;
                }


                bool anyOfflineClient = clientStatuses.Where(x => x.Color == BusStatusColor.RED).Count() > 0;

                if (isBusInService && anyOfflineClient)
                    return BusStatusColor.RED;

                if (isBusInService && !anyOfflineClient)
                    return BusStatusColor.GREEN;

                return BusStatusColor.NONE;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return BusStatusColor.NONE;

        }
        //private static BusStatusColor GetBusStatus(bool inOperation, bool isBusInService, DateTime busLastTimeInService, ref BusUnitStatus[] clientStatuses, bool busHasServiceCase)
        //{

        //    if (!inOperation) //max priority - Light grey
        //    {
        //        foreach (BusUnitStatus clientStatus in clientStatuses)
        //        {
        //            clientStatus.Color = (clientStatus.Color == BusStatusColor.NONE) ? BusStatusColor.NONE : BusStatusColor.LIGHT_GREY;
        //        }

        //        return BusStatusColor.LIGHT_GREY;
        //    }

        //    if (busHasServiceCase)
        //    {
        //        //turns all RED clients into YELLOW
        //        foreach (BusUnitStatus clientStatus in clientStatuses)
        //        {
        //            if (clientStatus.Color == BusStatusColor.RED)
        //                clientStatus.Color = BusStatusColor.YELLOW;
        //        }

        //        return BusStatusColor.YELLOW;
        //    }



        //    if (busLastTimeInService > DateTime.MinValue)
        //    {
        //        bool anyClientWith24HrsOldPing;

        //        try
        //        {
        //            anyClientWith24HrsOldPing = clientStatuses.Where(x => x.Exists && x.LastPing < busLastTimeInService.AddHours(-24) && x.LastPing != DateTime.MinValue).Count() > 0;
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }

        //        if (anyClientWith24HrsOldPing)
        //            return BusStatusColor.ORANGE;

        //    }

        //    if (!isBusInService || busLastTimeInService == DateTime.MinValue)
        //        return BusStatusColor.GREY;


        //    bool anyOfflineClient = clientStatuses.Where(x => x.Color == BusStatusColor.RED).Count() > 0;

        //    if (isBusInService && anyOfflineClient)
        //        return BusStatusColor.RED;

        //    if (isBusInService && !anyOfflineClient)
        //        return BusStatusColor.GREEN;


        //    return BusStatusColor.NONE;

        //}

        public static IBI.Shared.Models.BusTree.Group GetGroup(int groupId)
        {

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var groups = dbContext.Groups.Where(g => g.GroupId == groupId).Select(g => new IBI.Shared.Models.BusTree.Group()
                {
                    CustomerId = g.CustomerId,
                    CustomerName = g.Customer.FullName,
                    GroupId = g.GroupId,
                    GroupName = g.GroupName,
                    ParentGroupId = g.ParentGroupId,
                    ParentGroupName = g.ParentGroup.GroupName

                });

                if (groups != null & groups.Count() > 0)
                {
                    return groups.FirstOrDefault();
                }
            }

            return null;
        }

        public static IBI.Shared.Models.BusTree.Bus GetBus(int customerId, string busNumber)
        {

            ////#hack to save geography information
            //var point = SqlGeography.Point(double.Parse(latitude), double.Parse(longitude), 4326);
            //var poly = point.BufferWithTolerance(1, 0.01, true);

            using (IBIDataModel dbContnext = new IBIDataModel())
            {
                IBI.Shared.Models.BusTree.Bus bus = dbContnext.vBuses.Where(b => b.BusNumber == busNumber && b.CustomerId == customerId).Select(b => new IBI.Shared.Models.BusTree.Bus
                {
                    BusNumber = b.BusNumber,
                    BusAlias = b.BusAlias,
                    CustomerId = b.CustomerId,
                    CustomerName = b.CustomerName,
                    ParentGroupId = b.ParentGroupId,
                    ParentGroupName = b.ParentGroupName,
                    CurrentJourneyNumber = b.CurrentJourneyNumber,
                    Description = b.Description,
                    InOperation = b.InOperation,
                    LastTimeInService = b.LastTimeInService ?? DateTime.MinValue,
                    //DateAdded = b.DateAdded.Value ?? DateTime.MinValue.ToString(),
                    //DateModified = b.DateModified.Value ?? DateTime.MinValue.ToString(),
                    LastKnownLocation = new Geography { Latitude = b.LastKnownLocation.Latitude ?? 0.00, Longitude = b.LastKnownLocation.Longitude ?? 0.00 }
                }).FirstOrDefault();

                return bus;
            }



        }

        public static List<IBI.Shared.Models.BusTree.BusDetail> GetBusLookUpDetail(string busNumber)
        {
            List<BusDetail> busDetail = new List<BusDetail>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var buses = dbContext.Buses.Where(b => b.BusNumber == busNumber).ToList();
                foreach (var bus in buses)
                {
                    //get customer config for BusTreeService
                    BusTreeCustomer custConfig = BusTreeServiceConfigurations.GetCustomerSettings(bus.CustomerId);

                    busDetail.Add(GetBusDetail(bus, custConfig));
                }
            }
            return busDetail;
        }

        public static IBI.Shared.Models.BusTree.BusDetail GetBusDetail(string busNumber)
        {
            BusDetail busDetail = null;

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var bus = dbContext.Buses.FirstOrDefault(b => b.BusNumber == busNumber);

                if (bus != null)
                {
                    //get customer config for BusTreeService
                    BusTreeCustomer custConfig = BusTreeServiceConfigurations.GetCustomerSettings(bus.CustomerId);
                    busDetail = GetBusDetail(bus, custConfig);
                }
            }

            return busDetail;
        }


        public static string GetBusServiceTickets(string busNumber, int customerId)
        {
            String busServiceTickets = "[";


            using (IBIDataModel dbContext = new IBIDataModel())
            {
                //var bus = dbContext.Buses.FirstOrDefault(b => b.BusNumber == busNumber && b.CustomerId == customerId);
                var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == customerId).FirstOrDefault();
                if (bus != null && bus.ServiceLevel != 0)
                {
                    string[] tickets = bus.ServiceDetail.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    string allTickets = string.Empty;
                    foreach (string ticket in tickets)
                    {
                        string[] arrTicket = ticket.Split('~');
                        allTickets += string.Format(@"&nbsp;*&nbsp;{0}{1}<br/>",
                                                            arrTicket[0] != null ? arrTicket[0] + ": " : "",
                                                            arrTicket[1] != null ? arrTicket[1].Substring(0, arrTicket[1].Length > 30 ? 30 : arrTicket[1].Length) + "..." : ""
                                                );
                    }
                    busServiceTickets += "{";
                    busServiceTickets += "\"ServiceTickets\":\"" + allTickets + "\"";
                    busServiceTickets += "}";
                }
            }
            busServiceTickets += "]";
            return busServiceTickets;
        }


        public static List<IBI.Shared.Models.BusTree.Client> GetClient(string macAddress)
        {
            List<IBI.Shared.Models.BusTree.Client> retclients = new List<IBI.Shared.Models.BusTree.Client>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var clients = dbContext.Clients.Where(c => c.MacAddress.Contains(macAddress)); //SqlMethods.Like(c.FullName, "%"+FirstName+"%,"+LastName)
                foreach (var c in clients)
                {
                    var client = new IBI.Shared.Models.BusTree.Client
                    {
                        BusNumber = c.BusNumber,
                        ClientId = c.ClientId,
                        ClientType = c.ClientType,
                        CustomerId = c.CustomerId,
                        LastPing = c.LastPing,
                        MacAddress = c.MacAddress,
                        MarkedForService = c.MarkedForService ?? false,
                        SimId = c.SimID
                    };
                    retclients.Add(client);
                }
                return retclients;
            }

            return null;
        }

        public static List<string> GetAllBusNumbers(int customerId = 0, bool InOperation = true, bool checkInOperation = true)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var busNumbers = dbContext.Buses
                    .Where(b =>
                        (b.CustomerId == customerId || customerId == 0) &&
                        (b.InOperation == InOperation || checkInOperation == false)
                    ).Select(b => b.BusNumber).ToList();

                return busNumbers;
            }

        }

        public static List<IBI.Shared.Models.BusTree.Group> GetGroups(int userId)
        {
            IBIDataModel dbContext = new IBIDataModel();
            var groups = DBHelper.GetUserGroups(userId, dbContext).Select(g => new IBI.Shared.Models.BusTree.Group
            {
                CustomerId = g.CustomerId,
                CustomerName = g.Customer == null ? "" : g.Customer.FullName,
                GroupId = g.GroupId,
                GroupName = g.GroupName,
                ParentGroupId = g.ParentGroupId,
                ParentGroupName = g.ParentGroup == null ? "" : g.ParentGroup.GroupName,
                Description = g.Description
            }).ToList();

            return groups;
        }

        public static bool ChangeBusGroup(int customerid, string busnumber, int groupid)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var selectedBus = (from bus in dbContext.Buses
                                       where (bus.BusNumber == busnumber && bus.CustomerId == customerid)
                                       select bus).First();

                    var selectedGroup = (from busgroup in dbContext.Groups
                                         where busgroup.GroupId == groupid
                                         select busgroup).First();

                    selectedBus.Groups.Clear();
                    selectedBus.Groups.Add(selectedGroup);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch
            {

            }
            return false;
        }

        public static bool ChangeBusStatus(int customerId, string busNumber, bool InOperation)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var selectedBus = (from bus in dbContext.Buses
                                       where (bus.BusNumber == busNumber && bus.CustomerId == customerId)
                                       select bus).First();

                    selectedBus.InOperation = InOperation;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch
            {

            }
            return false;
        }

        public static bool ModifyBusAlias(int customerId, string busNumber, string busAlias)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var selectedBus = (from bus in dbContext.Buses
                                       where (bus.BusNumber == busNumber && bus.CustomerId == customerId)
                                       select bus).First();

                    selectedBus.BusAlias = busAlias;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch
            {

            }
            return false;
        }

        public static List<String> GetUserBuses(int userId)
        {
            List<String> busList = new List<String>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                busList = IBI.REST.Shared.DBHelper.GetUserBuses(userId, dbContext).Select(b => b.BusNumber).ToList();
            }

            return busList;
        }
        public static List<String> GetUserBusesHavingVTC(int userId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var buses = IBI.REST.Shared.DBHelper.GetUserBuses(userId, dbContext).ToList();
                var busList = (from bus in buses
                               join client in dbContext.Clients on bus.BusNumber equals client.BusNumber
                               where client.ClientType == "VTC" && bus.CustomerId == client.CustomerId
                               select client.BusNumber).ToList();
                return busList;
            }
        }

        public static string GetBusClientsLastPings(string busNumber, int customerId)
        {
            String json = "[";
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var selectedBus = (from bus in dbContext.Buses
                                   where (bus.BusNumber == busNumber && bus.CustomerId == customerId)
                                   select bus).First();
                try
                {
                    if (selectedBus != null)
                    {
                        string DCULastPing = string.Empty;
                        string VTCLastPing = string.Empty;
                        string CCULastPing = string.Empty;
                        string INFOLastPing = string.Empty;
                        string HOTSPOTLastPing = string.Empty;

                        foreach (var client in selectedBus.Clients)
                        {
                            switch (client.ClientType)
                            {
                                case "DCU":
                                    {
                                        DCULastPing = client.LastPing.ToString("yyyyMMdd-HHmmss");
                                        HOTSPOTLastPing = IBI.Shared.AppSettings.HotspotPing ? IBI.REST.Core.SystemServiceController.getLastHotspotPing(selectedBus.BusNumber).ToString("yyyyMMdd-HHmmss") : string.Empty;
                                        break;
                                    }
                                case "CCU":
                                    {
                                        CCULastPing = client.LastPing.ToString("yyyyMMdd-HHmmss");
                                        HOTSPOTLastPing = IBI.Shared.AppSettings.HotspotPing ? IBI.REST.Core.SystemServiceController.getLastHotspotPing(selectedBus.BusNumber).ToString("yyyyMMdd-HHmmss") : string.Empty;
                                        break;
                                    }
                                case "VTC":
                                    {
                                        VTCLastPing = client.LastPing.ToString("yyyyMMdd-HHmmss");
                                        INFOLastPing = !string.IsNullOrEmpty(client.MacAddress) && IBI.Shared.AppSettings.InfotainmentPing ? IBI.REST.Core.SystemServiceController.getLastInfotainmentPing(client.MacAddress).ToString("yyyyMMdd-HHmmss") : string.Empty;
                                        break;
                                    }
                            }
                        }
                        string currentrow = string.Empty;
                        currentrow += "{";
                        currentrow += "\"DCULastPing\":\"" + DCULastPing + "\"";
                        currentrow += ",\"VTCLastPing\":\"" + VTCLastPing + "\"";
                        currentrow += ",\"HotSpotLastPing\":\"" + HOTSPOTLastPing + "\"";
                        currentrow += ",\"InfoLastPing\":\"" + INFOLastPing + "\"";
                        currentrow += "}";
                        json += currentrow;
                    }
                }
                catch (Exception ex)
                {
                    //do nothing
                }
            }
            json += "]";
            return json;
        }

        public static string GetUserSpecificBuses(string filter, string customerIds, bool offline, bool online)
        {
            String json = "[";

            var filterBuses = filter.Split(',').
                                      Select(tag => tag.Trim()).
                                      Where(tag => !string.IsNullOrEmpty(tag));

            var filterCustomer = customerIds.Split(',').
                                      Select(tag => tag.Trim()).
                                      Where(tag => !string.IsNullOrEmpty(tag));

            if ((offline || online) && (filterBuses != null && filterBuses.Count() > 0))
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var buses = dbContext.GetUserSpecificBusesForMap(String.Join(",", filterBuses.ToArray<string>()), String.Join(",", filterCustomer.ToArray<string>()));
                    int rowCounter = 0;
                    foreach (var bus in buses)
                    {
                        try
                        {
                            string online1 = (bus.LastPing != null ? (bus.LastPing.Value.AddMinutes(AppSettings.ShowOfflineBusInterval) > DateTime.Now ? "true" : "false") : "false");
                            if ((online && offline) || (online && online1 == "true") || (offline && online1 == "false"))
                            {

                                var HasServiceTickets = false;
                                var ServiceTicketsDetail = string.Empty;
                                if (bus.ServiceLevel.HasValue && bus.ServiceLevel.Value != 0)
                                {
                                    HasServiceTickets = true;
                                    string[] tickets = bus.ServiceDetail.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string ticket in tickets)
                                    {
                                        string[] arrTicket = ticket.Split('~');

                                        //ServiceTicketsDetail += string.Format(@"&nbsp;*&nbsp;{0}{1}<br/>",
                                        //                                    arrTicket[0] != null ? arrTicket[0].Replace("Ticket# ","#") + ": " : "",
                                        //                                    arrTicket[1] != null ? arrTicket[1].Substring(0, arrTicket[1].Length > 30 ? 30 : arrTicket[1].Length) + "..." : ""
                                        //                        );

                                        ServiceTicketsDetail += string.Format(@"<div class='ticketservicetooltip' style='display:inline-block;width:101%;z-index:3;' title='{0}{1}'>&nbsp;*&nbsp;{2}{3}</div><br/>",
                                        arrTicket[0] != null ? arrTicket[0] + ": " : "",
                                        arrTicket[1] != null ? arrTicket[1] : "",
                                        arrTicket[0] != null ? arrTicket[0].Replace("Ticket# ", "#") + ": " : "",
                                        arrTicket[1] != null ? arrTicket[1].Substring(0, arrTicket[1].Length > 30 ? 30 : arrTicket[1].Length) + "..." : ""
                                        );

                                    }
                                }

                                string currentBus = string.Empty;
                                currentBus += (rowCounter == 0 ? "" : ",");
                                currentBus += "{";
                                currentBus += "\"BusNumber\":\"" + bus.BusNumber + "\"";
                                currentBus += ",\"BusAlias\":\"" + (string.IsNullOrEmpty(bus.BusName) ? "" : bus.BusName) + "\"";
                                currentBus += ",\"HasServiceTickets\":\"" + HasServiceTickets + "\"";
                                currentBus += ",\"ServiceTicketsDetail\":\"" + ServiceTicketsDetail + "\"";
                                currentBus += ",\"CustomerId\":" + bus.CustomerId.ToString();
                                currentBus += ",\"Online\":" + online1;
                                currentBus += ",\"Position\":{";
                                currentBus += "\"lat\":" + (!string.IsNullOrEmpty(bus.Latitude) ? bus.Latitude.ToString(CultureInfo.InvariantCulture) : "0.0");
                                currentBus += ",\"long\":" + (!string.IsNullOrEmpty(bus.Longitude) ? bus.Longitude.ToString(CultureInfo.InvariantCulture) : "0.0") + "}";
                                currentBus += ",\"Schedule\":{";
                                currentBus += "\"ScheduleId\":\"" + bus.ScheduleId.ToString() + "\"";
                                currentBus += ",\"Line\":\"" + (string.IsNullOrEmpty(bus.Line) ? string.Empty : bus.Line) + "\"";
                                currentBus += ",\"Destination\":\"" + (string.IsNullOrEmpty(bus.Destination) ? string.Empty : bus.Destination) + "\"";
                                currentBus += ",\"Via\":\"" + (string.IsNullOrEmpty(bus.Via) ? string.Empty : bus.Via) + "\"" + "}";
                                currentBus += "}";

                                json += currentBus;
                                rowCounter++;
                            }
                        }
                        catch (Exception ex)
                        {
                            //do nothing
                        }
                    }
                }
            }
            json += "]";
            return json;
        }

        public static string GetAllUsersBuses(int groupId = 0, string busNumber = "")
        {
            String json = "[";
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var buses = new List<DataAccess.DBModels.Bus>();

                if (groupId != 0)
                {
                    buses = DBHelper.GetAllChildGroupsBuses(groupId, dbContext);
                }
                else
                {
                    buses = dbContext.Buses.Where(b => b.LastKnownLocation != null && b.InOperation == true).ToList();
                }

                if (!string.IsNullOrEmpty(busNumber))
                {
                    buses = buses.Where(bus => bus.BusNumber == busNumber).ToList();
                }

                int rowCounter = 0;
                foreach (var bus in buses)
                {
                    if (bus.Clients != null && bus.Clients.Count() > 0 && bus.Clients.OrderByDescending(c => c.LastPing).FirstOrDefault().LastPing.AddMinutes(5) <= DateTime.Now) continue;
                    object latitudeObject = bus.LastKnownLocation != null ? bus.LastKnownLocation.Latitude : null;
                    object longitudeObject = bus.LastKnownLocation != null ? bus.LastKnownLocation.Longitude : null;
                    String latitude = latitudeObject is Double ? ((Double)latitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                    String longitude = longitudeObject is Double ? ((Double)longitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                    json += (rowCounter == 0 ? "" : ",");
                    json += "{";
                    json += "\"BusNumber\":\"" + bus.BusNumber.ToString() + "\"";
                    json += ",\"Online\":true";
                    json += ",\"Position\":{";
                    json += "\"lat\":" + latitude;
                    json += ",\"long\":" + longitude + "}";
                    json += "}";
                    rowCounter++;
                }
            }
            json += "]";
            return json;
        }


        public static string GetBusesDataForMap(int scheduleId)
        {

            String json = "[";
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var buses = dbContext.Buses.Where(b => b.LastKnownLocation != null && b.InOperation == true).ToList();

                int rowCounter = 0;
                foreach (var bus in buses)
                {
                    if (bus.Clients != null && bus.Clients.Count() > 0 && bus.Clients.OrderByDescending(c => c.LastPing).FirstOrDefault().LastPing.AddMinutes(5) <= DateTime.Now) continue;

                    //string online1 = (bus.Clients != null && bus.Clients.Count() > 0 && bus.Clients.OrderByDescending(c => c.LastPing).FirstOrDefault().LastPing.AddMinutes(5) >= DateTime.Now ? "true" : "false");
                    var journey = bus.Journeys.Where(s => s.ScheduleId == scheduleId).OrderByDescending(j => j.JourneyId).FirstOrDefault();
                    if (journey == null) continue;
                    var journeystops = journey.JourneyStops.Where(js => js.ActualArrivalTime != null).OrderByDescending(l => l.StopSequence).FirstOrDefault();
                    string online1 = (journeystops.PlannedArrivalTime.Value - journeystops.ActualArrivalTime.Value).TotalMinutes < 0 ? "false" : "true";
                    object latitudeObject = bus.LastKnownLocation != null ? bus.LastKnownLocation.Latitude : null;
                    object longitudeObject = bus.LastKnownLocation != null ? bus.LastKnownLocation.Longitude : null;
                    String latitude = latitudeObject is Double ? ((Double)latitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                    String longitude = longitudeObject is Double ? ((Double)longitudeObject).ToString(CultureInfo.InvariantCulture) : "0.0";
                    json += (rowCounter == 0 ? "" : ",");
                    json += "{";
                    json += "\"BusNumber\":\"" + bus.BusNumber.ToString() + "\"";
                    json += ",\"Online\":" + online1;
                    json += ",\"Position\":{";
                    json += "\"lat\":" + latitude;
                    json += ",\"long\":" + longitude + "}";
                    json += "}";
                    rowCounter++;
                }
            }
            json += "]";
            return json;
        }

        public static List<IBI.Shared.Models.BusTree.Group> GetUserGroups(int userId)
        {

            List<IBI.Shared.Models.BusTree.Group> groupList = new List<IBI.Shared.Models.BusTree.Group>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    groupList = IBI.REST.Shared.DBHelper.GetUserGroups(userId, dbContext).Select(g => new IBI.Shared.Models.BusTree.Group
                    {
                        CustomerId = g.CustomerId,
                        Description = g.Description,
                        CustomerName = g.Customer.FullName,
                        GroupId = g.GroupId,
                        GroupName = g.GroupName,
                        ParentGroupId = g.ParentGroupId,
                        // ParentGroupName = g.ParentGroup.GroupName
                    }).ToList();
                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }

            return groupList;
        }

        #endregion

        #region Bus Status Log

        public static List<IBI.Shared.Models.BusTree.BusStatusLog> GetBusStatusLog()
        {
            List<IBI.Shared.Models.BusTree.BusStatusLog> list = new List<IBI.Shared.Models.BusTree.BusStatusLog>();
            //using (IBIDataModel dbContext = new IBIDataModel())
            //{
            //    list = dbContext.BusStatusLogs.OrderByDescending(c => c.Timestamp).Select(l => new IBI.Shared.Models.BusTree.BusStatusLog
            //    {
            //        ActualJourneyNumber = (int)l.ActualJourneyNumber,
            //        BusNumber = l.BusNumber,
            //        IsOK = l.IsOK,
            //        LastDCUPing = l.LastDCUPing,
            //        LastHotspotPing = l.LastHotspotPing,
            //        LastInfotainmentPing = l.LastInfotainmentPing,
            //        LastSchedule = l.LastSchedule,
            //        LastVTCPing = l.LastVTCPing,
            //        Latitude = l.Latitude,
            //        Longitude = l.Longitude,
            //        MarkedForService = l.MarkedForService ?? false,
            //        Remarks = l.Remarks,
            //        ScheduledJourneyNumber = (int)l.ScheduledJourneyNumber.Value,
            //        ServiceLevel = l.ServiceLevel ?? 0,
            //        TimeStamp = l.Timestamp
            //    }).ToList();

            //}

            return list;
        }

        #endregion

        #region Bus Event Log

        public static List<IBI.Shared.Models.BusTree.BusEventLog> GetBusEventLog(string busNumber, DateTime date)
        {
            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, string.Format("GetBusEventLog--> busNumber={0}, date={1}", busNumber, date.ToString()));

            List<IBI.Shared.Models.BusTree.BusEventLog> list = new List<IBI.Shared.Models.BusTree.BusEventLog>();

            //try
            //{
            //    DateTime startDate = date;
            //    DateTime endDate = date.AddDays(1);

            //    using (IBIDataModel dbContext = new IBIDataModel())
            //    {
            //        list = dbContext.BusEventLogs.Where(log => log.BusNumber == busNumber && log.Timestamp >= startDate && log.Timestamp <= endDate).OrderByDescending(col => col.Timestamp).Select(log => new IBI.Shared.Models.BusTree.BusEventLog
            //        {
            //            BusNumber = log.BusNumber,
            //            ClientType = log.ClientType,
            //            Destination = log.Destination,
            //            EventData = log.EventData,
            //            EventId = log.EventId,
            //            EventSource = log.EventSource,
            //            ExtraData = log.ExtraData,
            //            From = log.From,
            //            GPS1 = "",
            //            GPS2 = "",
            //            IsMaster = log.IsMaster,
            //            IsOverwritten = log.IsOverwritten,
            //            JourneyNumber = log.JourneyNumber,
            //            Line = log.Line,
            //            Result = log.Result,
            //            Source = log.Source,
            //            StopNumber = log.StopNumber,
            //            TimeStamp = log.Timestamp,
            //            Zone = log.Zone

            //        }).Take(10000).ToList();
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            //    throw ex;
            //}

            return list;
        }

        #endregion

        #region common
        public static List<IBI.Shared.Models.Customer> GetUserCustomers(int userId)
        {

            List<IBI.Shared.Models.Customer> userCustomers = new List<IBI.Shared.Models.Customer>();

            try
            {

                List<IBI.Shared.Models.BusTree.Group> groupList = BusServiceController.GetUserGroups(userId);

                var list = groupList.GroupBy(g => g.CustomerId).Select(g => g.FirstOrDefault());

                userCustomers = list.Select(g => new IBI.Shared.Models.Customer
                {
                    CustomerName = g.CustomerName,
                    CustomerId = g.CustomerId
                }).ToList();

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return userCustomers;
        }
        #endregion

        #region Client Configurations

        public static List<IBI.Shared.Models.Configuration> GetUserConfigurationProperties(int userId)
        {

            List<IBI.Shared.Models.Configuration> userConfigProps = new List<IBI.Shared.Models.Configuration>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    List<IBI.DataAccess.DBModels.ClientConfigurationProperty> cfgsProps = dbContext.ClientConfigurationProperties.OrderBy(o=>o.PropertyName).ToList();

                    userConfigProps = cfgsProps.Select(g => new IBI.Shared.Models.Configuration
                    {
                        ConfgurationPropertyId = g.ConfgurationPropertyId,
                        PropertyName = g.PropertyName,
                        DefaultValue = g.DefaultValue
                    }).ToList();

                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return userConfigProps;
        }

        //public static Tree CreateClientConfigurationsTree(int userId, List<BusStatusColor> filters, int groupId = 0)
        public static Tree CreateClientConfigurationsTree(int userId, int groupId = 0)
        {
            try
            {
                List<BusNode> rootGroups = new List<BusNode>();

                IBIDataModel dbContext = new IBIDataModel();

                //Dictionary<string, int> counters = new Dictionary<string, int>();
                //counters.Add("GREEN", 0);
                //counters.Add("GREY", 0);
                //counters.Add("LIGHT_GREY", 0);
                //counters.Add("ORANGE", 0);
                //counters.Add("RED", 0);
                //counters.Add("YELLOW", 0);
                //counters.Add("PURPLE", 0);
                //counters.Add("IN_OPERATION", 0);
                //counters.Add("INACTIVE", 0);

                var uGroups = dbContext.Users.FirstOrDefault(x => x.UserId == userId).UserGroups;

                foreach (var uGroup in uGroups)
                {
                    var groups = groupId > 0 ? uGroup.Groups.Where(x => x.GroupId == groupId) : uGroup.Groups;

                    foreach (var group in groups)
                    {
                        //GroupNode gNode = SetConfigurationGroupNode(group, filters, counters);
                        GroupNode gNode = SetConfigurationGroupNode(group);
                        GroupNode rootGNode = GetParentGroupNode(group, gNode);

                        MergeWithExistingRootNodes(rootGroups, rootGNode);

                        //break;
                    }
                }

                //counters["IN_OPERATION"] = counters["GREEN"] + counters["GREY"] + counters["ORANGE"] + counters["RED"] + counters["YELLOW"] + counters["PURPLE"];
                //counters["INACTIVE"] = counters["LIGHT_GREY"];

                return new Tree(rootGroups, userId);
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }

        //recursive function to populate every group and underlying buses.
        //private static GroupNode SetConfigurationGroupNode(IBI.DataAccess.DBModels.Group group, List<BusStatusColor> filters = null, Dictionary<string, int> counters = null)
        private static GroupNode SetConfigurationGroupNode(IBI.DataAccess.DBModels.Group group)
        {
            try
            {
                using (IBIDataModel db = new IBIDataModel())
                {



                    List<BusNode> children = new List<BusNode>(); //for tree node children

                    //get child groups
                    List<IBI.DataAccess.DBModels.Group> childGroups = group.Groups.ToList();

                    foreach (IBI.DataAccess.DBModels.Group grp in childGroups)
                    {
                        //children.Add(SetConfigurationGroupNode(grp, filters, counters));
                        children.Add(SetConfigurationGroupNode(grp));
                        // break;
                    }

                    //get customer config for BusTreeService
                    BusTreeCustomer custConfig = BusTreeServiceConfigurations.GetCustomerSettings(group.CustomerId.Value);



                    //get buses 
                    List<IBI.DataAccess.DBModels.Bus> childBuses = group.Buses.ToList();

                    foreach (IBI.DataAccess.DBModels.Bus bus in childBuses)
                    {
                        //set every child bus node

                        //BusDetail busDetail = GetBusDetail(bus, custConfig);

                        //prepare a busNode for this bus
                        //BusNode busNode = BusNode.CreateBusNode(bus.BusNumber, busDetail);
                        BusNode busNode = BusNode.CreateBusNode(bus);

                        foreach (IBI.DataAccess.DBModels.ClientConfiguration cfg in bus.ClientConfigurations)
                        {
                            //busNode.ClientConfigurationsValues += cfg.ConfigurationPropertyId.ToString() + "::" + cfg.PropertyValue + "&&";
                            busNode.ClientConfigurationsValues += "###" + cfg.ConfigurationPropertyId.ToString() + "::" + cfg.PropertyValue + "&&&";
                        }

                        //increment color counters here
                        //if (busDetail.BusStatus != null && counters != null)
                        //{
                        //    int valCounter = 0;
                        //    counters.TryGetValue(busDetail.BusStatus.ToString(), out valCounter);
                        //    valCounter++;
                        //    counters[busDetail.BusStatus.ToString()] = valCounter;
                        //}

                        //decide here to either show this bus in result set or not?
                        //if (filters != null)
                        //{

                        //    foreach (BusStatusColor color in filters)
                        //    {
                        //        if (busDetail.BusStatus == color)
                        //            children.Add(busNode); // only add those buses to result set, which has colors matched in filters.
                        //    }
                        //}
                        children.Add(busNode);

                    }

                    //set current node
                    GroupNode node = GroupNode.CreateGroup(group.GroupName, group.GroupId, Convert.ToInt32(group.CustomerId), children);

                    //ClientConfigurations
                    foreach (IBI.DataAccess.DBModels.ClientConfiguration cfg in group.ClientConfigurations)
                    {
                        node.ClientConfigurationsValues += "###" + cfg.ConfigurationPropertyId.ToString() + "::" + cfg.PropertyValue + "&&&";
                    }

                    return node;

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return null;

        }

        #endregion
    }
}