﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using IBI.Shared.Models;

namespace IBI.REST.BusTree
{
    
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")] 
    public interface IBusTreeService
    {
        //[OperationContract]
        //[WebInvoke(Method = "GET",
        //    RequestFormat = WebMessageFormat.Json,
        //    ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.Bare,
        //    UriTemplate = "BusTree")]
        //List<IBI.Shared.Models.BusTree.BusNode> GetBusTree();

        //[OperationContract]
        //[WebInvoke(Method = "GET",
        //    RequestFormat = WebMessageFormat.Json,
        //    ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.Bare,
        //    UriTemplate = "BusTree/{userId}")]
        //List<IBI.Shared.Models.BusTree.BusNode> GetBusTreeByUserId(string userId);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare,
             ResponseFormat = WebMessageFormat.Json,
             UriTemplate = "Json/Groups/{userId}")]
        List<IBI.Shared.Models.BusTree.Group> GetGroups(string userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle =WebMessageBodyStyle.Bare,
            UriTemplate = "SimpleBusTree?userId={userId}&groupId={groupId}&customerId={customerId}&inOperation={inOperation}")]
        IBI.Shared.Models.BusTree.Tree GetSimpleBusTree(string userId, string groupId, string customerId, string inOperation);


        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "GetBusGroupTree?userId={userId}&groupId={groupId}")]
        IBI.Shared.Models.BusTree.Tree GetBusGroupTree(string userId, string groupId);



        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "BusTree")]
        IBI.Shared.Models.BusTree.Tree GetBusTree(IBI.Shared.Models.BusTree.Tree emptyTree);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Group/{groupId}")]
        IBI.Shared.Models.BusTree.Group GetGroup(string groupId);


        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Bus?c={customerId}&b={busNumber}")]
        IBI.Shared.Models.BusTree.Bus GetBus(string customerId, string busNumber);


        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "ImportLiveDB")]
        bool ImportLiveDB();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.BusTree.BusDetail> GetBuses(int customerId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "BusDetail/{busNumber}")]
        IBI.Shared.Models.BusTree.BusDetail GetBusDetail(string busNumber);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "GetBusLookUpDetail/{busNumber}")]
        List<IBI.Shared.Models.BusTree.BusDetail> GetBusLookUpDetail(string busNumber);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetBusServiceTickets(string busNumber, int customerId);


        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        bool ChangeBusGroup(int customerId, string busNumber, int groupId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        bool ChangeBusStatus(int customerId, string busNumber, bool InOperation);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        bool ModifyBusAlias(int customerId, string busNumber, string busAlias);


        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<string> GetAllBusNumbers(int customerId = 0, bool InOperation = true, bool checkInOperation = true);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.BusTree.BusStatusLog> GetBusStatusLog();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.BusTree.BusEventLog> GetBusEventLog(string busNumber, DateTime date);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Client/{macAddress}")]
        List<IBI.Shared.Models.BusTree.Client> GetClient(string macAddress);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<String> GetUserBuses(int userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<String> GetUserBusesHavingVTC(int userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetUserSpecificBuses(string filter,string customerIds, bool offline, bool online);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetBusClientsLastPings(string busNumber, int customerId);


        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetAllUsersBuses(int groupId = 0, string busNumber = "");

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetBusesDataForMap(int scheduleId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.BusTree.Group> GetUserGroups(int userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.Customer> GetUserCustomers(int userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.Configuration> GetUserConfigurationProperties(int userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        ServiceResponse<bool> DeleteBus(string busNumber, int customerId, string username);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "ClientConfigurationsTree")]
        IBI.Shared.Models.BusTree.Tree GetClientConfigurationsTree(IBI.Shared.Models.BusTree.Tree emptyTree);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        ServiceResponse<bool> SetClientConfigurations(int ConfigurationPropertyId, string PropertyValue, int GroupId, int CustomerID, string BusNumber);
    }
}
