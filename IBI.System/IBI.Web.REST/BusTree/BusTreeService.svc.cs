﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using IBI.Shared.Models.BusTree;
using System.ServiceModel.Web;
using IBI.REST.Shared;
using System.ServiceModel.Activation;
using IBI.Shared.Diagnostics;
using System.IO;
using System.Configuration;
using IBI.Shared;
using System.Net.Mail;

namespace IBI.REST.BusTree
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI", MaxItemsInObjectGraph = 65536)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BusTreeService : IBusTreeService
    {

        //public List<BusNode> GetBusTree()
        //{
        //    return BusTreeServiceController.CreateBusTree();
        //}

        //public List<BusNode> GetBusTreeByUserId(string userId)
        //{
        //    return BusTreeServiceController.CreateBusTree(Convert.ToInt32(userId), 0);
        //}
        public List<IBI.Shared.Models.BusTree.Group> GetGroups(string userId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetGroups"))
            {
                List<IBI.Shared.Models.BusTree.Group> list = BusServiceController.GetGroups(int.Parse(userId));
                return list;
            }
            
        }

        public bool ChangeBusGroup(int customerId, string busNumber, int groupId)
        {
            using (new IBI.Shared.CallCounter("BusTree.ChangeBusGroup"))
            {
                return BusServiceController.ChangeBusGroup(customerId, busNumber, groupId);
            }
            
        }

        public bool ChangeBusStatus(int customerId, string busNumber, bool InOperation)
        {
            using (new IBI.Shared.CallCounter("BusTree.ChangeBusStatus"))
            {
                return BusServiceController.ChangeBusStatus(customerId, busNumber, InOperation);
            }
            
        }

        public bool ModifyBusAlias(int customerId, string busNumber, string busAlias)
        {
            using (new IBI.Shared.CallCounter("BusTree.ModifyBusAlias"))
            {
                return BusServiceController.ModifyBusAlias(customerId, busNumber, busAlias);
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <param name="customerId"></param>
        /// <param name="inOperation">if inOperation == true then only those buses will be fetched which are InOperation, otherwise all the buses. By Default all the buses</param>
        /// <returns></returns>
        public Tree GetSimpleBusTree(string userId, string groupId, string customerId, string inOperation)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetSimpleBusTree"))
            {
                int uid = int.Parse(userId);
                int grpId = int.Parse(groupId);
                int custId = int.Parse(customerId);
                bool inOp = inOperation.ToLower() == "true" ? true : false;

                Tree t = BusServiceController.CreateSimpleBusTree(uid, grpId, custId, inOp);
                return t;
            }
            
        }

        public Tree GetBusGroupTree(string userId, string groupId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusGroupTree"))
            {
                int uid = int.Parse(userId);
                int grpId = int.Parse(groupId);
                Tree t = BusServiceController.CreateGroupTree(uid, grpId);
                return t;
            }
            
        }

        public Tree GetBusTree(Tree emptyTree)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusTree"))
            {
                return BusServiceController.CreateBusTree(emptyTree.UserID, emptyTree.Filters, 0);
            }
            
        }

        public IBI.Shared.Models.BusTree.Group GetGroup(string groupId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetGroup"))
            {
                return BusServiceController.GetGroup(Convert.ToInt32(groupId));
            }
            
        }

        public IBI.Shared.Models.BusTree.Bus GetBus(string customerId, string busNumber)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBus"))
            {
                return BusServiceController.GetBus(Convert.ToInt32(customerId), busNumber);
            }
            
        }

        public bool ImportLiveDB()
        {
            using (new IBI.Shared.CallCounter("BusTree.ImportLiveDB"))
            {
                IBIDataModel dbContext = new IBIDataModel();
                int val = dbContext.ImportLiveDB();

                return true;
            }
            
        }


        public List<BusDetail> GetBuses(int customerId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBuses"))
            {
                IBIDataModel dbContext = new IBIDataModel();
                var buses = dbContext.Buses.Where(b => b.CustomerId == customerId).Select(b => new BusDetail
                {
                    BusNumber = b.BusNumber,
                    CustomerID = b.CustomerId,
                    CustomerName = b.Customer.FullName,
                    LastTimeInService = DateTime.Now,
                    InOperation = (bool)b.InOperation,
                    ServiceLevel = (int)b.ServiceLevel
                });
                return buses.ToList();
            }
            
        }

        public IBI.Shared.Models.BusTree.BusDetail GetBusDetail(string busNumber)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusDetail"))
            {
                BusDetail bDetail = BusServiceController.GetBusDetail(busNumber);
                return bDetail;
            }
            
        }


        public List<IBI.Shared.Models.BusTree.BusDetail> GetBusLookUpDetail(string busNumber)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusLookUpDetail"))
            {
                List<BusDetail> bDetail = BusServiceController.GetBusLookUpDetail(busNumber);
                return bDetail;
            }
            
        }


        public string GetBusServiceTickets(string busNumber, int customerId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusServiceTickets"))
            {
                string busServiceTickets = BusServiceController.GetBusServiceTickets(busNumber, customerId);
                return busServiceTickets;
            }
           
        }


        public List<IBI.Shared.Models.BusTree.Client> GetClient(string macAddress)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetClient"))
            {
                return BusServiceController.GetClient(macAddress);
            }
            
        }

        public List<string> GetAllBusNumbers(int customerId = 0, bool InOperation = true, bool checkInOperation = true)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetAllBusNumbers"))
            {
                return BusServiceController.GetAllBusNumbers(customerId, InOperation, checkInOperation);
            }
            
        }

        public List<String> GetUserBusesHavingVTC(int userId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetUserBusesHavingVTC"))
            {
                return BusServiceController.GetUserBusesHavingVTC(userId);
            }
            
        }

        public List<String> GetUserBuses(int userId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetUserBuses"))
            {
                return BusServiceController.GetUserBuses(userId);
            }
            
        }

        public string GetUserSpecificBuses(string filter, string customerIds, bool offline, bool online)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetUserSpecificBuses"))
            {
                return BusServiceController.GetUserSpecificBuses(filter, customerIds, offline, online);
            }
            
        }

        public string GetBusClientsLastPings(string busNumber, int customerId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetUserSpecificBuses"))
            {
                return BusServiceController.GetBusClientsLastPings(busNumber, customerId);
            }
            
        }

        public string GetAllUsersBuses(int groupId = 0, string busNumber = "")
        {
            using (new IBI.Shared.CallCounter("BusTree.GetAllUsersBuses"))
            {
                return BusServiceController.GetAllUsersBuses(groupId, busNumber);
            }
            
        }


        public string GetBusesDataForMap(int scheduleId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusesDataForMap"))
            {
                return BusServiceController.GetBusesDataForMap(scheduleId);
            }
            
        }


        public List<IBI.Shared.Models.BusTree.Group> GetUserGroups(int userId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetUserGroups"))
            {
                return BusServiceController.GetUserGroups(userId);
            }
            
        }

        public List<IBI.Shared.Models.Customer> GetUserCustomers(int userId)
        {

            //List<IBI.Shared.Models.Customer> userCustomers = new List<IBI.Shared.Models.Customer>();

            //try
            //{

            //    List<IBI.Shared.Models.BusTree.Group> groupList = BusServiceController.GetUserGroups(userId);

            //    var list = groupList.GroupBy(g => g.CustomerId).Select(g=>g.FirstOrDefault());

            //    userCustomers = list.Select(g => new IBI.Shared.Models.Customer
            //      {
            //        CustomerName = g.CustomerName,
            //        CustomerId = g.CustomerId
            //      }).ToList();

            //}
            //catch (Exception ex)
            //{
            //    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            //}
            //return userCustomers;            
            using (new IBI.Shared.CallCounter("BusTree.GetUserCustomers"))
            {
                return BusServiceController.GetUserCustomers(userId);
            }
            
        }

        public List<IBI.Shared.Models.Configuration> GetUserConfigurationProperties(int userId)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetUserConfigurationProperties"))
            {
                return BusServiceController.GetUserConfigurationProperties(userId);
            }
            
        }

        #region Bus Status Log

        public List<IBI.Shared.Models.BusTree.BusStatusLog> GetBusStatusLog()
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusStatusLog"))
            {
                return BusServiceController.GetBusStatusLog();
            }
            
        }
        #endregion

        #region Bus Event Log

        public List<IBI.Shared.Models.BusTree.BusEventLog> GetBusEventLog(string busNumber, DateTime date)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusEventLog"))
            {
                return BusServiceController.GetBusEventLog(busNumber, date);
            }
            
        }
        #endregion

        #region Bus Deletion
        private string defaultDateFormat = "yyyy-MM-dd HH:mm:ss";
        public ServiceResponse<bool> DeleteBus(string busNumber, int customerId, string username)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool>();
            using (new IBI.Shared.CallCounter("BusTree.DeleteBus"))
            {
                try
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        //gather data for logs
                        //Clients

                        var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == customerId).FirstOrDefault();

                        if (bus == null)
                        {
                            response.Data = false;
                            response.Message = "Bus does not exist";
                        }
                        else
                        {
                            var clients = bus.Clients.ToList();
                            var groups = bus.Groups.ToList();
                            var land2BusMsgs = bus.Land2BusMessages.ToList();
                            var bus2LandMsgs = bus.Bus2LandMessages.ToList();
                            var journeys = bus.Journeys.ToList();

                            //Log bus's data here
                            StringBuilder log = new StringBuilder();
                            GetBusDataLog(bus, groups, username, log);
                            GetClientDataLog(clients, log);
                            //GetBusGroupsDataLog(groups, log);
                            GetLand2BusMsgsDataLog(land2BusMsgs, log);
                            GetBus2LandMsgsDataLog(bus2LandMsgs, log);
                            GetJourneysDataLog(journeys, log);

                            //Save logs
                            FileInfo logFile = LogDeletedBusData(log, customerId, busNumber);

                            //delete bus
                            dbContext.DeleteBus(busNumber, customerId);

                            SendBusDeletionNotificationEmail(busNumber, customerId, bus.Customer.FullName, username, logFile);

                            response.Data = true;
                            response.Message = "Bus deleted successfully";
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.BUS_OPERATIONS, Logger.GetDetailedError(ex));
                    response.Message = "System fails to delete this bus right now. Please try later";
                    response.Data = false;
                }

                return response;
            }
            
        }
         
        private void SendBusDeletionNotificationEmail(string busNumber, int customerId, string customerName, string username, FileInfo logFile)
        {
            using (new IBI.Shared.CallCounter("BusTree.SendBusDeletionNotificationEmail"))
            {
                try
                {

                    StringBuilder body = new StringBuilder();

                    body.AppendFormat("Bus # {0} for customer {1} ({2}) has been deleted from IBI System by '{3}'. A log file is generated for this action.<br /><br /> -IBI System.",
                         busNumber, customerId, customerName, username);

                    List<Attachment> attachments = new List<Attachment>();

                    //if (logFile != null)
                    //{
                    //    MemoryStream memoryStream = new MemoryStream(File.ReadAllBytes(logFile.FullName));

                    //    Attachment attachement = new Attachment(memoryStream, logFile.Name);
                    //    attachement.ContentId = logFile.Name;

                    //    attachments.Add(attachement);
                    //}


                    List<string> toEmails = AppSettings.NotificationEmail("BusDeletion", customerId.ToString());

                    foreach (string toEmail in toEmails)
                    {
                        Common.Mail.SendSimpleMail("noreply@mermaid.dk", toEmail, string.Format("Bus {0} Deleted", busNumber), body.ToString(), null);
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.BUS_OPERATIONS, string.Format("Bus Deletion: Failed to send notification email. [Bus {0}, Customer: {1}, Error: {2}]", busNumber, customerId, Logger.GetDetailedError(ex)));
                }
            }
            
        }


        private void GetBusDataLog(DataAccess.DBModels.Bus bus, List<DataAccess.DBModels.Group> groups, string username, StringBuilder log)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBusDataLog"))
            {
                string separator = "\n\n-------\n\n";

                log.AppendLine("Bus Detail");
                log.AppendLine(string.Format("CustomerId: {0},\n\rCustomer: {1},\n BusNumber: {2},\n Alias: {3},\n Last Time in Service: {4},\n Date Added: {5},\n Date Deleted: {6}, \n Deleted By: {7}" + separator,
                     bus.CustomerId, bus.Customer.FullName, bus.BusNumber, bus.BusAlias, bus.LastTimeInService, bus.DateAdded, DateTime.Now, username));

                log.AppendLine("GROUP/LINE");

                string g = string.Empty;
                foreach (var group in groups)
                {
                    log.AppendLine(string.Format("{0}/{1}\n\n", group.ParentGroup != null ? group.ParentGroup.GroupName : "", group.GroupName));
                }

                log.AppendLine("--------------------------------\n\n");
            }
        }

        private void GetClientDataLog(List<DataAccess.DBModels.Client> clients, StringBuilder log)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetClientDataLog"))
            {
                log.AppendLine("Clients");
                int counter = 1;
                foreach (var client in clients)
                {
                    log.AppendLine(counter.ToString() + ":");
                    log.AppendLine(string.Format("Client Type: {0},\n MAC: {1},\n SIM: {2},\n Last Ping: {3}\n\n",
                         client.ClientType, client.MacAddress, client.SimID, client.LastPing));

                    counter++;
                }

                log.AppendLine("--------------------------------\n\n");
            }
        }

        //private void GetBusGroupsDataLog(List<DataAccess.DBModels.Group> groups, StringBuilder log)
        //{
        //    log.AppendLine("GROUP/LINE");

        //    string g = string.Empty;
        //    foreach (var group in groups)
        //    {
        //        log.AppendFormat("{0}/{1}\n\n", group.ParentGroup != null ? group.ParentGroup.GroupName : "", group.GroupName);
        //    }

        //    log.AppendLine("--------------------------------\n\n");
        //}

        private void GetLand2BusMsgsDataLog(List<DataAccess.DBModels.Land2BusMessages> msgs, StringBuilder log)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetLand2BusMsgsDataLog"))
            {
                log.AppendLine("LAND to BUS Messages");
                int counter = 1;
                foreach (var msg in msgs)
                {
                    log.AppendLine(string.Format("#{0},\n DATE ADDED: {1},\n SENDER: {2},\n SUBJECT: {3},\n CATEGORY: {4},\n MESSAGE: {5},\n VALID FROM: {6},\n VALID TO: {7} \n\n------\n\n",
                          counter.ToString(), msg.DateAdded, msg.Sender, msg.Subject, msg.Land2BusMessageCategory.CategoryName, msg.MessageText, msg.ValidFrom, msg.ValidTo));

                    counter++;
                }

                log.AppendLine("--------------------------------\n\n");
            }
        }


        private void GetBus2LandMsgsDataLog(List<DataAccess.DBModels.Bus2LandMessages> msgs, StringBuilder log)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetBus2LandMsgsDataLog"))
            {
                log.AppendLine("BUS to LAND Messages");
                int counter = 1;
                foreach (var msg in msgs)
                {
                    log.AppendLine(string.Format("#{0}\n DATE ADDED: {1},\n DESTINATION: {2},\n LINE: {3},\n LONGITUDE: {4},\n LATITIDE: {5},\n MAC: {6},\n MESSAGE TEXT: {7} \n\n------\n\n",
                          counter.ToString(), msg.DateAdded, msg.Destination, msg.Line, msg.Longitude, msg.Latitude, msg.MacAddress, msg.MessageText));

                    counter++;
                }

                log.AppendLine("--------------------------------\n\n");
            }
        }


        private void GetJourneysDataLog(List<DataAccess.DBModels.Journey> journeys, StringBuilder log)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetJourneysDataLog"))
            {
                log.AppendLine("JOURNEYS");
                int counter = 1;
                foreach (var j in journeys)
                {
                    log.AppendLine(string.Format("#{0}\n: JourneyId: {1},\n LAST UPDATED: {2},\n LINE: {3},\n DESTINATION: {4},\n VIA: {5},\n START TIME: {6},\n END TIME: {7}\n\n------\n\n",
                          counter.ToString(), j.JourneyId, j.LastUpdated, j.Schedule.Line, j.Schedule.Destination, j.Schedule.ViaName, j.StartTime, j.EndTime));
                    counter++;
                }

                log.AppendLine("--------------------------------\n\n");
            }
        }


        private FileInfo LogDeletedBusData(StringBuilder log, int customerId, string busNumber)
        {
            using (new IBI.Shared.CallCounter("BusTree.LogDeletedBusData"))
            {
                string resourcePath = ConfigurationManager.AppSettings["ResourceDirectory"];
                FileInfo logFile = new FileInfo(Path.Combine(resourcePath, Path.Combine("Deleted Buses", customerId.ToString()), busNumber + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt"));
                try
                {
                    if (!logFile.Directory.Exists)
                    {
                        logFile.Directory.Create();
                    }

                    File.WriteAllText(logFile.FullName, log.ToString());

                    return logFile;

                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.BUS_OPERATIONS, "DELETE BUS --> Filed to write Log file" + Logger.GetDetailedError(ex));

                }

                return null;
            }
        }
        #endregion

        #region Client Configurations

        public Tree GetClientConfigurationsTree(Tree emptyTree)
        {
            using (new IBI.Shared.CallCounter("BusTree.GetClientConfigurationsTree"))
            {
                return BusServiceController.CreateClientConfigurationsTree(emptyTree.UserID, 0);
            }
        }

        public ServiceResponse<bool> SetClientConfigurations(int ConfigurationPropertyId, string PropertyValue, int GroupId, int CustomerID, string BusNumber)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool>();
            using (new IBI.Shared.CallCounter("BusTree.SetClientConfigurations"))
            {
                try
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        //response.Data = true;
                        if (GroupId > 0)
                        {
                            var ClientConfiguration = dbContext.ClientConfigurations.Where(c =>
                               c.ConfigurationPropertyId == ConfigurationPropertyId &&
                               c.GroupId == GroupId).FirstOrDefault();

                            if (ClientConfiguration != null)
                            {
                                if (string.IsNullOrEmpty(PropertyValue))
                                {
                                    // delete 
                                    dbContext.ClientConfigurations.Remove(ClientConfiguration);
                                }
                                else
                                {   // update
                                    ClientConfiguration.PropertyValue = PropertyValue;
                                }

                                dbContext.SaveChanges();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(PropertyValue))
                                {
                                    ClientConfiguration cc = new ClientConfiguration
                                    {
                                        ConfigurationPropertyId = ConfigurationPropertyId,
                                        PropertyValue = PropertyValue,
                                        GroupId = GroupId,

                                    };
                                    dbContext.ClientConfigurations.Add(cc);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            var ClientConfiguration = dbContext.ClientConfigurations.Where(c =>
                                c.ConfigurationPropertyId == ConfigurationPropertyId &&
                                c.CustomerID == CustomerID &&
                                c.BusNumber == BusNumber
                                ).FirstOrDefault();

                            if (ClientConfiguration != null)
                            {
                                if (string.IsNullOrEmpty(PropertyValue))
                                {
                                    // delete 
                                    dbContext.ClientConfigurations.Remove(ClientConfiguration);
                                }
                                else
                                {   // update
                                    ClientConfiguration.PropertyValue = PropertyValue;
                                }

                                dbContext.SaveChanges();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(PropertyValue))
                                {
                                    ClientConfiguration cc = new ClientConfiguration
                                    {
                                        ConfigurationPropertyId = ConfigurationPropertyId,
                                        PropertyValue = PropertyValue,
                                        CustomerID = CustomerID,
                                        BusNumber = BusNumber

                                    };
                                    dbContext.ClientConfigurations.Add(cc);
                                    dbContext.SaveChanges();
                                }
                            }
                        }

                        response.Data = true;

                    }

                }
                catch (Exception ex)
                {
                    response.Message = "System fails to SetClientConfigurations this bus or node right now. Please try later";
                    response.Data = false;
                }

                return response;
            }
        }
        #endregion
    }

}
