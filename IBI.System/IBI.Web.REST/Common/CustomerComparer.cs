﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.REST.Common
{ 

    public class CustomerComparer : IEqualityComparer<IBI.Shared.Models.Customer>
    {
        public int Compare(IBI.Shared.Models.Customer c1, IBI.Shared.Models.Customer c2)
        {
             

            //if (IsNumeric(s1) && IsNumeric(s2))
            //{
            //    if (Convert.ToInt32(s1) > Convert.ToInt32(s2)) return 1;
            //    if (Convert.ToInt32(s1) < Convert.ToInt32(s2)) return -1;
            //    if (Convert.ToInt32(s1) == Convert.ToInt32(s2)) return 0;
            //}

            //if (c1.CustomerId == c2.CustomerId)
            //    return 1;
            //else
            //    return -1;
            
            return string.Compare(c1.CustomerId.ToString(), c2.CustomerId.ToString(), true);
        }
         
    }
}