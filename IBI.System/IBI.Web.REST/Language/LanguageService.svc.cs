﻿using IBI.Shared.Models.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace IBI.REST.Language
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
   public class LanguageService : ILanguageService
    {
       public List<LanguageLabel> GetDCULanguage(string language)
       {
           using (new IBI.Shared.CallCounter("LanguageService.GetDCULanguage"))
           {
               return LanguageServiceController.GetDCULanguageTags(language);
           }
           
       }

       public List<LanguageLabel> GetWebLanguage(string language)
       {
           using (new IBI.Shared.CallCounter("LanguageService.GetWebLanguage"))
           {
               return LanguageServiceController.GetWebLanguageTags(language);
           }
           
       }
    }
}
