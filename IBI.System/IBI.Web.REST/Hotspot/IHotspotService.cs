﻿using IBI.Shared.Models;
using IBI.Shared.Models.Hotspot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;



namespace IBI.REST.Hotspot
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHotspotService" in both code and config file together.
    //[ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    [ServiceContract]
    public interface IHotspotService
    {
        [OperationContract]
        [WebGet]
        ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>> Hotspots(int id, string name, bool fetchAccessPoints);


        [OperationContract]
        [WebGet]
        ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointGroup>> AccessPointGroups(int hotspotId, int id, string name, bool fetchAccessPoints);

        [OperationContract]
        [WebGet]
        ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPoint>> AccessPoints(int hotspotId, int accessPointGroupId, int id, string name, string mac, string searchMode);

        [OperationContract]
        [WebGet]
        ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointDailyActivity>> AccessPointDailyActivity(int accessPointId, DateTime startDate, DateTime endDate);

        [OperationContract]
        [WebInvoke(Method = "POST")]
        ServiceResponse<bool> SaveAccessPoint(IBI.Shared.Models.Hotspot.AccessPoint accessPoint);

    }
}
