﻿using IBI.Shared.Models.Movia.HogiaTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;

namespace IBI.REST.Journey
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBIJourney")]
    public interface IJourneyService
    {
        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetActiveJourney(int scheduleNumber, int customerId, string busNumber);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetActiveJourneyByClientRef(int scheduleNumber, int customerId, string busNumber, string clientRef);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        string GetActiveFutureJourney(string journeyData);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        string SaveActiveJourney(int journeyNumber, string stopName, string journeyXml);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        string UpdateActiveSchedule(int customerId, string busNumber, int scheduleNumber);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        void GenerateFutureJourneys(int customerId, int numberOfJourneys, int hoursAfter);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        [AspNetCacheProfile("JourneyListCache")]
        string GetFutureJourneys(int customerid, int period);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        [AspNetCacheProfile("JourneyListCache")]
        string GetFutureJourneysWithVehicleSchedules(int customerid, int period);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare)]
        [AspNetCacheProfile("SpecificJourneyCache")]
        string GetFutureJourney(int journeyId);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        string GetCorrespondingTraffic(int customerId, string busNumber, string stopNumber, string line, string dateTime);


        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Models.Journey.Journey> GetJourneys(int customerId, DateTime sDate, DateTime eDate, int pageIndex, int pageSize);

        #region MoviaRealTime
        [OperationContract]
        [WebGet]
        string MoviaLines(string busNumber, int customerId, string gps);

        [OperationContract]
        [WebGet]
        string MoviaLineJourneys(string busNumber, int customerId, string gps, string line);

        [OperationContract]
        [WebGet]
        string MoviaJourneySignOn(string busNumber, int customerId, string journeyId, string line, string destination, string viaName, bool force);


        [OperationContract]
        [WebGet]
        MoviaJourney DownloadMoviaJourney(string busNumber, int customerId);

        #endregion

        #region Journey Predictions

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        string GetJourneyPredictions(int journeyId, string lastModified);


        #endregion
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml)]
        string SaveMessage(string msg);


    }
}
