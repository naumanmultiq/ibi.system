﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Xml;
using System.Globalization;
using IBI.Shared.Models.Movia.HogiaTypes;

namespace IBI.REST.Journey
{
    [XmlSerializerFormat]
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBIJourney", ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class JourneyService : IJourneyService
    {
        public string GetActiveJourney(int scheduleNumber, int customerId, string busNumber)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetActiveJourney"))
            {
                return JourneyServiceController.GetActiveJourney(scheduleNumber, customerId, busNumber);
            }
            
        }

        public string GetActiveJourneyByClientRef(int scheduleNumber, int customerId, string busNumber, string clientRef)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetActiveJourneyByClientRef"))
            {
                return JourneyServiceController.GetActiveJourney(scheduleNumber, customerId, busNumber, clientRef);
            }
            
        }

        public string SaveActiveJourney(int journeyNumber, string stopName, string journeyXml)
        {
            using (new IBI.Shared.CallCounter("JourneyService.SaveActiveJourney"))
            {
                return JourneyServiceController.SaveActiveJourney(journeyNumber, stopName, journeyXml);
            }
            
        }

        public string UpdateActiveSchedule(int customerId, string busNumber, int scheduleNumber)
        {
            using (new IBI.Shared.CallCounter("JourneyService.UpdateActiveSchedule"))
            {
                return JourneyServiceController.UpdateActiveSchedule(customerId, busNumber, scheduleNumber);
            }
            
        }

        public string GetActiveFutureJourney(string journeyData)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetActiveFutureJourney"))
            {
                return JourneyServiceController.GetActiveFutureJourney(journeyData);
            }
            
        }

        public void GenerateFutureJourneys(int customerId, int numberOfJourneys, int hoursAfter)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GenerateFutureJourneys"))
            {
                JourneyServiceController.GenerateFutureJourneys(customerId, numberOfJourneys, hoursAfter);
            }
            
        }

        public string GetFutureJourney(int journeyId)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetFutureJourney"))
            {
                return JourneyServiceController.GetFutureJourney(journeyId);
            }
            
        }

        public string GetFutureJourneys(int customerid, int period)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetFutureJourneys"))
            {
                return JourneyServiceController.GetFutureJourneys(customerid, period);
            }
            
        }


        public string GetFutureJourneysWithVehicleSchedules(int customerId, int period)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetFutureJourneysWithVehicleSchedules"))
            {
                return JourneyServiceController.GetFutureJourneysWithVehicleSchedules(customerId, period);
            }
            
        }

        public string GetCorrespondingTraffic(int customerId, string busNumber, string stopNumber, string line, string dateTime)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetCorrespondingTraffic"))
            {
                return JourneyServiceController.GetCorrespondingTraffic(customerId, busNumber, stopNumber, line, dateTime);
            }
            
        }

        public string SaveMessage(string msg)
        {
            using (new IBI.Shared.CallCounter("JourneyService.SaveMessage"))
            {
                return msg;
            }
            
        }


        public List<IBI.Shared.Models.Journey.Journey> GetJourneys(int customerId, DateTime sDate, DateTime eDate, int pageIndex, int pageSize)
        {
            List<IBI.Shared.Models.Journey.Journey> list = new List<IBI.Shared.Models.Journey.Journey>();
            using (new IBI.Shared.CallCounter("JourneyService.GetJourneys"))
            {
                try
                {

                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        list = dbContext.GetOperationsData_Journeys_2(customerId, sDate, eDate, pageIndex, pageSize).Select(j => new IBI.Shared.Models.Journey.Journey
                        {
                            Row = j.Row,
                            CustomerId = j.CustomerId,
                            ScheduleId = j.ScheduleId,

                            Line = j.Line,
                            FromName = j.FromName,
                            Destination = j.Destination,
                            ViaName = j.ViaName,
                            BusNumber = j.BusNumber,
                            ClientRef = j.ClientRef,

                            EndTime = j.EndTime,
                            ExternalReference = j.ExternalReference,
                            JourneyId = j.JourneyId,
                            JourneyStateId = j.JourneyStateId,
                            LastUpdated = j.LastUpdated,
                            PlannedEndTime = j.PlannedEndTime,
                            PlannedStartTime = j.PlannedStartTime,

                            StartTime = j.StartTime,
                            PunctualityData = j.PunctualityData,
                            JourneyStartTime = j.JourneyStartTime,
                            TotalRows = j.TotalRows

                        }).ToList();

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            //DateTime sDate = DateTime.ParseExact(date.ToString("yyyy-MM-dd") + " " + sTime, "yyyy-MM-dd H:mm", CultureInfo.InvariantCulture);
            //DateTime eDate = DateTime.ParseExact(date.ToString("yyyy-MM-dd") + " " + eTime, "yyyy-MM-dd H:mm", CultureInfo.InvariantCulture);
            
            return list;

        }


        #region MOVIA

        //55.7473751649053,12.4949772866192
        public string MoviaLines(string busNumber, int customerId, string gps)
        {
            //int nWaited = 0;
            //int maxWait = 300;
            //while (nWaited < maxWait)
            //{
            //    System.Threading.Thread.Sleep(5000);
            //    nWaited += 5;
            //}
            using (new IBI.Shared.CallCounter("JourneyService.MoviaLines"))
            {
                return JourneyServiceController.MoviaLines(busNumber, customerId, gps);
            }
            
        }

        public string MoviaLineJourneys(string busNumber, int customerId, string gps, string line)
        {
            //int nWaited = 0;
            //int maxWait = 300;
            //while (nWaited < maxWait)
            //{
            //    System.Threading.Thread.Sleep(5000);
            //    nWaited += 5;
            //}
            using (new IBI.Shared.CallCounter("JourneyService.MoviaLineJourneys"))
            {
                return JourneyServiceController.MoviaLineJourneys(busNumber, customerId, gps, line);
            }
            
        }

        public string MoviaJourneySignOn(string busNumber, int customerId, string journeyId, string line, string destination, string viaName, bool force)
        {
            using (new IBI.Shared.CallCounter("JourneyService.MoviaJourneySignOn"))
            {
                return JourneyServiceController.MoviaJourneySignOn(busNumber, customerId, journeyId, line, destination, viaName, force);
            }
            
        }

        public MoviaJourney DownloadMoviaJourney(string busNumber, int customerId)
        {
            using (new IBI.Shared.CallCounter("JourneyService.DownloadMoviaJourney"))
            {
                return JourneyServiceController.DownloadMoviaJourney(busNumber, customerId);
            }
            
        }

        #endregion


        #region Journey Prediction

        public string GetJourneyPredictions(int journeyId, string lastModified)
        {
            using (new IBI.Shared.CallCounter("JourneyService.GetJourneyPredictions"))
            {
                return JourneyServiceController.GetJourneyPredictions(journeyId, lastModified);
            }
            
        }

        #endregion


    }
}
