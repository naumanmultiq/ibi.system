﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using Acapela.Vaas;
using IBI.DataAccess.DBModels;
using IBI.Shared;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models;
using mermaid.BaseObjects.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.Collections;
using System.Text;
using IBI.Shared.Common;
using IBI.Shared.Models.Movia.HogiaTypes;
using System.Web;


namespace IBI.REST.Journey
{
    public class JourneyServiceController
    {
        private static bool IsLastStop(string journeyXml, string stopName)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(journeyXml);
            XmlNodeList nodes = doc.SelectNodes("//Journey/JourneyStops/StopInfo");
            if (nodes != null && nodes.Count > 0)
            {
                return (String.Compare(nodes[nodes.Count - 1].SelectSingleNode("StopName").InnerText, stopName, true) == 0);
            }
            return false;
        }

        public static string UpdateActiveSchedule(int customerId, string busNumber, int scheduleNumber)
        {
            string result = "status:success";
            return result;
        }

        public static string GetActiveJourney(int scheduleNumber, int customerId, string busNumber, string clientRef = null)
        {
            string result = string.Empty;


            try
            {
                //DateTime? plannedStartTime = null;
                //DateTime? plannedEndTime = null;
                //string journeyStops = string.Empty;
                //GetJourneyTimestamp(customerId, busNumber, scheduleNumber, ref plannedStartTime, ref plannedEndTime , ref journeyStops);

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    //string output= String.Join("",dbContext.xml_GetActiveJourney(scheduleNumber, customerId, busNumber, plannedStartTime, plannedEndTime).ToArray<string>());
                    //string output = String.Join("", dbContext.xml_GetActiveJourney(scheduleNumber, customerId, busNumber, plannedStartTime == DateTime.MinValue ? null : plannedStartTime.ToString("yyyy-MM-ddTHH:mm:ss"), plannedEndTime == DateTime.MinValue ? null : plannedEndTime.ToString("yyyy-MM-ddTHH:mm:ss"), null).ToArray<string>());
                    //string output = String.Join("", dbContext.xml_GetActiveJourney(scheduleNumber, customerId, busNumber, plannedStartTime, plannedEndTime, null, AppSettings.MoviaJourneySynLogEnabled()).ToArray<string>());
                    string output = String.Join("", dbContext.xml_GetActiveJourney(scheduleNumber, customerId, busNumber, clientRef, AppSettings.MoviaJourneySynLogEnabled()).ToArray<string>());

                    output = IBI.Shared.AppUtility.FixScheduleStopAddonsXML(output);
                    output = IBI.Shared.AppUtility.FixScheduleAddonsXML(output, "Journey");

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(output);

                    result = xDoc.OuterXml;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY, ex);
            }

            return result;
        }

        public static string SaveActiveJourney(int currentJourneyNumber, string stopName, string stopData)
        {
            string result = string.Empty;

            ///TODO
            ///rewrite complete logic of SaveActiveJourney
            /// Update JourneyStops with ActualArrival and ActualDeparture timestamps for this stop, based on StopId(StopNumber)
            /// if this is first update call then update StartTime for Journey table as well
            /// if this is last stop then update Endtime, Active=0 in Journey Table
            /// Update ahead and behind for this stop - Query from DB script(2264)
            /// Check if valid cache file
            ///     if yes
            ///         then update planned times for JourneyStops if they are not popupalted already. 
            ///         Make entries for Estimated times for remaining stops in prediction table
            ///     if no
            ///         do nothing


            /*
             * 
             * <Stop>
                 <StopId>9025200000000400</StopId>
                 <PlanDifference>-47</PlanDifference>
                 <JourneyNumber>3377382</JourneyNumber>
                 <ClientRef/>
                 <Status>LATE</Status>
                 <BusNumber>1113</BusNumber>
                 <ActualArrival>2014-08-07T12:46:28</ActualArrival>
                 <ActualDeparture>2014-08-07T12:46:37</ActualDeparture>
                 <ScheduleNumber>915</ScheduleNumber>
                 <PlannedArrival>2014-08-07T11:59:00</PlannedArrival>
                 <PlannedDeparture>2014-08-07T11:59:00</PlannedDeparture>
                 <FromName>R?dovrehallen</FromName>
                 <ViaName/>
                 <IsLastStop>false</IsLastStop>
                 <StopSequence>3</StopSequence>
                 <CustomerId>2140</CustomerId>
                 <ExternalReference>281</ExternalReference>
                </Stop>
             * 
             */

            try
            {
                XmlDocument stopXmlDoc = new XmlDocument();
                stopXmlDoc.LoadXml(stopData);

                //The Client which won't pass ExternalReference in stopData would be excluded from SaveActiveJourney Implementation
                if (stopXmlDoc.SelectSingleNode("//Stop/ExternalReference") == null)
                    return "";
                ///////////////////////////////////////////////////////////////////////////////////


                string actualDeparture = stopXmlDoc.SelectSingleNode("//Stop/ActualDeparture").InnerText;
                string actualArrival = stopXmlDoc.SelectSingleNode("//Stop/ActualArrival").InnerText;

                ////////string plannedDeparture = stopXmlDoc.SelectSingleNode("//Stop/PlannedDeparture").InnerText;
                ////////string plannedArrival = stopXmlDoc.SelectSingleNode("//Stop/PlannedArrival").InnerText;

                string stopId = stopXmlDoc.SelectSingleNode("//Stop/StopId").InnerText;

                string scheduleId = stopXmlDoc.SelectSingleNode("//Stop/ScheduleNumber").InnerText;

                string busNumber = stopXmlDoc.SelectSingleNode("//Stop/BusNumber").InnerText;
                int customerId = stopXmlDoc.SelectSingleNode("//Stop/CustomerId") == null ? 2140 : int.Parse(stopXmlDoc.SelectSingleNode("//Stop/CustomerId").InnerText);

                string schedulRef = "";
                string scheduleXml = string.Empty;

                DateTime? actualDepartureTime = null;
                DateTime? actualArrivalTime = null;
                //////DateTime? plannedDepartureTime = null;
                //////DateTime? plannedArrivalTime = null;

                //////if (!String.IsNullOrEmpty(plannedDeparture))
                //////    plannedDepartureTime = DateTime.ParseExact(plannedDeparture, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                //////if (!String.IsNullOrEmpty(plannedArrival))
                //////    plannedArrivalTime = DateTime.ParseExact(plannedArrival, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);


                if (!String.IsNullOrEmpty(actualDeparture))
                    actualDepartureTime = DateTime.ParseExact(actualDeparture, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                if (!String.IsNullOrEmpty(actualArrival))
                    actualArrivalTime = DateTime.ParseExact(actualArrival, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);



                string lastStopString = stopXmlDoc.SelectSingleNode("//Stop/IsLastStop").InnerText;

                bool isLastStop = false;

                if (lastStopString == "1" || lastStopString == "true")
                {
                    isLastStop = true;
                }



                string journeyStops = string.Empty;
                //////DateTime? plannedStartTime = null;
                //////DateTime? plannedEndTime = null;
                //////GetJourneyTimestamp(customerId, busNumber, int.Parse(scheduleId), ref plannedStartTime, ref plannedEndTime, ref journeyStops);

                //call DB SP
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    // dbContext.xml_UpdateStop(currentJourneyNumber, plannedStartTime, plannedEndTime, decimal.Parse(stopId), actualArrivalTime, actualDepartureTime, isLastStop, externalRef, journeyStops, stopData);
                    string output = String.Join("", dbContext.xml_UpdateStop(currentJourneyNumber, actualArrivalTime, actualDepartureTime, isLastStop, stopData, AppSettings.MoviaJourneySynLogEnabled()).ToArray<string>());

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(output);

                    result = xDoc.OuterXml;

                    // result = result.Replace("</Data>", "<Behind></Behind></Data>");
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY, ex);
            }

            return result;
        }


        public static string GetActiveFutureJourney(string journeyData)
        {
            string result = string.Empty;

            //journeyData="journeyId=123;customerId=2140;overwrite=true;"
            Hashtable pingValues = Shared.RestUtil.ExtractPingValues(journeyData);

            int customerId = 0;
            string busNumber = "";
            int journeyId = 0;
            string clientRef = string.Empty;
            bool makeCopy = false;
            DateTime startTime = DateTime.Now;


            int.TryParse((String)pingValues["customerid"], out customerId);
            busNumber = pingValues["busnumber"].ToString();
            int.TryParse((String)pingValues["journeyid"], out journeyId);
            clientRef = pingValues["clientref"].ToString();
            startTime = DateTime.ParseExact(pingValues["starttime"].ToString(), "yyyy-MM-ddTHH:mm:ss:fff", System.Globalization.CultureInfo.InvariantCulture);
            if (pingValues.ContainsKey("overwrite"))
            {
                bool.TryParse((String)pingValues["overwrite"], out makeCopy);
            }

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    string output = String.Join("", dbContext.xml_GetActiveFutureJourney(customerId, busNumber, journeyId, clientRef, startTime, makeCopy, AppSettings.MoviaJourneySynLogEnabled()).ToArray<string>());

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(output);

                    result = xDoc.OuterXml;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY, ex);
            }

            return result;
        }

        /// <summary>
        /// planned departure & arrival read from cache - schedule file (if it's not present in journey record)
        /// </summary>
        /// <param name="currentJourney"></param>
        private static void GetJourneyTimestamp(int customerId, string busNumber, int scheduleId, ref DateTime? plannedDepartureTime, ref DateTime? plannedArrivalTime, ref string journeyStops)
        {
            {
                string schFilePath = Path.Combine(ConfigurationManager.AppSettings["ResourceDirectory"], String.Format("{0}\\Cache\\Schedule_{1}.xml", customerId, busNumber));

                if (File.Exists(schFilePath))
                {
                    FileInfo fl = new FileInfo(schFilePath);

                    if (fl.LastWriteTime > DateTime.Now.AddMinutes(-10))
                    {

                        string scheduleText = File.ReadAllText(schFilePath, System.Text.Encoding.UTF8);
                        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                        MemoryStream journeyStream = new MemoryStream(encoding.GetBytes(scheduleText));

                        XDocument xDoc = XDocument.Load(journeyStream);

                        string scheduleNumber = (from p in xDoc.Descendants("ScheduleNumber")
                                                 select p.Value).FirstOrDefault();

                        if (!(string.IsNullOrEmpty(scheduleNumber)))
                        {
                            if (scheduleId == int.Parse(scheduleNumber))
                            {

                                String tmpplannedDepartureTime = (from p in xDoc.Descendants("PlannedDepartureTime")
                                                                  select p.Value).FirstOrDefault();
                                String tmpplannedArrivalTime = (from p in xDoc.Descendants("PlannedDepartureTime")
                                                                select p.Value).LastOrDefault();

                                bool correctDate;
                                if (!string.IsNullOrEmpty(tmpplannedArrivalTime))
                                {
                                    DateTime pArrival;
                                    correctDate = DateTime.TryParseExact(tmpplannedArrivalTime, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out pArrival);

                                    if (correctDate)
                                        plannedArrivalTime = pArrival;
                                }

                                if (!string.IsNullOrEmpty(tmpplannedDepartureTime))
                                {
                                    DateTime pDeparture;
                                    correctDate = DateTime.TryParseExact(tmpplannedDepartureTime, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out pDeparture);

                                    if (correctDate)
                                        plannedDepartureTime = pDeparture;
                                }


                                journeyStops = (from s in xDoc.Descendants("JourneyStops")
                                                select s).LastOrDefault().ToString(SaveOptions.DisableFormatting);
                            }
                        }


                    }


                }
            }
        }


        /// <summary>
        /// planned departure & arrival read from cache - schedule file (if it's not present in journey record)
        /// </summary>
        /// <param name="currentJourney"></param>
        private static void SetCurrentJourneyPlannedStamps(ref DataAccess.DBModels.Journey currentJourney)
        {

            if (currentJourney.PlannedStartTime == null || currentJourney.PlannedEndTime == null)
            {
                string schFilePath = Path.Combine(ConfigurationManager.AppSettings["ResourceDirectory"], String.Format("{0}\\Cache\\Schedule_{1}.xml", currentJourney.CustomerId, currentJourney.BusNumber));

                if (File.Exists(schFilePath))
                {
                    string scheduleText = File.ReadAllText(schFilePath, System.Text.Encoding.UTF8);
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    MemoryStream journeyStream = new MemoryStream(encoding.GetBytes(scheduleText));

                    XDocument xDoc = XDocument.Load(journeyStream);

                    //XDocument xDoc = XDocument.Load(schFilePath);


                    var plannedDepartureTime = (from p in xDoc.Descendants("PlannedDepartureTime")
                                                select p.Value).FirstOrDefault();
                    var plannedArrivalTime = (from p in xDoc.Descendants("PlannedDepartureTime")
                                              select p.Value).LastOrDefault();

                    bool correctDate;
                    if (!string.IsNullOrEmpty(plannedArrivalTime))
                    {
                        DateTime pArrival;
                        correctDate = DateTime.TryParseExact(plannedArrivalTime, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out pArrival);

                        if (correctDate)
                            currentJourney.PlannedEndTime = pArrival;
                    }

                    if (!string.IsNullOrEmpty(plannedDepartureTime))
                    {
                        DateTime pDeparture;
                        correctDate = DateTime.TryParseExact(plannedDepartureTime, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out pDeparture);

                        if (correctDate)
                            currentJourney.PlannedStartTime = pDeparture;
                    }
                }
            }
        }

        public static string GetFutureJourneys(int customerid, int period)
        {
            try
            {
                string result = string.Empty;
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    int noOfHours = period == 0 ? 24 : period;
                    string output = String.Join("", dbContext.GetFutureJourneysList(noOfHours, customerid).ToArray<string>());
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(output);
                    result = xDoc.OuterXml;
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
            }
            return string.Empty;
        }

        public static string GetFutureJourneysWithVehicleSchedules(int customerId, int period)
        {
            try
            {
                StringBuilder result = new StringBuilder();
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    int noOfHours = period == 0 ? 24 : period;
                    var data = dbContext.GetFutureJourneysList2(noOfHours, customerId).ToList();
                    result.Append("<VehicleSchedules>");

                    foreach (var vs in data.Select(d => new { d.VehicleScheduleId, d.VSExternalReference, d.VSPlannedStartTime, d.VSPlannedEndTime }).Distinct())
                    {
                        StringBuilder journey = new StringBuilder();
                        foreach (var j in data.Where(j => j.VehicleScheduleId == vs.VehicleScheduleId))
                        {
                            journey.Append("<Journey ");

                            if (j.CustomerId != null)
                                journey.Append(string.Format(" CustomerId = \"{0}\"", j.CustomerId));
                            if (j.ScheduleId != null)
                                journey.Append(string.Format(" ScheduleId = \"{0}\"", j.ScheduleId));
                            if (j.JourneySequence != null)
                                journey.Append(string.Format(" SequenceNumber = \"{0}\"", j.JourneySequence));
                            if (j.JourneyId != null)
                                journey.Append(string.Format(" JourneyId = \"{0}\"", j.JourneyId));
                            if (j.Line != null)
                                journey.Append(string.Format(" Line = \"{0}\"", j.Line));
                            if (j.FromName != null)
                                journey.Append(string.Format(" FromName = \"{0}\"", j.FromName));
                            if (j.Destination != null)
                                journey.Append(string.Format(" Destination = \"{0}\"", j.Destination));
                            if (j.ViaName != null)
                                journey.Append(string.Format(" ViaName = \"{0}\"", j.ViaName));
                            if (j.PlannedDepartureTime != null)
                                journey.Append(string.Format(" PlannedDepartureTime = \"{0}\"", j.PlannedDepartureTime.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
                            if (j.PlannedArrivalTime != null)
                                journey.Append(string.Format(" PlannedArrivalTime = \"{0}\"", j.PlannedArrivalTime.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
                            if (j.Latitude != null)
                                journey.Append(string.Format(" Latitude = \"{0}\"", j.Latitude));
                            if (j.Longitude != null)
                                journey.Append(string.Format(" Longitude = \"{0}\"", j.Longitude));
                            if (j.LastUpdated != null)
                                journey.Append(string.Format(" LastUpdated = \"{0}\"", j.LastUpdated.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
                            if (j.ExternalReference != null)
                                journey.Append(string.Format(" ExternalReference = \"{0}\"", j.ExternalReference));
                            journey.Append(" />");
                        }//end journey loop


                        StringBuilder vehicleSchedule = new StringBuilder();

                        if (vs.VehicleScheduleId != null)
                            vehicleSchedule.Append(string.Format("<VehicleScheduleId>{0}</VehicleScheduleId>", vs.VehicleScheduleId));
                        else
                            vehicleSchedule.Append("<VehicleScheduleId />");

                        if (vs.VSExternalReference != null)
                            vehicleSchedule.Append(string.Format("<ExternalReference>{0}</ExternalReference>", vs.VSExternalReference));
                        else
                            vehicleSchedule.Append("<ExternalReference />");

                        if (vs.VSPlannedStartTime != null)
                            vehicleSchedule.Append(string.Format("<PlannedStartTime>{0}</PlannedStartTime>", vs.VSPlannedStartTime.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
                        else
                            vehicleSchedule.Append("<PlannedStartTime />");

                        if (vs.VSPlannedEndTime != null)
                            vehicleSchedule.Append(string.Format("<PlannedEndTime>{0}</PlannedEndTime>", vs.VSPlannedEndTime.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
                        else
                            vehicleSchedule.Append("<PlannedEndTime />");

                        result.Append(string.Format(@"<VehicleSchedule>{0}<Journeys>{1}</Journeys></VehicleSchedule>", vehicleSchedule.ToString(), journey.ToString()));

                    }//end VS loop

                    result.Append("</VehicleSchedules>");

                    //string output = String.Join("", dbContext.xml_FutureJourneysList(customerId, noOfHours).ToArray<string>());
                    //XmlDocument xDoc = new XmlDocument();
                    //xDoc.LoadXml(output);
                    //result = xDoc.OuterXml;
                    return result.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
            }
            return string.Empty;
        }

        public static string GetFutureJourney(int journeyId)
        {
            try
            {
                string result = string.Empty;

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    string output = String.Join("", dbContext.xml_FutureJourney(journeyId).ToArray<string>());

                    output = IBI.Shared.AppUtility.FixScheduleStopAddonsXML(output); // scheduleStopAddons must be set before scheduleAddons
                    output = IBI.Shared.AppUtility.FixScheduleAddonsXML(output, "Journey");


                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(output);

                    result = xDoc.OuterXml;
                    return result;
                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
            }
            return string.Empty;
        }

        public static void GenerateFutureJourneys(int customerId, int numberOfJourneys, int hoursAfter)
        {
            ///TODO
            ///implement this function if needed to populate sample future journeys

            //try
            //{
            //    using (IBIDataModel dbContext = new IBIDataModel())
            //    {
            //        var schedules = (from x in dbContext.Schedules
            //                         where x.CustomerID == customerId
            //                         select x).ToList();

            //        DateTime now = DateTime.Now;
            //        now = now.AddHours(hoursAfter);
            //        DateTime journeyTimeStamp = now;
            //        int difference = 10;
            //        foreach (var schedule in schedules)
            //        {
            //            journeyTimeStamp = now.AddMinutes(difference);
            //            for (int counter = 1; counter <= numberOfJourneys; counter++)
            //            {
            //                DataAccess.DBModels.Journey journey = new DataAccess.DBModels.Journey();
            //                journey.CustomerId = schedule.CustomerID;
            //                journey.BusNumber = "0";
            //                journey.ScheduleNumber = schedule.ScheduleID;
            //                journey.JourneyDate = journeyTimeStamp;
            //                journey.Line = schedule.Line;
            //                journey.StartPoint = schedule.FromName;
            //                journey.Destination = schedule.Destination;
            //                // schedule to journey
            //                XmlDocument doc = new XmlDocument();
            //                doc.LoadXml(schedule.ScheduleXML);

            //                XmlDocument docNew = new XmlDocument();
            //                XmlElement newRoot = docNew.CreateElement("Journey");
            //                docNew.AppendChild(newRoot);
            //                newRoot.InnerXml = doc.DocumentElement.InnerXml;

            //                journey.PlannedArrivalTime = journeyTimeStamp;
            //                XmlNodeList nodes = docNew.SelectNodes("/Journey/JourneyStops/StopInfo");
            //                if (nodes == null || nodes.Count == 0)
            //                    continue;

            //                XmlElement ns = docNew.CreateElement("GPSCoordinateNS");
            //                ns.InnerText = nodes[0].SelectSingleNode("GPSCoordinateNS").InnerText;

            //                XmlElement ew = docNew.CreateElement("GPSCoordinateEW");
            //                ew.InnerText = nodes[0].SelectSingleNode("GPSCoordinateEW").InnerText;

            //                docNew.DocumentElement.PrependChild(ns);
            //                docNew.DocumentElement.PrependChild(ew);

            //                foreach (XmlNode node in nodes)
            //                {
            //                    XmlElement pa = docNew.CreateElement("PlannedArrivalTime");
            //                    pa.InnerText = journeyTimeStamp.ToString();
            //                    journeyTimeStamp = journeyTimeStamp.AddMinutes(2);
            //                    XmlElement pd = docNew.CreateElement("PlannedDepartureTime");
            //                    pd.InnerText = journeyTimeStamp.ToString();

            //                    XmlElement ea = docNew.CreateElement("EstimatedArrivalTime");
            //                    ea.InnerText = string.Empty;
            //                    XmlElement ed = docNew.CreateElement("EstimatedDepartureTime");
            //                    ed.InnerText = string.Empty;

            //                    XmlElement aa = docNew.CreateElement("ActualArrivalTime");
            //                    aa.InnerText = string.Empty;

            //                    XmlElement ad = docNew.CreateElement("ActualDepartureTime");
            //                    ad.InnerText = string.Empty;

            //                    node.AppendChild(pa);
            //                    node.AppendChild(pd);
            //                    node.AppendChild(ea);
            //                    node.AppendChild(ed);
            //                    node.AppendChild(aa);
            //                    node.AppendChild(ad);

            //                    journeyTimeStamp = journeyTimeStamp.AddSeconds(30);
            //                }
            //                journey.PlannedDepartureTime = journeyTimeStamp;
            //                dbContext.Journeys.Add(journey);
            //                dbContext.SaveChanges();


            //                XmlElement jn = docNew.CreateElement("JourneyNumber");
            //                jn.InnerText = journey.JourneyNumber.ToString();
            //                docNew.DocumentElement.PrependChild(jn);

            //                // Create MD5
            //                XmlElement md5 = docNew.CreateElement("md5");
            //                md5.InnerText = AppUtility.ConvertStringtoMD5(doc.OuterXml);
            //                docNew.DocumentElement.PrependChild(md5);

            //                journey.JourneyData = docNew.OuterXml;

            //                dbContext.SaveChanges();
            //                journeyTimeStamp = journey.JourneyDate.Value.AddMinutes(5);
            //            }

            //        }
            //        dbContext.SaveChanges();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, ex);
            //}
        }


        public static string GetCorrespondingTraffic(int customerId, string busNumber, string stopNumber, string currentLine, string dateTime)
        {

            //return IBI.Web.CorrespondingTraffic.CorrespondingTrafficController.GetCorrespondingTraffic(customerId, busNumber, stopNumber, currentLine, dateTime);

            int showJourneys = int.Parse(ConfigurationSettings.AppSettings["CTraffic_ShowJourneys"]);

            DateTime serviceDateTime = DateTime.Now;

            if (!String.IsNullOrEmpty(dateTime))
            {
                serviceDateTime = DateTime.ParseExact(dateTime, "yyyy-MM-ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture);

            }

            IBI.Shared.Interfaces.ISyncScheduleFactory dBoardFactory = new IBI.Implementation.Schedular.SyncScheduleFactory();
            IBI.Shared.Interfaces.ISyncDepartureBoard dBoard = dBoardFactory.GetSyncDepartureBoard(customerId);

            string strTraffic = String.Empty;


            strTraffic = dBoard.GetDepartureBoard(busNumber, stopNumber, currentLine, showJourneys, serviceDateTime);



            string result = "";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strTraffic);

            //return doc.OuterXml;

            XslCompiledTransform xslTransform = new XslCompiledTransform();
            StringWriter writer = new StringWriter();
            xslTransform.Load(System.Web.HttpContext.Current.Server.MapPath("~/Journey/CorrespondingTraffic.xslt"));
            xslTransform.Transform(doc.CreateNavigator(), null, writer);

            result = writer.ToString();

            return result;
        }

        #region Private Helper


        public static string ConvertStringtoMD5(string strword)
        {
            return mermaid.BaseObjects.IO.FileHash.FromStream(GenerateStreamFromString(strword)).ToString();
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        private static void LinkJourneys(int journeyNumber, int journeyAhead, int journeyBehind, IBIDataModel dbContext)
        {
            //if (journeyAhead != 0)
            //{
            //    var journeyAheadObj = (from x in dbContext.Journeys
            //                           where x.JourneyNumber == journeyAhead
            //                           select x).FirstOrDefault();
            //    if (journeyAheadObj != null)
            //    {
            //        journeyAheadObj.JourneyBehind = journeyNumber;
            //    }
            //}
            //if (journeyBehind != 0)
            //{
            //    var journeyBehindObj = (from x in dbContext.Journeys
            //                            where x.JourneyNumber == journeyBehind
            //                            select x).FirstOrDefault();
            //    if (journeyBehindObj != null)
            //    {
            //        journeyBehindObj.JourneyAhead = journeyNumber;
            //    }
            //}
        }

        private static void AddJourneyDetail(IBIDataModel dbContext, int journeyId, string stopName, string journeyData, string stopData)
        {

            JourneyStop journeyDetail = new JourneyStop();
            journeyDetail.JourneyId = journeyId;
            journeyDetail.StopName = stopName;

            ///TODO
            ///populate journeyStops record here

            //journeyDetail.JourneyData = journeyData;
            //journeyDetail.Timestamp = DateTime.Now;
            //journeyDetail.StopData = stopData;
            //TODO:KHI:SyncDBChanges:IBIDataModel then uncomment this line
            //journeyDetail.IsLastStop = IsLastStop(JourneyData, stopName);

            dbContext.JourneyStops.Add(journeyDetail);
            dbContext.SaveChanges();

        }

        private static void UpdateJourneyDetail(IBIDataModel dbContext, int journeyId, string stopName, string journeyData, string stopData)
        {
            var journeyDetail = (from x in dbContext.JourneyStops
                                 where x.JourneyId == journeyId && x.StopName == stopName
                                 select x).FirstOrDefault();
            if (journeyDetail != null)
            {
                ///TODO
                ///update JourneyStop Row here


                //journeyDetail.JourneyData = journeyData;
                //journeyDetail.Timestamp = DateTime.Now;                
                //journeyDetail.StopData = stopData;
            }
            dbContext.SaveChanges();
        }

        public static int SyncJourney(int journeyId, int scheduleId, string clientRef, string journeyXML, bool journeyCompleted, out string resultXml)
        {


            /*
             * if journeyId is 0 and clientRef is also empty
             *  this is request to create new journey, create all stop entries in JourneyStops as well. Create XML from table for this journey and return in resultXML along with journeyId
             * if journeyId is not zero and clientRef is also empty - journeyXml - populate stops info from this xml if it is not done already in JourneyStops. Also update Journey table if needed
             * if journeyId is zero and clientRef is not empty - create journey if needed and also create all stops data from journeyXml
             */
            resultXml = "";
            return 0;
        }

        #endregion


        #region MOVIA

        internal static string MoviaLines(string busNumber, int customerId, string gps)
        {
            DateTime stime = DateTime.Now;
            int hMSecs = 0;

            string latitude = string.Empty;
            string longitude = string.Empty;

            if (!string.IsNullOrEmpty(gps))
            {
                string[] arrGps = gps.Split(',');
                if (arrGps.Length > 0)
                {
                    latitude = arrGps[0];
                }
                if (arrGps.Length > 1)
                {
                    longitude = arrGps[1];
                }

                try
                {

                    if (string.IsNullOrEmpty(latitude) || string.IsNullOrEmpty(longitude))
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.MoviaHogia, string.Format("Bad GPS provided to fetch lines from Hogia: {0}", gps));

                        return string.Format("<Lines gps=\"{0}\" />", gps);
                    }

                    RESTManager rest = RESTManager.Instance;

                    rest.BasePath = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.GetServerAddress(busNumber, customerId, "HogiaRealtimeJourneyService");
                    //bool bExtendedSearch = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.ExtendedSearchEnabled(busNumber, customerId, "HogiaRealtimeJourneyService");


                    //Line[] lines = rest.Get<Line[]>(string.Format("/{0}@{1},{2}/lines{3}", busNumber, latitude, longitude, bExtendedSearch?"?extendedSearch=true":string.Empty));
                    Line[] lines = rest.Get<Line[]>(string.Format("/{0}@{1},{2}/lines?extendedSearch=true", busNumber, latitude, longitude));

                    hMSecs = DateTime.Now.Subtract(stime).Milliseconds;

                    int tmpBusNumber = 0;

                    string data = string.Format("<Lines gps=\"{0}\">", gps);
                    if (lines == null || lines.Length == 0)
                    {
                        if (Int32.TryParse(busNumber, out tmpBusNumber) && Enumerable.Range(1600, 41).Contains(tmpBusNumber))
                        {
                            data += "<Line number=\"5\" designation=\"5C\" typeCode=\"CBUS\" />";
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("No Lines available for these parameters location busNumber={0}, gps={1} -- [{2}]", busNumber, gps, "No data returned, appending line 5"));
                        }
                    }
                    else
                    {
                        bool line5Found = false;
                        string bTempData = string.Empty;
                        foreach (Line line in lines)
                        {
                            bTempData += string.Format("<Line number=\"{0}\" designation=\"{1}\" typeCode=\"{2}\" />", line.number, line.designation, line.typeCode);
                            if (line.number == 5)
                            {
                                line5Found = true;
                            }
                        }
                        if (!line5Found && Int32.TryParse(busNumber, out tmpBusNumber) && Enumerable.Range(1600, 41).Contains(tmpBusNumber))
                        {
                            data += "<Line number=\"5\" designation=\"5C\" typeCode=\"CBUS\" />" + bTempData;
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("There is data but no line 5C for parameters location busNumber={0}, gps={1} -- [{2}]", busNumber, gps, "appending line 5"));
                        }
                        else
                        {
                            data += bTempData;
                        }
                        data += "</Lines>";
                    }


                    return data;

                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("No Lines available for these parameters location busNumber={0}, gps={1} -- [{2}]", busNumber, gps, ex.Message));
                }

                finally
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaHogia, string.Format("Bus {0} - GET LINES - Rest Call response: {1} MS, Hogia Call response: {2} MS", busNumber, DateTime.Now.Subtract(stime).Milliseconds, hMSecs));
                }

            }
            int tmpBusNo = 0;
            string exceptionData = string.Format("<Lines gps=\"{0}\">", gps);
            if (Int32.TryParse(busNumber, out tmpBusNo) && Enumerable.Range(1600, 41).Contains(tmpBusNo))
            {
                exceptionData += "<Line number=\"5\" designation=\"5C\" typeCode=\"CBUS\" />";
            }
            exceptionData += "<Line number=\"5\" designation=\"5C\" typeCode=\"CBUS\" />";
            return exceptionData;
        }

        internal static string MoviaLineJourneys(string busNumber, int customerId, string gps, string line)
        {
            DateTime stime = DateTime.Now;
            int hMSecs = 0;

            string result = string.Empty;

            string latitude = string.Empty;
            string longitude = string.Empty;

            if (!string.IsNullOrEmpty(gps))
            {
                string[] arrGps = gps.Split(',');
                if (arrGps.Length > 0)
                {
                    latitude = arrGps[0];
                }
                if (arrGps.Length > 1)
                {
                    longitude = arrGps[1];
                }

                try
                {

                    if (string.IsNullOrEmpty(latitude) || string.IsNullOrEmpty(longitude))
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.MoviaHogia, string.Format("Bad GPS provided to fetch lines from Hogia: {0}", gps));

                        result = string.Format("<Journeys gps=\"{0}\" line=\"{1}\" />", gps, line);
                        goto Return;
                    }

                    RESTManager rest = RESTManager.Instance;

                    rest.BasePath = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.GetServerAddress(busNumber, customerId, "HogiaRealtimeJourneyService");
                    //bool bExtendedSearch = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.ExtendedSearchEnabled(busNumber, customerId, "HogiaRealtimeJourneyService");


                    IBI.Shared.Models.Movia.HogiaTypes.Journey[] journeys = null;
                    bool bException = false;
                    try
                    {
                        journeys = rest.Get<IBI.Shared.Models.Movia.HogiaTypes.Journey[]>(string.Format("/{0}@{1},{2}/lines/{3}/journeys", busNumber, latitude, longitude, line));

                    }
                    catch (Exception exJourneys)
                    {
                        bException = true;
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("No Journeys available for these parameters location busNumber={0}, gps={1}, Line={2} -- [{3}]", busNumber, gps, line, exJourneys.Message));

                    }
                    //if (bExtendedSearch && (bException || journeys == null || journeys.Length == 0))
                    bool bExtended = false;
                    if ((bException || journeys == null || journeys.Length == 0))
                    {
                        journeys = rest.Get<IBI.Shared.Models.Movia.HogiaTypes.Journey[]>(string.Format("/{0}@{1},{2}/lines/{3}/journeys?extendedSearch=true", busNumber, latitude, longitude, line));
                        bExtended = true;
                    }
                    hMSecs = DateTime.Now.Subtract(stime).Milliseconds;

                    string data = string.Format("<Journeys gps=\"{0}\" line=\"{1}\">", gps, line);

                    if (journeys != null && journeys.Length>0)
                    {
                        journeys = journeys.OrderBy(jj => jj.isAssigned).ThenBy(jj => jj.plannedStartDateTime).ToArray<IBI.Shared.Models.Movia.HogiaTypes.Journey>();
                        foreach (IBI.Shared.Models.Movia.HogiaTypes.Journey j in journeys)
                        {
                            if (bExtended && DateTime.Now.AddMinutes(15) < DateTime.Parse(j.plannedStartDateTime))
                                continue;
                            //<Journey id="26.1.lines" lineNumber="1" destination="budding st." score="0.922333" isAssigned="false" isCancelled="false" plannedStartDateTime="2015-06-15T13:40:00" plannedEndDateTime="2015-06-15T15:20:00" />
                            data += string.Format("<Journey id=\"{0}\" lineNumber=\"{1}\" destination=\"{2}\" score=\"{3}\" isAssigned=\"{4}\" isCancelled=\"{5}\" plannedStartDateTime=\"{6}\" plannedEndDateTime=\"{7}\" line=\"{8}\" />",
                                 j.id, j.isOnLine.number, j.destination, j.score, j.isAssigned, j.isCanceled, j.plannedStartDateTime, j.plannedEndDateTime, j.isOnLine.designation);
                        }
                    }
                    
                    data += "</Journeys>";

                    result = data;

                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("No Journeys available for these parameters location busNumber={0}, gps={1}, Line={2} -- [{3}]", busNumber, gps, line, ex.Message));
                    result = string.Format("<Journeys gps=\"{0}\" line=\"{1}\" />", gps, line);
                }

                finally
                {

                    Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaHogia, string.Format("Bus {0} - GET JOURNEY - Rest Call response: {1} MS, Hogia Call response: {2} MS", busNumber, DateTime.Now.Subtract(stime).Milliseconds, hMSecs));

                }



            }

        Return:
            return result;
            //    XmlDocument xDoc = new XmlDocument();
            //xDoc.LoadXml(result);
            //return xDoc;


        }

        //        internal static string MoviaJourneySignOn(string busNumber, int customerId, string journeyId, bool force)
        //        {
        //            string result = string.Empty;
        //            try
        //            {
        //                RESTManager rest = RESTManager.Instance;
        //                rest.BasePath = GetMoviaHogiaJourneyServerPath(busNumber); //new Uri(System.Configuration.ConfigurationSettings.AppSettings["HogiaBasePath"].ToString());

        //                result = rest.Post(string.Format("/{0}/assignment/{1}?force={2}", busNumber, journeyId, (force ? "True" : "False")), new byte[] { });

        //                //return result;
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, ex);
        //            }

        //            string[] arrResult = result.Split('|');

        //            string message = "";

        //            if (arrResult.Length > 1)
        //            {
        //                //arrResult[1] = arrResult[1].Replace("\"", "").Replace("{", "").Replace("}", "").Replace("\r", "").Replace("\n", "").Trim();

        //                if (arrResult[1].ToLower().Contains("message"))
        //                {
        //                    message = arrResult[1].Replace("\"message\":", "");

        //                    message = message.Trim().Substring(message.IndexOf("\"") + 1, message.IndexOf("\","));
        //                    message = message.Replace("\r", "").Replace("\n", "").Replace("\",  \"", "").Trim();
        //                }
        //                //else
        //                //{
        //                //    message = arrResult[1].Trim();
        //                //}
        //            }


        //            string data = string.Format(@"<Response>
        //                           <ValidationNeeded>{0}</ValidationNeeded>
        //                          <Language name='default'>
        //                           <Message><![CDATA[{1}]]></Message>
        //                           <Buttons>
        //                            <Button action='Ok'>{2}</Button>
        //                            <Button action='Cancel'>{3}</Button>
        //                          </Buttons>
        //                          </Language>
        //                        </Response>",
        //                                   result.StartsWith("SUCCESS") ? "false" : "true",
        //                                   message,
        //                                   "[SIGHNAGE_OkTextButton]", "[SIGHNAGE_CancelTextButton]"
        //                                   );


        //            //return string.Format("<SignOn status=\"{0}\"><![CDATA[{1}]]></SignOn>", arrResult[0].Trim(), arrResult[1]);
        //            return data;

        //        }

        internal static string MoviaJourneySignOn(string busNumber, int customerId, string journeyId, string line, string destination, string viaName, bool force)
        {
            DateTime stime = DateTime.Now;
            int hMSecs = 0;

            string result = string.Empty;
            try
            {
                RESTManager rest = RESTManager.Instance;
                rest.BasePath = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.GetServerAddress(busNumber, customerId, "HogiaRealtimeJourneyService");


                result = rest.Post(string.Format("/{0}/assignment/{1}?force={2}", busNumber, journeyId, (force ? "True" : "False")), new byte[] { });

                hMSecs = DateTime.Now.Subtract(stime).Milliseconds;

                //return result;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, ex);
            }

            string[] arrResult = result.Split('|');

            string message = "";

            if (arrResult.Length > 1)
            {
                //arrResult[1] = arrResult[1].Replace("\"", "").Replace("{", "").Replace("}", "").Replace("\r", "").Replace("\n", "").Trim();

                if (arrResult[1].ToLower().Contains("message"))
                {
                    message = arrResult[1].Replace("\"message\":", "");

                    message = message.Trim().Substring(message.IndexOf("\"") + 1, message.IndexOf("\","));
                    message = message.Replace("\r", "").Replace("\n", "").Replace("\",  \"", "").Trim();
                }
                //else
                //{
                //    message = arrResult[1].Trim();
                //}
            }


            string data = string.Format(@"<Response>
                           <ValidationNeeded>{0}</ValidationNeeded>
                          <Language name='default'>
                           <Message><![CDATA[{1}]]></Message>
                           <Buttons>
                            <Button action='Ok'>{2}</Button>
                            <Button action='Cancel'>{3}</Button>
                          </Buttons>
                          </Language>
                        </Response>",
                                   result.StartsWith("SUCCESS") ? "false" : "true",
                                   message,
                                   "[SIGHNAGE_OkTextButton]", "[SIGHNAGE_CancelTextButton]"
                                   );


            if (result.StartsWith("SUCCESS"))
            {

                try
                {
                    //IBI should queue a request to sync sign generation
                    IBI.Shared.MSMQ.MSMQManager qm = new IBI.Shared.MSMQ.MSMQManager(ConfigurationManager.AppSettings["SignQueueName"].ToString());
                    string msg = string.Format("{0}|{1}|{2}|{3}", customerId, line, HttpContext.Current.Server.UrlDecode(destination), viaName);
                    qm.Send(msg, msg);
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, ex);
                }

                //try
                //{                    
                //    IBI.Implementation.Schedular.SignController.CheckSignForNewSchedule(customerId, line, destination, viaName, "Hogia_Journey_SignOn");
                //}
                //catch (Exception ex) 
                //{
                //    throw new Exception(string.Format("Errors while creating new sign during Hogia realtime journey sign on. [{0}]", ex.Message));
                //}
            }
            else
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("Bus {0} - Journey SIGN-ON fails - JourneyId: {1} - Hogia Response: {2} ", busNumber, journeyId, result));
            }


            //Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaHogia, string.Format("Bus {0} - SIGN-ON - Rest Call response: {1} MS, Hogia Call response: {2} MS", busNumber, DateTime.Now.Subtract(stime).Milliseconds, hMSecs));

            return data;

        }


        internal static MoviaJourney DownloadMoviaJourney(string busNumber, int customerId)
        {
            DateTime stime = DateTime.Now;
            int hMSecs = 0;

            try
            {

                RESTManager rest = RESTManager.Instance;

                rest.BasePath = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.GetServerAddress(busNumber, customerId, "HogiaRealtimeJourneyService");


                var journey = rest.Get<IBI.Shared.Models.Movia.HogiaTypes.MoviaJourney>(string.Format("/{0}/journey", busNumber));

                hMSecs = DateTime.Now.Subtract(stime).Milliseconds;


                //string data = "<Schedule xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>";

                //foreach (Line line in journey)
                //{
                //    data += string.Format("<Line number=\"{0}\" designation=\"{1}\" typeCode=\"{2}\" />", line.number, line.designation, line.typeCode);
                //}
                //data += "</Lines>";

                return journey;

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaHogia, string.Format("{0} - No Journey information for busNumber={1}, Error is [{2}]", "DownloadMoviaJourney", busNumber, ex.Message));
            }

            finally
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaHogia, string.Format("Bus {0} - GET JOURNEY - Rest Call response: {1} MS, Hogia Call response: {2} MS", busNumber, DateTime.Now.Subtract(stime).Milliseconds, hMSecs));
            }

            return null;
        }

        #endregion


        #region JourneyPrediction

        public static string GetJourneyPredictions(int journeyId, string lastModified)
        {


            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var journey = dbContext.GetJourneyPredictions(journeyId).ToList();

                    string result = string.Empty;

                    //int customerId ;
                    //int journeyId;
                    //string clientRef;


                    if (journey != null && journey.Count() > 0)
                    {
                        //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, string.Format("journey found: {0}", journeyId));

                        var j = journey.FirstOrDefault();
                        DateTime lastModified_client = DateTime.ParseExact(lastModified, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime lastModified_server = journey.OrderByDescending(js => js.EstimationTime).FirstOrDefault().EstimationTime;
                        lastModified_server = lastModified_server.AddMilliseconds(-lastModified_server.Millisecond);
                        if (lastModified_server.Ticks > lastModified_client.Ticks)
                        {

                            result += string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?><JourneyStopsPrediction><JourneyId>{0}</JourneyId><LastModified>{1}</LastModified><JourneyStops>", journeyId, lastModified_server.ToString("yyyy-MM-ddTHH:mm:ss"));

                            foreach (var stop in journey)
                            {
                                result += string.Format(@"<StopInfo StopNumber=""{0}""><StopName>{1}</StopName><StopSequence>{2}</StopSequence><EstimatedArrivalTime>{3}</EstimatedArrivalTime><EstimatedDepartureTime>{4}</EstimatedDepartureTime></StopInfo>",
                                         stop.StopId, stop.StopName, stop.EstimatedStopSequence, stop.EstimatedArrivalTime.Value.ToString("yyyy-MM-ddTHH:mm:ss"), stop.EstimatedDepartureTime.Value.ToString("yyyy-MM-ddTHH:mm:ss"));
                            }
                            result += "</JourneyStops></JourneyStopsPrediction>";


                        }
                    }

                    try
                    {
                        bool predictionQueueEnabled = bool.Parse(ConfigurationManager.AppSettings["PredictionQueueEnabled"]);

                        if (predictionQueueEnabled)
                        {

                            //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, string.Format("Making queue entry for journey: {0}", journeyId));
                            //var jr = dbContext.Journeys.First(j => j.JourneyId == journeyId);
                            //IBI should queue a request to fetch next estimate from 3rd party source
                            IBI.Shared.MSMQ.MSMQManager qm = new IBI.Shared.MSMQ.MSMQManager(ConfigurationManager.AppSettings["PredictionQueueName"].ToString());
                            //string msg = string.Format("{0}|{1}|{2}", jr.CustomerId, jr.JourneyId, jr.ClientRef);
                            string msg = string.Format("{0}", journeyId);
                            qm.Send(msg, msg);
                            //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, string.Format("success making queue entry for journey: {0}, Queue Name: {1}", journeyId, ConfigurationManager.AppSettings["PredictionQueueName"].ToString()));
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, ex);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, ex);
            }

            return string.Empty;
        }

        #endregion
    }
}
