﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IBI.REST.Journey
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBIJourney")]     
    public interface IJourneyPost
    {
        [WebInvoke(UriTemplate = "ping")]
        Stream Ping(Stream request);
        

        [WebInvoke(UriTemplate = "SubmitJourney")]
        Stream SubmitJourney(Stream request);

    }
}
