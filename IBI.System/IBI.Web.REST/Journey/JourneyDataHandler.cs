﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared;
using IBI.DataAccess.DBModels;
using System.Xml.Linq;
using System.Data.Entity;
using System.Linq;
using IBI.Shared.Models;
using IBI.Shared.Models.Schedules;
using IBI.REST.Common;


namespace IBI.REST.Journey
{
    public class JourneyDataHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        //public string JourneyId { get; set; }



        public void ProcessRequest(HttpContext context)
        {
            String journeyId = context.Request["journeyid"];

         

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            //sb.Append(GetDestinationTree(int.Parse(customerID)));
            sb.Append(GetJourneyData(journeyId));

            Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

            //Set the HTTP MIME type of the output stream 
            context.Response.ContentType = "text/xml";

            //Write the data out to the client. 
            context.Response.BinaryWrite(fileContent);
        }

        private char[] GetJourneyData(string journeyId)
        { 
            StringBuilder sb = new StringBuilder();
            int jid = int.Parse(journeyId);
            using(IBIDataModel dbContext = new IBIDataModel())
            {
                var journey = dbContext.Journeys.Where(j => j.JourneyId==jid).FirstOrDefault();
                if(journey == null)
                {
                    sb.Append(string.Format("journey journeyid=\"{0}\" valid=\"false\" />", journeyId));      
                }
                else
                { 
                    string line = journey.Schedule.Line;
                    string destination = journey.Schedule.Destination;
                    string firstStopId = journey.JourneyStops.FirstOrDefault().StopId.ToString();
                    string firstStopName = journey.JourneyStops.FirstOrDefault().StopName;
                    string firstStopPlannedDepartureTime =  ParseDateToString(journey.JourneyStops.FirstOrDefault().PlannedDepartureTime);
                    string firstStopActualDepartureTime=ParseDateToString(journey.JourneyStops.FirstOrDefault().ActualDepartureTime);
                    string lastStopId = journey.JourneyStops.LastOrDefault().StopId.ToString();
                    string lastStopName = journey.JourneyStops.LastOrDefault().StopName;
                    string lastStopPlannedArrivalTime = ParseDateToString(journey.JourneyStops.LastOrDefault().PlannedArrivalTime);
                    int lastStopSequence = journey.JourneyStops.LastOrDefault().StopSequence;
                    var lastStopEstimation = journey.JourneyStopEstimations.Where(jse => jse.EstimatedStopSequence == lastStopSequence).OrderByDescending(js=>js.EstimationTime).FirstOrDefault();

                    string lastStopEstimationArrivalTime = string.Empty; 
                    
                    if(lastStopEstimation !=null)
                    {
                        lastStopEstimationArrivalTime =  ParseDateToString(lastStopEstimation.EstimatedArrivalTime);
                    }
                    
                     
                    sb.Append(string.Format("<journey journeyid=\"{0}\" valid=\"true\">	<line>{1}</line><destination>{2}</destination><firststop id=\"{3}\" name=\"{4}\" planneddeparturetime=\"{5}\" actualdeparturetime=\"{6}\"/>	<laststop id=\"{7}\" name=\"{8}\" plannedarrivaltime=\"{9}\" estimatedarrivaltime=\"{10}\"/></journey>",
                        journeyId, line, destination, firstStopId, firstStopName, firstStopPlannedDepartureTime, firstStopActualDepartureTime, lastStopId, lastStopName, lastStopPlannedArrivalTime, lastStopEstimationArrivalTime));	
                     
                }
    
                return sb.ToString().ToCharArray();
                
            }

        }

        private static string ParseDateToString(object d)
        {
            if(d==null)
                return string.Empty;

            DateTime val = DateTime.MinValue;
            bool result=false;
            result = DateTime.TryParse(d.ToString(), out val);

            if(result)
            {
                return val.ToString("yyyy-MM-ddTHH:mm:ss");
            }

            return string.Empty;
        }



        #endregion

         


    }






}
