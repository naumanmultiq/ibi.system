﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.Messages;
using IBI.REST.Shared;
using IBI.Shared;
using Newtonsoft.Json;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace IBI.REST.Messaging
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MessageService : IMessageService 
    {
        //public List<Message> GetMessagesXML(string busNumber, string customerId)
        //{
        //    return GetMessages(busNumber, int.Parse(customerId) );
            
        //}

        public List<Message> GetMessagesJSON(string busNumber, string customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessagesJSON"))
            {
                return GetMessages(busNumber, int.Parse(customerId));
            }
            
        }

        public List<MessageCategory> GetMessagesByCategoriesJSON(string busNumber, string customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessagesByCategoriesJSON"))
            {
                return GetMessagesByCategories(busNumber, int.Parse(customerId));
            }
            
        }

        //public List<MessageCategory> GetMessagesByCategoriesXML(string busNumber, string customerId)
        //{
        //    List<MessageCategory> list = GetMessagesByCategories(busNumber, int.Parse(customerId)); 
        //    return list;
        //}

        //public Message GetMessageXML(string messageId)
        //{
        //    return GetMessage(int.Parse(messageId));
        //}

        public Message GetMessageJSON(string messageId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessageJSON"))
            {
                return GetMessage(int.Parse(messageId));
            }
            
        }

        public int SaveMessageJSON(string msg)
        {
            using (new IBI.Shared.CallCounter("MessageService.SaveMessageJSON"))
            {
                Message m = JsonConvert.DeserializeObject<Message>(msg);
                return MessageServiceController.Save(m);
            }
            
        }

        //public bool SaveMessageXML(string msg)
        //{
        //    Models.Messages.Message m = XmlUtility.Deserialize<Models.Messages.Message>(msg);
        //    return MessageServiceController.Save(m);
        //}

        //public List<MessageCategory> GetMessageCategoriesXML()
        //{
        //    return GetMessageCategories();
        //}
                       
    
        #region Incoming Messages

        public List<IncomingMessage> GetIncomingMessages(string userId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetIncomingMessages"))
            {
                return MessageServiceController.GetIncomingMessages(int.Parse(userId));
            }
           
        }

        public List<IBI.Shared.Models.Messages.IncomingMessage> GetIncomingMessagesByFilters(int userId, DateTime date, string busNumber, string messageText, string position, string destination, string line, string nextStop, int groupId, bool isArchived)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetIncomingMessagesByFilters"))
            {
                return MessageServiceController.GetIncomingMessages(userId, date, busNumber, messageText, position, destination, line, nextStop, groupId, isArchived);
            }
          
        }

        public List<IncomingMessage> GetArchiveIncomingMessages(string userId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetArchiveIncomingMessages"))
            {
                return MessageServiceController.GetIncomingMessages(int.Parse(userId), true);
            }
            
        }


        //[KHI]: MessagePing() currenly consumed from IBIService.svc.MessagePing().
        //public void MessagePing(string pingData)
        //{
        //    StringBuilder logMessage = new StringBuilder();

        //    try
        //    {
        //        if (Common.Settings.MessagePing == true)
        //        {
        //            using (new IBI.Shared.CallCounter("MessageService.MessagePing"))
        //            {
        //                Hashtable pingValues = Shared.RestUtil.ExtractPingValues(pingData);

        //                string busNumber = pingValues.ContainsKey("busNumber") ? pingValues["busNumber"].ToString() : "";

        //                string strTimestamp = pingValues.ContainsKey("timestamp") ? pingValues["timestamp"].ToString() : "";

        //                DateTime timestamp = DateTime.ParseExact(strTimestamp, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

        //                string line = pingValues.ContainsKey("line") ? pingValues["line"].ToString() : "";
        //                string destination = pingValues.ContainsKey("destination") ? pingValues["destination"].ToString() : "";
        //                string nextStop = pingValues.ContainsKey("nextStop") ? pingValues["nextStop"].ToString() : "";

        //                //string subject = pingValues.ContainsKey("title") ? pingValues["title"].ToString() : "";
        //                string MessageText = pingValues.ContainsKey("messageText") ? pingValues["messageText"].ToString() : "";
        //                string macAddress = pingValues.ContainsKey("mac") ? pingValues["mac"].ToString() : "";
        //                string latitude = pingValues.ContainsKey("lat") ? pingValues["lat"].ToString() : "0";
        //                string longitude = pingValues.ContainsKey("lon") ? pingValues["lon"].ToString() : "0";

        //                if (!string.IsNullOrEmpty(busNumber))
        //                {
        //                    int customerID = 0;
        //                    int.TryParse((string)pingValues["customerId"], out customerID);

        //                    if (customerID == 0)
        //                        customerID = DBHelper.GetBusCustomerId(busNumber);

        //                    if (!string.IsNullOrEmpty(MessageText))
        //                    {
        //                        try
        //                        {
        //                            IBI.Shared.Models.Messages.IncomingMessage msg = new Models.Messages.IncomingMessage();
        //                            msg.BusNumber = busNumber;
        //                            msg.CustomerId = DBHelper.GetBusCustomerId(busNumber);
        //                            msg.DateAdded = timestamp;
        //                            msg.DateModified = DateTime.Now;
        //                            msg.MessageText = MessageText;
        //                            //msg.ClientId = "?" to be determined while saving
        //                            msg.Destination = destination;
        //                            msg.GPS = "";
        //                            msg.Latitude = latitude;
        //                            msg.Line = line;
        //                            msg.Longitude = longitude;
        //                            msg.MacAddress = macAddress;
        //                            msg.NextStop = nextStop;
        //                            msg.IsArchived = false;

        //                            bool result = Messaging.MessageServiceController.SaveIncomingMessage(msg);
        //                        }
        //                        catch (Exception exInner)
        //                        {
        //                            Logger.appendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, "Bus: " + busNumber + ", MessagePing Error: " + exInner);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        Logger.appendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "Message Ping received with empty text message. MAC:" + macAddress);
        //                    }
        //                }
        //                else
        //                {
        //                    Logger.appendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "Ping received with empty busno. Mac: " + macAddress);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.appendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, "Exception : " + ex.Message);
        //    }
        //}



        #endregion

        #region Outgoing Messages


        public List<IBI.Shared.Models.Messages.Message> GetOutgoingMessages(int userId, string busNumber, DateTime validFrom, DateTime validTo, string title, string messageText, string messageCategoryName, int groupId, string senderName, bool showExpiredOnly)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetOutgoingMessages"))
            {
                List<IBI.Shared.Models.Messages.Message> msgs = MessageServiceController.GetOutgoingMessages(userId, busNumber, validFrom, validTo, title, messageText, messageCategoryName, groupId, senderName, showExpiredOnly);
                return msgs;
            }
           
        }

        public IBI.Shared.Models.Messages.MessageConfirmationsAndReplys GetMessageConfirmationsAndReplys(int messageId,string busNumber)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessageConfirmationsAndReplys"))
            {
                IBI.Shared.Models.Messages.MessageConfirmationsAndReplys msgs = MessageServiceController.GetMessageConfirmationsAndReplys(messageId, busNumber);
                return msgs;
            }
            
        }

        public List<IBI.Shared.Models.Messages.IncomingMessage> GetMessageReplies(int messageId, string busNumber)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessageReplies"))
            {
                List<IBI.Shared.Models.Messages.IncomingMessage> msgs = MessageServiceController.GetMessageReplies(messageId, busNumber);
                return msgs;
            }
            
        }

        public List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> GetMessageConfirmations(int messageId, string busNumber)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessageConfirmations"))
            {
                List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> msgs = MessageServiceController.GetMessageConfirmations(messageId, busNumber);
                return msgs;
            }
            
        }

        public bool SetExpireMessage(int messageId)
        {
            using (new IBI.Shared.CallCounter("MessageService.SetExpireMessage"))
            {
                return MessageServiceController.SetExpireMessage(messageId);
            }
            

        }
        #endregion


        #region Controller Calls

        public List<Message> GetMessages(string busNumber, int customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessages"))
            {
                return MessageServiceController.GetMessages(busNumber, customerId);
            }
            
        }

        public List<Message> GetArchiveMessages(string busNumber, int customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetArchiveMessages"))
            {
                return MessageServiceController.GetMessages(busNumber, customerId, false, true);
            }
            
        }

        public List<MessageCategory> GetMessagesByCategories(string busNumber, int customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessagesByCategories"))
            {
                return MessageServiceController.GetMessagesByCategories(busNumber, customerId);
            }
            
        }

        public Message GetMessage(int messageId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessage"))
            {
                return MessageServiceController.GetMessage(messageId);
            }
            
        }

        public MessageCategory GetCategory(int categoryId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetCategory"))
            {
                return MessageServiceController.GetCategory(categoryId);
            }
            
        }

        public int SaveMessage(Message msg)
        {
            using (new IBI.Shared.CallCounter("MessageService.SaveMessage"))
            {
                return MessageServiceController.Save(msg);
            }
            
        }

        public List<MessageCategory> GetMessageCategories(int customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessageCategories"))
            {
                return MessageServiceController.GetMessageCategories(customerId);
            }
            
        }

        public List<IBI.Shared.Models.Messages.MessageReplyType> GetMessageReplyTypes() 
        {
            using (new IBI.Shared.CallCounter("MessageService.GetMessageReplyTypes"))
            {
                return MessageServiceController.GetMessageReplyTypes();
            }
            
        }



        #endregion Private Functions
    }
}