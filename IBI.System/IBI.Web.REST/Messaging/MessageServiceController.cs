﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.Shared.Models;
using IBI.Shared.Models.Messages;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using System.ServiceModel.Web;
using IBI.Shared;
using System.Data;
using System.Data.SqlClient;
using IBI.Shared.Diagnostics;
using IBI.REST.Shared;
using System.Text;
using System.Collections;
using IBI.REST.BusTree;

namespace IBI.REST.Messaging
{
    public class MessageServiceController
    {
        #region Message CRUD

        public static IBI.Shared.Models.Messages.MessageCategory GetCategory(int categoryId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {

                var category = dbContext.Land2BusMessageCategories.Where(m => m.MessageCategoryId == categoryId).FirstOrDefault();

                if (category != null)
                {
                    IBI.Shared.Models.Messages.MessageCategory cat = new IBI.Shared.Models.Messages.MessageCategory();
                    cat.MessageCategoryId = category.MessageCategoryId;
                    cat.CategoryName = category.CategoryName;
                    cat.DateAdded = category.DateAdded;
                    cat.DateModified = category.DateModified;
                    cat.Description = category.Description;
                    cat.Validity = category.Validity;
                    cat.ValidityRule = category.ValidityRule;
                    cat.ValidityValue = category.ValidityValue;
                    return cat;
                }
            }

            return null;

        }

        public static IBI.Shared.Models.Messages.Message GetMessage(int messageId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {

                var messages = dbContext.Land2BusMessages.Include("Buses").Where(m => m.MessageId == messageId);

                if (messages != null && messages.Count() > 0)
                {
                    IBI.Shared.Models.Messages.Message msg = messages.Select(m => new IBI.Shared.Models.Messages.Message
                    {
                        // BusNumber = m.BusNumber,
                        CategoryName = m.Land2BusMessageCategory.CategoryName,
                        CustomerId = m.CustomerId ?? 0,
                        CustomerName = m.Customer.FullName,
                        DbBuses = m.Buses,
                        DateAdded = m.DateAdded,
                        DateModified = m.DateModified,
                        LastModifiedBy = m.LastModifiedBy,
                        MessageId = m.MessageId,
                        MessageCategoryId = m.MessageCategoryId,
                        MessageText = m.MessageText,
                        Subject = m.Subject,
                        ValidFrom = m.ValidFrom,
                        ValidTo = m.ValidTo,
                        SenderId = m.User.UserId,
                        SenderName = m.User.Username,
                        IsArchived = m.IsArchived,
                        MessageReplyTypeId = m.MessageReplyTypeId,
                        MessageReplyTypeName = m.Land2BusMessageReplyTypes.MessageReplyTypeName,
                        ReplyValues = m.Land2BusMessageReplyTypes.ReplyValues,
                        ShowAsPopup = m.ShowAsPopup

                        //Buses = m.Buses.Select(b=>b.BusNumber).ToList<String>()
                    }).First();


                    //UGLY FIX - EntityFramework must be able to do it in one query
                    msg.Buses = dbContext.Land2BusMessages.Where(m => m.MessageId == msg.MessageId).FirstOrDefault().Buses.Select(b => b.BusNumber).ToList();


                    return msg;
                }
            }

            return null;
        }

        //get messages of 1 bus.
        public static List<IBI.Shared.Models.Messages.Message> GetMessages(string busNumber, int customerId, bool activeOnly = true, bool isArchived = false)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == customerId).FirstOrDefault();
                var messages = dbContext.Land2BusMessages.Include("Buses").Where(m => m.Buses.Any(b => b.BusNumber == busNumber) && (m.ValidTo > DateTime.Now || activeOnly == false) && m.IsArchived == isArchived);

                //var messages = dbContext.Messages.Where(m => m.BusNumber == busNumber && m.CustomerId == customerId && );

                if (messages != null && messages.Count() > 0)
                {

                    List<IBI.Shared.Models.Messages.Message> msgs = messages.Select(m => new IBI.Shared.Models.Messages.Message
                    {
                        //BusNumber = m.BusNumber,
                        CategoryName = m.Land2BusMessageCategory.CategoryName,
                        CustomerId = m.CustomerId ?? 0,
                        CustomerName = m.Customer.FullName,
                        DateAdded = m.DateAdded,
                        DateModified = m.DateModified,
                        LastModifiedBy = m.LastModifiedBy,
                        MessageId = m.MessageId,
                        MessageCategoryId = m.MessageCategoryId,
                        MessageText = m.MessageText,
                        Subject = m.Subject,
                        ValidFrom = m.ValidFrom,
                        ValidTo = m.ValidTo,
                        SenderId = m.User.UserId,
                        SenderName = m.User.Username,
                        IsArchived = m.IsArchived,
                        MessageReplyTypeId = m.MessageReplyTypeId,
                        MessageReplyTypeName = m.Land2BusMessageReplyTypes.MessageReplyTypeName,
                        ReplyValues = m.Land2BusMessageReplyTypes.ReplyValues,
                        ShowAsPopup = m.ShowAsPopup,
                        DbBuses = m.Buses

                    }).OrderByDescending(col => col.DateModified).ToList();


                    //UGLY fix - Entity framework must bring child buses with the previous call actually.
                    foreach (IBI.Shared.Models.Messages.Message msg in msgs)
                    {
                        msg.Buses = dbContext.Land2BusMessages.Where(m => m.MessageId == msg.MessageId).FirstOrDefault().Buses.Select(b => b.BusNumber).ToList();
                    }


                    return msgs;
                }
            }

            return new List<IBI.Shared.Models.Messages.Message>();
        }

        public static int Save(IBI.Shared.Models.Messages.Message msg)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    DataAccess.DBModels.Land2BusMessages msgDb;

                    if (msg.MessageId <= 0)
                    {
                        DateTime currentTime  = DateTime.Now.AddMinutes(-5);
                        var msgs = dbContext.Land2BusMessages.Where(m => m.Subject == msg.Subject && m.MessageText == msg.MessageText && DateTime.Compare(currentTime, m.DateAdded.Value) < 0);

                        if (msgs != null && msgs.Count() > 0)
                        {
                            return msgs.First().MessageId;
                        }

                        msgDb = new DataAccess.DBModels.Land2BusMessages();
                        msgDb.DateAdded = DateTime.Now;
                    }
                    else
                    {
                        var msgs = dbContext.Land2BusMessages.Where(m => m.MessageId == msg.MessageId);
                        msgDb = (msgs != null && msgs.Count() > 0) ? msgs.First() : null;
                    }

                    if (msgDb != null)
                    {
                        //msgDb.BusNumber = msg.BusNumber;                    
                        msgDb.CustomerId = msg.CustomerId;
                        msgDb.DateModified = DateTime.Now;
                        msgDb.LastModifiedBy = msg.LastModifiedBy;
                        msgDb.MessageId = msg.MessageId;
                        msgDb.MessageCategoryId = msg.MessageCategoryId;
                        msgDb.MessageText = msg.MessageText;
                        msgDb.Subject = msg.Subject;
                        msgDb.ValidFrom = msg.ValidFrom;
                        msgDb.ValidTo = msg.ValidTo;
                        msgDb.Sender = msg.SenderId;
                        msgDb.IsArchived = msg.IsArchived;
                        msgDb.ShowAsPopup = msg.ShowAsPopup;
                        //if (msg.MessageReplyTypeId != null)
                        {
                            msgDb.MessageReplyTypeId = msg.MessageReplyTypeId;
                        }

                        //save for multiple buses                        
                        List<IBI.DataAccess.DBModels.Bus> buses = dbContext.Buses.Where(b => b.CustomerId == msg.CustomerId && msg.Buses.Contains(b.BusNumber)).ToList();

                        msgDb.Buses.Clear();

                        foreach (var bus in buses)
                        {
                            try
                            {
                                msgDb.Buses.Remove(bus);
                            }
                            catch { }

                            msgDb.Buses.Add(bus);
                        }

                        if (msg.MessageId <= 0)
                            dbContext.Land2BusMessages.Add(msgDb);

                        dbContext.SaveChanges();
                        return msgDb.MessageId;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);

            }
            return -1;
        }

        public static bool Remove(int messageId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    var msgs = dbContext.Land2BusMessages.Where(m => m.MessageId == messageId);
                    IBI.DataAccess.DBModels.Land2BusMessages msgToDelete = null;

                    if (msgs != null && msgs.Count() > 0)
                    {
                        msgToDelete = msgs.First();
                    }
                    if (msgToDelete != null)
                    {
                        dbContext.Land2BusMessages.Remove(msgToDelete);
                        dbContext.SaveChanges();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                }
            }

            return false;
        }

        public static bool SetExpireMessage(int messageId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    var msgs = dbContext.Land2BusMessages.Where(m => m.MessageId == messageId);
                    IBI.DataAccess.DBModels.Land2BusMessages msgToDelete = null;

                    if (msgs != null && msgs.Count() > 0)
                    {
                        msgToDelete = msgs.First();
                    }
                    if (msgToDelete != null)
                    {
                        msgToDelete.ValidTo = DateTime.Now.AddMinutes(-1); //expires outgoing message
                        msgToDelete.DateModified = DateTime.Now;
                        dbContext.SaveChanges();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                }
            }

            return false;
        }

        public static List<IBI.Shared.Models.Messages.MessageCategory> GetMessagesByCategories(string busNumber, int customerId, bool activeOnly = true, bool isArchived = false)
        {
            List<IBI.Shared.Models.Messages.MessageCategory> lstCategories = new List<IBI.Shared.Models.Messages.MessageCategory>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    var dbCategories = dbContext.Land2BusMessageCategories.Where(mc => mc.CustomerId == customerId);

                    if (dbCategories != null && dbCategories.Count() > 0)
                    {

                        foreach (var dbCat in dbCategories)
                        {
                            IBI.Shared.Models.Messages.MessageCategory category = new IBI.Shared.Models.Messages.MessageCategory
                            {
                                CategoryName = dbCat.CategoryName,
                                DateAdded = dbCat.DateAdded,
                                DateModified = dbCat.DateModified,
                                Description = dbCat.Description,
                                MessageCategoryId = dbCat.MessageCategoryId,
                                Validity = dbCat.Validity
                            };

                            var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber && b.CustomerId == customerId).FirstOrDefault();
                            //populate messages for each category.
                            category.Messages = dbCat.Land2BusMessages.Where(m => (m.Buses.Contains(bus)) && (m.ValidTo > DateTime.Now || activeOnly == false) && m.IsArchived == isArchived).Select(m => new IBI.Shared.Models.Messages.Message
                            {
                                //BusNumber = m.BusNumber,
                                CategoryName = m.Land2BusMessageCategory.CategoryName,
                                //CustomerId = m.CustomerId,
                                //CustomerName = m.Bus.Customer.FullName,
                                DateAdded = m.DateAdded,
                                DateModified = m.DateModified,
                                LastModifiedBy = m.LastModifiedBy,
                                MessageId = m.MessageId,
                                MessageCategoryId = m.MessageCategoryId,
                                MessageText = System.Web.HttpUtility.HtmlDecode(m.MessageText),
                                Subject = System.Web.HttpUtility.HtmlDecode(m.Subject),
                                ValidFrom = m.ValidFrom,
                                ValidTo = m.ValidTo,
                                SenderId = m.User.UserId,
                                SenderName = m.User.Username,
                                IsArchived = m.IsArchived,
                                MessageReplyTypeId = (m.MessageReplyTypeId == null ? 0 : m.MessageReplyTypeId),
                                MessageReplyTypeName = (m.MessageReplyTypeId == null ? "" : m.Land2BusMessageReplyTypes.MessageReplyTypeName),
                                ReplyValues = (m.MessageReplyTypeId == null ? "" : m.Land2BusMessageReplyTypes.ReplyValues),
                                ShowAsPopup = m.ShowAsPopup,
                                DbBuses = m.Buses
                            }).OrderByDescending(col => col.DateModified).ToList();

                            lstCategories.Add(category);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
            }

            return lstCategories;
        }

        #endregion

        #region MessageCategory

        public static List<IBI.Shared.Models.Messages.MessageCategory> GetMessageCategories(int customerId)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var categories = dbContext.Land2BusMessageCategories.Where(mc => mc.CustomerId == customerId).Select(mc => new IBI.Shared.Models.Messages.MessageCategory
                {
                    CategoryName = mc.CategoryName,
                    DateAdded = mc.DateAdded,
                    Description = mc.Description,
                    DateModified = mc.DateModified,
                    MessageCategoryId = mc.MessageCategoryId,
                    Validity = mc.Validity,
                    ValidityRule = mc.ValidityRule,
                    ValidityValue = mc.ValidityValue
                }).ToList();

                return categories.ToList();
            }

            return null;
        }

        #endregion


        #region Incoming Messages

        internal static string GetIncomingMessageTypes(string busNumber, int customerId)
        {
            String results = string.Empty;

            StringBuilder messageTypes = new StringBuilder();
            

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var data = dbContext.GetIncomingMessageTypes(busNumber, customerId).ToList();

                foreach (var item in data)
                {

                    if (item.GroupId == null && data.Where(i => i.GroupId != null && i.Message == item.Message).Count() > 0)
                        continue;

                    if (data.Where(i => i.GroupId > item.GroupId && i.Message == item.Message).Count() > 0)
                        continue;


                    messageTypes.Append(String.Format("<MessageType priority='{0}'><message><![CDATA[{1}]]></message><confirmationmessage><![CDATA[{2}]]></confirmationmessage><imagename base64='true'><![CDATA[{3}]]></imagename></MessageType>", (item.Priority ?? 0), item.Message, item.ConfirmationMessage, item.Base64Image));
                }
            }

            results = String.Format("<Data><MessageTypes>{0}</MessageTypes></Data>", messageTypes.ToString());
            
            return results;
        }

        public static bool SaveIncomingMessage(IBI.Shared.Models.Messages.IncomingMessage msg)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    DataAccess.DBModels.Bus2LandMessages msgDb;

                    if (msg.MessageId <= 0)
                    {
                        msgDb = new DataAccess.DBModels.Bus2LandMessages();

                    }
                    else
                    {
                        var msgs = dbContext.Bus2LandMessages.Where(m => m.MessageId == msg.MessageId);
                        msgDb = (msgs != null && msgs.Count() > 0) ? msgs.First() : null;
                    }

                    if (msgDb != null)
                    {
                        var clients = dbContext.Clients.Where(c => c.MacAddress == msg.MacAddress);
                        int? clientId = null;
                        if (clients != null && clients.Count() > 0)
                        {
                            clientId = clients.First().ClientId;
                        }

                        msgDb.BusNumber = msg.BusNumber;
                        msgDb.CustomerId = msg.CustomerId;
                        msgDb.ClientId = clientId;
                        msgDb.DateAdded = msg.DateAdded;
                        msgDb.DateModified = msg.DateModified;
                        msgDb.Destination = msg.Destination;
                        msgDb.IsArchived = msg.IsArchived;
                        msgDb.Latitude = msg.Latitude;
                        msgDb.Longitude = msg.Longitude;
                        msgDb.Line = msg.Line;
                        msgDb.MacAddress = msg.MacAddress;
                        msgDb.MessageId = msg.MessageId;
                        msgDb.MessageText = msg.MessageText;
                        msgDb.NextStop = msg.NextStop;
                        msgDb.OutgoingMessageId = msg.OutgoingMessageid;

                        if (msg.MessageId <= 0)
                            dbContext.Bus2LandMessages.Add(msgDb);

                        dbContext.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);

            }
            return false;
        }


        public static List<IBI.Shared.Models.Messages.IncomingMessage> GetIncomingMessages(int userId, bool isArchived = false)
        {

            var list = new List<IBI.Shared.Models.Messages.IncomingMessage>();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<DataAccess.DBModels.Bus> buses = DBHelper.GetUserBuses(userId, dbContext);
                List<string> busNumbers = buses.Select(b => b.BusNumber).ToList();

                list = dbContext.Bus2LandMessages.Where(m => busNumbers.Contains(m.BusNumber) && m.IsArchived == isArchived).Select(m => new IBI.Shared.Models.Messages.IncomingMessage
                    {
                        BusNumber = m.BusNumber,
                        BusAlias = m.Bus.BusAlias,
                        ClientId = m.ClientId,
                        CustomerId = m.CustomerId,
                        DateAdded = m.DateAdded,
                        DateModified = m.DateModified,
                        Destination = m.Destination,
                        GPS = "",
                        IsArchived = m.IsArchived,
                        Latitude = m.Latitude,
                        Line = m.Line,
                        Longitude = m.Longitude,
                        MacAddress = m.MacAddress,
                        MessageId = m.MessageId,
                        MessageText = m.MessageText,
                        NextStop = m.NextStop,
                        OutgoingMessageid = m.OutgoingMessageId,
                        BusGroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                        BusGroupName = m.Bus.Groups.FirstOrDefault().Description
                    }).OrderByDescending(m => m.DateAdded).ToList<IBI.Shared.Models.Messages.IncomingMessage>();

            }

            return list;
        }


        public static List<IBI.Shared.Models.Messages.IncomingMessage> GetIncomingMessages(int userId, DateTime date, string busNumber, string messageText, string position, string destination, string line, string nextStop, int groupId, bool isArchived = false)
        {
            DateTime endDate = date.AddDays(1);

            var list = new List<IBI.Shared.Models.Messages.IncomingMessage>();

            using (IBIDataModel dbContext = new IBIDataModel())
            {

                List<DataAccess.DBModels.Bus> buses = DBHelper.GetUserBuses(userId, dbContext);
                List<string> busNumbers = buses.Select(b => b.BusNumber).ToList();

                list = dbContext.Bus2LandMessages.Where(m =>
                     m.IsArchived == isArchived &&
                    (busNumbers.Contains(m.BusNumber)) &&
                    ((m.DateAdded >= date && m.DateAdded < endDate) || date == DateTime.MinValue) &&
                    (m.BusNumber == busNumber || String.IsNullOrEmpty(busNumber)) &&
                    (m.MessageText.Contains(messageText) || String.IsNullOrEmpty(messageText)) &&
                    (m.Latitude.StartsWith(position) || m.Longitude.StartsWith(position) || String.IsNullOrEmpty(position)) &&
                    (m.Destination == destination || String.IsNullOrEmpty(destination)) &&
                    (m.Line == line || String.IsNullOrEmpty(line)) &&
                    (m.NextStop == nextStop || String.IsNullOrEmpty(nextStop)) &&
                    (m.Bus.Groups.FirstOrDefault().GroupId == groupId || groupId == -1) //this line will be removed once busesNumber<> would be filtered correctly by given group.
                    ).Select(m => new IBI.Shared.Models.Messages.IncomingMessage
                {
                    BusNumber = m.BusNumber,
                    BusAlias = m.Bus.BusAlias,
                    ClientId = m.ClientId,
                    CustomerId = m.CustomerId,
                    DateAdded = m.DateAdded,
                    DateModified = m.DateModified,
                    Destination = m.Destination,
                    GPS = "",
                    IsArchived = m.IsArchived,
                    Latitude = m.Latitude,
                    Line = m.Line,
                    Longitude = m.Longitude,
                    MacAddress = m.MacAddress,
                    MessageId = m.MessageId,
                    MessageText = m.MessageText,
                    NextStop = m.NextStop,
                    OutgoingMessageid = m.OutgoingMessageId,
                    BusGroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                    BusGroupName = m.Bus.Groups.FirstOrDefault().Description
                }).OrderByDescending(m => m.DateAdded).ToList<IBI.Shared.Models.Messages.IncomingMessage>();

            }

            return list;
        }

        #endregion

        #region Outgoing Messages

        public static List<IBI.Shared.Models.Messages.Message> GetOutgoingMessages(int userId, string busNumber, DateTime validFrom, DateTime validTo, string title, string messageText, string messageCategoryName, int groupId, string senderName, bool showExpiredOnly)
        {

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                busNumber = busNumber ?? "";
                List<DataAccess.DBModels.Bus> uBuses = DBHelper.GetUserBuses(userId, dbContext);
                List<string> busNumbers = uBuses.Select(b => b.BusNumber).ToList();
                string commaSeparatedList = string.Join(",", busNumbers);
                List<int> userCustomers = BusServiceController.GetUserCustomers(userId).Select(l => l.CustomerId.Value).ToList();

                if (string.IsNullOrEmpty(busNumber) || (!string.IsNullOrEmpty(busNumber) && commaSeparatedList.Contains(busNumber)))
                {
                    DateTime endDate_from = validFrom.AddDays(1);
                    DateTime endDate = validTo.AddDays(1);

                    var bus = dbContext.Buses.Where(b => b.BusNumber == busNumber).FirstOrDefault();

                    var messages = dbContext.Land2BusMessages.Include("Buses").Where(m =>
                        (userCustomers.Contains(m.CustomerId.Value)) &&
                        (String.IsNullOrEmpty(busNumber) || m.Buses.Any(b => b.BusNumber.Contains(busNumber))) &&
                        (m.Buses.Any(b => commaSeparatedList.Contains(b.BusNumber))) &&
                        (validTo == DateTime.MinValue || (m.ValidFrom <= endDate)) &&
                        (validTo == DateTime.MinValue || (m.ValidTo >= validTo)) &&
                        (String.IsNullOrEmpty(title) || m.Subject.StartsWith(title)) &&
                        (String.IsNullOrEmpty(messageText) || m.MessageText.Contains(messageText)) &&
                        (String.IsNullOrEmpty(messageCategoryName) || m.Land2BusMessageCategory.CategoryName.StartsWith(messageCategoryName)) &&
                        (String.IsNullOrEmpty(senderName) || m.User.Username == senderName) &&
                        ((validTo != DateTime.MinValue) || ((validTo == DateTime.MinValue && showExpiredOnly == false && m.ValidTo >= DateTime.Now) || (validTo == DateTime.MinValue && showExpiredOnly == true && m.ValidTo < DateTime.Now)))
                        ).OrderByDescending(m => m.DateModified);

                    if (messages != null && messages.Count() > 0)
                    {
                        List<IBI.Shared.Models.Messages.Message> msgs = messages.Select(m => new IBI.Shared.Models.Messages.Message
                        {
                            //BusNumber = m.BusNumber,
                            CategoryName = m.Land2BusMessageCategory.CategoryName,
                            CustomerId = m.CustomerId ?? 0,
                            CustomerName = m.Customer.FullName,
                            DateAdded = m.DateAdded,
                            DateModified = m.DateModified,
                            LastModifiedBy = m.LastModifiedBy,
                            MessageId = m.MessageId,
                            MessageCategoryId = m.MessageCategoryId,
                            MessageText = m.MessageText,
                            Subject = m.Subject,
                            ValidFrom = m.ValidFrom,
                            ValidTo = m.ValidTo,
                            SenderId = m.User.UserId,
                            SenderName = m.User.Username,
                            IsArchived = m.IsArchived,
                            MessageReplyTypeId = m.MessageReplyTypeId,
                            MessageReplyTypeName = m.Land2BusMessageReplyTypes.MessageReplyTypeName,
                            ReplyValues = m.Land2BusMessageReplyTypes.ReplyValues,
                            ShowAsPopup = m.ShowAsPopup,
                            Editable = m.Buses.All(b => busNumbers.Contains(b.BusNumber))
                            //GroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                            //GroupName = m.Bus.Groups.FirstOrDefault().Description,
                            //Buses = m.Buses.Select(b=>b.BusNumber).ToList()
                        }).OrderByDescending(col => col.DateModified).ToList();


                        //UGLY fix - Entity framework must bring child buses with the previous call actually.
                        foreach (IBI.Shared.Models.Messages.Message msg in msgs)
                        {
                            msg.Buses = dbContext.Land2BusMessages.Where(m => m.MessageId == msg.MessageId).FirstOrDefault().Buses.Select(b => b.BusNumber).ToList();
                            msg.BusesAlias = dbContext.Land2BusMessages.Where(m => m.MessageId == msg.MessageId).FirstOrDefault().Buses.Select(b => b.BusAlias).ToList();

                        }

                        return msgs;
                    }
                }
                else
                {
                    return new List<IBI.Shared.Models.Messages.Message>();
                }
            }

            return new List<IBI.Shared.Models.Messages.Message>();
        }

        public static IBI.Shared.Models.Messages.MessageConfirmationsAndReplys GetMessageConfirmationsAndReplys(int messageId,string busNumber="")
        {
            IBI.Shared.Models.Messages.MessageConfirmationsAndReplys msgConfAndReplys = new IBI.Shared.Models.Messages.MessageConfirmationsAndReplys();
            msgConfAndReplys.MessageId = messageId;
            msgConfAndReplys.MessageDisplayConfirmations = GetMessageConfirmations(messageId, busNumber);
            msgConfAndReplys.ReplysMessages = GetMessageReplies(messageId, busNumber); 
            return msgConfAndReplys;
        }

        public static List<IBI.Shared.Models.Messages.IncomingMessage> GetMessageReplies(int messageId, string busNumber="")
        {

            List<IBI.Shared.Models.Messages.IncomingMessage> msgReplies = new List<IBI.Shared.Models.Messages.IncomingMessage>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                if (string.IsNullOrEmpty(busNumber))
                {
                    msgReplies = dbContext.Bus2LandMessages.Where(m => m.OutgoingMessageId == messageId).Select(m => new IBI.Shared.Models.Messages.IncomingMessage
                    {
                        BusNumber = m.BusNumber,
                        BusAlias = m.Bus.BusAlias,
                        ClientId = m.ClientId,
                        CustomerId = m.CustomerId,
                        DateAdded = m.DateAdded,
                        DateModified = m.DateModified,
                        Destination = m.Destination,
                        GPS = "",
                        IsArchived = m.IsArchived,
                        Latitude = m.Latitude,
                        Line = m.Line,
                        Longitude = m.Longitude,
                        MacAddress = m.MacAddress,
                        MessageId = m.MessageId,
                        MessageText = m.MessageText,
                        NextStop = m.NextStop,
                        OutgoingMessageid = m.OutgoingMessageId,
                        BusGroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                        BusGroupName = m.Bus.Groups.FirstOrDefault().Description
                    }).OrderByDescending(m => m.MessageText).ToList<IBI.Shared.Models.Messages.IncomingMessage>();
                }
                else
                {
                    msgReplies = dbContext.Bus2LandMessages.Where(m => m.OutgoingMessageId == messageId && m.BusNumber.Contains(busNumber)).Select(m => new IBI.Shared.Models.Messages.IncomingMessage
                    {
                        BusNumber = m.BusNumber,
                        BusAlias = m.Bus.BusAlias,
                        ClientId = m.ClientId,
                        CustomerId = m.CustomerId,
                        DateAdded = m.DateAdded,
                        DateModified = m.DateModified,
                        Destination = m.Destination,
                        GPS = "",
                        IsArchived = m.IsArchived,
                        Latitude = m.Latitude,
                        Line = m.Line,
                        Longitude = m.Longitude,
                        MacAddress = m.MacAddress,
                        MessageId = m.MessageId,
                        MessageText = m.MessageText,
                        NextStop = m.NextStop,
                        OutgoingMessageid = m.OutgoingMessageId,
                        BusGroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                        BusGroupName = m.Bus.Groups.FirstOrDefault().Description
                    }).OrderByDescending(m => m.MessageText).ToList<IBI.Shared.Models.Messages.IncomingMessage>();
                }
            }
            return msgReplies;
        }

        public static List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> GetMessageConfirmations(int messageId, string busNumber = "")
        {

            List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> msgConfirmations = new List<IBI.Shared.Models.Messages.MessageDisplayConfirmations>();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                if (string.IsNullOrEmpty(busNumber))
                {
                    msgConfirmations = dbContext.Land2BusMessageDisplayConfirmations.Where(m => m.MessageId == messageId).Select(m => new MessageDisplayConfirmations
                    {
                        MessageId = m.MessageId,
                        BusNumber = m.BusNumber,
                        CustomerId = m.CustomerId,
                        DisplayTime = m.DisplayTime,
                        BusGroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                        BusGroupName = m.Bus.Groups.FirstOrDefault().Description
                    }).OrderByDescending(col => col.DisplayTime).ToList();
                }
                else
                {
                    msgConfirmations = dbContext.Land2BusMessageDisplayConfirmations.Where(m => m.MessageId == messageId && m.BusNumber.Contains(busNumber)).Select(m => new MessageDisplayConfirmations
                    {
                        MessageId = m.MessageId,
                        BusNumber = m.BusNumber,
                        CustomerId = m.CustomerId,
                        DisplayTime = m.DisplayTime,
                        BusGroupId = m.Bus.Groups.FirstOrDefault().GroupId,
                        BusGroupName = m.Bus.Groups.FirstOrDefault().Description
                    }).OrderByDescending(col => col.DisplayTime).ToList();

                }
            }
            return msgConfirmations;
        }

        #endregion



        internal static List<IBI.Shared.Models.Messages.MessageReplyType> GetMessageReplyTypes()
        {
            List<IBI.Shared.Models.Messages.MessageReplyType> list = new List<IBI.Shared.Models.Messages.MessageReplyType>();
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    list = dbContext.Land2BusMessageReplyTypes.Select(mrt => new IBI.Shared.Models.Messages.MessageReplyType
                    {
                        MessageReplyTypeId = mrt.MessageReplyTypeId,
                        MessageReplyTypeName = mrt.MessageReplyTypeName,
                        ReplyValues = mrt.ReplyValues
                    }).ToList();
                }
            }

            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Land2Bus, ex);
            }


        return list;
        }

    }
}