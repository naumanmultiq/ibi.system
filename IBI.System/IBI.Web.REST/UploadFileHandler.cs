﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Security.AccessControl;
using IBI.REST.Shared;
using ICSharpCode.SharpZipLib.Zip;
using IBI.Shared.Diagnostics;
using IBI.Shared;

namespace IBI.REST
{
    public class UploadFileHandler : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("UploadFileHandler.ProcessRequest"))
            {
                String message = "";
                String details = "Details: ";
                
                try
                {
                    if (context.Request.Files.Count > 0)
                    {
                        // Download file to temp folder
                        DirectoryInfo targetDirectory = new DirectoryInfo(Path.Combine(Path.GetTempPath(), "ibi\\" + Guid.NewGuid().ToString()));

                        if (!targetDirectory.Exists)
                            targetDirectory.Create();

                        try
                        {
                            // Determine upload type
                            String uploadType = context.Request["uploadtype"];
                            
                            for (int i = 0; i < context.Request.Files.Count; i++)
                            {
                                String filePath = Path.Combine(targetDirectory.FullName, Guid.NewGuid().ToString() + ".tmp");

                                context.Request.Files[i].SaveAs(filePath);

                                if (String.Compare("Logs", uploadType, true) == 0 || String.Compare("ProgressLog", uploadType, true) == 0)
                                {
                                    String logDirectory = Path.Combine(AppSettings.GetResourceDirectory(), "Logs\\" + DateTime.Now.ToString("yyyyMMdd"));

                                    if (!Directory.Exists(logDirectory))
                                        Directory.CreateDirectory(logDirectory);

                                    File.Move(filePath, Path.Combine(logDirectory, Path.GetFileName(filePath)));

                                    message += "UploadResult=OK;Message=Log has been saved for later processing";

                                    //Processing done
                                    break;
                                }

                                String[] files = null;

                                if (String.Compare(".zip", Path.GetExtension(context.Request.Files[i].FileName), true) == 0)
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Upload, "Zip file uploaded, not extracting: " + context.Request.Files[i].FileName);
                                    //[KHI: following 2 lines are temporarily commented to avoid a Server Error]

                                    //details += "Extracting files from: " + context.Request.Files[i].FileName + Environment.NewLine;
                                    //files = this.extractFiles(filePath);
                                }
                                else
                                {
                                    details += "Reading file: " + context.Request.Files[i].FileName + Environment.NewLine;
                                    files = new String[] { filePath };
                                }

                                if (String.Compare("Screenshot", uploadType, true) == 0)
                                {
                                    String command = context.Request["command"];

                                    handleScreenshots(files, command);
                                }
                                else
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Upload, "Unknown upload type: " + uploadType);
                                    throw new ApplicationException("Unknown upload type: " + uploadType);
                                }
                            }
                        }
                        finally
                        {
                            if (targetDirectory.Exists)
                                targetDirectory.Delete(true);
                        }

                        if (String.IsNullOrEmpty(message))
                            message += "UploadResult=OK;Message=Log has been imported" + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Upload, ex);

                    String description = String.Empty;

                    Exception tmpException = ex;

                    while (tmpException != null)
                    {
                        description += tmpException.Message + Environment.NewLine;
                        description += tmpException.StackTrace + Environment.NewLine;
                        description += "--" + Environment.NewLine;

                        tmpException = tmpException.InnerException;
                    }

                    message += "UploadResult=FAILED;Error uploading files" + Environment.NewLine;
                    message += description;
                }
                finally
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        context.Response.Clear();
                        context.Response.Write(message + Environment.NewLine + details);
                        context.Response.End();
                    }
                }
            }
        }

        private void handleScreenshots(String[] files, String command)
        { 
            using (new IBI.Shared.CallCounter("UploadFileHandler.handleScreenshots"))
            {
                 

                if (command == null)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Upload, "command not specified");
                    throw new ApplicationException("command not specified");
                }
                 


                String[] commandValues = command.Split(';');

                if (String.Compare("forwardimage", commandValues[0], true) == 0)
                {

                }
                else if (String.Compare("periodic_upload", commandValues[0], true) == 0)
                {
                  
                    String busNumber = commandValues[1];
                    String clientType = commandValues[2];

                    DirectoryInfo targetDirectory = new DirectoryInfo(Path.Combine(AppSettings.GetResourceDirectory(), "Screenshots\\" + clientType + "\\" + busNumber));

                    String filename = "Screenshot_" + busNumber + "_" + clientType + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".jpg";

                    if (!targetDirectory.Exists)
                        targetDirectory.Create();

                    File.Move(files[0], Path.Combine(targetDirectory.FullName, filename));

                    //FileSecurity fileSec = File.GetAccessControl(Path.Combine(targetDirectory.FullName, filename));
                    //fileSec.AddAccessRule(new FileSystemAccessRule(AppSettings.GetIUSR(), FileSystemRights.Read, AccessControlType.Allow));

                    //File.SetAccessControl(Path.Combine(targetDirectory.FullName, filename), fileSec);

                    if (Directory.GetFiles(Path.GetDirectoryName(files[0])).Length == 0)
                        Directory.Delete(Path.GetDirectoryName(files[0]));

                    //try
                    //{
                    //    log += "Begin Loop: targetDirectory.GetFiles('Screenshot_*.jpg')" + Environment.NewLine;
                    //    foreach (FileInfo existingFileInfo in targetDirectory.GetFiles("Screenshot_*.jpg"))
                    //    {
                    //        String fileName = Path.GetFileNameWithoutExtension(existingFileInfo.FullName);
                    //        String existingFileTimestampString = fileName.Substring(fileName.Length - 15);

                    //        log += "fileName:" + filename + Environment.NewLine;
                    //        log += "existingFileTimestampString:" + existingFileTimestampString + Environment.NewLine;

                    //        if (existingFileTimestampString.Length == 15)
                    //        {
                    //            int year = int.Parse(existingFileTimestampString.Substring(0, 4));
                    //            int month = int.Parse(existingFileTimestampString.Substring(4, 2));
                    //            int day = int.Parse(existingFileTimestampString.Substring(6, 2));

                    //            int hour = int.Parse(existingFileTimestampString.Substring(9, 2));
                    //            int minute = int.Parse(existingFileTimestampString.Substring(11, 2));
                    //            int second = int.Parse(existingFileTimestampString.Substring(13, 2));

                    //            DateTime existingFileTimestamp = new DateTime(year, month, day, hour, minute, second);

                    //            log += "existingFileTimestamp:" + existingFileTimestamp + Environment.NewLine;
                                
                    //            if (existingFileTimestamp.AddDays(7) < DateTime.Now)
                    //                mermaid.BaseObjects.IO.FileSystem.DeleteFile(existingFileInfo);

                    //            log += "File Deleted:" + Environment.NewLine;
                    //        }
                    //    }

                    //    log += "End Loop:" + Environment.NewLine;
                    //}
                    //catch (Exception ex)
                    //{
                    //    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Upload, ex);
                    //}
                }
                else
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Upload, "Unknown Screenshot command");
                    throw new ApplicationException("Unknown Screenshot command");
                }
            }
        }

        private String[] extractFiles(String filePath)
        {
            //String targetPath = Path.Combine(Path.GetTempPath(), DateTime.Now.ToBinary().ToString());
            //String targetPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            String targetPath = Path.GetDirectoryName(filePath);

            List<String> fileList = new List<String>();

            FastZip zipEngine = new FastZip();
            zipEngine.ExtractZip(filePath, targetPath, "");

            foreach (String logFilePath in Directory.GetFiles(targetPath))
            {
                if(String.Compare(Path.GetFileName(filePath), Path.GetFileName(logFilePath), true) != 0)
                    fileList.Add(logFilePath);
            }

            return fileList.ToArray();
        }

        #endregion
    }
}
