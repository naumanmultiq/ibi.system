﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using IBI.Shared.Models.Accounts;
using IBI.Shared.Diagnostics;
using System.DirectoryServices.AccountManagement;

//using Auth = IBI.DataAccess.DBModels.Auth;

namespace IBI.REST.Auth
{
    public class UserAuthServiceController
    {
        public enum LoggedInUserType
        {
            IBI = 1,
            DOMAIN = 2
        }

        public static IBI.Shared.Models.Accounts.Auth Authenticate(string username, string password, string apiKey)
        {
            IBI.Shared.Models.Accounts.User usr = null; 

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var app = dbContext.Apps.Where(a => a.ApiKey == apiKey).FirstOrDefault();

                if(app==null)
                {
                    return new IBI.Shared.Models.Accounts.Auth
                    {
                        ErrorMessage = "Invalid Api Key provided", IsValid=false
                    };
                }


                byte type = GetUserType(username);


                //if(type==1)
                //    usr = AuthenticateIBIUser(dbContext, username, password);
                    
                //if(type==2)
                //    usr = AuthenticateDomainUser(dbContext, username, password);

                usr = AuthenticateUser(dbContext, username, password);

                               
                if(usr!=null && usr.UserId>0)
                {

                    DateTime validityTime = DateTime.Now.Subtract(IBI.Shared.AppSettings.AuthTokenValidity);

                    var auth = dbContext.Auths.Where(a =>
                        a.UserId == usr.UserId && 
                        a.ApiId == app.ApiId && 
                        a.LastAccessed > validityTime).OrderByDescending(a => a.LastAccessed)
                    .Select(a => new IBI.Shared.Models.Accounts.Auth
                    {
                        AuthToken=a.AuthToken,
                        CreatedOn = a.CreatedOn,
                        ErrorMessage="",
                        FullName = a.User.FullName,
                        Id=a.Id,
                        IsValid = true,
                        LastAccessed=a.LastAccessed,
                        ApiId = a.App.ApiId,
                        ApiKey = a.App.ApiKey,
                        Applicaiton = a.App.ApplicationName,
                        Type = usr.Domain,
                        UserId=a.UserId,
                        Username = a.User.Username                    
                    }).Take(1).ToList();
                              
                    if(auth!=null && auth.Count()>0)
                    {
                        //update auth timestamp.
                        auth.First().LastAccessed = DateTime.Now;
                        auth.First().IsValid = true;
                        auth.First().User = null; //GetModelUser(dbContext, auth.First().UserId); need to fix datetime issue in it.

                        IBI.DataAccess.DBModels.User user = dbContext.Users.Single(s => s.Username == usr.Username);
                        user.LastLogin = DateTime.Now;

                        dbContext.SaveChanges();
                        return auth.First();
                    }
                    else
                    {
                       return CreateAuthToken(dbContext, usr, app); 
                    } 
                }
                else
                {
                    var noAuth = InvalidAuthToken(app, usr.ErrorMessage);                    
                    return noAuth;
                }
            }
        }

       
        public static IBI.Shared.Models.Accounts.Auth Validate(string authToken)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {

                DateTime validityTime = DateTime.Now.Subtract(IBI.Shared.AppSettings.AuthTokenValidity);

                var auths = dbContext.Auths.Where(a => 
                    a.AuthToken == authToken && 
                    a.LastAccessed > validityTime
                    ).OrderByDescending(a => a.LastAccessed).Select(a => new IBI.Shared.Models.Accounts.Auth
                {
                    AuthToken = a.AuthToken,
                    CreatedOn = a.CreatedOn,
                    ErrorMessage = "",
                    FullName = a.User.FullName,
                    Id = a.Id,
                    IsValid=true,
                    LastAccessed = a.LastAccessed,
                    ApiId = a.App.ApiId,
                    ApiKey = a.App.ApiKey,
                    Applicaiton = a.App.ApplicationName,
                    Type = a.User.Domain,
                    UserId = a.UserId,
                    Username = a.User.Username,
                }).Take(1).ToList();

                if (auths != null && auths.Count() > 0)
                {
                    //update auth timestamp.
                    var auth = auths.FirstOrDefault();
                    auth.LastAccessed = DateTime.Now;
                    auth.IsValid = true;
                    auth.User = null; //GetModelUser(dbContext, auth.First().UserId); need to fix datetime issue in it


                    //update authToken in DB.
                    var dbAuths = dbContext.Auths.Where(a => a.AuthToken == authToken).OrderByDescending(a => a.LastAccessed).Take(1).ToList();
                    if (dbAuths != null && dbAuths.Count() > 0)
                    {
                        IBI.DataAccess.DBModels.Auth dbAuth = dbAuths.FirstOrDefault();
                        dbAuth.LastAccessed = DateTime.Now;

                        dbContext.SaveChanges();                       
                    }
                     
                    return auths.First();
                }
            }

            IBI.Shared.Models.Accounts.Auth noAuth = InvalidAuthToken();
            return noAuth;
        }


        #region Private Helper

        private static IBI.Shared.Models.Accounts.User AuthenticateUser(IBIDataModel dbContext, string username, string password)
        {
            try
            {
                IBI.Shared.Models.Accounts.User usr = (IBI.Shared.Models.Accounts.User)dbContext.Users.Where(u => u.Username == username).Select(u => new IBI.Shared.Models.Accounts.User()
                {
                    DateAdded = u.DateAdded ?? DateTime.MinValue,
                    DateModified = u.DateModified ?? DateTime.MinValue,
                    Description = u.Description,
                    Domain = u.Domain,
                    ErrorMessage = "",
                    FullName = u.FullName,
                    LastLogin = u.LastLogin,
                    Password = u.Password, 
                    UserId = u.UserId,
                    Username = u.Username,
                    IsAdmin = u.UserGroups.FirstOrDefault().Name=="Admin"
                }).FirstOrDefault();

                if (usr != null)
                {   //user exists - now validate password
                    //if it's a domain user, validate him from domain.
                    if (usr.Domain !=null && usr.Domain != "")
                    {
                        if(ValidateDomainUser(dbContext, username + "@" + usr.Domain, password))
                        {
                            return usr;
                        }
                        else
                        {
                            return new IBI.Shared.Models.Accounts.User("Password is incorrect");
                        }
                    }
                    else
                    {
                        if (usr.Password == password)
                        {                    
                            return usr;
                        }
                        else
                        {
                            return new IBI.Shared.Models.Accounts.User("Password is incorrect");
                        }
                    }
                }
                else
                    return new IBI.Shared.Models.Accounts.User("Username doesn't exist");

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                return new IBI.Shared.Models.Accounts.User { ErrorMessage = String.Format("Incorrect Username, failed to validate an IBI user {0}", username) };
            }
        }

        private static bool ValidateDomainUser(IBIDataModel dbContext, string username, string password)
        {
            //return new User { ErrorMessage = "Domain users validation not implemented yet!!!" };
            bool valid = false;

            string domain = GetDomainName(username);

            try
            {
                 using (PrincipalContext context = new PrincipalContext(ContextType.Domain))
                {
                    valid = context.ValidateCredentials(username, password);
                }

                return valid;
            }
            catch(Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                return false;
            }
           
        }
         
        private static IBI.Shared.Models.Accounts.Auth CreateAuthToken(IBIDataModel dbContext, IBI.Shared.Models.Accounts.User user, IBI.DataAccess.DBModels.App app)
        { 
            string authToken = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.Ticks.ToString();
             
            IBI.DataAccess.DBModels.Auth auth = new IBI.DataAccess.DBModels.Auth();

            auth.AuthToken = authToken;
            auth.UserId = user.UserId;
            auth.CreatedOn = auth.LastAccessed = DateTime.Now;
            auth.ApiId = app.ApiId;
            auth.Id = 0;


            dbContext.Auths.Add(auth);
            dbContext.SaveChanges();

            return new IBI.Shared.Models.Accounts.Auth
            {
                AuthToken = auth.AuthToken,
                CreatedOn = auth.CreatedOn,
                ErrorMessage = "",
                FullName = user.FullName,
                Id = auth.Id,
                IsValid = true,
                LastAccessed = auth.LastAccessed,
                ApiId = auth.App.ApiId,
                ApiKey = auth.App.ApiKey,
                Applicaiton = auth.App.ApplicationName,
                Type = user.Domain,
                UserId = auth.UserId,
                Username = user.Username 
            };

        }

        //function not used so far.
        private static IBI.DataAccess.DBModels.User CreateDomainUser(IBIDataModel dbContext, string username)
        {
            try
            {
                IBI.DataAccess.DBModels.User usr = new IBI.DataAccess.DBModels.User();
                
                usr.DateAdded = usr.DateModified = usr.LastLogin = DateTime.Now;
                usr.Domain = GetDomainName(username);
                usr.FullName = GetDomainUser(username);

                dbContext.Users.Add(usr);
                dbContext.SaveChanges();

                return usr;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, "Error Creating New Domain User: Error = " + ex.Message + ". " + ex.StackTrace);
                return null;
            }
        }

        private static IBI.Shared.Models.Accounts.User GetModelUser(IBIDataModel dbContext, int userId)
        {
            if (userId <=0)
                return null;

            var usrs = dbContext.Users.Where(u=>u.UserId == userId).Select(u=>new IBI.Shared.Models.Accounts.User{
                DateAdded = u.DateAdded ?? Types.Dates.MinDateTime,
                DateModified = u.DateModified ?? Types.Dates.MinDateTime,
                Description = u.Description,
                Domain = u.Domain,
                ErrorMessage = "",
                FullName = u.FullName,
                LastLogin = (u.LastLogin == null || u.LastLogin == DateTime.MinValue) ? Types.Dates.MinDateTime : u.LastLogin,
                Password = u.Password, 
                UserId = u.UserId,
                Username = u.Username
            });

            if (usrs != null && usrs.Count() > 0)
                return usrs.First();
            else
                return null;
        }

        private static IBI.Shared.Models.Accounts.User GetModelUser(IBI.DataAccess.DBModels.User ibiUser)
        {
            if (ibiUser == null)
                return null;

            return new IBI.Shared.Models.Accounts.User
            {
                DateAdded = ibiUser.DateAdded ?? Types.Dates.MinDateTime,
                DateModified = ibiUser.DateModified ?? Types.Dates.MinDateTime,
                Description = ibiUser.Description,
                Domain = ibiUser.Domain,
                ErrorMessage = "",
                FullName = ibiUser.FullName,
                LastLogin = ibiUser.LastLogin ?? Types.Dates.MinDateTime,
                Password = ibiUser.Password, 
                UserId = ibiUser.UserId,
                Username = ibiUser.Username
            };
        }


        private static IBI.Shared.Models.Accounts.Auth InvalidAuthToken(IBI.DataAccess.DBModels.App app = null, string errorMessage = "")
        {
            if (app == null)
                return new IBI.Shared.Models.Accounts.Auth
                {
                    ErrorMessage="Invalid or expired token", IsValid=false
                };

            string errMsg = errorMessage != "" ? errorMessage : "Invalid or expired token";

            return new IBI.Shared.Models.Accounts.Auth
            {
                AuthToken = "",
                CreatedOn = Types.Dates.MinDateTime,
                ErrorMessage = errMsg,
                FullName = "",
                Id = -1,
                IsValid = false,
                LastAccessed = Types.Dates.MinDateTime,
                ApiId = app.ApiId,
                ApiKey = app.ApiKey,
                Applicaiton=app.ApplicationName,
                Type = "",
                UserId = -1,
                Username = "",
                User = null
            };



        }

        internal static IBI.Shared.Models.Accounts.User GetUser(string userName)
        {
             using (IBIDataModel dbContext = new IBIDataModel())
            {
                var users = dbContext.Users.Where(u => u.Username == userName);

                if (users != null)
                {
                    return users.Select(u => new IBI.Shared.Models.Accounts.User
                    {
                        DateAdded = u.DateAdded ?? Types.Dates.MinDateTime,
                        DateModified = u.DateModified ?? Types.Dates.MinDateTime,
                        Description = u.Description,
                        Domain = u.Domain,
                        ErrorMessage = "",
                        FullName = u.FullName,
                        LastLogin = u.LastLogin ?? Types.Dates.MinDateTime,
                        Password = u.Password, 
                        UserId = u.UserId,
                        Username = u.Username,
                        Company = u.Company,
                        Email = u.Email,
                        PhoneNumber = u.PhoneNumber,
                        ShortName = u.ShortName,
                        IsAdmin = u.UserGroups.Where(ug=>ug.Name=="Admin").FirstOrDefault() == null ? false : true

                    }).FirstOrDefault();
                }
            }

            return null;
        }

        private static string GetDomainName(string username)
        {
            if(username.Contains("@"))
            {
               return username.Substring(username.IndexOf("@")+1);
            }

            if(username.Contains(@"\"))
            {
                return username.Substring(0, username.IndexOf(@"\"));
            }

            return "";
        }

        private static string GetDomainUser(string username)
        {
            if(username.Contains("@"))
            {
               return username.Substring(0, username.IndexOf("@"));
            }

            if(username.Contains(@"\"))
            {
                return username.Substring(username.IndexOf(@"\") + 1);
            }

            return "";
        }

        private static byte GetUserType(string username)
        {
            return (byte) ((username.Contains("@") || username.Contains(@"\")) ? 2 : 1);
        }

        #endregion


        internal static bool ChangePassword(string userName, string newPassword)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.DataAccess.DBModels.User user = dbContext.Users.Single(s => s.Username == userName);
                    user.Password = newPassword;
                    dbContext.SaveChanges();
                    return true; 
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal static bool EditAccountSettings(IBI.Shared.Models.Accounts.User model)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.DataAccess.DBModels.User user = dbContext.Users.Single(s => s.Username == model.Username);
                    user.Company = model.Company;
                    user.Email = model.Email;
                    user.FullName = model.FullName;
                    user.PhoneNumber = model.PhoneNumber;
                    user.ShortName = model.ShortName;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal static bool ConfirmAdminPassword(string username, string password)
        {

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var usr = AuthenticateUser(dbContext, username, password);
                    if (usr != null && usr.UserId>0 && usr.IsAdmin)
                    {
                        return true;
                    }
                }
            }
            catch(Exception ex) {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.USER_MGMT, ex);
            }

            return false;
        }
    }
}