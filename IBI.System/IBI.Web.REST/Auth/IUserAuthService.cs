﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace IBI.REST.Auth
{
   [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")] 
    public interface IUserAuthService
    {

        [OperationContract]
        //[WebGet]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Json/Authenticate?u={username}&p={password}&apiKey={apiKey}")]
        IBI.Shared.Models.Accounts.Auth Authenticate(string username, string password, string apiKey);

        [OperationContract]
        //[WebGet]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Json/Validate?authToken={authToken}")]
        IBI.Shared.Models.Accounts.Auth Validate(string authToken);

         

        [OperationContract]
        [WebGet]
        IBI.Shared.Models.Accounts.User GetUser(string userName);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool ChangePassword(string userName, string newPassword);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool EditAccountSettings(IBI.Shared.Models.Accounts.User model);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool ConfirmAdminPassword(string username, string password);
    }
}
