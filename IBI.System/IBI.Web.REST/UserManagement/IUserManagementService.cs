﻿using IBI.Shared.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IBI.REST.UserManagement
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserManagementService" in both code and config file together.
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface IUserManagementService
    {
        /* GetMessages /*********************/

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<UserPage> GetUserPages(int userId);


        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Common.Types.IBIKeyValuePair> GetUserActivityConfigurations(int userId);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        void SetUserActivityConfiguration(int userId, string property, string value);

    }


   
}
