﻿using IBI.Shared.Common.Types;
using IBI.Shared.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace IBI.REST.UserManagement
{
   
   [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UserManagementService : IUserManagementService
    {
        public List<UserPage> GetUserPages(int userId) {
            using (new IBI.Shared.CallCounter("UserManagementService.GetUserPages"))
            {
                return UserManagementController.GetUserPages(userId);
            }
            
        }

        public List<IBIKeyValuePair> GetUserActivityConfigurations(int userId)
        {
            using (new IBI.Shared.CallCounter("UserManagementService.GetUserActivityConfigurations"))
            {
                return UserManagementController.GetUserActivityConfigurations(userId);
            }
            
        }

        public void SetUserActivityConfiguration(int userId, string property, string value)
       {
           using (new IBI.Shared.CallCounter("UserManagementService.SetUserActivityConfiguration"))
           {
               UserManagementController.SetUserActivityConfiguration(userId, property, value);
           }
           
       }

    }
}
