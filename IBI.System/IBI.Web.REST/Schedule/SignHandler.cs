﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Collections.Generic;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Schedule
{
    public class SignHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("SignHandler"))
            {
                String signXml = "";
                String customerId = context.Request["customerid"];
                String signDataId = context.Request["signdataid"];

                if (String.IsNullOrEmpty(customerId))
                    throw new ArgumentException("customerId is not specified", "customerId");

                if (String.IsNullOrEmpty(signDataId))
                    throw new ArgumentException("signDataId is not specified", "signdataid");

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        String script = "SELECT [Picture]" + Environment.NewLine +
                                        "FROM [SignData]" + Environment.NewLine +
                                        "WHERE [SqlId]=" + signDataId + " AND [CustomerId]=" + customerId;

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            SqlDataReader dataReader;

                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                dataReader = command.ExecuteReader();
                            }

                            using (dataReader)
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();

                                    XmlDocument signsDocument = new XmlDocument();
                                    signsDocument.LoadXml(dataReader.GetString(dataReader.GetOrdinal("Picture")));

                                    signXml = signsDocument.OuterXml;
                                }
                            }
                        }
                    }
                }

                Byte[] fileContent = Encoding.UTF8.GetBytes(signXml);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
