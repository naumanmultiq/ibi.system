﻿using System;
using System.Web;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Schedule
{
    public class StaticViaNameListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("StaticViaNameListHandler"))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<Data>");
                sb.AppendLine("  <StaticViaNames>");

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        String script = "DECLARE @line varChar(max)" + Environment.NewLine +
                                        "DECLARE @destination varChar(max)" + Environment.NewLine +
                                        "DECLARE @viaName varChar(max)" + Environment.NewLine +
                                        Environment.NewLine +
                                        "CREATE TABLE #FixedViaNames (" + Environment.NewLine +
                                        "   [Line] varChar(max)," + Environment.NewLine +
                                        "   [Destination] varChar(max)," + Environment.NewLine +
                                        "   [ViaName] varChar(max)" + Environment.NewLine +
                                        ")" + Environment.NewLine +
                                        Environment.NewLine +
                                        "DECLARE cFixedViaNames CURSOR FOR" + Environment.NewLine +
                                        "   SELECT [Line], [Destination]	" + Environment.NewLine +
                                        "   FROM [Destinations]" + Environment.NewLine +
                                        "   GROUP BY [Line], [Destination]" + Environment.NewLine +
                                        "   HAVING COUNT(*)=1" + Environment.NewLine +
                                        "   ORDER BY [Line], [Destination]" + Environment.NewLine +
                                        Environment.NewLine +
                                        "OPEN cFixedViaNames" + Environment.NewLine +
                                         Environment.NewLine +
                                        "FETCH NEXT FROM cFixedViaNames INTO @line, @destination" + Environment.NewLine +
                                        Environment.NewLine +
                                        "WHILE (@@fetch_status <> -1)" + Environment.NewLine +
                                        "BEGIN" + Environment.NewLine +
                                        "   SELECT @viaName = [ViaName] FROM [Destinations] WHERE [Line]=@line AND [Destination]=@destination" + Environment.NewLine +
                                            Environment.NewLine +
                                        "   IF(@viaName <> '')" + Environment.NewLine +
                                        "       INSERT INTO #FixedViaNames([Line], [Destination], [ViaName]) VALUES(@line, @destination, @viaName)" + Environment.NewLine +
                                            Environment.NewLine +
                                        "   FETCH NEXT FROM cFixedViaNames INTO @line, @destination" + Environment.NewLine +
                                        Environment.NewLine +
                                        "END" + Environment.NewLine +
                                        Environment.NewLine +
                                        "DEALLOCATE cFixedViaNames" + Environment.NewLine +
                                        Environment.NewLine +
                                        "SELECT * FROM #FixedViaNames" + Environment.NewLine +
                                        Environment.NewLine +
                                        "DROP TABLE #FixedViaNames" + Environment.NewLine;

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            SqlDataReader dataReader;

                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                dataReader = command.ExecuteReader();
                            }

                            using (dataReader)
                            {
                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {
                                        String line = dataReader.GetString(dataReader.GetOrdinal("Line"));
                                        String destination = dataReader.GetString(dataReader.GetOrdinal("Destination"));
                                        String viaName = dataReader.GetString(dataReader.GetOrdinal("ViaName"));

                                        sb.AppendLine("    <StaticViaName line=\"" + line + "\" destination=\"" + destination + "\">" + destination + "/" + viaName + "</StaticViaName>");
                                    }
                                }
                            }
                        }
                    }
                }

                sb.AppendLine("  </StaticViaNames>");
                sb.AppendLine("</Data>");

                Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
