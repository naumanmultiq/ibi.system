﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Xml;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using IBI.Shared.Models;
using IBI.REST.Shared;
using IBI.Shared;
using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;
using IBI.Implementation.Schedular;
using IBI.Shared.Interfaces;
using System.Data.Entity;
using System.Data.Spatial;
using Microsoft.SqlServer.Types;
using System.Text;
using System.ServiceModel.Web;
using IBI.Shared.Models.Destinations;
using System.Data.Objects;
using IBI.Shared.Models.Signs;
using IBI.REST.Common;
using System.Xml.Linq;
using System.Configuration;
using System.Net.Mime;
using IBI.Shared.Models.SignAddons;
using IBI.Shared.Models.ScheduleTreeItems;
//using Google.KML;

namespace IBI.REST.Schedule
{
    public abstract class ScheduleServiceController
    {
        #region "Attributes"

        private static ISyncScheduleFactory _schedularFactory = new SyncScheduleFactory();

        #endregion

        #region Properties

        #endregion

        private static string ObjectToXml(Type type, object obj)
        {

            System.Xml.Serialization.XmlSerializer xsSubmit = new System.Xml.Serialization.XmlSerializer(type);
            var subReq = obj;
            StringWriter sww = new StringWriter();
            XmlWriter writer = XmlWriter.Create(sww);
            xsSubmit.Serialize(writer, subReq);
            var xml = sww.ToString(); // Your xml 

            return xml;
        }

        private static XmlDocument EmbedAddonsToScheduleStop(string scheduleXml)
        {
            StringReader rd = new StringReader(scheduleXml);
            XDocument xDoc = XDocument.Load(rd);
            var scheduleNode = xDoc.Descendants("ScheduleNumber").FirstOrDefault();

            if (scheduleNode != null)
            {
                int scheduleId = int.Parse(xDoc.Descendants("ScheduleNumber").FirstOrDefault().Value);

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var sAddons = dbContext.ScheduleStopsAddons.Where(ssa => ssa.ScheduleId == scheduleId);


                    foreach (var stopInfo in xDoc.Descendants("StopInfo"))
                    {
                        int stopSequence = int.Parse(stopInfo.Descendants("StopSequence").FirstOrDefault().Value);

                        var addons = sAddons.Where(a => a.StopSequence == stopSequence);
                        foreach (var addon in addons)
                        {
                            string elementName = addon.Key.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
                            XElement addonNode = new XElement(elementName, new XCData(addon.Value.ToString()));
                            stopInfo.Add(addonNode);
                        }

                    }

                }
            }

            XmlDocument retObject = new XmlDocument();
            retObject.LoadXml(xDoc.ToString());


            return retObject;
        }

        public static XmlDocument GetCurrentSchedule(String busNumber, String customerId)
        {
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaWS, "Bus#: " + busNumber + ", Customer: " + customerId + ". GetCurrentSchedule [Schedule Handler Started]");
            using (new IBI.Shared.CallCounter("ScheduleServiceController.GetCurrentSchedule"))
            {
                try
                {
                    //HACK --> hard coded customerId.
                    if (String.IsNullOrEmpty(customerId))
                    {
                        customerId = DBHelper.GetBusCustomerId(busNumber).ToString();
                    }

                    ISyncSchedular schedular = _schedularFactory.GetSyncSchedular(int.Parse(customerId));
                    if (schedular != null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaWS, "Bus#: " + busNumber + ", Customer: " + customerId + ". GetCurrentSchedule [Schedule Handler Returned]");

                        string scheduleXml = schedular.GetCurrentSchedule(AppSettings.GetResourceDirectory(), busNumber);
                        XmlDocument xDoc = new XmlDocument();

                        xDoc.LoadXml(scheduleXml);

                        return xDoc;

                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                }

                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.MoviaWS, "Bus#: " + busNumber + ", Customer: " + customerId + ". GetCurrentSchedule [Schedule Handler Returning Null]");
                return null;
            }
        }

        public static XmlDocument DownloadCurrentSchedule(String busNumber, String customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.DownloadCurrentSchedule"))
            {
                try
                {
                    //HACK --> hard coded customerId.
                    if (String.IsNullOrEmpty(customerId))
                    {
                        customerId = DBHelper.GetBusCustomerId(busNumber).ToString();
                    }
                    ISyncSchedular schedular = _schedularFactory.GetSyncSchedular(int.Parse(customerId));
                    if (schedular != null)
                    {
                        string scheduleXml = schedular.DownloadCurrentSchedule(busNumber);

                        XmlDocument xDoc = new XmlDocument();

                        xDoc.LoadXml(scheduleXml);

                        return xDoc;
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWS, "Bus#: " + busNumber + " DownloadCurrentSchedule [" + ex.Message + "]");
                }
                return null;
            }
        }

        public static string GetInServiceStatus(String busNumber, String customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.GetInServiceStatus"))
            {
                try
                {
                    //HACK --> hard coded customerId.
                    if (String.IsNullOrEmpty(customerId))
                    {
                        customerId = DBHelper.GetBusCustomerId(busNumber).ToString();
                    }
                    ISyncInService schedular = _schedularFactory.GetSyncInService(int.Parse(customerId));
                    if (schedular != null)
                    {
                        string result = schedular.SyncInServiceStatus(busNumber);

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.InServiceStatus, "Bus#: " + busNumber + " GetInServiceStatus [" + ex.Message + "]");
                }
                return null;
            }
        }

        public static ActiveBusReply GetActiveBus()
        {
            return GetActiveBus(1000, false);
        }

        public static ActiveBusReply GetActiveBus(int customerId, int searchSeed, Boolean preferViaNames)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.GetActiveBus"))
            {

                ISyncSchedular schedular = _schedularFactory.GetSyncSchedular(customerId);
                if (schedular != null)
                {
                    return schedular.GetActiveBus(AppSettings.GetResourceDirectory(), searchSeed, preferViaNames);
                }

                return null;
            }
        }

        public static ActiveBusReply GetActiveBus(int searchSeed, Boolean preferViaNames)
        {
            return GetActiveBus(2140, searchSeed, preferViaNames);
        }

        public static StopInfo GetCurrentStop(String busNo)
        {
            return null;
        }

        public static String GetStopName(int customerId, long stopNo, String originalStopName)
        {
            using (new CallCounter("ScheduleServiceController.GetStopName"))
            {

                ISyncSchedular schedular = _schedularFactory.GetSyncSchedular(customerId);
                if (schedular != null)
                {
                    return schedular.GetStopName(stopNo, originalStopName);
                }

                return null;
            }
        }

        public static void SyncAllStops()
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var customers = (from x in dbContext.Customers
                                 select x);

                foreach (var customer in customers)
                {
                    SyncAllStops(customer.CustomerId);
                }
            }
        }

        public static void SyncAllStops(int customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.SyncAllStops"))
            {
                ISyncSchedular schedular = _schedularFactory.GetSyncSchedular(customerId);
                if (schedular != null)
                {
                    schedular.SyncAllStops();
                }
            }
        }

        //public static ZoneLookup GetZone(String latitude, String longitude)
        //{
        //    return GetZone(2140, latitude, longitude);//#hack for 2140
        //}

        //public static ZoneLookup GetZone(int customerId, String latitude, String longitude)
        //{

        //    //ISyncSchedular schedular = _schedularFactory.GetSyncSchedular(customerId);
        //    //if (schedular != null)
        //    //{
        //    //    return schedular.GetZone(latitude, longitude);
        //    //}

        //    //return null;


        //}

        private static String TransformStopNo(String stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = "9025200" + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }

        #region Bus Journey



        public static List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string plannedDeparture, string plannedArrival, string actualDeparture, string actualArrival)
        {

            List<IBI.Shared.Models.BusJourney.Journey> list = new List<IBI.Shared.Models.BusJourney.Journey>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    int jNum = (journeyNumber == null || journeyNumber == "") ? 0 : int.Parse(journeyNumber);

                    string jDate = journeyDate == DateTime.MinValue ? "" : journeyDate.ToString("MM/dd/yyyy");


                    var journeys = dbContext.GetJourneys(line, busNumber, journeyNumber, jDate, from, to, plannedDeparture, plannedArrival, actualDeparture, actualArrival);
                    if (journeys != null)
                    {
                        list = journeys.Select(j => new IBI.Shared.Models.BusJourney.Journey
                        {
                            BusNumber = j.BusNumber,
                            CustomerId = j.CustomerId,
                            Destination = j.Destination,
                            EndTime = j.EndTime,
                            JourneyDate = j.StartTime ?? DateTime.MinValue,
                            JourneyNumber = j.JourneyId,
                            Line = j.Line,
                            PlannedArrivalTime = j.PlannedEndTime ?? DateTime.MinValue,
                            PlannedDepartureTime = j.PlannedStartTime ?? DateTime.MinValue,
                            StartPoint = j.FromName,
                            StartTime = j.StartTime
                        }).ToList();
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }


            return new List<IBI.Shared.Models.BusJourney.Journey>();
        }

        public static List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string stopName, string plannedDeparture, string actualDeparture, string actualArrival, string planDifference, string status)
        {
            List<IBI.Shared.Models.BusJourney.Punctuality> list = new List<IBI.Shared.Models.BusJourney.Punctuality>();
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    int jNum = (journeyNumber == null || journeyNumber == "") ? 0 : int.Parse(journeyNumber);

                    string jDate = journeyDate == DateTime.MinValue ? "" : journeyDate.ToString("MM/dd/yyyy");

                    var punctuality = dbContext.GetPunctuality(line, busNumber, journeyNumber, jDate, from, to, stopName, plannedDeparture, actualArrival, actualDeparture, planDifference, status);
                    if (punctuality != null)
                    {
                        list = punctuality.Select(p => new IBI.Shared.Models.BusJourney.Punctuality
                        {
                            ActualArrival = AppUtility.ToDateTime(p.ArrivalTime),
                            ActualDeparture = AppUtility.ToDateTime(p.DepartureTime),
                            BusNumber = p.BusNumber,
                            CustomerId = p.CustomerID,
                            Destination = p.Destination,
                            JourneyDate = AppUtility.ToDateTime(p.StartTime),
                            JourneyNumber = p.JourneyId,
                            Line = p.Line,
                            PlannedDepartureTime = AppUtility.ToDateTime(p.PlannedDepartureTime),
                            StartPoint = p.FromName,
                            StopName = p.StopName,
                            PlanDifference = p.PlanDifference,
                            Status = p.Status
                        }).ToList();
                    }


                    return list;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }


            return list;
        }


        public static List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(DateTime startDate, string busNumber = "")
        {

            List<IBI.Shared.Models.BusJourney.Journey> list = new List<IBI.Shared.Models.BusJourney.Journey>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    // int jNum = (journeyNumber == null || journeyNumber == "") ? 0 : int.Parse(journeyNumber);

                    //string jDate = startDate == DateTime.MinValue ? "" : startDate.ToString("MM/dd/yyyy");


                    var journeys = dbContext.GetJourneys_NEW(startDate, string.IsNullOrEmpty(busNumber) ? "" : busNumber);
                    if (journeys != null)
                    {
                        list = journeys.Select(j => new IBI.Shared.Models.BusJourney.Journey
                        {
                            BusNumber = j.BusNumber,
                            CustomerId = j.CustomerId,
                            Destination = j.Destination,
                            EndTime = j.EndTime,
                            JourneyDate = j.StartTime ?? DateTime.MinValue,
                            JourneyNumber = j.JourneyId,
                            Line = j.Line,
                            PlannedArrivalTime = j.PlannedEndTime ?? DateTime.MinValue,
                            PlannedDepartureTime = j.PlannedStartTime ?? DateTime.MinValue,
                            StartPoint = j.FromName,
                            StartTime = j.StartTime
                        }).ToList();
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }


            return new List<IBI.Shared.Models.BusJourney.Journey>();
        }

        public static List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(DateTime startDate, string busNumber, int journeyNumber = 0)
        {
            List<IBI.Shared.Models.BusJourney.Punctuality> list = new List<IBI.Shared.Models.BusJourney.Punctuality>();
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    //string jDate = journeyDate == DateTime.MinValue ? "" : journeyDate.ToString("MM/dd/yyyy");

                    var punctuality = dbContext.GetPunctuality_NEW(startDate, busNumber, journeyNumber);
                    if (punctuality != null)
                    {
                        list = punctuality.Select(p => new IBI.Shared.Models.BusJourney.Punctuality
                        {
                            ActualArrival = AppUtility.ToDateTime(p.ArrivalTime),
                            ActualDeparture = AppUtility.ToDateTime(p.DepartureTime),
                            BusNumber = p.BusNumber,
                            CustomerId = p.CustomerID,
                            Destination = p.Destination,
                            JourneyDate = AppUtility.ToDateTime(p.StartTime),
                            JourneyNumber = p.JourneyId,
                            Line = p.Line,
                            PlannedDepartureTime = AppUtility.ToDateTime(p.PlannedDepartureTime),
                            StartPoint = p.FromName,
                            StopName = p.StopName,
                            PlanDifference = p.PlanDifference,
                            Status = p.Status
                        }).ToList();
                    }


                    return list;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }


            return list;
        }

        #endregion

        #region "Destination Pictures"
        #endregion

        #region "Signs"


        public static bool FindScheduleId(int scheduleId)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var retscheduleId = dbContext.Schedules.Where(s =>
                                        s.ScheduleId == scheduleId
                                        ).FirstOrDefault();

                    return (retscheduleId != null && retscheduleId.ScheduleId > 0) ? true : false;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }

        private static bool ShouldRefreshImage(IBI.Shared.Models.Signs.Sign sign, DataAccess.DBModels.SignItem signItem)
        {
            if (string.Compare(sign.Line, signItem.Line) != 0 ||
                string.Compare(sign.MainText, signItem.MainText) != 0 ||
                string.Compare(sign.SubText, signItem.SubText) != 0
                )
            {
                return true;
            }
            return false;
        }

        public static bool SaveSign(IBI.Shared.Models.Signs.Sign sign, string userName)
        {
            try
            {
                bool updateImages = false;
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    DataAccess.DBModels.SignItem signItem = null;
                    DataAccess.DBModels.SignItem oldSignItem = null;
                    int? oldGroupId = null;

                    if (sign.SignId > 0)
                    {
                        signItem = dbContext.SignItems.Single(s => s.SignId == sign.SignId); //GetSignItem(sign.SignId);

                        oldSignItem = ObjectCopier.Clone<SignItem>(signItem);

                        oldGroupId = oldSignItem.GroupId;
                    }

                    if (signItem == null)
                    {
                        signItem = new DataAccess.DBModels.SignItem();
                        signItem.DateAdded = DateTime.Now;
                    }

                    updateImages = ShouldRefreshImage(sign, signItem);

                    signItem.CustomerId = sign.CustomerId;
                    signItem.DateModified = DateTime.Now;
                    signItem.Excluded = !(sign.IsActive);
                    signItem.GroupId = sign.ParentGroupId == 0 ? null : sign.ParentGroupId;
                    signItem.Line = sign.Line;
                    signItem.MainText = sign.MainText;
                    signItem.Name = sign.Name;
                    signItem.ScheduleId = sign.ScheduleId == 0 ? null : sign.ScheduleId;
                    signItem.SignId = sign.SignId;
                    signItem.SortValue = sign.Priority;
                    signItem.SubText = sign.SubText;


                    //SORT ALGORITHM//
                    if (sign.SignId <= 0 || oldGroupId != signItem.GroupId)
                    {
                        var pGroupMembers = dbContext.SignGroups.Where(s => s.CustomerId == signItem.CustomerId && ((s.ParentGroupId == null && signItem.GroupId == null) || s.ParentGroupId == signItem.GroupId)).ToList();
                        var pSignMembers = dbContext.SignItems.Where(s => s.CustomerId == signItem.CustomerId && ((s.GroupId == null && signItem.GroupId == null) || s.GroupId == signItem.GroupId)).ToList();


                        int maxValue = 1;
                        if (pGroupMembers != null || pSignMembers != null)
                        {
                            int gMax = pGroupMembers.Count() > 0 ? pGroupMembers.Max(s => s.SortValue) : 0;
                            int sMax = pSignMembers.Count() > 0 ? pSignMembers.Max(s => s.SortValue) : 0;

                            maxValue = gMax > sMax ? gMax + 1 : sMax + 1;
                        }

                        signItem.SortValue = maxValue == null ? 1 : maxValue;
                    }

                    bool isNew = sign.SignId <= 0;

                    if (isNew)
                    {
                        dbContext.SignItems.Add(signItem);
                    }

                    dbContext.SaveChanges();



                    if (isNew)
                    {

                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_ITEM, ActivityLogger.Action.CREATE, "", null, signItem);

                        //CreateDestination(signItem);
                        SyncDestinationImages(signItem.CustomerId, signItem, userName);
                    }
                    else
                    {

                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_ITEM, ActivityLogger.Action.EDIT, "", oldSignItem, signItem);

                        if (updateImages)
                        {
                            SyncSignData(signItem.SignId, userName);
                        }
                    }
                    HandleDestinationListChange(signItem.CustomerId);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }

        }

        private static void DeleteSignData(int customerId, string mainText, string subText, string line)
        {
            //using (IBIDataModel dbContext = new IBIDataModel())
            //{
            //    SignContent content = dbContext.SignContents.Where(c => c.CustomerId == customerId && c.MainText == mainText && c.SubText == subText && c.Line == line).FirstOrDefault();
            //    if (content != null)
            //    {
            //        int contentId = content.SignId;
            //        dbContext.DeleteObject(content);
            //        SignData[] signElements = dbContext.SignDatas.Where(sd => sd.CustomerId == customerId && sd.SignId == contentId).ToArray<SignData>();
            //        if (signElements.Length > 0)
            //        {
            //            foreach (SignData element in signElements)
            //            {
            //                dbContext.DeleteObject(element);
            //            }
            //        }
            //        dbContext.SaveChanges();
            //    }
            //}
        }

        public static bool DeleteSign(int signId, string userName)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.DataAccess.DBModels.SignItem signItem = dbContext.SignItems.Where(s => s.SignId == signId).FirstOrDefault();

                    if (signItem != null)
                    {
                        string item2Delete = signItem.Name;
                        dbContext.SignItems.Remove(signItem);
                        dbContext.SaveChanges();

                        //update DataStatuses
                        HandleSignDataChange(signItem.CustomerId);
                        HandleDestinationListChange(signItem.CustomerId);
                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_ITEM, ActivityLogger.Action.DELETE, String.Format("SignId: {0}, Name: {1}", signId, item2Delete));
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }

        }

        private static IBI.Shared.Models.Signs.Sign SignExists(List<IBI.Shared.Models.Signs.Sign> signs, IBI.Shared.Models.Signs.Sign item)
        {
            foreach (IBI.Shared.Models.Signs.Sign sign in signs)
            {
                if (sign.Equals(item))
                {
                    return sign;
                }
            }
            return null;
        }

        private static void AddGroupToSigns(IBI.Shared.Models.Signs.Sign sign, SignGroup group, bool leafIncldued, bool showInactive, int customerId, string customerName)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                int? groupId = sign.GroupId;
                string groupName = (group == null) ? sign.Name : group.GroupName;
                List<IBI.Shared.Models.Signs.Sign> subSigns = new List<IBI.Shared.Models.Signs.Sign>();


                List<SignGroup> subGroups = null;
                if (string.Compare(groupName, "Special Destinations", true) == 0)
                {
                    subGroups = dbContext.SignGroups.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && (x.ParentGroupId == null && x.GroupName.StartsWith("#") == false)).OrderBy(b => b.SortValue).ToList();
                }
                else
                    if (string.Compare(groupName, "Destinations", true) == 0)
                    {
                        subGroups = dbContext.SignGroups.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && (x.ParentGroupId == null && x.GroupName.StartsWith("#") == true)).OrderBy(b => b.SortValue).ToList();
                    }
                    else
                    {
                        subGroups = dbContext.SignGroups.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && x.ParentGroupId == groupId).OrderBy(b => b.SortValue).ToList();
                    }


                if (subGroups != null || subGroups.Count() > 0)
                {
                    foreach (SignGroup tmpGroup in subGroups)
                    {
                        Sign tmpSign = new IBI.Shared.Models.Signs.Sign(tmpGroup.GroupId, 0, customerId, customerName, 0, tmpGroup.GroupName, "", "", "", true, groupId, "", tmpGroup.Excluded, tmpGroup.SortValue);


                        subSigns.Add(tmpSign);
                        AddGroupToSigns(tmpSign, tmpGroup, leafIncldued, showInactive, customerId, customerName);
                    }

                }

                if (leafIncldued)
                {
                    // add Items
                    List<SignItem> subItems = dbContext.SignItems.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && (x.GroupId == groupId || (groupId == 10000001 && x.GroupId == null))).OrderBy(b => b.SortValue).ToList();
                    if (subItems != null && subItems.Count() > 0)
                    {
                        // add Items
                        foreach (SignItem subItem in subItems)
                        {
                            subSigns.Add(new IBI.Shared.Models.Signs.Sign(0, subItem.SignId, customerId, customerName, subItem.ScheduleId, subItem.Name, subItem.MainText, subItem.SubText, subItem.Line, false, groupId, groupName, subItem.Excluded, subItem.SortValue));
                        }
                    }
                }


                //subSigns = subSigns.OrderBy(m => m.Priority).ThenBy(b => b.Name).ToList();

                //if(!leafIncldued)
                //    subSigns = subSigns.OrderBy(s => s.Name, new ManualDestinationLineComparer()).ThenBy(s => s.Priority).ToList();
                //else
                subSigns = subSigns.OrderBy(m => m.Priority).ThenBy(b => b.Name).ToList();

                sign.children = subSigns;
            }
        }

        public static SignTree GetSignTree(SignTree tree, bool leafIncldued, bool showInactive, bool setAlphNumericGrouping)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    List<IBI.Shared.Models.Signs.Sign> treeSigns = new List<IBI.Shared.Models.Signs.Sign>();

                    // Add Groups
                    var customer = dbContext.Customers.FirstOrDefault(c => c.CustomerId == tree.CustomerID);
                    if (tree == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, new Exception("tree object is null"));
                    }
                    if (customer == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, new Exception("Customer is null: " + tree.CustomerID));
                    }

                    Sign rootSpecial = new Sign(10000001, 0, tree.CustomerID, customer.FullName, 0, "Special Destinations", "", "", "", true, 0, "", false, 0);
                    treeSigns.Add(rootSpecial);
                    Sign rootNormal = new Sign(10000002, 0, tree.CustomerID, customer.FullName, 0, "Destinations", "", "", "", true, 0, "", false, 0);
                    treeSigns.Add(rootNormal);

                    // root level groups
                    AddGroupToSigns(rootSpecial, null, leafIncldued, showInactive, customer.CustomerId, customer.FullName);
                    AddGroupToSigns(rootNormal, null, leafIncldued, showInactive, customer.CustomerId, customer.FullName);

                    //if (!setAlphNumericGrouping) { 
                    //    Common.ManualDestinationLineComparer comparer = new ManualDestinationLineComparer();
                    //    rootNormal.children = rootNormal.children.OrderBy(s=>s.).OrderBy(s => s.Name, comparer).ToList();
                    //}
                    //treeSigns.
                    ////////////////////////////////////////////////////////////////////////////////////////
                    //[KHI]: Implementation of Alpha Numeric Grouping for Scheduled Signs

                    //if (setAlphNumericGrouping)
                    //{
                    List<Sign> grpList = new List<Sign>();

                    int grpCounter = rootNormal.GroupId.Value;

                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "A-Z", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "0", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "1", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "2", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "3", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "4", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "5", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "6", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "7", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "8", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));
                    grpList.Add(new Sign(++grpCounter, 0, customer.CustomerId, customer.FullName, 0, "9", "", "", "", true, rootNormal.GroupId.Value, rootNormal.Name, false, 0));


                    foreach (Sign subSign in rootNormal.children)
                    {

                        //string grpInitial = subSign.Name.Replace("#", "").Substring(0, 1);

                        string grpInitial = subSign.Name.Length == 0 ? "" : subSign.Name.Replace("#", "").Substring(0, 1);

                        bool isNumeric = false;
                        string grpName;
                        int intGrpName;
                        isNumeric = int.TryParse(grpInitial, out intGrpName);

                        if (!isNumeric)
                        {
                            grpName = "A-Z";
                        }
                        else
                        {
                            grpName = intGrpName.ToString();
                        }

                        Sign grp = grpList.Where(g => g.Name == grpName).FirstOrDefault();

                        subSign.ParentGroupId = grp.GroupId;
                        grp.children.Add(subSign);
                        //tmpSign = new Sign(tmpGroup.GroupId, 0, customerId, customerName, 0, tmpGroup.GroupName, "", "", "", true, grp.GroupId, "", tmpGroup.Excluded, tmpGroup.SortValue ?? 0);

                    }

                    if (setAlphNumericGrouping)
                    {
                        rootNormal.children = grpList;
                    }
                    else
                    {
                        rootNormal.children.Clear();
                        Common.ManualDestinationLineComparer comparer = new ManualDestinationLineComparer();

                        foreach (var group in grpList)
                        {
                            //rootNormal.children.AddRange(group.children.OrderBy(s => s.Name, comparer));
                            rootNormal.children.AddRange(group.children);
                        }
                    }

                    //}
                    //////////////////////////////////////////////////////////////////////////////////////////

                    tree.Signs = treeSigns;
                    return tree;
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, ex);
                }
            }
            return null;
        }

        //internal static string SyncSignData(SignItem signItem)
        internal static string SyncSignData(int signId, string userName)
        {
            string response = string.Empty;

            if (signId <= 0)
            {
                return "<status>SignItem doesn't exist</status>";
            }

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignItem signItem = dbContext.SignItems.Where(s => s.SignId == signId).FirstOrDefault();


                    if (signItem == null)
                        return "<status>SignItem doesn't exist</status>";

                    SignData[] signsDataElements = signItem.SignDatas.ToArray();

                    foreach (SignData element in signsDataElements)
                    {

                        SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == element.LayoutId).FirstOrDefault();
                        SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == element.GraphicsRuleId).FirstOrDefault();
                        if (rule == null)
                        {
                            rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == layout.GraphicsRuleId).FirstOrDefault();
                        }

                        if (signItem != null && layout != null)
                        {
                            SyncSignData(dbContext, signItem, layout, rule, userName);
                            response = "<status>success</status>";
                        }
                        else
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, "The content and layout are not valid for this Sign with signdataid: " + element.SqlId + ", content: " + element.SignId + ", layout: " + element.LayoutId);
                            response = "<status>error</status>";
                        }
                    }
                    dbContext.SaveChanges();
                    HandleDestinationListChange(signItem.CustomerId);
                    HandleSignDataChange(signItem.CustomerId);
                }
            }
            return response;
        }

        internal static Sign GetSign(int signId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signItem = dbContext.SignItems.Where(s => s.SignId == signId).Select(s => new Sign
                    {
                        CustomerId = s.CustomerId,
                        DateAdded = s.DateAdded,
                        DateModified = s.DateModified,
                        GroupId = s.GroupId ?? 0,
                        IsActive = !(s.Excluded),
                        IsGroup = false,
                        Line = s.Line,
                        MainText = s.MainText,
                        ParentGroupId = s.GroupId ?? 0,
                        Priority = s.SortValue,
                        ScheduleId = s.ScheduleId,
                        SignId = s.SignId,
                        Name = s.Name,
                        SubText = s.SubText

                    }).FirstOrDefault();

                    return signItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return null;
            }

        }

        internal static IBI.DataAccess.DBModels.SignItem GetSignItem(int signId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signItem = dbContext.SignItems.Where(s => s.SignId == signId).FirstOrDefault();

                    return signItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return null;
            }

        }

        internal static IBI.DataAccess.DBModels.SignGroup GetSignGroup(int groupId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var group = dbContext.SignGroups.Where(s => s.GroupId == groupId).FirstOrDefault();

                    return group;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return null;
            }

        }

        internal static Sign FindSign(int customerId, int? groupId, string line, string name, string mainText, string subText)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signItem = dbContext.SignItems.Where(s =>

                        s.CustomerId == customerId &&
                        s.GroupId == groupId &&
                        s.Line == line &&
                        s.Name == name &&
                        s.MainText == mainText &&
                        s.SubText == subText
                        ).Select(s => new Sign
                        {
                            CustomerId = s.CustomerId,
                            DateAdded = s.DateAdded,
                            DateModified = s.DateModified,
                            GroupId = s.GroupId,
                            IsActive = !(s.Excluded),
                            IsGroup = false,
                            Line = s.Line,
                            MainText = s.MainText,
                            ParentGroupId = s.GroupId,
                            Priority = s.SortValue,
                            ScheduleId = s.ScheduleId ?? 0,
                            SignId = s.SignId,
                            Name = s.Name,
                            SubText = s.SubText

                        }).FirstOrDefault();

                    return signItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return null;
            }
        }

        internal static IBI.Shared.Models.Signs.Group GetGroup(int groupId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signItem = dbContext.SignGroups.Where(s => s.GroupId == groupId).Select(s => new IBI.Shared.Models.Signs.Group
                    {
                        CustomerId = s.CustomerId,
                        GroupId = s.GroupId,
                        GroupName = s.GroupName,
                        ParentGroupId = s.ParentGroupId,
                        Excluded = s.Excluded,
                        SortValue = s.SortValue

                    }).FirstOrDefault();

                    return signItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return null;
            }

        }

        internal static IBI.Shared.Models.Signs.Group FindSignGroup(int customerId, int? parentGroupId, string groupName)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signGroup = dbContext.SignGroups.Where(s =>

                        s.CustomerId == customerId &&
                        s.GroupName == groupName &&
                        s.ParentGroupId == parentGroupId
                        ).Select(s => new IBI.Shared.Models.Signs.Group
                        {
                            CustomerId = s.CustomerId,
                            ParentGroupId = s.ParentGroupId,
                            GroupId = s.GroupId,
                            GroupName = s.GroupName,
                            DateAdded = s.DateAdded ?? DateTime.MinValue,
                            DateModified = s.DateModified ?? DateTime.MinValue,
                            Excluded = s.Excluded,
                            SortValue = s.SortValue
                        }).FirstOrDefault();

                    return signGroup;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return null;
            }
        }

        internal static bool HasChildSignGroup(int groupId)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signGroup = dbContext.SignGroups.Where(s =>
                        s.ParentGroupId == groupId
                        ).FirstOrDefault();
                    return signGroup != null;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }

        internal static bool HasChildSign(int groupId)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signGroup = dbContext.SignItems.Where(s =>
                        s.GroupId == groupId
                        ).FirstOrDefault();
                    return signGroup != null;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }

        internal static bool SaveSignGroup(IBI.Shared.Models.Signs.Group group, string userName)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    SignGroup oldSignGroup = null;

                    DataAccess.DBModels.SignGroup signGroup = null;
                    if (group.GroupId > 0)
                    {
                        signGroup = dbContext.SignGroups.Single(s => s.GroupId == group.GroupId); //GetSignItem(sign.SignId);
                        oldSignGroup = ObjectCopier.Clone<SignGroup>(signGroup);
                    }

                    if (signGroup == null)
                    {
                        signGroup = new DataAccess.DBModels.SignGroup();
                        signGroup.DateAdded = DateTime.Now;
                    }


                    signGroup.CustomerId = group.CustomerId;
                    signGroup.DateModified = DateTime.Now;
                    signGroup.Excluded = group.Excluded;
                    signGroup.ParentGroupId = group.ParentGroupId;
                    signGroup.GroupName = group.GroupName;
                    signGroup.SortValue = group.SortValue;



                    if (group.GroupId <= 0)
                    {
                        var pGroupMembers = dbContext.SignGroups.Where(s => s.CustomerId == signGroup.CustomerId && ((s.ParentGroupId == null && signGroup.ParentGroupId == null) || s.ParentGroupId == signGroup.ParentGroupId)).ToList();//.Max(s=>s.SortValue);

                        int maxValue = 1;
                        if (pGroupMembers != null && pGroupMembers.Count() > 0)
                        {
                            maxValue = pGroupMembers.Max(s => s.SortValue) + 1;
                        }

                        signGroup.SortValue = maxValue == null ? 1 : maxValue;

                        dbContext.SignGroups.Add(signGroup);
                    }


                    dbContext.SaveChanges();

                    if (group.GroupId <= 0)
                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_GROUP, ActivityLogger.Action.CREATE, "", null, signGroup);
                    else
                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_GROUP, ActivityLogger.Action.EDIT, "", oldSignGroup, signGroup);

                    HandleDestinationListChange(group.CustomerId);

                }



                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }


        internal static bool SaveSignGroup(IBI.Shared.Models.Signs.Group group, int? oldParentId, string userName)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    SignGroup oldSignGroup = null;

                    DataAccess.DBModels.SignGroup signGroup = null;
                    if (group.GroupId > 0)
                    {
                        signGroup = dbContext.SignGroups.Single(s => s.GroupId == group.GroupId); //GetSignItem(sign.SignId);

                        oldSignGroup = ObjectCopier.Clone<SignGroup>(signGroup);

                    }

                    if (signGroup == null)
                    {
                        signGroup = new DataAccess.DBModels.SignGroup();
                        signGroup.DateAdded = DateTime.Now;
                    }


                    signGroup.CustomerId = group.CustomerId;
                    signGroup.DateModified = DateTime.Now;
                    signGroup.Excluded = group.Excluded;
                    signGroup.ParentGroupId = group.ParentGroupId;
                    signGroup.GroupName = group.GroupName;
                    signGroup.SortValue = group.SortValue;

                    //SORT ALGORITHM//
                    if (group.GroupId <= 0 || oldParentId != signGroup.ParentGroupId)
                    {
                        var pGroupMembers = dbContext.SignGroups.Where(s => s.CustomerId == signGroup.CustomerId && ((s.ParentGroupId == null && signGroup.ParentGroupId == null) || s.ParentGroupId == signGroup.ParentGroupId)).ToList();
                        var pSignMembers = dbContext.SignItems.Where(s => s.CustomerId == signGroup.CustomerId && ((s.GroupId == null && signGroup.ParentGroupId == null) || s.GroupId == signGroup.ParentGroupId)).ToList();


                        int maxValue = 1;
                        if (pGroupMembers != null || pSignMembers != null)
                        {
                            int gMax = pGroupMembers.Count() > 0 ? pGroupMembers.Max(s => s.SortValue) : 0;
                            int sMax = pSignMembers.Count() > 0 ? pSignMembers.Max(s => s.SortValue) : 0;
                            maxValue = gMax > sMax ? gMax + 1 : sMax + 1;
                        }

                        signGroup.SortValue = maxValue == null ? 1 : maxValue;
                    }

                    if (group.GroupId <= 0)
                    {
                        dbContext.SignGroups.Add(signGroup);
                    }
                    else
                    {
                        ///var cGroupMembers = dbContext.SignGroups.Where(s => s.CustomerId == signGroup.CustomerId && ((s.ParentGroupId == null && signGroup.ParentGroupId == null) || s.ParentGroupId == signGroup.GroupId)).ToList();
                        //if (cGroupMembers != null && cGroupMembers.Count > 0)
                        //    cGroupMembers.ForEach(p => p.ParentGroupId = signGroup.ParentGroupId);

                        if (group.ParentGroupId != null)
                        {
                            if (group.ParentGroupId.Value != oldParentId)
                            {
                                IBI.Shared.Models.Signs.Group pGroup = GetGroup(group.ParentGroupId.Value);

                                if (pGroup.ParentGroupId == group.GroupId)
                                {
                                    pGroup.ParentGroupId = oldParentId;
                                    SaveSignGroup(pGroup, userName);
                                }

                            }

                        }

                    }

                    dbContext.SaveChanges();

                    if (group.GroupId <= 0)
                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_GROUP, ActivityLogger.Action.CREATE, "", null, signGroup);
                    else
                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_GROUP, ActivityLogger.Action.EDIT, "", oldSignGroup, signGroup);

                    HandleDestinationListChange(group.CustomerId);
                }



                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }

        internal static bool DeleteSignGroup(int groupId, string userName)
        {
            try
            {
                bool hasChildSignGroup = HasChildSignGroup(groupId);
                if (!hasChildSignGroup)
                {
                    bool hasChildSign = HasChildSign(groupId);
                    if (!hasChildSign)
                    {
                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            IBI.DataAccess.DBModels.SignGroup signGroup = dbContext.SignGroups.Where(s => s.GroupId == groupId).FirstOrDefault();

                            if (signGroup != null)
                            {
                                string group2Delete = signGroup.GroupName;
                                dbContext.SignGroups.Remove(signGroup);
                                dbContext.SaveChanges();
                                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_GROUP, ActivityLogger.Action.DELETE, String.Format("GroupId: {0}, GroupName: {1}", groupId, group2Delete));
                                HandleDestinationListChange(signGroup.CustomerId);
                            }
                        }
                        return true;
                    }
                    return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }

        }

        internal static IBI.Shared.Models.Signs.SignXmlLayout GetSignLayout(int signLayoutId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.Shared.Models.Signs.SignXmlLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == signLayoutId).Select(l => new IBI.Shared.Models.Signs.SignXmlLayout
                    {
                        LayoutId = l.LayoutId,
                        CustomerId = l.CustomerId,
                        GraphicRuleId = l.GraphicsRuleId,
                        Height = l.Height,
                        Manufacturer = l.Manufacturer,
                        Technology = l.Technology,
                        Type = l.Type,
                        Width = l.Width
                    }).FirstOrDefault();

                    return layout;
                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }

            return null;
        }

        internal static SignGraphicRule GetSignGraphicRule(int signGraphicRuleId)
        {

            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.Shared.Models.Signs.SignGraphicRule graphicRule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == signGraphicRuleId).Select(r => new IBI.Shared.Models.Signs.SignGraphicRule
                    {
                        AniDelay = r.AniDelay,
                        AllCaps = r.AllCaps,
                        Center = r.Center,
                        DownScale = r.DownScale,
                        GraphicsRuleId = r.GraphicsRuleId,
                        LineArea = r.LineArea,
                        LineDivider = r.LineDivider,
                        LineFont = r.LineFont,
                        LineFontSize = r.LineFontSize,
                        LineX = r.LineX,
                        LineY = r.LineY,
                        MainFont = r.MainFont,
                        MainFontSize = r.MainFontSize,
                        MainStartX = r.MainStartX,
                        MainStartY = r.MainStartY,
                        ShowErrors = r.ShowErrors,
                        ShowGrid = r.ShowGrid,
                        SubFont = r.SubFont,
                        SubFontSize = r.SubFontSize,
                        SubStartX = r.SubStartX,
                        SubStartY = r.SubStartY,
                        template = r.template,
                        UpScale = r.UpScale


                    }).FirstOrDefault();

                    return graphicRule;
                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }

            return null;

        }



        public static string ForceMD5Recalculation(int customerId)
        {
            string response = string.Empty;
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    var signsDataElements = (from x in dbContext.SignDatas
                                             where x.CustomerId == customerId
                                             select x);
                    foreach (var element in signsDataElements)
                    {
                        element.MD5 = AppUtility.ConvertStringtoMD5(element.Picture);
                    }
                    dbContext.SaveChanges();
                    response = "<status>success</status>";
                }
            }
            return response;
        }

        public static string ForceManualRule(int customerId, int ruleid)
        {
            string response = string.Empty;
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignData[] signsDataElements = (from x in dbContext.SignDatas
                                                    where x.CustomerId == customerId && x.GraphicsRuleId == ruleid
                                                    select x).ToArray<SignData>();

                    SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == ruleid).FirstOrDefault();
                    if (rule != null)
                    {
                        foreach (SignData element in signsDataElements)
                        {
                            SignItem content = dbContext.SignItems.Where(c => c.SignId == element.SignId).FirstOrDefault();
                            SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == element.LayoutId).FirstOrDefault();

                            if (content != null && layout != null)
                            {
                                SyncSignData(dbContext, content, layout, rule, "Browser");
                            }
                            else
                            {
                                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, "The content and layout are not valid for this Sign with signdataid: " + element.SqlId + ", content: " + element.SignId + ", layout: " + element.LayoutId);
                            }
                        }
                        dbContext.SaveChanges();
                        HandleSignDataChange(customerId);
                    }
                    else
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, "The rule id is not valid: " + ruleid);
                    }

                    response = "<status>success</status>";
                }
            }
            return response;
        }

        public static bool CreateDestination(SignItem signItem, string userName)
        {
            try
            {
                SyncDestinationImages(signItem.CustomerId, signItem, userName);
                HandleDestinationListChange(signItem.CustomerId);
                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }

        private static void HandleDestinationListChange(int customerId)
        {
            IBI.Shared.Common.Operations.UpdateDataStatus(IBI.Shared.Common.Types.APIDataKeys.DESTINATION_LIST, customerId);
        }

        public static bool UpdateDestination(int signId, string userName)
        {
            try
            {
                //SyncDestinationImages(customerId, content.SignId);
                SyncSignData(signId, userName);

                //IBI.Shared.Common.Operations.UpdateDataStatus(IBI.Shared.Common.Types.APIDataKeys.DESTINATION_LIST, signItem.CustomerId);
                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }
        }


        public static SignXml GetSignContentXmlData(int customerId, int layoutid, int contentid)
        {
            SignXml tmpImage = new SignXml();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    var signsDataElements = (from x in dbContext.SignDatas
                                             where x.CustomerId == customerId && x.SignId == contentid && x.LayoutId == layoutid
                                             select x);
                    foreach (var element in signsDataElements)
                    {
                        tmpImage.Xml = element.Picture;
                    }
                }
            }
            return tmpImage;
        }

        //commented by Sha
        //public static List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignsXmlData_Original(int signId)
        //{
        //    List<IBI.Shared.Models.Signs.SignXmlLayout> layouts = new List<IBI.Shared.Models.Signs.SignXmlLayout>();
        //    using (IBIDataModel dbContext = new IBIDataModel())
        //    {
        //        //List<SignContent> contents = dbContext.SignContents.Where(m => m.CustomerId == customerId && (string.IsNullOrEmpty(mainText) || m.MainText == mainText) && (string.IsNullOrEmpty(subText) || m.SubText == subText) && (string.IsNullOrEmpty(line) || m.Line == line)).ToList();
        //        SignItem signItem = dbContext.SignItems.Where(m => m.SignId == signId).FirstOrDefault();

        //        if (signItem != null)
        //        {
        //            var signsDataElements = signItem.SignDatas.ToList();

        //            SignXmlLayout newLayoutInfo = null;
        //            List<SignXmlData> images = new List<SignXmlData>();
        //            SignXmlData tmpImage = null;

        //            //List<IBI.Shared.Models.Signs.SignXmlLayout> retVal = signsDataElements.Select(s=> new SignXmlLayout
        //            //    {
        //            //     CustomerId = s.CustomerId,
        //            //      GraphicRuleId=s.GraphicsRuleId,
        //            //       Height=s.Sign
        //            //    }
        //            //    )

        //            //temparory commented
        //            foreach (var element in signsDataElements)
        //            {
        //                SignLayout tmpLayout = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId && m.CustomerId == element.CustomerId ).FirstOrDefault();
        //                if (tmpLayout == null) continue;
        //                SignXmlLayout tmpNewLayoutInfo = new SignXmlLayout(tmpLayout, element.GraphicsRuleId);

        //                if (newLayoutInfo != null)
        //                {
        //                    if (newLayoutInfo.LayoutId != tmpNewLayoutInfo.LayoutId)
        //                    {
        //                        newLayoutInfo.Signs = images.ToArray();
        //                        layouts.Add(newLayoutInfo);
        //                        images = new List<SignXmlData>();
        //                    }
        //                }

        //                tmpImage = new SignXmlData();
        //                tmpImage.ContentId = element.SignId.ToString();
        //                tmpImage.SignId = element.SqlId.ToString();
        //                tmpImage.Page = element.Page.ToString();
        //                tmpImage.Base64 = element.Picture;
        //                tmpImage.MD5 = element.MD5;

        //                images.Add(tmpImage);

        //                newLayoutInfo = new SignXmlLayout(tmpLayout, element.GraphicsRuleId);
        //            }
        //            if (newLayoutInfo != null)
        //            {
        //                newLayoutInfo.Signs = images.ToArray();
        //                layouts.Add(newLayoutInfo);
        //            }
        //        }
        //    }
        //    return layouts;
        //}

        public static List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignsXmlData(int signId)
        {
            List<IBI.Shared.Models.Signs.SignXmlLayout> layouts = new List<IBI.Shared.Models.Signs.SignXmlLayout>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignItem signItem = dbContext.SignItems.Where(m => m.SignId == signId).FirstOrDefault();
                if (signItem != null)
                {
                    SignXmlData tmpImage = null;
                    var tmpLayouts = dbContext.SignLayouts.Where(sl => sl.CustomerId == signItem.CustomerId).ToList();
                    foreach (var tmpLayout in tmpLayouts)
                    {
                        var element = signItem.SignDatas.Where(sd => sd.CustomerId == signItem.CustomerId && sd.LayoutId == tmpLayout.LayoutId && sd.SignId == signItem.SignId).FirstOrDefault();
                        SignXmlLayout tmpNewLayoutInfo = new SignXmlLayout(tmpLayout, element == null ? tmpLayout.GraphicsRuleId : element.GraphicsRuleId);

                        tmpImage = new SignXmlData();
                        tmpImage.ContentId = signItem.SignId.ToString();
                        tmpImage.SignId = element == null ? signItem.SignId.ToString() : element.SqlId.ToString();
                        tmpImage.Page = element == null ? string.Empty : element.Page.ToString();
                        tmpImage.Base64 = element == null ? string.Empty : element.Picture;
                        tmpImage.MD5 = element == null ? string.Empty : element.MD5;
                        tmpNewLayoutInfo.Signs = new SignXmlData[] { tmpImage };
                        layouts.Add(tmpNewLayoutInfo);
                    }
                }
            }
            return layouts;
        }

        public static string GetSignContentData(int customerId, int layoutid, int contentid)
        {
            StringBuilder sb = new StringBuilder();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                sb.AppendLine("<signs>");
                {
                    var signsDataElements = (from x in dbContext.SignDatas
                                             where x.CustomerId == customerId && x.SignId == contentid && x.LayoutId == layoutid
                                             select x);
                    string contentData = "";
                    int numPages = 0;

                    SignLayout layoutInfo = null;


                    foreach (var element in signsDataElements)
                    {
                        contentData += "<image page='" + element.Page + "' md5='" + element.MD5 + "' base64='" + element.Picture + "'></image>";
                        numPages++;
                        layoutInfo = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();
                    }
                    if (numPages > 0)
                    {
                        sb.AppendLine("<images count='" + numPages + "'>");
                        sb.AppendLine(contentData);
                        sb.AppendLine("</images>");
                        numPages = 0;
                        contentData = "";
                    }
                }
            }
            sb.AppendLine("</signs>");
            return sb.ToString();

        }

        public static List<SignDataImageItem> GetSignImages(int customerId, int layoutid, int contentid)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signsDataElements = (from x in dbContext.SignDatas
                                             where x.CustomerId == customerId && x.SignId == contentid && x.LayoutId == layoutid
                                             select new
                                             {
                                                 signDataId = x.SqlId,
                                                 Picture = x.Picture,
                                                 ViewedPicture = x.ViewedPicture
                                             }
                                            ).FirstOrDefault();


                    if (signsDataElements != null)
                    {

                        List<SignDataImageItem> imgItem = new List<SignDataImageItem>();
                        string strImage = (string.IsNullOrEmpty(signsDataElements.ViewedPicture) ? signsDataElements.Picture.ToString() : signsDataElements.ViewedPicture.ToString());
                        XDocument xdoc = XDocument.Parse(strImage);
                        var base64Images = (from x in xdoc.Descendants("image")
                                            select new
                                            {
                                                base64 = x.Attribute("base64").Value,
                                                base64View = x.Attribute("base64View") == null ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(x.Attribute("base64").Value)) : x.Attribute("base64View").Value,
                                                base64ViewLarge = x.Attribute("base64ViewLarge") == null ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImageLarge(x.Attribute("base64").Value)) : x.Attribute("base64ViewLarge").Value
                                            }
                                            ).ToList();

                        foreach (var base64Image in base64Images)
                        {
                            SignDataImageItem signDataImageItem = new SignDataImageItem();
                            signDataImageItem.SignDataId = signsDataElements.signDataId;
                            signDataImageItem.Base64String = base64Image.base64;
                            signDataImageItem.base64ViewString = base64Image.base64View;
                            signDataImageItem.base64ViewLargeString = base64Image.base64ViewLarge;
                            imgItem.Add(signDataImageItem);
                        }


                        return imgItem;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        //public static string GetSignImages(int customerId, int layoutid, int contentid)
        //{
        //    try
        //    {
        //        using (IBIDataModel dbContext = new IBIDataModel())
        //        {
        //            var signsDataElements = (from x in dbContext.SignDatas
        //                                     where x.CustomerId == customerId && x.SignId == contentid && x.LayoutId == layoutid
        //                                     select new
        //                                     {
        //                                         signDataId = x.SqlId,
        //                                         Picture = x.Picture
        //                                     }
        //                                    ).FirstOrDefault();





        //            return signsDataElements != null ? string.Format("{0}$${1}", signsDataElements.signDataId, signsDataElements.Picture) : string.Empty;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        private static string GenerateSignsData(int customerId, string mainText, string subText, string line)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable layOutTable = new Hashtable();
            Hashtable signDataTable = new Hashtable();
            ArrayList layoutIdList = new ArrayList();
            // debug
            string logsData = string.Empty;
            DateTime before = DateTime.Now;
            DateTime after = DateTime.Now;

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<SignItem> contents = dbContext.SignItems.Where(m => m.CustomerId == customerId && (string.IsNullOrEmpty(mainText) || m.MainText == mainText) && (string.IsNullOrEmpty(subText) || m.SubText == subText) && (string.IsNullOrEmpty(line) || m.Line == line)).ToList();
                logsData += "Total records " + contents.Count;
                sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                sb.AppendLine("<signs>");

                //var List<SignLayout> customerLaouts

                foreach (SignItem signContent in contents)
                {

                    var signsDataElements = (from x in dbContext.SignDatas
                                             join l in dbContext.SignLayouts on x.LayoutId equals l.LayoutId
                                             where x.CustomerId == customerId && l.CustomerId == customerId && x.SignId == signContent.SignId
                                             select x);
                    string contentData = "";
                    int numPages = 0;

                    SignLayout layoutInfo = null;// = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();


                    foreach (var element in signsDataElements)
                    {
                        SignLayout tmpLayout = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();
                        if (layoutInfo != null)
                        {
                            if (layoutInfo.LayoutId != tmpLayout.LayoutId)
                            {
                                if (!layOutTable.ContainsKey(layoutInfo.LayoutId))
                                {
                                    layOutTable[layoutInfo.LayoutId] = layoutInfo;
                                    layoutIdList.Add(layoutInfo.LayoutId);
                                }
                                if (signDataTable.ContainsKey(layoutInfo.LayoutId))
                                {
                                    string tmpdata = (string)signDataTable[layoutInfo.LayoutId];
                                    tmpdata += contentData;
                                    signDataTable[layoutInfo.LayoutId] = tmpdata;
                                }
                                else
                                {
                                    signDataTable[layoutInfo.LayoutId] = contentData;
                                }
                                numPages = 0;
                                contentData = "";
                            }
                        }

                        contentData += "<signdata line='" + signContent.Line + "' maintext='" + HttpContext.Current.Server.HtmlEncode(signContent.MainText) + "' subtext='" + HttpContext.Current.Server.HtmlEncode(signContent.SubText) + "' md5='" + element.MD5 + "' signdataid='" + element.SqlId + "' signitemid='" + signContent.SignId + "'></signdata>";
                        numPages++;
                        layoutInfo = tmpLayout;
                    }
                    if (numPages > 0)
                    {
                        if (!layOutTable.ContainsKey(layoutInfo.LayoutId))
                        {
                            layOutTable[layoutInfo.LayoutId] = layoutInfo;
                            layoutIdList.Add(layoutInfo.LayoutId);
                        }
                        if (signDataTable.ContainsKey(layoutInfo.LayoutId))
                        {
                            string tmpdata = (string)signDataTable[layoutInfo.LayoutId];
                            tmpdata += contentData;
                            signDataTable[layoutInfo.LayoutId] = tmpdata;
                        }
                        else
                        {
                            signDataTable[layoutInfo.LayoutId] = contentData;
                        }

                        numPages = 0;
                        contentData = "";
                    }
                }
                after = DateTime.Now;
                logsData += "Fetching from DB and making list took " + (after - before).TotalSeconds;
                before = DateTime.Now;
                foreach (int layoutid in layoutIdList)
                {
                    SignLayout tmpLayout = (SignLayout)layOutTable[layoutid];
                    sb.AppendLine("<layout layoutid='" + tmpLayout.LayoutId + "' type='" + tmpLayout.Type + "' technology='" + tmpLayout.Technology + "' manufacturer='" + tmpLayout.Manufacturer + "' height='" + tmpLayout.Height + "' width='" + tmpLayout.Width + "'>");
                    sb.AppendLine((string)signDataTable[layoutid]);
                    sb.AppendLine("</layout>");
                }
                after = DateTime.Now;
                logsData += "result compisition took " + (after - before).TotalSeconds;
            }
            sb.AppendLine("</signs>");
            return sb.ToString();

        }

        public static string GetSignsData(int customerId, string mainText, string subText, string line)
        {
            FileInfo fInfo = new FileInfo(Path.Combine(System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"], "Cache\\SignList_" + customerId + ".xml"));
            if (fInfo.Exists)
            {
                return File.ReadAllText(fInfo.FullName);
            }

            return GenerateSignsData(customerId, mainText, subText, line);
        }

        public static string GetDestinationImagesData(int customerId, string mainText, string subText, string line)
        {
            StringBuilder sb = new StringBuilder();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<SignItem> contents = dbContext.SignItems.Where(m => m.CustomerId == customerId && (string.IsNullOrEmpty(mainText) || m.MainText == mainText) && (string.IsNullOrEmpty(subText) || m.SubText == subText) && (string.IsNullOrEmpty(line) || m.Line == line)).ToList();
                sb.Append("<signs customerid='" + customerId + "'>");
                foreach (SignItem signContent in contents)
                {
                    var signsDataElements = (from x in dbContext.SignDatas
                                             where x.CustomerId == customerId && x.SignId == signContent.SignId
                                             select x);
                    string contentData = "";
                    int numPages = 0;

                    SignLayout layoutInfo = null;// = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();

                    foreach (var element in signsDataElements)
                    {
                        contentData += "<image page='" + element.Page + "' md5='" + element.MD5 + "' base64='" + element.Picture + "'></image>";
                        numPages++;
                        layoutInfo = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();
                    }
                    if (numPages > 0)
                    {
                        sb.Append("<images count='" + numPages + "' maintext='" + signContent.MainText + "' subtext='" + signContent.SubText + "' line='" + signContent.Line + "' height='" + layoutInfo.Height + "' width='" + layoutInfo.Width + "'>");
                        sb.Append(contentData);
                        sb.Append("</images>");
                        numPages = 0;
                        contentData = "";
                    }
                }
            }
            sb.Append("</signs>");
            return sb.ToString();
        }

        public static SignGraphicRule GetGraphicRule(int graphicRuleId, string userName)
        {
            SignGraphicRule signRule = new SignGraphicRule();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == graphicRuleId).FirstOrDefault();
                if (rule != null)
                {
                    signRule.AllCaps = rule.AllCaps;
                    signRule.AniDelay = rule.AniDelay;
                    signRule.Center = rule.Center;
                    signRule.DownScale = rule.DownScale;
                    signRule.GraphicsRuleId = rule.GraphicsRuleId;
                    signRule.LineArea = rule.LineArea;
                    signRule.LineDivider = rule.LineDivider;
                    signRule.LineFont = rule.LineFont;
                    signRule.LineFontSize = rule.LineFontSize;
                    signRule.LineX = rule.LineX;
                    signRule.LineY = rule.LineY;
                    signRule.MainFont = rule.MainFont;
                    signRule.MainFontSize = rule.MainFontSize;
                    signRule.MainStartX = rule.MainStartX;
                    signRule.MainStartY = rule.MainStartY;
                    signRule.ShowErrors = rule.ShowErrors;
                    signRule.ShowGrid = rule.ShowGrid;
                    signRule.SubFont = rule.SubFont;
                    signRule.SubFontSize = rule.SubFontSize;
                    signRule.SubStartX = rule.SubStartX;
                    signRule.SubStartY = rule.SubStartY;
                    signRule.template = rule.template;
                    signRule.UpScale = rule.UpScale;
                }
                return signRule;
            }
        }

        public static int SaveGraphicRule(SignGraphicRule rule, string userName)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignGraphicsRule signRule = new SignGraphicsRule();
                signRule.AllCaps = rule.AllCaps;
                signRule.AniDelay = rule.AniDelay;
                signRule.Center = rule.Center;
                signRule.DownScale = rule.DownScale;
                signRule.LineArea = rule.LineArea;
                signRule.LineDivider = rule.LineDivider;
                signRule.LineFont = rule.LineFont;
                signRule.LineFontSize = rule.LineFontSize;
                signRule.LineX = rule.LineX;
                signRule.LineY = rule.LineY;
                signRule.MainFont = rule.MainFont;
                signRule.MainFontSize = rule.MainFontSize;
                signRule.MainStartX = rule.MainStartX;
                signRule.MainStartY = rule.MainStartY;
                signRule.ShowErrors = rule.ShowErrors;
                signRule.ShowGrid = rule.ShowGrid;
                signRule.SubFont = rule.SubFont;
                signRule.SubFontSize = rule.SubFontSize;
                signRule.SubStartX = rule.SubStartX;
                signRule.SubStartY = rule.SubStartY;
                signRule.template = rule.template;
                signRule.UpScale = rule.UpScale;
                signRule.Note = rule.Note;
                dbContext.SignGraphicsRules.Add(signRule);
                dbContext.SaveChanges();
                return signRule.GraphicsRuleId;
            }
        }

        public static bool SaveSignDataImages(int layoutId, SignXmlData signData, string userName)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    int? singId = int.Parse(signData.SignId);
                    int customerid = dbContext.SignItems.Where(s => s.SignId == singId).Select(c => c.CustomerId).FirstOrDefault();
                    SignData sData = dbContext.SignDatas.Where(s => s.SignId == singId && s.CustomerId == customerid && s.LayoutId == layoutId).FirstOrDefault();
                    if (sData == null)
                    {
                        //singId = int.Parse(signData.ContentId);
                        sData = new SignData();
                        sData.CustomerId = customerid; //dbContext.SignItems.Where(s => s.SignId == singId).Select(c => c.CustomerId).FirstOrDefault();
                        sData.Picture = signData.Base64;
                        sData.Page = 1;
                        sData.SignId = singId;
                        sData.LayoutId = layoutId;
                        sData.MD5 = AppUtility.ConvertStringtoMD5(signData.Base64);
                        sData.GraphicsRuleId = dbContext.SignGraphicsRules.Where(r => r.template.ToLower() == "manual").Select(t => t.GraphicsRuleId).FirstOrDefault();

                        dbContext.SignDatas.Add(sData);
                    }
                    else
                    {
                        sData.Picture = signData.Base64;
                        sData.MD5 = AppUtility.ConvertStringtoMD5(signData.Base64);
                        sData.ViewedPicture = signData.Page;
                        sData.GraphicsRuleId = dbContext.SignGraphicsRules.Where(r => r.template.ToLower() == "manual").Select(t => t.GraphicsRuleId).FirstOrDefault();

                    }
                    dbContext.SaveChanges();
                    HandleSignDataChange(sData.CustomerId.Value);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static bool SaveImageGeneratorData(int layoutId, int signItemId, int signRuleId, string userName)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignItem signItem = dbContext.SignItems.Where(s => s.SignId == signItemId).FirstOrDefault();
                    if (signItem != null)
                    {
                        SignLayout[] layouts = (from y in dbContext.SignLayouts
                                                where y.CustomerId == signItem.CustomerId && (y.LayoutId == layoutId || layoutId == 0)
                                                select y).ToArray<SignLayout>();

                        SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == signRuleId).FirstOrDefault();
                        foreach (SignLayout layout in layouts)
                        {
                            if (rule == null)
                            {
                                rule = (from r in dbContext.SignGraphicsRules
                                        where r.GraphicsRuleId == layout.GraphicsRuleId
                                        select r).FirstOrDefault();
                            }
                            if (rule != null)
                            {
                                SyncSignData(dbContext, signItem, layout, rule, userName);
                            }

                        }
                        dbContext.SaveChanges();
                        HandleSignDataChange(signItem.CustomerId);
                        return true;
                    }

                }
            }
            return false;
        }


        public static bool SaveSignData(int signItemId, int signRuleId, string userName)
        {
            return SaveImageGeneratorData(0, signItemId, signRuleId, userName);
            /*
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignItem signItem = dbContext.SignItems.Where(s => s.SignId == signItemId).FirstOrDefault();
                    if (signItem != null)
                    {
                        SignLayout[] layouts = (from y in dbContext.SignLayouts
                                                where y.CustomerId == signItem.CustomerId
                                                select y).ToArray<SignLayout>();

                        SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == signRuleId).FirstOrDefault();
                        foreach (SignLayout layout in layouts)
                        {
                            if (rule == null)
                            {
                                rule = (from r in dbContext.SignGraphicsRules
                                        where r.GraphicsRuleId == layout.GraphicsRuleId
                                        select r).FirstOrDefault();
                            }
                            if (rule != null)
                            {
                                SyncSignData(dbContext, signItem, layout, rule, userName);
                            }

                        }
                        dbContext.SaveChanges();
                        return true;
                    }

                }
            }
            return false;
             */
        }

        public static List<string> GetImagePreview(int signItemId, SignGraphicRule signGraphicRule, int layoutId, string userName)
        {
            List<string> imagebase64 = null;
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignItem signItem = dbContext.SignItems.Where(s => s.SignId == signItemId).FirstOrDefault();
                if (signItem != null)
                {
                    SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == layoutId).FirstOrDefault();
                    if (signItem != null && layout != null)
                    {
                        string url = string.Empty;
                        string retXML = GetSignDataPreviewXml(signItem, layout, signGraphicRule, out url);

                        XDocument xdoc = XDocument.Parse(retXML);
                        imagebase64 = (from img in xdoc.Descendants("image")
                                       select (img.Attribute("base64View") == null) ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(img.Attribute("base64").Value)) : img.Attribute("base64View").Value
                                       ).ToList();
                        return imagebase64;
                    }
                }
            }
            return imagebase64;
        }
        public static List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignPreview(int signItemId, int signRuleId, int layoutId, string userName)
        {
            List<IBI.Shared.Models.Signs.SignXmlLayout> layouts = new List<IBI.Shared.Models.Signs.SignXmlLayout>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignItem signItem = dbContext.SignItems.Where(s => s.SignId == signItemId).FirstOrDefault();
                    if (signItem != null)
                    {
                        SignData[] signsDataElements = signItem.SignDatas.Where(sd => sd.LayoutId == layoutId || layoutId == 0).ToArray();
                        SignXmlLayout newLayoutInfo = null;
                        List<SignXmlData> images = new List<SignXmlData>();
                        SignXmlData tmpImage = null;

                        foreach (SignData element in signsDataElements)
                        {
                            SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == element.LayoutId).FirstOrDefault();
                            SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == signRuleId).FirstOrDefault();
                            if (rule == null)
                            {
                                rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == layout.GraphicsRuleId).FirstOrDefault();
                            }

                            if (signItem != null && layout != null)
                            {
                                SignXmlLayout tmpNewLayoutInfo = new SignXmlLayout(layout);
                                if (newLayoutInfo != null)
                                {
                                    if (newLayoutInfo.LayoutId != tmpNewLayoutInfo.LayoutId)
                                    {
                                        newLayoutInfo.Signs = images.ToArray();
                                        layouts.Add(newLayoutInfo);
                                        images = new List<SignXmlData>();
                                    }
                                }

                                string url = string.Empty;
                                string dataXml = string.Empty;
                                string ledDataXml = string.Empty;

                                if (String.Compare(rule.template, "MANUAL", true) != 0)
                                {
                                    dataXml = GetValidPreviewXmlData(GetSignDataPreviewXml(signItem, layout, rule, out url));
                                    dataXml = GetLEDPreviewXmlData(dataXml);
                                    tmpImage.MD5 = AppUtility.ConvertStringtoMD5(dataXml);
                                }
                                else
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.SIGNMANAGEMENT, "The preview is not generated because rule id: " + rule.GraphicsRuleId + " has template=MANUAL");
                                    dataXml = string.Empty;
                                    tmpImage.MD5 = string.Empty;
                                }
                                tmpImage = new SignXmlData();
                                tmpImage.ContentId = element.SignId.ToString();
                                tmpImage.SignId = "0";
                                tmpImage.Page = "0";
                                tmpImage.Base64 = dataXml;
                                images.Add(tmpImage);

                                newLayoutInfo = new SignXmlLayout(layout);
                            }
                            if (newLayoutInfo != null)
                            {
                                newLayoutInfo.Signs = images.ToArray();
                                layouts.Add(newLayoutInfo);
                            }
                        }
                    }

                }
            }
            return layouts;

        }




        private static string GetImagePreviewXml(SignItem signItem, IBI.DataAccess.DBModels.SignLayout layout, SignGraphicsRule graphicRule)
        {
            string imagexml = string.Empty;
            string serverPath = string.IsNullOrEmpty(AppSettings.GetImageBuilderServerUrl()) ? "http://krusty.mermaid.dk/imagegen/buildimage.php?" : AppSettings.GetImageBuilderServerUrl();
            string url = serverPath + "output=xml&width=" + layout.Width + "&height=" + layout.Height;
            string mainText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.MainText : "";
            string subText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.SubText : "";

            url += "&linearea=" + graphicRule.LineArea + "&linedivider=" + graphicRule.LineDivider;
            url += "&anidelay=" + graphicRule.AniDelay;
            url += "&center=" + graphicRule.Center;
            url += "&downscale=" + graphicRule.DownScale;
            url += "&upscale=" + graphicRule.UpScale;
            url += "&showerrors=" + graphicRule.ShowErrors;
            url += "&line=" + signItem.Line + "&linefont=" + graphicRule.LineFont;
            url += "&linefontsize=" + graphicRule.LineFontSize + "&linex=" + graphicRule.LineX + "&liney=" + graphicRule.LineY + "&main=" + mainText + "&mainfont=" + graphicRule.MainFont + "&mainfontsize=" + graphicRule.MainFontSize + "&mainx=" + graphicRule.MainStartX + "&mainy=" + graphicRule.MainStartY + "&sub=" + subText + "&subfont=" + graphicRule.SubFont + "&subfontsize=" + graphicRule.SubFontSize + "&subx=" + graphicRule.SubStartX + "&suby=" + graphicRule.SubStartY + "&showgrid=" + graphicRule.ShowGrid + "&allcaps=" + graphicRule.AllCaps;
            url += "&template=" + graphicRule.template;
            url += "&note=" + graphicRule.Note;
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBIData, url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse urlResponse = (HttpWebResponse)request.GetResponse();
            if (urlResponse.StatusCode == HttpStatusCode.OK && urlResponse.ContentLength > 0)
            {
                TextReader reader = new StreamReader(urlResponse.GetResponseStream(), Encoding.Default);
                imagexml = reader.ReadToEnd();
            }

            return imagexml;
        }

        private static bool SyncSignData(IBIDataModel dbContext, SignItem signItem, IBI.DataAccess.DBModels.SignLayout layout, SignGraphicsRule graphicRule, string userName)
        {
            string url = string.Empty;
            string imagexml;
            string dataXml = string.Empty;
            string viewedDataXml = string.Empty;

            if (String.Compare(graphicRule.template, "MANUAL", true) == 0)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.SIGNMANAGEMENT, "The rule template is manual so not sending the call, ruleid: " + graphicRule.GraphicsRuleId);
            }
            else
            {
                imagexml = GetSignDataPreviewXml(signItem, layout, graphicRule, out url);
                if (!string.IsNullOrEmpty(imagexml))
                {
                    dataXml = GetValidPreviewXmlData(imagexml);

                    viewedDataXml = GetLEDPreviewXmlData(dataXml);
                }
                else
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.SIGNMANAGEMENT, "The XML generated from image generator is null/empty");
                }
            }

            SignData signData = dbContext.SignDatas.Where(m => m.SignId == signItem.SignId && m.LayoutId == layout.LayoutId).FirstOrDefault();
            bool bNew = false;

            SignData oldSignData = null;

            if (signData == null)
            {
                signData = new SignData();
                bNew = true;

            }
            else
            {
                oldSignData = ObjectCopier.Clone<SignData>(signData);
            }
            signData.SignId = signItem.SignId;
            signData.CustomerId = signItem.CustomerId;
            signData.LayoutId = layout.LayoutId;
            signData.Page = 1;
            if (String.Compare(graphicRule.template, "MANUAL", true) != 0)
            {
                signData.Picture = dataXml;
                signData.ViewedPicture = viewedDataXml;
                if (!String.IsNullOrEmpty(dataXml))
                {
                    signData.MD5 = AppUtility.ConvertStringtoMD5(dataXml);
                }
                else
                {
                    signData.MD5 = "";
                }
            }
            signData.GraphicsRuleId = graphicRule.GraphicsRuleId;
            if (bNew)
            {
                dbContext.SignDatas.Add(signData);

                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_IMAGE, ActivityLogger.Action.CREATE, "", null, signData);
            }
            else
            {
                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_IMAGE, ActivityLogger.Action.EDIT, "", oldSignData, signData);
            }

            //update SignData TimeStamp
            //HandleSignDataChange(signItem.CustomerId);

            IBI.Shared.Models.Accounts.User usr = IBI.REST.Auth.UserAuthServiceController.GetUser(userName);
            string uName = usr == null ? userName : usr.FullName;

            string customerName = DBHelper.GetCustomerName(signItem.CustomerId).ToString();

            SendNotificationEmail(uName, signItem.CustomerId, customerName, signItem.MainText, signItem.SubText, signItem.Line, layout, signItem, url, dataXml);
            return true;
        }

        private static void SendNotificationEmail(string userName, int customerId, string customerName, string mainText, string subText, string line, IBI.DataAccess.DBModels.SignLayout layout, SignItem destination, string url, string imageXml)
        {
            try
            {
                XDocument xdoc = XDocument.Parse(imageXml);
                var imagebase64 = (from img in xdoc.Descendants("image")
                                   select new
                                   {
                                       base64 = img.Attribute("base64").Value,
                                       base64View = (img.Attribute("base64View") == null) ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(img.Attribute("base64").Value)) : img.Attribute("base64View").Value
                                   }
                                ).ToList();

                string signLayout = layout.Type + " - " + layout.Width + " × " + layout.Height + "  " + layout.Manufacturer + "  -  " + layout.Technology;

                List<Attachment> attachments = new List<Attachment>();
                string sbOutput = "";
                if (imagebase64.Count > 0)
                {
                    int index = 0;
                    int i = 0;
                    foreach (var image in imagebase64)
                    {

                        byte[] byteBuffer = Convert.FromBase64String(image.base64);
                        MemoryStream memoryStream = new MemoryStream(byteBuffer);
                        Attachment blackWhiteAttachment = new Attachment(memoryStream, "blackwhite.png", "image/png");
                        blackWhiteAttachment.ContentId = "BlackWhiteImage" + i;
                        attachments.Add(blackWhiteAttachment);


                        byte[] imgBytes = Convert.FromBase64String(image.base64View);
                        MemoryStream LEDmemoryStream = new MemoryStream(imgBytes);
                        Attachment LEDAttachment = new Attachment(LEDmemoryStream, "LED.png", "image/png");
                        LEDAttachment.ContentId = "LEDImage" + i;
                        attachments.Add(LEDAttachment);

                        sbOutput += "<img  style='border:1px solid #021a40;' src=\"cid:BlackWhiteImage" + i + "\"><br/><br/><img src=\"cid:LEDImage" + i + "\"><br/><br/>";
                        index = (index + 1) % 3;
                        if (index == 0)
                        {
                            //sbOutput += "</div>";
                            sbOutput += "<br/>";
                        }
                        i++;
                    }
                    if (index != 0)
                    {
                        //sbOutput += "</div>";
                        sbOutput += "<br/>";
                    }

                }
                else
                {
                    //sbOutput += "<div  style='color:red;padding: 10px;font-weight: bold;'><b>There are no images available</b></div>";
                    sbOutput += "<br/><b>There are no images available</b>";
                }


                StringBuilder body = new StringBuilder();
                body.AppendLine("<html><body>");
                body.AppendLine("<font size='3' face='Helvetica'>A destination has been created/updated by '" + userName + "'</fond><br/>");


                body.AppendLine("<br><font size='3'><b>CustomerId</b> : " + customerId);
                body.AppendLine("<br><b>CustomerName</b> : " + customerName + "</font><br>");


                body.AppendLine("<br><font size='3'><b>Line&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>: " + line);
                body.AppendLine("<br/><b>MainText</b>&nbsp;&nbsp; : " + mainText);
                body.AppendLine("<br/><b>SubText</b>&nbsp;&nbsp;&nbsp;&nbsp;: " + subText + "</font>");
                body.AppendLine("<br/><br/><span style='font-size:18pt;'><hr>");
                body.AppendLine("<br/><font size='3'>" + signLayout + "</font><br/><br/>");
                body.AppendLine(sbOutput);


                body.AppendLine("<span style='font-size:18pt;'><hr>");
                body.AppendLine("</body></html>");


                List<string> toEmails = AppSettings.NotificationEmail("SignImageUpdate", customerId.ToString());

                foreach (string toEmail in toEmails)
                {
                    Common.Mail.SendSimpleMailWithEmbededImage("noreply@mermaid.dk", toEmail, "Destination images creates/updated", body.ToString(), attachments);
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI_SERVER, ex);
            }
        }

        private static void HandleSignDataChange(int customerId)
        {
            try
            {
                IBI.Shared.Common.Operations.UpdateDataStatus(IBI.Shared.Common.Types.APIDataKeys.SIGNDATA, customerId);
                string xmlSignList = GenerateSignsData(customerId, string.Empty, string.Empty, string.Empty);
                FileInfo fInfo = new FileInfo(Path.Combine(System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"], "Cache\\SignList_" + customerId + ".xml"));
                if (!fInfo.Directory.Exists)
                {
                    fInfo.Directory.Create();
                }
                File.Delete(fInfo.FullName);
                File.WriteAllText(fInfo.FullName, xmlSignList);
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
        }

        private static string GetValidPreviewXmlData(string imagexml)
        {
            string dataXml = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(imagexml);
                dataXml = doc.OuterXml;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return dataXml;
        }

        private static string GetLEDPreviewXmlData(string imagexml)
        {
            string dataXml = string.Empty;
            try
            {
                var doc = XDocument.Parse(imagexml);
                var targetNodes = doc.Descendants("image");

                if (targetNodes == null)
                {
                    return "";
                }

                foreach (var targetNode in targetNodes)
                {
                    Byte[] bytes = AppUtility.ConvertBase64toLEDImage(targetNode.Attribute("base64").Value);
                    string ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute = new XAttribute("base64View", ledbase64);
                    targetNode.Add(attribute);

                    bytes = AppUtility.ConvertBase64toLEDImageLarge(targetNode.Attribute("base64").Value);
                    ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute1 = new XAttribute("base64ViewLarge", ledbase64);
                    targetNode.Add(attribute1);

                }
                return string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", doc.ToString());
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return dataXml;
        }

        private static string GetSignDataPreviewXml(SignItem signItem, IBI.DataAccess.DBModels.SignLayout layout, SignGraphicsRule graphicRule, out string url)
        {
            string serverPath = string.IsNullOrEmpty(AppSettings.GetImageBuilderServerUrl()) ? "http://krusty.mermaid.dk/imagegen/buildimage.php?" : AppSettings.GetImageBuilderServerUrl();
            url = serverPath + "output=xml&width=" + layout.Width + "&height=" + layout.Height;
            string mainText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.MainText : "";
            string subText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.SubText : "";

            url += "&linearea=" + graphicRule.LineArea + "&linedivider=" + graphicRule.LineDivider;
            url += "&anidelay=" + graphicRule.AniDelay;
            url += "&center=" + graphicRule.Center;
            url += "&downscale=" + graphicRule.DownScale;
            url += "&upscale=" + graphicRule.UpScale;
            url += "&showerrors=" + graphicRule.ShowErrors;
            url += "&line=" + signItem.Line + "&linefont=" + graphicRule.LineFont;
            url += "&linefontsize=" + graphicRule.LineFontSize + "&linex=" + graphicRule.LineX + "&liney=" + graphicRule.LineY + "&main=" + mainText + "&mainfont=" + graphicRule.MainFont + "&mainfontsize=" + graphicRule.MainFontSize + "&mainx=" + graphicRule.MainStartX + "&mainy=" + graphicRule.MainStartY + "&sub=" + subText + "&subfont=" + graphicRule.SubFont + "&subfontsize=" + graphicRule.SubFontSize + "&subx=" + graphicRule.SubStartX + "&suby=" + graphicRule.SubStartY + "&showgrid=" + graphicRule.ShowGrid + "&allcaps=" + graphicRule.AllCaps;
            url += "&template=" + graphicRule.template;
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBIData, url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse urlResponse = (HttpWebResponse)request.GetResponse();
            if (urlResponse.StatusCode != HttpStatusCode.OK || urlResponse.ContentLength <= 0)
            {
                string response = "error: response code: " + urlResponse.StatusCode.ToString();
                response += url;
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, response);
                return string.Empty;
            }

            TextReader reader = new StreamReader(urlResponse.GetResponseStream(), Encoding.Default);
            return reader.ReadToEnd();
        }



        private static string GetSignDataPreviewXml(SignItem signItem, IBI.DataAccess.DBModels.SignLayout layout, IBI.Shared.Models.Signs.SignGraphicRule graphicRule, out string url)
        {
            string serverPath = string.IsNullOrEmpty(AppSettings.GetImageBuilderServerUrl()) ? "http://krusty.mermaid.dk/imagegen/buildimage.php?" : AppSettings.GetImageBuilderServerUrl();
            url = serverPath + "output=xml&width=" + layout.Width + "&height=" + layout.Height;
            string mainText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.MainText : "";
            string subText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.SubText : "";

            url += "&linearea=" + graphicRule.LineArea + "&linedivider=" + graphicRule.LineDivider;
            url += "&anidelay=" + graphicRule.AniDelay;
            url += "&center=" + graphicRule.Center;
            url += "&downscale=" + graphicRule.DownScale;
            url += "&upscale=" + graphicRule.UpScale;
            url += "&showerrors=" + graphicRule.ShowErrors;
            url += "&line=" + signItem.Line + "&linefont=" + graphicRule.LineFont;
            url += "&linefontsize=" + graphicRule.LineFontSize + "&linex=" + graphicRule.LineX + "&liney=" + graphicRule.LineY + "&main=" + mainText + "&mainfont=" + graphicRule.MainFont + "&mainfontsize=" + graphicRule.MainFontSize + "&mainx=" + graphicRule.MainStartX + "&mainy=" + graphicRule.MainStartY + "&sub=" + subText + "&subfont=" + graphicRule.SubFont + "&subfontsize=" + graphicRule.SubFontSize + "&subx=" + graphicRule.SubStartX + "&suby=" + graphicRule.SubStartY + "&showgrid=" + graphicRule.ShowGrid + "&allcaps=" + graphicRule.AllCaps;
            url += "&template=" + graphicRule.template;
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBIData, url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse urlResponse = (HttpWebResponse)request.GetResponse();
            if (urlResponse.StatusCode != HttpStatusCode.OK || urlResponse.ContentLength <= 0)
            {
                string response = "error: response code: " + urlResponse.StatusCode.ToString();
                response += url;
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, response);
                return string.Empty;
            }

            TextReader reader = new StreamReader(urlResponse.GetResponseStream(), Encoding.Default);
            return reader.ReadToEnd();
        }

        public static string SyncDestinationContentImages(string signItems, string signGroups, string userName)
        {
            string response = string.Empty;
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignItem[] signs = null;
                int customerId = 0;
                if (!string.IsNullOrEmpty(signItems) || !string.IsNullOrEmpty(signGroups))
                {
                    int[] items = signItems.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToArray();
                    int[] groups = signGroups.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToArray();

                    signs = (from x in dbContext.SignItems
                             where items.Contains(x.SignId)
                             select x).ToArray<SignItem>();

                    if (signs != null && signs.Length > 0)
                    {
                        customerId = signs[0].CustomerId;
                        SyncDestinationContentImages(customerId, userName, dbContext, signs);
                        dbContext.SaveChanges();
                        HandleDestinationListChange(customerId);
                        HandleSignDataChange(customerId);
                        response = "<status>success</status><notification>Check system log for errors if any</notification>";
                    }


                    //set order for new Items.
                    SetOrderForNewSignItems(items, groups);

                }
            }

            return response;
        }



        public static string SyncDestinationImages(int customerId, SignItem signItem, string userName)
        {
            string response = string.Empty;
            // SyncDestinationContent(customerId);
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignItem[] signs = null;
                if (signItem != null)
                {
                    signs = new SignItem[] { signItem };
                }
                else
                {
                    signs = (from x in dbContext.SignItems
                             where x.CustomerId == customerId
                             select x).ToArray<SignItem>();

                }

                SyncDestinationContentImages(customerId, userName, dbContext, signs);

                dbContext.SaveChanges();

                HandleSignDataChange(customerId);

                response = "<status>success</status><notification>Check system log for errors if any</notification>";
            }
            return response;
        }

        private static void SyncDestinationContentImages(int customerId, string userName, IBIDataModel dbContext, SignItem[] signs)
        {
            foreach (SignItem sign in signs)
            {
                //add Activity log for this signItem, bcoz it's always a new SignItem created from IBI SERVER
                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_ITEM, ActivityLogger.Action.CREATE, "", null, sign);

                SignLayout[] layouts = (from y in dbContext.SignLayouts
                                        where y.CustomerId == customerId
                                        select y).ToArray<SignLayout>();
                foreach (SignLayout layout in layouts)
                {

                    SignGraphicsRule graphicRule = (from r in dbContext.SignGraphicsRules
                                                    where r.GraphicsRuleId == layout.GraphicsRuleId
                                                    select r).FirstOrDefault();
                    if (graphicRule != null)
                    {
                        SyncSignData(dbContext, sign, layout, graphicRule, userName);
                    }

                }
            }
            //update dataUpdateStatus
            //HandleSignDataChange(customerId);
        }


        #endregion


        #region Sign Addons

        public static string GetSignAddonsData(int customerId, string signText, string udpText)
        {
            FileInfo fInfo = new FileInfo(Path.Combine(System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"], "Cache\\SignAddonList_" + customerId + ".xml"));
            if (fInfo.Exists)
            {
                return File.ReadAllText(fInfo.FullName);
            }

            return GenerateSignAddonsData(customerId, signText, udpText);
        }

        private static string GenerateSignAddonsData(int customerId, string signText, string udpText)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable layOutTable = new Hashtable();
            Hashtable signAddonDataTable = new Hashtable();
            ArrayList layoutIdList = new ArrayList();
            // debug
            string logsData = string.Empty;
            DateTime before = DateTime.Now;
            DateTime after = DateTime.Now;

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                List<SignAddonItem> contents = dbContext.SignAddonItems.Where(m => m.CustomerId == customerId && (string.IsNullOrEmpty(signText) || m.SignText == signText) && (string.IsNullOrEmpty(udpText))).ToList();
                logsData += "Total records " + contents.Count;
                sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                sb.AppendLine("<signaddons>");

                //var List<SignLayout> customerLaouts

                foreach (SignAddonItem signAddon in contents)
                {

                    var signsDataElements = (from x in dbContext.SignAddonDatas
                                             join l in dbContext.SignLayouts on x.LayoutId equals l.LayoutId
                                             where x.CustomerId == customerId && l.CustomerId == customerId && x.SignAddonItemId == signAddon.SignAddonItemId
                                             select x);
                    string contentData = "";
                    int numPages = 0;

                    SignLayout layoutInfo = null;// = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();


                    foreach (var element in signsDataElements)
                    {
                        SignLayout tmpLayout = dbContext.SignLayouts.Where(m => m.LayoutId == element.LayoutId).FirstOrDefault();
                        if (layoutInfo != null)
                        {
                            if (layoutInfo.LayoutId != tmpLayout.LayoutId)
                            {
                                if (!layOutTable.ContainsKey(layoutInfo.LayoutId))
                                {
                                    layOutTable[layoutInfo.LayoutId] = layoutInfo;
                                    layoutIdList.Add(layoutInfo.LayoutId);
                                }
                                if (signAddonDataTable.ContainsKey(layoutInfo.LayoutId))
                                {
                                    string tmpdata = (string)signAddonDataTable[layoutInfo.LayoutId];
                                    tmpdata += contentData;
                                    signAddonDataTable[layoutInfo.LayoutId] = tmpdata;
                                }
                                else
                                {
                                    signAddonDataTable[layoutInfo.LayoutId] = contentData;
                                }
                                numPages = 0;
                                contentData = "";
                            }
                        }

                        contentData += "<signaddondata  signtext='" + HttpContext.Current.Server.HtmlEncode(signAddon.SignText) + "' md5='" + element.MD5 + "' signaddondataid='" + element.SqlId + "' signaddonitemid='" + signAddon.SignAddonItemId + "'></signaddondata>";
                        numPages++;
                        layoutInfo = tmpLayout;
                    }
                    if (numPages > 0)
                    {
                        if (!layOutTable.ContainsKey(layoutInfo.LayoutId))
                        {
                            layOutTable[layoutInfo.LayoutId] = layoutInfo;
                            layoutIdList.Add(layoutInfo.LayoutId);
                        }
                        if (signAddonDataTable.ContainsKey(layoutInfo.LayoutId))
                        {
                            string tmpdata = (string)signAddonDataTable[layoutInfo.LayoutId];
                            tmpdata += contentData;
                            signAddonDataTable[layoutInfo.LayoutId] = tmpdata;
                        }
                        else
                        {
                            signAddonDataTable[layoutInfo.LayoutId] = contentData;
                        }

                        numPages = 0;
                        contentData = "";
                    }
                }
                after = DateTime.Now;
                logsData += "Fetching from DB and making list took " + (after - before).TotalSeconds;
                before = DateTime.Now;
                foreach (int layoutid in layoutIdList)
                {
                    SignLayout tmpLayout = (SignLayout)layOutTable[layoutid];
                    sb.AppendLine("<layout layoutid='" + tmpLayout.LayoutId + "' type='" + tmpLayout.Type + "' technology='" + tmpLayout.Technology + "' manufacturer='" + tmpLayout.Manufacturer + "' height='" + tmpLayout.Height + "' width='" + tmpLayout.Width + "'>");
                    sb.AppendLine((string)signAddonDataTable[layoutid]);
                    sb.AppendLine("</layout>");
                }
                after = DateTime.Now;
                logsData += "result compisition took " + (after - before).TotalSeconds;
            }
            sb.AppendLine("</signaddons>");
            return sb.ToString();

        }

        public static List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignAddonsXmlData(int signAddonItemId)
        {
            List<IBI.Shared.Models.Signs.SignXmlLayout> layouts = new List<IBI.Shared.Models.Signs.SignXmlLayout>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignAddonItem signAddonItem = dbContext.SignAddonItems.Where(m => m.SignAddonItemId == signAddonItemId).FirstOrDefault();
                if (signAddonItem != null)
                {
                    SignXmlData tmpImage = null;
                    var tmpLayouts = dbContext.SignLayouts.Where(sl => sl.CustomerId == signAddonItem.CustomerId).ToList();
                    foreach (var tmpLayout in tmpLayouts)
                    {
                        var element = signAddonItem.SignAddonDatas.Where(sd => sd.CustomerId == signAddonItem.CustomerId && sd.LayoutId == tmpLayout.LayoutId && sd.SignAddonItemId == signAddonItem.SignAddonItemId).FirstOrDefault();
                        SignXmlLayout tmpNewLayoutInfo = new SignXmlLayout(tmpLayout, element == null ? tmpLayout.GraphicsRuleId : element.GraphicRuleId);

                        tmpImage = new SignXmlData();
                        tmpImage.ContentId = signAddonItem.SignAddonItemId.ToString();
                        tmpImage.SignId = element == null ? signAddonItem.SignAddonItemId.ToString() : element.SqlId.ToString();
                        tmpImage.Page = element == null ? string.Empty : element.Page.ToString();
                        tmpImage.Base64 = element == null ? string.Empty : element.Picture;
                        tmpImage.MD5 = element == null ? string.Empty : element.MD5;
                        tmpNewLayoutInfo.Signs = new SignXmlData[] { tmpImage };
                        layouts.Add(tmpNewLayoutInfo);
                    }
                }
            }
            return layouts;
        }


        private static bool ShouldRefreshSignAddonImage(IBI.Shared.Models.SignAddons.SignAddon signAddon, DataAccess.DBModels.SignAddonItem signAddonItem)
        {
            if (string.Compare(signAddon.Name, signAddonItem.Name) != 0 ||
                string.Compare(signAddon.SignText, signAddonItem.SignText) != 0
                )
            {
                return true;
            }
            return false;
        }

        public static bool SaveSignAddon(IBI.Shared.Models.SignAddons.SignAddon signAddon, string userName)
        {
            try
            {
                bool updateImages = false;
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    DataAccess.DBModels.SignAddonItem signAddonItem = null;
                    DataAccess.DBModels.SignAddonItem oldSignAddonItem = null;
                    int? oldGroupId = null;

                    if (signAddon.SignAddonItemId > 0)
                    {
                        signAddonItem = dbContext.SignAddonItems.Single(s => s.SignAddonItemId == signAddon.SignAddonItemId); //GetSignItem(sign.SignId);

                        oldSignAddonItem = ObjectCopier.Clone<SignAddonItem>(signAddonItem);

                        //oldGroupId = oldSignAddonItem.GroupId;
                    }

                    if (signAddonItem == null)
                    {
                        signAddonItem = new DataAccess.DBModels.SignAddonItem();
                        signAddonItem.DateAdded = DateTime.Now;
                    }

                    updateImages = ShouldRefreshSignAddonImage(signAddon, signAddonItem);

                    signAddonItem.CustomerId = signAddon.CustomerId;
                    signAddonItem.DateModified = DateTime.Now;
                    signAddonItem.Excluded = !(signAddon.IsActive);

                    signAddonItem.SignText = signAddon.SignText;

                    signAddonItem.Name = signAddon.Name;

                    signAddonItem.SignAddonItemId = signAddon.SignAddonItemId;
                    signAddonItem.SortValue = signAddon.Priority;


                    //SORT ALGORITHM//
                    if (signAddon.SignAddonItemId <= 0)
                    {
                        var custSignAddons = dbContext.SignAddonItems.Where(s => s.CustomerId == signAddonItem.CustomerId).ToList();

                        int maxValue = 1;

                        int gMax = custSignAddons.Count() > 0 ? custSignAddons.Max(s => s.SortValue) : 0;

                        maxValue = gMax + 1;

                        signAddonItem.SortValue = maxValue == null ? 1 : maxValue;
                    }

                    bool isNew = signAddon.SignAddonItemId <= 0;

                    if (isNew)
                    {
                        dbContext.SignAddonItems.Add(signAddonItem);
                    }

                    dbContext.SaveChanges();


                    if (isNew)
                    {

                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_ADDON_MANAGEMENT, ActivityLogger.Entity.SIGN_ADDON_ITEM, ActivityLogger.Action.CREATE, "", null, signAddonItem);

                        //CreateDestination(signItem);
                        SyncSignAddonImages(signAddonItem.CustomerId, signAddonItem, userName);
                    }
                    else
                    {

                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_ADDON_MANAGEMENT, ActivityLogger.Entity.SIGN_ADDON_ITEM, ActivityLogger.Action.EDIT, "", oldSignAddonItem, signAddonItem);

                        if (updateImages)
                        {
                            SyncSignAddonData(signAddonItem.SignAddonItemId, userName);
                        }
                    }
                    HandleSignAddonListChange(signAddonItem.CustomerId);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }

        }

        private static void DeleteSignAddonData(int customerId, string mainText, string subText, string line)
        {
            //using (IBIDataModel dbContext = new IBIDataModel())
            //{
            //    SignContent content = dbContext.SignContents.Where(c => c.CustomerId == customerId && c.MainText == mainText && c.SubText == subText && c.Line == line).FirstOrDefault();
            //    if (content != null)
            //    {
            //        int contentId = content.SignId;
            //        dbContext.DeleteObject(content);
            //        SignData[] signElements = dbContext.SignDatas.Where(sd => sd.CustomerId == customerId && sd.SignId == contentId).ToArray<SignData>();
            //        if (signElements.Length > 0)
            //        {
            //            foreach (SignData element in signElements)
            //            {
            //                dbContext.DeleteObject(element);
            //            }
            //        }
            //        dbContext.SaveChanges();
            //    }
            //}
        }

        public static bool DeleteSignAddon(int signAddonItemId, string userName)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.DataAccess.DBModels.SignAddonItem signAddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).FirstOrDefault();

                    if (signAddonItem != null)
                    {
                        string item2Delete = signAddonItem.Name;
                        dbContext.SignAddonItems.Remove(signAddonItem);
                        dbContext.SaveChanges();

                        //update DataStatuses
                        HandleSignAddonDataChange(signAddonItem.CustomerId);
                        HandleSignAddonListChange(signAddonItem.CustomerId);
                        ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_ADDON_MANAGEMENT, ActivityLogger.Entity.SIGN_ADDON_ITEM, ActivityLogger.Action.DELETE, String.Format("SignAddonItemId: {0}, Name: {1}", signAddonItemId, item2Delete));
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                return false;
            }

        }

        private static IBI.Shared.Models.SignAddons.SignAddon SignAddonExists(List<IBI.Shared.Models.SignAddons.SignAddon> signAddons, IBI.Shared.Models.SignAddons.SignAddon item)
        {
            foreach (IBI.Shared.Models.SignAddons.SignAddon signAddon in signAddons)
            {
                if (signAddon.Equals(item))
                {
                    return signAddon;
                }
            }
            return null;
        }

        //private static void AddGroupToSigns(IBI.Shared.Models.Signs.Sign sign, SignGroup group, bool leafIncldued, bool showInactive, int customerId, string customerName)
        //{
        //    using (IBIDataModel dbContext = new IBIDataModel())
        //    {
        //        int? groupId = sign.GroupId;
        //        string groupName = (group == null) ? sign.Name : group.GroupName;
        //        List<IBI.Shared.Models.Signs.Sign> subSigns = new List<IBI.Shared.Models.Signs.Sign>();


        //        List<SignGroup> subGroups = null;
        //        if (string.Compare(groupName, "Special Destinations", true) == 0)
        //        {
        //            subGroups = dbContext.SignGroups.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && (x.ParentGroupId == null && x.GroupName.StartsWith("#") == false)).OrderBy(b => b.SortValue).ToList();
        //        }
        //        else
        //            if (string.Compare(groupName, "Destinations", true) == 0)
        //            {
        //                subGroups = dbContext.SignGroups.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && (x.ParentGroupId == null && x.GroupName.StartsWith("#") == true)).OrderBy(b => b.SortValue).ToList();
        //            }
        //            else
        //            {
        //                subGroups = dbContext.SignGroups.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && x.ParentGroupId == groupId).OrderBy(b => b.SortValue).ToList();
        //            }


        //        if (subGroups != null || subGroups.Count() > 0)
        //        {
        //            foreach (SignGroup tmpGroup in subGroups)
        //            {
        //                Sign tmpSign = new IBI.Shared.Models.Signs.Sign(tmpGroup.GroupId, 0, customerId, customerName, 0, tmpGroup.GroupName, "", "", "", true, groupId, "", tmpGroup.Excluded, tmpGroup.SortValue);


        //                subSigns.Add(tmpSign);
        //                AddGroupToSigns(tmpSign, tmpGroup, leafIncldued, showInactive, customerId, customerName);
        //            }

        //        }

        //        if (leafIncldued)
        //        {
        //            // add Items
        //            List<SignItem> subItems = dbContext.SignItems.Where(x => x.CustomerId == customerId && (showInactive == true || x.Excluded == false) && (x.GroupId == groupId || (groupId == 10000001 && x.GroupId == null))).OrderBy(b => b.SortValue).ToList();
        //            if (subItems != null && subItems.Count() > 0)
        //            {
        //                // add Items
        //                foreach (SignItem subItem in subItems)
        //                {
        //                    subSigns.Add(new IBI.Shared.Models.Signs.Sign(0, subItem.SignId, customerId, customerName, subItem.ScheduleId, subItem.Name, subItem.MainText, subItem.SubText, subItem.Line, false, groupId, groupName, subItem.Excluded, subItem.SortValue));
        //                }
        //            }
        //        }


        //        //subSigns = subSigns.OrderBy(m => m.Priority).ThenBy(b => b.Name).ToList();

        //        //if(!leafIncldued)
        //        //    subSigns = subSigns.OrderBy(s => s.Name, new ManualDestinationLineComparer()).ThenBy(s => s.Priority).ToList();
        //        //else
        //        subSigns = subSigns.OrderBy(m => m.Priority).ThenBy(b => b.Name).ToList();

        //        sign.children = subSigns;
        //    }
        //}

        public static SignAddonTree GetSignAddonTree(SignAddonTree tree, bool leafIncldued, bool showInactive, bool setAlphNumericGrouping)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    List<IBI.Shared.Models.SignAddons.SignAddon> treeSigns = new List<IBI.Shared.Models.SignAddons.SignAddon>();

                    // Add Groups
                    var customer = dbContext.Customers.FirstOrDefault(c => c.CustomerId == tree.CustomerID);
                    if (tree == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, new Exception("tree object is null"));
                    }
                    if (customer == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, new Exception("Customer is null: " + tree.CustomerID));
                    }

                    //SignAddon rootSpecial = new SignAddon(10000001, 0, tree.CustomerID, customer.FullName, 0, "SignAddons", "", "", "", true, 0, "", false, 0);
                    //treeSigns.Add(rootSpecial);


                    SignAddon rootSpecial = new SignAddon(10000001, 0, tree.CustomerID, customer.FullName, 0, "SignAddons", "", "", "", true, 0, "", false, 0);
                    rootSpecial.children = new List<SignAddon>();
                    treeSigns.Add(rootSpecial);
                    // root level groups
                    var signAddons = dbContext.SignAddonItems.Where(s => s.CustomerId == tree.CustomerID && (showInactive == true || s.Excluded == false)).OrderBy(s => s.SortValue);

                    foreach (SignAddonItem s in signAddons)
                    {
                        rootSpecial.children.Add(new IBI.Shared.Models.SignAddons.SignAddon(0, s.SignAddonItemId, tree.CustomerID, "", 0, s.Name, s.SignText, "", "", false, 0, "", s.Excluded, s.SortValue));
                    }


                    tree.SignAddons = new List<SignAddon>() { rootSpecial };
                    return tree;
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, ex);
                }
            }
            return null;
        }

        //internal static string SyncSignData(SignItem signItem)
        internal static string SyncSignAddonData(int signAddonItemId, string userName)
        {
            string response = string.Empty;

            if (signAddonItemId <= 0)
            {
                return "<status>SignAddonItem doesn't exist</status>";
            }

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignAddonItem signaddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).FirstOrDefault();


                    if (signaddonItem == null)
                        return "<status>SignAddonItem doesn't exist</status>";

                    SignAddonData[] signsDataElements = signaddonItem.SignAddonDatas.ToArray();

                    foreach (SignAddonData element in signsDataElements)
                    {

                        SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == element.LayoutId).FirstOrDefault();
                        SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == element.GraphicRuleId).FirstOrDefault();
                        if (rule == null)
                        {
                            rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == layout.GraphicsRuleId).FirstOrDefault();
                        }

                        if (signaddonItem != null && layout != null)
                        {
                            SyncSignAddonData(dbContext, signaddonItem, layout, rule, userName);
                            response = "<status>success</status>";
                        }
                        else
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SignAddonManagement, "The content and layout are not valid for this Sign with signdataid: " + element.SqlId + ", content: " + element.SignAddonItemId + ", layout: " + element.LayoutId);
                            response = "<status>error</status>";
                        }
                    }
                    dbContext.SaveChanges();
                    HandleSignAddonListChange(signaddonItem.CustomerId);
                    HandleSignAddonDataChange(signaddonItem.CustomerId);
                }
            }
            return response;
        }

        internal static SignAddon GetSignAddon(int signAddonItemId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signAddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).Select(s => new SignAddon
                    {
                        CustomerId = s.CustomerId,
                        DateAdded = s.DateAdded,
                        DateModified = s.DateModified,
                        IsActive = !(s.Excluded),
                        SignText = s.SignText,
                        Priority = s.SortValue,
                        SignAddonItemId = s.SignAddonItemId,
                        Name = s.Name
                        //UdpText = s.UdpText

                    }).FirstOrDefault();

                    return signAddonItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SignAddonManagement, ex);
                return null;
            }

        }

        internal static IBI.DataAccess.DBModels.SignAddonItem GetSignAddonItem(int signAddonItemId)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signAddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).FirstOrDefault();

                    return signAddonItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SignAddonManagement, ex);
                return null;
            }

        }


        internal static SignAddon FindSignAddon(int customerId, int? groupId, string line, string name, string signText, string udpText)
        {
            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signAddonItem = dbContext.SignAddonItems.Where(s =>

                        s.CustomerId == customerId &&
                        s.Name == name &&
                        s.SignText == signText
                        ).Select(s => new SignAddon
                        {
                            CustomerId = s.CustomerId,
                            DateAdded = s.DateAdded,
                            DateModified = s.DateModified,
                            IsActive = !(s.Excluded),
                            SignText = s.SignText,
                            Priority = s.SortValue,
                            SignAddonItemId = s.SignAddonItemId,
                            Name = s.Name

                        }).FirstOrDefault();

                    return signAddonItem;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SignAddonManagement, ex);
                return null;
            }
        }


        public static string SyncSignAddonImages(int customerId, SignAddonItem signAddonItem, string userName)
        {
            string response = string.Empty;
            // SyncDestinationContent(customerId);
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignAddonItem[] signAddons = null;
                if (signAddonItem != null)
                {
                    signAddons = new SignAddonItem[] { signAddonItem };
                }
                else
                {
                    signAddons = (from x in dbContext.SignAddonItems
                                  where x.CustomerId == customerId
                                  select x).ToArray<SignAddonItem>();

                }

                SyncSignAddonContentImages(customerId, userName, dbContext, signAddons);

                dbContext.SaveChanges();

                HandleSignAddonDataChange(customerId);

                response = "<status>success</status><notification>Check system log for errors if any</notification>";
            }
            return response;
        }


        private static void SyncSignAddonContentImages(int customerId, string userName, IBIDataModel dbContext, SignAddonItem[] signAddons)
        {
            foreach (SignAddonItem signAddon in signAddons)
            {
                //add Activity log for this signItem, bcoz it's always a new SignItem created from IBI SERVER
                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_ADDON_MANAGEMENT, ActivityLogger.Entity.SIGN_ADDON_ITEM, ActivityLogger.Action.CREATE, "", null, signAddon);

                SignLayout[] layouts = (from y in dbContext.SignLayouts
                                        where y.CustomerId == customerId
                                        select y).ToArray<SignLayout>();
                foreach (SignLayout layout in layouts)
                {

                    SignGraphicsRule graphicRule = (from r in dbContext.SignGraphicsRules
                                                    where r.GraphicsRuleId == layout.GraphicsRuleId
                                                    select r).FirstOrDefault();
                    if (graphicRule != null)
                    {
                        SyncSignAddonData(dbContext, signAddon, layout, graphicRule, userName);
                    }

                }
            }
            //update dataUpdateStatus
            //HandleSignDataChange(customerId);
        }

        private static bool SyncSignAddonData(IBIDataModel dbContext, SignAddonItem signAddonItem, IBI.DataAccess.DBModels.SignLayout layout, SignGraphicsRule graphicRule, string userName)
        {
            string url = string.Empty;
            string imagexml;
            string dataXml = string.Empty;
            string viewedDataXml = string.Empty;

            if (String.Compare(graphicRule.template, "MANUAL", true) == 0)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.SIGNMANAGEMENT, "The rule template is manual so not sending the call, ruleid: " + graphicRule.GraphicsRuleId);
            }
            else
            {
                imagexml = GetSignAddonDataPreviewXml(signAddonItem, layout, graphicRule, out url);


                if (layout.Type == "Number")
                {
                    imagexml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><signs><images count=\"0\"></images></signs>";
                }

                if (!string.IsNullOrEmpty(imagexml))
                {
                    dataXml = GetValidPreviewXmlData(imagexml);

                    viewedDataXml = GetLEDPreviewXmlData(dataXml);
                }
                else
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.SignAddonManagement, "The XML generated from image generator is null/empty");
                }
            }

            SignAddonData signAddonData = dbContext.SignAddonDatas.Where(m => m.SignAddonItemId == signAddonItem.SignAddonItemId && m.LayoutId == layout.LayoutId).FirstOrDefault();
            bool bNew = false;

            SignAddonData oldSignAddonData = null;

            if (signAddonData == null)
            {
                signAddonData = new SignAddonData();
                bNew = true;

            }
            else
            {
                oldSignAddonData = ObjectCopier.Clone<SignAddonData>(signAddonData);
            }
            signAddonData.SignAddonItemId = signAddonItem.SignAddonItemId;
            signAddonData.CustomerId = signAddonItem.CustomerId;
            signAddonData.LayoutId = layout.LayoutId;
            signAddonData.Page = 1;
            if (String.Compare(graphicRule.template, "MANUAL", true) != 0)
            {
                signAddonData.Picture = dataXml;
                signAddonData.ViewedPicture = viewedDataXml;
                if (!String.IsNullOrEmpty(dataXml))
                {
                    signAddonData.MD5 = AppUtility.ConvertStringtoMD5(dataXml);
                }
                else
                {
                    signAddonData.MD5 = "";
                }
            }
            signAddonData.GraphicRuleId = graphicRule.GraphicsRuleId;
            if (bNew)
            {
                dbContext.SignAddonDatas.Add(signAddonData);

                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_IMAGE, ActivityLogger.Action.CREATE, "", null, signAddonData);
            }
            else
            {
                ActivityLogger.AddActivityLog(userName, ActivityLogger.Module.SIGN_MANAGEMENT, ActivityLogger.Entity.SIGN_IMAGE, ActivityLogger.Action.EDIT, "", oldSignAddonData, signAddonData);
            }

            //update SignData TimeStamp
            //HandleSignAddonDataChange(signAddonItem.CustomerId);

            IBI.Shared.Models.Accounts.User usr = IBI.REST.Auth.UserAuthServiceController.GetUser(userName);
            string uName = usr == null ? userName : usr.FullName;

            string customerName = DBHelper.GetCustomerName(signAddonItem.CustomerId).ToString();

            SendSignAddonNotificationEmail(uName, signAddonItem.CustomerId, customerName, signAddonItem.SignText, layout, signAddonItem, url, dataXml);
            return true;
        }


        public static bool SaveSignAddonData(int signAddonItemId, int signRuleId, string userName)
        {
            return SaveSignAddonImageGeneratorData(0, signAddonItemId, signRuleId, userName);
        }

        public static bool SaveSignAddonImageGeneratorData(int layoutId, int signAddonItemId, int signRuleId, string userName)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignAddonItem signAddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).FirstOrDefault();
                    if (signAddonItem != null)
                    {
                        SignLayout[] layouts = (from y in dbContext.SignLayouts
                                                where y.CustomerId == signAddonItem.CustomerId && (y.LayoutId == layoutId || layoutId == 0)
                                                select y).ToArray<SignLayout>();

                        SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == signRuleId).FirstOrDefault();
                        foreach (SignLayout layout in layouts)
                        {
                            if (rule == null)
                            {
                                rule = (from r in dbContext.SignGraphicsRules
                                        where r.GraphicsRuleId == layout.GraphicsRuleId
                                        select r).FirstOrDefault();
                            }
                            if (rule != null)
                            {
                                SyncSignAddonData(dbContext, signAddonItem, layout, rule, userName);
                            }

                        }
                        dbContext.SaveChanges();
                        HandleSignAddonDataChange(signAddonItem.CustomerId);
                        return true;
                    }

                }
            }
            return false;
        }

        public static List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignAddonPreview(int signAddonItemId, int signRuleId, int layoutId, string userName)
        {
            List<IBI.Shared.Models.Signs.SignXmlLayout> layouts = new List<IBI.Shared.Models.Signs.SignXmlLayout>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                {
                    SignAddonItem signAddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).FirstOrDefault();
                    if (signAddonItem != null)
                    {
                        SignAddonData[] signAddonDataElements = signAddonItem.SignAddonDatas.Where(sd => sd.LayoutId == layoutId || layoutId == 0).ToArray();
                        SignXmlLayout newLayoutInfo = null;
                        List<SignXmlData> images = new List<SignXmlData>();
                        SignXmlData tmpImage = null;

                        foreach (SignAddonData element in signAddonDataElements)
                        {
                            SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == element.LayoutId).FirstOrDefault();
                            SignGraphicsRule rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == signRuleId).FirstOrDefault();
                            if (rule == null)
                            {
                                rule = dbContext.SignGraphicsRules.Where(r => r.GraphicsRuleId == layout.GraphicsRuleId).FirstOrDefault();
                            }

                            if (signAddonItem != null && layout != null)
                            {
                                SignXmlLayout tmpNewLayoutInfo = new SignXmlLayout(layout);
                                if (newLayoutInfo != null)
                                {
                                    if (newLayoutInfo.LayoutId != tmpNewLayoutInfo.LayoutId)
                                    {
                                        newLayoutInfo.Signs = images.ToArray();
                                        layouts.Add(newLayoutInfo);
                                        images = new List<SignXmlData>();
                                    }
                                }

                                string url = string.Empty;
                                string dataXml = string.Empty;
                                string ledDataXml = string.Empty;

                                if (String.Compare(rule.template, "MANUAL", true) != 0)
                                {
                                    dataXml = GetValidPreviewXmlData(GetSignAddonDataPreviewXml(signAddonItem, layout, rule, out url));
                                    dataXml = GetLEDPreviewXmlData(dataXml);
                                    tmpImage.MD5 = AppUtility.ConvertStringtoMD5(dataXml);
                                }
                                else
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.SIGNMANAGEMENT, "The preview is not generated because rule id: " + rule.GraphicsRuleId + " has template=MANUAL");
                                    dataXml = string.Empty;
                                    tmpImage.MD5 = string.Empty;
                                }
                                tmpImage = new SignXmlData();
                                tmpImage.ContentId = element.SignAddonItemId.ToString();
                                tmpImage.SignId = "0";
                                tmpImage.Page = "0";
                                tmpImage.Base64 = dataXml;
                                images.Add(tmpImage);

                                newLayoutInfo = new SignXmlLayout(layout);
                            }
                            if (newLayoutInfo != null)
                            {
                                newLayoutInfo.Signs = images.ToArray();
                                layouts.Add(newLayoutInfo);
                            }
                        }
                    }

                }
            }
            return layouts;

        }


        public static bool SaveSignAddonDataImages(int layoutId, SignXmlData signAddonData, string userName)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    int? signAddonId = int.Parse(signAddonData.SignId); //sign id here means SignAddonDataId
                    int customerid = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonId).Select(c => c.CustomerId).FirstOrDefault();
                    SignAddonData sData = dbContext.SignAddonDatas.Where(s => s.SignAddonItemId == signAddonId && s.CustomerId == customerid && s.LayoutId == layoutId).FirstOrDefault();
                    if (sData == null)
                    {
                        //singId = int.Parse(signData.ContentId);
                        sData = new SignAddonData();
                        sData.CustomerId = customerid; //dbContext.SignItems.Where(s => s.SignId == singId).Select(c => c.CustomerId).FirstOrDefault();
                        sData.Picture = signAddonData.Base64;
                        sData.Page = 1;
                        sData.SignAddonItemId = signAddonId;
                        sData.LayoutId = layoutId;
                        sData.MD5 = AppUtility.ConvertStringtoMD5(signAddonData.Base64);
                        sData.GraphicRuleId = dbContext.SignGraphicsRules.Where(r => r.template.ToLower() == "manual").Select(t => t.GraphicsRuleId).FirstOrDefault();

                        dbContext.SignAddonDatas.Add(sData);
                    }
                    else
                    {
                        sData.Picture = signAddonData.Base64;
                        sData.MD5 = AppUtility.ConvertStringtoMD5(signAddonData.Base64);
                        sData.ViewedPicture = signAddonData.Page;
                        sData.GraphicRuleId = dbContext.SignGraphicsRules.Where(r => r.template.ToLower() == "manual").Select(t => t.GraphicsRuleId).FirstOrDefault();

                    }
                    dbContext.SaveChanges();
                    HandleSignAddonDataChange(sData.CustomerId.Value);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private static string GetSignAddonDataPreviewXml(SignAddonItem signAddonItem, IBI.DataAccess.DBModels.SignLayout layout, SignGraphicsRule graphicRule, out string url)
        {
            string serverPath = string.IsNullOrEmpty(AppSettings.GetImageBuilderServerUrl()) ? "http://krusty.mermaid.dk/imagegen/buildimage.php?" : AppSettings.GetImageBuilderServerUrl();
            url = serverPath + "output=xml&width=" + layout.Width + "&height=" + layout.Height;
            string mainText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signAddonItem.SignText : "";
            //string subText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signAddonItem.SubText : "";

            url += "&linearea=" + graphicRule.LineArea + "&linedivider=" + graphicRule.LineDivider;
            url += "&anidelay=" + graphicRule.AniDelay;
            url += "&center=" + graphicRule.Center;
            url += "&downscale=" + graphicRule.DownScale;
            url += "&upscale=" + graphicRule.UpScale;
            url += "&showerrors=" + graphicRule.ShowErrors;
            //url += "&line=" + signAddonItem.Line + "&linefont=" + graphicRule.LineFont;
            url += "&linefontsize=" + graphicRule.LineFontSize + "&linex=" + graphicRule.LineX + "&liney=" + graphicRule.LineY + "&main=" + mainText + "&mainfont=" + graphicRule.MainFont + "&mainfontsize=" + graphicRule.MainFontSize + "&mainx=" + graphicRule.MainStartX + "&mainy=" + graphicRule.MainStartY + "&subfont=" + graphicRule.SubFont + "&subfontsize=" + graphicRule.SubFontSize + "&subx=" + graphicRule.SubStartX + "&suby=" + graphicRule.SubStartY + "&showgrid=" + graphicRule.ShowGrid + "&allcaps=" + graphicRule.AllCaps;
            url += "&template=" + graphicRule.template;
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBIData, url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse urlResponse = (HttpWebResponse)request.GetResponse();
            if (urlResponse.StatusCode != HttpStatusCode.OK || urlResponse.ContentLength <= 0)
            {
                string response = "error: response code: " + urlResponse.StatusCode.ToString();
                response += url;
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, response);
                return string.Empty;
            }

            TextReader reader = new StreamReader(urlResponse.GetResponseStream(), Encoding.Default);

            return reader.ReadToEnd();
        }

        private static string GetSignAddonDataPreviewXml(SignAddonItem signAddonItem, IBI.DataAccess.DBModels.SignLayout layout, IBI.Shared.Models.Signs.SignGraphicRule graphicRule, out string url)
        {
            string serverPath = string.IsNullOrEmpty(AppSettings.GetImageBuilderServerUrl()) ? "http://krusty.mermaid.dk/imagegen/buildimage.php?" : AppSettings.GetImageBuilderServerUrl();
            url = serverPath + "output=xml&width=" + layout.Width + "&height=" + layout.Height;
            string mainText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signAddonItem.SignText : "";
            //string subText = ((string.Compare(layout.Type, "Number", true) != 0)) ? signItem.SubText : "";

            url += "&linearea=" + graphicRule.LineArea + "&linedivider=" + graphicRule.LineDivider;
            url += "&anidelay=" + graphicRule.AniDelay;
            url += "&center=" + graphicRule.Center;
            url += "&downscale=" + graphicRule.DownScale;
            url += "&upscale=" + graphicRule.UpScale;
            url += "&showerrors=" + graphicRule.ShowErrors;
            // url += "&line=" + signItem.Line + "&linefont=" + graphicRule.LineFont;
            url += "&linefontsize=" + graphicRule.LineFontSize + "&linex=" + graphicRule.LineX + "&liney=" + graphicRule.LineY + "&main=" + mainText + "&mainfont=" + graphicRule.MainFont + "&mainfontsize=" + graphicRule.MainFontSize + "&mainx=" + graphicRule.MainStartX + "&mainy=" + graphicRule.MainStartY + "&sub=&subfont=" + graphicRule.SubFont + "&subfontsize=" + graphicRule.SubFontSize + "&subx=" + graphicRule.SubStartX + "&suby=" + graphicRule.SubStartY + "&showgrid=" + graphicRule.ShowGrid + "&allcaps=" + graphicRule.AllCaps;
            url += "&template=" + graphicRule.template;
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBIData, url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse urlResponse = (HttpWebResponse)request.GetResponse();
            if (urlResponse.StatusCode != HttpStatusCode.OK || urlResponse.ContentLength <= 0)
            {
                string response = "error: response code: " + urlResponse.StatusCode.ToString();
                response += url;
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, response);
                return string.Empty;
            }

            TextReader reader = new StreamReader(urlResponse.GetResponseStream(), Encoding.Default);
            return reader.ReadToEnd();
        }

        public static List<SignDataImageItem> GetSignAddonImages(int customerId, int layoutid, int signAddonItemId)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var signsAddonDataElements = (from x in dbContext.SignAddonDatas
                                                  where x.CustomerId == customerId && x.SignAddonItemId == signAddonItemId && x.LayoutId == layoutid
                                                  select new
                                                  {
                                                      signAddonDataId = x.SqlId,
                                                      Picture = x.Picture,
                                                      ViewedPicture = x.ViewedPicture
                                                  }
                                            ).FirstOrDefault();


                    if (signsAddonDataElements != null)
                    {

                        List<SignDataImageItem> imgItem = new List<SignDataImageItem>();
                        string strImage = (string.IsNullOrEmpty(signsAddonDataElements.ViewedPicture) ? signsAddonDataElements.Picture.ToString() : signsAddonDataElements.ViewedPicture.ToString());
                        XDocument xdoc = XDocument.Parse(strImage);
                        var base64Images = (from x in xdoc.Descendants("image")
                                            select new
                                            {
                                                base64 = x.Attribute("base64").Value,
                                                base64View = x.Attribute("base64View") == null ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(x.Attribute("base64").Value)) : x.Attribute("base64View").Value,
                                                base64ViewLarge = x.Attribute("base64ViewLarge") == null ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImageLarge(x.Attribute("base64").Value)) : x.Attribute("base64ViewLarge").Value
                                            }
                                            ).ToList();

                        foreach (var base64Image in base64Images)
                        {
                            SignDataImageItem signAddonDataImageItem = new SignDataImageItem();
                            signAddonDataImageItem.SignDataId = signsAddonDataElements.signAddonDataId;
                            signAddonDataImageItem.Base64String = base64Image.base64;
                            signAddonDataImageItem.base64ViewString = base64Image.base64View;
                            signAddonDataImageItem.base64ViewLargeString = base64Image.base64ViewLarge;
                            imgItem.Add(signAddonDataImageItem);
                        }


                        return imgItem;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public static List<string> GetSignAddonImagePreview(int signAddonItemId, SignGraphicRule signGraphicRule, int layoutId, string userName)
        {
            List<string> imagebase64 = null;
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                SignAddonItem signAddonItem = dbContext.SignAddonItems.Where(s => s.SignAddonItemId == signAddonItemId).FirstOrDefault();
                if (signAddonItem != null)
                {
                    SignLayout layout = dbContext.SignLayouts.Where(l => l.LayoutId == layoutId).FirstOrDefault();
                    if (signAddonItem != null && layout != null)
                    {
                        string url = string.Empty;
                        string retXML = GetSignAddonDataPreviewXml(signAddonItem, layout, signGraphicRule, out url);

                        XDocument xdoc = XDocument.Parse(retXML);
                        imagebase64 = (from img in xdoc.Descendants("image")
                                       select (img.Attribute("base64View") == null) ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(img.Attribute("base64").Value)) : img.Attribute("base64View").Value
                                       ).ToList();
                        return imagebase64;
                    }
                }
            }
            return imagebase64;
        }

        private static void HandleSignAddonDataChange(int customerId)
        {
            try
            {
                IBI.Shared.Common.Operations.UpdateDataStatus(IBI.Shared.Common.Types.APIDataKeys.SIGN_ADDON_LIST, customerId);
                string xmlSignList = GenerateSignAddonsData(customerId, string.Empty, string.Empty);
                FileInfo fInfo = new FileInfo(Path.Combine(System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"], "Cache\\SignAddonList_" + customerId + ".xml"));
                if (!fInfo.Directory.Exists)
                {
                    fInfo.Directory.Create();
                }
                File.Delete(fInfo.FullName);
                File.WriteAllText(fInfo.FullName, xmlSignList);
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
        }

        private static void HandleSignAddonListChange(int customerId)
        {
            IBI.Shared.Common.Operations.UpdateDataStatus(IBI.Shared.Common.Types.APIDataKeys.SIGN_ADDON_LIST, customerId);
        }


        private static void SendSignAddonNotificationEmail(string userName, int customerId, string customerName, string signText, IBI.DataAccess.DBModels.SignLayout layout, SignAddonItem signAddon, string url, string imageXml)
        {
            try
            {
                XDocument xdoc = XDocument.Parse(imageXml);
                var imagebase64 = (from img in xdoc.Descendants("image")
                                   select new
                                   {
                                       base64 = img.Attribute("base64").Value,
                                       base64View = (img.Attribute("base64View") == null) ? Convert.ToBase64String(AppUtility.ConvertBase64toLEDImage(img.Attribute("base64").Value)) : img.Attribute("base64View").Value
                                   }
                                ).ToList();

                string signLayout = layout.Type + " - " + layout.Width + " × " + layout.Height + "  " + layout.Manufacturer + "  -  " + layout.Technology;

                List<Attachment> attachments = new List<Attachment>();
                string sbOutput = "";
                if (imagebase64.Count > 0)
                {
                    int index = 0;
                    int i = 0;
                    foreach (var image in imagebase64)
                    {

                        byte[] byteBuffer = Convert.FromBase64String(image.base64);
                        MemoryStream memoryStream = new MemoryStream(byteBuffer);
                        Attachment blackWhiteAttachment = new Attachment(memoryStream, "blackwhite.png", "image/png");
                        blackWhiteAttachment.ContentId = "BlackWhiteImage" + i;
                        attachments.Add(blackWhiteAttachment);


                        byte[] imgBytes = Convert.FromBase64String(image.base64View);
                        MemoryStream LEDmemoryStream = new MemoryStream(imgBytes);
                        Attachment LEDAttachment = new Attachment(LEDmemoryStream, "LED.png", "image/png");
                        LEDAttachment.ContentId = "LEDImage" + i;
                        attachments.Add(LEDAttachment);

                        sbOutput += "<img  style='border:1px solid #021a40;' src=\"cid:BlackWhiteImage" + i + "\"><br/><br/><img src=\"cid:LEDImage" + i + "\"><br/><br/>";
                        index = (index + 1) % 3;
                        if (index == 0)
                        {
                            //sbOutput += "</div>";
                            sbOutput += "<br/>";
                        }
                        i++;
                    }
                    if (index != 0)
                    {
                        //sbOutput += "</div>";
                        sbOutput += "<br/>";
                    }

                }
                else
                {
                    //sbOutput += "<div  style='color:red;padding: 10px;font-weight: bold;'><b>There are no images available</b></div>";
                    sbOutput += "<br/><b>There are no images available</b>";
                }


                StringBuilder body = new StringBuilder();
                body.AppendLine("<html><body>");
                body.AppendLine("<font size='3' face='Helvetica'>A destination has been created/updated by '" + userName + "'</fond><br/>");


                body.AppendLine("<br><font size='3'><b>CustomerId</b> : " + customerId);
                body.AppendLine("<br><b>CustomerName</b> : " + customerName + "</font><br>");


                //body.AppendLine("<br><font size='3'><b>Line&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>: " + line);
                body.AppendLine("<br/><b>Sign Text</b>&nbsp;&nbsp; : " + signText);
                //body.AppendLine("<br/><b>SubText</b>&nbsp;&nbsp;&nbsp;&nbsp;: " + subText + "</font>");
                body.AppendLine("<br/><br/><span style='font-size:18pt;'><hr>");
                body.AppendLine("<br/><font size='3'>" + signLayout + "</font><br/><br/>");
                body.AppendLine(sbOutput);


                body.AppendLine("<span style='font-size:18pt;'><hr>");
                body.AppendLine("</body></html>");


                List<string> toEmails = AppSettings.NotificationEmail("SignAddonImageUpdate", customerId.ToString());

                foreach (string toEmail in toEmails)
                {
                    Common.Mail.SendSimpleMailWithEmbededImage("noreply@mermaid.dk", toEmail, "Sign Addon images creates/updated", body.ToString(), attachments);
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI_SERVER, ex);
            }
        }

        #endregion


        #region "Schedule Tree"


        public static ScheduleTree GetScheduleTree(ScheduleTree tree, bool leafIncldued, bool showInactive, bool setAlphNumericGrouping)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    List<IBI.Shared.Models.ScheduleTreeItems.Schedule> treeSchedules = new List<IBI.Shared.Models.ScheduleTreeItems.Schedule>();

                    // Add Groups
                    var customer = dbContext.Customers.FirstOrDefault(c => c.CustomerId == tree.CustomerID);
                    if (tree == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, new Exception("tree object is null"));
                    }
                    if (customer == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, new Exception("Customer is null: " + tree.CustomerID));
                    }

                    //SignAddon rootSpecial = new SignAddon(10000001, 0, tree.CustomerID, customer.FullName, 0, "SignAddons", "", "", "", true, 0, "", false, 0);
                    //treeSigns.Add(rootSpecial);


                    IBI.Shared.Models.ScheduleTreeItems.Schedule rootSpecial = new IBI.Shared.Models.ScheduleTreeItems.Schedule(0, tree.CustomerID, "0", "", "", "", DateTime.MinValue, null);
                    rootSpecial.children = new List<IBI.Shared.Models.ScheduleTreeItems.Schedule>();
                    treeSchedules.Add(rootSpecial);
                    // root level groups
                    var schedules = dbContext.Schedules.Where(s => s.CustomerId == tree.CustomerID).OrderBy(s => s.ScheduleId);

                    foreach (var s in schedules)
                    {
                        rootSpecial.children.Add(new IBI.Shared.Models.ScheduleTreeItems.Schedule(s.ScheduleId, tree.CustomerID, s.Line, s.FromName, s.Destination, s.ViaName, s.LastUpdated,
                            s.ScheduleStops.Select(stop => new IBI.Shared.Models.ScheduleTreeItems.ScheduleStop
                            {
                                StopId = stop.StopId,
                                StopName = stop.StopName,
                                StopSequence = stop.StopSequence,
                                Zone = stop.Zone,
                                Latitude = stop.StopGPS != null ? stop.StopGPS.Latitude.ToString() : "",
                                Longitude = stop.StopGPS != null ? stop.StopGPS.Longitude.ToString() : ""
                            }).ToList()));
                    }


                    tree.Schedules = new List<IBI.Shared.Models.ScheduleTreeItems.Schedule>() { rootSpecial };
                    return tree;
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SIGNMANAGEMENT, ex);
                }
            }
            return null;
        }

        public static IBI.Shared.Models.ScheduleTreeItems.Schedule GetSchedule(int scheduleId)
        {

            IBI.Shared.Models.ScheduleTreeItems.Schedule schedule = new IBI.Shared.Models.ScheduleTreeItems.Schedule();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    schedule = dbContext.Schedules.Where(s => s.ScheduleId == scheduleId).Select(s => new IBI.Shared.Models.ScheduleTreeItems.Schedule
                    {
                        ScheduleId = s.ScheduleId,
                        CustomerId = s.CustomerId,
                        Line = s.Line,
                        FromName = s.FromName,
                        Destination = s.Destination,
                        ViaName = s.ViaName,
                        LastUpdated = s.LastUpdated,
                        ScheduleStops = s.ScheduleStops.Select(ss => new IBI.Shared.Models.ScheduleTreeItems.ScheduleStop
                        {
                            Latitude = ss.StopGPS.Latitude.ToString(),
                            Longitude = ss.StopGPS.Longitude.ToString(),
                            StopId = ss.StopId,
                            StopName = ss.StopName,
                            StopSequence = ss.StopSequence,
                            Zone = ss.Zone
                        }).ToList()

                    }).FirstOrDefault();

                    return schedule;
                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, ex);
            }

            return schedule;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sch"></param>
        /// <returns>
        ///  > 0 : new schedule created
        ///  0 : error, no schedule saved
        /// -1 : schedule already exists - no changes made (may not occur)
        /// -2 : schedule already exists - with changes
        /// -3 : schedule already exists - no changes
        /// -4 : schedule already exists - updated successfully
        /// </returns>
        public static int SaveSchedule(IBI.Shared.Models.ScheduleTreeItems.Schedule sch, string forcedOption)
        {
            int scheduleId = GetScheduleNumber(sch.CustomerId, sch.Line, sch.FromName, sch.Destination, sch.ViaName, sch.ScheduleStops, true, forcedOption);
            return scheduleId;

        }

        //returns true if another schedule already exists with similar information
        public static bool CheckExistingSchedule(int scheduleId, int customerId, string line, string fromName, string destination, string viaName)
        {

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var anotherSchedule = dbContext.Schedules.Where(s =>
                        s.ScheduleId != scheduleId &&
                        s.CustomerId == customerId &&
                        s.Line == line &&
                        s.Destination == destination &&
                        s.FromName == fromName &&
                        (s.ViaName == viaName || (s.ViaName == null && string.IsNullOrEmpty(viaName)))
                        ).FirstOrDefault();

                    return (anotherSchedule != null) ? true : false;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, string.Format("Failed to Check Existing Schedule, Error: {0}", Logger.GetDetailedError(ex)));
            }

            return false;
        }

        public static int UpdateSchedule(IBI.Shared.Models.ScheduleTreeItems.Schedule sch, string forcedOption)
        {
            //forcedOption can either by "new" or "overwrite" or ""
            //if empty: dont save in case of duplicate error
            //if "new": create new schedule in case of duplicate
            //if "overwrite": update current schedule anyways
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    bool causesDuplication = false;
                    causesDuplication = CheckExistingSchedule(sch.ScheduleId, sch.CustomerId, sch.Line, sch.FromName, sch.Destination, sch.ViaName);
                    if (causesDuplication && string.IsNullOrEmpty(forcedOption))
                    {
                        return -1; //already exists;
                    }

                    var schedule = dbContext.Schedules.Where(s => s.ScheduleId == sch.ScheduleId).FirstOrDefault();


                    List<IBI.DataAccess.DBModels.ScheduleStop> stops = new List<DataAccess.DBModels.ScheduleStop>();
                    if (schedule != null)
                        {
                            foreach (var stop in schedule.ScheduleStops)
                            {
                                stops.Add(new DataAccess.DBModels.ScheduleStop
                                {
                                    StopGPS = stop.StopGPS,
                                    StopId = stop.StopId,
                                    StopName = stop.StopName,
                                    StopSequence = stop.StopSequence,
                                    Zone = stop.Zone
                                });
                                
                            }
                        }

                    if (forcedOption == "new")
                    {
                        schedule = new DataAccess.DBModels.Schedule();
                        schedule.CustomerId = sch.CustomerId;
                        schedule.Line = sch.Line;
                        schedule.FromName = sch.FromName;
                        schedule.Destination = sch.Destination;
                        schedule.ViaName = string.IsNullOrEmpty(sch.ViaName) ? null : sch.ViaName;
                        schedule.LastUpdated = DateTime.Now;
                        schedule.RouteDirectionId = GetRouteDirectionId(sch.CustomerId, sch.Line, sch.Destination, sch.ViaName, true);

                        schedule.ScheduleStops.Clear();
                        foreach (var stop in stops)
                        {
                            schedule.ScheduleStops.Add(stop);
                        } 

                        dbContext.Schedules.Add(schedule);
                        dbContext.SaveChanges();
                    }


                    if (schedule != null && (forcedOption == "overwrite" || string.IsNullOrEmpty(forcedOption)))
                    {
                        schedule.Line = sch.Line;
                        schedule.FromName = sch.FromName;
                        schedule.Destination = sch.Destination;
                        schedule.ViaName = string.IsNullOrEmpty(sch.ViaName) ? null : sch.ViaName;
                        schedule.LastUpdated = DateTime.Now;
                        schedule.RouteDirectionId = GetRouteDirectionId(sch.CustomerId, sch.Line, sch.Destination, sch.ViaName, true);
                        dbContext.SaveChanges();
                    }


                    return schedule.ScheduleId;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, string.Format("Failed to update a schedule Error: {0}", Logger.GetDetailedError(ex)));
            }


            return 0;

        }


        public static int UpdateScheduleStops(IBI.Shared.Models.ScheduleTreeItems.Schedule sch)
        {

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    //if (CheckExistingSchedule(sch.ScheduleId, sch.CustomerId, sch.Line, sch.FromName, sch.Destination, sch.ViaName))
                    //{
                    //    return -1; //already exists;
                    //}

                    var schedule = dbContext.Schedules.Where(s => s.ScheduleId == sch.ScheduleId).FirstOrDefault();


                    if (schedule != null)
                    {
                        //dont update schedule info ... only update stops
                        //schedule.Line = sch.Line;
                        //schedule.FromName = sch.FromName;
                        //schedule.Destination = sch.Destination;
                        //schedule.ViaName = string.IsNullOrEmpty(sch.ViaName) ? null : sch.ViaName;
                        //schedule.LastUpdated = DateTime.Now; 

                        schedule.ScheduleStops.Clear();

                        foreach (var s in sch.ScheduleStops)
                        {

                            DataAccess.DBModels.ScheduleStop sstop = new DataAccess.DBModels.ScheduleStop();
                            sstop.StopSequence = s.StopSequence;
                            sstop.StopId = s.StopId;
                            sstop.StopName = s.StopName;
                            sstop.Zone = s.Zone;

                            string sqlPoint = string.Format("POINT({0} {1})", s.Longitude, s.Latitude);

                            sstop.StopGPS = System.Data.Entity.Spatial.DbGeography.FromText(sqlPoint, 4326);


                            schedule.ScheduleStops.Add(sstop);
                        }

                        schedule.LastUpdated = DateTime.Now;

                        dbContext.SaveChanges();
                    }

                    return schedule.ScheduleId;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, string.Format("Failed to update a schedule Error: {0}", Logger.GetDetailedError(ex)));
            }


            return 0;

        }


        public static bool DeleteSchedule(int scheduleId)
        {

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var schedule = dbContext.Schedules.Where(s => s.ScheduleId == scheduleId).FirstOrDefault();

                    if (schedule != null)
                    {
                        dbContext.Schedules.Remove(schedule);

                        dbContext.SaveChanges();

                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, string.Format("Failed to delete a schedule. Error: {0}", Logger.GetDetailedError(ex)));
            }


            return false;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sch"></param>
        /// <returns>
        ///  > 0 : new schedule created
        ///  0 : error, no schedule saved
        /// -1 : schedule already exists - no changes made (may not occur)
        /// -2 : schedule already exists - with changes
        /// -3 : schedule already exists - no changes
        /// -4 : schedule already exists - updated successfully
        /// </returns>
        public static int GetScheduleNumber(int customerId, string line, string fromName, string destination, string viaName, ICollection<IBI.Shared.Models.ScheduleTreeItems.ScheduleStop> stops, bool generateScheduleInDB = true, string forcedOption = "")
        {
            //int scheduleId = 0;
            string scheduleStopSequence = string.Empty;
            string stopSequence = string.Empty;

            stopSequence = IBI.Shared.AppUtility.ConvertStringtoMD5(IBI.Shared.AppUtility.ListToSingleLineText(stops.OrderBy(s => s.StopSequence).Select(js => js.StopId.ToString()).ToList()));

            string key = customerId.ToString() + "|" + line + "|" + fromName + "|" + destination + "|" + viaName;

            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var schedules = dbContext.Schedules.Where(s => s.CustomerId == customerId &&
                                                    s.Line == line &&
                                                    s.FromName == fromName &&
                                                    s.Destination == destination &&
                                                    (s.ViaName == viaName || (s.ViaName == null && viaName == ""))
                                                    );

                    IBI.DataAccess.DBModels.Schedule schedule = schedules.OrderByDescending(s => s.ScheduleStops.Count()).FirstOrDefault();


                    if (schedule != null && forcedOption != "new")
                    {
                        if (schedule.ScheduleStops.Count() > 0)
                        {
                            scheduleStopSequence = IBI.Shared.AppUtility.ConvertStringtoMD5(IBI.Shared.AppUtility.ListToSingleLineText(schedule.ScheduleStops.Select(js => js.StopId.ToString()).ToList()));
                        }

                        if (scheduleStopSequence == stopSequence)
                        {
                            //no change detected , return existing scheduleid
                            //return schedule.ScheduleId;
                            if (forcedOption != "overwrite")
                            {
                                return -3;
                            }
                        }
                        else
                        {
                            if (forcedOption != "overwrite")
                            {
                                return -2;
                            }
                        }
                    }


                    if ((schedule == null && generateScheduleInDB) || forcedOption == "new")
                    {
                        //if (stops.Count()==0 && schedule != null)
                        //{
                        //    stops = schedule.ScheduleStops.Select(ss => new IBI.Shared.Models.ScheduleTreeItems.ScheduleStop
                        //    {
                        //        Latitude = ss.StopGPS != null ? ss.StopGPS.Latitude.ToString() : "1",
                        //        Longitude = ss.StopGPS != null ? ss.StopGPS.Longitude.ToString() : "1",
                        //        StopId = ss.StopId,
                        //        StopName = ss.StopName,
                        //        StopSequence = ss.StopSequence,
                        //        Zone = ss.Zone
                        //    }).ToList();
                        //}
                        schedule = new IBI.DataAccess.DBModels.Schedule
                        {
                            CustomerId = customerId,
                            Destination = destination,
                            FromName = fromName,
                            Line = line,
                            //ViaName = viaName,
                            ViaName = string.IsNullOrEmpty(viaName) ? null : viaName,
                            RouteDirectionId = GetRouteDirectionId(customerId, line, destination, viaName)
                        };

                        schedule.ScheduleStops = new List<IBI.DataAccess.DBModels.ScheduleStop>();
                    }

                    bool isNew = schedule.ScheduleId <= 0 || forcedOption == "new";

                    if (isNew || generateScheduleInDB)
                    {
                        schedule.ScheduleStops.Clear();

                        foreach (IBI.Shared.Models.ScheduleTreeItems.ScheduleStop stop in stops)
                        {
                            IBI.DataAccess.DBModels.ScheduleStop sStop = new IBI.DataAccess.DBModels.ScheduleStop();
                            string sqlPoint = string.Format("POINT({0} {1})", stop.Longitude, stop.Latitude);

                            sStop.StopGPS = System.Data.Entity.Spatial.DbGeography.FromText(sqlPoint, 4326);
                            sStop.StopId = stop.StopId;
                            sStop.StopName = stop.StopName;
                            sStop.StopSequence = stop.StopSequence;
                            sStop.Zone = stop.Zone;
                            //sStop.ScheduleId = sch.ScheduleId;

                            schedule.ScheduleStops.Add(sStop);
                        }

                        if (isNew)
                        {
                            dbContext.Schedules.Add(schedule);
                        }


                        schedule.LastUpdated = DateTime.Now;
                        dbContext.SaveChanges();

                        if (isNew)
                        {
                            return schedule.ScheduleId;
                        }
                        else
                        {
                            return -4;
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                //AppUtility.Log2File(GetJourneyLogFileName(customerId, subId), "Failed to get schedule for current journey. info: " + key + " [" + ex.Message + "]\n" + ex.StackTrace, true, true);
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.SCHEDULE_TREE, "Failed to get/generate schedule number. info: " + key + " [" + ex.Message + "]\n" + ex.StackTrace);
                return 0;
                //throw ex;
            }

            return 0;
        }


        //For route direction id from existing schedules
        private static int GetRouteDirectionIdFromExistingSchedules(int customerId, string line, string destination)
        {
            int routeDirectionId = 0;
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var schedules = dbContext.Schedules.Where(s => s.CustomerId == customerId &&
                                                    s.Line == line &&
                                                    s.Destination == destination
                                                    ).OrderBy(e=>e.ScheduleId);
                    if (schedules != null && schedules.Count() > 0)
                    {
                        var schedule = schedules.FirstOrDefault();
                        routeDirectionId = schedule.RouteDirectionId;
                        return routeDirectionId;
                    }

                }

            }
            catch (Exception ex)
            {
                //AppUtility.Log2File("Hacon_" + customerId.ToString() , "Failed to get RouteDirectionId from exisitng schedules for current journey [" + ex.Message + "]");
                AppUtility.Log2File("Customer_" + customerId.ToString(), "Failed to get RouteDirectionId from exisitng schedules for current journey [" + ex.Message + "]");
            }

            return routeDirectionId;
        }

        private static int GetRouteDirectionId(int customerId, string line, string destination, string viaName, bool generateRouteInDB = true)
        {
            int routeDirectionId = GetRouteDirectionIdFromExistingSchedules(customerId, line, destination);
            if (routeDirectionId > 0)
                return routeDirectionId;

            routeDirectionId = 0;//reset
            try
            {
                //string name = customerId.ToString() + "-" + line + "-" + destination + "-" + viaName;
                string name = customerId.ToString() + "-" + line + "-" + destination ;
                name = name.Trim('-');

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                   // var routes = dbContext.RouteDirections.Where(r => r.Name == name);

                    //foreach (var route in routes)
                    //{
                    //    routeDirectionId = route.RouteDirectionId;
                    //}

                    if (routeDirectionId == 0 && generateRouteInDB)
                    {
                        RouteDirection rd = new RouteDirection
                        {
                            Name = name

                        };

                        dbContext.RouteDirections.Add(rd);

                        dbContext.SaveChanges();

                        routeDirectionId = rd.RouteDirectionId;
                    }

                }

            }

            catch (Exception ex)
            {
                //do nothing
                //AppUtility.Log2File("Hacon_" + customerId.ToString() + "_Journeys_sub_" + subId.ToString(), "Failed to get RouteDirectionId for current journey [" + ex.Message + "]");
            }



            return routeDirectionId;
        }

        //original private static int GetRouteDirectionId(int customerId, string line, string destination, string viaName, bool generateRouteInDB = true)
        //{
        //    int routeDirectionId = 0;

        //    try
        //    {
        //        string name = customerId.ToString() + "-" + line + "-" + destination + "-" + viaName;
        //        name = name.Trim('-');

        //        routeDirectionId = 0; //reset
        //        using (IBIDataModel dbContext = new IBIDataModel())
        //        {
        //            var routes = dbContext.RouteDirections.Where(r => r.Name == name);

        //            foreach (var route in routes)
        //            {
        //                routeDirectionId = route.RouteDirectionId;
        //            }

        //            if (routeDirectionId == 0 && generateRouteInDB)
        //            {
        //                RouteDirection rd = new RouteDirection
        //                {
        //                    Name = name

        //                };

        //                dbContext.RouteDirections.Add(rd);

        //                dbContext.SaveChanges();

        //                routeDirectionId = rd.RouteDirectionId;
        //            }

        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        //do nothing
        //        //AppUtility.Log2File("Hacon_" + customerId.ToString() + "_Journeys_sub_" + subId.ToString(), "Failed to get RouteDirectionId for current journey [" + ex.Message + "]");
        //    }

          

        //    return routeDirectionId;
        //}

        #endregion

        #region Zone

        //new implementation of Zones

        public static IBI.Shared.Models.ZoneLookup GetZone(string lat, string lon)
        {
            return GetZone(2140, lat, lon);
        }

        public static string GetKML(string lat, string lon)
        {
            //double latitude = double.Parse(lat);
            //double longitude = double.Parse(lon);


            IBI.DataAccess.DBModels.Zone dbZone = GetDBZone(lat, lon);

            if (dbZone == null)
                throw new FileNotFoundException();


            StringBuilder xmlBuilder = new StringBuilder();


            xmlBuilder.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            xmlBuilder.AppendLine(dbZone == null ? "" : dbZone.KML);

            return xmlBuilder.ToString();
        }

        public static IBI.Shared.Models.ZoneLookup GetZone(int customerId, string lat, string lon)
        {
            //customerId never used in this current implementation.

            //double latitude;
            //double.TryParse(lat, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out latitude);
            //double longitude;
            //double.TryParse(lon, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out longitude);

            IBI.DataAccess.DBModels.Zone dbZone = GetDBZone(lat, lon);

            if (dbZone == null)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
                //return new ZoneLookup
                //{
                //    Comments = "",
                //    GPSCoordinate = lat + "," + lon,
                //    Zone = ""
                //};
            }

            return new ZoneLookup
            {
                Comments = "",
                GPSCoordinate = lat + "," + lon,
                Zone = dbZone.ZoneName,
                Operator = dbZone.Operator,
                OperatorName = dbZone.Description
            };
        }


        private static IBI.DataAccess.DBModels.Zone GetDBZone(string lat, string lon)
        {
            IBI.DataAccess.DBModels.Zone dbZone = null;

            using (IBIDataModel dbContext = new IBIDataModel())
            {

                dbZone = dbContext.FindGeoPointInZones(lat, lon).FirstOrDefault();

                if (dbZone == null || dbZone.Timestamp < DateTime.Now.AddDays(-7)) //zone not found in DB.
                {
                    //fetch form live service update in DB
                    dbZone = FetchZone(dbContext, lat, lon);
                }
            }

            return dbZone;
        }

        private static IBI.DataAccess.DBModels.Zone FetchZone(IBIDataModel dbContext, string lat, string lon)
        {
            //Logger.appendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.OIORest, "FetchZone() start: lat=" + lat.ToString() + "&lon=" + lon.ToString());

            string requestURL = "http://geo.oiorest.dk/takstzoner/" + lat + "," + lon;

            string zone = "";
            string operatorInfo = "";
            string zoneName = "";
            string zoneShapeXml = "";
            string polyCords = "";
            // List<string> shapes = new List<string>();

            try
            {
                String oiorestReply = IBI.Shared.WebUtility.GetWebResponse(requestURL);

                XmlDocument oiorestDocument = new XmlDocument();
                oiorestDocument.LoadXml(oiorestReply);
                zone = oiorestDocument.SelectSingleNode("takstzone/nr").InnerText;
                operatorInfo = oiorestDocument.SelectSingleNode("takstzone/operatør/nr").InnerText;
                zoneName = oiorestDocument.SelectSingleNode("takstzone/operatør/navn").InnerText;


                String kmlUrl = oiorestDocument.SelectSingleNode("takstzone/grænse").Attributes.GetNamedItem("href").Value;

                String kmlReply = IBI.Shared.WebUtility.GetWebResponse(kmlUrl);

                XmlDocument kmlDocument = new XmlDocument();
                kmlDocument.LoadXml(kmlReply);

                XmlNamespaceManager namespaceManager = new XmlNamespaceManager(kmlDocument.NameTable);
                namespaceManager.AddNamespace("geo", "http://earth.google.com/kml/2.2");

                zoneShapeXml = kmlDocument.SelectSingleNode("//geo:MultiGeometry", namespaceManager).OuterXml;
                //polyCords = kmlDocument.SelectSingleNode("//geo:MultiGeometry", namespaceManager).InnerText;
                XmlNodeList shapes = kmlDocument.SelectNodes("//geo:MultiGeometry/geo:Polygon/geo:outerBoundaryIs", namespaceManager);


                string multiPolyText = "";
                foreach (XmlNode shape in shapes)
                {
                    //shapes.Add(FixPolyCords(shape.InnerText));
                    multiPolyText += FixPolyCords(shape.SelectSingleNode("geo:LinearRing/geo:coordinates", namespaceManager).InnerText, shapes.Count > 1) + ",";
                }
                multiPolyText = multiPolyText.TrimEnd(',');

                //Ugly fix:
                zoneShapeXml = zoneShapeXml.Replace(" xmlns=\"http://earth.google.com/kml/2.2\"", "");




                var zones = dbContext.Zones.Where(z => z.ZoneName == zone && z.Operator == operatorInfo);

                IBI.DataAccess.DBModels.Zone dbZone = null;

                if (zones == null || zones.Count() <= 0)
                {
                    dbZone = new IBI.DataAccess.DBModels.Zone();
                    dbZone.Timestamp = DateTime.Now;
                }
                else
                    dbZone = zones.FirstOrDefault();

                //prepare KML text.
                StringBuilder kmlBuilder = new StringBuilder();
                //kmlBuilder.AppendLine("<kml xmlns=\"http://schemas.mermaid.dk/IBI/kml/2.2\">");
                kmlBuilder.AppendLine("<kml xmlns=\"http://earth.google.com/kml/2.2\">");

                if (!String.IsNullOrEmpty(zoneShapeXml))
                {
                    kmlBuilder.AppendLine("  <Placemark>");
                    kmlBuilder.AppendLine("     <Style id=\"yellowLineGreenPoly\">");
                    kmlBuilder.AppendLine("         <LineStyle>");
                    kmlBuilder.AppendLine("             <color>7f00ffff</color>");
                    kmlBuilder.AppendLine("             <width>4</width>");
                    kmlBuilder.AppendLine("         </LineStyle>");
                    kmlBuilder.AppendLine("         <PolyStyle>");
                    kmlBuilder.AppendLine("             <color>7f00ff00</color>");
                    kmlBuilder.AppendLine("         </PolyStyle>");
                    kmlBuilder.AppendLine("     </Style>");

                    kmlBuilder.AppendLine("    <name>Zone " + zone + "</name>");
                    kmlBuilder.AppendLine("    " + zoneShapeXml);
                    kmlBuilder.AppendLine("  </Placemark>");
                }
                kmlBuilder.AppendLine("  </kml>");

                dbZone.Timestamp = DateTime.Now;
                dbZone.ZoneName = zone;
                dbZone.Description = zoneName;
                dbZone.KML = kmlBuilder.ToString();
                dbZone.Operator = operatorInfo;
                //var geo = SqlGeography.Parse(String.Format("POINT({1} {0})", lat, lon));

                //string polyText = polyCords;
                //polyText = FixPolyCords(polyCords);


                System.Data.Entity.Spatial.DbGeography poly = dbContext.GetSqlPolygon(multiPolyText).First();

                //dbZone.GEO
                dbZone.GEO = poly;

                if (dbZone.ZoneId <= 0)
                    dbContext.Zones.Add(dbZone);

                dbContext.SaveChanges();

                return dbZone;

                //}

            }
            catch (Exception ex)
            {
                string clientIP = Shared.RestUtil.GetClientIP();
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.OIORest, String.Format("Invalid Zone info requested from {0}: Lat={1}&Lon={2}", clientIP, lat.ToString(), lon.ToString()));
                //return new Zone
                //{
                //    ZoneId=0,
                //    GEO =null,
                //    KML="",
                //    Timestamp=DateTime.Now,
                //    ZoneName="0",
                //    Operator=""
                //};
                //throw ex;
                return null;

            }
        }

        private static string FixPolyCords(string polyCords, bool isMultiPoly)
        {
            polyCords = polyCords.Replace(",", "^");
            polyCords = polyCords.Replace(" ", ",");
            polyCords = polyCords.Replace("^", " ");

            string[] arr = polyCords.Split(',');

            if (isMultiPoly == false)
                arr = arr.Reverse().ToArray();

            string polyText = "";
            foreach (string s in arr)
            {
                polyText += s + ",";
            }


            //if(isMultiPoly)
            polyText = "((" + polyText.Trim(',') + "))";
            //else
            //    polyText = "(" + polyText.Trim(',') + ")";

            return polyText;
        }


        #endregion

        #region Private Helper

        private static void SetOrderForNewSignItems(int[] signIDs, int[] groupIDs)
        {
            List<SortedSignItem> Items;

            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    Items = dbContext.SignGroups.Select(sg => new SortedSignItem
                    {
                        Id = sg.GroupId,
                        Name = sg.GroupName,
                        ParentGroupID = sg.ParentGroupId,
                        SortValue = sg.SortValue,
                        Type = "group",
                        Modified = false,
                        CustomerID = sg.CustomerId
                    }).ToList();

                    Items.AddRange(dbContext.SignItems.Select(si => new SortedSignItem
                    {
                        Id = si.SignId,
                        Name = si.Name,
                        ParentGroupID = si.GroupId,
                        SortValue = si.SortValue,
                        Type = "item",
                        Modified = false,
                        CustomerID = si.CustomerId
                    }).ToList());


                    List<SortedSignItem> items2Sort = Items.Where(i => signIDs.Contains(i.Id) && i.Type == "item").ToList();
                    List<SortedSignItem> groups2Sort = Items.Where(i => groupIDs.Contains(i.Id) && i.Type == "group").ToList();


                    foreach (SortedSignItem grp in groups2Sort)
                    {
                        if (Items.Contains(grp))
                            Items.Remove(grp);
                    }

                    foreach (SortedSignItem itm in items2Sort)
                    {
                        if (Items.Contains(itm))
                            Items.Remove(itm);
                    }

                    foreach (SortedSignItem grp in groups2Sort)
                    {
                        grp.SortValue = 0;
                        grp.Modified = true;
                        AddSortedItem(ref Items, grp);
                    }

                    foreach (SortedSignItem itm in items2Sort)
                    {
                        itm.SortValue = 0;
                        itm.Modified = true;
                        AddSortedItem(ref Items, itm);
                    }

                    var groups2Update = Items.Where(i => i.Modified == true && i.Type == "group").ToList();
                    var items2Update = Items.Where(i => i.Modified == true && i.Type == "item").ToList();

                    int[] listOfGroupIds = (groups2Update == null || groups2Update.Count() == 0) ? new int[] { } : groups2Update.Select(g => g.Id).ToArray();
                    int[] listOfSignIds = (items2Update == null || items2Update.Count() == 0) ? new int[] { } : items2Update.Select(g => g.Id).ToArray();

                    var groups = dbContext.SignGroups.Where(grp => listOfGroupIds.Contains(grp.GroupId));
                    var signs = dbContext.SignItems.Where(itm => listOfSignIds.Contains(itm.SignId));

                    if (groups != null)
                    {
                        foreach (var grp in groups)
                        {
                            grp.SortValue = groups2Update.FirstOrDefault(g => g.Id == grp.GroupId).SortValue;
                            grp.DateModified = DateTime.Now;
                        }
                    }

                    if (signs != null)
                    {
                        foreach (var sItem in signs)
                        {
                            sItem.SortValue = items2Update.FirstOrDefault(i => i.Id == sItem.SignId).SortValue;
                            sItem.DateModified = DateTime.Now;
                        }
                    }

                    dbContext.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, ex);
            }
        }


        private static void AddSortedItem(ref List<SortedSignItem> Items, SortedSignItem newItem)
        {
            // Items.Add(item, Items.Max(x=>x.Value) + 1);

            try
            {
                Common.SignSort signSort = new Common.SignSort();

                int groupInitials = 0;
                bool isAlphabetGroup = !(newItem.Name.StartsWith("#") && (int.TryParse(newItem.Name.Substring(1, 1), out groupInitials)));

                string rootGroup = "#" + groupInitials.ToString();

                List<SortedSignItem> staticList = Items.Where(x =>
                    x.CustomerID == newItem.CustomerID &&
                    x.ParentGroupID == newItem.ParentGroupID &&
                    x.Id != newItem.Id &&
                    ((isAlphabetGroup && !(x.Name.StartsWith("#"))) ||
                     x.Name.StartsWith(rootGroup))
                    ).OrderBy(x => x.SortValue).ThenBy(x => x.Name, signSort).ToList();

                //List<SortedSignItem> staticList = Items.Where(x => x.CustomerID == newItem.CustomerID && x.ParentGroupID == newItem.ParentGroupID && x.Id != newItem.Id).OrderBy(x => x.SortValue).ThenBy(x => x.Name, signSort).ToList();

                bool inserted = false;

                if (staticList == null || staticList.Count() == 0)
                {
                    newItem.SortValue = 1;
                    Items.Add(newItem);
                    inserted = true;
                    return;
                }


                int currentSortValue = 0;
                for (int i = 0; i < staticList.Count(); i++)
                {
                    bool isFirstItem = i == 0;
                    bool isLastItem = i == staticList.Count() - 1;

                    SortedSignItem item = staticList[i];
                    SortedSignItem nextItem = null;

                    //currentSortValue = isFirstItem ? 1 : item.SortValue;

                    if (!isLastItem)
                    {
                        nextItem = staticList[i + 1];
                    }

                    if (!inserted)
                    {
                        int comparer = signSort.Compare(newItem.Name, item.Name);

                        int comparerNext = !isLastItem ? signSort.Compare(newItem.Name, nextItem.Name) : -1;

                        //if ((comparer >= 0 && comparerNext <= 0) || 
                        //    (comparer==-1 && comparerNext==-1))
                        //{
                        //    currentSortValue = isFirstItem ? 1 : item.SortValue;

                        //    newItem.SortValue = currentSortValue;
                        //    Items.Add(newItem);
                        //    inserted = true;

                        //    //item.SortValue += 1;
                        //    item.SortValue = ++currentSortValue;
                        //    item.modified = true;

                        //}
                        if ((comparer > 0 && comparerNext < 0))
                        {   //insert After
                            item.Modified = true;
                            item.SortValue = ++currentSortValue;

                            newItem.SortValue = ++currentSortValue;
                            Items.Add(newItem);
                            inserted = true;


                        }


                        if (comparer <= 0 && !inserted)
                        {   //insert Before
                            newItem.SortValue = ++currentSortValue;
                            Items.Add(newItem);
                            inserted = true;

                            item.Modified = true;
                            item.SortValue = ++currentSortValue;
                        }

                        if (comparer > 0 && !inserted)
                        {
                            item.SortValue = ++currentSortValue;
                            item.Modified = true;
                            continue;
                        }


                        if (isLastItem && !inserted)
                        {
                            newItem.SortValue = ++currentSortValue;
                            Items.Add(newItem);
                            inserted = true;
                        }
                    }
                    else
                    {
                        item.SortValue = ++currentSortValue;
                        item.Modified = true;

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        public class SortedSignItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int SortValue { get; set; }
            public int? ParentGroupID { get; set; }
            public string Type { get; set; }
            public bool Modified { get; set; }
            public int CustomerID { get; set; }

        }

        #endregion


        internal static IBI.Shared.Models.UserScheduleModels.Schedule GetUserSchedule(int userId, int scheduleId)
        {
            int?[] customerIds = IBI.REST.BusTree.BusServiceController.GetUserCustomers(userId).Select(c => c.CustomerId).ToArray();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var schedule = dbContext.Schedules.Where(s => customerIds.Contains(s.CustomerId) && s.ScheduleId == scheduleId).FirstOrDefault();

                if (schedule != null)
                {
                    IBI.Shared.Models.UserScheduleModels.Schedule sch = new IBI.Shared.Models.UserScheduleModels.Schedule
                    {
                        Destination = schedule.Destination,
                        FromName = schedule.FromName,
                        Line = schedule.Line,
                        ScheduleID = schedule.ScheduleId,
                        Stops = schedule.ScheduleStops.Select(st => new IBI.Shared.Models.UserScheduleModels.ScheduleStop
                        {
                            Name = st.StopName,
                            Position = new IBI.Shared.Models.UserScheduleModels.Position
                            {
                                Lat = st.StopGPS.Latitude,
                                Long = st.StopGPS.Longitude
                            },
                            SequenceNumber = st.StopSequence,
                            StopNumber = st.StopId,
                            Zone = st.Zone
                        }).OrderByDescending(l => l.Position.Lat).ToList(),
                        Via = schedule.ViaName

                    };

                    return sch;
                }
            }

            return null;

        }
    }

}