﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Collections.Generic;
using IBI.REST.Shared;
using IBI.Shared;
using System.Linq;
using System.Xml.Linq;
using System.Data.Entity;
using System.Text.RegularExpressions;


namespace IBI.REST.Schedule
{
    public class ScheduleHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("ScheduleHandler"))
            {
                String scheduleXml = "";
                String scheduleID = context.Request["scheduleid"];

                if (String.IsNullOrEmpty(scheduleID))
                    throw new ArgumentException("ScheduleID is not specified", "scheduleid");

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        String script = "xml_Schedule";
                        //String script = "SELECT [Schedule]" + Environment.NewLine +
                        //                "FROM [Schedules]" + Environment.NewLine +
                        //                "WHERE [ScheduleID]=" + scheduleID;

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            command.CommandType = System.Data.CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("ScheduleId", scheduleID);


                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                XmlReader reader = command.ExecuteXmlReader();

                                bool bHasData = reader.Read();
                                while (reader.ReadState != System.Xml.ReadState.EndOfFile)
                                {
                                    scheduleXml = reader.ReadOuterXml();
                                }

                                reader.Close();

                                if (!bHasData)
                                {
                                    throw new FileNotFoundException();
                                }
                            }

                        }
                    }
                }

                scheduleXml = IBI.Shared.AppUtility.FixScheduleStopAddonsXML(scheduleXml); // scheduleStopAddons must be set before scheduleAddons
                scheduleXml = IBI.Shared.AppUtility.FixScheduleAddonsXML(scheduleXml, "Schedule");

                Byte[] fileContent = Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"utf-8\"?>" + scheduleXml);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
