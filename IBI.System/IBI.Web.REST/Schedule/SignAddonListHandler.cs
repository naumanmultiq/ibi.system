﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Collections.Generic;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Schedule
{
    public class SignAddonListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("SignAddOnListHandler"))
            {
                String customerId = context.Request["customerid"];
                String signtext = context.Request["signtext"];
                String udptext = context.Request["udptext"];

                if (String.IsNullOrEmpty(customerId))
                    throw new ArgumentException("customerId is not specified", "customerId");

                string key = string.Concat(customerId, signtext, udptext);
                string signXml = string.Empty;
                object customerIDCache = context.Cache[key + "signaddonlistexpiry"];
                if (customerIDCache != null && ((DateTime)customerIDCache).AddMinutes(AppSettings.SignAddOnListCache) > DateTime.Now)
                {
                    signXml = (String)context.Cache[key + "signaddonlistcontent"];
                }
                else
                {
                    signXml = ScheduleServiceController.GetSignAddonsData(int.Parse(customerId), signtext, udptext);
                    context.Cache[key + "signaddonlistexpiry"] = DateTime.Now;
                    context.Cache[key + "signaddonlistcontent"] = signXml;
                }
                Byte[] fileContent = Encoding.UTF8.GetBytes(signXml);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
