﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared;
using IBI.DataAccess.DBModels;
using System.Xml.Linq;
using System.Data.Entity;
using System.Linq;
using IBI.Shared.Models;
using IBI.Shared.Models.Schedules;
using IBI.REST.Common;


namespace IBI.REST.Schedule
{
    public class DestinationSelectionListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }



        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("DestinationSelectionListHandler"))
            {
                String customerID = context.Request["customerid"];

                String content = string.Empty;
                if (String.IsNullOrEmpty(customerID))
                    customerID = "2140";

                object customerIDCache = context.Cache[customerID + "destinationselectionlistexpiry"];
                if (customerIDCache != null && ((DateTime)customerIDCache).AddMinutes(AppSettings.DestinationSelectionListCache) > DateTime.Now)
                {
                    content = (String)context.Cache[customerID + "destinationselectionlistcontent"];
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    //sb.Append(GetDestinationTree(int.Parse(customerID)));
                    sb.Append(GetSignTree(int.Parse(customerID)));
                    content = sb.ToString();
                    context.Cache[customerID + "destinationselectionlistcontent"] = content;
                    context.Cache[customerID + "destinationselectionlistexpiry"] = DateTime.Now;
                }
                Byte[] fileContent = Encoding.UTF8.GetBytes(content);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion


        #region Helper

        private static void AppendSigns(List<DestListModel> schedules, List<IBI.Shared.Models.Signs.Sign> signs, bool bSpecial)
        {
            if (signs == null || signs.Count() == 0)
            {
                return;
            }

            foreach (IBI.Shared.Models.Signs.Sign s in signs)
            {
                if (s.IsGroup)
                {
                    AppendSigns(schedules, s.children, bSpecial);
                }
                else
                {
                    DestListModel tmpSign = new DestListModel();
                    tmpSign.Line = s.Line;
                    tmpSign.Destination = s.MainText;
                    tmpSign.From = ""; // KHI - where to read from
                    tmpSign.Via = s.SubText;
                    tmpSign.StopCount = 0;  // KHI - where to read from
                    tmpSign.ScheduleId = s.ScheduleId; // KHI - where to read from
                    tmpSign.IsSpecial = bSpecial;
                    tmpSign.Priority = (s.Priority == null || s.Priority == 0) ? 99999 : s.Priority;
                    schedules.Add(tmpSign);
                }
            }
        }

        private static void AppendChildren(IBI.Shared.Models.Signs.Sign sign, ref StringBuilder xmlData, int level, bool special)
        {
            try
            {
                level++;
                foreach (IBI.Shared.Models.Signs.Sign s in sign.children)
                {
                    if (s.IsGroup)
                    {
                        StringBuilder data = new StringBuilder();
                        AppendChildren(s, ref data, level, special);
                        string groupTag = (special == false && level == 1) ? "<Line line=\"" + (s.Name.StartsWith("#") ? s.Name.Substring(1) : s.Name) + "\">" : "<Group name=\"" + (s.Name.StartsWith("#") ? s.Name.Substring(1) : s.Name) + "\">";
                        xmlData.Append(groupTag);
                        xmlData.Append(data);
                        string endTag = (special == false && level == 1) ? "</Line>" : "</Group>";
                        xmlData.Append(endTag);
                    }
                    else
                    {
                        string destText = string.Empty;
                        string innerText = string.Empty;
                        if (string.IsNullOrEmpty(s.SubText))
                        {
                            innerText = destText = s.MainText;
                        }
                        else
                        {
                            destText = s.MainText + "/" + s.SubText;
                            innerText = s.MainText + ", via " + s.SubText;
                        }

                        string scheduleLine = s.Line;
                        string scheduleDestination = destText;
                        string scheduleId = ((s.ScheduleId == null || s.ScheduleId <= 0) ? "" : s.ScheduleId.ToString());
                        string scheduleinnertext = innerText;
                        string dest = s.MainText;
                        string via = s.SubText;

                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            if (!string.IsNullOrEmpty(scheduleId))
                            {
                                int tmpScheduleId = int.Parse(scheduleId);
                                var schedule = dbContext.Schedules.Where(m => m.ScheduleId == tmpScheduleId).FirstOrDefault();
                                if (schedule != null)
                                {
                                    scheduleLine = schedule.Line;
                                    dest = String.IsNullOrEmpty(schedule.Destination) ? string.Empty : schedule.Destination;
                                    via = String.IsNullOrEmpty(schedule.ViaName) ? string.Empty : schedule.ViaName;

                                    if (String.IsNullOrEmpty(schedule.ViaName))
                                        scheduleDestination = schedule.Destination;
                                    else
                                        scheduleDestination = schedule.Destination + "/" + schedule.ViaName;
                                }
                            }
                        }
                        xmlData.Append("<Destination dest=\"" + dest + "\" via=\"" + via + "\" line=\"" + scheduleLine + "\" destination=\"" + scheduleDestination + "\" schedule=\"" + scheduleId + "\" signitemid=\"" + s.SignId + "\" linetext=\"" + s.Line + "\" maintext=\"" + s.MainText + "\" subtext=\"" + s.SubText + "\">" + s.Name + "</Destination>");
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static string GetSignTree(int customerId)
        {
            IBI.Shared.Models.Signs.SignTree signTree = new IBI.Shared.Models.Signs.SignTree();
            signTree.CustomerID = customerId;
            signTree = ScheduleServiceController.GetSignTree(signTree, true, false, false);
            StringBuilder sb = new StringBuilder();
            if (signTree != null)
            {
                sb.Append("<Data>");
                List<DestListModel> schedules = new List<DestListModel>();
                // Special Destinations
                IBI.Shared.Models.Signs.Sign specialSign = signTree.Signs[0];
                sb.Append("<SpecialDestinations>");
                StringBuilder sbdata = new StringBuilder();
                AppendChildren(specialSign, ref sbdata, 0, true);
                sb.Append(sbdata.ToString());
                sb.Append("</SpecialDestinations>");
                IBI.Shared.Models.Signs.Sign scheduledSign = signTree.Signs[1];
                sb.Append("<Destinations>");
                sbdata.Clear();
                AppendChildren(scheduledSign, ref sbdata, 0, false);
                sb.Append(sbdata.ToString());
                sb.Append("</Destinations>");
                sb.Append("</Data>");
            }

            return sb.ToString();
        }



        #endregion



    }






}
