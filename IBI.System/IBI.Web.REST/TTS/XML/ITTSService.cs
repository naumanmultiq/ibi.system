﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Shared.Models;

namespace IBI.REST.TTS.XML
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    [XmlSerializerFormat]
    public interface ITTSService
    {
        [OperationContract]
        [WebGet(
            UriTemplate = "TTSData?customerid={customerid}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        [AspNetCacheProfile("TTSDataCache")]
        //[WebInvoke(Method = "GET", UriTemplate = "TTSData?customerid={customerid}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        TTSData GetTTSData(String customerid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "TTSDataFromDate?fromdate={fromdate}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        TTSData GetTTSDataFromDate(String fromDate);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "UnfilteredTTSData", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        TTSData GetUnfilteredTTSData();
    }
}
