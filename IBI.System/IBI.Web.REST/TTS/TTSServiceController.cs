﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using Acapela.Vaas;
using IBI.DataAccess.DBModels;
using IBI.Shared;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models;
using mermaid.BaseObjects.IO;
using System.Data.SqlClient;
using System.Xml;
using IBI.DataAccess;

namespace IBI.REST.TTS
{
    public class TTSServiceController
    {
        #region Properties

        private static Mermaid.TTS.Storage TTSStorage
        {
            get
            {
                return new Mermaid.TTS.Storage(AppSettings.GetTTSDatabaseConnectionString());
            }
        }

        private static Mermaid.TTS.ITTSEngine TTSEngine
        {
            get
            {
                // return new Acapela.Vaas.AcapelaEngine("Mermaid", "zlp0ngk49c", "http://vaas.acapela-group.com/Services/", "Mermaid_APP", Voice);
                return null;
            }
        }

        private static String Voice
        {
            get
            {
                return "mette22k";
            }
        }

        #endregion

        public static TTSData GetTTSData(String customerId)
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.GetTTSData"))
            {
                String voice = ConfigurationManager.AppSettings["AudioFileDefaultVoice"];
                List<AudioFile> fileList = new List<AudioFile>();
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var resultList = dbContext.TTSData(String.IsNullOrEmpty(customerId) ? 0 : Int32.Parse(customerId)).ToList();
                    foreach (TTSData_Result audiofileEntry in resultList)
                    {
                        AudioFile audioFile = new AudioFile();
                        audioFile.File = audiofileEntry.FileName;
                        audioFile.Version = (int)audiofileEntry.Version;
                        audioFile.Hash = audiofileEntry.FileHash;
                        //audioFile.Stop = audiofileEntry.Text.ToLower();

                        bool approved = audiofileEntry.Approved ?? false;
                        bool rejected = audiofileEntry.Rejected ?? false;

                        audioFile.Approved = approved ? 1 : 0;
                        audioFile.Rejected = rejected ? 1 : 0;

                        fileList.Add(audioFile);
                    }
                    TTSData ttsData = new TTSData();
                    ttsData.Files = fileList.ToArray();

                    return ttsData;

                }
            }
        }

        public static TTSData GetTTSDataFromDate(DateTime fromDate)
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.GetTTSDataFromDate"))
            {
                List<AudioFile> fileList = new List<AudioFile>();

                foreach (DataRow row in TTSStorage.GetAllApprovedAudioData().Rows)
                {
                    DateTime lastModified = (DateTime)row["LastModified"];

                    if (lastModified > fromDate)
                    {
                        AudioFile audioFile = new AudioFile();
                        audioFile.File = row["FileName"].ToString();
                        audioFile.Version = int.Parse(row["Version"].ToString());
                        audioFile.Hash = row["FileHash"].ToString();

                        fileList.Add(audioFile);
                    }
                }

                TTSData ttsData = new TTSData();
                ttsData.Files = fileList.ToArray();

                return ttsData;
            }
        }

        public static TTSData GetUnfilteredTTSData()
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.GetUnfilteredTTSData"))
            {
                List<AudioFile> fileList = new List<AudioFile>();

                DataTable dataResult = TTSStorage.GetAllApprovedAudioData();
                dataResult.Merge(TTSStorage.GetAllUnapprovedAudioData(true));

                foreach (DataRow row in dataResult.Rows)
                {
                    AudioFile audioFile = new AudioFile();
                    audioFile.File = row["FileName"].ToString();
                    audioFile.Version = int.Parse(row["Version"].ToString());
                    audioFile.Hash = row["FileHash"].ToString();

                    fileList.Add(audioFile);
                }

                TTSData ttsData = new TTSData();
                ttsData.Files = fileList.ToArray();

                return ttsData;
            }
        }

        public static Byte[] GetTTSFile(String filename, String customerid)
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.GetTTSFile"))
            {
                String voice = ConfigurationManager.AppSettings["AudioFileDefaultVoice"];
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    if (!String.IsNullOrEmpty(customerid))
                    {
                        int custId = Int32.Parse(customerid);
                        var customervoice = (from x in dbContext.Customers
                                             where x.CustomerId == custId
                                             select x.AnnouncementVoice).FirstOrDefault();
                        if (!String.IsNullOrEmpty(customervoice.ToString()))
                        {
                            voice = customervoice.ToString();
                        }
                    }

                }
                return TTSStorage.GetAudioFile(filename, voice);
            }
        }

        public static Byte[] GetTTSFile(String filename)
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.GetTTSFile"))
            {
                return TTSStorage.GetAudioFile(filename);
            }
        }

        public static void EnsureTTSFileExists(String text)
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.EnsureTTSFileExists"))
            {
                try
                {
                    Mermaid.TTS.AudioFile audioFile = TTSStorage.GetAudioData(text, Voice);

                    if (audioFile == null)
                    {
                        Console.WriteLine("AudioFile not found: " + text + " | " + Voice);

                        using (Mermaid.TTS.ITTSEngine ttsEngine = TTSEngine)
                        {
                            String targetFile = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.InternetCache), FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(text))).ToString() + ".mp3");

                            ttsEngine.GenerateAudioFile(text, targetFile);

                            FileHash fileHash = FileHash.FromFile(targetFile, false);

                            TTSStorage.SaveAudioData(text, Voice, false, false, targetFile, fileHash.ToString(), 1);

                            //UNDONE: Notify somehow that a new file needs approval
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }
            }
        }

        public static void SyncAudioFilesCache()
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                dbContext.SyncCustomerAudioFiles();
            }
        }

        private static Mermaid.TTS.ITTSEngine initializeEngine(string voice)
        {
            try
            {
                return new AcapelaEngine("Mermaid", "zlp0ngk49c", "http://vaas.acapela-group.com/Services/", "Mermaid_APP", voice);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            return null;
        }
        public static void SyncAudioFiles()
        {
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.TTS, "SyncAudioFiles Started");

            try
            {
                Hashtable engines = new Hashtable();
                
                Mermaid.TTS.ITTSEngine ttsEngine = null;//new AcapelaEngine("Mermaid", "zlp0ngk49c", "http://vaas.acapela-group.com/Services/", "Mermaid_APP", voice);
                Mermaid.TTS.Storage dbStorage = new Mermaid.TTS.Storage(AppSettings.GetTTSDatabaseConnectionString());

                // Call stored procedure StopMissingAudio from IBIDataModel
                using (IBI.DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                {

                   Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.TTS, dbContext.Database.Connection.ConnectionString);

                    foreach (var customer in dbContext.Customers.ToList())
                    {

                        int customerID = customer.CustomerId;
                        string customerVoice = customer.AnnouncementVoice;
                        //StopMissingAudio_Result r = new StopMissingAudio_Result();
                       
                        List<string> list = dbContext.StopMissingAudio(customerID, customerVoice).Select(r=>r.StopName).ToList<string>();

                        Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.TTS, String.Format("Customer: {0}, stop list {1}", customer.CustomerId, list.Count()));

                        if (list != null && list.Count > 0)
                        {
                            if (!engines.ContainsKey(customerVoice))
                            {
                                engines[customerVoice] = initializeEngine(customerVoice);
                            }

                            ttsEngine = (Mermaid.TTS.ITTSEngine)engines[customerVoice];
                            if (ttsEngine == null)
                            {
                                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.TTS, "The engine could not be initialized for customer " + customerID.ToString() + " and voice " + customerVoice);
                                continue;
                            }
                        }
                        System.Text.StringBuilder mailMsg = new System.Text.StringBuilder();
                        mailMsg.AppendLine(String.Format("The system has automaticly created {0} new audiofiles.<br /><br />", list.Count));

                        

                        foreach (string stopName in list)
                        {
                            if (String.IsNullOrEmpty(stopName))
                                continue;

                            String customers = String.Empty;
                            List<StopCustomers_Result> resultCustomers = dbContext.StopCustomers(stopName, customerVoice).ToList();
                            if (resultCustomers != null && resultCustomers.Count > 0)
                            {
                                foreach (StopCustomers_Result result in resultCustomers)
                                {
                                    customers += result.FullName + "(" + result.CustomerID.ToString() + "),";
                                }
                                customers = customers.Remove(customers.Length - 1);
                            }

                          
                            //String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopname))).ToString() + ".mp3");
                            String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                            


                            string fixedText = stopName;

                            ttsEngine.GenerateAudioFile(stopName, actualFile, ref fixedText);
                            FileHash fileHash = FileHash.FromFile(actualFile, false);
                            //dbStorage.SaveAudioData(stopname, voice, false, false, actualFile, fileHash.ToString(), 1);
                            dbStorage.SaveAudioData(stopName, customerVoice, false, false, actualFile, fileHash.ToString(), 1, fixedText, customerVoice, null);

                            string fileName = System.IO.Path.GetFileName(actualFile);
                            mailMsg.AppendLine(String.Format("Stop name: {0}<br />", stopName));
                            mailMsg.AppendLine(String.Format("Customers: {0}<br />", customers));
                            mailMsg.AppendLine(String.Format("Voice: {0}<br />", customerVoice));
                            mailMsg.AppendLine(String.Format("Filename: {0}<br />", fileName));
                            mailMsg.AppendLine(String.Format("Click <a href='{0}/TTS/TTSFile.mp3?voice={1}&filename={2}'>here</a> to hear the audiofile.<br />", AppSettings.RestPath, customerVoice, fileName));
                            mailMsg.AppendLine("<br /><br />");

                            File.Delete(actualFile);
                        }

                        //only send notification email if there are 1 or more audio files synced recently.
                        if (list.Count > 0)
                        {
                           //string toEmail = System.Configuration.ConfigurationManager.AppSettings["EmailAudioFilesNotify"].ToString();
                            //string toEmail = AppSettings.NotificationEmail("AudioFileCreated", customerID.ToString());
                            List<string> toEmails = AppSettings.NotificationEmail("AudioFileCreated", customerID.ToString());
                            foreach (string toEmail in toEmails)
                            {
                                Common.Mail.SendSimpleMail("noreply@mermaid.dk", toEmail, String.Format("{0} new stop announcemnet audiofile(s) created", list.Count), mailMsg.ToString(), null);
                            }
                        }
                    }
                }
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.TTS, "SyncAudioFiles completed successfully");
            }
            catch (Exception ex)
            {
                //Logger.LogError(ex);
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.TTS, Logger.GetDetailedError(ex));
            }

        }


        public static List<IBI.Shared.Models.TTS.AudioFile> GetStopAudioFiles(string stopName, bool approved, bool rejected, bool unapproved)
        {
            List<IBI.Shared.Models.TTS.AudioFile> list = new List<IBI.Shared.Models.TTS.AudioFile>();

            if (stopName == null)
                stopName = "";
            

            try
            {
                using (TTSServerDataModel dbContext = new TTSServerDataModel())
                {
                    list = dbContext.GetStopAudioFiles(stopName, approved, rejected, unapproved).Select(a => new IBI.Shared.Models.TTS.AudioFile
                   {
                       Approved = a.Approved ?? false,
                       FileHash = a.FileHash,
                       Filename = a.FileName,
                       LastModified = a.LastModified,
                       Rejected = a.Rejected ?? false,
                       SourceText = a.SourceText,
                       SourceVoice = a.SourceVoice,
                       Text = a.Text,
                       Version = a.Version,
                       Voice = a.Voice
                   }).OrderBy(a=>a.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }

            return list;

        }

        public static IBI.Shared.Models.TTS.AudioFile GetAudioFile(string filename, string voice)
        {
            List<IBI.Shared.Models.TTS.AudioFile> list = new List<IBI.Shared.Models.TTS.AudioFile>();


            try
            {
                using (TTSServerDataModel dbContext = new TTSServerDataModel())
                {
                    list = dbContext.GetStopAudioFile(filename, voice).Select(a => new IBI.Shared.Models.TTS.AudioFile
                    {
                        Approved = a.Approved ?? false,
                        FileHash = a.FileHash,
                        Filename = a.FileName,
                        LastModified = a.LastModified,
                        Rejected = a.Rejected ?? false,
                        SourceText = a.SourceText,
                        SourceVoice = a.SourceVoice,
                        Text = a.Text,
                        Version = a.Version,
                        Voice = a.Voice
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }

            return list.FirstOrDefault();

        }

        public static byte[] GetTTSFileByStopName(string stopname, int customerid)
        {
            using (new IBI.Shared.CallCounter("TTSServiceController.GetTTSFile"))
            {
                String voice = ConfigurationManager.AppSettings["AudioFileDefaultVoice"];
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    if (customerid != 0)
                    {
                        var customervoice = (from x in dbContext.Customers
                                             where x.CustomerId == customerid
                                             select x.AnnouncementVoice).FirstOrDefault();
                        if (!String.IsNullOrEmpty(customervoice.ToString()))
                        {
                            voice = customervoice.ToString();
                        }
                    }
                }
                return TTSStorage.GetAudioFileByStopName(stopname, voice);
            }
        }

        public static byte[] GetTTSFileByStopName(string stopname, string voice)
        {
            return TTSStorage.GetAudioFileByStopName(stopname, voice);
        }

        public static byte[] GetTTSFileByVoice(string filename, string voice)
        {
            return TTSStorage.GetAudioFile(filename, voice);
        }

        public static byte[] GenerateAudioFile(string stopName, string sourceText, string voice)
        {
            try
            {

               string fileName = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                fileName = System.IO.Path.GetFileName(fileName);

                bool generateAudio = false; 

                using (TTSServerDataModel dbContext = new TTSServerDataModel())
                {
                    IBI.Shared.Models.TTS.AudioFile audioFile = dbContext.GetStopAudioFile(fileName, voice).Select(a => new IBI.Shared.Models.TTS.AudioFile
                    {
                        Approved = a.Approved ?? false,
                        FileHash = a.FileHash,
                        Filename = a.FileName,
                        LastModified = a.LastModified,
                        Rejected = a.Rejected ?? false,
                        SourceText = a.SourceText,
                        SourceVoice = a.SourceVoice,
                        Text = a.Text,
                        Version = a.Version,
                        Voice = a.Voice,
                        FileContent = a.FileContent
                    }).FirstOrDefault();


                    if (audioFile == null)
                    {
                        generateAudio = true; 
                    }
                    else
                    {
                        if (audioFile.SourceText != sourceText)
                        {
                            generateAudio = true;                            
                        }

                    }

                }
                Byte[] fileContent = null;

                if (!(String.IsNullOrEmpty(stopName)) && !(String.IsNullOrEmpty(voice)) && !generateAudio)
                { 
                    fileContent = TTSServiceController.GetTTSFileByStopName(stopName, voice);
                }
                else
                {
                    Mermaid.TTS.ITTSEngine ttsEngine = null;//new AcapelaEngine("Mermaid", "zlp0ngk49c", "http://vaas.acapela-group.com/Services/", "Mermaid_APP", voice);
                    Mermaid.TTS.Storage dbStorage = new Mermaid.TTS.Storage(AppSettings.GetTTSDatabaseConnectionString());
                     
                       
                    ttsEngine = initializeEngine(voice);

                    if (ttsEngine == null)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "The engine could not be initialized for voice " + voice);
                        return null;
                    }

                    String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                    
                    ttsEngine.GenerateAudioFile(sourceText, actualFile, ref sourceText);

                    fileContent = System.IO.File.ReadAllBytes(actualFile);
                }

                return fileContent;

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return null;
        }

        public static byte[] GenerateAudioPreview(string stopName, string sourceText, string voice)
        {
            try
            {

                string fileName = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                fileName = System.IO.Path.GetFileName(fileName);

                bool generateAudio = false;
                 
                Byte[] fileContent = null;
                
 
                Mermaid.TTS.ITTSEngine ttsEngine = null;//new AcapelaEngine("Mermaid", "zlp0ngk49c", "http://vaas.acapela-group.com/Services/", "Mermaid_APP", voice);
                Mermaid.TTS.Storage dbStorage = new Mermaid.TTS.Storage(AppSettings.GetTTSDatabaseConnectionString());


                ttsEngine = initializeEngine(voice);

                if (ttsEngine == null)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "The engine could not be initialized for voice " + voice);
                    return null;
                }

                String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");

                ttsEngine.GenerateAudioFile(sourceText, actualFile, ref sourceText);

                fileContent = System.IO.File.ReadAllBytes(actualFile);
               

                return fileContent;

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return null;
        }

        public static bool SaveAudioFile(string stopName, string sourceText, string voice, string status)
        {
            //RefreshMD5Values();
            //return false;

            try
            {
                bool approved = false;
                bool rejected = false;

                if (status == "A")
                    approved = true;
                if (status == "R")
                    rejected = true;



                string fileName = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                fileName = System.IO.Path.GetFileName(fileName);

                using (TTSServerDataModel dbContext = new TTSServerDataModel())
                {
                    bool generateAudio = false;
                    int version = 1;

                    IBI.Shared.Models.TTS.AudioFile audioFile = dbContext.GetStopAudioFile(fileName, voice).Select(a => new IBI.Shared.Models.TTS.AudioFile
                    {
                        Approved = a.Approved ?? false,
                        FileHash = a.FileHash,
                        Filename = a.FileName,
                        LastModified = a.LastModified,
                        Rejected = a.Rejected ?? false,
                        SourceText = a.SourceText,
                        SourceVoice = a.SourceVoice,
                        Text = a.Text,
                        Version = a.Version,
                        Voice = a.Voice,
                        FileContent = a.FileContent
                    }).FirstOrDefault();

                    if (audioFile==null)
                    {
                        generateAudio = true;
                        version = 1;
                    }
                    else
                    {
                       version = audioFile.Version ?? 1;

                       if(audioFile.SourceText!=sourceText  && sourceText!=null)
                       {
                        generateAudio = true;
                        version = (audioFile.Version ?? 0) + 1;   
                       }

                    }


                    Mermaid.TTS.ITTSEngine ttsEngine = null;//new AcapelaEngine("Mermaid", "zlp0ngk49c", "http://vaas.acapela-group.com/Services/", "Mermaid_APP", voice);
                    Mermaid.TTS.Storage dbStorage = new Mermaid.TTS.Storage(AppSettings.GetTTSDatabaseConnectionString());
                    
                    if (generateAudio)
                    {
                       
                        ttsEngine = initializeEngine(voice);

                        if (ttsEngine == null)
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "The engine could not be initialized for voice " + voice);
                            return false;
                        }

                        String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                        //String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName))).ToString() + ".mp3");

                        ttsEngine.GenerateAudioFile(sourceText, actualFile, ref sourceText);
                        FileHash fileHash = FileHash.FromFile(actualFile, false);
                                                
                        dbStorage.SaveAudioData(stopName, voice, approved, rejected, actualFile, fileHash.ToString(), version, sourceText, voice, null);

                    }
                    else
                    {
                        FileHash fileHash = FileHash.FromString(audioFile.FileHash); //FileHash.FromFile(audioFile.Filename, false);
                        dbStorage.SaveAudioData(stopName, voice, approved, rejected, audioFile.Filename, fileHash.ToString(), version, sourceText, voice, audioFile.FileContent);

                    }

                }



                return true;

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.TTS, ex);
            }

            return false;
        }

        public static bool SaveAudioFileMp3(string stopName, string sourceText, string voice, string status, byte[] mp3, string alternateSourceVoiceText)
        {
            //RefreshMD5Values();
            //return false;

            try
            {
                bool approved = false;
                bool rejected = false;

                if (status == "A")
                    approved = true;
                if (status == "R")
                    rejected = true;



                string fileName = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                fileName = System.IO.Path.GetFileName(fileName);

                using (TTSServerDataModel dbContext = new TTSServerDataModel())
                {
                    bool generateAudio = false;
                    int version = 1;

                    IBI.Shared.Models.TTS.AudioFile audioFile = dbContext.GetStopAudioFile(fileName, voice).Select(a => new IBI.Shared.Models.TTS.AudioFile
                    {
                        Approved = a.Approved ?? false,
                        FileHash = a.FileHash,
                        Filename = a.FileName,
                        LastModified = a.LastModified,
                        Rejected = a.Rejected ?? false,
                        SourceText = a.SourceText,
                        SourceVoice = a.SourceVoice,
                        Text = a.Text,
                        Version = a.Version,
                        Voice = a.Voice,
                        FileContent = a.FileContent
                    }).FirstOrDefault();

                    if (audioFile == null)
                    {
                        generateAudio = true;
                        version = 1;
                    }
                    else
                    {
                        version = audioFile.Version ?? 1;
                        version = (audioFile.Version ?? 0) + 1;
                    }

                    Mermaid.TTS.Storage dbStorage = new Mermaid.TTS.Storage(AppSettings.GetTTSDatabaseConnectionString());

                    audioFile.SourceVoice = alternateSourceVoiceText;

                    //String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                    //String actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName))).ToString() + ".mp3");
                    string actualFile = System.IO.Path.Combine(AppSettings.TemporaryDirectory, FileHash.FromStream(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(stopName.ToLower()))).ToString() + ".mp3");
                    //ttsEngine.GenerateAudioFile(sourceText, actualFile, ref sourceText);
                    FileHash fileHash = FileHash.FromStream(new MemoryStream(mp3));

                    dbStorage.SaveAudioData(stopName, audioFile.Voice, approved, rejected, actualFile, fileHash.ToString(), version, "", alternateSourceVoiceText, mp3);

                    return true;
                }
 
                

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.TTS, ex);
            }

            return false;
        }

        internal static List<string> GetAllSourceVoices()
        {
            using (TTSServerDataModel dbContext = new TTSServerDataModel())
            {
                return dbContext.GetAllSourceVoices().ToList();
            }
        }
    }
}