﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Core
{
    public class ClientConfigurationHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            String customerId = context.Request["customerid"];
            String busNumber = context.Request["busNumber"];

            if (String.IsNullOrEmpty(customerId))
                customerId = "2140";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

            sb.Append(SystemServiceController.GetClientConfigurations(busNumber, int.Parse(customerId)));

            Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

            //Set the HTTP MIME type of the output stream 
            context.Response.ContentType = "text/xml";

            //Write the data out to the client. 
            context.Response.BinaryWrite(fileContent);
        }

        #endregion
    }
}
