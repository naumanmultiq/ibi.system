﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using IBI.Shared.Models;
using IBI.Shared;

namespace IBI.REST.Core
{
    public class SecurityServiceController
    {

        public static String CreateSecurityToken(String user, String password)
        {
            return null;
        }

        public static SecurityGroup[] GetSecurityGroups()
        {
            Dictionary<String, SecurityGroup> securityGroups = new Dictionary<String, SecurityGroup>();

            using (new IBI.Shared.CallCounter("SystemServiceController.getBusList"))
            {
                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        //String script = "SELECT DISTINCT [BusNumber] FROM [Clients]";
                        String script = "SELECT * FROM [SecurityGroups]" + Environment.NewLine +
                                        "ORDER BY [Name] ASC";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            SqlDataReader dataReader;

                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                dataReader = command.ExecuteReader();
                            }

                            using (dataReader)
                            {
                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {
                                        String name = dataReader.GetString(dataReader.GetOrdinal("Name"));
                                        int accessibleCustomer = dataReader.GetInt32(dataReader.GetOrdinal("AccessibleCustomer"));

                                        SecurityGroup group = securityGroups.ContainsKey(name) ? securityGroups[name] : null;

                                        if (group == null)
                                        {
                                            group = new SecurityGroup();
                                            group.Name = name;
                                            securityGroups[name] = group;
                                        }

                                        if (!group.AccessibleCustomers.Contains(accessibleCustomer))
                                            group.AccessibleCustomers.Add(accessibleCustomer);

                                    }
                                }
                            }
                        }
                    }
                }
            }

            return securityGroups.Values.ToArray<SecurityGroup>();
        }

    }
}