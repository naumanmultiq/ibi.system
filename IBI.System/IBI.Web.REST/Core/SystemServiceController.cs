﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using IBI.Shared.Models;
using IBI.REST.Shared;
using IBI.DataAccess.DBModels;
using IBI.Implementation.Schedular;
using IBI.Shared.Interfaces;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.BusTree;
using IBI.Shared;
using System.Data.Objects;
using IBI.Shared;
using System.Text;

namespace IBI.REST.Core
{
    public class SystemServiceController
    {
        #region "Attributes"

        private static ISyncScheduleFactory _schedularFactory = new SyncScheduleFactory();

        #endregion

        #region "Implementation"

        #region DataUpdate
        public static string GetDataUpdateStatus(string busNumber, int customerId)
        {
            string retVal = "";

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                retVal = dbContext.GetDataUpdateStatus(customerId, busNumber).FirstOrDefault();
            }

            return retVal;

        }

        #endregion

        #region BusTree

        //a test function
        public static List<BusNode> CreateBusTree()
        {
            return CreateBusTree(3, 0);
        }

        //function to get the Hierarcal Bus Tree
        public static List<BusNode> CreateBusTree(int userId, int groupId = 0)
        {

            List<BusNode> rootGroups = new List<BusNode>();


            IBIDataModel dbContext = new IBIDataModel();


            var uGroups = dbContext.Users.FirstOrDefault(x => x.UserId == userId).UserGroups;

            foreach (var uGroup in uGroups)
            {
                var groups = groupId > 0 ? uGroup.Groups.Where(x => x.GroupId == groupId) : uGroup.Groups;

                foreach (var group in groups)
                {

                    GroupNode gNode = SetGroupNode(group);
                    GroupNode rootGNode = GetParentGroupNode(group, gNode);

                    MergeWithExistingRootNodes(rootGroups, rootGNode);


                }
            }

            return rootGroups;

        }


        //function to get the Hierarcal Bus Tree
        public static GroupNode GetGroupNode(int groupId)
        {

            IBIDataModel dbContext = new IBIDataModel();

            var group = dbContext.Groups.Where(x => x.GroupId == groupId).FirstOrDefault();


            if (group != null)
            {

                GroupNode gNode = SetGroupNode(group);
                return gNode;

            }

            return null;

        }


        //Recursive function to merge current GroupNode with the existing nodes.
        private static void MergeWithExistingRootNodes(List<BusNode> rootGroups, GroupNode rootGNode)
        {
            //if rootNode already exists. don't add another root, rather embed inside it.
            GroupNode existingGroupNode = (GroupNode)(rootGroups.Find(x => x.GroupName == rootGNode.GroupName));

            if (existingGroupNode == null)
                rootGroups.Add(rootGNode);
            else
                MergeWithExistingRootNodes(existingGroupNode.children, (GroupNode)rootGNode.children.First());
        }

        //recursive function to get the Parent Hierarchy of Group Nodes
        private static GroupNode GetParentGroupNode(IBI.DataAccess.DBModels.Group group, GroupNode gNode)
        {
            IBI.DataAccess.DBModels.Group pGroup = group.ParentGroup;

            if (pGroup != null)
            {
                GroupNode pGroupNode = new GroupNode(Guid.NewGuid(), pGroup.GroupName, pGroup.GroupId, group.CustomerId ?? 0, new List<BusNode>());
                pGroupNode.children.Add(gNode);

                if (pGroup.ParentGroupId == null)
                {
                    return pGroupNode;
                }
                else
                {
                    return GetParentGroupNode(pGroup, pGroupNode);

                }

            }
            else
            {
                return gNode;
            }


        }

        //recursive function to populate every group and underlying buses.
        private static GroupNode SetGroupNode(IBI.DataAccess.DBModels.Group group)
        {

            using (IBIDataModel db = new IBIDataModel())
            {
                List<BusNode> children = new List<BusNode>(); //for tree node children

                //get child groups
                List<IBI.DataAccess.DBModels.Group> childGroups = group.Groups.ToList();

                foreach (IBI.DataAccess.DBModels.Group grp in childGroups)
                {
                    //set every child group
                    children.Add(SetGroupNode(grp));
                }

                //get buses 
                List<IBI.DataAccess.DBModels.Bus> childBuses = group.Buses.ToList();

                foreach (IBI.DataAccess.DBModels.Bus bus in childBuses)
                {
                    //set every child bus node

                    //some vars to fill bus properties
                    int customerId = Convert.ToInt32(bus.CustomerId);



                    BusDetail busDetail = new BusDetail();


                    busDetail.CustomerID = customerId;
                    busDetail.CustomerName = bus.Customer.FullName;
                    busDetail.InOperation = AppUtility.ToBool(bus.InOperation);
                    busDetail.LastTimeInService = AppUtility.ToDateTime(bus.LastTimeInService);
                    busDetail.CurrentJourney = AppUtility.IsNullInt(bus.CurrentJourneyNumber);
                    busDetail.LastTimeInService = AppUtility.ToDateTime(bus.LastTimeInService); //client..LastScheduleTime; [KHI]: LastScheduleTime not known
                    busDetail.Remarks = string.Empty;
                    busDetail.ServiceLevel = AppUtility.IsNullInt(bus.ServiceLevel);

                    busDetail.InService = GetBusInServiceStatus(busDetail.InOperation, busDetail.LastTimeInService, AppUtility.ToInt64(bus.ScheduleJourneyNumber));

                    if (busDetail.InService.Exists)
                        busDetail.ScheduledJourney = AppUtility.ToInt64(bus.ScheduleJourneyNumber);


                    busDetail.IsServiceCase = busDetail.ServiceLevel != 0;

                    foreach (var client in bus.Clients)
                    {
                        //some vars to fill client's properties
                        bool client_MarkedForService;

                        switch (client.ClientType)
                        {
                            case "DCU":
                                {
                                    busDetail.DCU.LastPing = client.LastPing;
                                    busDetail.DCU.Exists = true;
                                    break;
                                }
                            case "VTC":
                                {
                                    busDetail.VTC.LastPing = client.LastPing;
                                    busDetail.VTC.Exists = true;
                                    break;
                                }
                            case "CCU":
                                {
                                    busDetail.CCU.LastPing = client.LastPing;
                                    busDetail.CCU.Exists = true;
                                    break;
                                }
                        }

                        client_MarkedForService = AppUtility.ToBool(client.MarkedForService);
                        //TODO: Ask Martin
                        if (string.Compare(client.ClientType, "CCU", true) == 0)
                        {
                            if (client.LastPing > busDetail.DCU.LastPing)
                            {
                                busDetail.CCU.MarkedForService = client_MarkedForService;

                            }

                            busDetail.CCU.LastPing = client.LastPing;
                        }

                        if (string.Compare(client.ClientType, "VTC", true) == 0)
                        {
                            if (client.LastPing > busDetail.DCU.LastPing)
                            {
                                busDetail.VTC.MarkedForService = client_MarkedForService;

                            }

                            busDetail.VTC.LastPing = client.LastPing;

                            busDetail.InfotainmentMacAddress = client.MacAddress;
                        }

                        if (string.Compare(client.ClientType, "DCU", true) == 0)
                        {
                            busDetail.DCU.LastPing = client.LastPing;

                        }


                    }

                    if (!string.IsNullOrEmpty(busDetail.InfotainmentMacAddress))
                        busDetail.Infotainment.LastPing = getLastInfotainmentPing(busDetail.InfotainmentMacAddress);




                    busDetail.Hotspot.LastPing = getLastHotspotPing(bus.BusNumber);


                    if (AppSettings.ShouldValidateSchedules())
                    {
                        //[KHI]: bus_SceduledJourney ???? where to get value from
                        busDetail.ScheduleOk = (busDetail.CurrentJourney == busDetail.ScheduledJourney);

                        if (!busDetail.ScheduleOk)
                            busDetail.Remarks += " - Wrong journey" + Environment.NewLine;
                    }



                    //conclude each client's Status color here.

                    // bool busHasServiceCase = false; //hard coded... in real coming from novo DB.

                    TimeSpan offlineToleranceTime = AppSettings.GetOfflineTolerance();

                    //DCU

                    bool isInService = busDetail.InService.Color == BusStatusColor.GREEN ? true : false;

                    if (busDetail.DCU.Exists)
                        busDetail.DCU.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.DCU.LastPing, offlineToleranceTime);

                    if (busDetail.VTC.Exists)
                        busDetail.VTC.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.VTC.LastPing, offlineToleranceTime);

                    if (busDetail.CCU.Exists)
                        busDetail.CCU.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.CCU.LastPing, offlineToleranceTime);

                    if (busDetail.Hotspot.LastPing > DateTime.MinValue)
                        busDetail.Hotspot.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.Hotspot.LastPing, offlineToleranceTime);

                    if (busDetail.Infotainment.LastPing > DateTime.MinValue)
                        busDetail.Infotainment.Color = GetClientStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, busDetail.Infotainment.LastPing, offlineToleranceTime);

                    BusUnitStatus[] clientStatuses = new BusUnitStatus[]
                        {
                            busDetail.CCU,
                            busDetail.DCU,
                            busDetail.VTC,
                            
                            busDetail.Hotspot,
                            busDetail.Infotainment
                        };


                    busDetail.BusStatus = GetBusStatus(busDetail.InOperation, isInService, busDetail.LastTimeInService, ref clientStatuses, busDetail.IsServiceCase);


                    BusNode busNode = BusNode.CreateBusNode(bus.BusNumber, busDetail);

                    children.Add(busNode);

                }

                //set current node
                GroupNode node = GroupNode.CreateGroup(group.GroupName, group.GroupId, Convert.ToInt32(group.CustomerId), children);
                return node;

            }


        }

        private static BusUnitStatus GetBusInServiceStatus(bool inOperation, DateTime busLastTimeInService, long scheduledJourneyNumber)
        {
            BusUnitStatus busInService = new BusUnitStatus("In Service");



            busInService.LastPing = busLastTimeInService;

            if (busLastTimeInService == DateTime.MinValue) //busDetail.InService.LastPing>DateTime.MinValue && busDetail.LastTimeInService > DateTime.Now.AddHours(-30);
            {
                busInService.Exists = false;
                busInService.Color = BusStatusColor.NONE;
            }
            else
            {
                busInService.Exists = true;

                if (!inOperation) //extreme priority case - Light Grey [if bus is marked as Not in operation]
                {
                    busInService.Color = BusStatusColor.LIGHT_GREY;
                }
                else
                    if (scheduledJourneyNumber == -1)
                    {
                        busInService.Color = BusStatusColor.RED;
                    }
                    else
                        if (scheduledJourneyNumber == 0 || busLastTimeInService < DateTime.Now.AddHours(-30))
                            busInService.Color = BusStatusColor.GREY;
                        else
                            busInService.Color = BusStatusColor.GREEN;

            }

            return busInService;

        }

        //helper function to get the Client's status color
        private static BusStatusColor GetClientStatus(bool inOperation, bool isBusInService, DateTime busLastTimeInService, DateTime clientLastPing, TimeSpan offlineTolerance)
        {

            if (!inOperation)
                return BusStatusColor.LIGHT_GREY;

            bool isClientOnline = !(clientLastPing.Add(offlineTolerance) < DateTime.Now); //!(clientLastPing.Add(offlineTolerance) < busLastTimeInService);

            if (isBusInService && isClientOnline)
                return BusStatusColor.GREEN;

            if (!isBusInService && isClientOnline)
                return BusStatusColor.LIGHT_GREEN;

            if (!isBusInService && !isClientOnline)
                return BusStatusColor.GREY;

            if (isBusInService && !isClientOnline || (clientLastPing.Add(offlineTolerance) < busLastTimeInService && !isBusInService))
                return BusStatusColor.RED;



            return BusStatusColor.NONE;
        }

        //helper function to get the overall Bus Status color.
        private static BusStatusColor GetBusStatus(bool inOperation, bool isBusInService, DateTime busLastTimeInService, ref BusUnitStatus[] clientStatuses, bool busHasServiceCase)
        {

            if (!inOperation) //max priority - Light grey
            {
                foreach (BusUnitStatus clientStatus in clientStatuses)
                {
                    clientStatus.Color = BusStatusColor.LIGHT_GREY;
                }

                return BusStatusColor.LIGHT_GREY;
            }

            if (busHasServiceCase)
            {
                //turns all RED clients into YELLOW
                foreach (BusUnitStatus clientStatus in clientStatuses)
                {
                    if (clientStatus.Color == BusStatusColor.RED)
                        clientStatus.Color = BusStatusColor.YELLOW;
                }

                return BusStatusColor.YELLOW;
            }


            if (!isBusInService || busLastTimeInService == DateTime.MinValue)
                return BusStatusColor.GREY;


            bool anyClientWith24HrsOldPing;

            try
            {
                anyClientWith24HrsOldPing = clientStatuses.Where(x => x.Exists && x.LastPing < busLastTimeInService.AddHours(-24)).Count() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (anyClientWith24HrsOldPing)
                return BusStatusColor.ORANGE;



            bool anyOfflineClient = clientStatuses.Where(x => x.Color == BusStatusColor.RED).Count() > 0;

            if (isBusInService && anyOfflineClient)
                return BusStatusColor.RED;

            if (isBusInService && !anyOfflineClient)
                return BusStatusColor.GREEN;


            return BusStatusColor.NONE;

        }


        ////recursive TEST function to set every group and node.
        //private static GroupNode SetGroup_Test(Group group)
        //{

        //    using (IBIDataModel db = new IBIDataModel())
        //    {
        //        List<BusNode> children = new List<BusNode>(); //for tree node children

        //        //get child groups
        //        List<IBI.DataAccess.DBModels.Group> childGroups = group.Groups.ToList();

        //        foreach (IBI.DataAccess.DBModels.Group grp in childGroups)
        //        {
        //            //set every child group
        //            children.Add(SetGroup_Test(grp));
        //        }

        //        List<IBI.DataAccess.DBModels.Bus> childBuses = group.Buses.ToList();

        //        foreach (IBI.DataAccess.DBModels.Bus b in childBuses)
        //        {
        //            //set every child bus node
        //            DateTime now = DateTime.Now;
        //            BusDetail busDetail = new BusDetail
        //            {
        //                DCU = new BusUnitStatus() { Color = BusStatusColor.GREEN, LastPing = now, Description = "DCU info", Link = "" },
        //                VTC = new BusUnitStatus() { Color = BusStatusColor.GREY, LastPing = now, Description = "VTC info", Link = "" },
        //                Hotspot = new BusUnitStatus() { Color = BusStatusColor.YELLOW, LastPing = now, Description = "Hotspot info", Link = "" },
        //                Infotainment = new BusUnitStatus() { Color = BusStatusColor.RED, LastPing = now, Description = "Infotainment info", Link = "" },
        //                CustomerID = Convert.ToInt32(b.CustomerId)
        //            };


        //            children.Add(BusNode.CreateBusNode(b.BusNumber, busDetail));
        //        }

        //        //set current node
        //        GroupNode node = GroupNode.CreateGroup(group.GroupName, group.GroupId, Convert.ToInt32(group.CustomerId), children);
        //        return node;
        //    }
        //}


        public static IBI.Shared.Models.BusTree.Group GetGroup(int groupId)
        {

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                var groups = dbContext.Groups.Where(g => g.GroupId == groupId).Select(g => new IBI.Shared.Models.BusTree.Group()
                {
                    CustomerId = g.CustomerId,
                    CustomerName = g.Customer.FullName,
                    GroupId = g.GroupId,
                    GroupName = g.GroupName,
                    ParentGroupId = g.ParentGroupId,
                    ParentGroupName = g.ParentGroup.GroupName

                });

                if (groups != null & groups.Count() > 0)
                {
                    return groups.FirstOrDefault();
                }
            }

            return null;
        }



        public static IBI.Shared.Models.BusTree.Bus GetBus(int customerId, string busNumber)
        {

            ////#hack to save geography information
            //var point = SqlGeography.Point(double.Parse(latitude), double.Parse(longitude), 4326);
            //var poly = point.BufferWithTolerance(1, 0.01, true);

            string sql = @"SELECT * FROM vBuses WHERE CustomerId=" + customerId + " AND BusNumber='" + busNumber + "'";

            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    //var param = new SqlParameter(@"Location", poly);
                    //param.UdtTypeName = "Geography";
                    //command.Parameters.Add(param);

                    IBI.Shared.Models.BusTree.Bus bus = new IBI.Shared.Models.BusTree.Bus();

                    try
                    {

                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            bus.BusNumber = AppUtility.IsNullStr(reader["BusNumber"]);
                            bus.CustomerId = AppUtility.IsNullInt(reader["CustomerId"]);
                            bus.CustomerName = AppUtility.IsNullStr(reader["CustomerName"]);
                            bus.ParentGroupId = AppUtility.IsNullInt(reader["ParentGroupId"]);
                            bus.ParentGroupName = AppUtility.IsNullStr(reader["ParentGroupName"]);
                            bus.CurrentJourneyNumber = AppUtility.IsNullLong(reader["CurrentJourneyNumber"]);
                            bus.Description = AppUtility.IsNullStr(reader["Description"]);
                            bus.InOperation = AppUtility.IsNullBool(reader["InOperation"]);
                            bus.LastTimeInService = AppUtility.IsNullDate(reader["LastTimeInService"], Types.Dates.MinDateTime);
                            bus.DateAdded = AppUtility.IsNullStr(reader["DateAdded"]);
                            bus.DateModified = AppUtility.IsNullStr(reader["DateModified"]);

                            dynamic poly = reader["LastKnownLocation"];

                            Microsoft.SqlServer.Types.SqlGeography geo = new Microsoft.SqlServer.Types.SqlGeography();

                            try
                            {
                                geo = (Microsoft.SqlServer.Types.SqlGeography)poly;

                                if (!geo.IsNull)
                                {
                                    //convert DB data to custom "IBI.Shared.Models.BusTree.Geography" type. 
                                    //We will enhance "IBI.Shared.Models.BusTree.Geography" type, once we get more details about its implementation.
                                    Geography lastKnownLocation = new Geography()
                                    {
                                        Latitude = !geo.EnvelopeCenter().Lat.IsNull ? geo.EnvelopeCenter().Lat.Value : 0.00,
                                        Longitude = !geo.EnvelopeCenter().Long.IsNull ? geo.EnvelopeCenter().Long.Value : 0.00

                                    };

                                    bus.LastKnownLocation = lastKnownLocation;
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                                //ignore exception and set default Geography info.
                                bus.LastKnownLocation = new Geography(0.00, 0.00);
                            }



                            reader.Close();
                            connection.Close();

                            return bus;

                        }
                    }
                    catch (Exception ex) { throw ex; }

                    finally
                    {
                        if (connection.State != System.Data.ConnectionState.Closed)
                            connection.Close();
                    }
                }

            }


            return null;

        }



        #endregion


        public static IBI.Shared.Models.Bus[] getBusList()
        {
            return getBusList(null);
        }

        public static IBI.Shared.Models.Bus[] getBusList(List<int> busNumbers)
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.getBusList"))
            {
                try
                {
                    List<IBI.Shared.Models.Bus> busList = new List<IBI.Shared.Models.Bus>();
                    IBIDataModel dbContext = new IBIDataModel();

                    var clients = dbContext.Clients;

                    //select unique groups from selected clients.
                    //clients.Select(x=>Models.Bus {})

                    Hashtable busMap = new Hashtable();
                    foreach (var client in clients)
                    {
                        IBI.Shared.Models.Bus tmpBus = (IBI.Shared.Models.Bus)busMap[client.BusNumber + "|" + client.CustomerId.ToString()];
                        if (tmpBus == null)
                        {
                            tmpBus = new IBI.Shared.Models.Bus();
                            tmpBus.BusNumber = client.BusNumber;
                            tmpBus.CustomerId = client.CustomerId;
                            //tmpBus.LastScheduleTime =  client.LastScheduleTime; [KHI]
                            tmpBus.IsOK = true;
                            tmpBus.Remarks = string.Empty;
                            //tmpBus.ServiceLevel = client.ServiceLevel!=null ? client.ServiceLevel.Value : 0; [KHI]
                            tmpBus.MarkedForService = client.MarkedForService != null ? client.MarkedForService.Value : false;
                            //tmpBus.InOperation = client.InOperation != null ? client.InOperation.Value : false; [KHI]
                        }

                        if (string.Compare(client.ClientType, "VTC", true) == 0)
                        {
                            if (client.LastPing > tmpBus.LastDCUPing)
                            {
                                tmpBus.CurrentJourney = client.Bus.CurrentJourneyNumber ?? 0;
                                tmpBus.ScheduledJourney = client.Bus.ScheduleJourneyNumber ?? 0;
                                //tmpBus.CurrentJourney = client.CurrentJourneyNumber.Value;  [KHI]

                                if (client.Bus.LastKnownLocation != null)
                                {
                                    tmpBus.Latitude = client.Bus.LastKnownLocation.Latitude.ToString();
                                    tmpBus.Longitude = client.Bus.LastKnownLocation.Longitude.ToString();
                                }
                                //[KHI] moved to LastKnownLocation of the bus
                                //tmpBus.Longitude = client.Longitude; 
                                //tmpBus.Latitude = client.Latitude;
                            }

                            tmpBus.LastVTCPing = client.LastPing;
                            tmpBus.InfotainmentMacAddress = client.MacAddress;


                            // [KHI] commented
                            //if (client.LastPing.Add(AppSettings.GetOfflineTolerance()) < client.LastScheduleTime)
                            //{
                            //    tmpBus.IsOK = false;
                            //    tmpBus.Remarks += " - VTC offline" + Environment.NewLine;
                            //}
                        }
                        if (string.Compare(client.ClientType, "DCU", true) == 0)
                        {
                            if (client.LastPing > tmpBus.LastVTCPing)
                            {
                                tmpBus.CurrentJourney = client.Bus.CurrentJourneyNumber ?? 0;
                                tmpBus.ScheduledJourney = client.Bus.ScheduleJourneyNumber ?? 0;

                                if (client.Bus.LastKnownLocation != null)
                                {
                                    tmpBus.Latitude = client.Bus.LastKnownLocation.Latitude.ToString();
                                    tmpBus.Longitude = client.Bus.LastKnownLocation.Longitude.ToString();
                                }

                                //tmpBus.CurrentJourney = client.CurrentJourneyNumber.Value; [KHI]
                                //tmpBus.Longitude = client.Longitude;
                                //tmpBus.Latitude = client.Latitude;
                            }

                            tmpBus.LastDCUPing = client.LastPing;
                            //[KHI]
                            //if (client.LastPing.Add(AppSettings.GetOfflineTolerance()) < client.LastScheduleTime)
                            //{
                            //    tmpBus.IsOK = false;
                            //    tmpBus.Remarks += " - DCU offline" + Environment.NewLine;
                            //}
                        }

                        if (string.Compare(client.ClientType, "CCU", true) == 0)
                        {
                            if (client.LastPing > tmpBus.LastVTCPing)
                            {
                                tmpBus.CurrentJourney = client.Bus.CurrentJourneyNumber ?? 0;
                                tmpBus.ScheduledJourney = client.Bus.ScheduleJourneyNumber ?? 0;

                                if (client.Bus.LastKnownLocation != null)
                                {
                                    tmpBus.Latitude = client.Bus.LastKnownLocation.Latitude.ToString();
                                    tmpBus.Longitude = client.Bus.LastKnownLocation.Longitude.ToString();
                                }
                            }

                            tmpBus.LastCCUPing = client.LastPing;
                        }

                        busMap[client.BusNumber + "|" + client.CustomerId.ToString()] = tmpBus;
                    }


                    busList = busMap.Values.Cast<IBI.Shared.Models.Bus>().ToList();

                    //Find all buses without clients, so their hotspot can be represented as well
                    //List<String> busNumbersWithClients = dbContext.Clients.Select(c => new { c.BusNumber, c.CustomerId}).Distinct().ToList());


                    var busesWithoutClients = dbContext.Buses.Where(b => b.Clients.Count() == 0)
                        .Select(b => new IBI.Shared.Models.Bus
                                              {
                                                  BusNumber = b.BusNumber,
                                                  CustomerId = b.CustomerId,
                                                  IsOK = true,
                                                  Remarks = string.Empty,
                                                  MarkedForService = false
                                              });

                    //var busesWithoutClients = from b in dbContext.Buses
                    //where !busNumbersWithClients.Contains(b.BusNumber)
                    //select new IBI.Shared.Models.Bus()
                    //{
                    //    BusNumber = b.BusNumber,
                    //    CustomerId = b.CustomerId,
                    //    IsOK = true,
                    //    Remarks = string.Empty,
                    //    MarkedForService = false
                    //};

                    busList.AddRange(busesWithoutClients);


                    // busList = busMap.Values.Cast<IBI.Shared.Models.Bus>().ToList();

                    foreach (IBI.Shared.Models.Bus bus in busList)
                    {
                        Boolean scheduleOK = true;
                        Boolean infotainmentOK = true;
                        Boolean hotspotOK = true;

                        if (!string.IsNullOrEmpty(bus.InfotainmentMacAddress))
                            bus.LastInfotainmentPing = getLastInfotainmentPing(bus.InfotainmentMacAddress);

                        bus.LastHotspotPing = getLastHotspotPing(bus.BusNumber);

                        if (AppSettings.ShouldValidateSchedules())
                        {
                            scheduleOK = (bus.CurrentJourney == bus.ScheduledJourney);

                            if (!scheduleOK)
                                bus.Remarks += " - Wrong journey" + Environment.NewLine;
                        }
                        if (bus.LastInfotainmentPing != DateTime.MinValue)
                        {
                            infotainmentOK = (bus.LastInfotainmentPing.Value.Add(AppSettings.GetOfflineTolerance()) > bus.LastScheduleTime);
                            if (!infotainmentOK)
                                bus.Remarks += " - Infotainment offline" + Environment.NewLine;
                        }
                        if (bus.LastHotspotPing != DateTime.MinValue)
                        {
                            hotspotOK = (bus.LastHotspotPing.Value.Add(TimeSpan.FromTicks(AppSettings.GetOfflineTolerance().Ticks / 2)) > bus.LastScheduleTime);

                            if (!hotspotOK)
                                bus.Remarks += " - Hotspot offline" + Environment.NewLine;
                        }

                        if (bus.InOperation)
                            bus.ScheduledJourney = getScheduledJourneyNumber(bus.BusNumber, bus.CustomerId);

                        if (!scheduleOK || !infotainmentOK || !hotspotOK)
                        {
                            bus.IsOK = false;
                        }

                        if (bus.ScheduledJourney == 0 || !bus.InOperation)
                            bus.IsOK = true;
                    }

                    return busList.ToArray();
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);

                    throw new ApplicationException("Error getting buslist", ex);
                }
            }
        }

        public static void SyncAllBusesData()
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.SyncAllBusesData"))
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var buses = dbContext.Buses.Where(bus => bus.Groups.Count() == 0).ToList();
                    foreach (var bus in buses)
                    {
                        if (bus.Groups.Count == 0)
                        {
                            var groups = dbContext.Groups.Where(y => y.CustomerId == bus.CustomerId && y.ParentGroupId == null).ToList();
                            IBI.DataAccess.DBModels.Group groupToAdd = null;
                            foreach (var group in groups)
                            {
                                groupToAdd = group;
                            }
                            if (groupToAdd != null)
                            {
                                bus.Groups.Add(groupToAdd);
                            }
                        }
                    }
                    dbContext.SaveChanges();
                }
            }
        }


        public static void SyncAllSupportData()
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.SyncAllSupportData"))
            {
                Logger.LogMessage("SyncAllSupportData called");
                try
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        var selectedBuses = (from x in dbContext.Buses
                                             select x);

                        foreach (var bus in selectedBuses)
                        {
                            var selectedClients = (from x in dbContext.Clients
                                                   where x.BusNumber == bus.BusNumber
                                                   select x);

                            //Ticket #: {TicketNumber}, Ticket Headline: {TicketHeadline}
                            ArrayList arrList = getServiceLevel(Int32.Parse(bus.BusNumber));
                            bus.ServiceLevel = int.Parse(arrList[0].ToString());
                            bus.ServiceDetail = arrList[1].ToString();

                            foreach (var client in selectedClients)
                            {
                                client.MarkedForService = getMarkedForService(client.MacAddress);
                            }
                        }

                        dbContext.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                    Logger.LogError(ex);
                }
                finally
                {
                    Logger.LogMessage("SyncAllSupportData completed");
                }
            }
        }

        public static void DumpCurrentStatus()
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.DumpCurrentStatus"))
            {
                IBI.Shared.Models.Bus[] currentStatus = SystemServiceController.getBusList();

                DateTime logTimestamp = DateTime.Now;
                int min = logTimestamp.Minute - (logTimestamp.Minute % 15);
                logTimestamp = new DateTime(logTimestamp.Year, logTimestamp.Month, logTimestamp.Day, logTimestamp.Hour, min, 0);

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        foreach (IBI.Shared.Models.Bus bus in currentStatus)
                        {
                            String timestamp = "'" + logTimestamp.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            String isOK = (bus.IsOK ? "1" : "0");
                            String lastDCUPing = (bus.LastDCUPing == DateTime.MinValue ? "NULL" : "'" + bus.LastDCUPing.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            String lastCCUPing = (bus.LastCCUPing == DateTime.MinValue ? "NULL" : "'" + bus.LastCCUPing.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            String lastVTCPing = (bus.LastVTCPing == DateTime.MinValue ? "NULL" : "'" + bus.LastVTCPing.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            String lastInfotainmentPing = (bus.LastInfotainmentPing == DateTime.MinValue ? "NULL" : "'" + bus.LastInfotainmentPing.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            String lastHotspotPing = (bus.LastHotspotPing == DateTime.MinValue ? "NULL" : "'" + bus.LastHotspotPing.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            String lastSchedule = (bus.LastScheduleTime == DateTime.MinValue ? "NULL" : "'" + bus.LastScheduleTime.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            String scheduledJourneyNumber = bus.ScheduledJourney.ToString();
                            String actualJourneyNumber = bus.CurrentJourney.ToString();
                            String latitude = "'" + bus.Latitude + "'";
                            String longitude = "'" + bus.Longitude + "'";
                            String remarks = "'" + bus.Remarks + "'";
                            String markedForService = bus.MarkedForService ? "1" : "0";
                            String serviceLevel = bus.ServiceLevel.ToString();

                            Boolean insertLogEntry = false;

                            String script = "SELECT * FROM [BusStatusLog] WHERE [BusNumber]='" + bus.BusNumber + "' AND [Timestamp]='" + logTimestamp.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                            using (SqlCommand command = new SqlCommand(script, connection))
                            {
                                using (SqlDataReader dataReader = command.ExecuteReader())
                                {
                                    insertLogEntry = !dataReader.HasRows;

                                    if (dataReader.HasRows)
                                    {
                                        dataReader.Read();
                                        if (bus.IsOK && !dataReader.GetBoolean(dataReader.GetOrdinal("IsOK")))
                                            isOK = "0";
                                        dataReader.Close();
                                    }
                                }
                            }


                            if (insertLogEntry)
                            {
                                script = "INSERT INTO [BusStatusLog](" +
                                            "[Timestamp], " +
                                            "[BusNumber], " +
                                            "[IsOK], " +
                                            "[LastDCUPing], " +
                                            "[LastCCUPing], " +
                                            "[LastVTCPing], " +
                                            "[LastInfotainmentPing], " +
                                            "[LastHotspotPing], " +
                                            "[LastSchedule], " +
                                            "[ScheduledJourneyNumber], " +
                                            "[ActualJourneyNumber], " +
                                            "[Latitude], " +
                                            "[Longitude]," +
                                            "[Remarks]," +
                                            "[MarkedForService]," +
                                            "[ServiceLevel]" +
                                            ") " +
                                            "VALUES(" +
                                            timestamp + ", " +
                                            bus.BusNumber + ", " +
                                            isOK + ", " +
                                            lastDCUPing + ", " +
                                            lastCCUPing + ", " +
                                            lastVTCPing + ", " +
                                            lastInfotainmentPing + ", " +
                                            lastHotspotPing + ", " +
                                            lastSchedule + ", " +
                                            scheduledJourneyNumber + ", " +
                                            actualJourneyNumber + ", " +
                                            latitude + ", " +
                                            longitude + ", " +
                                            remarks + ", " +
                                            markedForService + ", " +
                                            serviceLevel +
                                            ")";

                                using (SqlCommand command = new SqlCommand(script, connection))
                                {
                                    using (new IBI.Shared.CallCounter("Database calls"))
                                    {
                                        command.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void PerformDatabaseMaintenance()
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.PerformDatabaseMaintenance"))
            {
                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        String script = "BACKUP LOG IBI_Data WITH TRUNCATE_ONLY" + Environment.NewLine +
                                        "GO" + Environment.NewLine +
                                        "DBCC SHRINKFILE ('IBI_Data_log', 1)" + Environment.NewLine +
                                        "GO" + Environment.NewLine +
                                        "DBCC SHRINKFILE ('IBI_Data', 1)" + Environment.NewLine +
                                        "GO";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }

        private static long getScheduledJourneyNumber(string busNumber, int customerId)
        {
            long journeyNumber = 0;

            using (new IBI.Shared.CallCounter("SystemServiceController.getScheduledJourneyNumber"))
            {
                try
                {
                    FileInfo scheduleFile = new FileInfo(Path.Combine(AppSettings.GetResourceDirectory(), customerId + "\\Cache\\Schedule_" + busNumber + ".xml"));

                    if (scheduleFile.Exists && scheduleFile.LastWriteTime > DateTime.Now.AddMinutes(-15))
                    {
                        XmlDocument schedule = new XmlDocument();
                        schedule.Load(scheduleFile.FullName);

                        XmlNode journeyNumberNode = schedule.SelectSingleNode("Schedule/JourneyNumber");

                        if (journeyNumberNode != null)
                            journeyNumber = long.Parse(journeyNumberNode.InnerText);
                    }
                    else
                    {
                        journeyNumber = -1;
                    }

                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                    Logger.LogError(ex);
                    journeyNumber = -1;
                }
            }

            return journeyNumber;
        }

        public static DateTime getLastHotspotPing(string busNumber)
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.getLastHotspotPing"))
            {
                try
                {
                    using (HotSpotDataModel hotSpotContext = new HotSpotDataModel())
                    {
                        DateTime? lastPing = (from x in hotSpotContext.AccessPoints
                                              where x.Name.EndsWith(string.Concat("-", busNumber)) && !x.IsDeleted && x.AccessPointGroup_ID != 11
                                              orderby x.LastPing descending
                                              select x.LastPing).FirstOrDefault();
                        return lastPing.HasValue ? lastPing.Value : lastPing.GetValueOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    return DateTime.MinValue;
                }
            }
        }

        public static DateTime getLastInfotainmentPing(string macAddress)
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.getLastInfotainmentPing"))
            {
                using (vTouchDataModel vTouchContext = new vTouchDataModel())
                {

                    var selectedIpPlayers = (from x in vTouchContext.vTouch_Server_Model_IPPlayer
                                             where x.MACAddress == macAddress
                                             select x).ToList();


                    foreach (var player in selectedIpPlayers)
                    {
                        return player.LastOnline.Value;
                    }
                }

                return DateTime.MinValue;
            }
        }


        private static ArrayList getServiceLevel(int busNumber)
        {
            ArrayList arrList = new ArrayList(2);
            // [KHI]: we will return following ServiceCase Attributes
            // 0:  ServiceLevel, 1: TicketNumber, 2: TicketHeadline

            int serviceLevel = 0;
            string serviceDetail = "";

            using (SqlConnection connection = AppSettings.GetNovoDatabaseConnection())
            {

                using (new IBI.Shared.CallCounter("SystemServiceController.getServiceLevel"))
                {
                    List<String> accountList = new List<string>();

                    foreach (String accountID in ConfigurationManager.AppSettings["NovoAccountIDs"].Split(new string[] { ";" }, StringSplitOptions.None))
                    {
                        accountList.Add("accounts.[ACCOUNT_ID]=" + accountID);
                    }

                    String script = "SELECT cases.[CASE_ID], cases.[CASE_TITLE], levels.[DEPART_ID] AS [LEVEL_ID], levels.[DEPART_NAME] AS [LEVEL]" + Environment.NewLine +
                                    "FROM" + Environment.NewLine +
                                    "  [UDF_VALUES] customValues" + Environment.NewLine +
                                    "  INNER JOIN" + Environment.NewLine +
                                    "  [CUSTOMERS] customers" + Environment.NewLine +
                                    "  ON customValues.[TABLE_REF]=customers.[CUSTOMER_ID]" + Environment.NewLine +
                                    "  INNER JOIN" + Environment.NewLine +
                                    "  [ACCOUNTS] accounts" + Environment.NewLine +
                                    "  ON customers.[ACCOUNT_REF]=accounts.[ACCOUNT_ID]" + Environment.NewLine +
                                    "  INNER JOIN" + Environment.NewLine +
                                    "  [CASES] cases" + Environment.NewLine +
                                    "  ON customValues.[TABLE_REF]=cases.[CUSTOMER_REF]" + Environment.NewLine +
                                    "  INNER JOIN [DEPARTMENTS] levels" + Environment.NewLine +
                                    "  ON cases.[DEPART_REF]=levels.[DEPART_ID]" + Environment.NewLine +
                                    "WHERE customValues.[DEFINITION_REF]=2" + Environment.NewLine +
                                    "  AND customValues.[UDF_VALUES_VARCHAR]='" + busNumber + "'" + Environment.NewLine +
                                    "  AND cases.[CASE_STATUS]<>8" + Environment.NewLine +
                                    "  AND (" + mermaid.BaseObjects.Functions.JoinListAsString(accountList, " OR ") + ")" + Environment.NewLine;

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        SqlDataReader dataReader;

                        try
                        {
                            connection.Open();
                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                dataReader = command.ExecuteReader();
                            }

                            using (dataReader)
                            {
                                while (dataReader.Read())
                                {
                                    serviceLevel = dataReader.GetInt32(dataReader.GetOrdinal("LEVEL_ID"));

                                    switch (serviceLevel)
                                    {
                                        case 2: // "1. level":
                                            { serviceLevel = 1; break; }

                                        case 3: // "2. level":
                                            { serviceLevel = 2; break; }

                                        case 4: // "3. level":
                                            { serviceLevel = 3; break; }

                                        case 6: // "4. level":
                                            { serviceLevel = 4; break; }

                                        case 7: // "Installation":
                                            { serviceLevel = 5; break; }

                                        case 5: // "no level (Kundens ansvar / Customer is Responsible)":
                                            { serviceLevel = 6; break; }

                                        case 1: // "Super Admin":
                                            { serviceLevel = 7; break; }

                                    }

                                    serviceDetail += "Ticket# " + AppUtility.IsNullStr(dataReader["CASE_ID"]) + "~" + AppUtility.IsNullStr(dataReader["CASE_TITLE"]) + "|";


                                }
                                serviceDetail = serviceDetail.TrimEnd('|');

                            }

                            arrList.Add(serviceLevel);
                            arrList.Add(serviceDetail);

                            return arrList;
                        }

                        catch (Exception ex)
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Novo, "NOVO PROB: " + ex);
                        }
                        finally
                        {
                            if (connection.State != System.Data.ConnectionState.Closed)
                                connection.Close();
                        }

                    }

                    arrList.Add(0);
                    arrList.Add("");

                    return arrList;
                }
            }



            /*To -DO
                    int serviceLevel = 0;

                    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["IBIDatabaseConnection"].ToString());

                    try 
                    {
                        SqlCommand cmd = new SqlCommand("GetServiceCase", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        con.Open();
                        serviceLevel = AppUtility.IsNullInt(cmd.ExecuteScalar());

                    }
                    catch(Exception ex)
                    {
                         Logger.appendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex); 
                    }
                    finally
                    {
                        if(con.State !=System.Data.ConnectionState.Closed)
                        {
                            con.Close();
                        }    
                    }

                    return serviceLevel;
                */
        }

        private static bool getMarkedForService(String macAddress)
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.getMarkedForService"))
            {
                using (vTouchDataModel vTouchContext = new vTouchDataModel())
                {
                    var selectedIpPlayers = (from x in vTouchContext.vTouch_Server_Model_IPPlayer
                                             where x.MACAddress == macAddress
                                             select x).ToList();
                    foreach (var player in selectedIpPlayers)
                    {
                        return player.MarkedForService.Value;
                    }
                }

                return false;
            }
        }


        internal static string GetClientConfigurations(string busNumber, int customerId)
        {
            String results = string.Empty;  
            
           StringBuilder configurations = new StringBuilder();

           using(IBIDataModel dbContext = new IBIDataModel())
           {
              var data = dbContext.GetClientConfigurations(busNumber, customerId).ToList();
               
               foreach(var item in data)
               {
                   //if (item.GroupId == null && string.IsNullOrEmpty(item.BusNumber) && item.CustomerID == null)
                   //    continue;

                   //if (item.GroupId == null && (String.IsNullOrEmpty(item.BusNumber) || item.CustomerID == null))
                   //    continue;

                   //if (item.GroupId != null && (!(String.IsNullOrEmpty(item.BusNumber)) || item.CustomerID != null))
                   //    continue;

                      


                   if(item.BusNumber==null && data.Where(i=>i.Key.Equals(item.Key) && String.IsNullOrEmpty(i.BusNumber)==false).Count()>0)
                       continue;

                   if(item.GroupId!=null && data.Where(i=>i.Key.Equals(item.Key) && item.GroupId < i.GroupId).Count()>0)
                       continue;


                   configurations.Append(String.Format("<Configuration><Key><![CDATA[{0}]]></Key><Value><![CDATA[{1}]]></Value></Configuration>", item.Key.Trim(), item.Value.Trim()));
               }
           }

           results = String.Format("<Configurations customerId='{0}' busNumber='{1}'>{2}</Configurations>", customerId.ToString(), busNumber, configurations.ToString());

           return results;
        }


         
        #endregion


    }
}
