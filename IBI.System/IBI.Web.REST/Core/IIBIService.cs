﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IBI.REST.Core
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface IIBIService
    {
        [OperationContract]
        void Ping(String busNo, String clientType, String pingData);
               
        [OperationContract]
        String GetCommand(String busNo, String clientType);

        [OperationContract]
        String MessagePing(String pingData);

        [OperationContract]
        String JourneyPing(String pingData);

    }
}
