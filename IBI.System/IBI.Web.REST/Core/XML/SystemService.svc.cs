﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using IBI.Shared.Models.BusTree;
using System.ServiceModel.Web;
using IBI.REST.Shared;
using IBI.Shared.Models.Accounts;
using User = IBI.Shared.Models.Accounts.User;
using System.ServiceModel.Activation;


namespace IBI.REST.Core.XML
{
    [DataContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BusTreeStatus
    {
        [DataMember]
        public IBI.Shared.Models.BusTree.GroupNode[] Groups { get; set; }
    }


    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SystemService : ISystemService
    {
        public SystemStatus GetSystemStatus()
        {
            SystemStatus status = new SystemStatus();
            status.BusStatus = SystemServiceController.getBusList();

            return status;
        }

        public string GetDataUpdateStatus(String busNumber, String customerId)
        {
            return SystemServiceController.GetDataUpdateStatus(busNumber, int.Parse(customerId));
        }
   
         
        //public String GetClientConfigurations(string busNumber, int customerId)
        //{
        //    String result = SystemServiceController.GetClientConfigurations(busNumber, customerId);
        //    return result;
        //}
    } 
}
