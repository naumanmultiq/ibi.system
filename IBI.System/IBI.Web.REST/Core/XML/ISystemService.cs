﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Shared.Models;

namespace IBI.REST.Core.XML
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    [XmlSerializerFormat]
    public interface ISystemService
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "SystemStatus", 
            RequestFormat = WebMessageFormat.Xml, 
            ResponseFormat = WebMessageFormat.Xml, 
            BodyStyle = WebMessageBodyStyle.Bare)]
        SystemStatus GetSystemStatus();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "DataUpdateStatus?busNumber={busNumber}&customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string GetDataUpdateStatus(String busNumber, String customerId);


        //[OperationContract]
        //[WebInvoke(Method = "GET",
        //    UriTemplate = "GetClientConfigurations?busNumber={busNumber}&customerId={customerId}",
        //    RequestFormat = WebMessageFormat.Xml,
        //    ResponseFormat = WebMessageFormat.Xml,
        //    BodyStyle = WebMessageBodyStyle.Bare)]
        //String GetClientConfigurations(string busNumber, int customerId);
    }
}
