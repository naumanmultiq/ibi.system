﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IBI.Shared.MSMQ;
using System.Messaging;

namespace MSMQ.TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnQueue_Click(object sender, EventArgs e)
        {
            MSMQManager qm = new MSMQManager(txtQueueName.Text);
            qm.Send(txtQueueData.Text, txtQueueData.Text);

            RefreshQueueList();
            
        }

        private void btnEnqueue_Click(object sender, EventArgs e)
        {
            MSMQManager qm = new MSMQManager(txtQueueName.Text);
            
            txtEnqueue.Text = ((System.Messaging.Message)qm.Receive()).Label;

            RefreshQueueList();
        }

        private void RefreshQueueList() {

            lstQueue.Items.Clear();

            QueueManager qm = new QueueManager(txtQueueName.Text);
            System.Messaging.Message[] msgs = qm.GetMessages();

            int index = 0;
            foreach (System.Messaging.Message mes in qm.GetMessages()) 
            {  
                mes.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                
                lstQueue.Items.Insert(0, mes.Body.ToString());
            }
            

        }
    }
}
