﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.AdiBusTimetables
{
    public class Journey
    {
        public int JourneyNumber { get; set; }
        public int ScheduleNumber { get; set; }
        public string BusNumber { get; set; }
        public int CustomerId { get; set; }
        public DateTime JourneyDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Line { get; set; }
        public string StartPoint { get; set; }
        public string Destination { get; set; }
        public string ViaName { get; set; }
        public DateTime? PlannedArrivalTime { get; set; }
        public DateTime? PlannedDepartureTime { get; set; }
        public string JourneyData { get; set; }
        public int? JourneyAhead { get; set; }
        public int? JourneyBehind { get; set; }
        public bool? IsLastStop { get; set; }
        public string TripId { get; set; }


        public ArrayList JourneyStops { get; set; }


        public Journey()
        {
            //Stops = new List<JourneyStop>();
            JourneyStops = new ArrayList();
        }
    }

    class StopHop
    {
        public string StopNumber { get; set; }
        public string StopName { get; set; }
        public string ArrivalTime { get; set; }
        public string DepartureTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Zone { get; set; }
        public string IsCheckpoint { get; set; }
        public int Day2addArrival { get; set; }
        public int Day2addDeparture { get; set; }


        public StopHop(string stopNumber, string stopName, string arrivalTime, string departureTime, string lat, string lon, string zone, string isCheckpoint, int day2AddArrival, int day2AddDeparture)
        {
            this.StopNumber = stopNumber;
            this.StopName = stopName;
            this.ArrivalTime = arrivalTime;
            this.DepartureTime = departureTime;
            this.Latitude = lat;
            this.Longitude = lon;
            this.Zone = zone;
            this.IsCheckpoint = String.IsNullOrEmpty(IsCheckpoint) ? "false" : isCheckpoint;
            this.Day2addArrival = day2AddArrival;
            this.Day2addDeparture = day2AddDeparture;
        }
    }
}
