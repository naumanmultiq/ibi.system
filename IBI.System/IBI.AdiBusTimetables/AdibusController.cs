﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
//using IBI.Shared;
using IBI.Shared.Diagnostics;
using IBI.DataAccess.DBModels;
using System.Data.Entity.Spatial;
using System.Configuration;
using IBI.Shared.ConfigSections;
using System.Timers;
//using mermaid.BaseObjects.Diagnostics;

namespace IBI.AdiBusTimetables
{
    public class AdibusController : IDisposable
    {
        #region Data Members

        //static Int64 SubscriptionCounter
        //{
        //    get
        //    {
        //        if (SubscriptionCounter != null && SubscriptionCounter >= 0)
        //            return SubscriptionCounter + 1;
        //        else
        //        {
        //            SubscriptionCounter = 1;
        //            return SubscriptionCounter;
        //        }
        //    }
        //}

        public List<SubscriptionTimer> SubscriptionTimers { get; set; }

        #endregion
        
        #region Constructor

        public AdibusController()
        {
            //Clear existing Cache elements
            IBI.Cache.JourneyCache.ClearZonesCache();
            IBI.Cache.JourneyCache.ClearSchedulesCache();
            IBI.Cache.JourneyCache.ClearRouteCache();

            ////Fill cache elements from DB
            IBI.Cache.JourneyCache.FillRouteDirections();
            IBI.Cache.JourneyCache.FillSchedules();

            VdvSubscriptionConfiguration[] configurations = VdvSubscriptionConfiguration.ReadAllConfigurations();

            SubscriptionTimers = new List<SubscriptionTimer>();
            foreach (VdvSubscriptionConfiguration config in configurations)
            {
                SubscriptionTimer subTimer = new SubscriptionTimer(config);

                SubscriptionTimers.Add(subTimer);
            }
        }


        #endregion

        #region Events


        public void Dispose()
        {
            foreach (SubscriptionTimer ct in SubscriptionTimers)
            {
                ct.Dispose();
            }

        }
        #endregion
        
        #region Methods



        #endregion

        #region Private Helper

      

        #endregion
         
    }




}
