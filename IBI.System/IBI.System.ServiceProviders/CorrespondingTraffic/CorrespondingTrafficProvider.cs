﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBI.Shared.Interfaces.ServiceProviders;

namespace IBI.ServiceProviders.CorrespondingTraffic
{
    class CorrespondingTrafficProvider : ICorrespondingTrafficProvider
    {


        #region Attributes

        private int _customerId;
        private string _username;
        private string _password;
        private string _url;
        private string _stopInfoUrl;

        #endregion

        #region "Properties"
        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                this._customerId = value;
            }
        }

        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
            }
        }

        public string StopInfoUrl
        {
            get
            {
                return _stopInfoUrl;
            }
            set
            {
                _stopInfoUrl = value;
            }
        }
        #endregion


        public virtual string GetDepartureBoard(List<KeyValuePair<string, object>> parameters, ref string logEntry)
        {
            throw new NotImplementedException();
        }




    }
}
