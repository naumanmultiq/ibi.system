﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace IBI.ServiceProviders.CorrespondingTraffic
{
    public class TransportType
    {
        public TransportType(){}

        public TransportType(string typeName, List<TransportType> subTypes = null)
        {
            this.TypeName = typeName;
            if(subTypes!=null)
            {
                this.SubTypes = subTypes;
            }
        }

        public string TypeName { get; set; }
        public string MappedTypeName { get; set; }
        public string Line { get; set; }
        public List<TransportType> SubTypes
        {
            get; set;            
        }


        //public List<TransportType> GetTypes()
        //{
        //    string result = "Failure";

        //        try
        //        {
        //            string schFilePath = Path.Combine(Environment.CurrentDirectory, "{0}\\TransportTypes.xml");

        //            if (File.Exists(schFilePath))
        //            {
        //                string scheduleText = File.ReadAllText(schFilePath, System.Text.Encoding.UTF8);
        //                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        //                MemoryStream journeyStream = new MemoryStream(encoding.GetBytes(scheduleText));

        //                XDocument xDoc = XDocument.Load(journeyStream);

        //                //XDocument xDoc = XDocument.Load(schFilePath);


        //                var plannedDepartureTime = (from p in xDoc.Descendants("PlannedDepartureTime")
        //                                            select p.Value).FirstOrDefault();
        //                var plannedArrivalTime = (from p in xDoc.Descendants("PlannedDepartureTime")
        //                                          select p.Value).LastOrDefault();

        //                bool correctDate;
        //                if (!string.IsNullOrEmpty(plannedArrivalTime))
        //                {
        //                    DateTime pArrival;
        //                    correctDate = DateTime.TryParseExact(plannedArrivalTime, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out pArrival);

        //                    if (correctDate)
        //                        currentJourney.PlannedArrivalTime = pArrival;
        //                }

        //                if (!string.IsNullOrEmpty(plannedDepartureTime))
        //                {
        //                    DateTime pDeparture;
        //                    correctDate = DateTime.TryParseExact(plannedDepartureTime, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out pDeparture);

        //                    if (correctDate)
        //                        currentJourney.PlannedDepartureTime = pDeparture;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY, ex);
        //            result = "Failure";
        //        } 

        //    System.Text.ASCIIEncoding rEncoding = new System.Text.ASCIIEncoding();
        //    MemoryStream ms = new MemoryStream(rEncoding.GetBytes(result));
        //    WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
        //    return ms;

        //}

        //public static List<TransportType> ReadTransportTypes()
        //{
        //    //read all from configuration xml file and save in a List<TransportType>

        //    //hard coded



        //    //List<TransportType> busList = new List<TransportType>();            

        //    //busList.Add(new TransportType("BUS"))
        //    //busList.Add(new TransportType("NB"))
        //    //busList.Add(new TransportType("EXB"))
        //    //busList.Add(new TransportType("TB"))



        //    //List<TransportType> trainList = new List<TransportType>();            

        //    //trainlist.Add(new TransportType("S"))
        //    //trainlist.Add(new TransportType("M"))
        //    //trainlist.Add(new TransportType("REG"))
        //    //trainlist.Add(new TransportType("TOG"))
        //    //trainlist.Add(new TransportType("IC"))
        //    //trainlist.Add(new TransportType("LYN"))




        //}

    }


}
