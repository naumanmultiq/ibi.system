﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBI.Shared.Interfaces;
using System.Reflection;
using System.IO;
using System.Xml;
using mermaid.BaseObjects.Diagnostics;
using System.Web;
using IBI.Shared.Interfaces.ServiceProviders;


namespace IBI.ServiceProviders
{
    public class ProviderFactory : IProviderFactory
    {
        public IScheduleProvider GetSchedularProvider(int customerId)
        {
            try
            {
                Assembly currentAssembly = Assembly.GetAssembly(typeof(ProviderFactory));
                if (currentAssembly == null)
                {
                    return null;
                }

                string filePath = Path.Combine(Path.GetDirectoryName(currentAssembly.CodeBase), "Schedular_Configuration.xml");
                filePath = filePath.Replace("file:\\", string.Empty);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                XmlDocument configDocument = new XmlDocument();
                configDocument.Load(filePath);
                XmlNode customerNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]");
                if (customerNode == null)
                {
                    return null;
                }

                XmlNode classNameNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]/Class");
                if (classNameNode == null)
                {
                    return null;
                }

                string schedularClassName = classNameNode.InnerText;
                if (string.IsNullOrEmpty(schedularClassName))
                {
                    return null;
                }

                Type[] types = currentAssembly.GetTypes();
                Type runTimeClass = currentAssembly.GetType(schedularClassName);
                if (runTimeClass == null)
                {
                    return null;
                }

                IScheduleProvider schedular = (IScheduleProvider)Activator.CreateInstance(runTimeClass);
                schedular.CustomerId = customerId;
                return schedular;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("SyncSchedular", ex.Message + ex.StackTrace);
            }
            return null;
        }

        
        public ICorrespondingTrafficProvider GetCorrespondingTrafficProvider(int customerId)
        {
            try
            {
                Assembly currentAssembly = Assembly.GetAssembly(typeof(ProviderFactory));
                if (currentAssembly == null)
                {
                    return null;
                }

                string filePath = Path.Combine(Path.GetDirectoryName(currentAssembly.CodeBase), "Schedular_Configuration.xml");
                filePath = filePath.Replace("file:\\", string.Empty);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                XmlDocument configDocument = new XmlDocument();
                configDocument.Load(filePath);
                XmlNode customerNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]");
                if (customerNode == null)
                {
                    return null;
                }

                XmlNode classNameNode = configDocument.SelectSingleNode("//Configurations/Configuration[CustomerID=" + customerId.ToString() + "]/DepartureClass");
                if (classNameNode == null)
                {
                    return null;
                }

                string departureBoardClassName = classNameNode.SelectSingleNode("Class").InnerText;
                if (string.IsNullOrEmpty(departureBoardClassName))
                {
                    return null;
                }

                Type[] types = currentAssembly.GetTypes();
                Type runTimeClass = currentAssembly.GetType(departureBoardClassName);
                if (runTimeClass == null)
                {
                    return null;
                }

                ICorrespondingTrafficProvider departureBoard = (ICorrespondingTrafficProvider)Activator.CreateInstance(runTimeClass);

                departureBoard.CustomerId = customerId;
                departureBoard.Url = classNameNode.SelectSingleNode("Url").InnerText;
                departureBoard.Username = classNameNode.SelectSingleNode("Username").InnerText;
                departureBoard.Password = classNameNode.SelectSingleNode("Password").InnerText;
                departureBoard.StopInfoUrl = classNameNode.SelectSingleNode("StopInfoUrl").InnerText;
                return departureBoard;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("SyncDepartureBoard", ex.Message + ex.StackTrace);
            }
            return null;
        }
    }
}
